# Goal: Study whether or not using objects in the photon container is useful

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"

f_search = ["SlepSlep*100_0_0p1ns", "SlepSlep*100_0_1ns"]

variables = {
        #"pt":       {"nbins": 25,    "xmin": 0.,     "xmax": 200.,     "xlabel": "p_{T} [GeV]"},
        "d0":       {"nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "d_{0} [mm]"},
        }

for s in f_search:
    print s

    c = ROOT.TCanvas("c_%s"%s, "c_%s"%s)
    f_string = "%s/*%s*trees.root/*.root"%(d, s)
    tree = getTree(f_string)
    hist = getHist(tree, "h_%s"%s, "lepton_d0[0]:lepton_d0[1]", "25, 0., 100., 25, 0., 100.")
    hist.SetMarkerColor(ROOT.kWhite)
    hist.Draw("colz text")
    raw_input("...")
    c.SaveAs("plots/d0_%s_unweighted.pdf"%s)

    # Get crossection
    files = glob.glob(f_string)
    xs = getXS(files[0].split("/")[-2].split(".")[3])
    n_events = getNEvents(f_string)
    print "Cross section:", xs, "N events:", n_events
    ev_weight = lumi*xs/n_events
    print ev_weight

    hist.Scale(ev_weight)
    hist.Draw("colz text")
    c.Update()
    raw_input("...")
    c.SaveAs("plots/d0_%s_weighted.pdf"%s)

exit()


#print "\nWorking with point lt =",lt,"m =",m
files = []
for search in f_search:
    f_string = "%s/*%s*trees.root/*.root"%(d, search)
    files += glob.glob(f_string)
print "Found", len(files), "files."
print files
tree = getTreeFromList(files)
samples["initial_rdf"] = ROOT.ROOT.RDataFrame(tree)

# Get crossection
xs = getXS(files[0].split("/")[-2].split(".")[3])
n_events = getNEvents(f_string)
print "Cross section:", xs, "N events:", n_events
ev_weight = lumi*xs/n_events
print "Found", samples["initial_rdf"].Count().GetValue(), "events."

# Make basic event selection
df_init = samples["initial_rdf"].Filter(event_selection, "ee selection")

# Define needed variables
e_fcloose = '''
        std::vector<double> v;
        for (int i=0; i<electron_pt.size(); i++) {
            float fcloose = 0;
            if (electron_topoetcone20[i]/electron_pt[i] < 0.20 && electron_ptvarcone20[i]/electron_pt[i] < 0.15) fcloose = 1;
            v.push_back((double)fcloose);
        }
        return v;
'''
e_nholes = '''
    std::vector<float> si_layers{33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554};
    std::vector<int> n_pix_layers{4, 3, 2, 1, 0, 0, 0, 0};
    std::vector<int> n_sct_layers{8, 8, 8, 8, 8, 6, 4, 2};
    std::vector<double> v;
    for (int i=0; i<electron_pt.size(); i++){
        int n_sct_exp = 0;
        int n_pix_exp = 0;
        for (int j=0; j<si_layers.size(); j++)
            if ( electron_RFirstHit[i] < si_layers[j] + 10 ){
                n_sct_exp = n_sct_layers[j];
                n_pix_exp = n_pix_layers[j];
                break;
            }
        int n_sct_missing = std::max( n_sct_exp - electron_nSCT[i], 0 );
        int n_pix_missing = std::max( n_pix_exp - electron_nPIX[i], 0 );
        int n_holes = n_sct_missing + n_pix_missing;
        v.push_back((double)n_holes);
    }
    return v;
'''
df_defs = df_init.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt") \
        .Define("electron_nholes", e_nholes) \
        .Define("electron_fcloose", e_fcloose)

print "ee selection:", event_selection
df_baseline = df_defs.Filter(e_baseline.getCutString(0), "e0 baseline").Filter(e_baseline.getCutString(1), "e1 baseline")
print "e0 selection:", e_baseline.getCutString(0)
print "e1 selection:", e_baseline.getCutString(1)

# Add filtered variables
# Doing this with a funky executable string... Surely there is a better way
exec_str = "df_vars = df_baseline"
for v_ref in variables:
    exec_str += ".Define(\"e_baseline_%s\", e_baseline.selectedVectorString(\"electron_%s\"))"%(v_ref, v_ref)
    exec_str += ".Define(\"e0_baseline_%s\", \"e_baseline_%s[0]\")"%(v_ref, v_ref)
    exec_str += ".Define(\"e1_baseline_%s\", \"e_baseline_%s[1]\")"%(v_ref, v_ref)
exec(exec_str)
print "Adding variables..."
print exec_str

# Plot
df = df_vars

for v_ref in variables:
    samples["2Dhists"][v_ref] = {}
    samples["2Dmods"][v_ref] = {}
    samples["2Dcans"][v_ref] = {}

    # Create the histograms
    samples["2Dmods"][v_ref] = ROOT.RDF.TH2DModel("h_%s_compare_leptons"%(v_ref), "h_%s_compare_leptons"%(v_ref), variables[v_ref]["nbins"], variables[v_ref]["xmin"], variables[v_ref]["xmax"], variables[v_ref]["nbins"], variables[v_ref]["xmin"], variables[v_ref]["xmax"])
    samples["2Dhists"][v_ref] = df.Histo2D(samples["2Dmods"][v_ref], "e0_baseline_%s"%v_ref, "e1_baseline_%s"%v_ref)

# Draw plots
for v1 in samples["2Dhists"]:
    for lt in lifetimes:
        for m in masses:
            can_name = "c_%s_compare_leptons"%(v1)
            samples["2Dcans"][v1] = ROOT.TCanvas(can_name, can_name)
            samples["2Dhists"][v1].Scale(ev_weight)
            samples["2Dhists"][v1].SetMaximum(1e2)
            samples["2Dhists"][v1].SetMinimum(1e-2)
            samples["2Dhists"][v1].GetXaxis().SetTitle("Leading Electron %s"%variables[v1]["xlabel"])
            samples["2Dhists"][v1].GetYaxis().SetTitle("Subleading Electron %s"%variables[v1]["xlabel"])
            samples["2Dhists"][v1].Draw("colz")
            samples["2Dcans"][v1].SetLogz()
            print "Saving canvas:", "plots/"+can_name+append+".pdf"
            samples["2Dcans"][v1].SaveAs("plots/"+can_name+append+".pdf")


