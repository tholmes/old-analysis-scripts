# Goal: Study whether or not using objects in the photon container is useful

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"

# File type
f_search = "SlepSlep"
append = "_dptcut_d0cut_v2"
#append = "_v2"

# Define grid
#lifetimes = ["0p01ns", "0p1ns", "1ns"]
lifetimes = ["*ns"]
#masses = ["50", "100", "200", "300", "400", "500", "700"]
#masses = ["100", "300", "500"]
masses = ["100"]
#maximums = {"100":22, "300":7.5, "500":2.2}

# Channels
# 0 = ee, 1 = mm, 2 = em, 3 = me

variables = {
        "pt":  {"nbins": 25,    "xmin": 0.,     "xmax": 200.,     "xlabel": "True Electron p_{T} [GeV]"},
        "d0":  {"nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "True Electron d_{0} [mm]"},
        "R":   {"nbins": 60,    "xmin": 0.,   "xmax": 600.,       "xlabel": "True Electron decay R [mm]"},
        "reco_RFirstHit":       {"nbins": 60,    "xmin": 0.,   "xmax": 600.,       "xlabel": "Radius of First Hit [mm]"},
        "reco_dpt":             {"nbins": 25,    "xmin": -1.,   "xmax": 5., "xlabel": "(p_{T}^{track} - p_{T})/p_{T}"},
        }

# Loop over samples
samples = {}
for lt in lifetimes:
    samples[lt] = {}
    for m in masses:
        samples[lt][m] = {"hists": {}, "effhists": {}, "cans": {}, "mods": {}, "2Dhists": {}, "2Dcans": {}, "2Dmods": {}}

        print "\nWorking with point lt =",lt,"m =",m
        f_string = "%s/*%s*_%s*%s*trees.root/*.root"%(d, f_search, m, lt)
        files = glob.glob(f_string)
        print "Found", len(files), "files."
        print files
        tree = getTree(f_string)
        dframe = ROOT.ROOT.RDataFrame(tree)

        # Get crossection
        xs = getXS(files[0].split("/")[-2].split(".")[3])
        n_events = getNEvents(f_string)
        print "Cross section:", xs, "N events:", n_events
        ev_weight = lumi*xs/n_events
        print "Found", dframe.Count().GetValue(), "events."

        # Do basic event skimming (only ee events)
        n_e = "int n_e = 0; for (int i=0; i<truthLepton_pt.size(); i++) { if (abs(truthLepton_pdgId[i])==11 && truthLepton_pt[i] > 65) n_e++; } return n_e;"
        df_ne = dframe.Define("n_true_e", n_e).Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt")
        df_ee = df_ne.Filter("n_true_e > 1", "ee selection")
        truth_R = '''auto v = truthLepton_pt; v.clear();
                for (int i=0; i<truthLepton_pt.size(); i++)
                    v.push_back((double)TMath::Sqrt(truthLepton_VtxX[i]*truthLepton_VtxX[i]+truthLepton_VtxY[i]*truthLepton_VtxY[i]));
                return v;'''
        df_trueR = df_ee.Define("truthLepton_R", truth_R)

        # Select all truth-matched electrons
        matched_e_pt = '''auto v = electron_pt; v.clear();
        for (int i=0; i<electron_pt.size(); i++)
            for (int j=0; j<truthLepton_barcode.size(); j++)
                if ( abs(truthLepton_pdgId[j])==11 && electron_passOR[i] && electron_pt[i]>65 && abs(electron_d0[i])>3 && electron_dpt[i] > -0.5 && electron_truthMatchedBarcode[i] == truthLepton_barcode[j] )
                    v.push_back((double)truthLepton_pt[j]);
        return v;'''
        matched_e_d0 = matched_e_pt.replace("truthLepton_pt", "truthLepton_d0")
        matched_e_R  = matched_e_pt.replace("truthLepton_pt", "truthLepton_R")
        matched_e_reco_RFirstHit= matched_e_pt.replace("truthLepton_pt[j]", "electron_RFirstHit[i]")
        matched_e_reco_dpt      = matched_e_pt.replace("truthLepton_pt[j]", "electron_dpt[i]")
        df_matched_vars = df_trueR.Define("tm_electron_pt", matched_e_pt) \
                                .Define("tm_electron_d0", matched_e_d0) \
                                .Define("tm_electron_R", matched_e_R) \
                                .Define("tm_electron_reco_RFirstHit", matched_e_reco_RFirstHit) \
                                .Define("tm_electron_reco_dpt", matched_e_reco_dpt)

        # Plot
        df = df_matched_vars

        for v_ref in ["R"]:
            samples[lt][m]["2Dhists"][v_ref] = {}
            samples[lt][m]["2Dmods"][v_ref] = {}
            samples[lt][m]["2Dcans"][v_ref] = {}
            for v in variables:

                if not v == v_ref:
                    samples[lt][m]["2Dhists"][v_ref][v] = {}
                    samples[lt][m]["2Dmods"][v_ref][v] = {}
                    samples[lt][m]["2Dcans"][v_ref][v] = {}
                    t = "true"
                    samples[lt][m]["2Dmods"][v_ref][v][t] = ROOT.RDF.TH2DModel("h_%s_%s_vs_%s"%(t,v_ref, v), t, variables[v_ref]["nbins"], variables[v_ref]["xmin"], variables[v_ref]["xmax"], variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
                    samples[lt][m]["2Dhists"][v_ref][v][t] = df.Histo2D(samples[lt][m]["2Dmods"][v_ref][v][t], "tm_electron_%s"%v_ref, "tm_electron_%s"%v)

# Draw plots
for v1 in samples[lt][m]["2Dhists"]:
    for v2 in samples[lt][m]["2Dhists"][v1]:
        for lt in lifetimes:
            for m in masses:
                can_name = "c_%s_vs_%s_%s_%s"%(v1, v2, m, lt)
                print samples[lt][m]["2Dhists"]
                samples[lt][m]["2Dcans"][v1][v2] = ROOT.TCanvas(can_name, can_name)
                samples[lt][m]["2Dhists"][v1][v2]["true"].SetMaximum(10e4)
                samples[lt][m]["2Dhists"][v1][v2]["true"].SetMinimum(1)
                samples[lt][m]["2Dhists"][v1][v2]["true"].GetXaxis().SetTitle(variables[v1]["xlabel"])
                samples[lt][m]["2Dhists"][v1][v2]["true"].GetYaxis().SetTitle(variables[v2]["xlabel"])
                samples[lt][m]["2Dhists"][v1][v2]["true"].Draw("colz")
                samples[lt][m]["2Dcans"][v1][v2].SetLogz()
                print "Saving canvas:", "plots/"+can_name+append+".pdf"
                samples[lt][m]["2Dcans"][v1][v2].SaveAs("plots/"+can_name+append+".pdf")


