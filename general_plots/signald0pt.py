import ROOT
import math
import glob
import os
import sys

execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)





_f0 = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14//v5.7_Aug12_lifetimefix/signal/399042e.root")
_f1 = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14//v5.7_Aug12_lifetimefix/signal/399043e.root")
_f2 = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14//v5.7_Aug12_lifetimefix/signal/399044e.root")

_t0 = _f0.Get("trees_SR_highd0_")
_t1 = _f1.Get("trees_SR_highd0_")
_t2 = _f2.Get("trees_SR_highd0_")

_h0 = ROOT.TH1F("h0","h0",25,0, 300)
_h1 = ROOT.TH1F("h1","h1",25,0, 300)
_h2 = ROOT.TH1F("h2","h2",25,0, 300)

_v0 = ROOT.TH1F("v0","v0",30,0, 1000)
_v1 = ROOT.TH1F("v1","v1",30,0, 1000)
_v2 = ROOT.TH1F("v2","v2",30,0, 1000)

_h2d = ROOT.TH2F("h2d","h2d",25,0,600,25,0,200)
_h2d_lt = ROOT.TH2F("h2d_lt","h2d_lt",25,0,300,15,0,5)

for event in _t0:
    for il in xrange(len(event.truthLepton_pdgId)):
        if abs(event.truthLepton_pdgId[il]) == 13:
            _h0.Fill(event.truthLepton_d0[il])
        break
    for ip in xrange(len(event.truthSparticle_VtxX)):
        _v0.Fill(math.sqrt(event.truthSparticle_VtxX[ip]**2 + event.truthSparticle_VtxY[ip]**2))

for event in _t1:
    for il in xrange(len(event.truthLepton_pdgId)):
        if abs(event.truthLepton_pdgId[il]) == 13:
            _h1.Fill(event.truthLepton_d0[il])
        break
    for ip in xrange(len(event.truthSparticle_VtxX)):
        _v1.Fill(math.sqrt(event.truthSparticle_VtxX[ip]**2 + event.truthSparticle_VtxY[ip]**2))

for event in _t2:
    for il in xrange(len(event.truthLepton_pdgId)):
        if abs(event.truthLepton_pdgId[il]) == 13:
            _h2.Fill(event.truthLepton_d0[il])
            _h2d.Fill(event.truthLepton_pt[il], event.truthLepton_d0[il])
            
            #find parent
            for ip in xrange(len(event.truthSparticle_pdgId)):
                if event.truthSparticle_barcode[ip] == event.truthLepton_parentBarcode[il]:
                    _h2d_lt.Fill(event.truthLepton_d0[il], event.truthSparticle_properLifetime[ip]*1000000)
            break
    for ip in xrange(len(event.truthSparticle_VtxX)):
        _v2.Fill(math.sqrt(event.truthSparticle_VtxX[ip]**2 + event.truthSparticle_VtxY[ip]**2))
            

c = ROOT.TCanvas()

ROOT.gPad.SetLogy()
_h0.SetLineColor(ROOT.kBlack)
_h0.Scale(1/_h0.Integral())
_h0.SetLineWidth(3)
_h1.SetLineColor(ROOT.kCyan+2)
_h1.SetLineStyle(7)
_h1.SetLineWidth(3)
_h1.Scale(1/_h1.Integral())
_h2.SetLineColor(ROOT.kViolet-3)
_h2.SetLineStyle(2)
_h2.SetLineWidth(3)
_h2.Scale(1/_h2.Integral())


_h0.GetXaxis().SetTitle("Leading Lepton #left|d_{0}#right| [mm]")
_h0.GetYaxis().SetTitle("Normalized Number of Leptons")
leg = ROOT.TLegend(0.55,0.59,0.80,0.78)
leg.SetTextFont(42)
leg.SetTextSize(0.04)
leg.AddEntry(_h0,"#tau(#tilde{l}) = 0.01 ns", "l")
leg.AddEntry(_h1,"#tau(#tilde{l}) = 0.1 ns", "l")
leg.AddEntry(_h2,"#tau(#tilde{l}) = 1 ns", "l")
leg.SetFillStyle(0)


_h0.Draw("hist")
_h1.Draw("hist same")
_h2.Draw("hist same")
leg.Draw("same")
ROOT.ATLASLabel(0.40,0.88, "Simulation Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
text.DrawLatex(0.615,0.84, "#sqrt{s} = 13 TeV" )
text2 = ROOT.TLatex()
text2.SetNDC()
text2.SetTextSize(0.04)
text2.DrawLatex(0.60,0.79, "m(#tilde{l}) = 400 GeV" )
c.Modified()
c.Update()
c.SaveAs("signal_d0.pdf")
c.SaveAs("signal_d0.eps")
c.SaveAs("signal_d0.root")
c.SaveAs("signal_d0.C")

_v0.SetLineColor(ROOT.kBlack)
_v1.SetLineColor(ROOT.kCyan+2)
_v2.SetLineColor(ROOT.kViolet-3)

_v0.GetXaxis().SetTitle("LLP decay radius (xy) [mm]")
leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
#ROOT.gStyle.SetTextFont(42)
leg.AddEntry(_v0,"#tau(l) = 0.01 ns", "l")
leg.AddEntry(_v1,"#tau(l) = 0.1 ns", "l")
leg.AddEntry(_v2,"#tau(l) = 1 ns", "l")
leg.SetFillStyle(0)


_v0.Draw("hist")
_v1.Draw("hist same")
_v2.Draw("hist same")
leg.Draw("same")
c.Modified()
c.Update()
c.SaveAs("signalRxy.pdf")

ROOT.gPad.SetLogy(0)

c.Clear()
_h2d.GetYaxis().SetTitle("leading muon d_{0} [mm]")
_h2d.GetXaxis().SetTitle("leading muon p_{T} [GeV]")
_h2d.Draw("COLZ")
ROOT.gPad.SetLogz()
c.Modified()
c.Update()
c.SaveAs("signal_ptd0.pdf")


c.Clear()
_h2d_lt.GetYaxis().SetTitle("parent lifetime [ns]")
_h2d_lt.GetXaxis().SetTitle("leading lepton d_{0} [mm]")
_h2d_lt.Draw("COLZ")
ROOT.gPad.SetLogz()
c.Modified()
c.Update()
c.SaveAs("signal_ltd0.pdf")
