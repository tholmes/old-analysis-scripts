import ROOT

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

files = {
        "ttbar": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar_lowpt/ttbar.root",
        "B-filtered photons": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.3_lowd0/photons_wip/photon.root",
        #"data": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.3_lowd0/data_wip/merged/*.root",
        }

plot_HF = True
append = "_mc_nothf"
if plot_HF: append = "_mc_hf"

#append = "_data_signal"

hists = {}
for f in files:
    print files[f]
    t = getTree(files[f])

    # Selections
    #event_sel = "lepton_n_signal<2 && electron_n_baseline>1"
    #e_tag = Selection("tag")
    #e_tag.addCut("electron_pt", 160)
    #e_tag.addCut("electron_d0", 2, abs_val=True)
    #e_tag.addCut("!electron_isSignal")
    #e_probe = Selection("probe")
    #e_probe.addCut("electron_pt", 50)
    #e_probe.addCut("electron_isGoodQual")

    #event_sel = "electron_n_baseline == 1 && muon_n_basline == 0 && electron_n_signal < 2"
    event_sel = "electron_n_baseline > 0"
    e_probe = Selection("probe")
    e_probe.addCut("electron_pt", 50)
    #e_probe.addCut("electron_d0", 2, abs_val=True)
    #e_probe.addCut("electron_isGoodQual")
    if plot_HF: e_probe.addCut("electron_truthOrigin", minval=22, maxval=36)
    else: e_probe.addCut("!(electron_truthOrigin > 22 & electron_truthOrigin < 36) && !(electron_truthOrigin > 8 && electron_truthOrigin < 13)")

    #sel_all = "%s && %s && %s "%(event_sel, e_tag.getCutString(0), e_probe.getCutString(1))
    #sel_anti = "electron_FCTightTTVA[1]==0 && %s"%sel_all
    #sel_all_65 = "%s && %s && %s && electron_pt[1]>65"%(event_sel, e_tag.getCutString(0), e_probe.getCutString(1))
    #sel_anti_65 = "electron_FCTightTTVA[1]==0 && %s"%sel_all_65

    sel_all = "%s && %s"%(event_sel, e_probe.getCutString())
    sel_anti = "electron_FCTightTTVA==0 && %s"%sel_all

    #h_all = getHist(t, "h_d0_all", "abs(electron_d0)", "20,0,20", sel_all)
    #h_anti = getHist(t, "h_d0_anti", "abs(electron_d0)", "20,0,20", sel_anti)
    h_all = getHist(t, "h_d0_all", "abs(electron_d0)", "4,0,1", "normweight * (%s)"%sel_all)
    h_anti = getHist(t, "h_d0_anti", "abs(electron_d0)", "4,0,1", "normweight * (%s)"%sel_anti)
    #h_all = getHist(t, "h_d0_all", "abs(electron_d0[1])", "10,0,20", sel_all)
    #h_anti = getHist(t, "h_d0_anti", "abs(electron_d0[1])", "10,0,20",  sel_anti)
    #h_all_65 = getHist(t, "h_d0_all_65", "abs(electron_d0[1])", "10,0,20", sel_all_65)
    #h_anti_65 = getHist(t, "h_d0_anti_65", "abs(electron_d0[1])", "10,0,20", sel_anti_65)

    eff = ROOT.TGraphAsymmErrors(h_anti, h_all)
    #eff_65 = ROOT.TGraphAsymmErrors(h_anti_65, h_all_65)

    hists[f] = eff

can = ROOT.TCanvas("c", "c")
for i, f in enumerate(files):
    if i==0:
        hists[f].Draw("alpe")
        hists[f].GetYaxis().SetRangeUser(0,1.4)
        ROOT.gPad.Update()
    else:
        hists[f].Draw("lpe same")
    hists[f].SetLineColor(theme_colors[i])
    hists[f].SetMarkerColor(theme_colors[i])
    hists[f].SetTitle("%s; electron d_{0} [mm]; Fraction Anti-Isolated"%f)

leg = can.BuildLegend(.5,.75,.8,.90)
ROOT.ATLASLabel(0.2,0.85, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
if plot_HF: text.DrawLatex(0.2,0.80, "baseline electrons from HF")
else: text.DrawLatex(0.2,0.80, "baseline electrons not from HF")
#text.DrawLatex(0.2,0.80, "signal electrons in data")
can.SaveAs("e_anti_ratio%s.pdf"%append)
raw_input("...")
