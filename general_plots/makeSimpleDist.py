import ROOT

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.3_lowd0/data_wip/merged/*.root"
t = getTree(d)

# Selections
event_sel = "lepton_n_signal<2 && electron_n_baseline>1"
e_tag = Selection("tag")
e_tag.addCut("electron_pt", 160)
e_tag.addCut("electron_d0", 2, abs_val=True)
e_probe = Selection("probe")
e_probe.addCut("electron_pt", 50)
#e_probe.addCut("electron_isGoodQual")

#signal_sel = "electron_n_signal<2 && muon_n_basline==0 && electron_n_baseline==2 && electron_pt[0]>60 && electron_ && electron_isGoodQual[1]"
#sel_all = "%s && ( ( %s && %s ) || (%s && %s) )"%(event_sel, e_tag.getCutString(0), e_probe.getCutString(1), e_tag.getCutString(1), e_probe.getCutString(0))
sel_all = "%s && %s && %s "%(event_sel, e_tag.getCutString(0), e_probe.getCutString(1))
sel_anti = "electron_FCTightTTVA[1]==0 && %s"%sel_all
sel_all_65 = "%s && %s && %s && electron_pt[1]>65"%(event_sel, e_tag.getCutString(0), e_probe.getCutString(1))
sel_anti_65 = "electron_FCTightTTVA[1]==0 && %s"%sel_all_65
sel_anti_sig = "electron_isGoodQual[1] && %s"%sel_anti_65

h_anti = getHist(t, "h_d0_anti", "abs(electron_d0[1])", "20,0,20",  sel_anti)
h_anti_65 = getHist(t, "h_d0_anti_65", "abs(electron_d0[1])", "20,0,20", sel_anti_65)
h_anti_chi2 = getHist(t, "h_d0_anti_chi2", "abs(electron_d0[1])", "20,0,20", "electron_chi2[1] < 2 && " + sel_anti_65)
h_anti_nmiss = getHist(t, "h_d0_anti_nmiss", "abs(electron_d0[1])", "20,0,20", "electron_chi2[1] < 2 && electron_nMissingLayers[1] < 2 && " + sel_anti_65)
h_anti_sig = getHist(t, "h_d0_anti_sig", "abs(electron_d0[1])", "20,0,20", sel_anti_sig)

can = ROOT.TCanvas("c", "c")
h_anti.SetLineColor(theme_colors[0])
h_anti_65.SetLineColor(theme_colors[1])
h_anti_chi2.SetLineColor(theme_colors[2])
h_anti_nmiss.SetLineColor(theme_colors[3])
h_anti_sig.SetLineColor(theme_colors[4])
h_anti.SetMarkerColor(theme_colors[0])
h_anti_65.SetMarkerColor(theme_colors[1])
h_anti_chi2.SetMarkerColor(theme_colors[2])
h_anti_nmiss.SetMarkerColor(theme_colors[3])
h_anti_sig.SetMarkerColor(theme_colors[4])
h_anti.GetXaxis().SetTitle("electron |d_{0}| [mm]")
h_anti.GetYaxis().SetTitle("Number of Electrons")
h_anti.SetMinimum(0.1)
h_anti.SetMaximum(1e6)
h_anti.SetTitle("p_{T} > 50 GeV")
h_anti_65.SetTitle("p_{T} > 65 GeV")
h_anti_chi2.SetTitle("p_{T} > 65 GeV && chi2")
h_anti_nmiss.SetTitle("p_{T} > 65 GeV && chi2 && nmiss")
h_anti_sig.SetTitle("p_{T} > 65 GeV && chi2 && nmiss && dpt/pt")
can.SetLogy()

print "Baseline:", h_anti.Integral(3, h_anti.GetNbinsX()+1)
print "Signal:", h_anti_sig.Integral(4, h_anti.GetNbinsX()+1)

h_anti.Draw("hist e")
h_anti_65.Draw("hist e same")
h_anti_chi2.Draw("hist e same")
h_anti_nmiss.Draw("hist e same")
h_anti_sig.Draw("hist e same")

leg = can.BuildLegend(.45,.75,.8,.90)
ROOT.ATLASLabel(0.2,0.85, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
text.DrawLatex(0.2,0.8, "Anti-Isolated Electrons")
can.SaveAs("data_e_anti.pdf")
raw_input("...")
