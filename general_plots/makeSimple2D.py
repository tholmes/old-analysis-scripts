import ROOT

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/bphys.root"
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/ztautau/*70_140*/*.root"
t = getTree(d)
print getNEvents(d)
#signal_sel = "muon_isNotCosmic && muon_isGoodQual && muon_isPassIDtrk && muon_isIsolated"
#signal_sel = "muon_CBtrack_chi2 < 3 && muon_IDtrack_chi2 < 2 && muon_nPres > 2 && muon_pt > 25"
signal_sel = "muon_pt > 25"

h = getHist(t, "h_pt_vs_d0", "muon_pt:abs(muon_IDtrack_d0)", "40,0,5,40,0,250", signal_sel)
#h.Scale(1./h.Integral(0,h.GetNbinsX()+1,0,h.GetNbinsY()+1))
#h.SetMinimum(1e-5)
h.SetMaximum(1e4)
h.SetMinimum(1)
h.GetYaxis().SetTitle("muon p_{T} [GeV]")
h.GetXaxis().SetTitle("muon |d_{0}| [mm]")
can = ROOT.TCanvas("c", "c")
h.Draw("colz")
can.SetLogz()

ROOT.ATLASLabel(0.2,0.85, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
#text.DrawLatex(0.2,0.80, "bb#mu#mu MC, baseline muons")
#can.SaveAs("bbmm_pt_v_d0_baseline.pdf")
#text.DrawLatex(0.2,0.80, "bb#mu#mu MC, signal muons")
#can.SaveAs("bbmm_pt_v_d0_signal.pdf")
text.DrawLatex(0.2,0.80, "Z#tau#tau MC, baseline muons")
can.SaveAs("ztt_pt_v_d0_baseline.pdf")
#text.DrawLatex(0.2,0.80, "Z#tau#tau MC, modified signal muons")
#can.SaveAs("ztt_pt_v_d0_signal.pdf")
