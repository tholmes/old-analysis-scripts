import ROOT 
import os

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()

f = ROOT.TFile("outputFiles/1muwider_tag.root", "UPDATE")

doSR = True
eta_bound = 0.015

outDir = "estimate_wider_tag/"

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()

def do_estimate(hist, iteration):
    
    ## nevents with x muons in ms acceptance                      n muon with id track in acceptance 
    ##                                                      =   
    ## nevents with x muons not in ms acceptance (brem)           n muons with id track not in acceptance

    bound = hist.FindBin(eta_bound)

    id_acc = hist.Integral(0,bound)
    id_rej = hist.Integral(bound+1,100)

    ## from cosmics/2cos.py
    n_cos_events = -1
    if doSR:
        #n_cos_events = 21089
        n_cos_events = 21118
    else: 
        #n_cos_events = 124626
        n_cos_events = 125256

    estimate = n_cos_events * (id_rej/id_acc)
    
    print "*"*8 + iteration + "*"*8
    print "cosmic events", n_cos_events
    print "inside id accept", id_acc
    print "outside id accept", id_rej
    print
    print "estimate", estimate
    print "*"*(16+len(iteration))
    print



def subtract_linear_tail(histo, savestring):
    hist = histo.Clone()
    
    fit = ROOT.TF1("pol1","pol1",0.1,0.2) 
    hist.Fit("pol1","RQ") #line 
    p0 = fit.GetParameter(0)
    p1 = fit.GetParameter(1)

    to_subtract = ROOT.TH1F(savestring,savestring,100,0,0.2)
    for i in xrange(0,100):
        x = to_subtract.GetBinLowEdge(i)
        val = p0 + p1*x
        to_subtract.SetBinContent(i,val)
    
    hist_sub = hist.Clone()
    hist_sub.Add(to_subtract,-1)

    
    if savestring != "":
        c.Clear()
        hist_sub.SetLineColor(ROOT.kRed)
        to_subtract.SetLineStyle(9)
        to_subtract.SetLineColor(ROOT.kBlue)
        hist.SetMinimum(0.1)
        
        hist.Draw()
        hist_sub.Draw("same")
        to_subtract.Draw("same")
        
        leg = ROOT.TLegend(0.65,0.8,0.8,0.9)
        leg.AddEntry(hist,"original","l")
        leg.AddEntry(hist_sub,"final","l")
        leg.AddEntry(to_subtract,"subtracted","l")
        leg.Draw("same")
                
        c.Modified()
        c.Update()
        c.SaveAs("outputPlots/"+outDir+"/"+savestring+".pdf")

    return hist_sub 

######################
## do baseline estimate
######################
h_sum = ROOT.TH1F("sum","sum",100,0,.2)
h_sum.Add(f.Get("data_segments_cos_dEta_min"))
h_sum.Add(f.Get("data_segments_mu_dEta_min"))
do_estimate(h_sum,"BASELINE")

c.Clear()
h_sum.Draw()
c.Modified()
c.Update()
c.SaveAs("outputPlots/"+outDir+"/baseline.pdf")

######################
## do baseline estimate with phi cut
######################
h_sum_p = ROOT.TH1F("sum_p","sum_p",100,0,.2)
h_sum_p.Add(f.Get("data_segments_cos_dEta_min_dphi_lp02"))
h_sum_p.Add(f.Get("data_segments_mu_dEta_min_dphi_lp02"))
do_estimate(h_sum_p,"PHI CUT")

c.Clear()
h_sum_p.Draw()
c.Modified()
c.Update()
c.SaveAs("outputPlots/"+outDir+"/baseline_phicut.pdf")

#######################
##Fit tail of random association 
######################
h_sum_sub = subtract_linear_tail(h_sum, "subtraction") 
do_estimate(h_sum_sub,"SUBTRACTION")

h_sum_sub_cos = subtract_linear_tail(f.Get("data_segments_cos_dEta_min"), "subtraction_cosmiconly") 
do_estimate(h_sum_sub_cos,"SUBTRACTION COSMIC ONLY")

#######################
##Fit tail of random association with phi cut 
######################
h_sum_sub_p = subtract_linear_tail(h_sum_p, "subtraction_phicut") 
do_estimate(h_sum_sub_p,"SUBTRACTION PHI CUT")

h_sum_sub_cos_p = subtract_linear_tail(f.Get("data_segments_cos_dEta_min_dphi_lp02"), "subtraction_cosmiconly_phicut") 
do_estimate(h_sum_sub_cos_p,"SUBTRACTION PHI CUT COSMIC ONLY")

