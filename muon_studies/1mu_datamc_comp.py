import ROOT
import math
import glob
import os
import sys
import numpy as np

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#feature = "baseline"
append = "author1"
feature = "v3.5"

names = { 
    "pt": { "nb": 50, "low": 0, "high": 1000, "label": "p_{T, lead}", "log": 1},   
    "d0": { "nb": 60, "low": -500, "high": 500, "label": "d_{0, lead}", "log": 1},   
    "z0": { "nb": 50, "low": -600, "high": 600, "label": "z_{0, lead}", "log": 1},   
    "eta": { "nb": 50, "low": -5, "high": 5, "label": "#eta_{lead}", "log": 1},   
    "phi": { "nb": 50, "low": -4, "high": 4, "label": "#phi_{lead}", "log": 1},   
    "qoverpsig": { "nb": 50, "low": 0, "high": 10, "label": "Q/P sig_{lead}", "log": 1},   
    "topoetcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "topoetcone20/pt", "log": 1},   
    "ptcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptcone20/pt", "log": 1},   
    "ptvarcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptvarcone20/pt", "log": 1},   
    "topoetcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "topoetcone20/pt", "log": 1},   
    "ptcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptcone20/pt", "log": 1},   
    "ptvarcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptvarcone20/pt", "log": 1},   
    "RFirstHit": { "nb": 100, "low": 0, "high": 600, "label": "R_{first hit}", "log": 1},   
    "author": { "nb": 10, "low": 0, "high": 10, "label": "author", "log": 1},   
    "nPresLays": { "nb": 10, "low": 0, "high": 10, "label": "N_{Precision Layers}", "log": 0},   
    "nPresLays_good": { "nb": 10, "low": 0, "high": 10, "label": "N_{Good Precision Layers}", "log": 0},   
    "nSi": { "nb": 25, "low": 0, "high": 25, "label": "N_{Si Hits}", "log": 1},   
    "nPix": { "nb": 25, "low": 0, "high": 25, "label": "N_{Pixel Hits}", "log": 1},   
    "nSCT": { "nb": 25, "low": 0, "high": 25, "label": "N_{SCT Hits}", "log": 1},   
    "nTRT": { "nb": 50, "low": 0, "high": 60, "label": "N_{TRT Hits}", "log": 1},   
    "CB_chi2": { "nb": 50, "low": 0, "high": 10, "label": "#chi^{2}_{lead}", "log": 1},   
    "ID_chi2": { "nb": 50, "low": 0, "high": 10, "label": "#chi^{2}_{lead}", "log": 1},   
    "ID_holes": { "nb": 10, "low": 0, "high": 10, "label": "ID track holes", "log": 1},   
    "t_avg": { "nb": 50, "low": -30, "high": 30, "label": "t_{0, avg}", "log": 1},   
    "t_avg_zoomout": { "nb": 100, "low": -200, "high": 200, "label": "t_{0, avg}", "log": 1},   
    "t_med": { "nb": 50, "low": -30, "high": 30, "label": "t_{0, median}", "log": 1},   
    "t_med_zoomout": { "nb": 100, "low": -200, "high": 200, "label": "t_{0, median}", "log": 1},   
    "nMSSeg": { "nb": 10, "low": 0, "high": 10, "label": "N_{MS Segments}", "log": 1},   
    "nPhiLays": { "nb": 10, "low": 0, "high": 10, "label": "N_{#phi Layers}", "log": 1},   
    "nPresHits": { "nb": 20, "low": 0, "high": 20, "label": "N_{Pres Hits}", "log": 1},   
    "nPresHoleLays": { "nb": 10, "low": 0, "high": 10, "label": "N_{Pres Hole Layers}", "log": 1},   
    "dPhi_int": { "nb": 50, "low": -0.1, "high": .1, "label": "#Delta #phi (muon, muon track)", "log": 1},   
    "dEta_int": { "nb": 50, "low": -0.1, "high": .1, "label": "#Delta #eta (muon, muon track)", "log": 1},   
                

}
names_2d ={
    "nSi_RFirstHit"       : { "nb_y": 24, "low_y": 0, "high_y": 25, "label_y": "nSilicon hits", "nb_x": 100, "low_x": 0, "high_x": 600, "label_x": "R_{first hit}", "log":1},
    "ptvarcone20_d0"       : { "nb_x": 50, "low_x": -0.1, "high_x": 0.7, "label_x": "ptvarcone20/pt", "nb_y": 50, "low_y": -300, "high_y": 300, "label_y": "d_{0}", "log":1},
    "chi2_phi"       : { "nb_y": 150, "low_y": 0, "high_y": 60, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -4, "high_x": 4, "label_x": "#phi", "log":0},
    "eta_phi_baseline"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_cosmictag"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_cosmictag_mm"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},

    
}
h = {}
h2 = {}

#tags = ["300_slep","300_stau","100_slep","100_stau"]
tags = ["data","500_slep", "500_stau","300_slep","300_stau","100_slep","100_stau"]
#tags = ["data_alltracks","500_slep_alltracks", "300_slep_alltracks"]
#tags = ["data_msseg","500_slep_msseg", "300_slep_msseg"]
#tags = ["data_msseg","300_slep_msseg", "ttbar_msseg"]

colors = [0, ROOT.kBlue-5, ROOT.kBlue-7, ROOT.kCyan+1, ROOT.kCyan+3, ROOT.kBlue+1, ROOT.kBlue+3]
#tags = {"300_slep","300_stau","100_slep","100_stau","data"}
#tag = "300_slep"
#tag = "300_stau"
#tag = "100_slep"
#tag = "100_stau"
#tag = "500_slep"
#tag = "500_stau"

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

dirname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/"
vers = "v4_Feb27"
for tag in tags:
    if tag == "300_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399039

    if tag == "300_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399014

    if tag == "100_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399030

    if tag == "100_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399007

    if tag == "500_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399048

    if tag == "500_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399023

    if tag == "data":
        #basename= dirname + 'data_wip/Data/*'
        #basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/*.root'
        #basename= '/eos/user/t/tholmes/llp/v3.5_Jan2020_DVs/Data/*.root'
        basename= '/afs/cern.ch/user/e/eressegu/public/ntuples/data*.root'

    print basename

    AODFiles = glob.glob(basename)
    t = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t.Add(filename)


    h[tag] = {}
    h2[tag] = {}
    h[tag] = setHistos(tag, names)
    h2[tag] = setHistos2D(tag, names_2d)

    print tag
    print t.GetEntries()
    nevents = t.GetEntries()

    lumi = 139
   # lumi = 3.2 #2015
    weight = 1
    if "data" not in tag:     
        xs = getXS(dsid)
        weight = xs*lumi / nevents

    eventNum = 0
    nPs = 0
    for event in t:
        if eventNum % 10000 == 0: print "Processing event ", eventNum
        eventNum+=1
        iLead = -1
        
    #    if eventNum > 20000: break
        
        try:
            event.muon_pt
    #        event.muon_msSegment_t0
        except:
            continue
        if not event.muon_pt: continue
        
        if not len(event.lepton_pt) == 1: continue
        if not len(event.muon_pt) == 1: continue
        
         
        if not event.pass_HLT_mu60_0eta105_msonly: continue
        
        if not preselect_muon(event, 0): continue
        
        if not event.muon_author[0] == 1: continue

        h2[tag]["eta_phi_baseline"].Fill(event.muon_phi[0], event.muon_eta[0]) 
        
        cosmic = not event.muon_isNotCosmic[0]
        if cosmic: continue 
        
        #cosmicTag,ms = cosTag(event, 0, .018 , .25) 
        #matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap) 
        #cosmic = False
        #if cosmicTag or matVeto: cosmic = True
         
        
         
        #non-cosmic tagged information
        #if not cosmicTag :
        #   h2[tag]["eta_phi_cosmictag"].Fill(event.muon_phi[0], event.muon_eta[0]) 
        #if not matVeto: 
        if not cosmic: 
            h2[tag]["eta_phi_cosmictag_mm"].Fill(event.muon_phi[0], event.muon_eta[0]) 
                     
            v1 = ROOT.TVector3()
            v1.SetPtEtaPhi(event.muon_pt[0], event.muon_eta[0], event.muon_phi[0])
            
            nPhiLays = 0
            nPresHits = 0
            t0s = []
            t0_med = 0
            t0_avg = 0

            try:
                for ims in xrange(0,event.muon_nMSSegments[0]): #this works bc there's only one muon
                    nPhiLays += event.muon_msSegment_nPhiLays[ims]
                    nPresHits += event.muon_msSegment_nPresHits[ims] 
                    t0s.append(event.muon_msSegment_t0[ims])
            except:
                print "what!!"
                continue
            
            t0_med = np.median(np.array(t0s))   
            t0_avg = t_avg(event, 0)
            
            if len(t0s) == 0: print t0_med
            if abs(t0_med) > 100 : print t0s 

            h[tag]["pt"].Fill(event.muon_pt[0], weight)
            h[tag]["d0"].Fill(event.muon_IDtrack_d0[0], weight)
            h[tag]["z0"].Fill(event.muon_IDtrack_z0[0], weight)
            h[tag]["eta"].Fill(event.muon_eta[0], weight)
            h[tag]["phi"].Fill(event.muon_phi[0], weight)
            h[tag]["qoverpsig"].Fill(event.muon_QoverPsignif[0], weight)
            h[tag]["author"].Fill(event.muon_author[0], weight)
            h[tag]["RFirstHit"].Fill(event.muon_IDtrack_RFirstHit[0], weight)
            h[tag]["nSi"].Fill(event.muon_IDtrack_nSi[0], weight)
            h[tag]["nTRT"].Fill(event.muon_IDtrack_nTRT[0], weight)
            h[tag]["nPix"].Fill(event.muon_IDtrack_nPIX[0], weight)
            h[tag]["nSCT"].Fill(event.muon_IDtrack_nSCT[0], weight)
            h[tag]["nPresLays"].Fill(event.muon_MStrack_nPres[0], weight)
            h[tag]["nPresLays_good"].Fill(event.muon_MStrack_nPresGood[0], weight)
            h[tag]["nPresHoleLays"].Fill(event.muon_MStrack_nPresHole[0], weight)
            h[tag]["CB_chi2"].Fill(event.muon_CBtrack_chi2[0], weight)
            h[tag]["ID_chi2"].Fill(event.muon_IDtrack_chi2[0], weight)
            h[tag]["ID_holes"].Fill(event.muon_IDtrack_nMissingLayers[0], weight)
            h[tag]["t_avg"].Fill(t0_avg, weight)
            h[tag]["t_avg_zoomout"].Fill(t0_avg, weight)
            h[tag]["t_med"].Fill(t0_med, weight)
            h[tag]["t_med_zoomout"].Fill(t0_med, weight)
            h[tag]["nMSSeg"].Fill(event.muon_nMSSegments[0], weight)
            h[tag]["nPhiLays"].Fill(nPhiLays, weight)
            h[tag]["nPresHits"].Fill(nPresHits, weight)
            h[tag]["dPhi_int"].Fill(event.muon_phi[0] - event.muon_IDtrack_phi[0], weight)
            h[tag]["dEta_int"].Fill(event.muon_eta[0] - event.muon_IDtrack_eta[0], weight)
            h[tag]["topoetcone20"].Fill(event.muon_topoetcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptvarcone20"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptcone20"].Fill(event.muon_ptcone20[0]/event.muon_pt[0], weight)
            h[tag]["topoetcone20_zoom"].Fill(event.muon_topoetcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptvarcone20_zoom"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptcone20_zoom"].Fill(event.muon_ptcone20[0]/event.muon_pt[0], weight)
            h2[tag]["chi2_phi"].Fill(event.muon_CBtrack_chi2[0], event.muon_phi[0], weight)
            h2[tag]["nSi_RFirstHit"].Fill(event.muon_IDtrack_RFirstHit[0], event.muon_IDtrack_nSi[0], weight)
            h2[tag]["ptvarcone20_d0"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], event.muon_IDtrack_d0[0], weight)
                
                 


c = ROOT.TCanvas()



for tag in tags:

    if not os.path.exists("outputPlots/"+tag): os.makedirs("outputPlots/"+tag)

    outFile = ROOT.TFile("outputFiles/" +tag+ "_histos.root", "RECREATE")


    for i, histo in enumerate(h[tag]):
        h[tag][histo] = addOverflow(h[tag][histo])
        h[tag][histo].Write()
        if names[histo]["log"] == 1:
            ROOT.gPad.SetLogy()
        else:
            ROOT.gPad.SetLogy(0)
        h[tag][histo].Draw("hist")
        c.SaveAs("outputPlots/"+ tag + "/" + tag + "_" + feature + "_1_"+ histo + "_" + append + ".pdf")
        
    

    ROOT.gPad.SetLogy(0)
    for histo in h2[tag]:
        h2[tag][histo].Write()
        if names_2d[histo]["log"] == 1:
            ROOT.gPad.SetLogz()
        else:
            ROOT.gPad.SetLogz(0)
        if histo == "cosTag_nPs": h2[histo].Draw("COLZTEXT")
        else: h2[tag][histo].Draw("COLZ")

        c.SaveAs("outputPlots/"+tag + "/" + tag + "_" + feature + "_2_"+ histo + "_" + append + ".pdf")

    outFile.Write()

if not os.path.exists("outputPlots/stack"): os.makedirs("outputPlots/stack")

ROOT.gPad.SetLogy()
for i, name in enumerate(names):
    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    for j, tag in enumerate(tags):
        leg.AddEntry(h[tag][name],tag,"p")
        print "drawing " + name + " " + tag
        if j == 0: 
            h[tag][name].SetMaximum(10**9)
            h[tag][name].SetMinimum(10**-4)
            h[tag][name].Draw("pe")
        else:
            h[tag][name].SetMarkerColor(colors[j])
            h[tag][name].Draw("pesame")
    ROOT.ATLASLabel(0.20,0.88, "Work In Progress")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.20,0.83, "Run 2 data, 139 fb^{-1}" )
    leg.Draw("same")
    ROOT.gPad.Update()
    c.Update()
    c.SaveAs("outputPlots/stack/stack_" + feature + "_" + name + "_" + append + ".pdf")


#
