import ROOT
import math
import glob
import os
import sys

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
#ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/studies/cosmics/cosmic_helpers.py")

verbose=False
#verbose=True
if verbose: ROOT.gROOT.SetBatch(0)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask")
middleMap = maps.Get("BM_mask")
outerMap = maps.Get("BO_mask")

#####
## File and trees
#####
#AODFiles = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.5_Jan2020_DVs/Data/data15_2ndcos.root") #DAOD_RPVLL
#AODFiles = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/2ndCos/*")              #SUSY15
AODFiles = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/2mu/*.root")
tree = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    tree.Add(filename)

#####
## Formatting
#####
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadBottomMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.05);
ROOT.gStyle.SetPadTopMargin(0.05);
ROOT.gStyle.SetPadTickX(1);
ROOT.gStyle.SetPadTickY(1);
ROOT.gStyle.SetTitleOffset(1.2, "xy");

#####
## Detector guide lines
####
guides = []
guides.append( ROOT.TEllipse(0.0, 0.0, 33.0, 33.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 50.5, 50.5) )
guides.append( ROOT.TEllipse(0.0, 0.0, 85.0, 85.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 125.0, 125.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 299.0, 299.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 371.0, 371.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 443.0, 443.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 514.0, 514.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 4800.0, 4800.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 6900.0, 6900.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 9700.0, 9700.0) )


for guide in guides:
    guide.SetFillStyle(0)
    guide.SetLineColor(ROOT.kGray)
    guide.SetLineStyle(1)
    guide.SetLineWidth(2)


def makeLineFromPhi(phi, startR, endR):
    x1 = startR * math.cos(phi)
    y1 = startR * math.sin(phi)
    x2 = endR * math.cos(phi)
    y2 = endR * math.sin(phi)

    line = ROOT.TGraph()
    line.SetPoint(0, x1, y1)
    line.SetPoint(1, x2, y2)

    return line

def getFirstHitXY(r, b, m, upward):

    # variables according to y = mx + b line equation
    # r = radius of first hit
    # if upward, choose the + solution to quadratic equation

    #a = 1+m**2
    #b = -2*slope
    #c = slope**2 - m**2*r**2

    mult = 1
    if not upward: mult = -1

    yfirst = ( 2*b + mult*math.sqrt((-2*b)**2 - 4*(m**2+1)*(b**2 - m**2*r**2)) ) / (2 * (m**2 + 1))
    xfirst = ( yfirst - b ) / m

    return xfirst,yfirst


for i, event in enumerate(tree):

    if not event.eventNumber == 1766857478: continue #Investigate 2-cosmic event
    #print i
    #if i>0: break
    if verbose:
        if not event.eventNumber == 1191022016: continue
    lines = []
    muon_lines = []

    # Draw center of MS chambers
    for seg_i in xrange(16):
        phi = (2*math.pi/16)*seg_i
        line = makeLineFromPhi(phi, 4000, 10000)
        line.SetLineColor(ROOT.kGray)
        line.SetLineWidth(2)
        line.SetLineStyle(2)
        muon_lines.append(line)

    cos_nphi = 0
    mu_nphi = 0

    #####
    ## Set up canvas
    #####
    scope = 10000
    csize = 1000
    #if verbose: csize = 1000
    c = ROOT.TCanvas(str(event.eventNumber), "", 0, 0, csize, csize)

    frame = c.DrawFrame( -scope, -scope, scope, scope)
    frame.SetLineStyle(0);
    frame.SetLineWidth(0);
    frame.SetMarkerStyle(0);
    frame.GetXaxis().SetTitleOffset(1.5);
    frame.GetYaxis().SetTitleOffset(1.8);
    frame.SetTitle(";x [mm];y [mm]");

    for guide in guides:
        guide.Draw("l+")

    x = event.PV_x
    y = event.PV_y

    #####
    ## Muons
    ####
    icos = -1
    imu = -1
    if abs(event.muon_eta[0]) > 1.05 or abs(event.muon_eta[1]) > 1.05:
        print "skipped ", event.eventNumber
        continue
    for im in xrange(len(event.muon_pt)):
        p = ROOT.TVector3()
        p.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])

        ct = cosTag(event, im, .012 , .18)
        mv = materialVeto(event, im, innerMap, middleMap, outerMap)

        cosmic = ct or mv

        if cosmic: icos = im
        else: imu = im

        #        |    /
        #        |   /
        # ----------/------
        #          / xint
        #         /


        upward = True
        if math.sin(event.muon_IDtrack_phi[im]) < 0: upward = False

        if verbose:
            print ""
            print "muon pt:", event.muon_pt[im]
            print "muon phi:", event.muon_IDtrack_phi[im]
            print "upward? :", upward
            print "muon d0 :", event.muon_IDtrack_d0[im]
            print "muon nPr:", event.muon_MStrack_nPres[im]
            print "muon nSe:", event.muon_nMSSegments[im]

        xint = event.muon_IDtrack_d0[im] / abs(math.sin(event.muon_IDtrack_phi[im]))
        if not upward: xint = xint * -1
        yint = -1 * event.muon_IDtrack_d0[im] / abs(math.sin(math.pi/2. - event.muon_IDtrack_phi[im]))
        if math.tan(event.muon_IDtrack_phi[im]) < 0: yint = yint * -1
        if not upward: yint = yint * -1
        slope = -yint/xint

        if verbose:
            print "muon xint:", xint
            print "muon yint:", yint
            print "muon slope:", slope

        '''
        yrange = 1000
        x1 = xint - yrange/slope
        x2 = xint + yrange/slope
        y1 = -1 * yrange
        y2 = yrange

        line = ROOT.TGraph()
        line.SetPoint(0, xint, 0)
        if upward:
            line.SetPoint(1, x2, y2)
        else:
            line.SetPoint(1, x1, y1)
        '''

        rfirst = event.muon_IDtrack_RFirstHit[im]
        xfirst,yfirst = getFirstHitXY(rfirst, yint, slope, upward)
        if verbose:
            print rfirst
            print xfirst, yfirst

        yrange = 1000
        x1 = xfirst - yrange/slope
        x2 = xfirst + yrange/slope
        y1 = yfirst - yrange
        y2 = yfirst + yrange
        line = ROOT.TGraph()
        line.SetPoint(0, xfirst, yfirst)
        if upward:
            line.SetPoint(1, x2, y2)
        else:
            line.SetPoint(1, x1, y1)

        ## Also make CB track line
        upward = True
        if math.sin(event.muon_phi[im]) < 0: upward = False

        xint = event.muon_CBtrack_d0[im] / abs(math.sin(event.muon_phi[im]))
        if not upward: xint = xint * -1
        yint = -1 * event.muon_CBtrack_d0[im] / abs(math.sin(math.pi/2. - event.muon_phi[im]))
        if math.tan(event.muon_phi[im]) < 0: yint = yint * -1
        if not upward: yint = yint * -1
        slope = -yint/xint

        #rfirst = event.muon_IDtrack_RFirstHit[im]
        xfirst,yfirst = getFirstHitXY(rfirst, yint, slope, upward)
        if verbose:
            print rfirst
            print xfirst, yfirst

        #xfirst = rfirst * math.cos(event.muon_phi[im])
        #yfirst = rfirst * math.sin(event.muon_phi[im])

        xfirst = xint
        yfirst = 0

        yrange = 10000
        x1 = xfirst - yrange/slope
        x2 = xfirst + yrange/slope
        y1 = yfirst - yrange
        y2 = yfirst + yrange
        line1 = ROOT.TGraph()
        line1.SetLineStyle(3)
        line1.SetPoint(0, xfirst, yfirst)
        if upward:
            line1.SetPoint(1, x2, y2)
        else:
            line1.SetPoint(1, x1, y1)

        ## Also make MS track line
        ms_phi = event.muon_MStrack_phi[im]
        startR = 4000
        endR = 10000

        line2 = makeLineFromPhi(ms_phi, startR, endR)
        line_ms_up = makeLineFromPhi(ms_phi+0.2, startR, endR)
        line_ms_down = makeLineFromPhi(ms_phi-0.2, startR, endR)
        line_ms_up.SetLineStyle(2)
        line_ms_down.SetLineStyle(2)
        line_ms_up.SetLineColor(ROOT.kGray)
        line_ms_down.SetLineColor(ROOT.kGray)

        if cosmic:
            if verbose: print "Red (cosmic)."
            line.SetLineColor(ROOT.kRed)
            line1.SetLineColor(ROOT.kRed)
            line2.SetLineColor(ROOT.kRed)
        else:
            if verbose: print "Dark red (not cosmic)."
            line.SetLineColor(ROOT.kRed+2)
            line1.SetLineColor(ROOT.kRed+2)
            line2.SetLineColor(ROOT.kRed+2)
        line.SetLineWidth(2)
        line2.SetLineWidth(2)
        #muon_lines.append(line_ms_up)
        #muon_lines.append(line_ms_down)
        muon_lines.append(line)
        muon_lines.append(line1)
        muon_lines.append(line2)

    #####
    ## Other MS Segments
    ####
    for ms in xrange(len(event.msSegment_x)):
        segment = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])


        skip = False
        for ms2 in xrange(len(event.muon_msSegment_x)):
            segment2 = ROOT.TVector3(event.muon_msSegment_x[ms2], event.muon_msSegment_y[ms2], event.muon_msSegment_z[ms2])
            if abs(segment.DeltaR(segment2)) == 0: skip = True

        if skip: continue

        R = math.sqrt(event.msSegment_x[ms]**2 + event.msSegment_y[ms]**2)

        line = ROOT.TGraph()
        xpos = event.msSegment_x[ms]
        ypos = event.msSegment_y[ms]

        line.SetPoint(0, xpos, ypos)
        #line.SetPoint(1, (R+300)*xpos, (R+300)*ypos)

        line.SetLineColor(ROOT.kBlue)
        line.SetMarkerColor(ROOT.kBlue)
        line.SetLineWidth(1)

        if abs(event.msSegment_z[ms]) > 6000: line.SetMarkerStyle(24)
        else: line.SetMarkerStyle(8)

        #line.SetMarkerSize(15)
        line.SetMarkerSize(3)


        lines.append(line)

    #####
    ## MS segments attached to muons
    ####
    for ms in xrange(len(event.muon_msSegment_x)):
        segment = ROOT.TVector3(event.muon_msSegment_x[ms], event.muon_msSegment_y[ms], event.muon_msSegment_z[ms])

        R = math.sqrt(event.muon_msSegment_x[ms]**2 + event.muon_msSegment_y[ms]**2)

        line = ROOT.TGraph()
        xpos = event.muon_msSegment_x[ms]
        ypos = event.muon_msSegment_y[ms]
        line.SetPoint(0, xpos, ypos)
        #line.SetPoint(1, xpos + 500*math.cos(segment.Phi()), ypos + 500*math.sin(segment.Phi()))

        if verbose:
            print "segment x", xpos
            print "segment y", ypos

        if event.muon_msSegment_muonIndex[ms] == icos:
            if verbose: print "Red (cosmic)."
            line.SetLineColor(ROOT.kRed)
            line.SetMarkerColor(ROOT.kRed)
            cos_nphi += event.muon_msSegment_nPhiLays[ms]
        elif event.muon_msSegment_muonIndex[ms] == imu:
            if verbose: print "Dark red (not cosmic)."
            line.SetLineColor(ROOT.kRed+2)
            line.SetMarkerColor(ROOT.kRed+2)
            mu_nphi += event.muon_msSegment_nPhiLays[ms]
        line.SetLineWidth(3)

        if abs(event.muon_msSegment_z[ms]) > 6000:
            line.SetMarkerStyle(28)
            if event.muon_msSegment_muonIndex[ms] == icos:
                print "endcap ms segment!"
        else: line.SetMarkerStyle(34)

        #line.SetMarkerSize(15)
        line.SetMarkerSize(3)

        lines.append(line)

    #####
    ## Draw Muons
    ####
    for line in muon_lines:
        line.Draw("l+")
    for line in lines:
        line.Draw("p+")

    #####
    ## Draw PV
    #####
    pv = ROOT.TEllipse(x, y, x+5, y+5)
    pv.SetFillStyle(1001)
    pv.SetFillColor(ROOT.kBlack)
    pv.SetLineColor(ROOT.kBlack)
    pv.Draw("+")

    #####
    ## Text
    #####
    latex = ROOT.TLatex()
    latex.SetTextAlign(12)
    latex.SetTextFont(42)
    latex.SetTextSize(0.02)
    latex.SetTextColor(ROOT.kRed)
    latex.DrawLatexNDC(0.18, 0.91, "cosmic p_{T} %3.3f"%(event.muon_pt[icos]) )
    latex.DrawLatexNDC(0.18, 0.87, "cosmic #phi layers %d"%(cos_nphi) )
    latex.SetTextColor(ROOT.kRed-2)
    latex.DrawLatexNDC(0.18, 0.83, "untagged p_{T} %3.3f"%(event.muon_pt[imu]) )
    latex.DrawLatexNDC(0.18, 0.79, "untagged #phi layers %d"%(mu_nphi) )
    latex.SetTextColor(ROOT.kBlack)
    latex.DrawLatexNDC(0.18, 0.75, "Event number " + str(event.eventNumber ) )


    ROOT.gPad.Modified()
    ROOT.gPad.Update()
    if verbose: raw_input("...")

    ROOT.gPad.SaveAs("outputPlots/susy15_"+ str(event.eventNumber) + ".pdf")
