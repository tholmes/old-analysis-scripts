This repo contains a bunch of little scripts that mostly don't depend on other things.

To run anything running over an aod, first do:

`asetup AthAnalysisBase,2.4.39,here`

Here are the scripts used to make the aux figures (excluding limits) and cutflow tables:
- Figure 1: workingpoints/egammaStudies_modern.py then workingpoints/makeEfficiencyPlots_2pads.py
- Figure 4: cosmics/2cos.py then cosmics/paperplots.py
- Figure 5: cosmics/2cos.py then cosmics/paperplots.py
- Figures 15-20: acceptance/aux_acceptance_final.py
- Figure 21: acceptance/aux_ptd0.py
- Figure 22: general_plots/signald0pt.py
- Tables 10-12: acceptance/cutflow.py

Truth selection script for hep data: acceptance/displacedLeptons_acceptance.py
