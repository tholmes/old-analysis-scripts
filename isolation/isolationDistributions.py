# Goal: Study whether or not using objects in the photon container is useful

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000

# File type
f_search = "SlepSlep"
append = "_slep"

# Define samples
fnames = {
        "Slep_300_0p1":     "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/*399039*/*.root",
        "Photon_70_140":    "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/*361044*/*.root",
        "Photon_280_500":    "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/*361050*/*.root",
        "Stau_100_0p1":     "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/*399006*/*.root",
        "ttbar":            "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/*DAOD_RPVLL_mc16a_031319*/*.root",
        }

# Variables
variables = {
        "l_pt":                   {"nbins": 40,    "xmin": 0.,    "xmax": 500.,   "xlabel": "Lepton p_{T} [GeV]",  "logy": False},
        "l_d0":                   {"nbins": 40,    "xmin": -20.,  "xmax": 20.,    "xlabel": "Lepton d_{0} [mm]",   "logy": True},
        "l_eta":                  {"nbins": 40,    "xmin": -3.,   "xmax": 3.,     "xlabel": "Lepton #eta",         "logy": False},
        "l_phi":                  {"nbins": 40,    "xmin": -3.5,  "xmax": 3.5,    "xlabel": "Lepton #phi",         "logy": False},
        "l_ptvarcone20":          {"nbins": 20,    "xmin": 0.,    "xmax": 10.,    "xlabel": "Lepton ptvarcone20",  "logy": True},
        "l_FCLoose":          {"nbins": 20,    "xmin": 0.,    "xmax": 10.,    "xlabel": "Lepton ptvarcone20",  "logy": True},
        }

# Define selections -- looking at individual electrons/muons
translate = {11: "electron", 13: "muon"}
selections = {11: Selection("electron sel"), 13: Selection("muon sel")}
for sel in selections:
    lep_str = translate[sel]
    selections[sel].addCut("%s_pt"%lep_str, 65)
    if sel==13:
        selections[sel].addCut("muon_IDtrack_d0", 1, abs_val=True)
    else: selections[sel].addCut("%s_d0"%lep_str, 1, abs_val=True)

# Loop over samples
samples = {}
cans = {}
for fname in fnames:
    samples[fname] = {}
    for flavor in [11, 13]:
        cans[flavor] = {}
        samples[fname][flavor] = {"hists": {}, "thists": {}, "mods": {}}

        # Load dataframe
        files = glob.glob(fnames[fname])
        #print files
        print "Found", len(files), "files."
        tree = getTree(fnames[fname])
        #dframe = ROOT.ROOT.RDataFrame(tree)

        # Get crossection
        xs = getXS(files[0].split("/")[-2].split(".")[3])
        n_events = getNEvents(fnames[fname])
        print "Cross section:", xs, "N events:", n_events
        ev_weight = lumi*xs/n_events
        #print "Found", dframe.Count().GetValue(), "events in loaded file."

        # Apply selection to variables
        lep_str = translate[flavor]
        cut_str = selections[flavor].getCutString()
        weight_str = "%f * (%s)"%(ev_weight, cut_str)

        #sel_pt = selections[flavor].selectedVectorString("%s_pt"%lep_str)
        #df_sel = dframe.Define("l_pt", sel_pt)

        # Plot
        #df = df_sel
        for v in variables:
            value_name = v.replace("l_", lep_str+"_")
            if value_name == "muon_d0": value_name = "muon_IDtrack_d0"
            hname = "h_%s_%d_%s"%(fname, flavor, v)
            binstr = "%d,%f,%f"%(variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
            samples[fname][flavor]["hists"][v] = getHist(tree, hname, value_name, binstr, weight_str, verbose=False)
            samples[fname][flavor]["hists"][v].SetTitle(hname)

        #    samples[fname][flavor]["mods"][v] = ROOT.RDF.TH1DModel("m_%s_%d_%s"%(fname, flavor, v), "true", variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
        #    samples[fname][flavor]["thists"][v] = df.Histo1D(samples[fname][flavor]["mods"][v], v)
        #    samples[fname][flavor]["hists"][v] = samples[fname][flavor]["thists"][v].Clone("h_%s_%d_%s"%(fname, flavor, v))


        # Cutflow for fun
        #report = dframe.Report()
        #report.Print()

# Draw plots
for flavor in [11, 13]:
    for v in variables:

        print "Plotting", v, "for flavor", flavor

        can_name = "c_%d_%s"%(flavor, v)
        cans[flavor][v] = ROOT.TCanvas(can_name, can_name)

        for i, fname in enumerate(samples):

            n_leps = samples[fname][flavor]["hists"][v].Integral()
            print "\tIn sample", fname, "found", n_leps, "events."
            if n_leps>0: samples[fname][flavor]["hists"][v].Scale(1./n_leps)

            if i==0:
                if variables[v]["logy"]:
                    samples[fname][flavor]["hists"][v].SetMinimum(0.00001)
                    samples[fname][flavor]["hists"][v].SetMaximum(100000)
                else:
                    samples[fname][flavor]["hists"][v].SetMinimum(0)
                    samples[fname][flavor]["hists"][v].SetMaximum(0.5)
                samples[fname][flavor]["hists"][v].GetXaxis().SetTitle(variables[v]["xlabel"].replace("Lepton", translate[flavor]))
                samples[fname][flavor]["hists"][v].GetYaxis().SetTitle("fraction of %ss"%translate[flavor])
                samples[fname][flavor]["hists"][v].Draw("hist PLC PMC")
            else:
                samples[fname][flavor]["hists"][v].Draw("same hist PLC PMC")

        if variables[v]["logy"]: cans[flavor][v].SetLogy()

        leg = cans[flavor][v].BuildLegend(.5,.55,.8,.90)
        leg.Draw()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.78, "Simulation, 140 fb^{-1}")

        cans[flavor][v].Update()
        raw_input("...")

        cans[flavor][v].SaveAs(can_name+append+".pdf")


