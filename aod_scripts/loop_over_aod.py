from math import *
import sys
import os
import glob
import json
import ROOT
import urllib2
from ROOT.POOL import TEvent

colors = {"e": ROOT.kRed, "m": ROOT.kBlue, "t": ROOT.kGreen}
colors = {
        "m": ROOT.TColor.GetColor('#003fff'),
        "t": ROOT.TColor.GetColor('#03ed3a'),
        "e": ROOT.TColor.GetColor('#e8000b'),
        }

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.SetAtlasStyle()

aod_file = "/afs/cern.ch/user/e/ekuwertz/public/forTova/TRUTH1.pool.root"
#aod_file = "/eos/user/l/lhoryn/inputFiles/data15_13TeV.00280368.physics_Main.deriv.DAOD_SUSY15.r9264_r10573_p3578_p4041/DAOD_SUSY15.20117417._000003.pool.root.1"

evt = TEvent(TEvent.kClassAccess)
chain = ROOT.TChain("CollectionTree")
chain.Add(aod_file)
evt.readFrom(chain)
nevents = evt.getEntries()
print 'Done with filesetup, have {0} events.'.format(nevents)

variables = {
        "pt0": {"xmin": 0., "xmax": 1400., "nbins": 7, "xlab": "leading p_{T} [GeV]"},
        "pt1": {"xmin": 0., "xmax": 1400., "nbins": 7, "xlab": "subleading p_{T} [GeV]"},
        "eta0": {"xmin": -4., "xmax": 4., "nbins": 8, "xlab": "leading #eta"},
        "eta1": {"xmin": -4., "xmax": 4., "nbins": 8, "xlab": "subleading #eta"},
        "r0": {"xmin": 0., "xmax": 10., "nbins": 20, "xlab": "leading R [mm]"},
        "r1": {"xmin": 0., "xmax": 10., "nbins": 20, "xlab": "subleading R [mm]"},
        "d00": {"xmin": 0., "xmax": 10., "nbins": 20, "xlab": "leading d_{0} [mm]"},
        "d01": {"xmin": 0., "xmax": 10., "nbins": 20, "xlab": "subleading d_{0} [mm]"},
        }

hists = {}
for var in variables:
    hists[var] = {}
    for flav in ["e", "m", "t"]:
        hname = "h_%s_%s"%(var, flav)
        hists[var][flav] = ROOT.TH1F(hname, hname, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])

def isGood(p):
    #if p.pt()<10000: return False

    # Is the parent a slepton?
    if p.parent().absPdgId() not in [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]: return False

    return True

def getD0(p):
    slepton = ROOT.TVector3(p.prodVtx().x(), p.prodVtx().y(), p.prodVtx().z())
    lepton = ROOT.TVector3()
    lepton.SetMagThetaPhi(1., p.p4().Theta(), p.phi())
    return slepton.Perp()*sin(fabs(lepton.DeltaPhi(slepton)))

for entry in xrange(nevents):
    if entry%10==0: print "\n\tProcessing event", entry
    evt.getEntry(entry)
    particles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")

    muons = []
    els = []
    taus = []
    for p in particles:
        if abs(p.pdgId())==13:
            if isGood(p): muons.append(p)
        elif abs(p.pdgId())==11:
            if isGood(p): els.append(p)
        elif abs(p.pdgId())==15:
            if isGood(p): taus.append(p)

    if len(muons) + len(els) + len(taus) != 2: print "Warning: found", len(muons) + len(els) + len(taus), "good leptons in event", entry
    muons.sort(key=lambda x: x.pt(), reverse=True)
    els.sort(key=lambda x: x.pt(), reverse=True)
    taus.sort(key=lambda x: x.pt(), reverse=True)

    for i, p in enumerate(muons):
        if i==1:
            if p.pt() > muons[0].pt(): print "Warning: muons out of order!"
        if i>1: break
        #print "muon", i, "pt:", p.pt()*.001
        hists["pt%d"%i]["m"].Fill(p.pt()*.001)
        hists["eta%d"%i]["m"].Fill(p.eta())
        hists["r%d"%i]["m"].Fill(p.prodVtx().perp())
        hists["d0%d"%i]["m"].Fill(getD0(p))

    for i, p in enumerate(els):
        if i==1:
            if p.pt() > els[0].pt(): print "Warning: els out of order!"
        if i>1: break
        #print "electron", i, "pt:", p.pt()*.001
        hists["pt%d"%i]["e"].Fill(p.pt()*.001)
        hists["eta%d"%i]["e"].Fill(p.eta())
        hists["r%d"%i]["e"].Fill(p.prodVtx().perp())
        hists["d0%d"%i]["e"].Fill(getD0(p))

    for i, p in enumerate(taus):
        if i==1:
            if p.pt() > taus[0].pt(): print "Warning: taus out of order!"
        if i>1: break
        #print "tau", i, "pt:", p.pt()*.001
        hists["pt%d"%i]["t"].Fill(p.pt()*.001)
        hists["eta%d"%i]["t"].Fill(p.eta())
        hists["r%d"%i]["t"].Fill(p.prodVtx().perp())
        hists["d0%d"%i]["t"].Fill(getD0(p))

for var in hists:
    for flav in hists[var]:
        hists[var][flav].GetXaxis().SetTitle(variables[var]["xlab"])
        hists[var][flav].SetLineColor(colors[flav])
        hists[var][flav].SetMaximum(1.3*max([hists[var][x].GetMaximum() for x in hists[var]]))

    can = ROOT.TCanvas("can", "can")
    hists[var]["e"].Draw()
    hists[var]["m"].Draw("same")
    hists[var]["t"].Draw("same")
    leg = ROOT.TLegend(.7,.7,.9,.9)
    leg.AddEntry(hists[var]["e"], "electrons", "l")
    leg.AddEntry(hists[var]["m"], "muons", "l")
    leg.AddEntry(hists[var]["t"], "taus", "l")
    leg.Draw("same")
    #raw_input("...")
    can.SaveAs("plots/%s.pdf"%var)
