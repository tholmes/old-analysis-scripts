import ROOT
import math
import glob
import os
import sys

execfile("../../studies/cosmics/cosmic_helpers.py")				       
execfile("../plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)


tfrom = 0.1
tto = 1
append = "%f_to_%f"%(tfrom,tto)

c = ROOT.TCanvas()
#500, 0.1 ns
f_to = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_400_0_%sns.v5.7_Aug12_lifetimefix*_trees.root/*"%str(tto).replace(".","p"))
#500, 1 ns
f_from = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_400_0_%sns.v5.7_Aug12_lifetimefix_*_trees.root/*"%str(tfrom).replace(".","p"))

t_to = ROOT.TChain("trees_SR_highd0_")
for filename in f_to:
    t_to.Add(filename)

t_from = ROOT.TChain("trees_SR_highd0_")
for filename in f_from:
    t_from.Add(filename)


weight_str = "(%f/%f) * TMath::Exp( (truthSparticle_properLifetime[%i]*1000000/%f) - (truthSparticle_properLifetime[%i]*1000000/%f) )"
w0 = weight_str%(tfrom, tto, 0, tfrom, 0, tto)
w1 = weight_str%(tfrom, tto, 1, tfrom, 1, tto)
w = "(%s * %s)"%(w0,w1)


binning_tau = "20,0.,5,20,0.,5"
binning_pt = "10,0.,500.,10,0.,500."
binning_d0 = "10,0.,100.,10,0.,100."

h2 ={}

### lifetime
h2["to_tau"] = getHist(t_to, "tau_tau_to", "truthSparticle_properLifetime[0]*1000000:truthSparticle_properLifetime[1]*1000000", binning=binning_tau)
h2["from_tau"] = getHist(t_from, "tau_tau_from", "truthSparticle_properLifetime[0]*1000000:truthSparticle_properLifetime[1]*1000000", binning=binning_tau)
h2["from_tau_weighted"] = getHist(t_from, "tau_tau_from_weighted", "truthSparticle_properLifetime[0]*1000000:truthSparticle_properLifetime[1]*1000000", sel_name=w, binning=binning_tau)

h2["ratio_tau"] = h2["to_tau"].Clone("ratio_tau")
h2["ratio_tau"].Divide(h2["to_tau"], h2["from_tau_weighted"], 1, 1, "B")

h2["sig_tau"] = h2["to_tau"].Clone("sig_tau")
h2["sig_tau"].Reset()
for i in xrange(1,h2["sig_tau"].GetNbinsX()+1):
    for j in xrange(1,h2["sig_tau"].GetNbinsY()+1):
        err = h2["ratio_tau"].GetBinError(i,j)
        val =  h2["ratio_tau"].GetBinContent(i,j) -1 
        try:
            fill = val/err
        except:
            fill = 0
        h2["sig_tau"].SetBinContent(i,j, fill)

### lifetime
h2["to_pt"] = getHist(t_to, "pt_pt_to", "truthLepton_pt[0]:truthLepton_pt[1]", binning=binning_pt)
h2["from_pt"] = getHist(t_from, "pt_pt_from", "truthLepton_pt[0]:truthLepton_pt[1]", binning=binning_pt)
h2["from_pt_weighted"] = getHist(t_from, "pt_pt_from_weighted", "truthLepton_pt[0]:truthLepton_pt[1]", sel_name=w, binning=binning_pt)
h2["ratio_pt"] = h2["to_pt"].Clone("ratio_pt")
h2["ratio_pt"].Divide(h2["to_pt"], h2["from_pt_weighted"], 1, 1, "B")
h2["sig_pt"] = h2["to_pt"].Clone("sig_pt")
h2["sig_pt"].Reset()
for i in xrange(1,h2["sig_pt"].GetNbinsX()+1):
    for j in xrange(1,h2["sig_pt"].GetNbinsY()+1):
        err = h2["ratio_pt"].GetBinError(i,j)
        val =  h2["ratio_pt"].GetBinContent(i,j) -1 
        try:
            fill = val/err
        except:
            fill = 0
        h2["sig_pt"].SetBinContent(i,j, fill)

h2["to_d0"] = getHist(t_to, "d0_d0_to", "truthLepton_d0[0]:truthLepton_d0[1]", binning=binning_d0)
h2["from_d0"] = getHist(t_from, "d0_d0_from", "truthLepton_d0[0]:truthLepton_d0[1]", binning=binning_d0)
h2["from_d0_weighted"] = getHist(t_from, "d0_d0_from_weighted", "truthLepton_d0[0]:truthLepton_d0[1]", sel_name=w, binning=binning_d0)
h2["ratio_d0"] = h2["to_d0"].Clone("ratio_d0")
h2["ratio_d0"].Divide(h2["to_d0"], h2["from_d0_weighted"], 1, 1, "B")
h2["sig_d0"] = h2["to_d0"].Clone("sig_d0")
h2["sig_d0"].Reset()
for i in xrange(1,h2["sig_d0"].GetNbinsX()+1):
    for j in xrange(1,h2["sig_d0"].GetNbinsY()+1):
        err = h2["ratio_d0"].GetBinError(i,j)
        val =  h2["ratio_d0"].GetBinContent(i,j) -1 
        try:
            fill = val/err
        except:
            fill = 0
        h2["sig_d0"].SetBinContent(i,j, fill)





ROOT.gStyle.SetPaintTextFormat("4.3f")
#ROOT.gPad.SetLogz()
for hist in h2:
    c.Clear()
    h2[hist].Draw("colz")
    if "ratio" in hist: h2[hist].SetMaximum(1.5)
    c.Update()
    c.SaveAs("outputPlots/%s_%s.pdf"%(append,hist))


