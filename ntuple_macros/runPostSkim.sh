INPATH=/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data_wip
OUTPATH=${INPATH}

setupATLAS
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt" 

CUT="(lepton_n_baseline>=2)"
dataFiles='data15 data16 data17 data18'
for data in $dataFiles
do
    ./postSkim ${INPATH}/merged/${data}*.root "trees_SR_highd0_" ${OUTPATH}/skim/${data}.root "trees_SR_highd0_" --cutExp ${CUT} 
done
