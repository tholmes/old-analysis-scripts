//-----------------------------------------------------------------------------------------------
// This script reads in an analysis-level ntuple, and adds branches to the ntuple:
// nGenEvents : the total number of AOD events, returned by EventCountHist->GetBinContent(2)
// weight1fb  : the total MC weight for 1/fb, including PU reweighting, lepton and trigger SFs
// SRZ, CRFS, CRT, VRT1, VRS1: integers indicating whether the event passes the given CR/VR/SR
//
// To use this script, check the list of samples in the 'addWeight' function, then do:
// root -l
// .L addBranch.cc+
// addBranch( [pathToNtuples] )
//-----------------------------------------------------------------------------------------------


#include <cstdlib>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <map>
#include <string>
#include <iomanip> 

#include "TFile.h"
#include "TF1.h"
#include "TH1.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TChain.h"
#include "TBranch.h"
#include "TLorentzVector.h"
#include "TSystemDirectory.h"
#include "TList.h"

using namespace std;

vector<string> list_dirs(const char *dirname) { 

    vector<string> v_files;
    TSystemDirectory dir(dirname, dirname); 
    TList *files = dir.GetListOfFiles(); 
    if (files) { 
        TSystemFile *file; TString fname; TIter next(files); 
        while ((file=(TSystemFile*)next())) { 
            fname = file->GetName();
            if (file->IsDirectory() && !fname.EndsWith(".")) { 
                //cout<<fname.Data()<<endl;
                v_files.push_back(fname.Data()); 
            } 
        } 
    } 
    return v_files;
}
vector<string> list_files(const char *dirname, const char *ext=".root") { 

    vector<string> v_files;
    TSystemDirectory dir(dirname, dirname); 
    TList *files = dir.GetListOfFiles(); 
    if (files) { 
        TSystemFile *file; TString fname; TIter next(files); 
        while ((file=(TSystemFile*)next())) { 
            fname = file->GetName();
            if (!file->IsDirectory() && fname.EndsWith(ext)) { 
                //cout<<fname.Data()<<endl;
                v_files.push_back(fname.Data()); 
            } 
        } 
    } 
    return v_files;
}

void addBranch() {

    // Getting the directories we want to add to
    // Directories are the worst in C++ / ROOT!
    string pathToNtuples = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019_IsoTest";
    char *dir = &pathToNtuples[0]; //"/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019_IsoTest";
    vector<string> sampleNames;

    vector<string> subdirs = list_dirs(dir);
    for (auto subdir : subdirs) {
        string dir_str = string(dir);
        string fullSubdir = Form("%s/%s", dir_str.c_str(), subdir.c_str());
        vector<string> rootfiles = list_files(&fullSubdir[0]);
        for (auto rootfile : rootfiles){
            sampleNames.push_back(Form("%s/%s", fullSubdir.c_str(), rootfile.c_str()));
        }
    }
    cout << "Found " << sampleNames.size() << " files." << endl;
    for (auto sampleName : sampleNames) { 

        //---------------------------------------------
        // open file, get Tree and EventCountHist
        //---------------------------------------------

        //string  filename       = Form("%s/%s",pathToNtuples.c_str(),sampleNames[0].c_str()); 
        TFile*  f              = new TFile(sampleName.c_str(),"update");          
        TTree*  T              = (TTree*)f->Get("trees_SR_highd0_");

        cout << endl;
        cout << "Opening file           : " << sampleName        << endl;
        cout << "Events in ntuple       : " << T->GetEntries() << endl;

        //-----------------------------
        // access existing branches
        //-----------------------------

        vector<float> *  electron_ptvarcone20      = 0 ;
        vector<float> *  electron_topoetcone20     = 0 ;
        vector<float> *  electron_pt               = 0 ;
        vector<float> *  electron_FCLoose          = 0 ;
        //vector<float> *  electron_FCLoose_new          = 0 ;
        vector<float> *  electron_FCTight          = 0 ;
        vector<float> *  muon_ptvarcone30          = 0 ;
        vector<float> *  muon_topoetcone20         = 0 ;
        vector<float> *  muon_pt                   = 0 ;
        vector<float> *  muon_FCLoose              = 0 ;
        vector<float> *  muon_FCTight              = 0 ;
        ULong64_t *      eventNumber               = 0 ;

        T->SetBranchAddress("electron_ptvarcone20",  &electron_ptvarcone20 );  
        T->SetBranchAddress("electron_topoetcone20", &electron_topoetcone20);  
        T->SetBranchAddress("electron_pt",           &electron_pt          );  
        //T->SetBranchAddress("electron_FCLoose",      &electron_FCLoose     );  
        //T->SetBranchAddress("electron_FCTight",      &electron_FCTight     );  
        T->SetBranchAddress("muon_ptvarcone30",      &muon_ptvarcone30     );  
        T->SetBranchAddress("muon_topoetcone20",     &muon_topoetcone20    );  
        T->SetBranchAddress("muon_pt",               &muon_pt              );  
        //T->SetBranchAddress("muon_FCLoose",          &muon_FCLoose         );  
        //T->SetBranchAddress("muon_FCTight",          &muon_FCTight         );  
        T->SetBranchAddress("eventNumber",           &eventNumber          );

        //TBranch *b_electron_FCLoose = T->GetBranch("electron_FCLoose");
        //TBranch *b_electron_FCTight = T->GetBranch("electron_FCTight");
        //TBranch *b_muon_FCLoose = T->GetBranch("muon_FCLoose");
        //TBranch *b_muon_FCTight = T->GetBranch("muon_FCTight");

        TBranch *b_electron_FCLoose = T->Branch("electron_FCLoose_updated"          , &electron_FCLoose);
        TBranch *b_electron_FCTight = T->Branch("electron_FCTight_updated"          , &electron_FCTight);
        TBranch *b_muon_FCLoose = T->Branch("muon_FCLoose_updated"          , &muon_FCLoose);
        TBranch *b_muon_FCTight = T->Branch("muon_FCTight_updated"          , &muon_FCTight);

        //-----------------------------
        // add new branches
        //-----------------------------

        // Actually just re-writing branches here, but keeping this code for reference

        //Float_t Z_phi_correct;
        //TBranch *b_zphi        = T->Branch("Z_phi_correct"    , &Z_phi_correct     ,   "Z_phi_correct/F" );

        //-----------------------------
        // loop over events
        //-----------------------------

        Long64_t nentries = T->GetEntries();

        for (Long64_t i=0;i<nentries;i++) {
            //for (Long64_t i=0;i<10;i++) {

            T->GetEntry(i);

            for (int i=0; i<electron_pt->size(); i++) {
                float fcloose = 0;
                float fctight = 0;
                if (electron_topoetcone20->at(i)/electron_pt->at(i) < 0.20 && electron_ptvarcone20->at(i)/electron_pt->at(i) < 0.15) fcloose = 1; 
                if (electron_topoetcone20->at(i)/electron_pt->at(i) < 0.06 && electron_ptvarcone20->at(i)/electron_pt->at(i) < 0.06) fctight = 1; 
                electron_FCLoose->push_back(fcloose);
                electron_FCTight->push_back(fctight);

                /*
                   cout<<endl;
                   cout<<"Event "<<eventNumber<<endl;
                   cout<<"TopoET "<<electron_topoetcone20->at(i)/electron_pt->at(i)<<endl;
                   cout<<"PtVar  "<<electron_ptvarcone20->at(i)/electron_pt->at(i)<<endl;
                   cout<<"FCLoose (<0.20, <0.15) "<<electron_FCLoose->at(i)<<endl;
                   cout<<"FCTight (<0.06, <0.06) "<<electron_FCTight->at(i)<<endl;
                   */
            }         
            for (int i=0; i<muon_pt->size(); i++) {
                float fcloose = 0;
                float fctight = 0;
                if (muon_topoetcone20->at(i)/muon_pt->at(i) < 0.30 && muon_ptvarcone30->at(i)/muon_pt->at(i) < 0.15) fcloose = 1; 
                if (muon_topoetcone20->at(i)/muon_pt->at(i) < 0.06 && muon_ptvarcone30->at(i)/muon_pt->at(i) < 0.06) fctight = 1;
                muon_FCLoose->push_back(fcloose);
                muon_FCTight->push_back(fctight);
            }         

            //b_electron_FCLoose->Fill();     
            //b_electron_FCTight->Fill();     
            //b_muon_FCLoose->Fill();     
            //b_muon_FCTight->Fill();     
            T->Fill();
            electron_FCLoose->clear();
            electron_FCTight->clear();
            muon_FCLoose->clear();
            muon_FCTight->clear();

        }

        //T->Print();
        T->Write();
        f->Close(); //TH
        delete f;


        }
    }


