B1;95;0cINPATH=/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data
OUTPATH=/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/1mu

#OUTPATH=/afs/cern.ch/user/e/eressegu/public

setupATLAS
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt" 

#CUT="(muon_n==0&&electron_n==1&&electron_pt[0]>50)"
CUT="(Length\$(muon_pt)==1&&Length\$(electron_pt)==0)"
data='data17'
for i in 1 2 3 4 5
do
    ./postSkim ${INPATH}/${data}*.root "trees_SR_highd0_" ${OUTPATH}/${data}_${i}.root "trees_SR_highd0_" --cutExp ${CUT} --divFile 5_${i}
done


