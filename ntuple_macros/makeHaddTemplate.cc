/*

Script to make empty tree template for merginging ntuples

Jonathan Long
16/01/08

Run:
root -l -b -q  ~/LLP/scripts/ntuple_macros/makeHaddTemplate.cc

 */



void makeHaddTemplate()
{

    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data_wip/user.tholmes.data15_13TeV.periodAllYear.physics_Main.DAOD_RPVLL_v3.0_trees.root/user.tholmes.00280464.r9264_r10573_p3578.19562640._000257.trees.root"; // data I checked
    // TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/user.tholmes.data17_13TeV.periodI.physics_Main.v3.3.2_dvs_trees.root/user.tholmes.00337662.r10258_r10658_p3578.19974448._000036.trees.root";
    
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/user.tholmes.data18_13TeV.periodK.physics_Main.SUSY15_v4_Feb27_trees.root/user.tholmes.00356124.f950_m1831_r10799_p3651_p4041.20693620._000031.trees.root";
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4.1_Apr2_NewReco/data/user.tholmes.data18_13TeV.periodAllYear.physics_Main.v4.1_Apr2_New_trees.root/user.tholmes.00363033.r11782_r11784_p4072.20929837._000845.trees.root";
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data_wip/user.tholmes.data15_13TeV.periodAllYear.physics_Main.v5.1_May14_NewReco_trees.root/user.tholmes.00284484.r9264_r10573_p3578.21292345._000178.trees.root";
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.2_June4_NewReco_withDVs/data_wip/user.eressegu.data15_13TeV.periodAllYear.physics_Main.v5.2b_June4_NewReco_withDVs_trees.root/user.eressegu.00284285.r11782_r11784_p4072.21515733._000270.trees.root"; 
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/ScaleFactors/user.tholmes.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.mc16e_muon1v3_trees.root/user.tholmes.361107.e3601_e5984_s3126_s3136_r10724_r10726_p4145.21754778._000859.trees.root";
    //TString outputFile = "/afs/cern.ch/user/t/tholmes/LLP/scripts/ntuple_macros/haddTemplate_mc_sfs.root";
    //TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/ScaleFactors/slim/user.tholmes.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.mc16a_muon1v3_trees_slimmed.root/user.tholmes.361107.e3601_s3126_r9364_r9315_p4145.21754904._000257.trees.root";
    TString sourceFile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/ScaleFactors/slim/user.tholmes.data18_13TeV.periodAllYear.physics_Main.muon1v3_trees_slimmed.root/user.tholmes.00364214.f1002_m2037_p4144.21754490._000426.trees.root";
    TString outputFile = "/afs/cern.ch/user/t/tholmes/LLP/scripts/ntuple_macros/haddTemplate_slimmed_data_sfs.root";

    TFile * sourceF = new TFile(sourceFile);
    TFile * outF = new TFile(outputFile,"recreate");

    // Clone tree
    TTree *sourceTree = (TTree*)sourceF->Get("trees_SR_highd0_");
    TTree *newTree = sourceTree->CloneTree(0);

    newTree->SetDirectory(outF);

    // Clone histogram
    // TH1D * newHist = (TH1D*)sourceF->Get("EventCountHist")->Clone();
    // newHist->Reset();
    // newHist->SetDirectory(outF);

    outF->Write();
    outF->Close();


}
