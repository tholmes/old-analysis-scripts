#!/usr/bin/env python
import os
import ROOT
import glob

# Define signal region, then remove all events in it from the file

# Define events to remove
trig_str = "e.triggerRegion_pass and e.lepton_n>1"
e_str = "e.electron_pt[0]>65 and abs(e.electron_d0[0])>3 and e.electron_dpt[0]>-0.5 and e.electron_nMissingLayers[0]<=1 and e.electron_chi2[0]<2"
m_str = "e.muon_pt[0]>65 and abs(e.muon_IDtrack_d0[0])>3 and e.muon_CBtrack_chi2[0]<3 and e.muon_nPres[0]>=3"

# Set up input/output files
input_dir = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/"
input_files = glob.glob(input_dir+"data*.root")

# Loop over input files
for input_file in input_files:
    if "blinded" in input_file: continue

    output_file = input_file.replace(".root", "_blinded.root")
    if os.path.isfile(output_file):
        print "Already have blinded ntuple for this file. Skipping."
        continue

    print "Opening file", input_file
    old_file = ROOT.TFile(input_file, 'READ')
    t_in = old_file.Get('trees_SR_highd0_')

    new_file = ROOT.TFile(output_file, 'RECREATE')
    t_out = t_in.CloneTree(0)

    # Loop over entries
    max_events = t_in.GetEntries()

    i_event = 0
    n_passed = 0
    for e in t_in:

        if i_event % 10000 == 0:
            print 'Processing event %i of %i' % (i_event, max_events)

        try:
            is_sr = False
            if eval(trig_str):
                if e.electron_n>1:
                    is_sr = eval("%s and %s"%(e_str, e_str.replace("[0]", "[1]")))
                elif e.muon_n>1:
                    is_sr = eval("%s and %s"%(m_str, m_str.replace("[0]", "[1]")))
                elif e.muon_n>0 and e.electron_n>0:
                    is_sr = eval("%s and %s"%(e_str, m_str))
        except Exception as ex:
            print ex
            print "electrons:", e.electron_n
            print "muons:", e.muon_n
            print "leptons:", e.lepton_n
            is_sr = False

        if not is_sr:
            t_out.Fill()
            n_passed += 1

        i_event += 1

    # Close files
    new_file.Write()
    old_file.Close()
    new_file.Close()

    # Don't print this with the real SR!!!
    #print "Passed", n_passed, "/", max_events, "events."
