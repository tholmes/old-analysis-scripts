import os

# need to run:
# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase; source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh; localSetupPyAMI

campaigns = ["mc16a", "mc16d", "mc16e"]

def getAMIEvents(ntup_file):
    amiCmd = 'ami command GetDatasetInfo -logicalDatasetName='+ntup_file+' | grep totalEvents'
    amiResult =  os.popen(amiCmd).read()
    amiEvt =  amiResult.split('totalEvents = ')[1].split('\n')[0]
    return int(amiEvt)

def getRucioCampaign(ntup_file):
    events = {"mc16a":0, "mc16d":0, "mc16e":0}
    rucioCmd1 = 'rucio list-content --short %s'%ntup_file
    rucioResult1 = os.popen(rucioCmd1).read()
    for subfile in rucioResult1.split('\n')[0:-1]:
        rucioCmd2 = 'rucio get-metadata %s'%subfile
        rucioResult2 = os.popen(rucioCmd2).read()
        rucioCampaign = rucioResult2.split('campaign:')[1].split('\n')[0].strip()
        rucioEvents = rucioResult2.split('events:')[1].split('\n')[0]
        if rucioCampaign == "None": events["mc16e"] += int(rucioEvents)
        elif rucioCampaign == "MC16:MC16a": events["mc16a"] += int(rucioEvents)
        elif rucioCampaign == "MC16:MC16d" or rucioCampaign == "MC16:MC16c": events["mc16d"] += int(rucioEvents)
        elif rucioCampaign == "MC16:MC16e": events["mc16e"] += int(rucioEvents)
    return events

dsids = {}
with open("HITS_files.txt", "r") as f:
    for line in f:
        dsid = line.split(".")[1]
        if dsid not in dsids: dsids[dsid] = {"hits": []}
        dsids[dsid]["hits"].append(line.strip("\n"))
with open("/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v5/WorkArea/run/mc16a_DAOD_RPVLL_new.ds", "r") as f:
    for line in f:
        dsid = line.split(".")[1]
        if dsid in dsids: dsids[dsid]["mc16a"] = line.strip("\n")
with open("/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v5/WorkArea/run/mc16d_DAOD_RPVLL_new.ds", "r") as f:
    for line in f:
        dsid = line.split(".")[1]
        if dsid in dsids: dsids[dsid]["mc16d"] = line.strip("\n")
with open("/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v5/WorkArea/run/mc16e_DAOD_RPVLL_new.ds", "r") as f:
    for line in f:
        dsid = line.split(".")[1]
        if dsid in dsids: dsids[dsid]["mc16e"] = line.strip("\n")

hits_events = {}
reco_events = {}
hits_metadata = {}

for dsid in sorted(dsids.keys()):
    print "\n%s"%dsid,
    print dsids[dsid]["hits"][0].split(".")[2]
    hits_events[dsid] = {}
    hits_metadata[dsid] = {}
    reco_events[dsid] = {}
    for c in campaigns:
        hits_events[dsid][c] = 0
        hits_metadata[dsid][c] = []
        reco_events[dsid][c] = 0

    # Load in HITS event numbers for each campaign
    for f in dsids[dsid]["hits"]:
        events = getRucioCampaign(f)
        tot_from_rucio = 0
        for c in campaigns:
            if events[c] > 0 : hits_metadata[dsid][c].append(f)
            hits_events[dsid][c] += events[c]
            tot_from_rucio += events[c]
        # Check against total AMI numbers
        tot_events = getAMIEvents(f)
        if tot_from_rucio != tot_events: print "WARNING: RUCIO AND AMI DON'T AGREE!! RUCIO: %d AMI: %d"%(tot_from_rucio, tot_events)

    #n_reco = 0
    print "                mc16a       mc16d       mc16e"
    print "HITS:    %12d%12d%12d"%(hits_events[dsid]["mc16a"], hits_events[dsid]["mc16d"], hits_events[dsid]["mc16e"])
    for c in campaigns:
        if c in dsids[dsid]:
            n_year = getAMIEvents(dsids[dsid][c])
            reco_events[dsid][c] += n_year
    print "RECO:    %12d%12d%12d"%(reco_events[dsid]["mc16a"], reco_events[dsid]["mc16d"], reco_events[dsid]["mc16e"])
    print "DIFF:    %12d%12d%12d"%(hits_events[dsid]["mc16a"]-reco_events[dsid]["mc16a"], hits_events[dsid]["mc16d"]-reco_events[dsid]["mc16d"], hits_events[dsid]["mc16e"]-reco_events[dsid]["mc16e"])

#Output files
for c in campaigns:
    with open("to_be_redone_%s.txt"%c, "w") as f:
        for dsid in sorted(dsids.keys()):
            if hits_events[dsid][c] != reco_events[dsid][c]:
                line = dsids[dsid]["hits"][0].split(".")[2]
                line += "\t" + "MC15.%s."%dsid + dsids[dsid]["hits"][0].split(".")[2] + ".py"
                line += "\t13000"
                line += "\t\t" + str(hits_events[dsid][c] - reco_events[dsid][c])
                line += "\t\t1\tDAOD_RPVLL"
                line += "\t\t\t\t\t" + hits_metadata[dsid][c][0]
                line += "\t\t\t\t\t\t" + hits_metadata[dsid][c][0].split(".")[-1].split("_")[0]
                line += "\t" + hits_metadata[dsid][c][0].split(".")[-1].split("_")[1]
                line += "\n"
                if len(hits_metadata[dsid][c]) > 1:
                    print "WARNING: more HITS files than expected!"
                    print hits_metadata[dsid][c]
                f.write(line)

