#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1.h"
#include "TCut.h"
#include "TROOT.h"
#include "TTreeFormula.h"
#include "TString.h"
#include "TKey.h"
#include "TObject.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include <cstdlib>
#include <cstdio>


///////////////////// Helper functions //////////////////////////////////
std::vector<TString> getObjNameInTFile(TFile* fin, TString obj_type);
void reportLapse(const int &ievt, const int &nevt, const clock_t &run_start);
std::vector<TString> extr_tokens(const TString &in_string, const TString &delimeter);
void maskBranches(TTree *t, TString inFilePath, bool list_keep);

////////////////////// Main /////////////////////////////////
int main(int argc , char** argv){

  bool openHelp=false;
  for(int k=0; k<argc; ++k){
    TString sargv = argv[k];
    if(sargv.Contains("-h") || sargv.Contains("--help")) openHelp=true;
 }
  if(argc<=4) openHelp=true;


  if(openHelp){
    std::cout << "--------------------------------------" << std::endl;
    std::cout << "Usage: apply event skimming based on the TCut experession / tree slimming based on input list" << std::endl;
    std::cout << " $1: Path to the input root file. Regular expression acceptable." << std::endl;
    std::cout << " $2: Name of input tree" << std::endl;
    std::cout << " $3: Path to the output root file (must be different from input path)" << std::endl;
    std::cout << " $4: Name of output tree" << std::endl;
    std::cout << "(optional)" << std::endl;
    std::cout << " --cutExp [cut expression]: TCut expression for the skim. (Use \"%\" instead of \"$\" in the expression)." << std::endl;
    std::cout << " --divFile [n]_[i]: Run only part of the input: i-th out of n-slices." << std::endl;
    std::cout << " --disable_branches [path to input file]: Disable the branches listed in the input file." << std::endl;
    std::cout << " --keep_branches [path to input file]: Disable the branches other than ones listed in the input file." << std::endl;
    std::cout << " --noOtherTrees: Do not save the other trees in the file. (All trees are saved by default)" << std::endl;
    std::cout << " --noHists: Do not save histrograms in the file. (All histograms are saved by default)" << std::endl;
    std::cout << "--------------------------------------" << std::endl;
    return 0;
  }    

  TString inFilePath  = argv[1];  

  TString inTreeName  = argv[2];

  TString outFilePath = argv[3];

  TString outTreeName = argv[4];  

  TString cutExp      = "nocut";

  TString divFile      = "";

  bool    writeAllTrees  = true;
  bool    writeHists  = true;

  TString inFilePath_NG = "";
  TString inFilePath_keep = "";

  for(int k=0; k<argc; ++k) {
    TString key = argv[k];
    if(key=="--cutExp")           cutExp    = argv[k+1];
    if(key=="--divFile")          divFile   = argv[k+1];
    if(key=="--disable_branches") inFilePath_NG   = argv[k+1];
    if(key=="--keep_branches")    inFilePath_keep = argv[k+1];
    if(key=="--noOtherTrees")     writeAllTrees   = false;
    if(key=="--noHists")          writeHists      = false;
  }

  if(divFile!=""){
    if(extr_tokens(divFile,"_").size()==0){
      std::cout << "--divFile option must be followed a token like X_Y where X specifies the number of division of the input, and Y the slice you want run on (Y-th slice). Exit." << std::endl;
      return 0;
    }
  }
  int nDiv = divFile=="" ? 1 : atoi(extr_tokens(divFile,"_")[0]);
  int iDiv = divFile=="" ? 1 : atoi(extr_tokens(divFile,"_")[1]);

  std::cout << std::endl;
  std::cout << "postSkim  INFO  inFilePath: " << inFilePath << std::endl;
  std::cout << "postSkim  INFO  inTreeName: " << inTreeName << std::endl;
  std::cout << "postSkim  INFO  outFilePath: " << outFilePath << std::endl;
  std::cout << "postSkim  INFO  outTreeName: " << outTreeName << std::endl;
  std::cout << "postSkim  INFO  cutExp: " << cutExp << std::endl;
  std::cout << "postSkim  INFO  divFile: " << divFile << " nDiv: " << nDiv <<  " iDiv: " << iDiv << std::endl;
  std::cout << "postSkim  INFO  inFilePath_NG: " << inFilePath_NG << std::endl;
  std::cout << "postSkim  INFO  inFilePath_keep: " << inFilePath_keep << std::endl;
  std::cout << "postSkim  INFO  writeAllTrees: " << writeAllTrees << std::endl;
  std::cout << "postSkim  INFO  writeHists: " << writeHists << std::endl;


  if(inFilePath==outFilePath){
    std::cout << "postSkim  ERROR  Input file path and output file path are the same. Please specify different output path. Exit." << std::endl;
    return 0;
  }

  // -----------------------------------------------------

  // Get tree to skim
  TChain *tin = new TChain(inTreeName);
  tin->Add(inFilePath);

  // Disable branches if necessary 
  if(inFilePath_keep!="")
    maskBranches(tin, inFilePath_keep, true);
  else if(inFilePath_NG!="")
    maskBranches(tin, inFilePath_NG, false);

  // Define output file
  TFile *fout = new TFile(outFilePath,"recreate");
  TTree *tout = 0;

  if(cutExp=="nocut"){
    tout = tin->CloneTree();
    tout->SetName(outTreeName);
  }
  else{

    tout = tin->CloneTree(0);
    tout->SetName(outTreeName);
    
    // Define selector
    cutExp.ReplaceAll("Sum%","Sum$"); 
    TTreeFormula *ttf_cut = new TTreeFormula("ttf_cut", (TCut)cutExp, tin);
    
    // Start loop
    int ntot = tin->GetEntries();
    int i_start = (iDiv-1)*(ntot/nDiv);
    int i_end = iDiv*(ntot/nDiv);
    int nevt = i_end-i_start;
    std::cout << "postSkim  INFO  i_start: " << i_start <<  "  i_end: " << i_end << "  nevt: " << nevt << "  ntot: " << ntot << std::endl;

    clock_t run_start = clock();
    tin->SetEstimate(nevt+1);
    
    for(int jj=i_start; jj<i_end; ++jj) {
      tin->GetEntry(jj);
      if(jj%10000==0){
	reportLapse(jj-i_start,nevt,run_start);
	if(jj) std::cout << "  " << tout->GetEntries() << " entries are filled. (Skim eff.: " << (tout->GetEntries()*100.)/jj << "%)" << std::endl;
      }
      ttf_cut->GetNdata();
    if(    ttf_cut->EvalInstance()  )  {
      tout->Fill();    
    }
    }
    std::cout << "postSkim  INFO  Skim done. " << tout->GetEntries() << " entries are filled. (Skim eff.: " << (tout->GetEntries()*100.)/nevt << "%)"  << std::endl;
    
  }


  // Copy other supplementary trees/histograms
  TFile *fin = new TFile(inFilePath,"read");  
  
  // Aux trees
  if(writeAllTrees){
    std::vector<TString> v_tname = getObjNameInTFile(fin,"TTree");
    for(int it=0; it<(int)v_tname.size(); it++){
      if(v_tname[it]==inTreeName) continue;
      
      TChain* tree_in = new TChain(v_tname[it]);
      tree_in->Add(inFilePath);
      fout->cd();
      TTree *tree_out = tree_in->CloneTree();
      tree_out->SetDirectory(fout);
      //delete tree_in;
      //delete tree_out;
    }
  }

  
  // Histograms
  if(writeHists){
    std::vector<TString> v_hname = getObjNameInTFile(fin,"TH1F");
    for(int ih=0; ih<(int)v_hname.size(); ih++){
      TH1F* h = (TH1F*) fin->Get(v_hname[ih]);
      if(!h) break;
      h->SetDirectory(fout);
      //delete h;
    }
  }
  std::cout << "postSkim  INFO  Done.   " << std::endl << std::endl;


  fin->Close();

  fout->cd();
  fout->Write();
  delete tin;
  fout->Close();

  //delete fout;
  //delete tout;
  //delete fin;
}

/////////////////////////////////////////////////////////////////////
std::vector<TString> getObjNameInTFile(TFile* fin, TString obj_type) {
   
   std::vector<TString> objNameList;
   
   TList* list = fin->GetListOfKeys() ;
   if (!list) { printf("<E> No keys found in file\n") ; exit(1) ; }

   TIter next(list) ;
   TKey* key ;
   TObject* obj ;
   
   int nsources=0;
   do{    
     key = (TKey*)next();	if(!key) break;
     nsources++;

     try {
       obj = key->ReadObj() ;     
            
       if ( obj->IsA()->GetName()==obj_type  ){
	 TString objName = obj->GetName();		
	 objNameList.push_back(objName);
	 //std::cout << "myUtils::getObjNameInTFile  INFO  Filled. nVec:" << objNameList.size() << std::endl;
       }	 
      
     } catch (std::exception& e)
	 {
	   std::cout << e.what() << std::endl;
	   std::cout << "myUtils::getObjNameInTFile  INFO  Hit the load limit of TFile. Return current vec." << std::endl;
	   return objNameList;
	 }
       
     
   } while (key) ;

   //std::cout << "myUtils::getObjNameInTFile  INFO  Done." << std::endl;

   //delete list;
   //delete key;
   //delete obj;

   return objNameList;
 }
///////////////////////////////////////////////////////////////////
void reportLapse(const int &ievt, const int &nevt, const clock_t &run_start){

  if(!nevt) return;
  
  clock_t run_stamp = clock(); 
  std::cout
    << " evt: " << ievt << " / " << nevt
    << " " << (int)ievt*100/nevt << "% processed so far." << std::endl;
    /*
    << "       lap:    " << (double)(run_stamp-run_start)/CLOCKS_PER_SEC << "s";
  
  if(ievt==0)
    std::cout  << "       ETA: ---- " << std::endl;
  else{
    double ETA = ((nevt-ievt)/(ievt+0.))*(double)(run_stamp-run_start)/CLOCKS_PER_SEC;
    if(ETA<60) 
      std::cout  << "       ETA:    " << ETA << "s" << std::endl;
    else if(ETA<3600)
      std::cout  << "       ETA:    " << (int)(ETA/60) << "m" << (int)ETA-60*(int)(ETA/60) << "s" << std::endl;
    else
      std::cout  << "       ETA:    " << (int)(ETA/3600) << "h" << (int)(ETA-3600*(int)(ETA/3600))/60 << "m" << std::endl;
  }
    */
  
}
////////////////////////////////////////////////////////////////////
// Return a std::vector of all tokens
std::vector<TString> extr_tokens(const TString &in_string, const TString &delimeter){

  std::vector<TString> vec;    
  int Nfields=(in_string.Tokenize(delimeter))->GetEntries();
  for( int field=1; field<=Nfields; field++ )
    vec.push_back(  ((TObjString *)(in_string.Tokenize(delimeter))->At(field-1))->String()  );
  return vec;
}
////////////////////////////////////////////////////////////////////
inline bool find(std::vector<TString> vec, TString source){
  for(int is=0; is<(int)vec.size(); ++is){ if(vec[is]==source) return true; }
  return false;
}
////////////////////////////////////////////////////////////////////
void maskBranches(TTree *t, TString inFilePath, bool list_keep){

  std::vector<TString> v_br ;
  std::ifstream fin(inFilePath);
  while( !fin.eof() ) { TString br; fin >> br; v_br.push_back(br); }
  fin.close();
  
  std::vector<TString> v_active, v_disabled ;
    
  TObjArray *branches = t->GetListOfBranches();
  for(int ib=0, nb=(int)branches->GetEntries(); ib<nb; ++ib){
    TString branchName=branches->At(ib)->GetName();  

    if(  (list_keep && !find(v_br,branchName) ) || 
	 (!list_keep && find(v_br,branchName)) )  {
      t->SetBranchStatus(branchName,0);
      v_disabled.push_back(branchName);
    }
    else v_active.push_back(branchName);
  }
  
  std::cout << "-------------------------------------------------------------------" << std::endl;
  std::cout << "postSkim::maskBranches  INFO  Branches kept: " << std::endl;
  for(int ib=0; ib<(int) v_active.size(); ++ib) 
    std::cout << "    " << ib << ": "<< v_active[ib] << std::endl;

  std::cout << "-------------------------------------------------------------------" << std::endl;
  std::cout << "postSkim::maskBranches  INFO  Branches disabled: " << std::endl;
  for(int ib=0; ib<(int) v_disabled.size(); ++ib) 
    std::cout << "    " << ib << ": "<< v_disabled[ib] << std::endl;
  
  std::cout << "-------------------------------------------------------------------" << std::endl;

  //delete branches;

  return;

}
////////////////////////////////////////////////////////////////////
