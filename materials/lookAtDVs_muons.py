# Goal: Estimate the fake muon contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gStyle.SetErrorX(0.45)
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"signal":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_dvs/data-trees/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101nochanges_1kevt.AOD.pool.root.root"},
        #"data":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_children_data/data-trees/DAOD_RPVLL.14400886._000210.pool.root.1.root"},
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.2_Nov2019_DVs/photon.root"},
        "photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/photon.root"},
        "ttbar":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/ttbar.root"},
        }

variables = {
        #"pdgid": {"nbins": 100, "xmin": 0, "xmax": 100, "xlabel": "Track PDG ID"},
        "pt":  {"nbins": 20, "xmin": 0, "xmax": 400, "xlabel": "muon p_{T} [GeV]",       "accessor": "event.muon_pt[e_index]"},
        #"R":  {"nbins": 20, "xmin": 0, "xmax": 200, "xlabel": "muon Decay Radius [mm]", "accessor": "getDecayRadius(event, t_index)},
        "d0":  {"nbins": 30, "xmin": 0, "xmax": 100, "xlabel": "muon d_{0} [mm]",        "accessor": "abs(event.muon_IDtrack_d0[e_index])"},
        "chi2":  {"nbins": 30, "xmin": 0, "xmax": 15, "xlabel": "muon #chi^{2}",        "accessor": "event.muon_CBtrack_chi2[e_index]"},
        "nprec":  {"nbins": 10, "xmin": -0.5, "xmax": 9.5, "xlabel": "muon number of precision hits",        "accessor": "event.muon_MStrack_nPres[e_index]"},
        "mat": {"nbins": 2, "xmin": -0.5, "xmax": 1.5, "xlabel": "Vertex passes material veto?", "accessor": "event.DV_passMaterialVeto[event.dvtrack_DVIndex[i]]"},
        "R":   {"nbins": 20, "xmin": 0, "xmax": 200, "xlabel": "R_{xy} of DV", "accessor": "event.DV_rxy[event.dvtrack_DVIndex[i]]"},
        "ntrack":   {"nbins": 20, "xmin": -0.5, "xmax": 19.5, "xlabel": "# of Tracks in DV", "accessor": "event.DV_nTracks[event.dvtrack_DVIndex[i]]"},
        }

def getDecayRadius(event, j):
    return (event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2)

def getTruthIndex(event, i):
    for j in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_barcode[j] == event.dvtrack_truthMatchedBarcode[i]: return j
    return -1

def getElIndex(event, i):
    for j in xrange(len(event.muon_pt)):
        if event.muon_IDtrack_pt[j] == event.dvtrack_pt[i]:
            return j
    return -1

hists = {}
for v in variables:
    hists[v] = ROOT.TH1F("h_%s"%v, "h_%s"%v, variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])

h_true_v_reco = ROOT.TH2F("true_v_reco", "true_v_reco", 2, -.5, 1.5, 2, -.5, 1.5)
#h_2nd_track = ROOT.TH2F("2nd", "true_v_reco", 2, -.5, 1.5, 2, -.5, 1.5)

verbose = True

#t = getTree(samples["signal"]["fname"])
#t = getTree(samples["photon"]["fname"])
t = getTree(samples["ttbar"]["fname"])
#t = getTree(samples["data"]["fname"])
n_events=0
for event in t:

    if n_events%5000==0: print "Processing event", n_events
    n_events += 1

    for i in xrange(len(event.dvtrack_pt)):
        #if event.dvtrack_pt[i]<50 or abs(event.dvtrack_d0[i])<1: continue
        t_index = getTruthIndex(event, i)
        e_index = getElIndex(event, i)
        if verbose:
            if t_index != -1 or e_index != -1:
                print "Event", n_events
                if t_index != -1 and abs(event.truthLepton_pdgId[t_index])==13:
                    print "\tTrue particle, pdgid:", event.truthLepton_pdgId[t_index]
                    print "\t\tpT:", event.truthLepton_pt[e_index]
                    print "\t\td0:", event.truthLepton_d0[e_index]
                if e_index != -1:
                    print "\tMatched reco mu:"
                    print "\t\tpT:", event.muon_pt[e_index]
                    print "\t\td0:", event.muon_IDtrack_d0[e_index]
        if e_index != -1:
            #if event.muon_dpt[e_index] < -0.5 or abs(event.muon_IDtrack_d0[e_index])<3: continue
            for v in variables:
                try:
                    hists[v].Fill(eval(variables[v]["accessor"]), event.normweight*lumi)
                except:
                    print v, e_index

            # Loop over all other tracks from the same DV
            print "\tMuon DV, ntracks:", event.DV_nTracks[event.dvtrack_DVIndex[i]]
            for j in xrange(len(event.dvtrack_pt)):
                if i!=j and event.dvtrack_DVIndex[i]==event.dvtrack_DVIndex[j]:
                    t_index2 = getTruthIndex(event, j)
                    e_index2 = getElIndex(event, j)
                    if verbose:
                        print "\tTrack from same DV:"
                        print "\t\tTrack pT:", event.dvtrack_pt[j]
                        print "\t\tTrack d0:", event.dvtrack_d0[j]
                        #print "\t\tTrack chi2:", event.dvtrack_chi2[j]
                        if t_index2 != -1 or e_index2 != -1:
                            if t_index2 != -1:
                                print "\t\tTrue particle, pdgid:", event.truthLepton_pdgId[t_index2]
                                print "\t\t\tpT:", event.truthLepton_pt[e_index2]
                                print "\t\t\td0:", event.truthLepton_d0[e_index2]
                            if e_index2 != -1:
                                print "\t\tMatched reco mu:"
                                print "\t\t\tpT:", event.muon_pt[e_index2]
                                print "\t\t\td0:", event.muon_IDtrack_d0[e_index2]

        h_true_v_reco.Fill( (t_index != -1), (e_index != -1), event.normweight*lumi)

h_true_v_reco.GetXaxis().SetTitle("Track matched to true particle?")
h_true_v_reco.GetYaxis().SetTitle("Track matched to reco muon?")
h_true_v_reco.Draw("colz")
raw_input("...")

cans = {}
for v in variables:
    cans[v] = ROOT.TCanvas("c_%s"%v, "c")
    hists[v].GetXaxis().SetTitle(variables[v]["xlabel"])
    hists[v].Draw()
    raw_input("...")
    cans[v].SaveAs("plots/h_%s.pdf"%v)

