# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os
import math
from ROOT import TLorentzVector, TArray
import numpy as np

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../cosmic_helpers.py")
ROOT.gStyle.SetErrorX(0.45)
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Applying cosmic veto
maps = ROOT.TFile("segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask")
middleMap = maps.Get("BM_mask")
outerMap = maps.Get("BO_mask")

# Settings
lumi = 140000
samples = {
        #"signal":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_dvs/data-trees/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101nochanges_1kevt.AOD.pool.root.root"},
        #"data":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_children_data/data-trees/DAOD_RPVLL.14400886._000210.pool.root.1.root"},
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.2_Nov2019_DVs/photon.root"},
        #"data":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/to_be_merged/Data/data*.root"},
        #"data":    {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.5_Jan2020_DVs/Data/*.root"},
        #"data":     {"fname": "/eos/user/t/tholmes/llp/v3.5_Jan2020_DVs/Data/*root"},
        #"data":     {"fname": "/afs/cern.ch/user/e/eressegu/public/ntuples/data*root"},
            "data":   {"fname": "/afs/cern.ch/user/e/eressegu/eos/e_data*.root"},
        "photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/photon.root"},
        "ttbar":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/ttbar.root"},
        }

variables = {
        "pt":  {"nbins": 15, "xmin": 0, "xmax": 300, "xlabel": "Electron p_{T} [GeV]",       "accessor": "event.electron_pt[e_index]"},
        "d0":  {"nbins": 25, "xmin": 0, "xmax": 50, "xlabel": "Electron d_{0} [mm]",        "accessor": "abs(event.electron_d0[e_index])"},
            "dpt": {"nbins": 12, "xmin": -1, "xmax": 5, "xlabel": "Electron #Delta p_{T}",      "accessor": "event.electron_dpt[e_index]"},
        "chi2": {"nbins": 20, "xmin": 0, "xmax": 10, "xlabel": "Electron #chi^{2}",      "accessor": "event.electron_chi2[e_index]"},
        "mat": {"nbins": 2, "xmin": -0.5, "xmax": 1.5, "xlabel": "Vertex passes material veto?", "accessor": "event.DV_passMaterialVeto[event.dvtrack_DVIndex[i]]"},
        "R":   {"nbins": 10, "xmin": 0, "xmax": 300, "xlabel": "R_{xy} of DV", "accessor": "event.DV_rxy[event.dvtrack_DVIndex[j]]"},
        "m":   {"nbins": 50, "xmin": 0, "xmax": 100, "xlabel": "m of DV", "accessor": "event.DV_m[event.dvtrack_DVIndex[j]]"},
        "ntrack":   {"nbins": 20, "xmin": -0.5, "xmax": 19.5, "xlabel": "# of Tracks in DV", "accessor": "event.DV_nTracks[event.dvtrack_DVIndex[i]]"},
        "trackpt":   {"nbins": 10, "xmin": 0., "xmax": 200., "xlabel": "p_{T} of non-electron tracks in DV", "accessor": "BYHAND"},
        "trackd0":   {"nbins": 20, "xmin": 0., "xmax": 50., "xlabel": "d_{0} of non-electron tracks in DV", "accessor": "BYHAND"},
        "trackz0":   {"nbins": 20, "xmin": 0., "xmax": 50., "xlabel": "z_{0} of non-electron tracks in DV", "accessor": "BYHAND"},
        "trackchi2": {"nbins": 20, "xmin": 0., "xmax": 10., "xlabel": "#chi^{2} of non-electron ID tracks in DV", "accessor": "BYHAND"},
        "trackpixholes": {"nbins": 10, "xmin": 0., "xmax": 10., "xlabel": "# of Pixel holes of non-electron tracks in DV", "accessor": "BYHAND"},
        "tracksctholes": {"nbins": 10, "xmin": 0., "xmax": 10., "xlabel": "# of SCT holes of non-electron tracks in DV", "accessor": "BYHAND"},
       "trackholes": {"nbins": 10, "xmin": 0., "xmax": 10., "xlabel": "# of SCT+Pixel holes of non-electron tracks in DV", "accessor": "BYHAND"},
        "eta":                  {"nbins": 30,    "xmin": -3.,   "xmax": 3.,     "xlabel": "Lepton #eta",   "accessor": "event.electron_eta[e_index]"},
        "phi":                  {"nbins": 36,    "xmin": -3.6,  "xmax": 3.6,    "xlabel": "Lepton #phi",  "accessor": "event.electron_phi[e_index]"},
        "tracketa": {"nbins": 30,    "xmin": -3.,   "xmax": 3.,     "xlabel": "#eta of non-electron tracks in DV",   "accessor": "BYHAND"},
        "trackphi": {"nbins": 36,    "xmin": -3.6,  "xmax": 3.6,    "xlabel": "#phi of non-electron tracks in DV",  "accessor": "BYHAND"},
        "tracketawrtdv": {"nbins": 30,    "xmin": -3.,   "xmax": 3.,     "xlabel": "#eta of non-electron tracks wrt DV",   "accessor": "BYHAND"},
        "trackphiwrtdv": {"nbins": 36,    "xmin": -3.6,  "xmax": 3.6,    "xlabel": "#phi of non-electron tracks wrt DV",  "accessor": "BYHAND"},
        "trackphiwrtel": {"nbins": 36,    "xmin": -3.6,  "xmax": 3.6,    "xlabel": "#Delta #phi of non-electron tracks wrt electron",  "accessor": "BYHAND"},
        "trackRwrtel": {"nbins": 25, "xmin":0, "xmax":5, "xlabel": "#Delta R(non-electron track, electron)", "accessor":"BYHAND"},
       #"nPresHits": {"nbins": 10, "xmin": 0., "xmax": 10., "xlabel": "# of MS precision hits of non-electron tracks in DV", "accessor": "BYHAND"},
        #"msSegment_t0": {"nbins":30     , "xmin":-30, "xmax": 30, "xlabel": "Electron t0", "accessor": "event.electron_msSegment_t0[e_index]"},
        }

selections = {
        "baseline": "1",
        "cleaning": "event.electron_dpt[0]>-0.5 and event.electron_nMissingLayers[0]<=1 and event.electron_chi2[0]<2",
        "kinematic": "event.electron_pt[0]>65 and abs(event.electron_d0[0])>3",
        "isolation": "event.electron_FCTight[0]==1",
        "material": "event.DV_passMaterialVeto[event.dvtrack_DVIndex[i]]==0",

        }
ordered_selections = ["baseline", "cleaning", "kinematic", "isolation", "material"]

def getDecayRadius(event, j):
    return (event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2)

def getTruthIndex(event, i):
    for j in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_barcode[j] == event.dvtrack_truthMatchedBarcode[i]: return j
    return -1

def getElIndex(event, i):
    for j in xrange(len(event.electron_IDtrack_index)):
        if event.electron_IDtrack_index[j] == event.dvtrack_index[i]:
            if not event.electron_IDtrack_isPrimary[j]: continue
            return event.electron_IDtrack_electronindex[j]
    return -1


def getTrackIndex(event, i):
    for j in xrange(len(event.idTrack_index)):
        if event.dvtrack_index[i] == event.idTrack_index[j]: return j
    return -1

hists = {}
for s in selections:
    for v in variables:
        hists[s,v] = ROOT.TH1F("h_%s_%s"%(v,s), s, variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])

h_true_v_reco = ROOT.TH2F("true_v_reco", "true_v_reco", 2, -.5, 1.5, 2, -.5, 1.5)
hists2d = {}
h_cutflow = {}
for s in selections:
    hists2d[s, "trackd0_v_trackpt"] = ROOT.TH2F("track_d0_v_pt_%s"%s, "track_d0_v_pt", 20, 0, 200, 20, 0, 20)
    hists2d[s, "trackd0_v_R"] = ROOT.TH2F("trackd0_v_R_%s"%s, "trackd0_v_R", 10, 0, 100, 20, 0, 20)
    hists2d[s, "trackpt_v_R"] = ROOT.TH2F("trackpt_v_R_%s"%s, "trackpt_v_R", 10, 0, 100, 20, 0, 200)
    hists2d[s, "trackd0_v_m"] = ROOT.TH2F("trackd0_v_m_%s"%s, "trackd0_v_m", 50, 0, 100, 20, 0, 20)
    hists2d[s, "trackpt_v_m"] = ROOT.TH2F("trackpt_v_m_%s"%s, "trackpt_v_m", 50, 0, 100, 20, 0, 200)
    hists2d[s, "d0_v_m"] = ROOT.TH2F("d0_v_m_%s"%s, "d0_v_m", 50, 0, 100, 25, 0, 50)
       #hists2d[s, "t0_v_m"] = ROOT.TH2F("t0_v_m_%s"%s, "t0_v_m", 50, 0, 100, 60, -30, 30)
use_data = True
verbose = False
do_scaling = False


if use_data:
    t = getTree(samples["data"]["fname"])
    #t = ROOT.TChain("trees_SR_highd0_")
    #for filename in AODFiles:
    #t.Add(filename)

else:
    #t = getTree(samples["signal"]["fname"])
    t = getTree(samples["photon"]["fname"])
    #t = getTree(samples["ttbar"]["fname"])


#AODFiles = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.5_Jan2020_DVs/user.tholmes.data*/user.tholmes.*.root")
#tree = ROOT.TChain("trees_SR_highd0_")
#for filename in AODFiles:
#    tree.Add(filename)

n_events=0
for event in t:
    #if n_events>50000: break
    if n_events%5000==0: print "Processing event", n_events
    n_events += 1

    # For data, only look in CRs
    if len(event.electron_pt) != 1: continue
    if len(event.muon_pt) != 0: continue

    weight = 1
    if not use_data: weight = event.normweight*lumi

    for i in xrange(len(event.dvtrack_pt)):
        e_index = getElIndex(event, i) #gets the electron index
        t_index = -1
        if not use_data: t_index = getTruthIndex(event, i)
        if verbose:
            if t_index != -1 or e_index != -1:
                print "Event", n_events
                if t_index != -1 and abs(event.truthLepton_pdgId[t_index])==13:
                    print "\tTrue particle, pdgid:", event.truthLepton_pdgId[t_index]
                    print "\t\tpT:", event.truthLepton_pt[e_index]
                    print "\t\td0:", event.truthLepton_d0[e_index]
                if e_index != -1:
                    print "\tMatched reco el:"
                    print "\t\tpT:", event.electron_pt[e_index]
                    print "\t\td0:", event.electron_d0[e_index]


        if e_index != -1:
            passing_cuts = True
            for s in ordered_selections:
                if not eval(selections[s]): 
                    passing_cuts = False
                if passing_cuts:

                    for v in variables:
                        try:
                            if v not in ["trackpt", "trackd0", "tracksctholes", "trackpixholes", "trackchi2", "tracketa", "trackphi", "tracketawrtdv", "trackphiwrtdv","R", "m", "trackholes", "trackz0", "trackd0", "trackphiwrtel", "trackRwrtel"]:
                                hists[s,v].Fill(eval(variables[v]["accessor"]), weight)
                               
                        except:
                            print v, e_index

                    
                    # Loop over all other tracks from the same DV
                    for j in xrange(len(event.dvtrack_pt)):
                        if i!=j and event.dvtrack_DVIndex[i]==event.dvtrack_DVIndex[j]:

                            #selection on tracks
                            id_index = getTrackIndex(event,j)
                            if id_index == -1: continue

                            #if event.DV_m[event.dvtrack_DVIndex[j]] > 80: continue
                            if abs(event.dvtrack_d0[j]) < 1: continue
                            if event.idTrack_chi2[id_index] > 2. : continue
                            if event.dvtrack_pt[j] < 10: continue 
                            
                            nholes = ord(event.dvtrack_NPixHoles[j])+ ord(event.dvtrack_NSctHoles[j])
                            if nholes > 1: continue

                            mu_tlv = TLorentzVector()
                            trk_tlv = TLorentzVector()
                            dPhiMuTrk = -999
                            mu_tlv.SetPtEtaPhiM(event.electron_pt[e_index], event.electron_eta[e_index], event.electron_phi[e_index], 0.511)
                            trk_tlv.SetPtEtaPhiM(event.dvtrack_pt[j], event.dvtrack_eta[j], event.dvtrack_phi[j], event.dvtrack_m[j])

                            dPhiMuTrk = trk_tlv.DeltaPhi(mu_tlv)
                            dRMuTrk = trk_tlv.DeltaR(mu_tlv)
                            
                            #if abs(dRMuTrk) < 0.2 : continue

                            #hists[s, "CBTrackchi2"].Fill(event.muon_CBtrack_chi2[event.dvtrack_DVIndex[j]], weight)
                            #hists[s, "nPresHits"].Fill(event.muon_msSegment_nPresHits[event.dvtrack_DVIndex[j]], weight)
                            hists[s,"m"].Fill(eval(variables["m"]["accessor"]), weight)

                            hists[s,"trackphiwrtel"].Fill(dPhiMuTrk, weight)
                            hists[s, "trackRwrtel"].Fill(dRMuTrk, weight)

                            hists[s,"trackchi2"].Fill(event.idTrack_chi2[id_index], weight)
                            hists[s,"R"].Fill(eval(variables["R"]["accessor"]), weight)
                            hists[s,"trackpt"].Fill(event.dvtrack_pt[j], weight)
                            hists[s,"tracksctholes"].Fill(event.dvtrack_NSctHoles[j], weight)
                            hists[s,"trackpixholes"].Fill(event.dvtrack_NPixHoles[j], weight)
                            hists[s,"trackholes"].Fill(nholes, weight)
                            hists[s,"trackd0"].Fill(abs(event.dvtrack_d0[j]), weight)
                            hists[s,"tracketa"].Fill(event.dvtrack_eta[j], weight)
                            hists[s,"trackphi"].Fill(event.dvtrack_phi[j], weight)
                            hists[s,"tracketawrtdv"].Fill(event.dvtrack_etaWrtDV[j], weight)
                            hists[s,"trackphiwrtdv"].Fill(event.dvtrack_phiWrtDV[j], weight)

                            #if id_index == -1: hists[s,"trackchi2"].Fill(-1, weight)
                            #else: hists[s,"trackchi2"].Fill(event.idTrack_chi2[id_index], weight)

                            # Second track selections
                            #if id_index == -1: continue
                            #print"hi1"
                            #if event.idTrack_chi2[id_index] > 2: continue
                            #print"hi2"
                            #print type(event.dvtrack_NPixHoles[j])
                            #print len(event.dvtrack_NPixHoles[j])
                            #print bytearray(event.dvtrack_NPixHoles[j])
                            #print "|%s|"%event.dvtrack_NPixHoles[i]
                            #print event.dvtrack_NSctHoles[j]
                            #if event.dvtrack_NPixHoles[j]+event.dvtrack_NSctHoles[j] > 1: continue
                            #print"h3"
                            #if event.dvtrack_pt[j] < 10: continue
                            #print"hi4"
                            #if abs(event.dvtrack_d0[j]) < 1: continue
                            #print "done"

                            hists2d[s, "trackd0_v_trackpt"].Fill(event.dvtrack_pt[j], abs(event.dvtrack_d0[j]), weight)
                            hists2d[s, "trackd0_v_R"].Fill(event.DV_rxy[event.dvtrack_DVIndex[i]], abs(event.dvtrack_d0[j]), weight)
                            hists2d[s, "trackpt_v_R"].Fill(event.DV_rxy[event.dvtrack_DVIndex[i]], abs(event.dvtrack_pt[j]), weight)
                            hists2d[s, "trackd0_v_m"].Fill(event.DV_m[event.dvtrack_DVIndex[i]], abs(event.dvtrack_d0[j]), weight)
                            hists2d[s, "trackpt_v_m"].Fill(event.DV_m[event.dvtrack_DVIndex[i]], abs(event.dvtrack_pt[j]), weight)
                            
                            
                            if s == 'isolation':
                                print 'event number', 'run number', 'lumi block'
                                print event.runNumber, event.eventNumber, event.lumiBlock
                                print 'electron pt, eta, phi, d0, z0'
                                print event.electron_pt[e_index], event.electron_eta[e_index], event.electron_phi[e_index], event.electron_d0[e_index], event.electron_z0[e_index]

                                print 'track pt, eta, phi, d0, z0'
                                print event.dvtrack_pt[j], event.dvtrack_eta[j], event.dvtrack_phi[j], event.dvtrack_d0[j], event.dvtrack_z0[j]

                                print 'dv chi2', 'ID track chi2', 'el chi2'
                                print event.DV_chisqPerDoF[event.dvtrack_DVIndex[j]], event.idTrack_chi2[id_index], event.electron_chi2[e_index]

                                print 'deltaPhi(el, trk), deltaR(el,trk)'
                                print dPhiMuTrk, dRMuTrk

                                print 'npixholes, nsctholes'
                                print ord(event.dvtrack_NPixHoles[j]), ord(event.dvtrack_NSctHoles[j])

                                print 'DV r', 'DV rxy', 'DV ntracks', 'pass trigger'
                                print event.DV_r[event.dvtrack_DVIndex[i]], event.DV_rxy[event.dvtrack_DVIndex[i]], event.DV_nTracks[event.dvtrack_DVIndex[i]], event.triggerRegion_pass
                            if verbose:
                                t_index2 = -1
                                if not use_data: t_index2 = getTruthIndex(event, j)
                                e_index2 = getElIndex(event, j)
                                print "\tTrack from same DV:"
                                print "\t\tTrack pT:", event.dvtrack_pt[j]
                                print "\t\tTrack d0:", event.dvtrack_d0[j]
                                #print "\t\tTrack chi2:", event.dvtrack_chi2[j]
                                if t_index2 != -1 or e_index2 != -1:
                                    if t_index2 != -1:
                                        print "\t\tTrue particle, pdgid:", event.truthLepton_pdgId[t_index2]
                                        print "\t\t\tpT:", event.truthLepton_pt[e_index2]
                                        print "\t\t\td0:", event.truthLepton_d0[e_index2]
                                    if e_index2 != -1:
                                        print "\t\tMatched reco el:"
                                        print "\t\t\tpT:", event.electron_pt[e_index2]
                                        print "\t\t\td0:", event.electron_d0[e_index2]

        if use_data:
            h_true_v_reco.Fill( (t_index != -1), (e_index != -1))
        else:
            h_true_v_reco.Fill( (t_index != -1), (e_index != -1), event.normweight*lumi)

#h_true_v_reco.GetXaxis().SetTitle("Track matched to true particle?")
#h_true_v_reco.GetYaxis().SetTitle("Track matched to reco muon?")
#h_true_v_reco.Draw("colz")
#raw_input("...")

cans2d = {}
for s,v in hists2d:
    cans2d[s,v] = ROOT.TCanvas("c_%s_%s"%(s,v), "c")
    addOverflow2D(hists2d[s,v])
    vy = v.split("_")[0]
    vx = v.split("_")[2]
    hists2d[s,v].GetXaxis().SetTitle(variables[vx]["xlabel"])
    hists2d[s,v].GetYaxis().SetTitle(variables[vy]["xlabel"])
    hists2d[s,v].Draw("colz")
    savename = "plots/h_%s_%s"%(v,s)
    if use_data: savename = "plots/h_electron_%s_%s_data"%(v,s)
    ATLAS_LABEL(0.2,0.87,1);
    myText(0.3,0.87,1,"Internal");
    myText(0.2,0.81,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}");

    cans2d[s,v].cd()
    cans2d[s,v].Modified()
    cans2d[s,v].Update()

    cans2d[s,v].SaveAs(savename+'.pdf')
    cans2d[s,v].SaveAs(savename+'.eps')

    


cans = {}
for v in variables:
    cans[v] = ROOT.TCanvas("c_%s"%v, "c")

    ymax = 0
    for i, s in enumerate(ordered_selections):
        addOverflow(hists[s,v])
        if do_scaling:
            if hists[s,v].Integral(0, hists[s,v].GetNbinsX()+1) > 0: hists[s,v].Scale(1./hists[s,v].Integral(0, hists[s,v].GetNbinsX()+1))
        hists[s,v].SetLineColor(theme_colors[i])
        hists[s,v].SetMarkerColor(theme_colors[i])
        hists[s,v].GetXaxis().SetTitle(variables[v]["xlabel"])
        hists[s,v].GetYaxis().SetTitle("Events")
        ymax = max(ymax, hists[s,v].GetMaximum())

    for i,s in enumerate(ordered_selections):
        if do_scaling:
            hists[s,v].SetMinimum(0)
            hists[s,v].SetMaximum(ymax*1.3)
        else:
            hists[s,v].SetMinimum(0.1)
            hists[s,v].SetMaximum(1000*ymax)
            cans[v].SetLogy()
        if i==0: hists[s,v].Draw()
        else: hists[s,v].Draw("same")

    leg = cans[v].BuildLegend(.6,.65,.8,.90)
    leg.Draw()
    #raw_input("...")
    savename = "plots/h_%s"%v
    if use_data: savename = "plots/h_electron_%s_data"%v

    ATLAS_LABEL(0.2,0.87,1);
    myText(0.3,0.87,1,"Internal");
    myText(0.2,0.81,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}");

    cans[v].cd()
    cans[v].Modified()
    cans[v].Update()

    cans[v].SaveAs(savename+'.pdf')
    cans[v].SaveAs(savename+'.eps')

    


