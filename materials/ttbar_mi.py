# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
import math
import glob
import os


PTCUT = 60
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 3
flav = ["e","m"]
variables = {
    "all_reco_pt": {"xmin": 0., "xmax": 200., "nbins": 100, "xlab": "all reco p_{T} [GeV]"},
    "all_reco_d0": {"xmin": 0., "xmax": 8., "nbins": 50, "xlab": "all reco d_{0} [mm]"},
    "50cut_reco_d0": {"xmin": 0., "xmax": 8., "nbins": 50, "xlab": "all reco d_{0}, p_{T} > 50 GeV, [mm]"},
    "100cut_reco_d0": {"xmin": 0., "xmax": 8., "nbins": 50, "xlab": "all reco d_{0}, p_{T} > 8 GeV, [mm]"},
    "150cut_reco_d0": {"xmin": 0., "xmax": 8., "nbins": 50, "xlab": "all reco d_{0}, p_{T} > 8 GeV, [mm]"},
    "all_reco_rdecay": {"xmin": 0., "xmax": 200., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    "50cut_reco_rdecay": {"xmin": 0., "xmax": 200., "nbins": 80, "xlab": "all reco r_{decay}, p_{T} > 50 GeV, [mm]"},
    "100cut_reco_rdecay": {"xmin": 0., "xmax": 200., "nbins": 80, "xlab": "all reco r_{decay}, p_{T} > 100 GeV, [mm]"},
    "150cut_reco_rdecay": {"xmin": 0., "xmax": 200., "nbins": 80, "xlab": "all reco r_{decay}, p_{T} > 150 GeV, [mm]"},
    "150cut_parent_barcode": {"xmin": 0., "xmax": 300000., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "150cut_parent_pdgid": {"xmin": 0., "xmax": 40., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "150cut_promptparent_pdgid": {"xmin": 0., "xmax": 40., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "150cut_promptparent_barcode": {"xmin": 0., "xmax": 300000., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "100cut_1mm_parent_barcode": {"xmin": 0., "xmax": 300000., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "100cut_1mm_parent_pdgid": {"xmin": 0., "xmax": 40., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "100cut_1mm_promptparent_pdgid": {"xmin": 0., "xmax": 40., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "100cut_1mm_promptparent_barcode": {"xmin": 0., "xmax": 300000., "nbins": 40, "xlab": "all reco r_{decay} [mm]"},
    "100cut_1mm_true_d0": {"xmin": 0., "xmax": 8., "nbins": 50, "xlab": "all reco d_{0}, p_{T} > 100 GeV, [mm]"},
    "all_true_rxy": {"xmin": 0., "xmax": 100., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    "signal_true_rxy": {"xmin": 0., "xmax": 100., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    "50cut_true_rxy": {"xmin": 0., "xmax": 100., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    "100cut_true_rxy": {"xmin": 0., "xmax": 100., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    "150cut_true_rxy": {"xmin": 0., "xmax": 100., "nbins": 80, "xlab": "all reco r_{decay} [mm]"},
    
}
variables2d = {
    "all_material_map" : {"xmin": 0., "xmax": 100., "xbins": 80, "ymin": -3.2, "ymax": 3.2, "ybins": 80},
    "signal_material_map" : {"xmin": 0., "xmax": 100., "xbins": 80, "ymin": -3.2, "ymax": 3.2, "ybins": 80},
    "50cut_material_map" : {"xmin": 0., "xmax": 100., "xbins": 80, "ymin": -3.2, "ymax": 3.2, "ybins": 80},
    "100cut_material_map" : {"xmin": 0., "xmax": 100., "xbins": 80, "ymin": -3.2, "ymax": 3.2, "ybins": 80},
    "150cut_material_map" : {"xmin": 0., "xmax": 100., "xbins": 80, "ymin": -3.2, "ymax": 3.2, "ybins": 80},
}
def deltaPhi(phi1, phi2):
	dphi = abs(phi1-phi2)
	if (dphi > math.pi):
		dphi = 2.*math.pi - dphi
	return dphi
				       
def deltaR(eta1, eta2, phi1, phi2):
	deltaEta = abs(eta1-eta2)
	deltaPhi = abs(phi1-phi2)
	dR = math.sqrt(deltaPhi*deltaPhi + deltaEta*deltaEta)

	return dR

def goodTrue(i):
    if event.truthLepton_pt[i] < PTCUT: return False
    if event.truthLepton_eta[i] > ETACUT: return False
    if abs(event.truthLepton_parentPdgId[it]) < 100000 : return False

    return True
    

def initializeLepHistos():
    h = {}
    for f in flav: 
        h[f] = {}
    
        for var in variables:
            hname = "h_%s_%s"%(var, f)
            h[f][var] = ROOT.TH1F(hname, hname, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            h[f][var].SetDirectory(0)

        for v2 in variables2d:
            hname = "h_%s_%s"%(v2, f)
            h[f][v2] = ROOT.TH2F(hname, hname, variables2d[v2]["xbins"], variables2d[v2]["xmin"], variables2d[v2]["xmax"],variables2d[v2]["ybins"], variables2d[v2]["ymin"], variables2d[v2]["ymax"])
            h[f][v2].SetDirectory(0)

    return h

def truthMatchElectron(i):
    if event.electron_truthMatchedBarcode[i] == -1: return False

    barcode = event.electron_truthMatchedBarcode[i]

    for ii in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_barcode[ii] == barcode and abs(event.truthLepton_parentPdgId[ii]) > 100000: return True

    return False

def truthMatchMuon(i):
    if event.muon_truthMatchedBarcode[i] == -1: return False

    barcode = event.muon_truthMatchedBarcode[i]

    for ii in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_barcode[ii] == barcode and abs(event.truthLepton_parentPdgId[ii]) > 100000: return True

    return False

inpdir = "/eos/user/l/lhoryn/highd0/prodVtxfix_signal/mc16_13TeV.399031.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_0p1ns/data-trees/DAOD_RPVLL.15429331._000002.pool.root.1.root"
tag = "100_0p1ns"
#inpdir = "/eos/user/l/lhoryn/highd0/120318/user.lhoryn.mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.120318_trees.root/user.lhoryn.16289020._000001.trees.root"
#tag = "500_1ns"
#inpdir = "/eos/user/l/lhoryn/highd0/120318/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.120718_trees.root/*"
#tag = "ttbar"


AODFiles = glob.glob(inpdir)
chain = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    chain.Add(filename)

# cross sections from here: /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt
# norm weight = (xs * kfactor * filter efficiency * lumi)/nevents
#normweight = (729.77 * 5.4384E-01 * 1.13975636159 * 100000)/chain.GetEntries() #ttbar
normweight = (  0.66465* 100000)/chain.GetEntries() # 100, .1ns
#normweight = (  0.001033* 100000)/chain.GetEntries() #500, 1ns 

h=initializeLepHistos()

nMisMatch = 0    


print "entries ", chain.GetEntries()
for event in chain:
    
    leadingLep = False
    subleadingLep = False
    nElecs = 0
    nMus = 0
    
    weight = normweight * event.mcEventWeight
    #weight = 1

    leadingLep = False
    subleadingLep = False
    nPassedReco = 0
        
    for ie in xrange(len(event.electron_pt)):

        if abs(event.electron_eta[ie]) < 2.5 and ( truthMatchElectron(ie) or tag == "ttbar"):
        #if abs(event.electron_eta[ie]) < 2.5 :
            h["e"]["all_reco_pt"].Fill(event.electron_pt[ie], weight)
            h["e"]["all_reco_d0"].Fill(event.electron_d0[ie], weight)

            if event.electron_pt[ie] > 50:
                h["e"]["50cut_reco_d0"].Fill(event.electron_d0[ie], weight)

            if event.electron_pt[ie] > 100:
                h["e"]["100cut_reco_d0"].Fill(event.electron_d0[ie], weight)
            
            if event.electron_pt[ie] > 150:
                h["e"]["150cut_reco_d0"].Fill(event.electron_d0[ie], weight)
    
     
    if len(event.muon_pt) != len(event.muon_IDtrack_d0):
       nMisMatch += 1
       continue 
    for im in xrange(len(event.muon_pt)):

        if abs(event.muon_eta[im]) < 2.5 and ( truthMatchMuon(im) or tag == "ttbar"):
            h["m"]["all_reco_pt"].Fill(event.muon_pt[im], weight)
            h["m"]["all_reco_d0"].Fill(event.muon_IDtrack_d0[im], weight)

            if event.muon_pt[im] > 50:
                h["m"]["50cut_reco_d0"].Fill(event.muon_IDtrack_d0[im], weight)

            if event.muon_pt[im] > 100:
                h["m"]["100cut_reco_d0"].Fill(event.muon_IDtrack_d0[im], weight)
            
            if event.muon_pt[im] > 150:
                h["m"]["150cut_reco_d0"].Fill(event.muon_IDtrack_d0[im], weight)

        
    for i in xrange(len(event.truthLepton_pt)):
        if abs(event.truthLepton_pdgId[i]) == 11: 
            #r = math.sqrt( event.truthLepton_VtxX[i]**2 + event.truthLepton_VtxY[i]**2 + event.truthLepton_VtxZ[i]**2)
            r = math.sqrt( event.truthLepton_VtxX[i]**2 + event.truthLepton_VtxY[i]**2 )
            phi = event.truthLepton_phi[i]
            if abs(event.truthLepton_parentPdgId[i]) > 100000 or tag == "ttbar":
                h["e"]["all_true_rxy"].Fill(r, weight)
                h["e"]["all_material_map"].Fill(r, phi, weight)
                if event.truthLepton_pt[i] > 50:
                    h["e"]["50cut_true_rxy"].Fill(r, weight)
                    h["e"]["50cut_material_map"].Fill(r, phi, weight)
                if event.truthLepton_pt[i] > 100:
                    h["e"]["100cut_true_rxy"].Fill(r, weight)
                    h["e"]["100cut_material_map"].Fill(r, phi, weight)
                    if r > 1 :
                        print "ELECTRON parent pdgid: %s parent barcode %s rxy %s d0 %s"%(event.truthLepton_parentPdgId[i], event.truthLepton_parentBarcode[i], r, event.truthLepton_d0[i]) 
                        h["e"]["100cut_1mm_parent_pdgid"].Fill(abs(event.truthLepton_parentPdgId[i]), weight)
                        h["e"]["100cut_1mm_parent_barcode"].Fill(event.truthLepton_parentBarcode[i], weight)
                        h["e"]["100cut_1mm_true_d0"].Fill(event.truthLepton_d0[i], weight)
        
                if event.truthLepton_pt[i] > 150:
                    h["e"]["150cut_true_rxy"].Fill(r, weight)
                    h["e"]["150cut_material_map"].Fill(r, phi, weight)
                    h["e"]["150cut_parent_pdgid"].Fill(abs(event.truthLepton_parentPdgId[i]), weight)
                    h["e"]["150cut_parent_barcode"].Fill(event.truthLepton_parentBarcode[i], weight)
        if abs(event.truthLepton_pdgId[i]) == 13: 
            #r = math.sqrt( event.truthLepton_VtxX[i]**2 + event.truthLepton_VtxY[i]**2 + event.truthLepton_VtxZ[i]**2)
            r = math.sqrt( event.truthLepton_VtxX[i]**2 + event.truthLepton_VtxY[i]**2 )
            phi = event.truthLepton_phi[i]
            if abs(event.truthLepton_parentPdgId[i]) > 100000 or tag == "ttbar":
                h["m"]["all_true_rxy"].Fill(r, weight)
                h["m"]["all_material_map"].Fill(r, phi, weight)
                if event.truthLepton_pt[i] > 50:
                    h["m"]["50cut_true_rxy"].Fill(r, weight)
                    h["m"]["50cut_material_map"].Fill(r, phi, weight)
                if event.truthLepton_pt[i] > 100:
                    h["m"]["100cut_true_rxy"].Fill(r, weight)
                    h["m"]["100cut_material_map"].Fill(r, phi, weight)
                    if r > 1 :
                        print "MUON parent pdgid: %s parent barcode %s rxy %s d0 %s"%(event.truthLepton_parentPdgId[i], event.truthLepton_parentBarcode[i], r, event.truthLepton_d0[i]) 
                        h["m"]["100cut_1mm_parent_pdgid"].Fill(abs(event.truthLepton_parentPdgId[i]), weight)
                        h["m"]["100cut_1mm_parent_barcode"].Fill(event.truthLepton_parentBarcode[i], weight)
                        h["m"]["100cut_1mm_promptparent_pdgid"].Fill(abs(event.truthLepton_promptParent_pdgId[i]), weight)
                if event.truthLepton_pt[i] > 150:
                    h["m"]["150cut_true_rxy"].Fill(r, weight)
                    h["m"]["150cut_material_map"].Fill(r, phi, weight)
                    h["m"]["150cut_parent_pdgid"].Fill(abs(event.truthLepton_parentPdgId[i]), weight)
                    h["m"]["150cut_parent_barcode"].Fill(event.truthLepton_parentBarcode[i], weight)




print "number of mismatch ", nMisMatch
outpfile = "../outputFiles/mistudies/"+tag+"_weighted_histos.root"
outFile = ROOT.TFile(outpfile, "RECREATE")
print "made ", outpfile
for f in flav:
    for var in h[f]:
        print "writing " + f + " " + var
        if h[f][var]: h[f][var].Write()
outFile.Write()

