import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

c = ROOT.TCanvas()

labels = { "nocut": "p_{T} > 20 GeV", "50cut": "p_{T} > 50 GeV", "65cut": "p_{T} > 65 GeV", "150cut": "p_{T} > 150 GeV", "signal":"signal"}

f_100_0p01ns = ROOT.TFile("../outputFiles/mistudies/100_0p1ns_weighted_histos.root")
#f_100_0p01ns = ROOT.TFile("../outputFiles/mistudies/500_1ns_weighted_histos.root")
#f_100_0p01ns = ROOT.TFile("../outputFiles/mistudies/ttbar_weighted_histos.root")
#tag = "ttbar"
tag = "100_0p1ns"


# draw pt
h_pt = f_100_0p01ns.Get("h_all_reco_pt_e")
h_pt.GetXaxis().SetTitle("p_{T} [GeV]")
h_pt.SetFillColor(ROOT.kAzure+8)


h_pt.Draw("hist")
ROOT.ATLASLabel(0.42,0.83, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.42,0.89, "electrons from "+tag )
ROOT.gPad.Update()
c.Update()
##raw_input("...")
c.SaveAs("../outputFiles/mistudies/plots/e_pt_"+tag+"_weighted.pdf")
c.SaveAs("../outputFiles/mistudies/plots/e_pt_"+tag+"_weighted.C")


#draw d0

c.Clear()

ROOT.gPad.SetLogy()
ROOT.gPad.Update()

h= {}
h["nocut"] = f_100_0p01ns.Get("h_all_reco_d0_e")
h["65cut"] = f_100_0p01ns.Get("h_65cut_reco_d0_e")

h["nocut"].SetFillColor(ROOT.kCyan)
h["65cut"].SetFillColor(ROOT.kCyan+2)

h["nocut"].GetXaxis().SetTitle("d_{0} [mm]")
h["nocut"].GetYaxis().SetTitle("Number of muons")
h["nocut"].GetYaxis().SetRangeUser(10**4,h["nocut"].GetMaximum()*10)

leg = ROOT.TLegend(0.60,0.74,0.80,0.92)
leg.AddEntry(h["nocut"], "p_{T} > 20 GeV", "f")
leg.AddEntry(h["65cut"], "p_{T} > 65 GeV", "f")


h["nocut"].Draw("hist")
h["65cut"].Draw("histsame")


leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "electrons from "+tag )

ROOT.gPad.SetLogy()
ROOT.gPad.Update()
c.Update()
#raw_input("...")
c.SaveAs("../outputFiles/mistudies/plots/e_d0_"+tag+"_weighted.pdf")
c.SaveAs("../outputFiles/mistudies/plots/e_d0_"+tag+"_weighted.C")



## draw radius of decay
c.Clear()


h= {}
h["nocut"] = f_100_0p01ns.Get("h_all_true_rxy_e")
h["65cut"] = f_100_0p01ns.Get("h_65cut_true_rxy_e")

h["nocut"].SetFillColor(ROOT.kCyan)
h["65cut"].SetFillColor(ROOT.kCyan+1)

h["nocut"].GetXaxis().SetTitle("true R_{xy} [mm]")
h["nocut"].GetYaxis().SetRangeUser(10**4,h["nocut"].GetMaximum()*10)

leg = ROOT.TLegend(0.60,0.74,0.80,0.92)
leg.AddEntry(h["nocut"], "p_{T} > 20 GeV", "f")
leg.AddEntry(h["65cut"], "p_{T} > 65 GeV", "f")

h["nocut"].Draw("hist")
h["65cut"].Draw("histsame")


leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "electrons from "+tag )

ROOT.gPad.SetLogy()
ROOT.gPad.Update()
c.Update()
#raw_input("...")
c.SaveAs("../outputFiles/mistudies/plots/e_rxy_"+tag+"_weighted.pdf")
c.SaveAs("../outputFiles/mistudies/plots/e_rxy_"+tag+"_weighted.C")


## material maps

c.Clear()

ROOT.gPad.SetLogy(0)
ROOT.gPad.SetLogz()
ROOT.gPad.Update()
h= {}
h["nocut"] = f_100_0p01ns.Get("h_all_material_map_e")
#h["50cut"] = f_100_0p01ns.Get("h_50cut_material_map_e")
h["65cut"] = f_100_0p01ns.Get("h_65cut_material_map_e")
#h["150cut"] = f_100_0p01ns.Get("h_150cut_material_map_e")
ROOT.gStyle.SetPalette(ROOT.kDeepSea)

for name in h:
    h[name].GetXaxis().SetTitle("R_{xy} [mm]")
    h[name].GetYaxis().SetTitle("#phi")
    h[name].GetZaxis().SetTitle("Number of muons")
    h[name].Draw("COLZ")
#    ROOT.ATLASLabel(0.55,0.88, "Internal")
#    text = ROOT.TLatex()
#    text.SetNDC()
#    text.DrawLatex(0.55,0.83, "electrons from "+tag )
#    text = ROOT.TLatex()
#    text.SetNDC()
#    text.DrawLatex(0.55,0.74, labels[name]  )

    ROOT.gPad.Update()
    c.Update()
    #raw_input("...")
    c.SaveAs("../outputFiles/mistudies/plots/e_mm_"+tag+"_"+name+"_weighted.pdf")
    c.SaveAs("../outputFiles/mistudies/plots/e_mm_"+tag+"_weighted.C")

