#!/usr/bin/env python

import math
import ROOT
from ROOT import *
import os,sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

#f = TFile(sys.argv[1])
#f = '/afs/cern.ch/work/t/tholmes/FTK/cluster_studies/mc15_13TeV.403220.MGPy8EG_A14N23LO_GG_mixedC1LLP_0p2_2000_600.recon.DAOD_IDTRKVALID.e5659_s2698_r8977/DAOD_IDTRKVALID.10365729._000008.pool.root.1'
#f = 'genIDTRKVALID/mc15_13TeV.402004.PythiaRhad_AUET2BCTEQ6L1_gen_gluino_p1_1600.merge.DAOD_IDTRKVALID.root'
#f = 'make_rel21_DAOD/500gev/user.tholmes.mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.DAOD_IDTRKVALID.root'
f = "/afs/cern.ch/work/t/tholmes/LLP/Trigger/data18_13TeV.00360026.physics_EnhancedBias.merge.AOD.r11637_r11638_p3989/AOD.19448522._000172.pool.root.1"
chain = ROOT.TChain("CollectionTree")
chain.Add(f)
print f

# For older releases
#file_ = ROOT.TFile(f, "READ")
#t = ROOT.xAOD.MakeTransientTree( file_ )
t = ROOT.xAOD.MakeTransientTree(chain, ROOT.xAOD.TEvent.kAthenaAccess)
import xAODRootAccess.GenerateDVIterators

auxDataCode = """
bool auxdataConstBool( const SG::AuxElement& el, const std::string& name ) {
   return el.auxdata< char >( name );
}
"""
ROOT.gInterpreter.Declare(auxDataCode)

variables = {
        "dEdx":     {"nbins": 30, "min": 0., "iblmax":15.,  "pixmax": 15.,  "accessor": "pixeldEdx"},
        "ToT":      {"nbins": 50, "min": 0., "iblmax":200., "pixmax": 500., "accessor": "ToT"},
        "sizeZ":    {"nbins": 10,  "min": 0., "iblmax":10.,  "pixmax": 10.,  "accessor": "sizeZ"},
        "sizeZsinTheta":{"nbins": 6,  "min": 0., "iblmax":6.,  "pixmax": 6.,  "accessor": "sizeZ"},
        "sizePhi":  {"nbins": 20,  "min": 0., "iblmax":20.,  "pixmax": 20.,  "accessor": "sizePhi"},
        "size":     {"nbins": 20,  "min": 0., "iblmax":40., "pixmax": 40., "accessor": "sizePhi*sizeZ"},
}

# Loop over events
for entry in xrange( t.GetEntries() ):
    t.GetEntry( entry )

    #if entry > 1: break

    #t.Print()
    printDebug = True if entry < 5 else False

    if entry % 100 == 0:
        print 'Processing event', entry

    for i_el, el in enumerate(t.Electrons):
        print el.pt()

exit()

# Set up histograms
hists = {}
hists1d = {}
for ptype in ["SUSY", "SM"]:
    hists[ptype] = {}
    hists1d[ptype] = {}
    for xvar in ["dEdx", "ToT", "sizePhi"]:
        hists[ptype][xvar] = {}
        for yvar in ["sizeZ", "sizePhi", "size", "sizeZsinTheta"]:
            hists[ptype][xvar][yvar] = {}
            hists1d[ptype][yvar] = {}
            for reg in ["IBL", "PIX", "ALL", "MIN", "MED"]:
                plotname = "%s_vs_%s_%s_%s"%(yvar, xvar, reg, ptype)
                x_min = variables[xvar]["min"]
                y_min = variables[yvar]["min"]
                x_bins = variables[xvar]["nbins"]
                y_bins = variables[yvar]["nbins"]
                if reg == "IBL":
                    x_max = variables[xvar]["iblmax"]
                    y_max = variables[yvar]["iblmax"]
                elif reg == "PIX":
                    x_max = variables[xvar]["pixmax"]
                    y_max = variables[yvar]["pixmax"]
                elif reg == "ALL":
                    x_max = .25*(3*variables[xvar]["pixmax"] + variables[xvar]["iblmax"])
                    y_max = .5*(3*variables[yvar]["pixmax"] + variables[yvar]["iblmax"])
                    y_bins = int(y_max)
                elif reg == "MIN" or reg == "MED":
                    x_max = variables[xvar]["pixmax"]
                    y_max = variables[yvar]["pixmax"]
                #print xvar, yvar, reg
                hists[ptype][xvar][yvar][reg] = ROOT.TH2F(plotname,plotname,x_bins,x_min,x_max,y_bins,y_min,y_max)
                plotname = "%s_%s_%s"%(yvar, reg, ptype)
                #if yvar=="size": y_max = 40; y_bins = 40
                #if yvar=="sizePhi": y_max = 15; y_bins = 15
                #if yvar=="sizeZ": y_max = 25; y_bins = 25
                hists1d[ptype][yvar][reg] = ROOT.TH1F(plotname,plotname,y_bins,y_min,y_max)

def goodBarcodes(barcodes):
    isgood = False
    for barcode in barcodes:
        if barcode<200000: isgood = True
    return isgood

count_passing_susy = 0
count_passing_sm = 0

for entry in xrange( t.GetEntries() ):
    t.GetEntry( entry )

    #if entry > 1: break

    #t.Print()
    printDebug = True if entry < 5 else False

    if entry % 100 == 0:
        print 'Processing event', entry

    for i_track, track in enumerate(t.InDetTrackParticles):
        pt = track.pt()
        dEdx = track.auxdataConst("float")("pixeldEdx")
        theta = track.auxdataConst("float")("theta")
        truth_link = track.auxdataConst("ElementLink<DataVector<xAOD::TruthParticle_v1, DataVector<xAOD::IParticle, DataModel_detail::NoBase> > >")("truthParticleLink")
        msos_link = track.auxdataConst("std::vector<ElementLink<DataVector<xAOD::TrackStateValidation_v1, DataModel_detail::NoBase> >, std::allocator<ElementLink<DataVector<xAOD::TrackStateValidation_v1, DataModel_detail::NoBase> > > >")("msosLink")

        # Make truth requirements
        try: pdgid = truth_link.auxdataConst("int")("pdgId")
        except: continue

        # Pt cut
        if pt<5000: continue

        # Get particle type
        ptype = "SM"
        if ( abs(pdgid)>=1000000 and abs(pdgid)<3000000 ): ptype = "SUSY"

        # Set up track quantities
        good_clusters = 0
        sum_size = 0
        sum_sizeZ = 0
        sum_sizePhi = 0
        sum_sizeZsinTheta = 0

        for msos in msos_link:
            cluster = msos.trackMeasurementValidationLink()

            try:
                ToT = cluster.auxdataConst("int")("ToT")
                layer = cluster.auxdataConst("int")("layer")
                sizeZ = cluster.auxdataConst("int")("sizeZ")
                sizePhi = cluster.auxdataConst("int")("sizePhi")
                bec = cluster.auxdataConst("int")("bec")
                barcodes = cluster.auxdataConst("vector<int>")("truth_barcode")
            except: continue
            sizeZsinTheta = sizeZ*abs(math.sin(theta))

            # Check that it came from some primary truth particles
            #if not goodBarcodes(barcodes): continue

            # Only barrel for now
            if bec!=0: continue

            good_clusters += 1
            sum_size += sizeZ*sizePhi
            sum_sizeZ += sizeZ
            sum_sizePhi += sizePhi
            sum_sizeZsinTheta += sizeZsinTheta

            if layer==0:
                hists[ptype]["sizePhi"]["sizeZ"]["IBL"].Fill(sizePhi, sizeZ)
                hists[ptype]["sizePhi"]["sizePhi"]["IBL"].Fill(sizePhi, sizePhi)
                hists[ptype]["sizePhi"]["size"]["IBL"].Fill(sizePhi, sizeZ*sizePhi)
                hists[ptype]["sizePhi"]["sizeZsinTheta"]["IBL"].Fill(sizePhi, sizeZsinTheta)
                hists[ptype]["ToT"]["sizeZ"]["IBL"].Fill(ToT, sizeZ)
                hists[ptype]["ToT"]["sizePhi"]["IBL"].Fill(ToT, sizePhi)
                hists[ptype]["ToT"]["size"]["IBL"].Fill(ToT, sizeZ*sizePhi)
                hists[ptype]["ToT"]["sizeZsinTheta"]["IBL"].Fill(ToT, sizeZsinTheta)
                hists[ptype]["dEdx"]["sizeZ"]["IBL"].Fill(dEdx, sizeZ)
                hists[ptype]["dEdx"]["sizePhi"]["IBL"].Fill(dEdx, sizePhi)
                hists[ptype]["dEdx"]["size"]["IBL"].Fill(dEdx, sizeZ*sizePhi)
                hists[ptype]["dEdx"]["sizeZsinTheta"]["IBL"].Fill(dEdx, sizeZsinTheta)
                hists1d[ptype]["sizeZ"]["IBL"].Fill(sizeZ)
                hists1d[ptype]["sizePhi"]["IBL"].Fill(sizePhi)
                hists1d[ptype]["size"]["IBL"].Fill(sizeZ*sizePhi)
                hists1d[ptype]["sizeZsinTheta"]["IBL"].Fill(sizeZsinTheta)
            else:
                hists[ptype]["sizePhi"]["sizeZ"]["PIX"].Fill(sizePhi, sizeZ)
                hists[ptype]["sizePhi"]["sizePhi"]["PIX"].Fill(sizePhi, sizePhi)
                hists[ptype]["sizePhi"]["size"]["PIX"].Fill(sizePhi, sizeZ*sizePhi)
                hists[ptype]["sizePhi"]["sizeZsinTheta"]["PIX"].Fill(sizePhi, sizeZsinTheta)
                hists[ptype]["ToT"]["sizeZ"]["PIX"].Fill(ToT, sizeZ)
                hists[ptype]["ToT"]["sizePhi"]["PIX"].Fill(ToT, sizePhi)
                hists[ptype]["ToT"]["size"]["PIX"].Fill(ToT, sizeZ*sizePhi)
                hists[ptype]["ToT"]["sizeZsinTheta"]["PIX"].Fill(ToT, sizeZsinTheta)
                hists[ptype]["dEdx"]["sizeZ"]["PIX"].Fill(dEdx, sizeZ)
                hists[ptype]["dEdx"]["sizePhi"]["PIX"].Fill(dEdx, sizePhi)
                hists[ptype]["dEdx"]["size"]["PIX"].Fill(dEdx, sizeZ*sizePhi)
                hists[ptype]["dEdx"]["sizeZsinTheta"]["PIX"].Fill(dEdx, sizeZsinTheta)
                hists1d[ptype]["sizeZ"]["PIX"].Fill(sizeZ)
                hists1d[ptype]["sizePhi"]["PIX"].Fill(sizePhi)
                hists1d[ptype]["size"]["PIX"].Fill(sizeZ*sizePhi)
                hists1d[ptype]["sizeZsinTheta"]["PIX"].Fill(sizeZsinTheta)

        # Fill full-track variables
        if good_clusters > 3:

            # Get acceptance based on some cuts
            if sum_sizeZsinTheta > (-2*sum_sizePhi + 22):
                if ptype == "SM": count_passing_sm += 1
                elif ptype == "SUSY": count_passing_susy += 1

            hists[ptype]["sizePhi"]["size"]["ALL"].Fill(sum_sizePhi, sum_size)
            hists[ptype]["sizePhi"]["sizeZ"]["ALL"].Fill(sum_sizePhi, sum_sizeZ)
            hists[ptype]["sizePhi"]["sizePhi"]["ALL"].Fill(sum_sizePhi, sum_sizePhi)
            hists[ptype]["sizePhi"]["sizeZsinTheta"]["ALL"].Fill(sum_sizePhi, sum_sizeZsinTheta)
            hists[ptype]["dEdx"]["size"]["ALL"].Fill(dEdx, sum_size)
            hists[ptype]["dEdx"]["sizeZ"]["ALL"].Fill(dEdx, sum_sizeZ)
            hists[ptype]["dEdx"]["sizePhi"]["ALL"].Fill(dEdx, sum_sizePhi)
            hists[ptype]["dEdx"]["sizeZsinTheta"]["ALL"].Fill(dEdx, sum_sizeZsinTheta)
            hists1d[ptype]["size"]["ALL"].Fill(sum_size)
            hists1d[ptype]["sizeZ"]["ALL"].Fill(sum_sizeZ)
            hists1d[ptype]["sizePhi"]["ALL"].Fill(sum_sizePhi)
            hists1d[ptype]["sizeZsinTheta"]["ALL"].Fill(sum_sizeZsinTheta)

            hists[ptype]["sizePhi"]["size"]["MIN"].Fill(sum_sizePhi, sum_size)
            hists[ptype]["sizePhi"]["sizeZ"]["MIN"].Fill(sum_sizePhi, sum_sizeZ)
            hists[ptype]["sizePhi"]["sizePhi"]["MIN"].Fill(sum_sizePhi, sum_sizePhi)
            hists[ptype]["sizePhi"]["sizeZsinTheta"]["MIN"].Fill(sum_sizePhi, sum_sizeZsinTheta)
            hists[ptype]["dEdx"]["size"]["MIN"].Fill(dEdx, sum_size)
            hists[ptype]["dEdx"]["sizeZ"]["MIN"].Fill(dEdx, sum_sizeZ)
            hists[ptype]["dEdx"]["sizePhi"]["MIN"].Fill(dEdx, sum_sizePhi)
            hists[ptype]["dEdx"]["sizeZsinTheta"]["MIN"].Fill(dEdx, sum_sizeZsinTheta)
            hists1d[ptype]["size"]["MIN"].Fill(sum_size)
            hists1d[ptype]["sizeZ"]["MIN"].Fill(sum_sizeZ)
            hists1d[ptype]["sizePhi"]["MIN"].Fill(sum_sizePhi)
            hists1d[ptype]["sizeZsinTheta"]["MIN"].Fill(sum_sizeZsinTheta)

            hists[ptype]["sizePhi"]["size"]["MED"].Fill(sum_sizePhi, sum_size)
            hists[ptype]["sizePhi"]["sizeZ"]["MED"].Fill(sum_sizePhi, sum_sizeZ)
            hists[ptype]["sizePhi"]["sizePhi"]["MED"].Fill(sum_sizePhi, sum_sizePhi)
            hists[ptype]["sizePhi"]["sizeZsinTheta"]["MED"].Fill(sum_sizePhi, sum_sizeZsinTheta)
            hists[ptype]["dEdx"]["size"]["MED"].Fill(dEdx, sum_size)
            hists[ptype]["dEdx"]["sizeZ"]["MED"].Fill(dEdx, sum_sizeZ)
            hists[ptype]["dEdx"]["sizePhi"]["MED"].Fill(dEdx, sum_sizePhi)
            hists[ptype]["dEdx"]["sizeZsinTheta"]["MED"].Fill(dEdx, sum_sizeZsinTheta)
            hists1d[ptype]["size"]["MED"].Fill(sum_size)
            hists1d[ptype]["sizeZ"]["MED"].Fill(sum_sizeZ)
            hists1d[ptype]["sizePhi"]["MED"].Fill(sum_sizePhi)
            hists1d[ptype]["sizeZsinTheta"]["MED"].Fill(sum_sizeZsinTheta)

print "SM pass:", count_passing_sm
print "SUSY pass:", count_passing_susy

outputFile = ROOT.TFile("cluster_plots.root", "RECREATE")
outputFile.cd()
for xvar in ["dEdx", "ToT", "sizePhi"]:
    for yvar in ["sizeZ", "sizePhi", "size", "sizeZsinTheta"]:
        for reg in ["IBL", "PIX", "ALL", "MIN", "MED"]:
            plotname = "%s_vs_%s_%s"%(yvar, xvar, reg)

            # Make ratio plot, weight SM and SUSY particles equally
            h = hists["SUSY"][xvar][yvar][reg].Clone(plotname+"_SigFrac")
            if h.Integral()>0: h.Scale(1./h.Integral())
            h_den = hists["SM"][xvar][yvar][reg].Clone(plotname+"_denom")
            if h_den.Integral()>0: h_den.Scale(1./h_den.Integral())
            h_den.Add(h)
            h.Divide(h_den)

            for hist in [h, hists["SM"][xvar][yvar][reg], hists["SUSY"][xvar][yvar][reg]]:
                can = ROOT.TCanvas("c_"+hist.GetName(), "c_"+hist.GetName())
                hist.GetXaxis().SetTitle(xvar)
                hist.GetYaxis().SetTitle(yvar)
                #if "SigFrac" not in hist.GetName(): can.SetLogz()
                hist.Draw("colz")
                #raw_input("...")
                can.SaveAs(hist.GetName()+".png")
                hist.Write()

for yvar in ["sizeZ", "sizePhi", "size", "sizeZsinTheta"]:
    for reg in ["IBL", "PIX", "ALL"]:
        h_sm =   hists1d["SM"][yvar][reg]
        h_susy = hists1d["SUSY"][yvar][reg]
        if h_sm.Integral()>0: h_sm.Scale(1./h_sm.Integral())
        if h_susy.Integral()>0: h_susy.Scale(1./h_susy.Integral())
        h_sm.SetMaximum(1.2*max([h_sm.GetMaximum(), h_susy.GetMaximum()]))

        can = ROOT.TCanvas("c_%s_%s"%(yvar, reg), "c_%s_%s"%(yvar, reg))
        h_sm.SetLineColor(ROOT.kRed-4)
        h_susy.SetLineColor(ROOT.kBlue-4)
        h_sm.GetXaxis().SetTitle(yvar)
        h_sm.SetMarkerSize(0)
        h_susy.SetMarkerSize(0)

        leg = ROOT.TLegend(.55,.6,.83,.8)
        leg.AddEntry(h_sm, "SM Particles")
        leg.AddEntry(h_susy, "3 TeV Rhadrons")

        h_sm.Draw("ehist")
        h_susy.Draw("ehist same")
        leg.Draw()

        can.SaveAs("compare_%s_%s.png"%(yvar, reg))
        h_sm.Write()
        h_susy.Write()
outputFile.Close()

