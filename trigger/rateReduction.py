# Goal: Study whether or not using objects in the photon container is useful

import collections
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../plot_helpers/math_helpers.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal"
d = "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v4/WorkArea/run/submit_dir/data-trees/AOD.19448522._000172.pool.root.1.root"
min_pt = 100

# Set variables
variables = {
        "events":   {"nbins": 1, "xmin": 0., "xmax": 1, "xlabel": "Events"},
        #"pt":       {"nbins": 25,    "xmin": 0.,     "xmax": 200.,     "xlabel": "p_{T} [GeV]"},
        #"d0":       {"nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "d_{0} [mm]"},
        }

# Set up histograms
hists={}
for l in ["e", "m"]:
    hists[l] = {}
    for v in variables:
        hists[l][v] = {}
        for t in ["all", "Pass L1", "Fail Prompt HLT", "Passes Pt Cut", "No Prompt Track", "Has LRT Track", "Has %d GeV Lepton"%min_pt]:
            hists[l][v][t] = ROOT.TH1F("h_%s_%s_%s"%(l,v,t), "h_%s_%s_%s"%(l,v,t), variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])

# Loop over events
tree = getTree(d)
n_cluster = 0
for i_event, event in enumerate(tree):
    if i_event%500==0: print "Processing event", i_event

    for v in variables:
        hists["e"][v]["all"].Fill(0)

        # Deal with electrons
        if not event.pass_L1_EM22VHI: continue
        hists["e"][v]["Pass L1"].Fill(0)

        # Other facts I would want to know
        # Location of RoI, E of calo cluster
        # Compare to ID tracks (have LRT or not already)

        has_cluster = False
        has_lrt_idtrack = False
        has_prompt_idtrack = False
        for i_clus in xrange(len(event.cluster_pt)):
            if event.cluster_pt[i_clus]<min_pt: continue
            if event.cluster_emProb[i_clus]<0.3: continue
            if abs(event.cluster_eta[i_clus])>2.5: continue
            has_cluster = True
            for i_trk in xrange(len(event.idTrack_pt)):
                if event.idTrack_pt[i_trk]<min_pt: continue
                dr = getDR(event.idTrack_phi[i_trk], event.cluster_phi[i_clus], event.idTrack_eta[i_trk], event.cluster_eta[i_clus])
                if dr > 0.2: continue
                if event.idTrack_isLRT[i_trk]: has_lrt_idtrack = True
                else: has_prompt_idtrack = True

        if not has_cluster: continue
        n_cluster += 1
        hists["e"][v]["Passes Pt Cut"].Fill(0)

        if has_prompt_idtrack: continue
        hists["e"][v]["No Prompt Track"].Fill(0)

        #if not has_lrt_idtrack: continue
        #hists["e"][v]["Has LRT Track"].Fill(0)

        if event.pass_HLT_e26_lhtight_nod0_ivarloose: continue
        hists["e"][v]["Fail Prompt HLT"].Fill(0)

        if event.electron_n>0 and event.electron_pt[0]>min_pt:
            hists["e"][v]["Has %d GeV Lepton"%min_pt].Fill(0)

    for v in variables:
        hists["m"][v]["all"].Fill(0)

        # Deal with muons
        if not event.pass_L1_MU20: continue
        hists["m"][v]["Pass L1"].Fill(0)

        # Other facts I would want to know
        # MS only track location, pT
        # Compare to ID tracks

        has_mst = False
        has_lrt_idtrack = False
        has_prompt_idtrack = False
        for i_mst in xrange(len(event.mstrack_pt)):
            if event.mstrack_pt[i_mst]<min_pt: continue
            if abs(event.mstrack_eta[i_mst])>2.5: continue
            has_mst = True
            for i_trk in xrange(len(event.idTrack_pt)):
                if event.idTrack_pt[i_trk]<min_pt: continue
                dr = getDR(event.idTrack_phi[i_trk], event.mstrack_phi[i_mst], event.idTrack_eta[i_trk], event.mstrack_eta[i_mst])
                if dr > 0.2: continue
                if event.idTrack_isLRT[i_trk]: has_lrt_idtrack = True
                else: has_prompt_idtrack = True

        if not has_mst: continue
        hists["m"][v]["Passes Pt Cut"].Fill(0)

        if has_prompt_idtrack: continue
        hists["m"][v]["No Prompt Track"].Fill(0)

        #if not has_lrt_idtrack: continue
        #hists["m"][v]["Has LRT Track"].Fill(0)

        if event.pass_HLT_mu26_ivarmedium: continue
        hists["m"][v]["Fail Prompt HLT"].Fill(0)

        if event.muon_n>0 and event.muon_pt[0]>min_pt:
            hists["m"][v]["Has %d GeV Lepton"%min_pt].Fill(0)

print "n with cluster", n_cluster

# Create histograms
ordered_hists = ["Pass L1", "Passes Pt Cut", "No Prompt Track", "Fail Prompt HLT", "Has %d GeV Lepton"%min_pt]
for l in hists:
    for v in variables:
        efflist = collections.OrderedDict()
        for hname in ordered_hists:
            eff = ROOT.TEfficiency(hists[l][v][hname], hists[l][v]["all"])
            eff.SetName("e_%s_%s_%s"%(l,v,hname))
            print l, v, hname
            eff.Draw()
            ROOT.gPad.Update()
            #raw_input("...")
            efflist[hname]=eff
        plotEfficiencies(efflist, "plots/h_%s_%s.pdf"%(l,v), xlabel="", ylabel="Fraction of Events")

