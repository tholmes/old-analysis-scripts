import glob
import ROOT
import os
import gc

gc.disable()
execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)
#ROOT.gStyle.SetPaintTextFormat(".5f");

dsids = [
        399006,
        399014,
        399030,
        399036,
        399047,
        ]

variables = {
        "pt":       {"nbins": 50,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Slepton p_{T} [GeV]", "acc": "TruthBSM.pt()*0.001" },
        #"d0":       {"nbins": 100,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Signal Lepton d_{0} [mm]"},
        "eta":      {"nbins": 80,    "xmin": -4,   "xmax": 4,       "xlabel": "Slepton #eta", "acc": "TruthBSM.eta()" },
        "phi":      {"nbins": 80,    "xmin": -3.2,   "xmax": 3.2,       "xlabel": "Slepton #phi", "acc": "TruthBSM.phi()" },
        #"rhit":     {"nbins": 200,    "xmin": 0.,   "xmax": 600.,       "xlabel": "Radius of First Hit"},
        #"npix":     {"nbins": 6,    "xmin": 0.,   "xmax": 6.,       "xlabel": "Number of Pixel Hits"},
        #"nsct":     {"nbins": 16,    "xmin": 0.,   "xmax": 16.,       "xlabel": "Number of SCT Hits"},
        #"nsi":      {"nbins": 22,    "xmin": 0.,   "xmax": 22.,       "xlabel": "Number of Silicon Hits"},
        #"nholes":   {"nbins": 12,    "xmin": -0.5,   "xmax": 12.5,     "xlabel": "Number of missing hits after first hit"},
        #"dpt":      {"nbins": 20,    "xmin": -1.,   "xmax": 4,          "xlabel": "Signal Lepton (p_{T}^{track}-p_{T})/p_{T}"},
        #"chi2":     {"nbins": 20,    "xmin": 0.,   "xmax": 10,          "xlabel": "Signal Lepton #chi^{2}"},
        #"fcloose":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCLoose"},
        #"fctight":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCTight"},
        }

# Read in histograms
hists = {}
for dsid in dsids:
    hists = {}
    hists[dsid] = {}
    fname = "hists_%d.root"%dsid
    f = ROOT.TFile(fname, "READ")
    keys = f.GetListOfKeys()
    for k in keys:
        h = k.ReadObj()
        hname = h.GetName()
        vtype = hname.split("_")[-1]
        var = hname.split("_")[1]
        if var not in hists[dsid]:
            hists[dsid][var]={}
        hists[dsid][var][vtype]=h

    # Make plots compared to nominal
    for var in hists[dsid]:
        for vtype in ["py1", "py2", "py3a", "py3b", "py3c", "qc", "sc"]:
            #if dsid == 399014 and var == "pt" and vtype == "py2": continue # This one keeps breaking, skipping for now
            try:
                hnom = hists[dsid][var]["nominal"].Clone()
                hup = hists[dsid][var][vtype+"up"]
                hdown = hists[dsid][var][vtype+"dw"]

                scaleHists([hnom,hup,hdown])
                for h in [hnom, hup, hdown]: h.Rebin(2)

                hnom.SetTitle("Nominal")
                hup.SetTitle("%s x 2"%vtype)
                hdown.SetTitle("%s / 2"%vtype)
                makeRatioPlot(hup, hnom, variables[var]["xlabel"], "plots/h_%d_%s_%s.pdf"%(dsid,var,vtype), logy = False, ytitle="", doEfficiency = False, h11=hdown, rmin=0.98, rmax=1.02)
            except Exception as e:
                print e
                print "Couldn't make plot for plots/h_%d_%s_%s.pdf"%(dsid,var,vtype)

    f.Close()

