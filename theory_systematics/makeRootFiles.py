#!/usr/bin/env python

import math
import ROOT
from ROOT import * #I'm sorry
import os,sys
#import xAODRootAccess.GenerateDVIterators

# Set up ROOT stuff
execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

#auxDataCode = """
#bool auxdataConstBool( const SG::AuxElement& el, const std::string& name ) {
#   return el.auxdata< char >( name );
#}
#"""
#ROOT.gInterpreter.Declare(auxDataCode)
variables = {
        "pt":       {"nbins": 50,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Signal Lepton p_{T} [GeV]", "acc": "TruthBSM.pt()*0.001" },
        #"d0":       {"nbins": 100,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Signal Lepton d_{0} [mm]"},
        "eta":      {"nbins": 80,    "xmin": -4,   "xmax": 4,       "xlabel": "Signal Lepton #eta", "acc": "TruthBSM.eta()" },
        "phi":      {"nbins": 80,    "xmin": -3.2,   "xmax": 3.2,       "xlabel": "Signal Lepton #phi", "acc": "TruthBSM.phi()" },
        #"rhit":     {"nbins": 200,    "xmin": 0.,   "xmax": 600.,       "xlabel": "Radius of First Hit"},
        #"npix":     {"nbins": 6,    "xmin": 0.,   "xmax": 6.,       "xlabel": "Number of Pixel Hits"},
        #"nsct":     {"nbins": 16,    "xmin": 0.,   "xmax": 16.,       "xlabel": "Number of SCT Hits"},
        #"nsi":      {"nbins": 22,    "xmin": 0.,   "xmax": 22.,       "xlabel": "Number of Silicon Hits"},
        #"nholes":   {"nbins": 12,    "xmin": -0.5,   "xmax": 12.5,     "xlabel": "Number of missing hits after first hit"},
        #"dpt":      {"nbins": 20,    "xmin": -1.,   "xmax": 4,          "xlabel": "Signal Lepton (p_{T}^{track}-p_{T})/p_{T}"},
        #"chi2":     {"nbins": 20,    "xmin": 0.,   "xmax": 10,          "xlabel": "Signal Lepton #chi^{2}"},
        #"fcloose":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCLoose"},
        #"fctight":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCTight"},
        }

# Figure out what files we have
syst_dir = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/theory/"
dsids = {}
for f in os.listdir(syst_dir):
    if len(f.split("."))<3: continue
    dsid = f.split(".")[2]
    var = f.split(".")[4]
    if dsid not in dsids: dsids[dsid] = {}
    dsids[dsid][var]={"file": f, "acc": -1, "hist": {}}
    for hvar in variables:
        dsids[dsid][var]["hist"][hvar] = ROOT.TH1F("h_%s_%s_%s"%(hvar,dsid,var), "h_%s_%s_%s"%(hvar,dsid,var), variables[hvar]["nbins"], variables[hvar]["xmin"], variables[hvar]["xmax"])

max_events = 10000
# Loop over the dsids for which we have nominal values
for dsid in sorted(dsids.keys()):
    if "nominal" not in dsids[dsid]: continue
    print "\n%s"%dsid

    for var in dsids[dsid]:

        try:
            chain = ROOT.TChain("CollectionTree")
            chain.Add(os.path.join(syst_dir, dsids[dsid][var]["file"]+"/*.root"))
            print dsids[dsid][var]["file"]
            t = ROOT.xAOD.MakeTransientTree(chain, ROOT.xAOD.TEvent.kAthenaAccess)
            tot_events = t.GetEntries()
            #irdf = ROOT.ROOT.RDataFrame(t)
        except:
            print "Couldn't load this tree"
            continue


        for hvar in variables:
            print "\tMaking hist for", hvar
            dsids[dsid][var]["hist"][hvar] = getHist(t,"h_%s_%s_%s"%(hvar,dsid,var),variables[hvar]["acc"],"%d,%f,%f"%(variables[hvar]["nbins"],variables[hvar]["xmin"],variables[hvar]["xmax"]))

        '''
        tot_events = min(tot_events, max_events)
        n_pass = 0
        event_i = 0
        for event in t:
            if event_i%1000==0:print "\tProcessing event", event_i
            if event_i>max_events: break
            for p in event.TruthBSM:
                dsids[dsid][var]["hist"]["pt"].Fill(p.pt()*.001)
                dsids[dsid][var]["hist"]["eta"].Fill(p.eta())
                dsids[dsid][var]["hist"]["phi"].Fill(p.phi())
            event_i += 1
        '''
        '''
        for event in t:
            n_signal_leptons = 0
            for l in event.TruthElectrons:
                if l.pt() > 65000: n_signal_leptons += 1
            for l in event.TruthMuons:
                if l.pt() > 65000: n_signal_leptons += 1
            if n_signal_leptons > 1: n_pass += 1
        dsids[dsid][var]["acc"]=1.*n_pass/tot_events
        print "\t", var, 1.*n_pass#/tot_events
        '''

    # Plot variations
    f = ROOT.TFile("hists_%s.root"%dsid, "RECREATE")
    for var in dsids[dsid]:
        for hvar in dsids[dsid][var]["hist"]:
            dsids[dsid][var]["hist"][hvar].Write()
    f.Close()

var_list = ["py1", "py2", "py3a", "py3b", "py3c", "qc", "sc"]
for dsid in sorted(dsids.keys()):
    print dsid
    for hvar in variables:
        c = ROOT.TCanvas("can_%s_%s"%(dsid, hvar), "can")
        dsids[dsid]["nominal"]["hist"][hvar].SetLineColor(theme_colors[0])
        dsids[dsid]["nominal"]["hist"][hvar].Draw()
        for i, var in enumerate(var_list):
            up_var = var+"up"
            down_var = var+"dw"
            if up_var not in dsids[dsid] or down_var not in dsids[dsid]: continue
            dsids[dsid][up_var]["hist"][hvar].SetLineColor(theme_colors[i+1])
            dsids[dsid][down_var]["hist"][hvar].SetLineColor(theme_colors[i+1])
            dsids[dsid][up_var]["hist"][hvar].SetLineStyle(3)
            dsids[dsid][down_var]["hist"][hvar].SetLineStyle(9)
            dsids[dsid][up_var]["hist"][hvar].Draw("same")
            dsids[dsid][down_var]["hist"][hvar].Draw("same")
        c.BuildLegend(.5,.65,.8,.90)
        c.SaveAs("plots/h_%s_%s.pdf"%(dsid, hvar))




exit()
# Make a table of results
print "\nPercent variations:"
for dsid in sorted(dsids.keys()):
    print dsid
    nom =  dsids[dsid]["nominal"]["acc"]
    print "\tnominal", nom
    for var in var_list:
        up_var = var+"up"
        down_var = var+"dw"
        if up_var not in dsids[dsid] or down_var not in dsids[dsid]: continue
        if dsids[dsid][up_var]["acc"]==-1 or dsids[dsid][down_var]["acc"]==-1: continue
        up_rel = 100.*(dsids[dsid][up_var]["acc"]-nom)/nom
        down_rel = 100.*(dsids[dsid][down_var]["acc"]-nom)/nom
        print "\t%s\tup: %4.2f\tdown: %4.2f"%(var, up_rel, down_rel)
