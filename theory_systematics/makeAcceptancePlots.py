import math
import glob
import ROOT
import os
import gc

gc.disable()
execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)
#ROOT.gStyle.SetPaintTextFormat(".5f");

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/"
sig_sel = "triggerRegion_pass && lepton_n_signal>1 && lepton_isSignal[0] && lepton_isSignal[1] && deltaR>0.2 && muon_n_cosmic==0"

variables = {
        "pt":       {"nbins": 25,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Slepton p_{T} [GeV]", "acc": "TruthBSM.pt()*0.001" },
        #"eta":      {"nbins": 40,    "xmin": -4,   "xmax": 4,       "xlabel": "Slepton #eta", "acc": "TruthBSM.eta()" },
        #"phi":      {"nbins": 40,    "xmin": -3.2,   "xmax": 3.2,       "xlabel": "Slepton #phi", "acc": "TruthBSM.phi()" },
        }

types = ["Stau", "Slep"]
masses = [50, 100, 200, 300, 400, 500, 600, 700, 800]
lifetimes = ["0p01ns", "0p1ns", "1ns"]
hists = {}

for t in ["Stau", "Slep"]:
    hists[t] = {}
    for m in masses:
        hists[t][m] = {}
        efficiencies = {}
        nl50 = 0
        ntot = 0
        for l in lifetimes:
            hists[t][m][l] = {}

            print "Working with sample mass:", m, "lifetime:", l
            fstring = "user.tholmes.*%s*_%d_0_%s*.root/*.root"%(t, m, l)
            tree = getTree(os.path.join(d, fstring))
            if tree==-1: continue

            binstr = getBinStr(variables["pt"])
            hists[t][m][l]["pass"] = getHist(tree, "h_pass_%s_%d_%s"%(t,m,l), "truthSparticle_pt", binstr, sig_sel)#, verbose=True)
            hists[t][m][l]["total"] = getHist(tree, "h_total_%s_%d_%s"%(t,m,l), "truthSparticle_pt", binstr)

            h_l50 = getHist(tree, "h_l50_%s_%d_%s"%(t,m,l), "truthSparticle_pt", binstr, "truthSparticle_pt<50")
            nl50 += h_l50.Integral(0, h_l50.GetNbinsX()+1)
            ntot += hists[t][m][l]["total"].Integral(0, h_l50.GetNbinsX()+1)

            efficiencies[l] = ROOT.TEfficiency(hists[t][m][l]["pass"], hists[t][m][l]["total"])

        try:
            nfail = ntot-nl50
            fracpass = 1.*nl50/ntot
            fracfail = 1.*nfail/ntot
            unc = fracpass * math.sqrt( fracfail**2 / nl50 + fracfail**2 / nfail )
            print "\tFrac <50 GeV:", fracpass, "+/-", unc

            #efficiencies["0p1ns"].Draw()
            #ROOT.gPad.Update()
            #raw_input()
            plotEfficiencies(efficiencies, "plots/e_%s_%d.pdf"%(t,m), variables["pt"]["xlabel"], "Signal Efficiency")
        except:
            pass
