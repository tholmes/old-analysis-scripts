import ROOT
import math
import glob
import os
import sys

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
#ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("cosmic_helpers.py")



maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


#####
## File and trees
#####
AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/2ndcos/*')
tree = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    tree.Add(filename)

##### 
## Formatting
##### 
ROOT.gStyle.SetPadLeftMargin(0.15);
ROOT.gStyle.SetPadBottomMargin(0.15);
ROOT.gStyle.SetPadRightMargin(0.05);
ROOT.gStyle.SetPadTopMargin(0.05);
ROOT.gStyle.SetPadTickX(1);
ROOT.gStyle.SetPadTickY(1);
ROOT.gStyle.SetTitleOffset(1.2, "xy");

#####
## Detector guide lines
####
guides = []
guides.append( ROOT.TEllipse(0.0, 0.0, 33.0, 33.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 50.5, 50.5) )
guides.append( ROOT.TEllipse(0.0, 0.0, 85.0, 85.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 125.0, 125.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 4500.0, 4500.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 6500.0, 6500.0) )
guides.append( ROOT.TEllipse(0.0, 0.0, 10000.0, 10000.0) )

for guide in guides:
    guide.SetFillStyle(0)
    guide.SetLineColor(ROOT.kGray)
    guide.SetLineStyle(1)
    guide.SetLineWidth(2)





for event in tree:
    
    if event.eventNumber == 263007427: print tree.GetCurrentFile().GetName()
    lines = []
    muon_lines = []
    
    ##### 
    ## Set up canvas
    ##### 
    scope = 10000
    c = ROOT.TCanvas(str(event.eventNumber), "", 0, 0, 10000, 10000)

    frame = c.DrawFrame( -scope, -scope, scope, scope)
    frame.SetLineStyle(0);
    frame.SetLineWidth(0);
    frame.SetMarkerStyle(0);
    frame.GetXaxis().SetTitleOffset(1.5);
    frame.GetYaxis().SetTitleOffset(1.8);
    frame.SetTitle(";x [mm];y [mm]");


    for guide in guides:
        guide.Draw("l+")

    x = event.PV_x 
    y = event.PV_y
    

    #####
    ## Muons
    ####
    icos = -1
    imu = -1
    if abs(event.muon_eta[0]) > 1.05 or abs(event.muon_eta[1]) > 1.05: 
        print "skipped ", event.eventNumber
        continue
    for im in xrange(len(event.muon_pt)):
        p = ROOT.TVector3()
        p.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
        
        ct = cosTag(event, im, .012 , .18)
        mv = materialVeto(event, im, innerMap, middleMap, outerMap)
        
        cosmic = ct or mv

        if cosmic: icos = im
        else: imu = im

        xpos = 200*math.cos(event.muon_phi[im])
        ypos = 200*math.sin(event.muon_phi[im])
         
        x_start = event.muon_IDtrack_d0[im]/(math.cos(event.muon_phi[im]))
        y_start = event.muon_IDtrack_d0[im]/(math.sin(math.pi - event.muon_phi[im]))
         
          
        line = ROOT.TGraph()
        line.SetPoint(0, x_start, y_start)
        line.SetPoint(1, x_start + xpos, y_start + ypos )
         
        if cosmic: 
            line.SetLineColor(ROOT.kRed)
        else:
            line.SetLineColor(ROOT.kRed+2)
        line.SetLineWidth(2)
        muon_lines.append(line)
        
        

    #####
    ## Other MS Segments
    #### 
    for ms in xrange(len(event.msSegment_x)):
        segment = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        
         
        skip = False
        for ms2 in xrange(len(event.muon_msSegment_x)):
            segment2 = ROOT.TVector3(event.msSegment_x[ms2], event.msSegment_y[ms2], event.msSegment_z[ms2])
            if abs(segment.DeltaR(segment2)) == 0: skip = True
        
        if skip: continue
        
        R = math.sqrt(event.msSegment_x[ms]**2 + event.msSegment_y[ms]**2)
        
        line = ROOT.TGraph()
        xpos = event.msSegment_x[ms]
        ypos = event.msSegment_y[ms]
        
        line.SetPoint(0, xpos, ypos)
        #line.SetPoint(1, (R+300)*xpos, (R+300)*ypos)
         
        line.SetLineColor(ROOT.kBlue)
        line.SetMarkerColor(ROOT.kBlue)
        line.SetLineWidth(1)
        
        if abs(event.msSegment_z[ms]) > 6000: line.SetMarkerStyle(24) 
        else: line.SetMarkerStyle(8)

        line.SetMarkerSize(15)
        
        
        lines.append(line)
   
    #####
    ## MS segments attached to muons
    ####
    for ms in xrange(len(event.muon_msSegment_x)):
        segment = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        
        R = math.sqrt(event.msSegment_x[ms]**2 + event.msSegment_y[ms]**2)
         
        line = ROOT.TGraph()
        xpos = event.msSegment_x[ms]
        ypos = event.msSegment_y[ms]
        line.SetPoint(0, xpos, ypos)
        #line.SetPoint(1, xpos + 500*math.cos(segment.Phi()), ypos + 500*math.sin(segment.Phi()))
        
        if event.muon_msSegment_muonIndex[ms] == icos: 
            line.SetLineColor(ROOT.kRed)
            line.SetMarkerColor(ROOT.kRed)
        else:
            line.SetLineColor(ROOT.kRed+2)
            line.SetMarkerColor(ROOT.kRed+2)
        line.SetLineWidth(3)
        
        if abs(event.msSegment_z[ms]) > 6000:  
            line.SetMarkerStyle(28)
            if event.muon_msSegment_muonIndex[ms] == icos:
                print "endcap ms segment!"
        else: line.SetMarkerStyle(34)

        line.SetMarkerSize(15)
        
        lines.append(line)

    
    #####
    ## Draw Muons
    ####
    for line in muon_lines:
        line.Draw("l+")
    for line in lines:
        line.Draw("p+")
     
    #####
    ## Draw PV
    #####
    pv = ROOT.TEllipse(x, y, x+5, y+5)
    pv.SetFillStyle(1001)
    pv.SetFillColor(ROOT.kBlack)
    pv.SetLineColor(ROOT.kBlack)
    pv.Draw("+")
    
    #####
    ## Text
    #####
    latex = ROOT.TLatex()
    latex.SetTextAlign(12)
    latex.SetTextFont(42)
    latex.SetTextSize(0.02)
    latex.DrawLatexNDC(0.18, 0.91, "untagged d0 %3.3f"%(abs(event.muon_IDtrack_d0[imu])) );
    latex.DrawLatexNDC(0.18, 0.86, "cosmic d0 %3.3f"%(abs(event.muon_IDtrack_d0[icos])) );
    latex.DrawLatexNDC(0.18, 0.81, "Event number " + str(event.eventNumber ) );


    ROOT.gPad.Modified()
    ROOT.gPad.Update()

    ROOT.gPad.SaveAs("outputPlots/EventDisplays/susy15_"+ str(event.eventNumber) + ".pdf")
