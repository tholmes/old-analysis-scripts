import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "1cos"
#what = "SR"
#what = "SR_nochi2"
what = "1cos_narrow_nochi2"


print "1cos" in what
#full
#dEta = 0.018
#dPhi = 0.25

#narrower
dEta = 0.013
dPhi = 0.018
 
tag = ""+what+"_v4"
#basename= dirname + 'data_wip/Data/*'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data1*.root'
#basename= '/afs/cern.ch/user/e/eressegu/public/ntuples/data*.root'

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

bound = 30
nbins = 6

#make Rgood
names = { 
    "t0_1mu_top_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi>0, good)", "log": 1},   
    "t0_1mu_bot_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi<0, good)", "log": 1},   
    
    "t0_1mu_bot_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi<0,  bad)", "log": 1},
    "t0_1mu_bot_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi<0, bad", "log": 1},
    "t0_1mu_bot_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi<0, bad)", "log": 1},
    "t0_1mu_bot_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi<0, bad)", "log": 1},
    
    "t0_1mu_top_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi>0, bad)", "log": 1},
    "t0_1mu_top_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi>0, bad)", "log": 1},
    "t0_1mu_top_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi>0, bad)", "log": 1},
    "t0_1mu_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, #phi>0, bad)", "log": 1},
    
    "t0_2mu_top_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi>0, bad)", "log": 1},
    "t0_2mu_top_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi>0, bad)", "log": 1},
    "t0_2mu_top_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi>0, bad)", "log": 1},
    "t0_2mu_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi>0, bad)", "log": 1},
    
    "t0_2mu_bot_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi<0, bad)", "log": 1},
    "t0_2mu_bot_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi<0, bad)", "log": 1},
    "t0_2mu_bot_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi<0, bad)", "log": 1},
    "t0_2mu_bot_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, #phi<0, bad)", "log": 1},
    
    "t0_2mu_good_lead": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu good, lead #mu)", "log": 1}, ###ONLY FOR VALIDATION
    "t0_2mu_good_sublead": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu good, sublead #mu)", "log": 1}, ###ONLY FOR VALIDATION
}

names_2d = {
    "2mu_phi_phi_bot_bad"       : { "nb_x": 8, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu 0}", "nb_y": 8, "low_y": -4, "high_y": 4, "label_y": "#phi_{#mu 1}", "log":0},
    "2mu_phi_phi_top_bad"       : { "nb_x": 8, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu 0}", "nb_y": 8, "low_y": -4, "high_y": 4, "label_y": "#phi_{#mu 1}", "log":0},
    "2mu_phi_phi_two_good"       : { "nb_x": 8, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu 0}", "nb_y": 8, "low_y": -4, "high_y": 4, "label_y": "#phi_{#mu 1}", "log":0},
}
h = {}
h = setHistos("", names)

h2 = {}
h2 = setHistos2D("", names_2d)


print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100000: break



    ####### 1 mu region
    if len(event.muon_pt) == 1 and len(event.lepton_pt) == 1: 
        ctag, ms_cos = cosTag(event, 0, dEta, dPhi)
        matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap)
        
        cosmic = ctag or matVeto
        
        if preselect_muon_basechi2(event, 0, doQual=False)  and not cosmic: 

            t0 = t_avg(event, 0) 
            
            phi_layers = phi_lays(event,0) 
            if event.muon_CBtrack_chi2[0] < 3 and event.muon_MStrack_nPres[0] >= 3 and phi_layers > 0: 
                if event.muon_phi[0] > 0: h["t0_1mu_top_good"].Fill(t0)
                else: h["t0_1mu_bot_good"].Fill(t0)
            else:
                # OR all quality vars
                if (event.muon_CBtrack_chi2[0] > 3 or event.muon_MStrack_nPres[0] < 3 or phi_layers < 1):
                    if event.muon_phi[0] > 0: h["t0_1mu_top_bad"].Fill(t0)
                    else: h["t0_1mu_bot_bad"].Fill(t0)
                #only chi2 is "bad"
                if (event.muon_CBtrack_chi2[0] > 3): 
                    if event.muon_phi[0] > 0: h["t0_1mu_top_bad_chi2"].Fill(t0)
                    else: h["t0_1mu_bot_bad_chi2"].Fill(t0)
                
                #only nPres is "bad"
                if (event.muon_MStrack_nPres[0] < 3): 
                    if event.muon_phi[0] > 0: h["t0_1mu_top_bad_nPres"].Fill(t0)
                    else: h["t0_1mu_bot_bad_nPres"].Fill(t0)
                
                #only nPhiLays is "bad"
                if (phi_layers < 1): 
                    if event.muon_phi[0] > 0: h["t0_1mu_top_bad_nPhiLays"].Fill(t0)
                    else: h["t0_1mu_bot_bad_nPhiLays"].Fill(t0)
    
    
    
    
    
    
    
    ###### 2 mu region
    elif len(event.muon_pt) >1 and len(event.lepton_pt) >1:
       

        mu_props = {}
        mu_props[0] = {}
        mu_props[1] = {}

        mu_props[0]["good"] = preselect_muon(event, 0)
        mu_props[1]["good"] = preselect_muon(event, 1)
        
        mu_props[0]["t0"] = t_avg(event, 0)
        mu_props[1]["t0"] = t_avg(event, 1)

        #at least one muon is good
        if mu_props[0]["good"] or mu_props[1]["good"]:
            
            ctag, ms_cos = cosTag(event, 0, dEta, dPhi)
            matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap)
            mu_props[0]["cosmic"] = ctag or matVeto
            
            ctag, ms_cos = cosTag(event, 1, dEta, dPhi)
            matVeto = materialVeto(event, 1, innerMap, middleMap, outerMap)
            mu_props[1]["cosmic"] = ctag or matVeto
            
            #fiducial selection based on region
            if ("SR" in what and not mu_props[0]["cosmic"] and not mu_props[1]["cosmic"]) or  ("1cos" in what and mu_props[0]["cosmic"] != mu_props[1]["cosmic"]) :  
            
                ### t0 distributuions of 2 good muon region if not in SR!
                if "SR" not in what and mu_props[0]["good"] and mu_props[1]["good"]:
                    h["t0_2mu_good_lead"].Fill(mu_props[0]["t0"])
                    h["t0_2mu_good_sublead"].Fill(mu_props[1]["t0"])
                    h2["2mu_phi_phi_two_good"].Fill(event.muon_phi[0],event.muon_phi[1])
                    
                
                ##### only one good muon in VR and SR 
                elif mu_props[0]["good"] != mu_props[1]["good"]:
                    igood = 1 
                    ibad = 0 
                    if mu_props[0]["good"]:
                        igood = 0
                        ibad = 1
                        
                    if preselect_muon_basechi2(event, ibad, doQual=False):
                            
                        phi_layers = phi_lays(event,ibad)

                        # OR all quality vars
                        if (event.muon_CBtrack_chi2[ibad] > 3 or event.muon_MStrack_nPres[ibad] < 3 or phi_layers < 1):
                            if event.muon_phi[ibad] > 0:
                                #print
                                #print "2mu bad top muon!"
                                #print event.eventNumber
                                #print event.muon_phi[0]
                                #print event.muon_phi[1]
                                #print mu_props[ibad]["t0"]
                                #print mu_props[igood]["t0"]

                                h["t0_2mu_top_bad"].Fill(mu_props[ibad]["t0"])
                                h2["2mu_phi_phi_top_bad"].Fill(event.muon_phi[0],event.muon_phi[1])
                            else: 
                                #print
                                #print "2mu bad bottom muon!"
                                #print event.eventNumber
                                #print event.muon_phi[0]
                                #print event.muon_phi[1]
                                #print mu_props[ibad]["t0"]
                                #print mu_props[igood]["t0"]
                                h["t0_2mu_bot_bad"].Fill(mu_props[ibad]["t0"])
                                h2["2mu_phi_phi_bot_bad"].Fill(event.muon_phi[0],event.muon_phi[1])
                        #only chi2 is "bad"
                        if (event.muon_CBtrack_chi2[ibad] > 3):
                            if event.muon_phi[ibad] > 0: h["t0_2mu_top_bad_chi2"].Fill(mu_props[ibad]["t0"])
                            else: h["t0_2mu_bot_bad_chi2"].Fill(mu_props[ibad]["t0"])
                                 
                        #only nPres is "bad"
                        if (event.muon_MStrack_nPres[ibad] < 3): 
                            if event.muon_phi[ibad] > 0: h["t0_2mu_top_bad_nPres"].Fill(mu_props[ibad]["t0"])
                            else: h["t0_2mu_bot_bad_nPres"].Fill(mu_props[ibad]["t0"])
                        
                        #only nPhiLays is "bad"
                        if (phi_layers < 1): 
                            if event.muon_phi[ibad] > 0: h["t0_2mu_top_bad_nPhiLays"].Fill(mu_props[ibad]["t0"])
                            else: h["t0_2mu_bot_bad_nPhiLays"].Fill(mu_props[ibad]["t0"])


outFile = ROOT.TFile("outputFiles/estimate_fake_noangle_"+tag+".root", "RECREATE")

c = ROOT.TCanvas()
for histo in h:
    #h[histo] = addOverflowBin(h[histo])
    #h[histo] = addUnderflowBin(h[histo])

    h[histo].Write()
    h[histo].Draw("hist e")
    c.SaveAs("outputPlots/estimate_fake/noangle_" + tag +"_"+ histo +".pdf")

for histo in h2:
    #h[histo] = addOverflowBin(h[histo])
    #h[histo] = addUnderflowBin(h[histo])

    h2[histo].Write()
    h2[histo].Draw("COLZ")
    c.SaveAs("outputPlots/estimate_fake/noangle_" + tag +"_"+ histo +".pdf")

outFile.Write()
outFile.Close()
