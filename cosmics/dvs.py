import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("cosmic_helpers.py")
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")

tag = "data_dv"

AODFiles = glob.glob('/eos/user/t/tholmes/llp/v3.5_Jan2020_DVs/Data/data18.root')
t_cos = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t_cos.Add(filename)


maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names = { 
    "cos_dv_m": { "nb": 50, "low": 0, "high": 4000, "label": "mass_{DV}", "log": 1},   
    "cos_dv_ntrk": { "nb": 10, "low": 0, "high": 10, "label": "N_{DV tracks}", "log": 1},   
    "cos_dv_ntrk_ptg50": { "nb": 10, "low": 0, "high": 10, "label": "N_{DV tracks} (p_{T} > 50 GeV)", "log": 1},   
    "cos_dv_R": { "nb": 50, "low": 0, "high": 1000, "label": "R_{DV, xy}", "log": 1}, 
    "cos_2trk_dphi": { "nb": 50, "low": 0, "high": 4, "label": "#Delta #phi (2 track DV)", "log": 1}, 
    "cos_2trk_deta": { "nb": 50, "low": 0, "high": 5, "label": "#Sigma #eta (2 track DV)", "log": 1}, 
    "cos_2trk_dR": { "nb": 50, "low": 0, "high": 6, "label": "#Delta R (2 track DV)", "log": 1}, 
    "cos_2trk_dR_zoom": { "nb": 50, "low": 0, "high": 1, "label": "#Delta R (2 track DV)", "log": 1}, 
      
    "mu_dv_m": { "nb": 50, "low": 0, "high": 4000, "label": "mass_{DV}", "log": 1},   
    "mu_dv_ntrk": { "nb": 10, "low": 0, "high": 10, "label": "N_{DV tracks}", "log": 1},   
    "mu_dv_ntrk_ptg50": { "nb": 10, "low": 0, "high": 10, "label": "N_{DV tracks} (p_{T} > 50 GeV)", "log": 1},   
    "mu_dv_R": { "nb": 50, "low": 0, "high": 1000, "label": "R_{DV, xy}", "log": 1},   
    "mu_2trk_dphi": { "nb": 50, "low": 0, "high": 4, "label": "#Delta #phi (2 track DV)", "log": 1}, 
    "mu_2trk_deta": { "nb": 50, "low": 0, "high": 5, "label": "#Sigma #eta (2 track DV)", "log": 1}, 
    "mu_2trk_dR": { "nb": 50, "low": 0, "high": 6, "label": "#Delta R (2 track DV)", "log": 1}, 
    "mu_2trk_dR_zoom": { "nb": 50, "low": 0, "high": 1, "label": "#Delta R (2 track DV)", "log": 1}, 
}

names_2d ={
    "cos_sEta_dPhi" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta (cosmic, track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (cosmic, track)", "log":0},
    "mu_sEta_dPhi" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta (muon, track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (muon, track)", "log":0},
    "mu_dv_ntrk_ptg50_all" : { "nb_x": 12, "low_x": 0, "high_x": 12, "label_x": "N_{DV tracks}", "nb_y": 12, "low_y": 0, "high_y": 12, "label_y": "N_{DV tracks, p_{T} > 50 GeV}", "log":0},
    "cos_dv_ntrk_ptg50_all" : { "nb_x": 12, "low_x": 0, "high_x": 12, "label_x": "N_{DV tracks}", "nb_y": 12, "low_y": 0, "high_y": 12, "label_y": "N_{DV tracks, p_{T} > 50 GeV}", "log":0},
}



h = {}
h = setHistos(tag, names)
h2 = {}
h2 = setHistos2D(tag, names_2d)



def getDVTrack(event, im):
    #print len(event.dvtrack_pt)
    for it in xrange(len(event.dvtrack_pt)):
        #print event.dvtrack_pt[it], event.muon_IDtrack_pt[im]
        if event.dvtrack_pt[it] == event.muon_IDtrack_pt[im]:
            return it
    
    return -1



print t_cos.GetEntries()

nevents = 0
nMuons = 0
nDVs = 0
for event in t_cos:
    if nevents % 5000 == 0: print "processing ", nevents
#    if nevents > 20000: break  
    nevents+=1
    try:
        event.muon_pt
        event.dvtrack_DVIndex
    except:
        continue


    if len(event.muon_pt) != 1: continue
    if len(event.electron_pt) != 0: continue
    
    if not (event.muon_isBaseline[0] and event.muon_isGoodQual[0]): continue
    cosmic = not event.muon_isNotCosmic[0]

    nMuons+=1 
    #if not event.muon_isNotCosmic[0]: continue
    # find dv track that matches muon track
    itrk = getDVTrack(event, 0)
    if itrk == -1: continue
    nDVs += 1
        
    ptg50_trks = 0
    trk_inds = []
    trk_vecs = []
    for it in xrange(len(event.dvtrack_pt)):
        if event.dvtrack_DVIndex[itrk] != event.dvtrack_DVIndex[itrk]: continue
        
        tk = ROOT.TVector3()
        tk.SetPtEtaPhi(event.dvtrack_pt[it], event.dvtrack_eta[it], event.dvtrack_phi[it])

        trk_inds.append(it)
        trk_vecs.append(tk)
     
        if event.dvtrack_pt > 50: ptg50_trks += 1
    
    
    if event.DV_nTracks[event.dvtrack_DVIndex[itrk]] == 2:
        if trk_vecs[0].Pt() > 50 and trk_vecs[1] > 50: 
            dphi = abs( abs(trk_vecs[0].DeltaPhi(trk_vecs[1])) - math.pi)
            seta = abs(trk_vecs[0].Eta() + trk_vecs[1].Eta())
            dR = math.sqrt( dphi**2 + seta**2)
            
            if cosmic:
                h["cos_2trk_dphi"].Fill(dphi)
                h["cos_2trk_deta"].Fill(seta)
                h["cos_2trk_dR"].Fill(dR)
                h["cos_2trk_dR_zoom"].Fill(dR)
                h2["cos_sEta_dPhi"].Fill(seta,dphi)
            else:
                h["mu_2trk_dphi"].Fill(dphi)
                h["mu_2trk_deta"].Fill(seta)
                h["mu_2trk_dR"].Fill(dR)
                h["mu_2trk_dR_zoom"].Fill(dR)
                h2["mu_sEta_dPhi"].Fill(seta,dphi)
    
    if cosmic:
        h["cos_dv_m"].Fill(event.DV_m[event.dvtrack_DVIndex[itrk]])  
        h["cos_dv_ntrk"].Fill(event.DV_nTracks[event.dvtrack_DVIndex[itrk]])  
        h["cos_dv_R"].Fill(event.DV_rxy[event.dvtrack_DVIndex[itrk]])  
        h["cos_dv_ntrk_ptg50"].Fill(ptg50_trks)  
        h2["cos_dv_ntrk_ptg50_all"].Fill(len(event.dvtrack_pt), ptg50_trks)  
    else:
        h["mu_dv_m"].Fill(event.DV_m[event.dvtrack_DVIndex[itrk]])  
        h["mu_dv_ntrk"].Fill(event.DV_nTracks[event.dvtrack_DVIndex[itrk]])  
        h["mu_dv_R"].Fill(event.DV_rxy[event.dvtrack_DVIndex[itrk]])  
        h["mu_dv_ntrk_ptg50"].Fill(ptg50_trks)  
        h2["mu_dv_ntrk_ptg50_all"].Fill(len(event.dvtrack_pt), ptg50_trks)  

print "n Muons ", nMuons
print "nDVs", nDVs
 
c = ROOT.TCanvas()
outFile = ROOT.TFile("outputFiles/dvs.root", "RECREATE")

for histo in h:

    h[histo] = addOverflow(h[histo])
    '''
    if names[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    '''
    h[histo].Draw("hist")
    h[histo].Write()
    c.SaveAs("outputPlots/DVs/"+ histo + ".pdf")

for histo in h2:
    #h[histo] = addOverflow2D(h2[histo])
    h2[histo].Draw("COLZ")
    c.SaveAs("outputPlots/DVs/"+ histo + ".pdf")
    h2[histo].Write()

outFile.Write()

    
toplot = ["dv_m", "dv_ntrk", "dv_R","dv_ntrk_ptg50", "2trk_dphi", "2trk_deta", "2trk_dR"]
for p in toplot:
    c.Clear()

    h_max = max(h["mu_"+p].GetMaximum(), h["cos_"+p].GetMaximum())
    
    h["mu_"+p].SetLineColor(ROOT.kRed)
    h["mu_"+p].GetYaxis().SetRangeUser(0,h_max+5)
    h["mu_"+p].Draw("hist")
    h["cos_"+p].Draw("hist same")
    
    leg = ROOT.TLegend(0.65,0.8,0.8,0.9)
    leg.AddEntry(h["mu_"+p],"muon","l")
    leg.AddEntry(h["cos_"+p],"cosmic","l")
    leg.Draw("same") 
    c.SaveAs("outputPlots/DVs/comp_"+p+".pdf")
    
