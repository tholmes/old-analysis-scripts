import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "1cos"
#what = "SR"
#what = "SR_nochi2_t00overflow"
#what = "1cos_narrow"
what = "SR"
#what = "1cos_narrow_nochi2"



print "1cos" in what
 
tag = ""+what+"_v5"
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data/merged/data1*.root'


print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

#bound = 40
bound = 30
nbins = 6 
names = { 
    "t0_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, good)", "log": 1},   
    
    "t0_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_top_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    
    "t0_top_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} (2#mu, top, good)", "log": 1}, ###ONLY FOR VALIDATION
}

h = {}
h = setHistos("", names)


mysteryNums = [4253530762, 646388285, 722317128, 765842373, 926487021]

muon_pass_noqual = "event.muon_isSRcuts[%i] == 1 and event.muon_isIsolated[%i] == 1 and event.muon_isPassIDtrk[%i] == 1 and event.muon_CBtrack_chi2[%i] < 3"
muon_pass = "event.muon_isSRcuts[%i] == 1 and event.muon_isIsolated[%i] == 1 and event.muon_isPassIDtrk[%i] == 1 and event.muon_isGoodQual[%i] == 1"



print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100000: break
        
    
    if event.eventNumber in mysteryNums:
        print event.eventNumber
        print len(event.muon_pt)
        print len(event.lepton_pt)
        print event.deltaR
        print "mu 0"
        print "signal", event.muon_isSignal[0]
        print "sr cuts", event.muon_isSRcuts[0]
        print "isolated", event.muon_isIsolated[0]
        print "id track", event.muon_isPassIDtrk[0]
        print "good qual", event.muon_isGoodQual[0]
        print "cb track", event.muon_CBtrack_chi2[0]
        print "ctag nar", event.muon_cosmicTag_narrow[0]
        print "ctag full", event.muon_cosmicTag_full[0]
        print "ctag mv", event.muon_cosmicTag_MV[0]
        print "ntup t0", event.muon_t0avg[0]
        print "calc t0", t_avg(event,0)
        print "phi", event.muon_phi[0]
        print "mu 1"
        print "signal", event.muon_isSignal[1]
        print "sr cuts", event.muon_isSRcuts[1]
        print "isolated", event.muon_isIsolated[1]
        print "id track", event.muon_isPassIDtrk[1]
        print "good qual", event.muon_isGoodQual[1]
        print "cb track", event.muon_CBtrack_chi2[1]
        print "ctag nar", event.muon_cosmicTag_narrow[1]
        print "ctag full", event.muon_cosmicTag_full[1]
        print "ctag mv", event.muon_cosmicTag_MV[1]
        print "ntup t1", event.muon_t0avg[1]
        print "calc t1", t_avg(event,1)
        print "phi", event.muon_phi[1]

    #1 muon histos
    if len(event.muon_pt) == 1 and len(event.lepton_pt) == 1: 
       
        # no cosmic tags
        if "narrow" in what:
            cosmic = event.muon_cosmicTag_narrow[0] or event.muon_cosmicTag_MV[0]
        else: 
            cosmic = event.muon_cosmicTag_full[0] or event.muon_cosmicTag_MV[0]

        if eval(muon_pass_noqual%(0,0,0,0)) and event.muon_phi[0] > 0 and not cosmic: 

            if event.muon_isGoodQual[0]: 
                h["t0_good"].Fill(event.muon_t0avg[0])
            # OR all quality vars
            else:
                if (event.muon_MStrack_nPres[0] < 3 or event.muon_nPhiLays[0] < 1):
                    h["t0_bad"].Fill(event.muon_t0avg[0])
                
                #only nPres is "bad"
                if (event.muon_MStrack_nPres[0] < 3): 
                    h["t0_bad_nPres"].Fill(event.muon_t0avg[0])
                
                #only nPhiLays is "bad"
                if (event.muon_nPhiLays[0] < 1): 
                    h["t0_bad_nPhiLays"].Fill(event.muon_t0avg[0])
    
    elif len(event.muon_pt) > 1 and len(event.lepton_pt) > 1 and len(event.muon_pt) == len(event.lepton_pt):

        if event.deltaR != -999 and abs(event.deltaR) > 0.2:
            
            if "narrow" in what:
                cosmic_0 = event.muon_cosmicTag_narrow[0] or event.muon_cosmicTag_MV[0] 
                cosmic_1 = event.muon_cosmicTag_narrow[1] or event.muon_cosmicTag_MV[1]
            else:
                cosmic_0 = event.muon_cosmicTag_full[0] or event.muon_cosmicTag_MV[0] 
                cosmic_1 = event.muon_cosmicTag_full[1] or event.muon_cosmicTag_MV[1] 

            if ("SR" in what and event.muon_phi[0] * event.muon_phi[1] < 0 and not cosmic_0 and not cosmic_1) or  ("1cos" in what and event.muon_phi[0] * event.muon_phi[1] < 0 and cosmic_0 != cosmic_1) :  
                itop = 0
                ibot = 1
                if event.muon_phi[0] < 0:
                    itop = 1
                    ibot = 0
                
                if eval(muon_pass%(ibot,ibot,ibot,ibot)): # good bottom muon
                    if eval(muon_pass_noqual%(itop,itop,itop,itop)):

                        # OR all quality vars
                        if (event.muon_MStrack_nPres[itop] < 3 or event.muon_nPhiLays[itop] < 1):
                            h["t0_top_bad"].Fill(event.muon_t0avg[itop])
                        #only nPres is "bad"
                        if (event.muon_MStrack_nPres[itop] < 3): 
                            h["t0_top_bad_nPres"].Fill(event.muon_t0avg[itop])
                        
                        #only nPhiLays is "bad"
                        if (event.muon_nPhiLays[0] < 1): 
                            h["t0_top_bad_nPhiLays"].Fill(event.muon_t0avg[itop])

                    if "1cos" in what and eval(muon_pass%(itop,itop,itop,itop)): #################ONLY FOR VALIDATION!!!!
                        h["t0_top_good"].Fill(event.muon_t0avg[itop])

                     
                                          


outFile = ROOT.TFile("outputFiles/estimate_fake_"+tag+".root", "RECREATE")

c = ROOT.TCanvas()
for histo in h:
    h[histo] = addOverflow(h[histo])
    #h[histo] = addUnderflowBin(h[histo])

    h[histo].Write()
    h[histo].Draw("hist e")
    c.SaveAs("outputPlots/estimate_fake_final/" + tag +"_"+ histo +".pdf")

