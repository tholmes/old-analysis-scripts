import ROOT
import math
import glob
import os
import sys


execfile("cosmic_helpers.py")				       
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")
ROOT.gROOT.SetBatch(1)

tag = "data"
#append = "theta_tag"
append = tag


if tag == "300_slep":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/051619/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_*.SUSY15_mc16d_051619_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "300_stau":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/051619/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_*.SUSY15_mc16d_051619_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "data":
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data*')
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root')
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/cos/data*.root')
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data*.root')
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data*.root')
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")




names = { 
    "n_mu": { "nb": 10, "low": 0, "high": 10, "label": "N_{muons}", "log": 1},   
    "n_mu_notcos": { "nb": 10, "low": 0, "high": 10, "label": "N_{non-cosmic muons}", "log": 1},   
    "n_el": { "nb": 10, "low": 0, "high": 10, "label": "N_{electrons}", "log": 1},   
    "n_ph": { "nb": 10, "low": 0, "high": 10, "label": "N_{photon}", "log": 1},   
    "n_lep": { "nb": 10, "low": 0, "high": 10, "label": "N_{leptons}", "log": 1},   
    "n_jet": { "nb": 10, "low": 0, "high": 10, "label": "N_{jets}", "log": 1},   
    "n_cos": { "nb": 10, "low": 0, "high": 10, "label": "N_{cosmic tag}", "log": 1},   
    
    "dR": { "nb": 50, "low": 0, "high": 5, "label": "dR_{cosmic, other lepton}", "log": 1},   
    "mu_dR": { "nb": 50, "low": 0, "high": 5, "label": "dR_{cosmic, muon}", "log": 1},   
    "mu_dPhi": { "nb": 50, "low": 0, "high": 5, "label": "d#phi_{cosmic, muon}", "log": 1},   
    "el_dR": { "nb": 50, "low": 0, "high": 5, "label": "dR_{cosmic, electron}", "log": 1},   
    "jet_dR": { "nb": 50, "low": 0, "high": 5, "label": "dR_{cosmic, jet}", "log": 1},   
    "2cos_dR": { "nb": 50, "low": 0, "high": 5, "label": "dR_{cosmic, cosmic}", "log": 1},   
    "sumEta_ID": { "nb": 100, "low": 0, "high": .2, "label": "#Sigma #eta (cosmic, cosmic)", "log": 1},   
    "sumTheta_ID": { "nb": 100, "low": 0, "high": .2, "label": "#Sigma #theta - #pi (cosmic, cosmic)", "log": 1},   
    
    "mu_topoetcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "topoetcone20/pt_{muon}", "log": 1},   
    "mu_ptcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptcone20/pt_{muon}", "log": 1},   
    "mu_ptvarcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptvarcone20/pt_{muon}", "log": 1},   
    "mu_topoetcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "topoetcone20/pt_{muon}", "log": 1},   
    "mu_ptcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptcone20/pt_{muon}", "log": 1},   
    "mu_ptvarcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptvarcone20/pt_{muon}", "log": 1},   
    "mu_pt": { "nb": 50, "low": 0, "high": 1000, "label": "p_{T, muon}", "log": 1},   
    "mu_eta": { "nb": 20, "low": -5, "high": 5, "label": "#eta_{muon}", "log": 1},   
    "mu_phi": { "nb": 20, "low": -3, "high": 3, "label": "#phi_{muon}", "log": 1},   
    "mu_nPres": { "nb": 10, "low": 0, "high": 10, "label": "N_{Precision Hits}", "log": 0},   
    "mu_chi2": { "nb": 100, "low": 0, "high": 60, "label": "#chi^{2}_{lead}", "log": 1}, 
    "mu_theta_baseline": { "nb": 100, "low": -4, "high": 4, "label": "#theta", "log": 1}, 
    "mu_sum_d0": { "nb": 100, "low": 0, "high": 3, "label": "#Sigma d_{0}", "log": 0},   
    "2cos_sum_d0": { "nb": 100, "low": 0, "high": 3, "label": "#Sigma d_{0}", "log": 0},   
    "2cos_t0_posphi": { "nb": 25, "low": -50, "high": 50, "label": "t_{0, avg} (#phi_{#mu} > 0)", "log": 0},   
    "2cos_t0_negphi": { "nb": 25, "low": -50, "high": 50, "label": "t_{0, avg} (#phi_{#mu} < 0)", "log": 0},   
    
    "cos_chi2_CB": { "nb": 50, "low": 0, "high": 20, "label": "#chi^{2}_{CB}", "log": 1}, 
    "cos_chi2_ID": { "nb": 50, "low": 0, "high": 20, "label": "#chi^{2}_{ID}", "log": 1}, 
    "1cos_t0_posphi": { "nb": 50, "low": -50, "high": 50, "label": "t_{0, avg} (#phi_{#mu} > 0)", "log": 0},   
    "1cos_t0_negphi": { "nb": 50, "low": -50, "high": 50, "label": "t_{0, avg} (#phi_{#mu} < 0)", "log": 0},   
    
    "el_topoetcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "topoetcone20/pt_{electron}", "log": 1},   
    "el_ptcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptcone20/pt_{electron}", "log": 1},   
    "el_ptvarcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptvarcone20/pt_{electron}", "log": 1},   
    "el_topoetcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "topoetcone20/pt_{electron}", "log": 1},   
    "el_ptcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptcone20/pt_{electron}", "log": 1},   
    "el_ptvarcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptvarcone20/pt_{electron}", "log": 1},   
    "el_pt": { "nb": 50, "low": 0, "high": 1000, "label": "p_{T, electron}", "log": 1},   
    
    "jet_pt": { "nb": 50, "low": 0, "high": 1000, "label": "p_{T, jet}", "log": 1},   
    
    "min_delta_eta": { "nb": 50, "low": -1, "high": 1, "label": "#Delta #eta (original, corrected)", "log": 1},   
    "min_delta_sum_eta": { "nb": 50, "low": -1, "high": 1, "label": "#Delta #Sigma #eta (original, corrected)", "log": 1},   


}

names_2d ={
    "mu_eta_phi"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "el_eta_phi"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{el}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{el}", "log":0},
    "jet_eta_phi"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{jet}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{jet}", "log":0},
    "2cos_d0_d0"       : { "nb_x": 100, "low_x": -400, "high_x": 400, "label_x": "d_{0, lead cosmic}", "nb_y": 100, "low_y": -400, "high_y": 400, "label_y": "d_{0, sublead cosmic}", "log":0},
    "2cos_z0_z0"       : { "nb_x": 100, "low_x": -600, "high_x": 600, "label_x": "z_{0, lead cosmic}", "nb_y": 100, "low_y": -600, "high_y": 600, "label_y": "z_{0, sublead cosmic}", "log":0},
    "2cos_eta_eta"       : { "nb_x": 100, "low_x": -5, "high_x": 5, "label_x": "#eta_{lead cosmic}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{sublead cosmic}", "log":0},
    "2cos_phi_phi"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{lead cosmic}", "nb_y": 100, "low_y": -4, "high_y": 4, "label_y": "#phi_{sublead cosmic}", "log":0},
    
    "mu_d0_d0"       : { "nb_x": 100, "low_x": -400, "high_x": 400, "label_x": "d_{0, cosmic}", "nb_y": 100, "low_y": -400, "high_y": 400, "label_y": "d_{0, muon}", "log":0},
    "mu_z0_z0"       : { "nb_x": 100, "low_x": -600, "high_x": 600, "label_x": "z_{0, cosmic}", "nb_y": 100, "low_y": -600, "high_y": 600, "label_y": "z_{0, muon}", "log":0},
    "mu_eta_eta"       : { "nb_x": 100, "low_x": -5, "high_x": 5, "label_x": "#eta_{cosmic}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{muon}", "log":0},
    "mu_phi_phi"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#eta_{cosmic}", "nb_y": 100, "low_y": -4, "high_y": 4, "label_y": "#eta_{muon}", "log":0},
    "mu_d0_d0_posphi"       : { "nb_x": 100, "low_x": -400, "high_x": 400, "label_x": "d_{0, cosmic}", "nb_y": 100, "low_y": -400, "high_y": 400, "label_y": "d_{0, muon}", "log":0},
    "el_d0_d0"       : { "nb_x": 100, "low_x": -400, "high_x": 400, "label_x": "d_{0, cosmic}", "nb_y": 100, "low_y": -400, "high_y": 400, "label_y": "d_{0, electron}", "log":0},
    "chi2_phi_untagged"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -4, "high_x": 4, "label_x": "#phi", "log":0},
    "chi2_eta_untagged"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -5, "high_x": 5, "label_x": "#eta", "log":0},
    "chi2_pt_untagged"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 100, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "log":0},
    "chi2_d0_untagged"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "d_{0}", "log":0},
    "chi2_nPres_untagged"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 10, "low_x": 0, "high_x": 10, "label_x": "N_{precision hits}", "log":0},
    "chi2_phi_cos"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -4, "high_x": 4, "label_x": "#phi", "log":0},
    "chi2_eta_cos"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -5, "high_x": 5, "label_x": "#eta", "log":0},
    "chi2_pt_cos"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 100, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "log":0},
    "chi2_d0_cos"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "d_{0}", "log":0},
    "chi2_nPres_cos"       : { "nb_y": 150, "low_y": 0, "high_y": 40, "label_y": "#chi^{2}", "nb_x": 10, "low_x": 0, "high_x": 10, "label_x": "d_{0}", "log":0},

    "eta_phi_baseline"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_baseline_trigger"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_costag"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_costag_mv"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "theta_phi_baseline"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#theta_{#mu}", "log":0},
    "theta_phi_baseline_trigger"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#theta_{#mu}", "log":0},
    "theta_phi_costag"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#theta_{#mu}", "log":0},
    "theta_phi_costag_mv"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#theta_{#mu}", "log":0},
    
    "sumEta_dPhi_ID" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta (cosmic track, cosmic track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (cosmic track, cosmic track)", "log":1},
    "sumEta_dPhi_ID_zoom" : { "nb_x": 50, "low_x": 0, "high_x": .2, "label_x": "#Sigma #eta (cosmic track, cosmic track)", "nb_y": 50, "low_y": 0, "high_y": .02, "label_y": "#Delta #phi (cosmic track, cosmic track)", "log":1},
    
    "sumTheta_dPhi_ID" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #theta (cosmic track, cosmic track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (cosmic track, cosmic track)", "log":1},
    "sumTheta_dPhi_ID_zoom" : { "nb_x": 50, "low_x": 0, "high_x": .2, "label_x": "#Sigma #theta (cosmic track, cosmic track)", "nb_y": 50, "low_y": 0, "high_y": .02, "label_y": "#Delta #phi (cosmic track, cosmic track)", "log":1},
    
    "sumEta_dPhi_min" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumEta_dPhi_min_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumEta_dPhi_min_corr" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumEta_dPhi_min_corr_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .35, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    
    "sumTheta_dPhi_min" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #theta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumTheta_dPhi_min_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #theta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumTheta_dPhi_min_corr" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #theta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    "sumTheta_dPhi_min_corr_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #theta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":1},
    
    "min_delta_eta_z0" : { "nb_x": 50, "low_x": -.5, "high_x": .5, "label_x": "#Delta #eta (original, corrected)", "nb_y": 50, "low_y": -500, "high_y": 500, "label_y": "z_{0} (muon)", "log":1},
    "min_delta_sum_eta_z0" : { "nb_x": 50, "low_x": -.5, "high_x": .5, "label_x": "#Delta #Sigma #eta (original, corrected)", "nb_y": 50, "low_y": -500, "high_y": 500, "label_y": "z_{0} (muon)", "log":1},


}



h = {}
h2 = {}
h = setHistos(tag, names)
h2 = setHistos2D(tag,names_2d)


eventNums = []
cosEventNums = []
print t_cos.GetEntries()

eventNum = 0
ncos = 0
for event in t_cos:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    #if eventNum > 50000: break
    eventNum+=1
    iCos = []
    nCosTag = 0
    nPs = 0
    #if not event.pass_HLT_mu60_0eta105_msonly: continue
    
    for im in xrange(0,len(event.muon_pt)):
        
        muon = ROOT.TVector3()
        muon.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])

        if preselect_muon(event, im, doTiming=False):
            mostBackToBack_ms(event, im, h, h2) 
        #if event.muon_isSRcuts[im] and event.muon_isGoodQual[im] and event.muon_isPassIDtrk[im] and event.muon_isIsolated[im]:
            h2["eta_phi_baseline"].Fill(event.muon_phi[im], event.muon_eta[im]) 
            h2["theta_phi_baseline"].Fill(event.muon_phi[im], muon.Theta()) 
            h["mu_theta_baseline"].Fill(muon.Theta()) 
            
            #h2["eta_phi_baseline_trigger"].Fill(event.muon_phi[im], event.muon_eta[im]) 
            #h2["theta_phi_baseline_trigger"].Fill(event.muon_phi[im], muon.Theta()) 
            nPs += 1

            #cosmicTag = cosTag(event, im, .012 , .18) 
            cosmicTag,isseg = cosTag(event, im, .018 , .25) 
            #cosmicTag = cosTag_theta(event, im, .012 , .18) 
            matVeto = materialVeto(event, im, innerMap, middleMap, outerMap)
            #matVeto = materialVeto_theta(event, im, innerMap, middleMap, outerMap)
        
            
            
            #cosmicTag = not event.muon_isNotCosmic[im] 
            cosmic = cosmicTag or matVeto
            if cosmic:
                iCos.append(im)


            #if not event.muon_cosmicTag_full[im]:
            if not cosmicTag:
                h2["eta_phi_costag"].Fill(event.muon_phi[im], event.muon_eta[im]) 
                h2["theta_phi_costag"].Fill(event.muon_phi[im], muon.Theta()) 
                #if not event.muon_cosmicTag_MV[im]:
                if not matVeto:
                    h2["eta_phi_costag_mv"].Fill(event.muon_phi[im], event.muon_eta[im]) 
                    h2["theta_phi_costag_mv"].Fill(event.muon_phi[im], muon.Theta()) 
            
            #mostBackToBack_ms(event, im, h, h2, (im in iCos))            

            if cosmicTag:
                h["cos_chi2_CB"].Fill(event.muon_CBtrack_chi2[im])
                h["cos_chi2_ID"].Fill(event.muon_IDtrack_chi2[im])
    if len(iCos) > 2: print "so many cosmics! There are %i cosmics, lumi block %i and event number %i and run number %i"%(len(iCos),event.lumiBlock,event.eventNumber, event.runNumber)
    if len(iCos) > 0 :
        h["n_cos"].Fill(len(iCos))
        
        if len(iCos) == 1:

            if event.muon_phi[iCos[0]] > 0: h["1cos_t0_posphi"].Fill(t_avg(event,iCos[0]))
            else: h["1cos_t0_negphi"].Fill(t_avg(event,iCos[0]))
        
        if len(iCos) == 2:
            iPos = iCos[0]
            iNeg = iCos[1]
            if event.muon_phi[iCos[1]] > 0: 
                iPos = iCos[1]
                iNeg = iCos[0]

            v_cos1 = ROOT.TVector3()
            v_cos1.SetPtEtaPhi(event.muon_pt[iCos[0]], event.muon_eta[iCos[0]], event.muon_phi[iCos[0]])
            v_cos2 = ROOT.TVector3()
            v_cos2.SetPtEtaPhi(event.muon_pt[iCos[1]], event.muon_eta[iCos[1]], event.muon_phi[iCos[1]])
            
            v_cos1_id = ROOT.TVector3()
            v_cos1_id.SetPtEtaPhi(event.muon_IDtrack_pt[iCos[0]], event.muon_IDtrack_eta[iCos[0]], event.muon_IDtrack_phi[iCos[0]])
            v_cos2_id = ROOT.TVector3()
            v_cos2_id.SetPtEtaPhi(event.muon_IDtrack_pt[iCos[1]], event.muon_IDtrack_eta[iCos[1]], event.muon_IDtrack_phi[iCos[1]])
            
            h["2cos_dR"].Fill(v_cos1.DeltaR(v_cos2))
            h["2cos_sum_d0"].Fill(abs(event.muon_IDtrack_d0[iCos[0]]+event.muon_IDtrack_d0[iCos[1]]))
            h2["2cos_d0_d0"].Fill(event.muon_IDtrack_d0[iCos[0]], event.muon_IDtrack_d0[iCos[1]])
            h2["2cos_z0_z0"].Fill(event.muon_IDtrack_z0[iCos[0]], event.muon_IDtrack_z0[iCos[1]])
            h2["2cos_eta_eta"].Fill(event.muon_eta[iCos[0]], event.muon_eta[iCos[1]])
            h2["2cos_phi_phi"].Fill(event.muon_phi[iCos[0]], event.muon_phi[iCos[1]])
            
            h2["sumEta_dPhi_ID"].Fill( abs(v_cos1_id.Eta() + v_cos2_id.Eta()),  abs(abs(v_cos1_id.DeltaPhi(v_cos2_id) - math.pi)))
            h2["sumEta_dPhi_ID_zoom"].Fill( abs(v_cos1_id.Eta() + v_cos2_id.Eta()),  abs(abs(v_cos1_id.DeltaPhi(v_cos2_id) - math.pi)))
            
            h2["sumTheta_dPhi_ID"].Fill( abs(abs(v_cos1_id.Theta() + v_cos2_id.Theta()) - math.pi),  abs(abs(v_cos1_id.DeltaPhi(v_cos2_id) - math.pi)))
            h2["sumTheta_dPhi_ID_zoom"].Fill( abs(abs(v_cos1_id.Theta() + v_cos2_id.Theta()) - math.pi),  abs(abs(v_cos1_id.DeltaPhi(v_cos2_id) - math.pi)))
            
            h["sumEta_ID"].Fill(abs(v_cos1_id.Eta() + v_cos2_id.Eta()))
            h["sumTheta_ID"].Fill(abs(abs(v_cos1_id.Theta() + v_cos2_id.Theta()) - math.pi))
            
            
            h["2cos_t0_posphi"].Fill(t_avg(event,iPos))
            h["2cos_t0_negphi"].Fill(t_avg(event,iNeg))

            cosEventNums.append(event.eventNumber)
            
        cos_v = ROOT.TVector3()
        cos_v.SetPtEtaPhi(event.muon_pt[iCos[0]], event.muon_eta[iCos[0]], event.muon_phi[iCos[0]])
         
        nMu = 0
        for i in xrange(len(event.muon_pt)):
            if i not in iCos and preselect_muon(event, i):
            #if i not in iCos :
                eventNums.append(event.eventNumber)
                nMu+=1
                v_tmp = ROOT.TVector3()
                v_tmp.SetPtEtaPhi(event.muon_pt[i], event.muon_eta[i], event.muon_phi[i])

                h["dR"].Fill(v_tmp.DeltaR(cos_v))
                h["mu_dR"].Fill(abs(v_tmp.DeltaR(cos_v)))
                h["mu_dPhi"].Fill(abs(v_tmp.DeltaPhi(cos_v)))
                h["mu_pt"].Fill(event.muon_pt[i])
                h["mu_eta"].Fill(event.muon_eta[i])
                h["mu_phi"].Fill(event.muon_phi[i])
                h["mu_nPres"].Fill(event.muon_MStrack_nPres[i])
                h["mu_chi2"].Fill(event.muon_CBtrack_chi2[i])
                
                h["mu_topoetcone20"].Fill(event.muon_topoetcone20[i]/event.muon_pt[i])
                h["mu_ptvarcone20"].Fill(event.muon_ptvarcone20[i]/event.muon_pt[i])
                h["mu_ptcone20"].Fill(event.muon_ptcone20[i]/event.muon_pt[i])
                h["mu_topoetcone20_zoom"].Fill(event.muon_topoetcone20[i]/event.muon_pt[i])
                h["mu_ptvarcone20_zoom"].Fill(event.muon_ptvarcone20[i]/event.muon_pt[i])
                h["mu_ptcone20_zoom"].Fill(event.muon_ptcone20[i]/event.muon_pt[i])
                h["mu_sum_d0"].Fill(abs(event.muon_IDtrack_d0[iCos[0]]+event.muon_IDtrack_d0[i]))

                h2["mu_d0_d0"].Fill(event.muon_IDtrack_d0[iCos[0]],event.muon_IDtrack_d0[i])
                h2["mu_z0_z0"].Fill(event.muon_IDtrack_z0[iCos[0]],event.muon_IDtrack_z0[i])
                h2["mu_eta_eta"].Fill(event.muon_eta[iCos[0]],event.muon_eta[i])
                h2["mu_phi_phi"].Fill(event.muon_phi[iCos[0]],event.muon_phi[i])
                if event.muon_phi[i] > 0: h2["mu_d0_d0_posphi"].Fill(event.muon_IDtrack_d0[iCos[0]],event.muon_IDtrack_d0[i])
                h2["mu_eta_phi"].Fill(event.muon_phi[i],event.muon_eta[i])
                h2["chi2_phi_untagged"].Fill(event.muon_phi[i],event.muon_CBtrack_chi2[i])
                h2["chi2_eta_untagged"].Fill(event.muon_eta[i],event.muon_CBtrack_chi2[i])
                h2["chi2_pt_untagged"].Fill(event.muon_pt[i],event.muon_CBtrack_chi2[i])
                h2["chi2_d0_untagged"].Fill(abs(event.muon_IDtrack_d0[i]),event.muon_CBtrack_chi2[i])
                h2["chi2_nPres_untagged"].Fill(event.muon_MStrack_nPres[i],event.muon_CBtrack_chi2[i])
            else:
                h2["chi2_phi_cos"].Fill(event.muon_phi[i],event.muon_CBtrack_chi2[i])
                h2["chi2_eta_cos"].Fill(event.muon_eta[i],event.muon_CBtrack_chi2[i])
                h2["chi2_pt_cos"].Fill(event.muon_pt[i],event.muon_CBtrack_chi2[i])
                h2["chi2_d0_cos"].Fill(abs(event.muon_IDtrack_d0[i]),event.muon_CBtrack_chi2[i])
                h2["chi2_nPres_cos"].Fill(event.muon_MStrack_nPres[i],event.muon_CBtrack_chi2[i])
        
        h["n_mu_notcos"].Fill(nMu)
        
        nPh = 0
        try: 
            x = len(event.photon_pt)
            for i in xrange(len(event.photon_pt)):
                if preselect_photon(event,i):
                #if True:
                    nPh+=1
        except:
            print "whoops" 
        h["n_ph"].Fill(nPh)
    
        ''' 
        nEl = 0
        for i in xrange(len(event.electron_pt)):
            if baseline_electron(event, i, tight=True):
            #if True:
                nEl += 1
                v_tmp = ROOT.TVector3()
                v_tmp.SetPtEtaPhi(event.electron_pt[i], event.electron_eta[i], event.electron_phi[i])

                h["dR"].Fill(abs(v_tmp.DeltaR(cos_v)))
                h["el_dR"].Fill(abs(v_tmp.DeltaR(cos_v)))
                h["el_pt"].Fill(event.electron_pt[i])
                h2["el_d0_d0"].Fill(event.muon_IDtrack_d0[iCos[0]],event.electron_d0[i])
                    
                h["el_topoetcone20"].Fill(event.electron_topoetcone20[i]/event.electron_pt[i])
                h["el_ptvarcone20"].Fill(event.electron_ptvarcone20[i]/event.electron_pt[i])
                h["el_ptcone20"].Fill(event.electron_ptcone20[i]/event.electron_pt[i])
                h["el_topoetcone20_zoom"].Fill(event.electron_topoetcone20[i]/event.electron_pt[i])
                h["el_ptvarcone20_zoom"].Fill(event.electron_ptvarcone20[i]/event.electron_pt[i])
                h["el_ptcone20_zoom"].Fill(event.electron_ptcone20[i]/event.electron_pt[i])
                
                h2["el_eta_phi"].Fill(event.electron_phi[i],event.electron_eta[i])
        h["n_el"].Fill(nEl)
        h["n_lep"].Fill(nEl + nMu)
        '''        

        for i in xrange(len(event.jet_pt)):
            v_tmp = ROOT.TVector3()
            v_tmp.SetPtEtaPhi(event.jet_pt[i], event.jet_eta[i], event.jet_phi[i])

            h["jet_dR"].Fill(abs(v_tmp.DeltaR(cos_v)))
            h["jet_pt"].Fill(event.jet_pt[i])
            h2["jet_eta_phi"].Fill(event.jet_phi[i],event.jet_eta[i])
        h["n_jet"].Fill(len(event.jet_pt))


print ncos

c = ROOT.TCanvas()

outfile = ROOT.TFile("outputFiles/2cos_"+append+".root","RECREATE")

for histo in h:
    #h[histo] = addOverflow(h[histo])
    h[histo].Write()
    if names[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    h[histo].Draw()
    c.SaveAs("outputPlots/CRCos/"+append+"_1_"+ histo + ".pdf")

ROOT.gStyle.SetPalette(ROOT.kBird)

ROOT.gPad.SetLogy(0)
for histo in h2:
    h2[histo].Write()
    if names_2d[histo]["log"] == 1:
        ROOT.gPad.SetLogz()
    else:
        ROOT.gPad.SetLogz(0)
    h2[histo].GetZaxis().SetTitle("number of events")
    #h2[histo].SetMaximum(2)
    h2[histo].Draw("COLZ")
    ROOT.ATLASLabel(0.20,0.88, "Internal")
    c.SaveAs("outputPlots/CRCos/"+append+"_2_"+ histo + ".pdf")

outfile.Write()
print "events with a second untagged muon", eventNums   



