import ROOT
import glob
import os

ROOT.gROOT.SetBatch(True)
execfile("../../scripts/plot_helpers/basic_plotting.py")


def cutAndDraw(var,cuts,filename):
   c = ROOT.TCanvas()
   print "Drawing %s, with %s cuts, saving as %s"%(var,cuts,filename)
   if ":" in var: chain.Draw(var, cuts, "COLZ")
   else: chain.Draw(var, cuts)
   c.Update()
   c.SaveAs("plots/"+filename+".pdf") 


f = ROOT.TFile("eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.user.kdipetri.data16_cos.mc16d_031519_trees.root/all.    root")
t_cos = f.Get("trees_SRCOS_")


variables = ['deltaR','suEta','deltaPhi','invMass','triggerRegion', 'triggerRegion_pass', 'deltaR:lepton_d0[0]','deltaR:lepton_d0[0]','deltaR:invMass']
variables = {'muon_pt[0]':'muon_:p


