import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "1cos"
#what = "SR_2mu"
#what = "SR_nochi2"
what = "1cos_narrow_2mu_p1"


print "1cos" in what
#full
#dEta = 0.018
#dPhi = 0.25

#narrower
dEta = 0.013
dPhi = 0.1
 
tag = ""+what+"_v4"
#basename= dirname + 'data_wip/Data/*'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data1*.root'
#basename= '/afs/cern.ch/user/e/eressegu/public/ntuples/data*.root'

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)


#make Rgood
names = { 
    "t0_good_any": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_any": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_any": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_any": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_any": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_good_none": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_none": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_none": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_none": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_none": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_good_id": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_id": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_id": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_id": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_id": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_good_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_good_pres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_pres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_pres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_pres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_pres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    "t0_good_phi": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, good)", "log": 1},   
    "t0_bad_phi": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_chi2_phi": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPres_phi": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    "t0_bad_nPhiLays_phi": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (1#mu, bad)", "log": 1},
    
    
    "t0_top_bad": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_chi2": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_nPres": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (2#mu, top, bad)", "log": 1},
    "t0_top_bad_nPhiLays": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (2#mu, top, bad)", "log": 1},
   
    "d0_top_bad": { "nb": 20, "low": -100, "high": 100, "label": "d_{0} (2#mu, top, bad)", "log": 1},
    "z0_top_bad": { "nb": 20, "low": -100, "high": 100, "label": "z_{0} (2#mu, top, bad)", "log": 1},
    "eta_top_bad": { "nb": 10, "low": -2.5, "high": 2.5, "label": "#eta (2#mu, top, bad)", "log": 1},
    "phi_top_bad": { "nb": 10, "low": -3, "high": 3, "label": "#phi (2#mu, top, bad)", "log": 1},
    "pt_top_bad": { "nb": 50, "low": 0, "high": 600, "label": "p_{T} (2#mu, top, bad)", "log": 1},
    "npres_top_bad": { "nb": 20, "low": 0, "high": 10, "label": "n pres (2#mu, top, bad)", "log": 1},
    "nphi_top_bad": { "nb": 20, "low": 0, "high": 600, "label": "n phi (2#mu, top, bad)", "log": 1},
    "chi2_top_bad": { "nb": 20, "low": 0, "high": 600, "label": "#chi^2 (2#mu, top, bad)", "log": 1},
   
    
    "t0_top_good": { "nb": 10, "low": -30, "high": 30, "label": "t_{0} (2#mu, top, good)", "log": 1}, ###ONLY FOR VALIDATION
    "d0_top_good": { "nb": 20, "low": -100, "high": 100, "label": "d_{0} (2#mu, top, good)", "log": 1},
    "z0_top_good": { "nb": 20, "low": -100, "high": 100, "label": "z_{0} (2#mu, top, good)", "log": 1},
    "eta_top_good": { "nb": 5, "low": -2.5, "high": 2.5, "label": "#eta (2#mu, top, good)", "log": 1},
    "phi_top_good": { "nb": 5, "low": -3, "high": 3, "label": "#phi (2#mu, top, good)", "log": 1},
    "pt_top_good": { "nb": 20, "low": 0, "high": 600, "label": "#phi (2#mu, top, good)", "log": 1},
    "npres_top_good": { "nb": 20, "low": 0, "high": 10, "label": "n pres (2#mu, top, good)", "log": 1},
    "nphi_top_good": { "nb": 20, "low": 0, "high": 600, "label": "n phi (2#mu, top, good)", "log": 1},
    "chi2_top_good": { "nb": 20, "low": 0, "high": 600, "label": "#chi^2 (2#mu, top, good)", "log": 1},
}
    

h = {}
h = setHistos("", names)


mysteryNums = [4253530762, 646388285, 722317128, 765842373, 926487021]


def select_bottom(event, i, selec):
    

    # pass kinematics and isolation
    #if not event.muon_hasIDtrack[i]: return False
    #if not event.muon_hasCBtrack[i]: return False
    #if not event.muon_hasMStrack[i]: return False
    #if event.muon_nMSSegments[i] == 0: return False
    
    #if event.muon_pt[i] < 65: return False
    #if abs(event.muon_IDtrack_d0[i]) < 3 : return False
    
    #if event.muon_topoetcone20[i]/event.muon_pt[i] > 0.15: return False
    #if event.muon_ptvarcone20[i]/event.muon_pt[i] > 0.04: return False
    
    if "none" in selec: return True
    #fail some quality cut
    if "any" or "id" in selec and  event.muon_IDtrack_chi2[i] < 2: return False
    if "any" or "id" in selec and event.muon_IDtrack_nMissingLayers[i] <=1 : return False 
    if "any" or "pres" in selec and event.muon_MStrack_nPres[i] > 2: return False  
    if "any" or "phi" in selec and  phi_lays(event, i) > 0: return False 
    if "any" or "chi2" in selec and event.muon_CBtrack_chi2[i] < 3 : return False 
    
    return True


#bad_probes = ["any","none","pres","phi","chi2","id"]
bad_probes = ["any","pres","phi","chi2","id"]

print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if event.eventNumber in mysteryNums:
        print "****event num", eventNum
        print "basline mu", len(event.muon_pt)
        print "basline e", len(event.electron_pt)
        for i in [0,1]:
            ctag_narrow, ms_cos = cosTag(event, i, .013, .18)
            ctag_full, ms_cos = cosTag(event, i, .018, .25)
            matVeto = materialVeto(event, i, innerMap, middleMap, outerMap)
            t0 = t_avg(event, i) 
            
            print "muon ", i 
            print "cosmic narrow", ctag_narrow 
            print "cosmic full", ctag_full 
            print "cosmic mv", matVeto 
            print "phi", event.muon_phi[i]
            print "tavg", t0
        print
    
    
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100000: break

    #1 muon histos
    if len(event.muon_pt) > 1 and len(event.electron_pt) == 0 and event.muon_phi[0] * event.muon_phi[1] < 0:
         
        itop = 0
        ibot = 1
        if event.muon_phi[0] < 0:
            itop = 1
            ibot = 0
        
        ctag, ms_cos = cosTag(event, itop, dEta, dPhi)
        matVeto = materialVeto(event, itop, innerMap, middleMap, outerMap)
        
        cosmic = ctag or matVeto
        
        


        if preselect_muon_basechi2(event, itop, doQual=False) and not cosmic: 
        #if preselect_muon(event, 0, doQual=False) and event.muon_phi[0] > 0: 
            for probe in bad_probes:
                if select_bottom(event, ibot, probe):
                    print 
                    print "++++ we made it ! ++++"
                    print "id track? ", event.muon_hasIDtrack[ibot]
                    print "ms track? ", event.muon_hasMStrack[ibot]
                    print "cb track? ", event.muon_hasCBtrack[ibot]
                    print "segments? ", event.muon_nMSSegments[ibot]
                    print "pt        ", event.muon_pt[ibot]
                    print "d0        ", event.muon_IDtrack_d0[ibot]
                    print "failed    ", probe
                    print  
                    try:
                        t0 = t_avg(event, itop) 
                    except:
                        print "alert!!", event.muon_nMSSegments[itop]
                        continue
                    phi_layers = phi_lays(event,itop) 
                    if event.muon_CBtrack_chi2[itop] < 3 and event.muon_MStrack_nPres[itop] >= 3 and phi_layers > 0: 
                        h["t0_good_"+probe].Fill(t0)
                    # OR all quality vars
                    else:
                        if (event.muon_CBtrack_chi2[itop] > 3 or event.muon_MStrack_nPres[itop] < 3 or phi_layers < 1):
                            h["t0_bad_"+probe].Fill(t0)
                        #only chi2 is "bad"
                        if (event.muon_CBtrack_chi2[itop] > 3): 
                            h["t0_bad_chi2_"+probe].Fill(t0)
                        
                        #only nPres is "bad"
                        if (event.muon_MStrack_nPres[itop] < 3): 
                            h["t0_bad_nPres_"+probe].Fill(t0)
                        
                        #only nPhiLays is "bad"
                        if (phi_layers < 1): 
                            h["t0_bad_nPhiLays_"+probe].Fill(t0)
            
    if len(event.muon_pt) == 2 and len(event.lepton_pt) == 2:
        
        ctag0, ms_cos = cosTag(event, 0, dEta, dPhi)
        matVeto0 = materialVeto(event, 0, innerMap, middleMap, outerMap)
        cosmic_0 = ctag0 or matVeto0
        
        ctag1, ms_cos = cosTag(event, 1, dEta, dPhi)
        matVeto1 = materialVeto(event, 1, innerMap, middleMap, outerMap)
        cosmic_1 = ctag1 or matVeto1

        if ("SR" in what and event.muon_phi[0] * event.muon_phi[1] < 0 and not cosmic_0 and not cosmic_1) or  ("1cos" in what and event.muon_phi[0] * event.muon_phi[1] < 0 and cosmic_0 != cosmic_1) :  
            itop = 0
            ibot = 1
            if event.muon_phi[0] < 0:
                itop = 1
                ibot = 0
            
            if preselect_muon(event, ibot): # good bottom muon
                if preselect_muon_basechi2(event, itop, doQual=False):
                    try:
                        t0 = t_avg(event, itop) 
                    except:
                        print "alert!!", event.muon_nMSSegments[itop]
                        continue
                        
                    phi_layers = phi_lays(event,itop)

                    # OR all quality vars
                    if (event.muon_CBtrack_chi2[itop] > 3 or event.muon_MStrack_nPres[itop] < 3 or phi_layers < 1):
                        h["t0_top_bad"].Fill(t0)
                        h["d0_top_bad"].Fill(event.muon_IDtrack_d0[itop])
                        h["z0_top_bad"].Fill(event.muon_IDtrack_z0[itop])
                        h["eta_top_bad"].Fill(event.muon_eta[itop])
                        h["phi_top_bad"].Fill(event.muon_phi[itop])
                        h["pt_top_bad"].Fill(event.muon_pt[itop])
                        h["chi2_top_bad"].Fill(event.muon_CBtrack_chi2[itop])
                        h["nphi_top_bad"].Fill(phi_layers)
                        h["npres_top_bad"].Fill(event.muon_MStrack_nPres[itop])


                        #print
                        #print event.eventNumber
                        #print "t0 ", t0
                        #print "chi2 ", event.muon_CBtrack_chi2[itop]
                        #print "nPres ", event.muon_MStrack_nPres[itop]
                        #print "phi lays ", phi_layers
                    #only chi2 is "bad"
                    if (event.muon_CBtrack_chi2[itop] > 3):
                        #print "fails chi2, t0 ", t0 
                        h["t0_top_bad_chi2"].Fill(t0)
                    
                    #only nPres is "bad"
                    if (event.muon_MStrack_nPres[itop] < 3): 
                        #print "fails nPres, t0 ", t0 
                        h["t0_top_bad_nPres"].Fill(t0)
                    
                    #only nPhiLays is "bad"
                    if (phi_layers < 1): 
                        #print "fails nPhi, t0 ", t0 
                        h["t0_top_bad_nPhiLays"].Fill(t0)

                if "1cos" in what and preselect_muon(event, itop): #################ONLY FOR VALIDATION!!!!
                    try:
                        t0 = t_avg(event, itop) 
                    except:
                        print "alert!!", event.muon_nMSSegments[itop]
                        continue
                    h["t0_top_good"].Fill(t0)
                    h["d0_top_good"].Fill(event.muon_IDtrack_d0[itop])
                    h["z0_top_good"].Fill(event.muon_IDtrack_z0[itop])
                    h["eta_top_good"].Fill(event.muon_eta[itop])
                    h["phi_top_good"].Fill(event.muon_phi[itop])
                    h["pt_top_good"].Fill(event.muon_pt[itop])
                    h["chi2_top_good"].Fill(event.muon_CBtrack_chi2[itop])
                    h["nphi_top_good"].Fill(phi_layers)
                    h["npres_top_good"].Fill(event.muon_MStrack_nPres[itop])

                 
                                      


outFile = ROOT.TFile("outputFiles/estimate_fake_"+tag+".root", "RECREATE")

c = ROOT.TCanvas()
for histo in h:
    h[histo].Write()
    h[histo].Draw("hist e")
    c.SaveAs("outputPlots/estimate_fake/" + tag +"_"+ histo +".pdf")
