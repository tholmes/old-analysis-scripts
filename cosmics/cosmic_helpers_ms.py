

def preselect_muon(event, i, tight=True):
    
    if "cosmic" in tag:
        d0 = event.muon_d0[i]
        z0 = event.muon_z0[i]
    else: 
        d0 = event.muon_IDtrack_d0[i]
        z0 = event.muon_IDtrack_z0[i]
#    h["orig_z0"].Fill( z0 )
#    h2["d0_z0"].Fill(d0, z0)

#    h["muon_cutflow"].Fill("all",1)

    #medium muon
    if "cosmic" in tag:
        if not event.muon_hasCBtrack[i]: return False
#        h["muon_cutflow"].Fill("combined",1)
        if abs(event.muon_QoverPsignif[i]) > 7: return False
#        h["muon_cutflow"].Fill("qOverPSig",1)
        #if event.muon_nPres[i] < 2 or not ( event.muon_nPres[i] == 1 and event.muon_nPresHole[i] < 2 and abs(event.muon_eta[i])<0.1): return False
        if event.muon_nPres[i] < 2 and not abs(event.muon_eta[i] < 0.1): return False
#        h["muon_cutflow"].Fill("nPres",1)
        if event.muon_nSCT[i] < 5: return False
#        h["muon_cutflow"].Fill("nSCT",1)



    if event.muon_pt[i] < 65: return False
#    h["muon_cutflow"].Fill("pt",1)
    if abs(event.muon_eta[i]) > 2.5: return False
#    h["muon_cutflow"].Fill("eta",1) 
    if abs( d0 ) < 3: return False
#    h["muon_cutflow"].Fill("mind0",1)
    if abs( z0 ) > 500: return False
#    h["muon_cutflow"].Fill("z0",1)
    if abs( d0 ) > 300: return False
#    h["muon_cutflow"].Fill("maxd0",1)
    if tight:
        if event.muon_CBtrack_chi2[i] > 3: return False 
        if event.muon_MStrack_nPres[i] < 3: return False 
    
    return True

def preselect_electron(event, i):

    if event.electron_pt[i] < 65: return False
    if abs(event.electron_eta[i]) > 2.5: return False
    if abs( event.electron_d0[i] ) < 3: return False
    
    return True

def preselect_photon(event, i):

    if event.photon_pt[i] < 65: return False
    if abs(event.photon_eta[i]) > 2.5: return False
    
    return True


def setHistos(tag, histos):
    
    h={}
    for name in histos:
        h[name] = ROOT.TH1F(tag + "_" + name, name, histos[name]["nb"], histos[name]["low"], histos[name]["high"])
        h[name].GetXaxis().SetTitle(histos[name]["label"])
    return h

def setHistos2D(tag, histos):
    h={}
    for name in histos:
        h[name] = ROOT.TH2F(tag + "_" + name,name, histos[name]["nb_x"], histos[name]["low_x"], histos[name]["high_x"], histos[name]["nb_y"], histos[name]["low_y"], histos[name]["high_y"]) 
        h[name].GetXaxis().SetTitle(histos[name]["label_x"])
        h[name].GetYaxis().SetTitle(histos[name]["label_y"])
    return h

def deltaRCos(v1, v2):
	sumEta = abs(v1.Eta() + v2.Eta())
	dPhi = v1.DeltaPhi(v2)
	dR = math.sqrt((abs(dPhi)-math.pi)**2 + sumEta**2)

	return dR

def eta_corrected(event, v, ms, im):
        
    #R_seg = math.sqrt(event.msSegment_x[ms]**2 + event.msSegment_y[ms]**2)
    R_seg = ROOT.TMath.Hypot(event.msSegment_x[ms], event.msSegment_y[ms])
    
    #try: z_corr = R_seg/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    z_corr = R_seg/math.tan(v.Theta()) - event.muon_MStrack_z0[im]
    #except: z_corr = R_seg/math.tan(v.Theta()) - event.muon_z0[im]
    
    theta_corr = math.atan(abs(R_seg/z_corr))
    eta_corr = -math.log(math.tan(theta_corr/2.0))
    if z_corr < 0: eta_corr = -eta_corr
    
    #h["eta_orig"].Fill(v.Eta())
    #h["theta_orig"].Fill(v.Theta())
    #h["theta_corr"].Fill(theta_corr)
    #h["eta_corr"].Fill(eta_corr)

    return eta_corr

def eta_corrected_R(event, im, R):
    
    v = ROOT.TVector3()
    #v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    v.SetPtEtaPhi(event.muon_MStrack_pt[im], event.muon_MStrack_eta[im], event.muon_MStrack_phi[im])
    
    #try: z_corr = R/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    try: z_corr = R/math.tan(v.Theta()) - event.muon_MStrack_z0[im]
    except: z_corr = R/math.tan(v.Theta()) - event.muon_z0[im]
    
    theta_corr = math.atan(abs(R/z_corr))
    eta_corr = -math.log(math.tan(theta_corr/2.0))
    if z_corr < 0: eta_corr = -eta_corr
    
    return eta_corr

def mostBackToBack_id(event, im):
    v = ROOT.TVector3()
    #v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    v.SetPtEtaPhi(event.muon_MStrack_pt[im], event.muon_MStrack_eta[im], event.muon_MStrack_phi[im])
    
    mindR = 999
    iMin = -1
    for idt in xrange(len(event.idTrack_eta)):
        vt = ROOT.TVector3()
        vt.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
        
        dR = deltaRCos(v, vt)
        h["dR_leadmu_allid"].Fill(dR)
        h["dPhi_leadmu_allid"].Fill( abs(v.DeltaPhi(vt)) )
        h["sumEta_leadmu_allid"].Fill( v.Eta() + vt.Eta() )
        if dR < mindR:
            mindR = dR
            iMin = idt

    return iMin  

def mostBackToBack_ms(event, im):
    v = ROOT.TVector3()
    #v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    v.SetPtEtaPhi(event.muon_MStrack_pt[im], event.muon_MStrack_eta[im], event.muon_MStrack_phi[im])
    
    mindR = 999
    iMin = -1
    min_sumEta = 999
    min_sumEta_corr = 99
    min_dPhi = 999
    min_dPhi_corr = 999

    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms]) 
        
        if vt.Phi() * v.Phi() > 0: continue
        
        eta_corr = eta_corrected(event, v, ms, im)
        sum_eta_corr = abs( eta_corr + vt.Eta() )
        delta_eta_corr = abs( eta_corr - vt.Eta() )
        dR = deltaRCos(v, vt)
        dPhi = abs((abs(v.DeltaPhi(vt)) - math.pi))

        if sum_eta_corr < 0: print "help~"
        if sum_eta_corr >  5: print "help 2"
         
        h["dR_leadmu_allms"].Fill(dR)
        h["dPhi_leadmu_allms"].Fill( dPhi )
        h["sumEta_leadmu_allms"].Fill( abs(v.Eta() + vt.Eta()) )
        h["sumEta_leadmu_allms_corr"].Fill( sum_eta_corr )
        
        h2["sumEta_dPhi"].Fill(      abs( v.Eta() + vt.Eta() ), dPhi) 
        h2["sumEta_dPhi_zoom"].Fill( abs( v.Eta() + vt.Eta() ), dPhi)            
        h2["deltaEta_dPhi"].Fill(    abs( v.Eta() - vt.Eta() ), dPhi) 
        
        h2["sumEta_dPhi_corr_zoom"].Fill( sum_eta_corr, dPhi)            
        h2["sumEta_dPhi_corr_zoom_2"].Fill( sum_eta_corr, dPhi)            
        h2["sumEta_dPhi_corr"].Fill(      sum_eta_corr, dPhi)            
        h2["deltaEta_dPhi_corr"].Fill(    delta_eta_corr, dPhi)            
        
        if abs(v.Eta() + vt.Eta()) < min_sumEta and abs(abs(v.DeltaPhi(vt)) - math.pi) < min_dPhi :
            min_sumEta = abs(v.Eta() + vt.Eta())
            min_dPhi = abs(abs(v.DeltaPhi(vt)) - math.pi)
        
        if sum_eta_corr < min_sumEta_corr and abs(abs(v.DeltaPhi(vt)) - math.pi) < min_dPhi_corr :
            min_sumEta_corr = sum_eta_corr
            min_dPhi_corr = abs(abs(v.DeltaPhi(vt)) - math.pi)

        if dR < mindR:
            mindR = dR
            iMin = ms
    if min_sumEta == 999: print "alert~ n segments " + str(len(event.msSegment_x)) + " eta " + str(v.Eta() ) 
    
    h2["sumEta_dPhi_min"].Fill(          min_sumEta,      min_dPhi)
    h2["sumEta_dPhi_min_zoom"].Fill(     min_sumEta,      min_dPhi)
    h2["sumEta_dPhi_min_corr"].Fill(     min_sumEta_corr, min_dPhi_corr)
    h2["sumEta_dPhi_min_corr_zoom"].Fill(min_sumEta_corr, min_dPhi_corr)
    h2["sumEta_dPhi_min_corr_zoom_2"].Fill(min_sumEta_corr, min_dPhi_corr)
    return iMin

def cosTag(event, im, dEta, dPhi):
    v = ROOT.TVector3()
    #v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    v.SetPtEtaPhi(event.muon_MStrack_pt[im], event.muon_MStrack_eta[im], event.muon_MStrack_phi[im])
    
    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        
        if vt.Phi() * v.Phi() > 0: continue
        
        eta_corr = eta_corrected(event, v, ms, im)
        sum_eta_corr = abs( eta_corr + vt.Eta() )
        delta_eta_corr = abs( eta_corr - vt.Eta() )
        
        deltaPhi = abs((abs(v.DeltaPhi(vt)) - math.pi))
        
        if deltaPhi < dPhi and sum_eta_corr < dEta : return True
    
     
    return False


def materialVeto(event, im, innerMap, middleMap, outerMap):

    #if abs(event.muon_eta[im]) > 1.05: return False
    if abs(event.muon_MStrack_eta[im]) > 1.05: return False
    n_poss_seg = 0 
    
    #phi = math.pi + event.muon_phi[im]
    phi = math.pi + event.muon_MStrack_phi[im]
    if (phi >= math.pi): phi -= 2*math.pi
    if (phi < -math.pi): phi += 2*math.pi


    if( innerMap.GetBinContent( innerMap.GetXaxis().FindBin( -eta_corrected_R(event, im,4500)) , innerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( middleMap.GetBinContent( middleMap.GetXaxis().FindBin( -eta_corrected_R(event, im,7000)) , middleMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( outerMap.GetBinContent( outerMap.GetXaxis().FindBin( -eta_corrected_R(event, im,9200)) , outerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    
    if n_poss_seg > 1: return False

    #not enough segments to have reconstructed a cosmic --> veto
    return True 
