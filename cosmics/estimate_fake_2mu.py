import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "1cos"
what = "SR"
#what = "SR_inverted"
#what = "1cos_narrow_nochi2_t00overflow"
#what = "1cos_narrow_nochi2"



print "1cos" in what
#full

 
tag = ""+what+"_v5_old"
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data/merged/data1*.root'

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

#bound = 40
bound = 30
nbins = 6 
names = { 
    "botbad_top_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom bad)", "log": 1},   
    
    "botbad_top_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom bad)", "log": 1},
    "botbad_top_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom bad)", "log": 1},
    "botbad_top_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom bad)", "log": 1},
    "botbad_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom bad)", "log": 1},
    
    "botsig_top_bad": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom signal)", "log": 1},
    "botsig_top_bad_chi2": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom signal)", "log": 1},
    "botsig_top_bad_nPres": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom signal)", "log": 1},
    "botsig_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom signal)", "log": 1},
    
    "botsig_top_good": { "nb": nbins, "low": -bound, "high": bound, "label": "t_{0} top (bottom signal)", "log": 1}, ###ONLY FOR VALIDATION
}

h = {}
h = setHistos("", names)


mysteryNums = [4253530762, 646388285, 722317128, 765842373, 926487021]

print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100000: break

    if event.eventNumber in mysteryNums:
        print "****event num", eventNum
        print "basline mu", event.muon_n_basline
        print "basline e", event.electron_n_baseline
        for i in [0,1]:
            print "muon ", i 
            print "sr cuts", event.muon_isSRcuts[i]
            print "isolated", event.muon_isIsolated[i]
            print "id trk",  event.muon_isPassIDtrk[i]
            print "goodqual", event.muon_isGoodQual[i]
            print "cosmic", event.muon_isNotCosmic[i]
            print "cosmic narrow", event.muon_cosmicTag_narrow[i]
            print "cosmic full", event.muon_cosmicTag_full[i]
            print "cosmic mv", event.muon_cosmicTag_MV[i]
            print "signal", event.muon_isSignal[i]
            print "phi", event.muon_phi[i]
            print "tavg", event.muon_t0avg[i]
        print



    #2 muons, opp side of detecdtor
    if event.muon_n_basline > 1 and event.electron_n_baseline == 0 and event.muon_phi[0]*event.muon_phi[1] < 0:
        itop = 0
        ibot = 1
        if event.muon_phi[0] < 0: 
            itop = 1
            ibot = 0
        
        cosmic_top = not event.muon_isNotCosmic[itop]
        cosmic_bot = not event.muon_isNotCosmic[ibot]
        if "narrow" in what:
            cosmic_top = event.muon_cosmicTag_narrow[itop] or event.muon_cosmicTag_MV[itop]
            cosmic_bot = event.muon_cosmicTag_narrow[ibot] or event.muon_cosmicTag_MV[ibot]

         
        # garbage bottom muon
        if event.muon_isSRcuts[ibot] == 1 and (event.muon_isGoodQual[ibot] == 0 or event.muon_isPassIDtrk[ibot]==0):
            #if not cosmic_top:
                #print "found one!" 
                #print "sr cuts", event.muon_isSRcuts[itop]
                #print "isolated", event.muon_isIsolated[itop]
                #print "id trk",  event.muon_isPassIDtrk[itop]
                #print "chi2", event.muon_CBtrack_chi2[itop]
                #print "cosmic", cosmic_top           
            #good except for quality top muon, not cosmic
            if event.muon_isIsolated[itop] == 1 and event.muon_isPassIDtrk[itop] == 1 and not cosmic_top: 
            #if event.muon_isSRcuts[itop] == 1 and event.muon_isIsolated[itop] == 1 and event.muon_isPassIDtrk[itop] == 1 and event.muon_CBtrack_chi2[itop] < 3 and not cosmic_top: 

                if event.muon_isGoodQual[itop] == 1: 
                    h["botbad_top_good"].Fill(event.muon_t0avg[itop])
                # OR all quality vars
                else:
                    if event.muon_isGoodQual[itop] == 0:
                        h["botbad_top_bad"].Fill(event.muon_t0avg[itop])
                    #only chi2 is "bad"
                    if event.muon_CBtrack_chi2[itop] > 3: 
                        h["botbad_top_bad_chi2"].Fill(event.muon_t0avg[itop])
                    
                    #only nPres is "bad"
                    if event.muon_MStrack_nPres[itop] < 3: 
                        h["botbad_top_bad_nPres"].Fill(event.muon_t0avg[itop])
                    
                    #only nPhiLays is "bad"
                    if event.muon_nPhiLays[itop] < 1: 
                        h["botbad_top_bad_nPhiLays"].Fill(event.muon_t0avg[itop])
        
        #else if signal bottom muon (or almost signal, but cosmic for VR) 
        if ("SR" in what and event.muon_isSignal[ibot] and not (cosmic_top or cosmic_bot)) or ("1cos" in what and event.muon_isSRcuts[ibot] == 1 and event.muon_isIsolated[ibot] == 1 and event.muon_isPassIDtrk[ibot] == 1 and event.muon_isGoodQual[ibot] == 1 and (cosmic_top != cosmic_bot)):
            print "found a good bottom muon!" 
            print "sr cuts", event.muon_isSRcuts[itop]
            print "isolated", event.muon_isIsolated[itop]
            print "id trk",  event.muon_isPassIDtrk[itop]
            print "chi2", event.muon_CBtrack_chi2[itop]
            #good except for quality top muon, not cosmic
            #if event.muon_isSRcuts[itop] == 1 and event.muon_isIsolated[itop] == 1 and event.muon_isPassIDtrk[itop] == 1 and event.muon_CBtrack_chi2[itop] < 3 and not cosmic_top: 
            #if event.muon_isSRcuts[itop] == 1 and event.muon_isIsolated[itop] == 1 and event.muon_isPassIDtrk[itop] == 1 and not cosmic_top: 
            if event.muon_isIsolated[itop] == 1 and event.muon_isPassIDtrk[itop] == 1: 
                
                
                ### only fill good if in VR
                if "1cos" in what and event.muon_isSignal[itop] == 1: 
                    h["botsig_top_good"].Fill(event.muon_t0avg[itop])
                # OR all quality vars
                else:
                    if event.muon_isGoodQual[itop] == 0:
                        h["botsig_top_bad"].Fill(event.muon_t0avg[itop])
                    #only chi2 is "bad"
                    if event.muon_CBtrack_chi2[itop] > 3: 
                        h["botsig_top_chi2"].Fill(event.muon_t0avg[itop])
                    
                    #only nPres is "bad"
                    if event.muon_MStrack_nPres[itop] < 3: 
                        h["botsig_top_nPres"].Fill(event.muon_t0avg[itop])
                    
                    #only nPhiLays is "bad"
                    if event.muon_nPhiLays[itop] < 1: 
                        h["botsig_top_nPhiLays"].Fill(event.muon_t0avg[itop]) 
                                      


    if event.eventNumber in mysteryNums:
        print "**** done with ", event.eventNumber
outFile = ROOT.TFile("outputFiles/2mu_estimate_fake_"+tag+".root", "RECREATE")

c = ROOT.TCanvas()
for histo in h:
    h[histo] = addOverflow(h[histo])
    h[histo] = addUnderflowBin(h[histo])

    h[histo].Write()
    h[histo].Draw("hist e")
    c.SaveAs("outputPlots/estimate_fake_2mu/" + tag +"_"+ histo +".pdf")

