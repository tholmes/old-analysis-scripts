import glob
import ROOT
import os

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

c = ROOT.TCanvas()
#ROOT.gPad.SetLogy()

f = ROOT.TFile("outputFiles/2cos_wider_tag.root")

h2n = f.Get("data_2cos_t0_negphi")
h2p = f.Get("data_2cos_t0_posphi")


h2n.SetMarkerColor(ROOT.kRed)
h2n.SetMarkerStyle(24)
h2p.SetMarkerStyle(20)

h2p.GetXaxis().SetTitle("t_{0, avg}")
h2p.GetYaxis().SetTitle("number of muons")
h2p.SetMaximum(3500)

h2p.Draw("pe")
h2n.Draw("pesame")

leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
leg.AddEntry(h2p, "#phi > 0 ", "p")
leg.AddEntry(h2n, "#phi < 0", "p")
leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "2 cosmic tagged muons" )

c.SaveAs("outputPlots/CRCos/t0_2cos_plusminus.pdf")

h1n = f.Get("data_1cos_t0_negphi")
h1p = f.Get("data_1cos_t0_posphi")

h1n.SetMarkerColor(ROOT.kRed)
h1n.SetMarkerStyle(24)
h1p.SetMarkerStyle(20)

h1p.GetXaxis().SetTitle("t_{0, avg}")
h1p.SetMaximum(getMax([h1p,h1n])*1.5)

h1p.Draw("pe")
h1n.Draw("pesame")

leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
leg.AddEntry(h1p, "#phi > 0 ", "p")
leg.AddEntry(h1n, "#phi < 0", "p")
leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")

c.SaveAs("outputPlots/CRCos/t0_1cos_plusminus.pdf")

h1p.SetMarkerColor(ROOT.kGreen+3)
h2p.SetMarkerColor(ROOT.kBlue)
h2p.SetMarkerStyle(24)
h1p.SetMarkerStyle(20)

h1p.GetXaxis().SetTitle("t_{0, avg}")
h1p.GetYaxis().SetTitle("number of muons")
h1p.SetMaximum(2500)

h1p.Draw("pe")
h2p.Draw("pesame")

leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
leg.AddEntry(h1p, "1 #mu events", "p")
leg.AddEntry(h2p, "2 #mu events", "p")
leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "#phi > 0 cosmic tagged muons" )

c.SaveAs("outputPlots/CRCos/t0_plusphi.pdf")

h1n.SetMarkerColor(ROOT.kGreen+3)
h2n.SetMarkerColor(ROOT.kBlue)
h2n.SetMarkerStyle(24)
h1n.SetMarkerStyle(20)

h1n.GetXaxis().SetTitle("t_{0, avg}")
h2p.GetYaxis().SetTitle("number of muons")
h1n.SetMaximum(25000)

h1n.Draw("pe")
h2n.Draw("pesame")

leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
leg.AddEntry(h1n, "1 #mu events", "p")
leg.AddEntry(h2n, "2 #mu events", "p")
leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "#phi < 0 cosmic tagged muons" )

c.SaveAs("outputPlots/CRCos/t0_negphi.pdf")

