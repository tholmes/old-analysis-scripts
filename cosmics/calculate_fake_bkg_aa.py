import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

doValidate=True
saveGraph =True
debug=False
assymErrors=True

#saveFlag = "noangle_SR_nochi2_v4"
saveFlag = "noangle_1cos_narrow_nochi2_v4"
#saveFlag = "1cos_narrow_nochi2_v4_40"
#f = ROOT.TFile("outputFiles/estimate_fake_chi2npres_"+saveFlag+".root")
f = ROOT.TFile("outputFiles/estimate_fake_"+saveFlag+".root")
c = ROOT.TCanvas()

quals = ["","_nPres","_nPhiLays","_chi2"]
#quals = [""]

for qual in quals:
    #rgood = f.Get("R_good")
    h_2mu_top = f.Get("_t0_2mu_top_bad"+qual)
    h_2mu_bot = f.Get("_t0_2mu_bot_bad"+qual)

    #Wilson interval
    top_good = f.Get("_t0_1mu_top_good")
    top_bad = f.Get("_t0_1mu_top_bad"+qual)
    rgood_top = ROOT.TGraphAsymmErrors()
    rgood_top.Divide(top_good, top_bad, "pois w")
    rgood_top.Draw("pela")
    rgood_top.SetName("rgood_top"+saveFlag+qual)
    rgood_top.SetTitle(saveFlag+qual+"top;t_{0, avg}")
    rgood_top.SetLineColor(ROOT.kRed)
    rgood_top.SetMarkerColor(ROOT.kRed)
    c.SaveAs("outputPlots/estimate_fake/"+saveFlag+qual+"_rgood_top.pdf")
    c.Clear()
    
    bot_good = f.Get("_t0_1mu_bot_good")
    bot_bad = f.Get("_t0_1mu_bot_bad"+qual)
    rgood_bot = ROOT.TGraphAsymmErrors()
    rgood_bot.Divide(bot_good, bot_bad, "pois w")
    rgood_bot.Draw("pela")
    rgood_bot.SetName("rgood_bot"+saveFlag+qual)
    rgood_bot.SetTitle(saveFlag+qual+"bot;t_{0, avg}")
    rgood_bot.SetLineColor(ROOT.kRed)
    rgood_bot.SetMarkerColor(ROOT.kRed)
    c.SaveAs("outputPlots/estimate_fake/"+saveFlag+qual+"_rgood_bot.pdf")
    c.Clear()

    nBins = h_2mu_top.GetNbinsX()
    nPoints = rgood_top.GetN()
    print "hist bins ", nBins
    print "graph points ", nPoints

    top_total = 0
    top_total_up = 0
    top_total_down = 0
    
    bot_total = 0
    bot_total_up = 0
    bot_total_down = 0
    
    #graph points count from 0, histogram bins count from 1 -_-
    for b in xrange(1,nBins+1):
    #for b in xrange(2,nBins):
            
        gb = b -1
        
        #central value at point
        x = ROOT.Double()
        y = ROOT.Double()
        rgood_top.GetPoint(gb,x,y)

        # histogram bin with that x value
        bn = h_2mu_top.FindBin(x)
        print bn, h_2mu_top.GetBinLowEdge(bn)
        
        if abs(h_2mu_top.GetBinLowEdge(bn)) > 30: continue

        top_total      += h_2mu_top.GetBinContent(bn) * y   
        top_total_up   += h_2mu_top.GetBinContent(bn) * (y + rgood_top.GetErrorYhigh(gb))
        top_total_down += h_2mu_top.GetBinContent(bn) * (y - rgood_top.GetErrorYlow(gb))
        

        bot_total      += h_2mu_bot.GetBinContent(bn) * y   
        bot_total_up   += h_2mu_bot.GetBinContent(bn) * (y + rgood_bot.GetErrorYhigh(gb))
        bot_total_down += h_2mu_bot.GetBinContent(bn) * (y - rgood_bot.GetErrorYlow(gb))
        

        '''
        if debug:
            print "point number, x, y ",gb,x,y 
            print "histo bin with x ", bn
            print "hist bin content ", h_2mu.GetBinContent(bn)
            print "hist low edge ", h_2mu.GetBinLowEdge(bn)
            print "hist up edge " , h_2mu.GetBinLowEdge(bn+1)
            print "rgood bound up ", y + rgood_w.GetErrorYhigh(gb)
            print "rgood error up ", rgood_w.GetErrorYhigh(gb)
            print "rgood bound down ", y - rgood_w.GetErrorYlow(gb)
            print "rgood error down ", rgood_w.GetErrorYlow(gb)
            print
        '''

    print saveFlag, qual
    print "estimated number of events top: ",top_total
    print "estimated number of events bot: ",bot_total
    if assymErrors:
        print "top up error:    value                 ",top_total_up - top_total
        print "                 percent               ",abs(top_total_up - top_total)*100/top_total if top_total!=0 else "--"
        print "top down error:  value                 ",top_total - top_total_down
        print "                 percent               ", abs(top_total_down - top_total)*100/top_total if top_total!=0  else "--"

        print 
        print "top upper bound:                ",top_total_up
        print "top lower bound:                ",top_total_down
        print
        
        print "bot up error:    value                 ",bot_total_up - bot_total
        print "                 percent               ",abs(bot_total_up - bot_total)*100/bot_total if bot_total!=0 else "--"
        print "bot down error:  value                 ",bot_total - bot_total_down
        print "                 percent               ", abs(bot_total_down - bot_total)*100/bot_total if bot_total!=0  else "--"

        print 
        print "bot upper bound:                ",bot_total_up
        print "bot lower bound:                ",bot_total_down
        print
    
    else:
        up_err = total_up - total
        down_err = total - total_down
        if up_err > down_err:
            rgood_err = up_err
        else:
            rgood_err = down_err
        
        if total!=0:
            twomu_err = 1.0/math.sqrt(h_2mu.Integral())
        else:
            twomu_err = 999

        total_err = math.sqrt(rgood_err**2 + (twomu_err)**2)

        print "rgood error  value                 ",rgood_err
        print "             percent               ",rgood_err*100/total if total!=0 else "--"
        print "2mu error    value                 ",twomu_err
        print "             percent               ",twomu_err*100/total if total!=0 else "--"
        print "total error  value                 ",total_err
        print "             percent               ",total_err*100/total if total!=0 else "--"
        print        
    
    if doValidate and "SR" not in saveFlag:
        actual = f.Get("_t0_2mu_good_lead").Integral()
        print "actual number of events ", actual
        #if actual != 0: print "percent difference      ", abs(actual-total)*100/actual


    if saveGraph:
        if not os.path.isfile('outputFiles/rgood.root'):
            of = ROOT.TFile("outputFiles/rgood.root", "RECREATE")
        else:
            of = ROOT.TFile("outputFiles/rgood.root", "UPDATE")
            
        rgood_top.Write()
        rgood_bot.Write()
        of.Write()
        of.Close()

