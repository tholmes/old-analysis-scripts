import ROOT
import array

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.gROOT.SetBatch(1)

f = ROOT.TFile("outputFiles/2cos_data.root")

color1 = ROOT.TColor.GetColor('#0F7173')
color2 = ROOT.TColor.GetColor('#00C2D1')
color3 = ROOT.TColor.GetColor('#390099')

NRGBs = 5
NCont = 99
stops = [ 0.0, 0.20, 0.40, 0.60, 0.80, 1.00 ]

##green fade -- not bad
#red = [   (243./255.), (198./255.), (136./255.), (26./255.) , (17./255.)  ]
#green = [ (233./255.), (218./255.), (212./255.), (147./255.), (75./255.)]
#blue = [  (210./255.), (191./255.), (152./255.), (111./255.), (95./255.)]

##rainbowish fade
#end light gree
#red = [   (244./255.), (27./255.),   (6./255.), (255./255.), (239./255.)  ]
#green = [ (255./255.), (154./255.), (214./255.), (196./255.), (71./255.)]
#blue = [  (214./255.), (170./255.), (160./255.), (61./255.), (111./255.)]

red = [   (236./255.), (27./255.),   (6./255.), (255./255.), (239./255.)  ]
green = [ (228./255.), (154./255.), (214./255.), (196./255.), (71./255.)]
blue = [  (241./255.), (170./255.), (160./255.), (61./255.), (111./255.)]

stopsArray = array('d', stops)
redArray = array('d', red)
greenArray = array('d', green)
blueArray = array('d', blue)
ROOT.TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)
ROOT.gStyle.SetNumberContours(NCont)
### cosmic tag

c = ROOT.TCanvas("c","c",600,600)
ROOT.gPad.SetRightMargin(0.08)
#ROOT.gPad.SetLeftMargin(3.2)

h2n = f.Get("data_2cos_t0_negphi")
h2p = f.Get("data_2cos_t0_posphi")


h2n.SetLineColor(color1)
h2n.SetLineWidth(4)
h2p.SetLineColor(color2)
h2p.SetLineWidth(4)
h2p.SetLineStyle(2)
h2p.SetMarkerSize(0)
h2n.SetMarkerSize(0)

h2p.GetXaxis().SetTitle("t_{0}^{avg} [ns]")
h2p.GetYaxis().SetTitle("Number of Muons")
h2p.GetYaxis().SetTitleOffset(1.7)
h2p.GetXaxis().SetNdivisions(607)
h2p.GetYaxis().SetNdivisions(606)
h2p.SetMaximum(3500)

h2p.Draw("histe")
h2n.Draw("histesame")

leg = ROOT.TLegend(0.70,0.80,0.90,0.92)
leg.SetTextFont(42)
leg.AddEntry(h2p, "#phi_{#mu} > 0 ", "l")
leg.AddEntry(h2n, "#phi_{#mu} < 0", "l")
leg.Draw("same")
leg.SetFillStyle(0)
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
text.DrawLatex(0.20,0.84, "2 cosmic-tagged muons" )
text2 = ROOT.TLatex()
text2.SetNDC()
text2.SetTextSize(0.04)
text2.DrawLatex(0.20,0.79, "#sqrt{s} = 13 TeV, 139 fb^{-1}" )

c.SaveAs("outputPlots/CRCos/2cos_plusminus_paper.pdf")
c.SaveAs("outputPlots/CRCos/2cos_plusminus_paper.eps")
c.SaveAs("outputPlots/CRCos/2cos_plusminus_paper.root")
c.SaveAs("outputPlots/CRCos/2cos_plusminus_paper.C")
#c = ROOT.TCanvas()
#ROOT.gStyle.SetPalette(ROOT.kBird)
#ROOT.TColor.InvertPalette()

c.Clear()
ROOT.gPad.SetRightMargin(0.2)
h2 = f.Get("data_sumEta_dPhi_min_corr_zoom")
h2.RebinX(2)
h2.RebinY(2)
h2.GetZaxis().SetTitle("Number of Muons")
h2.GetYaxis().SetTitle("#Delta #phi_{#mu,seg}")
h2.GetXaxis().SetTitle("#Sigma #eta_{#mu,seg}")
h2.GetXaxis().SetNdivisions(605)
h2.GetYaxis().SetNdivisions(608)
h2.GetZaxis().SetTitleOffset(1.3)
h2.GetXaxis().SetTitleOffset(1.4)
h2.GetYaxis().SetTitleOffset(1.6)
h2.Draw("COLZ")
ROOT.ATLASLabel(0.47,0.89, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
text.DrawLatex(0.46,0.85, "#sqrt{s} = 13 TeV, 139 fb^{-1}" )
ROOT.gPad.SetLogz()

b = ROOT.TBox(0,0,0.018,0.25)
b.SetFillStyle(0)
b.SetLineWidth(4)
b.SetLineColor(ROOT.kBlack)
b.Draw()

c.Modified()
c.Update()
c.SaveAs("outputPlots/CRCos/sumEta_dPhi_paper.pdf")
c.SaveAs("outputPlots/CRCos/sumEta_dPhi_paper.eps")
c.SaveAs("outputPlots/CRCos/sumEta_dPhi_paper.root")
c.SaveAs("outputPlots/CRCos/sumEta_dPhi_paper.C")

c.Clear()



