import glob
import ROOT
import os

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(0)

cos_file = ROOT.TFile("../outputFiles/cosmics/cosmics_histos.root")

n_muons = cos_file.Get("n_muons")
n_muons_pt100 = cos_file.Get("n_muons_pt100")
n_muons_pt25 = cos_file.Get("n_muons_pt25")

can = ROOT.TCanvas()
n_muons_pt100.SetLineColor(ROOT.kMagenta+1)
n_muons_pt25.SetLineColor(ROOT.kBlue)
n_muons_pt100.GetXaxis().SetTitle("muons/event")

leg = ROOT.TLegend(0.40,0.65,0.80,0.92)
leg.AddEntry(n_muons, "cosmic muons, p_{T} > 10 GeV", "l")
leg.AddEntry(n_muons_pt25, "cosmic muons, p_{T} > 25 GeV", "l")
leg.AddEntry(n_muons_pt100, "cosmic muons, p_{T} > 100 GeV", "l")

n_muons_pt100.Draw("hist")
n_muons.Draw("histsame")
n_muons_pt25.Draw("histsame")

leg.Draw("same")

raw_input("...")
can.SaveAs("../outputFiles/cosmics/plots/n_muons.pdf")
