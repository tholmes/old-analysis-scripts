import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("cosmic_helpers.py")
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")

tag = "data_dv"

doPrompt = True

if not doPrompt: AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/2ndCos/*')
else: AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/prompt_skim.root')

t_cos = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t_cos.Add(filename)


maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names = { 
    "min_sEta": { "nb": 100, "low": 0, "high": .1, "label": "#Sigma #eta_{min_{mu, seg}}", "log": 0},
    "min_dPhi": { "nb": 100, "low": 0, "high": .2, "label": "#Delta #phi_{min_{mu, seg}}", "log": 0},
    "cos_chi2": { "nb": 20, "low": 0, "high": 5, "label": "#chi^{2}", "log": 0},
    "mu_chi2": { "nb": 20, "low": 0, "high": 5, "label": "#chi^{2}", "log": 0},
    "mu_Eloss": { "nb": 40, "low": 0, "high": 10, "label": "Energy Loss", "log": 0},
    "mu_Eloss_sigma": { "nb": 40, "low": 0, "high": 10, "label": "Energy Loss Sigma", "log": 0},
    "cos_Eloss": { "nb": 40, "low": 0, "high": 10, "label": "Energy Loss", "log": 0},
    "cos_Eloss_sigma": { "nb": 40, "low": 0, "high": 10, "label": "Energy Loss Sigma", "log": 0},
    "cos_etaLayers": { "nb": 4, "low": 0, "high": 4, "label": "#eta layers (cos)", "log": 0},
    "mu_etaLayers": { "nb": 4, "low": 0, "high": 4, "label": "#eta layers (mu)", "log": 0},
    "cos_phiLayers": { "nb": 4, "low": 0, "high": 4, "label": "#phi layers (cos)", "log": 0},
    "mu_phiLayers": { "nb": 4, "low": 0, "high": 4, "label": "#phi layers (mu)", "log": 0},
    "cos_presHits": { "nb": 10, "low": 0, "high": 10, "label": "pres hits (cos)", "log": 0},
    "mu_presHits": { "nb": 10, "low": 0, "high": 10, "label": "pres hits (mu)", "log": 0},
    "cos_author": { "nb": 10, "low": 0, "high": 10, "label": "author (mu)", "log": 0},
    "mu_author": { "nb": 10, "low": 0, "high": 10, "label": "author (cos)", "log": 0},
    "cos_qop_err": { "nb": 20, "low": 0, "high": .3*10**-6, "label": "q/p err", "log": 0},
    "mu_qop_err": { "nb": 20, "low": 0, "high": .3*10**-6, "label": "q/p err", "log": 0},
    "cos_dPhi": { "nb": 20, "low": 0, "high": 1, "label": "#Delta #phi (MS, ID)", "log": 0},
    "mu_dPhi": { "nb": 20, "low": 0, "high": 1, "label": "#Delta #phi (MS, ID)", "log": 0},
}

names_2d ={
    "ms_x_y" : { "nb_x": 50, "low_x": -15000, "high_x": 15000, "label_x": "x_{ms}", "nb_y": 50, "low_y": -15000, "high_y": 15000, "label_y": "y_{ms}", "log":0},
    "z0_z0" : { "nb_x": 50, "low_x": -500, "high_x": 500, "label_x": "d_{0, cos}", "nb_y": 50, "low_y": -500, "high_y": 500, "label_y": "d_{0, #mu}", "log":0},
    "cos_eta_phi" : { "nb_x": 60, "low_x": -5, "high_x": 5, "label_x": "#eta_{cos}", "nb_y": 60, "low_y": -4, "high_y": 4, "label_y": "#phi_{cos}", "log":0},
    "mu_eta_phi" : { "nb_x": 60, "low_x": -5, "high_x": 5, "label_x": "#eta_{#mu}", "nb_y": 60, "low_y": -4, "high_y": 4, "label_y": "#phi_{#mu}", "log":0},
    "mu_idphi_msphi" : { "nb_x": 40, "low_x": -4, "high_x": 4, "label_x": "#phi_{ID track}", "nb_y": 40, "low_y": -4, "high_y": 4, "label_y": "#phi_{MS track}", "log":0},
    "cos_idphi_msphi" : { "nb_x": 40, "low_x": -4, "high_x": 4, "label_x": "#phi_{ID track}", "nb_y": 40, "low_y": -4, "high_y": 4, "label_y": "#phi_{MS track}", "log":0},
    "cos_ideta_mseta" : { "nb_x": 40, "low_x": -5, "high_x": 5, "label_x": "#eta_{ID track}", "nb_y": 40, "low_y": -5, "high_y": 5, "label_y": "#eta_{MS track}", "log":0},
    "mu_ideta_mseta" : { "nb_x": 40, "low_x": -5, "high_x": 5, "label_x": "#eta_{ID track}", "nb_y": 40, "low_y": -4, "high_y": 4, "label_y": "#eta_{MS track}", "log":0},
    "sumEta_dPhi_each" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_comb" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_each_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_comb_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
}



h = {}
h = setHistos(tag, names)
h2 = {}
h2 = setHistos2D(tag, names_2d)

events = [263007427L, 100811046L, 1618963497L, 581535532L, 3065574093L, 3310073945L, 127002089L, 673126560L, 2713174979L, 2978485396L, 3360813554L, 2368168110L, 639015584L, 1027860319L, 3166844131L, 3020937111L, 2234323959L, 3145370564L, 2325989980L, 1821716899L, 3763559523L, 2149094625L, 1191022016L, 883775519L, 864781245L, 3651352314L, 2212682464L, 293959127L, 1691295250L, 1773991425L, 866752213L, 3086796438L, 2452774838L, 1222598154L, 2629773157L, 1009018454L, 2667041321L, 2850205821L, 1114721568L, 2726804734L, 1577077213L]

print t_cos.GetEntries()
nevents = 0
for event in t_cos:
    if nevents % 10000 == 0: print "processing ", nevents
    nevents+=1
    
    if nevents > 100000: break    
    
    imu = -1
    icos = -1
    if doPrompt: 
        if len(event.muon_pt) != 1 and len(event.lepton_pt)!=1: continue
    for im in xrange(len(event.muon_pt)):
        #if preselect_muon(event, im, tight=True):
        if preselect_muon(event, im, tight=True, dod0=False):
            
            cosmicTag = cosTag(event, im, .012 , .18) 
            matVeto = materialVeto(event, im, innerMap, middleMap, outerMap)

            if cosmicTag or matVeto: 
                icos = im
            else:
                imu = im
            if doPrompt and imu == -1: continue
        h2["z0_z0"].Fill(event.muon_IDtrack_z0[icos], event.muon_IDtrack_z0[imu])
        h2["cos_eta_phi"].Fill(event.muon_eta[icos], event.muon_phi[icos])
        h2["mu_eta_phi"].Fill(event.muon_eta[imu], event.muon_phi[imu])
    
    if doPrompt:
        if imu == -1: continue
        icos = 0 #doesn't matter

    mu_phiLays = 0 
    cos_phiLays = 0 
    for ms in xrange(len(event.muon_msSegment_x)):
        if event.muon_msSegment_muonIndex[ms] == icos:
            h["cos_etaLayers"].Fill(event.muon_msSegment_nTrigEtaLays[ms])
            h["cos_presHits"].Fill(event.muon_msSegment_nPresHits[ms])
            cos_phiLays += event.muon_msSegment_nPhiLays[ms] 
        if event.muon_msSegment_muonIndex[ms] == imu:
            h["mu_etaLayers"].Fill(event.muon_msSegment_nTrigEtaLays[ms])
            h["mu_presHits"].Fill(event.muon_msSegment_nPresHits[ms])
            mu_phiLays += event.muon_msSegment_nPhiLays[ms] 

    h["cos_phiLayers"].Fill(cos_phiLays)
    h["mu_phiLayers"].Fill(mu_phiLays)
    
    v_mu = ROOT.TVector3()
    v_mu.SetPtEtaPhi(event.muon_pt[imu], event.muon_eta[imu], event.muon_phi[imu])
    
    v_cos = ROOT.TVector3()
    v_cos.SetPtEtaPhi(event.muon_pt[icos], event.muon_eta[icos], event.muon_phi[icos])
    
    h["cos_chi2"].Fill(event.muon_CBtrack_chi2[icos])
    h2["cos_idphi_msphi"].Fill(event.muon_IDtrack_phi[icos], event.muon_MStrack_phi[icos])
    h2["cos_ideta_mseta"].Fill(event.muon_IDtrack_eta[icos], event.muon_MStrack_eta[icos])
    h["cos_Eloss"].Fill(event.muon_MStrack_ELoss[icos])
    h["cos_Eloss_sigma"].Fill(event.muon_MStrack_ELossSigma[icos])
    h["mu_chi2"].Fill(event.muon_CBtrack_chi2[imu])
    h2["mu_ideta_mseta"].Fill(event.muon_IDtrack_eta[imu], event.muon_MStrack_eta[imu])
    h2["mu_idphi_msphi"].Fill(event.muon_IDtrack_phi[imu], event.muon_MStrack_phi[imu])
    h["mu_Eloss"].Fill(event.muon_MStrack_ELoss[imu])
    h["mu_Eloss_sigma"].Fill(event.muon_MStrack_ELossSigma[imu])
    h["mu_author"].Fill(event.muon_author[imu])
    h["cos_author"].Fill(event.muon_author[icos])
    h["mu_qop_err"].Fill(event.muon_CBtrack_qop_err[imu])
    h["cos_qop_err"].Fill(event.muon_CBtrack_qop_err[icos])
    h["cos_dPhi"].Fill( abs(event.muon_MStrack_phi[icos] - event.muon_IDtrack_phi[icos]) )
    h["mu_dPhi"].Fill( abs(event.muon_MStrack_phi[imu] - event.muon_IDtrack_phi[imu]) )

    if not doPrompt:   
        min_sEta = 999
        min_dPhi = 999
     
        min_sEta_comb = 999
        min_dPhi_comb = 999
        for iS in xrange(len(event.msSegment_x)):
            h2["ms_x_y"].Fill(event.msSegment_x[iS], event.msSegment_y[iS])
            
            v_ms = ROOT.TVector3(event.msSegment_x[iS], event.msSegment_y[iS], event.msSegment_z[iS])
            
            eta_corr = eta_corrected(event,v_mu,iS,imu) 
            sum_eta_corr = abs( eta_corr + v_ms.Eta() )
            if sum_eta_corr < min_sEta : min_sEta = sum_eta_corr
            
            dPhi =  abs(abs(v_ms.DeltaPhi(v_mu)) - math.pi)
            if dPhi < min_dPhi: min_dPhi = dPhi
            
            if sum_eta_corr < min_sEta_comb and dPhi < min_dPhi_comb :
                min_sEta_comb = sum_eta_corr 
                min_dPhi_comb = dPhi
            

        h["min_sEta"].Fill(min_sEta)
        h["min_dPhi"].Fill(min_dPhi)
        h2["sumEta_dPhi_each"].Fill(min_sEta, min_dPhi)
        h2["sumEta_dPhi_comb"].Fill(min_sEta_comb, min_dPhi_comb)
        h2["sumEta_dPhi_each_zoom"].Fill(min_sEta, min_dPhi)
        h2["sumEta_dPhi_comb_zoom"].Fill(min_sEta_comb, min_dPhi_comb)

append = ""
if doPrompt: append = "prompt_"

c = ROOT.TCanvas()
outFile = ROOT.TFile("outputFiles/2ndcos.root", "RECREATE")

for histo in h:
    h[histo] = addOverflow(h[histo])
    if names[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    h[histo].Draw("hist")
    h[histo].Write()
    c.SaveAs("outputPlots/2ndCos/"+ append + histo + ".pdf")

for histo in h2:
    h2[histo] = addOverflow2D(h2[histo])
    if names_2d[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    h2[histo].Draw("COLZ")
    h2[histo].Write()
    c.SaveAs("outputPlots/2ndCos/"+ append + histo + ".pdf")

outFile.Write()

if not doPrompt:
    toplot = ["chi2","Eloss","Eloss_sigma","etaLayers","phiLayers","presHits","author", "qop_err", "dPhi"]
    for p in toplot:
        c.Clear()

        h_max = max(h["mu_"+p].GetMaximum(), h["cos_"+p].GetMaximum())
        
        h["mu_"+p].SetLineColor(ROOT.kRed)
        h["mu_"+p].GetYaxis().SetRangeUser(0,h_max+5)
        h["mu_"+p].Draw("hist")
        h["cos_"+p].Draw("hist same")
        c.SaveAs("outputPlots/2ndCos/comp_"+p+".pdf")
        
         
        

