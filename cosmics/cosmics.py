import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")
ROOT.gROOT.SetBatch(1)



tag = "300_slep"
append = ""

if tag == "cosmic":
    AODFiles = ROOT.TFile('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.user.kdipetri.data16_cos.mc16d_031519_trees.root/all.root')
    t_cos = AODFiles.Get("trees_SRCOS_")
    tag = "cosmic"

if tag == "300_slep":
    #AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/051619/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_*.SUSY15_mc16d_051619_trees.root/*')
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/user.tholmes.mc16_13TeV.399047.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_0p1ns.DAOD_RPVLL_mc16d_v3.0_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "300_stau":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/051619/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_*.SUSY15_mc16d_051619_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "data":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.1_Nov2019_AllTracks/user.lhoryn.data17_13TeV.periodI.physics_Main.111519_alltracks_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names = { 
    "muon_cutflow": { "nb": 17, "low": 0, "high": 17, "label": "cutflow (muons)", "log": 1 },
    "cutflow": { "nb": 17, "low": 0, "high": 17, "label": "cutflow (events)", "log": 1 },
    "dR_mu":   { "nb": 200, "low": 0, "high": 5, "label": "#Delta R (2 muons)", "log": 0},   
    "dR_mu_zoom":   { "nb": 200, "low": 0, "high": .3, "label": "#Delta R (2 muons)", "log": 0},   
    "dR_leadmu_allms":   { "nb": 200, "low": 0, "high": 5, "label": "#Delta R (lead muon, all MSSegments)", "log": 1},   
    "dR_leadmu_allid":   { "nb": 200, "low": 0, "high": 5, "label": "#Delta R (lead muon, all ID tracks)", "log": 1},   
    "lead_dR_mbbid":   { "nb": 200, "low": 0, "high": 5, "label": "#Delta R (lead muon and most back to back ID track)", "log": 1},   
    "lead_dR_mbbid_zoom":   { "nb": 200, "low": 0, "high": .5, "label": "dR (lead muon and most back to back ID track)", "log": 1},   
    "lead_dR_mbbms":   { "nb": 200, "low": 0, "high": 5, "label": "#Delta R (lead muon and most back to back MS Segment)", "log": 1},   
    "lead_dR_mbbms_zoom":   { "nb": 200, "low": 0, "high": .5, "label": "#Delta R (lead muon and most back to back MS Segment)", "log": 1},   
    "dphi_mu": { "nb": 100, "low": 0, "high": 1, "label": "#Delta #phi (2 muons)", "log": 0},   
    "theta_corr": { "nb": 100, "low": -4, "high": 4, "label": "#theta_{corr}", "log": 0},   
    "theta_orig": { "nb": 100, "low": -4, "high": 4, "label": "#theta_{orig}", "log": 0},   
    "eta_orig": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{orig}", "log": 0},   
    "eta_corr": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{corr}", "log": 0},   
    "dphi_mu_zoom": { "nb": 100, "low": -.3, "high": .3, "label": "#Delta #phi (2 muons)", "log": 0},   
    "dPhi_leadmu_allms": { "nb": 100, "low": 0, "high": 6, "label": "#Delta #phi (lead muon, all MSSegments)", "log": 0},   
    "dPhi_leadmu_allid": { "nb": 100, "low": 0, "high": 6, "label": "#Delta #phi (lead muon, all ID Tracks)", "log": 0},   
    "sumEta_mu": { "nb": 100, "low": 0, "high": 6, "label": "#Sigma #eta (2 muons)", "log": 0},
    "sumEta_leadmu_allms": { "nb": 100, "low": 0, "high": 6, "label": "#Sigma #eta (lead muon, all MSSegments)", "log": 0},
    "sumEta_leadmu_allms_corr": { "nb": 100, "low": 0, "high": 6, "label": "#Sigma #eta (lead muon, all MSSegments)", "log": 0},
    "sumEta_leadmu_allid": { "nb": 100, "low": 0, "high": 6, "label": "#Sigma #eta (lead muon, all ID tracks)", "log": 0},
    "sumEta_mu_zoom": { "nb": 100, "low": 0, "high": .3, "label": "#Sigma #eta (2 muons)", "log": 0},
    "dd0": { "nb": 100, "low": 0, "high": 2, "label": "#Delta d_{0}", "log": 0},   
    "sumd0": { "nb": 100, "low": 0, "high": 6, "label": "#Sigma d_{0}", "log": 0},   
      
      
    "orig_z0": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0} before cuts", "log": 0},   
       
    "all_pt_mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, all}", "log": 0},   
    "all_d0_mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, all}", "log": 0},   
    "all_d0_mu_nops": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, all}", "log": 0},   
    "all_d0_mu_notcos": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, all}", "log": 0},   
    "all_z0_mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, all}", "log": 0},   
    "all_eta_mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{all}", "log": 0},   
    "all_phi_mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{all}", "log": 0},   
    
    "pt_1mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, 1#mu}", "log": 0},   
    "d0_1mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, 1#mu}", "log": 0},   
    "z0_1mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, 1#mu}", "log": 0},   
    "eta_1mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{1#mu}", "log": 0},   
    "phi_1mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{1#mu}", "log": 0},   
    
    "lead_pt_mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, lead}", "log": 0},   
    "lead_d0_mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, lead}", "log": 0},   
    "lead_z0_mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, lead}", "log": 0},   
    "lead_eta_mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{lead}", "log": 0},   
    "lead_phi_mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{lead}", "log": 0},   
    "lead_qoverpsig_mu": { "nb": 100, "low": 0, "high": 10, "label": "Q/P sig_{lead}", "log": 1},   
    "lead_chi2_mu": { "nb": 100, "low": 0, "high": 60, "label": "#chi^{2}_{lead}", "log": 1},   
    "lead_chi2_trigger_mu": { "nb": 100, "low": 0, "high": 60, "label": "#chi^{2}_{lead}", "log": 1},   

    "sublead_pt_mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, sublead}", "log": 0},   
    "sublead_d0_mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, sublead}", "log": 0},   
    "sublead_z0_mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, sublead}", "log": 0},   
    "sublead_eta_mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{sublead}", "log": 0},   
    "sublead_phi_mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{sublead}", "log": 0},   

    "failed_pt_mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, failed}", "log": 0},   
    "failed_d0_mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, failed}", "log": 0},   
    "failed_z0_mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, failed}", "log": 0},   
    "failed_eta_mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{failed}", "log": 0},   
    "failed_phi_mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{failed}", "log": 0},   
    "failed_qoverpsig_mu": { "nb": 100, "low": 0, "high": 10, "label": "Q/P sig_{failed}", "log": 1},   
    "failed_chi2_mu": { "nb": 100, "low": 0, "high": 60, "label": "#chi^{2}_{failed}", "log": 1},   
    
    "failed_full_pt_mu": { "nb": 100, "low": 0, "high": 1000, "label": "p_{T, failed_full}", "log": 0},   
    "failed_full_d0_mu": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, failed_full}", "log": 0},   
    "failed_full_z0_mu": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, failed_full}", "log": 0},   
    "failed_full_eta_mu": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{failed_full}", "log": 0},   
    "failed_full_phi_mu": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{failed_full}", "log": 0},   
    "failed_full_qoverpsig_mu": { "nb": 100, "low": 0, "high": 10, "label": "Q/P sig_{failed_full}", "log": 1},   
    "failed_full_chi2_mu": { "nb": 100, "low": 0, "high": 60, "label": "#chi^{2}_{failed_full}", "log": 1},   
    
    "idt_pt": { "nb": 100, "low": 0, "high": 600, "label": "p_{T, ID tracks}", "log": 1},   
    "idt_pt-1": { "nb": 100, "low": 0, "high": 1.2, "label": "#frac{1}/{p_{T, ID tracks}}", "log": 1},   
    "idt_d0": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, ID tracks}", "log": 0},   
    "idt_z0": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, ID tracks}", "log": 0},   
    "idt_eta": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{ID tracks}", "log": 0},   
    "idt_phi": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{ID tracks}", "log": 0},   
    "idt_n": { "nb": 40, "low": 0, "high": 40, "label": "N_{ID tracks}", "log": 1},   
    "idt_frac3Pix": { "nb": 20, "low": 0, "high": 1, "label": "frac with #geq 3 pixel hits", "log": 1},   
    "idt_nSiHits": { "nb": 20, "low": 0, "high": 20, "label": "N_{Silicon Hits}", "log": 1},   
    "idt_nPixHits": { "nb": 20, "low": 0, "high": 20, "label": "N_{Pixel Hits}", "log": 1},   
    "idt_nSCTHits": { "nb": 20, "low": 0, "high": 20, "label": "N_{SCT Hits}", "log": 1},   
    "idt_isLRT": { "nb": 2, "low": 0, "high": 2, "label": "isLRT", "log": 1},   
    "idt_pt_3pix": { "nb": 100, "low": 0, "high": 600, "label": "p_{T, ID tracks}", "log": 1},   
    "idt_pt-1_3pix": { "nb": 100, "low": 0, "high": 1.2, "label": "#frac{1}/{p_{T, ID tracks}}", "log": 1},   
    "idt_d0_3pix": { "nb": 100, "low": -300, "high": 300, "label": "d_{0, ID tracks}", "log": 0},   
    "idt_z0_3pix": { "nb": 100, "low": -1000, "high": 1000, "label": "z_{0, ID tracks}", "log": 0},   
    "idt_eta_3pix": { "nb": 100, "low": -5, "high": 5, "label": "#eta_{ID tracks}", "log": 0},   
    "idt_phi_3pix": { "nb": 100, "low": -4, "high": 4, "label": "#phi_{ID tracks}", "log": 0},   
    
    "n_muons" : { "nb": 10, "low": 0, "high": 10, "label": "number of muons", "log": 1},
    "n_msSeg" : { "nb": 10, "low": 0, "high": 10, "label": "number of MS Segments", "log": 1},
    "n_ps_ev" : { "nb": 10, "low": 0, "high": 10, "label": "number of events with >1 preselected muon", "log": 1},
    "n_ps_mu" : { "nb": 10, "low": 0, "high": 10, "label": "number of preselected muons", "log": 1},
    "n_passDR_id" : { "nb": 10, "low": 0, "high": 10, "label": "number of events passing #Delta R_{ID} cut", "log": 1},
    "n_passDR_ms" : { "nb": 10, "low": 0, "high": 10, "label": "number of events passing #Delta R_{MS} cut", "log": 1},
    "pass_trigger" : { "nb": 3, "low": 0, "high": 3, "label": "number of events passing MSOnly trigger, no PS muon req", "log": 1},
    "pass_trigger_ps" : { "nb": 3, "low": 0, "high": 3, "label": "number of events passing MSOnly trigger with >0 PS muons", "log": 1},
    "cosTag" : { "nb": 3, "low": 0, "high": 3, "label": "muon tagged as cosmic?", "log": 1},
    "cosTag_n" : { "nb": 5, "low": 0, "high": 5, "label": "n cosmic tagged muons", "log": 1},
    "cosTag_ev" : { "nb": 3, "low": 0, "high": 3, "label": "event tagged as cosmic?", "log": 1}
}

names_2d ={
    "sumEta_dPhi_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_min_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_min_corr_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_min_corr_zoom_2" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .5, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_corr_zoom_2" : { "nb_x": 100, "low_x": 0, "high_x": 2., "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .5, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_corr_zoom" : { "nb_x": 100, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "deltaEta_dPhi" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Delta #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_corr" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "deltaEta_dPhi_corr" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Delta #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_min" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "sumEta_dPhi_min_corr" : { "nb_x": 100, "low_x": 0, "high_x": 5, "label_x": "#Sigma #eta_{#mu, seg}", "nb_y": 100, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi_{#mu, seg}", "log":0},
    "ms_eta_phi" : { "nb_x": 100, "low_x": -3, "high_x": 3, "label_x": "#eta_{MSSeg}", "nb_y": 100, "low_y": -3.5, "high_y": 3.5, "label_y": "#phi_{MSSeg}", "log":1},
    "ms_ps_eta_phi" : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{MSSeg}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{MSSeg}", "log":0},
    "all_eta_pt"       : { "nb_x": 100, "low_x": -5, "high_x": 5, "label_x": "#eta_{all PS}", "nb_y": 100, "low_y": 0, "high_y": 1000, "label_y": "p_{T, all PS}", "log":0},
    "ms_x_y" : { "nb_x": 100, "low_x": -15000, "high_x": 15000, "label_x": "x_{MSSeg}", "nb_y": 100, "low_y": -15000, "high_y": 15000, "label_y": "y_{MSSeg}", "log":0},
    "mu_eta_phi" : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":1},
    "mu_ps_eta_phi" : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "failed_costag_eta_phi" : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "failed_full_costag_eta_phi" : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "d0_d0"       : { "nb_x": 100, "low_x": -1000, "high_x": 1000, "label_x": "d_{0, lead}", "nb_y": 100, "low_y": -1000, "high_y": 1000, "label_y": "d_{0, lead}", "log":0},
    "eta_d0"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#eta_{lead}", "nb_y": 100, "low_y": -1000, "high_y": 1000, "label_y": "d_{0, lead}", "log":0},
    "eta_z0"       : { "nb_x": 100, "low_x": -5, "high_x": 5, "label_x": "#eta_{lead}", "nb_y": 100, "low_y": -1000, "high_y": 1000, "label_y": "z_{0, sublead}", "log":0},
    "d0_z0"       : { "nb_x": 100, "low_x": -1000, "high_x": 1000, "label_x": "d_{0, lead}", "nb_y": 100, "low_y": -1000, "high_y": 1000, "label_y": "z_{0, lead}", "log":0},
    "pt_pt"       : { "nb_x": 100, "low_x": 0, "high_x": 1000, "label_x": "p_{T, lead}", "nb_y": 100, "low_y": 0, "high_y": 1000, "label_y": "p_{T, sublead}", "log":0},
    "eta_eta"       : { "nb_x": 100, "low_x": -5, "high_x": 5, "label_x": "#eta_{lead}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{sublead}", "log":0},
    "phi_phi"     : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{lead}", "nb_y": 100, "low_y": -4, "high_y": 4, "label_y": "#phi_{sublead}", "log":0},
    "cosTag_nPs"  : {"nb_x": 4, "low_x": 0, "high_x": 4, "label_x": "nCosTag", "nb_y": 4, "low_y": 0, "high_y": 4, "label_y": "nPs Muons", "log":1}, 
    "posPhi_ed_0"     : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta", "log":0},
    "idt_eta_phi"     : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta", "log":0},
    "idt_eta_phi_3pix"     : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta", "log":0},
    
    "idt_nhits_phi"       : { "nb_x": 20, "low_x": 0, "high_x": 20, "label_x": "nID hits", "nb_y": 100, "low_y": -4, "high_y": 4, "label_y": "#phi", "log":0},
    "idt_nPix_phi"       : { "nb_x": 20, "low_x": 0, "high_x": 20, "label_x": "nPix hits", "nb_y": 100, "low_y": -4, "high_y": 4, "label_y": "#phi", "log":0}

}


h = setHistos(tag,names)
h2 = setHistos2D(tag,names_2d)


print t_cos.GetEntries()
eventNum = 0
totalTracks = 0
n3Pix = 0

nmu = 0
nmups= 0
nmupsnotcos = 0
nevents = 0
for event in t_cos:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    try:
        event.muon_pt
    except:
        continue
     
    if not event.muon_pt: continue
    if event.muon_n == 0: continue 
    #if event.lepton_n > 1: continue
    h["n_muons"].Fill(len(event.muon_pt))
        
    nPs = 0
    nCos = 0
    iLead = -1
    iSublead = -1
    pass_dR_id = 0
    pass_dR_ms = 0
    nCosTag = 0
    foundFirst = False
    h["cutflow"].Fill("all",1)
    
    #h["pass_trigger"].Fill(event.passHLT_mu60_0eta105_msonly)

    ''' 
    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        h2["ms_eta_phi"].Fill(vt.Eta(), vt.Phi()) 
        h2["ms_x_y"].Fill(event.msSegment_x[ms], event.msSegment_y[ms]) 
    h["idt_n"].Fill(len(event.idTrack_NPix_Hits)) 
    for idt in xrange(len(event.idTrack_NPix_Hits)):
        totalTracks += 1
        h2["idt_nhits_phi"].Fill(event.idTrack_NPix_Hits[idt] + event.idTrack_NSct_Hits[idt], event.idTrack_phi[idt]) 
        h2["idt_eta_phi"].Fill(event.idTrack_phi[idt], event.idTrack_eta[idt])
        h["idt_pt"].Fill(event.idTrack_pt[idt])
        h["idt_pt-1"].Fill(1.0/(event.idTrack_pt[idt]))
        h["idt_phi"].Fill(event.idTrack_phi[idt])
        h["idt_eta"].Fill(event.idTrack_eta[idt])
        h["idt_d0"].Fill(event.idTrack_d0[idt])
        h["idt_z0"].Fill(event.idTrack_z0[idt])
        h["idt_nSiHits"].Fill(event.idTrack_NPix_Hits[idt] + event.idTrack_NSct_Hits[idt])
        h["idt_nPixHits"].Fill(event.idTrack_NPix_Hits[idt])
        h["idt_nSCTHits"].Fill(event.idTrack_NSct_Hits[idt])
        h["idt_isLRT"].Fill(event.idTrack_isLRT[idt])
        h2["idt_nPix_phi"].Fill(event.idTrack_NPix_Hits[idt], event.idTrack_phi[idt])
        if event.idTrack_NPix_Hits[idt] >= 3:
            n3Pix += 1
            h2["idt_eta_phi_3pix"].Fill(event.idTrack_phi[idt], event.idTrack_eta[idt])
            h["idt_pt_3pix"].Fill(event.idTrack_pt[idt])
            h["idt_pt-1_3pix"].Fill(1.0/(event.idTrack_pt[idt]))
            h["idt_phi_3pix"].Fill(event.idTrack_phi[idt])
            h["idt_eta_3pix"].Fill(event.idTrack_eta[idt])
            h["idt_d0_3pix"].Fill(event.idTrack_d0[idt])
            h["idt_z0_3pix"].Fill(event.idTrack_z0[idt])
    '''
    for im in xrange(len(event.muon_pt)):
        h2["mu_eta_phi"].Fill(event.muon_phi[im], event.muon_eta[im])
        h["all_d0_mu_nops"].Fill(event.muon_IDtrack_d0[im])
        nmu+= 1
        if preselect_muon(event, im):
            nmups+= 1
            nPs+=1
            
            h["all_pt_mu"].Fill(event.muon_pt[im])
            h["all_eta_mu"].Fill(event.muon_eta[im])
            h["all_phi_mu"].Fill(event.muon_phi[im])
            h2["mu_ps_eta_phi"].Fill(event.muon_phi[im], event.muon_eta[im])
            h2["all_eta_pt"].Fill(event.muon_eta[im], event.muon_pt[im])
            
            if "cosmic" in tag :
                h["all_d0_mu"].Fill(event.muon_d0[im])
                h["all_z0_mu"].Fill(event.muon_z0[im])
            else:
                h["all_d0_mu"].Fill(event.muon_IDtrack_d0[im])
                h["all_z0_mu"].Fill(event.muon_IDtrack_z0[im])
            if not (cosTag(event,im,0.013,.18) or materialVeto(event, im, innerMap, middleMap, outerMap)):
                h["all_d0_mu_notcos"].Fill(event.muon_IDtrack_d0[im])
                nmupsnotcos+= 1
                nCos +=1

            if iLead == -1:
                iLead = im

            elif iSublead == -1:
                iSublead = im

    if nPs > 1 and nCos == 0: nevents+=1

        
    # account for unsorted cosmics
    if iLead != -1 and iSublead != -1 and event.muon_pt[iLead] < event.muon_pt[iSublead]:
        tmp = iLead
        iLead = iSublead
        iSublead = tmp
    
    if tag == "data" and nPs > 1: continue #two muons in data


    # Fill leading muon info
    if iLead != -1:
        ctag = cosTag(event, iLead, .08, .2)           
        if not ctag: 
            h2["failed_costag_eta_phi"].Fill(event.muon_phi[iLead], event.muon_eta[iLead])
            h["failed_pt_mu"].Fill(event.muon_pt[iLead])
            if "cosmic" in tag:
                h["failed_d0_mu"].Fill(event.muon_d0[iLead])
                h["failed_z0_mu"].Fill(event.muon_z0[iLead])
            else:
                h["failed_d0_mu"].Fill(event.muon_IDtrack_d0[iLead])
                h["failed_z0_mu"].Fill(event.muon_IDtrack_z0[iLead])
            h["failed_eta_mu"].Fill(event.muon_eta[iLead])
            h["failed_phi_mu"].Fill(event.muon_phi[iLead])
            h["failed_qoverpsig_mu"].Fill(event.muon_QoverPsignif[iLead])
            h["failed_chi2_mu"].Fill(event.muon_CBtrack_chi2[iLead])
        
        full_ctag = ctag or materialVeto(event, iLead, innerMap, middleMap, outerMap)  
        if not full_ctag:
            h2["failed_full_costag_eta_phi"].Fill(event.muon_phi[iLead], event.muon_eta[iLead])
            h["failed_full_pt_mu"].Fill(event.muon_pt[iLead])
            if "cosmic" in tag:
                h["failed_full_d0_mu"].Fill(event.muon_d0[iLead])
                h["failed_full_z0_mu"].Fill(event.muon_z0[iLead])
            else:
                h["failed_full_d0_mu"].Fill(event.muon_IDtrack_d0[iLead])
                h["failed_full_z0_mu"].Fill(event.muon_IDtrack_z0[iLead])
            h["failed_full_eta_mu"].Fill(event.muon_eta[iLead])
            h["failed_full_phi_mu"].Fill(event.muon_phi[iLead])
            h["failed_full_qoverpsig_mu"].Fill(event.muon_QoverPsignif[iLead])
            h["failed_full_chi2_mu"].Fill(event.muon_CBtrack_chi2[iLead])
        if full_ctag: nCosTag += 1
                     
        h["cosTag"].Fill(int(ctag))
        #h["pass_trigger_ps"].Fill(event.pass_HLT_mu60_0eta105_msonly)
        i_ms = mostBackToBack_ms(event, iLead)
        ''' 
        for ms in xrange(len(event.msSegment_x)):
            vt = ROOT.TVector3()
            vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
            h2["ms_ps_eta_phi"].Fill(vt.Phi(), vt.Eta()) 
        v1 = ROOT.TVector3()
        v1.SetPtEtaPhi(event.muon_pt[iLead], event.muon_eta[iLead], event.muon_phi[iLead])
        
        i_id = mostBackToBack_id(event, iLead)
        v_id = ROOT.TVector3()
        if i_id != -1: 
            v_id.SetPtEtaPhi(event.idTrack_pt[i_id], event.idTrack_eta[i_id], event.idTrack_phi[i_id])
            dR_id = deltaRCos(v1,v_id)
            h["lead_dR_mbbid"].Fill( dR_id  )
            h["lead_dR_mbbid_zoom"].Fill( dR_id )

            if dR_id > .2: pass_dR_id+=1 
        v_ms = ROOT.TVector3()
        if i_ms != -1:
            v_ms = ROOT.TVector3(event.msSegment_x[i_ms], event.msSegment_y[i_ms], event.msSegment_z[i_ms])
            dR_ms = deltaRCos(v1,v_ms)
            h["lead_dR_mbbms"].Fill( dR_ms )
            h["lead_dR_mbbms_zoom"].Fill( dR_ms )
            #h2["sumEta_dPhi"].Fill( abs( v1.Eta() + v_ms.Eta() ), abs((abs(v1.DeltaPhi(v_ms)) - math.pi)))            

            if dR_ms > .2: pass_dR_ms+=1 
        
        '''
        h["lead_pt_mu"].Fill(event.muon_pt[iLead])
        if "cosmic" in tag:
            h["lead_d0_mu"].Fill(event.muon_d0[iLead])
            h["lead_z0_mu"].Fill(event.muon_z0[iLead])
        else:
            h["lead_d0_mu"].Fill(event.muon_IDtrack_d0[iLead])
            h["lead_z0_mu"].Fill(event.muon_IDtrack_z0[iLead])
        h["lead_eta_mu"].Fill(event.muon_eta[iLead])
        h["lead_phi_mu"].Fill(event.muon_phi[iLead])
        h["lead_qoverpsig_mu"].Fill(event.muon_QoverPsignif[iLead])
        h["lead_chi2_mu"].Fill(event.muon_CBtrack_chi2[iLead])
        if event.pass_HLT_mu60_0eta105_msonly: h["lead_chi2_trigger_mu"].Fill(event.muon_CBtrack_chi2[iLead])
        
        '''
        if iSublead == -1:
            if event.muon_phi[iLead] > 0:
                if not foundFirst:
                    foundFirst = True
                     
                    for idt in xrange(len(event.idTrack_NPix_Hits)):
                        h2["posPhi_ed_0"].Fill(event.idTrack_phi[idt],event.idTrack_eta[idt])
                    

            h["phi_1mu"].Fill(event.muon_phi[iLead]) 
            h["eta_1mu"].Fill(event.muon_eta[iLead]) 
            h["pt_1mu"].Fill(event.muon_pt[iLead]) 
            if "cosmic" in tag:
                h["d0_1mu"].Fill(event.muon_d0[iLead]) 
                h["z0_1mu"].Fill(event.muon_z0[iLead]) 
            else:
                h["d0_1mu"].Fill(event.muon_IDtrack_d0[iLead]) 
                h["z0_1mu"].Fill(event.muon_IDtrack_z0[iLead]) 
        '''      

        # fill subleading muon info 
        ''' 
        if iSublead != -1:
            v2 = ROOT.TVector3()
            v2.SetPtEtaPhi(event.muon_pt[iSublead], event.muon_eta[iSublead], event.muon_phi[iSublead])
                
            h["sublead_pt_mu"].Fill(event.muon_pt[iSublead])
            if "cosmic" in tag:
                h["sublead_d0_mu"].Fill(event.muon_d0[iSublead])
                h["sublead_z0_mu"].Fill(event.muon_z0[iSublead])
            else:
                h["sublead_d0_mu"].Fill(event.muon_IDtrack_d0[iSublead])
                h["sublead_z0_mu"].Fill(event.muon_IDtrack_z0[iSublead])
            h["sublead_eta_mu"].Fill(event.muon_eta[iSublead])
            h["sublead_phi_mu"].Fill(event.muon_phi[iSublead])
            
            # two muon vars
            h["dphi_mu"].Fill(abs(v1.DeltaPhi(v2)) - math.pi)
            h["dphi_mu_zoom"].Fill(abs(v1.DeltaPhi(v2)) - math.pi)
            h["dR_mu"].Fill(deltaRCos(v1, v2))
            h["dR_mu_zoom"].Fill(deltaRCos(v1, v2))
            h["sumEta_mu"].Fill(abs(event.muon_eta[iLead] + event.muon_eta[iSublead]))
            h["sumEta_mu_zoom"].Fill(abs(event.muon_eta[iLead] + event.muon_eta[iSublead]))
            

            h2["phi_phi"].Fill(event.muon_phi[iLead], event.muon_phi[iSublead])
            h2["pt_pt"].Fill(event.muon_pt[iLead], event.muon_pt[iSublead])
            h2["eta_eta"].Fill(event.muon_eta[iLead], event.muon_eta[iSublead])
            if "cosmic" in tag :
                h["dd0"].Fill(abs( abs(event.muon_d0[iLead])  - abs(event.muon_d0[iSublead])))
                h["sumd0"].Fill(abs(event.muon_d0[iLead]  + event.muon_d0[iSublead]))
                h2["d0_d0"].Fill(event.muon_d0[iLead], event.muon_d0[iSublead])
                h2["eta_d0"].Fill(event.muon_eta[iLead], event.muon_d0[iSublead])
                h2["eta_z0"].Fill(event.muon_eta[iLead], event.muon_z0[iSublead])
                 
            else: 
                h["dd0"].Fill(abs ( abs(event.muon_IDtrack_d0[iLead]) - abs(event.muon_IDtrack_d0[iSublead])))
                h2["d0_d0"].Fill(event.muon_IDtrack_d0[iLead], event.muon_IDtrack_d0[iSublead])
        ''' 

    h["n_ps_ev"].Fill(int(nPs>0))
    h["n_ps_mu"].Fill(nPs)
    h["n_passDR_id"].Fill(pass_dR_id)
    h["n_passDR_ms"].Fill(pass_dR_ms)
    h2["cosTag_nPs"].Fill(nCosTag, nPs)
    if nPs > 0: 
        h["cosTag_ev"].Fill(int(nCosTag>0))
        h["cosTag_n"].Fill(nCosTag)
    
    
    h["n_msSeg"].Fill(len(event.msSegment_x))
        

print "number of entries, uncorrected ", h2["sumEta_dPhi"].Integral()
print "number of entries, corrected   ", h2["sumEta_dPhi_corr"].Integral()

print "number of entries, uncorrected ", h2["sumEta_dPhi_zoom"].Integral()
print "number of entries, corrected   ", h2["sumEta_dPhi_corr_zoom"].Integral()

tag += append
if not os.path.exists("outputPlots/"+tag): os.makedirs("outputPlots/"+tag)

outFile = ROOT.TFile("outputFiles/" +tag+ "_histos.root", "RECREATE")
c = ROOT.TCanvas()


for histo in h:
    h[histo].Write()
    if names[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    h[histo].Draw()
    c.SaveAs("outputPlots/"+ tag + "/" + tag +"_1_"+ histo + ".pdf")

ROOT.gPad.SetLogy(0)
for histo in h2:
    h2[histo].Write()
    if names_2d[histo]["log"] == 1:
        ROOT.gPad.SetLogz()
    else:
        ROOT.gPad.SetLogz(0)
    if histo == "cosTag_nPs": h2[histo].Draw("COLZTEXT")
    else: 
        h2[histo].Draw("COLZ")

    c.SaveAs("outputPlots/"+tag + "/" + tag +"_2_"+ histo + ".pdf")

outFile.Write()

print "muons            ", nmu
print "ps muons         ", nmups
print "ps not cos muons ", nmupsnotcos
