import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "VR_v2_3030"
what = "VR"
#full
dEta_f = 0.018
dPhi_f = 0.25

#narrower
dEta_n = 0.013
dPhi_n = 0.02
 


print "1cos" in what
#full

 
tag = ""+what+"_v4"
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root'
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data/merged/data1*.root'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/2mu/data1*.root'
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data1*.root'

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

#bound = 40
bound = 30
u_bound = 40
nbins = 7 
names = { 
    "actual_pt": { "nb": 10, "low": 0, "high": 800, "label": "p_{T}", "log": 1},
    "unweighted_pt": { "nb": 10, "low": 0, "high": 800, "label": "p_{T}", "log": 1},
    "weighted_pt": { "nb": 10, "low": 0, "high": 800, "label": "p_{T}", "log": 1},
    
    "actual_d0": { "nb": 6, "low": 0, "high": 300, "label": "d_{0}", "log": 1},
    "unweighted_d0": { "nb": 6, "low": 0, "high": 300, "label": "d_{0}", "log": 1},
    "weighted_d0": { "nb": 6, "low": 0, "high": 300, "label": "d_{0}", "log": 1},
    
    "actual_z0": { "nb": 10, "low": 0, "high": 500, "label": "z_{0}", "log": 1},
    "unweighted_z0": { "nb": 10, "low": 0, "high": 500, "label": "z_{0}", "log": 1},
    "weighted_z0": { "nb": 10, "low": 0, "high": 500, "label": "z_{0}", "log": 1},
    
    "actual_t0": { "nb": 6, "low": -30, "high": 30, "label": "t_{0, avg}", "log": 1},
    "unweighted_t0": { "nb": 6, "low": -30, "high": 30, "label": "t_{0, avg}", "log": 1},
    "weighted_t0": { "nb": 6, "low": -30, "high": 30, "label": "t_{0, avg}", "log": 1},

    "actual_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "#eta", "log": 1},
    "unweighted_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "#eta", "log": 1},
    "weighted_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "#eta", "log": 1},
    
    "actual_phi": { "nb": 10, "low": -3.5, "high": 3.5, "label": "#phi", "log": 1},
    "unweighted_phi": { "nb": 10, "low": -3.5, "high": 3.5, "label": "#phi", "log": 1},
    "weighted_phi": { "nb": 10, "low": -3.5, "high": 3.5, "label": "#phi", "log": 1},
    
    "actual_cb_chi2": { "nb": 10, "low": 0, "high": 4, "label": "#chi^{2}_{CB  track}", "log": 1},
    "unweighted_cb_chi2": { "nb": 10, "low": 0, "high": 4, "label": "#chi^{2}_{CB  track}", "log": 1},
    "weighted_cb_chi2": { "nb": 10, "low": 0, "high": 4, "label": "#chi^{2}_{CB  track}", "log": 1},
    
    "actual_id_chi2": { "nb": 10, "low": 0, "high": 3, "label": "#chi^{2}_{CB  track}", "log": 1},
    "unweighted_id_chi2": { "nb": 10, "low": 0, "high": 3, "label": "#chi^{2}_{CB  track}", "log": 1},
    "weighted_id_chi2": { "nb": 10, "low": 0, "high": 3, "label": "#chi^{2}_{CB  track}", "log": 1},
}

h = {}
h = setHistos("", names)

# take scaling from calculate_fake_bkg_unbinned.py
#for unbinned VR from -30 to 30 (SR_v4)
rg = 0.78985801217


f_rg = ROOT.TFile("outputFiles/rgood_VR_v4.root")
rgood = f_rg.Get("rgood_VR_v4")


print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100: break
    
#

    #2 muons, opp side of detecdtor
    if len(event.muon_pt) > 1 and len(event.electron_pt) == 0 and event.muon_phi[0]*event.muon_phi[1] < 0:
        itop = 0
        ibot = 1
        if event.muon_phi[0] < 0: 
            itop = 1
            ibot = 0
        
        ctagt_f, ms_cos = cosTag(event, itop, dEta_f, dPhi_f)
        ctagt_n, ms_cos = cosTag(event, itop, dEta_n, dPhi_n)
        matVetot = materialVeto(event, itop, innerMap, middleMap, outerMap)
        cosmic_t_f = ctagt_f or matVetot
        cosmic_t_n = ctagt_n or matVetot
         
        ctagb_f, ms_cos = cosTag(event, ibot, dEta_f, dPhi_f)
        ctagb_n, ms_cos = cosTag(event, ibot, dEta_n, dPhi_n)
        matVetob = materialVeto(event, ibot, innerMap, middleMap, outerMap)
        cosmic_b_f = ctagb_f or matVetob
        cosmic_b_n = ctagb_n or matVetob
        
        phi_layers = phi_lays(event,itop) 
        t0 = t_avg(event, itop)
        
        #VR: signal, full cosmic but not narrow cosmic
        if (preselect_muon(event, ibot) and cosmic_b_f and not cosmic_b_n):
            
                #full but not narrow top
                if (preselect_muon_basechi2(event, itop, doQual=False) and abs(t0) < 30 and not cosmic_t_n and cosmic_t_f):
                    ### top passes quality
                    if event.muon_MStrack_nPres[itop] > 2 and phi_layers > 0: 
                        h["actual_pt"].Fill(event.muon_pt[itop])
                        h["actual_d0"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        h["actual_z0"].Fill(abs(event.muon_IDtrack_z0[itop]))
                        h["actual_t0"].Fill(t0)
                        h["actual_eta"].Fill(event.muon_eta[itop])
                        h["actual_phi"].Fill(event.muon_phi[itop])
                        h["actual_cb_chi2"].Fill(event.muon_CBtrack_chi2[itop])
                        h["actual_id_chi2"].Fill(event.muon_IDtrack_chi2[itop])
                    # top fails quality 
                    else:
                        h["unweighted_pt"].Fill(event.muon_pt[itop])
                        h["unweighted_d0"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        h["unweighted_z0"].Fill(abs(event.muon_IDtrack_z0[itop]))
                        h["unweighted_t0"].Fill(t0)
                        h["unweighted_eta"].Fill(event.muon_eta[itop])
                        h["unweighted_phi"].Fill(event.muon_phi[itop])
                        h["unweighted_cb_chi2"].Fill(event.muon_CBtrack_chi2[itop])
                        h["unweighted_id_chi2"].Fill(event.muon_IDtrack_chi2[itop])
                            
                        rg = rgood.Eval(abs(event.muon_IDtrack_d0[itop]))
                                           
                        h["weighted_pt"].Fill(event.muon_pt[itop], rg)
                        h["weighted_d0"].Fill(abs(event.muon_IDtrack_d0[itop]), rg)
                        h["weighted_z0"].Fill(abs(event.muon_IDtrack_z0[itop]), rg)
                        h["weighted_t0"].Fill(t0, rg)
                        h["weighted_eta"].Fill(event.muon_eta[itop], rg)
                        h["weighted_phi"].Fill(event.muon_phi[itop], rg)
                        h["weighted_cb_chi2"].Fill(event.muon_CBtrack_chi2[itop], rg)
                        h["weighted_id_chi2"].Fill(event.muon_IDtrack_chi2[itop], rg)
                                      



c = ROOT.TCanvas()
vv = {
    "pt":"p_{T} [GeV]",
    "d0":"d_{0} [mm]",
    "z0":"z_{0} [mm]",
    "t0":"t^{0}_{avg} [ns]",
    "eta":"#eta",
    "phi":"#phi",
    "id_chi2":"#chi^{2}_{ID track}",
    "cb_chi2":"#chi^{2}_{CB track}",
}
for v in vv:
   
    c.Clear() 
   
    h["weighted_"+v].SetTitle("reweighted")
    h["actual_"+v].SetTitle("actual")
    makeRatioPlot(h["weighted_"+v], h["actual_"+v], vv[v], "outputPlots/estimate_fake_cos/d0_ratio_"+v+".pdf")
    
    c.Clear() 
    h["weighted_"+v].SetLineStyle(2)
    h["weighted_"+v].SetLineColor(ROOT.kBlue)
    h["actual_"+v].SetLineColor(ROOT.kRed)
    h["unweighted_"+v].SetMaximum(1.5*getMaximum([ h["weighted_"+v], h["unweighted_"+v], h["actual_"+v]]))
    h["unweighted_"+v].Draw("hist")
    h["actual_"+v].Draw("hist same")
    h["weighted_"+v].Draw("hist same")
    
    
     
    leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
    leg.AddEntry(h["actual_"+v], "actual", "l")
    leg.AddEntry(h["unweighted_"+v], "unweighted", "l")
    leg.AddEntry(h["weighted_"+v], "weighted", "l")
    leg.Draw("same")

    c.SaveAs("outputPlots/estimate_fake_cos/d0_test_weighting_" + v +".pdf")
    
    c.Clear()
     

