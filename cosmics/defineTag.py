import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("cosmic_helpers.py")

tag = "data"

append = "p03"

if tag == "cosmic":
    AODFiles = ROOT.TFile('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.user.kdipetri.data16_cos.mc16d_031519_trees.root/all.root')
    t_cos = AODFiles.Get("trees_SRCOS_")
    tag = "cosmic"

if tag == "300_slep":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v3.0_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "300_stau":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_0_*.DAOD_RPVLL_mc16*_v3.0_trees.root/*')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

if tag == "data":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/*.root')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)


maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names_2d = {
    #"rejection"     : { "nb_x": 10, "low_x": 0, "high_x": .2, "label_x": "#Sigma #eta", "nb_y": 10, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi", "log":0},
    #"remaining"     : { "nb_x": 10, "low_x": 0, "high_x": .2, "label_x": "#Sigma #eta", "nb_y": 10, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi", "log":1}
    
    "rejection"     : { "nb_x": 10, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta", "nb_y": 10, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi", "log":0},
    "remaining"     : { "nb_x": 10, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta", "nb_y": 10, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi", "log":1}
    
}

h2 = setHistos2D(tag, names_2d)

binsize_eta = (names_2d["rejection"]["high_x"] - names_2d["rejection"]["low_x"])/names_2d["rejection"]["nb_x"]
binsize_phi = (names_2d["rejection"]["high_y"] - names_2d["rejection"]["low_y"])/names_2d["rejection"]["nb_y"]
eta_bins = []
phi_bins = []

for i in xrange(0,names_2d["rejection"]["nb_x"]+1): 
    eta_bins.append(i*binsize_eta)
for i in xrange(0,names_2d["rejection"]["nb_y"]+1): 
    phi_bins.append(i*binsize_phi)

eventNum = 0
print t_cos.GetEntries()
for event in t_cos:
    if eventNum%10000 == 0: print "processing event", eventNum
    eventNum += 1


for ie in xrange(len(eta_bins)-1):
    for ip in xrange(len(phi_bins)-1):
        print "region: sum_eta " + str(eta_bins[ie]) + " - " + str(eta_bins[ie+1]) + " ;  delta_phi " + str(phi_bins[ip]) + " - " + str(phi_bins[ip+1])

        total = 0
        rejected = 0
        eventNum = 0
        for event in t_cos:
            #if eventNum > 1000: break
            if eventNum % 10000 == 0: print "Processing event ", eventNum
            eventNum+=1
            if tag == "data" and event.lepton_n != 1: continue
            
            nPs = 0
            nCosTag = 0
            for im in xrange(len(event.muon_pt)):
                if preselect_muon(event, im, tight=True):
                    nPs+=1
                    
                    ctag = cosTag(event, im, eta_bins[ie+1], phi_bins[ip+1])
                    mveto = materialVeto(event, im, innerMap, middleMap, outerMap)           
                    if ctag or mveto: nCosTag +=1

            if nPs > 0: total += 1
            if nPs > 0 and nCosTag > 0: rejected +=1
        
        if total == 0: rate = 0 
        else : rate = rejected*1.0/total
        print "total ", total
        print "rejected ", rejected
        print "rate ", rate

        h2["rejection"].SetBinContent(ie+1, ip+1, rate)
        h2["remaining"].SetBinContent(ie+1, ip+1, 1-rate)


outFile = ROOT.TFile("outputFiles/integral/" +tag+ "rejection_histos_"+ append + ".root", "RECREATE")
c = ROOT.TCanvas()

for histo in h2:
    if names_2d[histo]["log"] == 1:
        ROOT.gPad.SetLogz()
    else:
        ROOT.gPad.SetLogz(0) 
    h2[histo].Write()
    ROOT.gStyle.SetPaintTextFormat("4.4f")
    h2[histo].Draw("COLZTEXT")

    c.SaveAs("outputPlots/integral/" + tag +"_"+ histo + "_"+ append + ".pdf")

outFile.Write()
