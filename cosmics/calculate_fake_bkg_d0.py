import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

doValidate=True
saveGraph =False
debug=False
doUnBinned=True

#saveFlag = "VR_v2_3030_v4"
saveFlag = "SR_v4"
#saveFlag = "1cos_narrow_nochi2_t00overflow_v4"
#saveFlag = "1cos_narrow_nochi2_v4_40"
#f = ROOT.TFile("outputFiles/estimate_fake_chi2npres_"+saveFlag+".root")
f = ROOT.TFile("outputFiles/cos_estimate_fake_"+saveFlag+".root")
c = ROOT.TCanvas()

quals = ["_nPres","_nPhiLays",""]
#quals = [""]

of = ROOT.TFile("outputFiles/rgood_"+saveFlag+".root", "RECREATE")

    
for qual in quals:
    h_2mu = f.Get("_d0_extrap_top_bad"+qual)
    t_good = f.Get("_d0_rgood_top_good")
    t_bad = f.Get("_d0_rgood_top_bad"+qual)
        

    #Wilson interval
    rgood_w = ROOT.TGraphAsymmErrors()
    rgood_w.Divide(t_good, t_bad, "pois w")
    rgood_w.Draw("pela")
    rgood_w.SetName("rgood_"+saveFlag+qual)
    rgood_w.SetTitle(saveFlag+qual+"-wilson errors;d_{0} [mm];R_{good}")
    ROOT.ATLASLabel(0.20,0.88, "Internal")
    c.Modified()
    c.Update()
    c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_rgood.pdf")
    c.Clear()
    
    h_2mu.GetXaxis().SetTitle("d_{0} [mm]")
    h_2mu.GetYaxis().SetTitle("events")
    h_2mu.SetMaximum(h_2mu.GetMaximum()*1.2)
    h_2mu.Draw("hist")
    ROOT.ATLASLabel(0.20,0.88, "Internal")
    c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_2mu.pdf")
    c.Clear() 

    nBins = h_2mu.GetNbinsX()
    
    h_weighted = h_2mu.Clone("reweighted%s"%qual)
    
    nPoints = rgood_w.GetN()
    print "hist bins ", nBins
    print "graph points ", nPoints

    total = 0
    total_up = 0
    total_down = 0
    
    h_total = h_2mu.Clone()
    h_total.Reset()
    h_up = h_2mu.Clone()
    h_up.Reset()
    h_dn = h_2mu.Clone()
    h_dn.Reset()
     
    #graph points count from 0, histogram bins count from 1 -_-
    for b in xrange(1,nBins+1):
    #for b in xrange(2,nBins):
            
        gb = b -1
        
        #central value at point
        x = ROOT.Double()
        y = ROOT.Double()
        rgood_w.GetPoint(gb,x,y)

        # histogram bin with that x value
        bn = h_2mu.FindBin(x)
        print bn, h_2mu.GetBinLowEdge(bn), y, h_2mu.GetBinContent(bn)
        #if abs(h_2mu.GetBinLowEdge(bn)) > 30: continue
        
        for x in xrange(int(h_2mu.GetBinContent(bn))): 
            h_total.Fill(h_2mu.GetBinLowEdge(bn)+1, y)
            h_weighted.Fill(h_2mu.GetBinLowEdge(bn)+1, y)
        #h_total.SetBinContent(bn, h_2mu.GetBinContent(bn) * y)
        #h_total.SetBinError  (bn, h_2mu.GetBinError(bn))
        h_up.SetBinContent(bn, (h_2mu.GetBinContent(bn) * (y + rgood_w.GetErrorYhigh(gb))) )
        h_dn.SetBinContent(bn, (h_2mu.GetBinContent(bn) * (y - rgood_w.GetErrorYlow(gb))) )
        
        total      += h_2mu.GetBinContent(bn) * y   
        total_up   += h_2mu.GetBinContent(bn) * (y + rgood_w.GetErrorYhigh(gb))
        total_down += h_2mu.GetBinContent(bn) * (y - rgood_w.GetErrorYlow(gb))
        
        
        h_weighted.SetBinContent(bn, h_2mu.GetBinContent(bn) * y)
            
        if debug:
            print "point number, x, y ",gb,x,y 
            print "histo bin with x ", bn
            print "hist bin content ", h_2mu.GetBinContent(bn)
            print "hist low edge ", h_2mu.GetBinLowEdge(bn)
            print "hist up edge " , h_2mu.GetBinLowEdge(bn+1)
            print "rgood bound up ", y + rgood_w.GetErrorYhigh(gb)
            print "rgood error up ", rgood_w.GetErrorYhigh(gb)
            print "rgood bound down ", y - rgood_w.GetErrorYlow(gb)
            print "rgood error down ", rgood_w.GetErrorYlow(gb)
            print 
       

    c.Clear()
    h_total.Draw()
    c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_central.pdf")
    c.Clear()
    h_up.Draw()
    c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_up.pdf")
    c.Clear()
    h_dn.Draw()
    c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_down.pdf")
    
    print saveFlag, qual
    est = h_total.Integral()
    if est == 0: est = 999999
    up_bnd = h_up.Integral()
    dn_bnd = h_dn.Integral()
    stat_err = ROOT.Double()
    h_total.IntegralAndError(1,h_total.GetNbinsX(),stat_err)
    print "stat error", stat_err
    print "1/sqrt(n)", 1.0/math.sqrt(h_2mu.Integral())
    
    tot_err_up = total * math.sqrt((stat_err/h_2mu.Integral())**2 + ( abs(up_bnd - est )/est)**2)
    tot_err_dn = total * math.sqrt((stat_err/h_2mu.Integral())**2 + ( abs(dn_bnd - est) /est)**2)


    print "estimated number of events: ",est
    print "                  up error: ",tot_err_up
    print "                         %: ",abs(tot_err_up - est)*100/est
    print "                down error: ",tot_err_dn
    print "                         %: ",abs(tot_err_dn - est)*100/est
        
        
        

        
         
    if doValidate and "SR" not in saveFlag:
        h_real = f.Get("_d0_extrap_top_good")
        actual = h_real.Integral()
        print "actual number of events ", actual
        if actual != 0: print "percent difference      ", abs(actual-est)*100/est
        if actual > est: print "significance            ", abs(actual-est)/tot_err_up
        if actual < est: print "significance            ", abs(actual-est)/tot_err_dn
        
        c.Clear()
        h_weighted.SetTitle("reweighted")
        h_real.SetTitle("actual")
        print h_weighted.GetMaximum()
        print h_real.GetMaximum()
        makeRatioPlot(h_weighted, h_real, "d_{0} [mm]", "outputPlots/estimate_fake_cos/d0_ratio_"+qual+".pdf")
        
        c.Clear() 
        h_weighted.SetLineStyle(2)
        h_weighted.SetLineColor(ROOT.kRed)
        h_real.SetMaximum(1.2*getMaximum([h_weighted,h_real]))
        h_real.GetXaxis().SetTitle("d_{0} [mm]")
        h_real.GetYaxis().SetTitle("events")
        h_real.Draw("hist")
        h_weighted.Draw("hist same")
        
        leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
        leg.AddEntry(h_weighted, "reweighted", "l")
        leg.AddEntry(h_real, "actual", "l")
        leg.Draw("same")
        ROOT.ATLASLabel(0.20,0.88, "Internal")
        
        c.SaveAs("outputPlots/estimate_fake_cos/d0_"+saveFlag+qual+"_reweighted.pdf")
        
    
    rgood_w.Write()
    of.Write()
    

of.Write()
of.Close()


