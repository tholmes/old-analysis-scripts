import glob
import ROOT
import os

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

c = ROOT.TCanvas()
#ROOT.gPad.SetLogy()

f = ROOT.TFile("outputFiles/cos_estimate_fake_VR_chi2_v4.root")

good = f.Get("_extrap_top_good")
bad = f.Get("_extrap_top_bad")
pres = f.Get("_extrap_top_bad_nPres")
phi = f.Get("_extrap_top_bad_nPhiLays")
chi2 = f.Get("_extrap_top_bad_chi2")

good = f.Get("_d0_extrap_top_good")
bad = f.Get("_d0_extrap_top_bad")
pres = f.Get("_d0_extrap_top_bad_nPres")
phi = f.Get("_d0_extrap_top_bad_nPhiLays")
chi2 = f.Get("_d0_extrap_top_bad_chi2")

good.GetXaxis().SetTitle("d_{0}")
good.GetYaxis().SetTitle("number of muons")
good.SetMaximum(1.5*getMaximum([good, bad, pres, phi, chi2]))

bad.SetLineStyle(2)
bad.SetLineColor(ROOT.kRed)

pres.SetLineStyle(2)
pres.SetLineColor(ROOT.kBlue)

phi.SetLineStyle(2)
phi.SetLineColor(ROOT.kGreen+1)

chi2.SetLineStyle(2)
chi2.SetLineColor(ROOT.kOrange)


good.Draw("hist")
bad.Draw("hist same")
pres.Draw("hist same")
phi.Draw("hist same")
chi2.Draw("hist same")


leg = ROOT.TLegend(0.6,0.75,0.82,0.92)
leg.AddEntry(good, "good", "l")
leg.AddEntry(bad, "fails OR", "l")
leg.AddEntry(pres, "fails pres", "l")
leg.AddEntry(phi, "fails phi", "l")
leg.AddEntry(chi2, "fails chi2", "l")
leg.Draw("same")
ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()

c.SaveAs("outputPlots/estimate_fake_cos/VR_comp_extrap_d0.pdf")

