import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#what = "VR_v2_3030"
what = "VR"
#full
dEta_f = 0.018
dPhi_f = 0.25

#narrower
dEta_n = 0.013
dPhi_n = 0.02
 


print "1cos" in what
#full

 
tag = ""+what+"_v4"
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root'
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data/merged/data1*.root'
basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/2mu/data1*.root'
#basename= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data1*.root'

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

print basename

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

#bound = 40
bound = 30
u_bound = 40
nbins = 7 
names = { 
    "rgood_top_good": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top good (rgood)", "log": 1},   
    "d0_rgood_top_good": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top good (rgood)", "log": 1},   
    "1bin_rgood_top_good": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top good (rgood)", "log": 1},   
    
    "rgood_top_bad": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    "d0_rgood_top_bad": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top bad (rgood)", "log": 1},
    "1bin_rgood_top_bad": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    
    "rgood_top_bad_nPres": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    "d0_rgood_top_bad_nPres": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top bad (rgood)", "log": 1},
    "1bin_rgood_top_bad_nPres": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    
    "rgood_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    "d0_rgood_top_bad_nPhiLays": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top bad (rgood)", "log": 1},
    "1bin_rgood_top_bad_nPhiLays": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    
    "rgood_top_bad_chi2": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    "d0_rgood_top_bad_chi2": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top bad (rgood)", "log": 1},
    "1bin_rgood_top_bad_chi2": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top bad (rgood)", "log": 1},
    
    "extrap_top_bad": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    "d0_extrap_top_bad": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top (extrapolation region)", "log": 1},
    "1bin_extrap_top_bad": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    
    "extrap_top_bad_nPres": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    "d0_extrap_top_bad_nPres": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top (extrapolation region)", "log": 1},
    "1bin_extrap_top_bad_nPres": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    
    "extrap_top_bad_nPhiLays": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    "d0_extrap_top_bad_nPhiLays": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top (extrapolation region)", "log": 1},
    "1bin_extrap_top_bad_nPhiLays": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    
    "extrap_top_bad_chi2": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    "d0_extrap_top_bad_chi2": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top (extrapolation region)", "log": 1},
    "1bin_extrap_top_bad_chi2": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1},
    
    "extrap_top_good": { "nb": nbins, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1}, ###ONLY FOR VALIDATION
    "d0_extrap_top_good": { "nb": 6, "low": 0, "high": 300, "label": "d_{0} top (extrapolation region)", "log": 1}, ###ONLY FOR VALIDATION
    "1bin_extrap_top_good": { "nb": 1, "low": -bound, "high": u_bound, "label": "t_{0} top (extrapolation region)", "log": 1}, ###ONLY FOR VALIDATION
}

h = {}
h = setHistos("", names)


mysteryNums = [4253530762, 646388285, 722317128, 765842373, 926487021]

print t.GetEntries()
eventNum = 0
for event in t:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    #if eventNum > 100000: break
    ''' 
    if event.eventNumber in mysteryNums:
        print "****event num", eventNum
        print "basline mu", len(event.muon_pt)
        print "basline e", len(event.electron_pt)
        for i in [0,1]:
            ctag_narrow, ms_cos = cosTag(event, i, .013, .18)
            ctag_full, ms_cos = cosTag(event, i, .018, .25)
            matVeto = materialVeto(event, i, innerMap, middleMap, outerMap)
            t0 = t_avg(event, i) 
            
            print "muon ", i 
            print "cosmic narrow", ctag_narrow 
            print "cosmic full", ctag_full 
            print "cosmic mv", matVeto 
            print "phi", event.muon_phi[i]
            print "tavg", t0
        print
    '''

    #2 muons, opp side of detecdtor
    if len(event.muon_pt) > 1 and len(event.electron_pt) == 0 and event.muon_phi[0]*event.muon_phi[1] < 0:
        
        #phi < 0 is the bottom
        #itop is probe muon
        '''
        itop = 0
        ibot = 1
        if event.muon_phi[0] < 0: 
            itop = 1
            ibot = 0
        '''
        itop = 0
        ibot = 1
        if ("inverted" in what and event.muon_phi[0] > 0) or ("inverted" not in what and event.muon_phi[0] < 0): 
            itop = 1
            ibot = 0
        
        ctagt_f, ms_cos = cosTag(event, itop, dEta_f, dPhi_f)
        ctagt_n, ms_cos = cosTag(event, itop, dEta_n, dPhi_n)
        matVetot = materialVeto(event, itop, innerMap, middleMap, outerMap)
        cosmic_t_f = ctagt_f or matVetot
        cosmic_t_n = ctagt_n or matVetot
         
        ctagb_f, ms_cos = cosTag(event, ibot, dEta_f, dPhi_f)
        ctagb_n, ms_cos = cosTag(event, ibot, dEta_n, dPhi_n)
        matVetob = materialVeto(event, ibot, innerMap, middleMap, outerMap)
        cosmic_b_f = ctagb_f or matVetob
        cosmic_b_n = ctagb_n or matVetob
        
        t0 = t_avg(event, itop)
        #hacky bullshit!
        #if t0 == 999: #failed fit gets its own bin
        #    t0 = 31
        #if t0 > 30 and t0 < 40: #throw it in the general overflow bin
        #    t0 = 50 
        phi_layers = phi_lays(event,itop) 
        
        #SR: signal, noncosmic bottom muon
        #VR: signal, not full cosmic but narrow cosmic
        if ("SR" in what and preselect_muon(event, ibot) and not cosmic_b_f) or ("VR" in what and preselect_muon(event, ibot) and cosmic_b_f and not cosmic_b_n):
            #almost signal top
            #if preselect_muon_basechi2(event, itop, doQual=False):
            if preselect_muon(event, itop, doQual=False):
                # is a cosmic
                if ("SR" in what and cosmic_t_f) or ("VR" in what and cosmic_t_n):
                    if event.muon_MStrack_nPres[itop] > 2 and phi_layers > 0 and event.muon_CBtrack_chi2[itop] < 3: 
                        h["rgood_top_good"].Fill(t0)
                        h["1bin_rgood_top_good"].Fill(t0)
                        if abs(t0) < 30: h["d0_rgood_top_good"].Fill(abs(event.muon_IDtrack_d0[itop]))
                    # OR all quality vars
                    else:
                        if event.muon_MStrack_nPres[itop] < 3 or phi_layers < 1 or event.muon_CBtrack_chi2[itop] > 3:
                            h["rgood_top_bad"].Fill(t0)
                            h["1bin_rgood_top_bad"].Fill(t0)
                            if abs(t0) < 30: h["d0_rgood_top_bad"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only nPres is "bad"
                        if event.muon_MStrack_nPres[itop] < 3: 
                            h["rgood_top_bad_nPres"].Fill(t0)
                            h["1bin_rgood_top_bad_nPres"].Fill(t0)
                            if abs(t0) < 30: h["d0_rgood_top_bad_nPres"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only nPhiLays is "bad"
                        if phi_layers < 1: 
                            h["rgood_top_bad_nPhiLays"].Fill(t0)
                            h["1bin_rgood_top_bad_nPhiLays"].Fill(t0)
                            if abs(t0) < 30: h["d0_rgood_top_bad_nPhiLays"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only chi2 is "bad"
                        if event.muon_CBtrack_chi2[itop] > 3: 
                            h["rgood_top_bad_chi2"].Fill(t0)
                            h["1bin_rgood_top_bad_chi2"].Fill(t0)
                            if abs(t0) < 30: h["d0_rgood_top_bad_chi2"].Fill(abs(event.muon_IDtrack_d0[itop]))
            
                #top is not a cosmic
                if ("SR" in what and not cosmic_t_f) or ("VR" in what and not cosmic_t_n and cosmic_t_f):
                #if ("SR" in what and not cosmic_t_f) or ("VR" in what and not cosmic_t_f):
                    ### only fill good if in VR
                    if "VR" in what and event.muon_MStrack_nPres[itop] > 2 and phi_layers > 0 and event.muon_CBtrack_chi2[itop] < 3: 
                        h["extrap_top_good"].Fill(t0)
                        h["1bin_extrap_top_good"].Fill(t0)
                        if abs(t0) < 30: h["d0_extrap_top_good"].Fill(abs(event.muon_IDtrack_d0[itop]))
                    
                    else:
                        if event.muon_MStrack_nPres[itop] < 3 or phi_layers < 1 or event.muon_CBtrack_chi2[itop] > 3 :
                            h["extrap_top_bad"].Fill(t0)
                            h["1bin_extrap_top_bad"].Fill(t0)
                            if abs(t0) < 30: h["d0_extrap_top_bad"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only nPres is "bad"
                        if event.muon_MStrack_nPres[itop] < 3: 
                            h["extrap_top_bad_nPres"].Fill(t0)
                            h["1bin_extrap_top_bad_nPres"].Fill(t0)
                            if abs(t0) < 30: h["d0_extrap_top_bad_nPres"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only nPhiLays is "bad"
                        if phi_layers < 1: 
                            h["extrap_top_bad_nPhiLays"].Fill(t0)
                            h["1bin_extrap_top_bad_nPhiLays"].Fill(t0)
                            if abs(t0) < 30: h["d0_extrap_top_bad_nPhiLays"].Fill(abs(event.muon_IDtrack_d0[itop]))
                        
                        #only chi2 is "bad"
                        if event.muon_CBtrack_chi2[itop] > 3: 
                            h["extrap_top_bad_chi2"].Fill(t0)
                            h["1bin_extrap_top_bad_chi2"].Fill(t0)
                            if abs(t0) < 30: h["d0_extrap_top_bad_chi2"].Fill(abs(event.muon_IDtrack_d0[itop]))
                                      


outFile = ROOT.TFile("outputFiles/cos_estimate_fake_"+tag+".root", "RECREATE")

c = ROOT.TCanvas()
for histo in h:
    if "wOF" in what:
        if "1bin" not in histo:
            h[histo] = addOverflowBin(h[histo])
            h[histo] = addUnderflowBin(h[histo])
        else:
            h[histo] = addOverflow(h[histo])
            h[histo] = addUnderflow(h[histo])

    h[histo].Write()
    h[histo].Draw("hist e")
    c.SaveAs("outputPlots/estimate_fake_cos/" + tag +"_"+ histo +".pdf")

