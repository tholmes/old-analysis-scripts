import ROOT
import math
import glob
import os
import sys

execfile("cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

doValidate=True
saveGraph =False
debug=False
assymErrors=False
doUnBinned=True

#saveFlag = "VR_v2_3030_v4"
saveFlag = "SR_strictervars_v4"
#saveFlag = "VR_chi2_v4"
#saveFlag = "1cos_narrow_nochi2_t00overflow_v4"
#saveFlag = "1cos_narrow_nochi2_v4_40"
#f = ROOT.TFile("outputFiles/estimate_fake_chi2npres_"+saveFlag+".root")
f = ROOT.TFile("outputFiles/cos_estimate_fake_"+saveFlag+".root")
c = ROOT.TCanvas()

quals = ["_nPres","_nPhiLays","_chi2",""]
#quals = [""]
#quals = [""]

of = ROOT.TFile("outputFiles/rgood_"+saveFlag+".root", "RECREATE")

    
for qual in quals:
    h_2mu = f.Get("_1bin_extrap_top_bad"+qual)
    t_good = f.Get("_1bin_rgood_top_good")
    t_bad = f.Get("_1bin_rgood_top_bad"+qual)
        

    #Wilson interval
    rgood_w = ROOT.TGraphAsymmErrors()
    rgood_w.Divide(t_good, t_bad, "pois w")

    
    #central value at point
    x = ROOT.Double()
    y = ROOT.Double()
    rgood_w.GetPoint(0,x,y)
    
    print x, y
     
    print "rgood", y

    est = h_2mu.Integral()*1.0 * y
    err_up =  rgood_w.GetErrorYhigh(0)
    err_dn =  rgood_w.GetErrorYlow(0)
    print est
    print err_up
    print err_dn

    if est == 0:
        print "estimate is 0!"
        print
        continue
    tot_err_up = est * math.sqrt(1/h_2mu.Integral() + (err_up/y)**2) 
    tot_err_dn = est * math.sqrt(1/h_2mu.Integral() + (err_dn/y)**2) 
    print saveFlag, qual
    print "estimated number of events: ",est
    print "                  up error: ",tot_err_up
    print "                         %: ",abs(tot_err_up)*100/est
    print "                down error: ",tot_err_dn
    print "                         %: ",abs(tot_err_dn)*100/est

        
         
    if doValidate and "SR" not in saveFlag:
        h_real = f.Get("_extrap_top_good")
        actual = h_real.Integral()
        print "actual number of events ", actual
        if actual != 0: print "percent difference      ", abs(actual-est)*100/est
        if actual > est: print "significance            ", abs(actual-est)/tot_err_up
        if actual < est: print "significance            ", abs(actual-est)/tot_err_dn
        
         
    rgood_w.Write()
    of.Write()
    

of.Write()
of.Close()


