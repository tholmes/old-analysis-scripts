

def preselect_muon(event, i, dod0=True, doQual=True, doIso=True, doTrackQual=True, doTiming=True, verbose=False):
    
    d0 = event.muon_IDtrack_d0[i]
    z0 = event.muon_IDtrack_z0[i]
    if verbose:
        print 

    if not event.muon_hasIDtrack[i]: return False
    if not event.muon_hasCBtrack[i]: return False
    if not event.muon_hasMStrack[i]: return False
    if event.muon_nMSSegments[i] == 0: return False
     
    if verbose: print "pt        ", event.muon_pt[i]
    if event.muon_pt[i] < 65: return False
    
    if verbose: print "eta       ", event.muon_eta[i]
    if abs(event.muon_eta[i]) > 2.5: return False
    
    if verbose: print "z0        ", event.muon_IDtrack_z0[i]
    if abs( z0 ) > 500: return False

    if verbose: print "d0        ", event.muon_IDtrack_d0[i]
    if abs( d0 ) > 300: return False
    if dod0: 
        if abs( d0 ) < 3: return False
    
    if doTrackQual:
        if verbose: print "id chi2   ", event.muon_IDtrack_chi2[i]
        if event.muon_IDtrack_chi2[i] > 2: return False
        
        if verbose: print "id nmiss  ", event.muon_IDtrack_nMissingLayers[i]
        if event.muon_IDtrack_nMissingLayers[i] > 1 : return False
    
    if doQual:
        if verbose: print "n pres    ", event.muon_MStrack_nPres[i]
        if event.muon_MStrack_nPres[i] < 3 or event.muon_MStrack_nPres[i] == -999: return False 
        
        if verbose: print "phi lays  ", phi_lays(event,i)
        if phi_lays(event, i) < 1: return False
        
        if verbose: print "cb chi2   ", event.muon_CBtrack_chi2[i]
        if event.muon_CBtrack_chi2[i] > 3 or event.muon_CBtrack_chi2[i] == -999: return False 
    
    if doTiming: 
        tz = t_avg(event, i)
        if verbose: print "t0        ", tz
        if abs(tz) > 30: return False       
         
    if doIso:
        if event.muon_topoetcone20[i]/event.muon_pt[i] > 0.15: return False
        if event.muon_ptvarcone20[i]/event.muon_pt[i] > 0.04: return False
        #if verbose: print "iso       ", event.muon_FCTightTTVA[i]
        #if not event.muon_FCTightTTVA[i]: return False
    
    if verbose:
        print "this muon was selected"

    return True


def baseline_muon(event,i):
    if event.muon_pt[i] < 50: return False
    if abs(event.muon_eta[i]) > 2.5: return False
    if abs( event.muon_IDtrack_z0[i] ) > 500: return False
    if abs( event.muon_IDtrack_d0[i] ) > 300: return False
    #if abs( event.muon_IDtrack_d0[i] ) < 2: return False
        
    return True 


def preselect_muon_basechi2(event, i, dod0=True, doQual=True, doIso=True, doTrackQual=True, verbose=False):
    
    d0 = event.muon_IDtrack_d0[i]
    z0 = event.muon_IDtrack_z0[i]
    if verbose:
        print 

    if not event.muon_hasIDtrack[i]: return False
    if not event.muon_hasCBtrack[i]: return False
    if not event.muon_hasMStrack[i]: return False
    
    if verbose: print "pt        ", event.muon_pt[i]
    if event.muon_pt[i] < 65: return False
    
    if verbose: print "eta       ", event.muon_eta[i]
    if abs(event.muon_eta[i]) > 2.5: return False
    
    if verbose: print "z0        ", event.muon_IDtrack_z0[i]
    if abs( z0 ) > 500: return False

    if verbose: print "d0        ", event.muon_IDtrack_d0[i]
    if abs( d0 ) > 300: return False
    if dod0: 
        if abs( d0 ) < 3: return False
    
    if verbose: print "cb chi2   ", event.muon_CBtrack_chi2[i]
    
    if event.muon_CBtrack_chi2[i] > 3 or event.muon_CBtrack_chi2[i] == -999: return False 
    
     
   
    if doTrackQual:
        if verbose: print "id chi2   ", event.muon_IDtrack_chi2[i]
        if event.muon_IDtrack_chi2[i] > 2: return False
        
        if verbose: print "id nmiss  ", event.muon_IDtrack_nMissingLayers[i]
        if event.muon_IDtrack_nMissingLayers[i] > 1 : return False

    
    
    if doQual:
        if verbose: print "n pres    ", event.muon_MStrack_nPres[i]
        if event.muon_MStrack_nPres[i] < 3 or event.muon_MStrack_nPres[i] == -999: return False 
        
        if verbose: print "phi lays  ", phi_lays(event,i)
        if phi_lays(event, i) < 1: return False
        
        
         
    if doIso:
        #if event.muon_topoetcone20[i]/event.muon_pt[i] > 0.15: return False
        #if event.muon_ptvarcone20[i]/event.muon_pt[i] > 0.04: return False
        if verbose: print "iso       ", event.muon_FCTightTTVA[i]
        if not event.muon_FCTightTTVA[i]: return False
    
    if verbose:
        print "this muon was selected"

    return True

def baseline_electron(event, i):

    if event.electron_pt[i] < 50: return False
    if abs(event.electron_eta[i]) > 2.5: return False
    if abs( event.electron_d0[i] ) < 2: return False
    if abs( event.electron_d0[i] ) > 300: return False
    if abs( event.electron_d0[i] ) > 500: return False
    
    if event.electron_topoetcone20[i]/event.electron_pt[i] > 0.15: return False
    if event.electron_ptvarcone20[i]/event.electron_pt[i] > 0.04: return False

    return True

def signal_electron(event, i):

    if event.electron_pt[i] < 65: return False
    if abs(event.electron_eta[i]) > 2.5: return False
    if abs( event.electron_d0[i] ) < 3: return False
    if abs( event.electron_d0[i] ) > 300: return False
    if abs( event.electron_d0[i] ) > 500: return False
    if (event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i] < -0.5: return False
    if event.electron_nMissingLayers[i] > 1: return False
    if event.electron_chi2[i] > 2: return False

    return True

def preselect_photon(event, i):

    if event.photon_pt[i] < 65: return False
    if abs(event.photon_eta[i]) > 2.5: return False
    
    return True


def get_offset(event, im):
    offset = 0
    for mu in xrange(0,len(event.muon_pt)):
        if mu == im: return offset
        offset += event.muon_nMSSegments[mu] 


def fillMSSeg(event, i, hist, hist_zoom):

    offset = 0
    maxt=-10000
    sumt=0
    mint=10000
    
    if i != 0: offset = get_offset(event, i)

    for iseg in xrange(0,event.muon_nMSSegments[i]):
        ms = iseg + offset
        if event.muon_msSegment_t0[ms] > maxt: maxt=event.muon_msSegment_t0[ms] 
        if event.muon_msSegment_t0[ms] < mint: mint=event.muon_msSegment_t0[ms] 
        sumt+=event.muon_msSegment_t0[ms]
        if event.muon_msSegment_t0[ms] != 0: hist.Fill(event.muon_msSegment_t0[ms])
        if event.muon_msSegment_t0[ms] != 0: hist_zoom.Fill(event.muon_msSegment_t0[ms])
    
    avg = -999 
    if event.muon_nMSSegments[i] != 0 : avg = sumt*1.0/event.muon_nMSSegments[i]

    return maxt, mint, avg 

def t_avg(event, i):
    
    offset = 0
    sumt = 0
    avg = -999

    if i !=0 : offset = get_offset(event,i)
    
    for iseg in xrange(0,event.muon_nMSSegments[i]):
        
        ms = iseg + offset
        
        if event.muon_msSegment_t0[ms] == 0: continue

        sumt+=event.muon_msSegment_t0[ms]

    if event.muon_nMSSegments[i] != 0: avg = sumt*1.0/event.muon_nMSSegments[i]
    
    if avg == 0: avg = 999

    return avg

def phi_lays(event, i):
    
    offset = 0
    sumt = 0

    if i !=0 : offset = get_offset(event,i)
    
    for iseg in xrange(0,event.muon_nMSSegments[i]):
        
        ms = iseg + offset

        sumt+=event.muon_msSegment_nPhiLays[ms]


    return sumt


def setHistos(tag, histos):
    h={}
    for name in histos:
        try: 
            h[name] = ROOT.TH1F(tag + "_" + name, name, histos[name]["nb"], histos[name]["low"], histos[name]["high"])
        
        except:
             h[name] = ROOT.TH1F(tag + "_" + name, name, len(histos[name]["bins"])-1, array('d',histos[name]["bins"]))
        
        h[name].GetXaxis().SetTitle(histos[name]["label"])
    return h

def setHistos2D(tag, histos):
    h={}
    for name in histos:
        try: 
            h[name] = ROOT.TH2F(tag + "_" + name,name, histos[name]["nb_x"], histos[name]["low_x"], histos[name]["high_x"], histos[name]["nb_y"], histos[name]["low_y"], histos[name]["high_y"]) 
        except:
            h[name] = ROOT.TH2F(tag + "_" + name,name, len(histos[name]["bins_x"])-1, array('d',histos[name]["bins_x"]), len(histos[name]["bins_y"])-1, array('d',histos[name]["bins_y"]))
        h[name].GetXaxis().SetTitle(histos[name]["label_x"])
        h[name].GetYaxis().SetTitle(histos[name]["label_y"])
    return h




def deltaRCos(v1, v2):
	sumEta = abs(v1.Eta() + v2.Eta())
	dPhi = v1.DeltaPhi(v2)
	dR = math.sqrt((abs(dPhi)-math.pi)**2 + sumEta**2)

	return dR

def eta_corrected(event, v, ms, z0):
        
    #R_seg = math.sqrt(event.msSegment_x[ms]**2 + event.msSegment_y[ms]**2)
    R_seg = ROOT.TMath.Hypot(event.msSegment_x[ms], event.msSegment_y[ms])
    
    #try: z_corr = R_seg/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    #z_corr = R_seg/math.tan(v.Theta()) - event.muon_MStrack_Z0[im]
    z_corr = R_seg/math.tan(v.Theta()) - z0
    #except: z_corr = R_seg/math.tan(v.Theta()) - event.muon_z0[im]
    
    theta_corr = math.atan(abs(R_seg/z_corr))
    eta_corr = -math.log(math.tan(theta_corr/2.0))
    if z_corr < 0: eta_corr = -eta_corr
    
    #h["eta_orig"].Fill(v.Eta())
    #h["theta_orig"].Fill(v.Theta())
    #h["theta_corr"].Fill(theta_corr)
    #h["eta_corr"].Fill(eta_corr)

    return eta_corr

def theta_corrected(event, v, vt, ms, im):
        
    R_seg = ROOT.TMath.Hypot(event.msSegment_x[ms], event.msSegment_y[ms])
    
    z_corr = R_seg/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    
    theta_corr = math.atan(abs(R_seg/z_corr))
    if z_corr < 0: theta_corr = -theta_corr
    #if  vt.Phi() < 0: theta_corr = -theta_corr
    
    return theta_corr

def eta_corrected_R(event, im, R):
    
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    try: z_corr = R/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    except: z_corr = R/math.tan(v.Theta()) - event.muon_z0[im]
    
    theta_corr = math.atan(abs(R/z_corr))
    eta_corr = -math.log(math.tan(theta_corr/2.0))
    if z_corr < 0: eta_corr = -eta_corr
    
    return eta_corr

def theta_corrected_R(event, im, R):
    
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    try: z_corr = R/math.tan(v.Theta()) - event.muon_IDtrack_z0[im]
    except: z_corr = R/math.tan(v.Theta()) - event.muon_z0[im]
    
    theta_corr = math.atan(abs(R/z_corr))
    if z_corr < 0: theta_corr = -theta_corr
    
    return theta_corr

def mostBackToBack_id(event, im):
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    mindR = 999
    iMin = -1
    for idt in xrange(len(event.idTrack_eta)):
        vt = ROOT.TVector3()
        vt.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
        
        dR = deltaRCos(v, vt)
        h["dR_leadmu_allid"].Fill(dR)
        h["dPhi_leadmu_allid"].Fill( abs(v.DeltaPhi(vt)) )
        h["sumEta_leadmu_allid"].Fill( v.Eta() + vt.Eta() )
        if dR < mindR:
            mindR = dR
            iMin = idt

    return iMin  

def mostBackToBack_ms(event, im, h, h2, cosmic=True):
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    mindR = 999
    iMin = -1
    min_sumEta = 999
    min_sumEta_corr = 99
    
    min_sumTheta = 999
    min_sumTheta_corr = 999
    
    min_dPhi = 999
    min_dPhi_corr = 999
    
    min_dPhi_t = 999
    min_dPhi_corr_t = 999
            
    min_delta_sum_eta = 999 
    min_delta_eta = 999
    
    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms]) 
        
       # if abs(vt.DeltaPhi(v)) < math.pi/2: continue
        
        eta_corr = eta_corrected(event, v, ms, event.muon_IDtrack_z0[im])
        sum_eta_corr = abs( eta_corr + vt.Eta() )
        
        theta_corr = theta_corrected(event, v, vt, ms, im)
        sum_theta_corr = abs(abs(theta_corr + vt.Theta()) - math.pi)
        
        
        dPhi = abs(abs(v.DeltaPhi(vt)) - math.pi)

         
        if abs(v.Eta() + vt.Eta()) < min_sumEta and dPhi < min_dPhi :
            min_sumEta = abs(v.Eta() + vt.Eta())
            min_dPhi = dPhi
        
        if abs(v.Theta() + vt.Theta()) < min_sumTheta and dPhi < min_dPhi_t :
            min_sumTheta = abs(v.Eta() + vt.Eta())
            min_dPhi_t = dPhi
        
        if sum_eta_corr < min_sumEta_corr and dPhi < min_dPhi_corr :
            min_sumEta_corr = sum_eta_corr
            min_delta_sum_eta = sum_eta_corr - abs(v.Eta() + vt.Eta()) 
            min_delta_eta = eta_corr - v.Eta() 
            min_dPhi_corr = dPhi
        
        if sum_theta_corr < min_sumTheta_corr and dPhi < min_dPhi_corr_t :
            min_sumTheta_corr = sum_theta_corr
            min_dPhi_corr_t = dPhi

    h2["sumEta_dPhi_min"].Fill(          min_sumEta,      min_dPhi)
    h2["sumEta_dPhi_min_zoom"].Fill(     min_sumEta,      min_dPhi)
    
    h2["sumTheta_dPhi_min"].Fill(        min_sumTheta,    min_dPhi_t)
    h2["sumTheta_dPhi_min_zoom"].Fill(   min_sumTheta,    min_dPhi_t)
    
    h2["sumEta_dPhi_min_corr"].Fill(         min_sumEta_corr,    min_dPhi_corr)
    h2["sumEta_dPhi_min_corr_zoom"].Fill(    min_sumEta_corr,    min_dPhi_corr)
#    h2["min_delta_eta_z0"].Fill( min_delta_eta, event.muon_IDtrack_z0[im] )
#    h2["min_delta_sum_eta_z0"].Fill( min_delta_sum_eta, event.muon_IDtrack_z0[im] )
#    h["min_delta_eta"].Fill( min_delta_eta )
#    h["min_delta_sum_eta"].Fill( min_delta_sum_eta )
    
    h2["sumTheta_dPhi_min_corr"].Fill(         min_sumTheta_corr,    min_dPhi_corr_t)
    h2["sumTheta_dPhi_min_corr_zoom"].Fill(    min_sumTheta_corr,    min_dPhi_corr_t)
    
#    if cosmic:
#        h2["cos_min_delta_eta_z0"].Fill( min_delta_eta, event.muon_IDtrack_z0[im] )
#        h2["cos_min_delta_sum_eta_z0"].Fill( min_delta_sum_eta, event.muon_IDtrack_z0[im] )
 #   else:
#        h2["mu_min_delta_eta_z0"].Fill( min_delta_eta, event.muon_IDtrack_z0[im] )
#        h2["mu_min_delta_sum_eta_z0"].Fill( min_delta_sum_eta, event.muon_IDtrack_z0[im] )
    
    return iMin

def cosTag(event, im, dEta, dPhi):
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    min_sumEta_corr = 999
    min_delta_sum_eta = 999
    min_delta_eta = 999
    min_dPhi_corr = 999
    ms_min = -1
    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        
        #if abs(vt.DeltaPhi(v)) < math.pi/2: continue
        
        #if vt.Phi() * v.Phi() > 0: continue
        
        eta_corr = eta_corrected(event, v, ms, event.muon_IDtrack_z0[im])
        sum_eta_corr = abs( eta_corr + vt.Eta() )
        delta_eta_corr = abs( eta_corr - vt.Eta() )
        
        deltaPhi = abs((abs(v.DeltaPhi(vt)) - math.pi))
        
        if deltaPhi < dPhi and sum_eta_corr < dEta : 
       #     h2["cos"]["min_delta_eta_z0"].Fill( eta_corr - v.Eta(), event.muon_IDtrack_z0[im] )
       #     h2["cos"]["min_delta_sum_eta_z0"].Fill( sum_eta_corr - abs(v.Eta() + vt.Eta()) , event.muon_IDtrack_z0[im] )
            
             
            return True, ms
        
        if sum_eta_corr < min_sumEta_corr and dPhi < min_dPhi_corr :
            ms_min = ms
            min_sumEta_corr = sum_eta_corr
            min_delta_sum_eta = sum_eta_corr - abs(v.Eta() + vt.Eta()) 
            min_delta_eta = eta_corr - v.Eta() 
            min_dPhi_corr = dPhi
        
    
#    h2["mu"]["min_delta_eta_z0"].Fill( min_delta_eta, event.muon_IDtrack_z0[im] )
#    h2["mu"]["min_delta_sum_eta_z0"].Fill( min_delta_sum_eta, event.muon_IDtrack_z0[im] )
     
    return False, ms


def materialVeto(event, im, innerMap, middleMap, outerMap):

    if abs(event.muon_eta[im]) > 1.05: return False
    n_poss_seg = 0 
    
    phi = math.pi + event.muon_phi[im]
    if (phi >= math.pi): phi -= 2*math.pi
    if (phi < -math.pi): phi += 2*math.pi


    if( innerMap.GetBinContent( innerMap.GetXaxis().FindBin( -eta_corrected_R(event, im,4500)) , innerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( middleMap.GetBinContent( middleMap.GetXaxis().FindBin( -eta_corrected_R(event, im,7000)) , middleMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( outerMap.GetBinContent( outerMap.GetXaxis().FindBin( -eta_corrected_R(event, im,9200)) , outerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    
    if n_poss_seg > 1: return False

    #not enough segments to have reconstructed a cosmic --> veto
    return True
    
    
 
def cosTag_theta(event, im, dTheta, dPhi):
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[im], event.muon_eta[im], event.muon_phi[im])
    
    for ms in xrange(len(event.msSegment_x)):
        vt = ROOT.TVector3()
        vt = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms])
        
        if vt.Phi() * v.Phi() > 0: continue
        
        theta_corr = theta_corrected(event, v, vt, ms, im)
        sum_theta_corr = abs (abs( theta_corr + vt.Theta() ) - math.pi)
        
        deltaPhi = abs((abs(v.DeltaPhi(vt)) - math.pi))
        
        if deltaPhi < dPhi and sum_theta_corr < dTheta : return True
    
     
    return False


def materialVeto_theta(event, im, innerMap, middleMap, outerMap):

    if abs(event.muon_eta[im]) > 1.05: return False
    n_poss_seg = 0 
    
    phi = math.pi + event.muon_phi[im]
    if (phi >= math.pi): phi -= 2*math.pi
    if (phi < -math.pi): phi += 2*math.pi


    if( innerMap.GetBinContent( innerMap.GetXaxis().FindBin( -theta_corrected_R(event, im,4500)) , innerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( middleMap.GetBinContent( middleMap.GetXaxis().FindBin( -theta_corrected_R(event, im,7000)) , middleMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    if( outerMap.GetBinContent( outerMap.GetXaxis().FindBin( -theta_corrected_R(event, im,9200)) , outerMap.GetYaxis().FindBin(phi)) == 1): n_poss_seg += 1
    
    if n_poss_seg > 1: return False

    #not enough segments to have reconstructed a cosmic --> veto
    return True 
