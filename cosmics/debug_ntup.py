import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

execfile("cosmic_helpers.py")				       


tag = "debug"
append = ""


if tag == "debug":
    AODFiles = glob.glob('debug_ntup.root')
    t_cos = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t_cos.Add(filename)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names = { 
    "cutflow_1" : {"nb": 12, "low": 0, "high": 12, "label": "cutflow"},
    "cutflow_2" : {"nb": 12, "low": 0, "high": 12, "label": "cutflow"},
    "cutflow_3" : {"nb": 12, "low": 0, "high": 12, "label": "cutflow"},
}


h = setHistos(tag,names)


print t_cos.GetEntries()
eventNum = 0

nmu = 0
v1_nmups= 0
v1_nmupsnotcos = 0
v1_nevents = 0

v2_nmups= 0
v2_nmupsnotcos = 0
v2_nevents = 0

for event in t_cos:
    if eventNum % 10000 == 0: print "Processing event ", eventNum
    eventNum+=1
    
    try:
        event.muon_pt
    except:
        continue
     
    if not event.muon_pt: continue
    if not event.muon_n == 2: continue 
    #if event.lepton_n > 1: continue
        
    nPs = 0
    nPs_2 = 0
    nCos = 0
    nCos_2 = 0
    foundFirst = False


    for im in xrange(len(event.muon_pt)):
        nmu+= 1
        h["cutflow_1"].Fill("all",1)
        if event.muon_pt[im] > 65:
            h["cutflow_1"].Fill("pt",1)
            if abs(event.muon_eta[im]) < 2.5:
                h["cutflow_1"].Fill("eta",1)
                if abs(event.muon_IDtrack_d0[im]) > 3 and event.muon_IDtrack_d0[im] != -999:
                    h["cutflow_1"].Fill("d0_min",1)
                    if abs(event.muon_IDtrack_d0[im]) < 300 and event.muon_IDtrack_d0[im] != -999:
                        h["cutflow_1"].Fill("d0_max",1)
                        if abs(event.muon_IDtrack_z0[im]) < 500 and event.muon_IDtrack_z0[im] != -999:
                            h["cutflow_1"].Fill("z0_max",1)
                            if event.muon_CBtrack_chi2[im] < 3 and event.muon_CBtrack_chi2[im] != -999:
                                h["cutflow_1"].Fill("chi2",1)
                                if event.muon_MStrack_nPres[im] > 2 and event.muon_MStrack_nPres[im] != -999:
                                    h["cutflow_1"].Fill("npres",1)
                                    if not cosTag(event,im,0.013,0.18):
                                        h["cutflow_1"].Fill("costag",1)
                                        if not materialVeto(event,im, innerMap, middleMap, outerMap):
                                            h["cutflow_1"].Fill("matveto",1)


        h["cutflow_2"].Fill("all",1)
        if event.muon_ptg65[im]:
            h["cutflow_2"].Fill("pt",1)
            if event.muon_etal25[im]:
                h["cutflow_2"].Fill("eta",1)
                if event.muon_d0g3[im]:
                    h["cutflow_2"].Fill("d0_min",1)
                    if event.muon_d0l300[im]:
                        h["cutflow_2"].Fill("d0_max",1)
                        if event.muon_z0l500[im]:
                            h["cutflow_2"].Fill("z0_max",1)
                            if event.muon_chi2l3[im]:
                                h["cutflow_2"].Fill("chi2",1)
                                if event.muon_npresg2[im]:
                                    h["cutflow_2"].Fill("npres",1)
                                    if not event.muon_cosmicTag_VR[im]:
                                        h["cutflow_2"].Fill("costag",1)
                                        if not event.muon_cosmicTag_MV[im]:
                                            h["cutflow_2"].Fill("matveto",1)
        
        h["cutflow_3"].Fill("all",1)
        if event.muon_isBaseline[im]:
            h["cutflow_3"].Fill("pt",0)
            h["cutflow_3"].Fill("eta",0)
            h["cutflow_3"].Fill("d0_min",0)
            h["cutflow_3"].Fill("d0_max",0)
            h["cutflow_3"].Fill("z0_max",1)
            if event.muon_qual_good[im]:
                h["cutflow_3"].Fill("chi2",0)
                h["cutflow_3"].Fill("npres",1)
                if not event.muon_cosmicTag_VR[im]:
                    h["cutflow_3"].Fill("costag",1)
                    if not event.muon_cosmicTag_MV[im]:
                        h["cutflow_3"].Fill("matveto",1)

c = ROOT.TCanvas()

for bi in xrange(0,h["cutflow_1"].GetNbinsX()):
    print str(bi) + ":\t " + str(h["cutflow_1"].GetBinContent(bi)) + " \t" + str(h["cutflow_2"].GetBinContent(bi)) + " \t" + str(h["cutflow_3"].GetBinContent(bi)) 



ROOT.gPad.SetLogy()
h["cutflow_1"].SetLineColor(ROOT.kBlue)
h["cutflow_3"].SetMarkerStyle(5)
h["cutflow_3"].SetMarkerColor(ROOT.kRed)
h["cutflow_1"].Draw("hist")
h["cutflow_2"].Draw(" psame")
h["cutflow_3"].Draw(" psame")
c.SaveAs("cutflows.pdf")

