import glob
import ROOT
import os

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(0)

c = ROOT.TCanvas()

f = ROOT.TFile("outputFiles/timing_wider_tag_histos.root")

prompt = f.Get("t0_avg_prompt_data")
prompt.SetTitle("prompt")
cosmic = f.Get("t0_avg_cosmic_data")
cosmic.SetTitle("cosmic")
precos = f.Get("t0_avg_1mu_precos_data")
precos.SetTitle("before cosmic removal")
postcos = f.Get("t0_avg_1mu_postcos_data")
postcos.SetTitle("after cosmic removal")

prompt.Scale(1.0/prompt.Integral())
cosmic.Scale(1.0/cosmic.Integral())
precos.Scale(1.0/precos.Integral())
postcos.Scale(1.0/postcos.Integral())
postcos.GetXaxis().SetTitle("t_{0, avg}")

prompt.SetLineColor(ROOT.kCyan+1)
cosmic.SetLineColor(ROOT.kBlue+1)

precos.SetLineColor(ROOT.kMagenta+1)
postcos.SetLineColor(ROOT.kViolet+6)


leg = ROOT.TLegend(0.57,0.8,0.87,0.92)
leg.AddEntry(prompt,prompt.GetTitle(),"l")
leg.AddEntry(cosmic,cosmic.GetTitle(),"l")
leg.SetFillStyle(0)


prompt.Draw("hist")
cosmic.Draw("hist same")
leg.Draw("same")

c.Modified()
c.Update()

c.SaveAs("outputPlots/timing/nice_avg_template_comp.pdf")


c.Clear()



leg = ROOT.TLegend(0.52,0.75,0.82,0.92)
leg.AddEntry(precos,precos.GetTitle(),"l")
leg.AddEntry(postcos,postcos.GetTitle(),"l")
leg.SetFillStyle(0)

postcos.Draw("hist")
precos.Draw("hist same")
leg.Draw("same")

ROOT.ATLASLabel(0.20,0.88, "Internal")

c.Modified()
c.Update()

c.SaveAs("outputPlots/timing/nice_avg_prepost_comp.pdf")


