import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

append = "signal_pt"


ttbar_files = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar_lowpt/user.tholmes.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.060520_trees.root/*"

AODFiles = glob.glob(ttbar_files)
ttbar = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    ttbar.Add(filename)

#signal_files = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.399019.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_1ns.DAOD_RPVLL_mc16*_v5.1_trees.root/*"
signal_files = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.399019.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_400_0_1ns.DAOD_RPVLL_mc16*_v5.1_trees.root/*"
AODFiles = glob.glob(signal_files)
signal = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    signal.Add(filename)


binstr_hist = "50,-50,50"
binstr_eff = "7,-50,50"

h_ttbar = getHist(ttbar, "t0_ttbar","muon_t0avg",binstr=binstr_hist,sel_name="muon_t0avg !=0")
h_signal = getHist(signal, "t0_signal","muon_t0avg",binstr=binstr_hist,sel_name="muon_t0avg !=0")

h_ttbar_pass = getHist(ttbar, "t0_ttbar_pass","muon_t0avg",binstr=binstr_eff,sel_name="muon_t0avg !=0 && muon_pt[0] > 60 && abs(muon_eta[0]) < 1.05 && pass_HLT_mu60_0eta105_msonly")
h_ttbar_all = getHist(ttbar, "t0_ttbar_all","muon_t0avg",binstr=binstr_eff,sel_name="muon_t0avg !=0 && muon_pt[0] > 60 && abs(muon_eta[0]) < 1.05")

h_signal_pass = getHist(signal, "t0_signal_pass","muon_t0avg",binstr=binstr_eff,sel_name="muon_t0avg !=0 && muon_pt[0] > 60 && abs(muon_eta[0]) < 1.05 && pass_HLT_mu60_0eta105_msonly")
h_signal_all = getHist(signal, "t0_signal_all","muon_t0avg",binstr=binstr_eff,sel_name="muon_t0avg !=0 && muon_pt[0] > 60 && abs(muon_eta[0]) < 1.05")





c = ROOT.TCanvas()
#ROOT.gPad.SetLogy()
h_ttbar.SetLineColor(ROOT.kBlue-4)
h_ttbar.Scale(1/h_ttbar.Integral())
h_signal.SetLineColor(ROOT.kRed+1)
h_signal.Scale(1/h_signal.Integral())

h_ttbar.GetYaxis().SetTitle("normalized a.u.")
h_ttbar.GetXaxis().SetTitle("t_{0} avg [ns]")
h_ttbar.SetMaximum(h_ttbar.GetMaximum()*1.5)

h_ttbar.Draw("hist")
h_signal.Draw("hist same")

leg = ROOT.TLegend(0.55,0.80,0.80,0.92)
leg.AddEntry(h_ttbar, "ttbar", "l")
leg.AddEntry(h_signal, "400 GeV stau, 1ns", "l")
leg.Draw("same")

ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "Muons")

ROOT.gPad.Update()
c.Modified()
c.Update()
c.SaveAs("timing.pdf")

#ratio_ttbar = h_ttbar_pass.Clone("ttbar_ratio")
#ratio_ttbar.Divide(h_ttbar_pass, h_ttbar_all,1,1,"B")
ratio_ttbar = ROOT.TEfficiency(h_ttbar_pass, h_ttbar_all)
ratio_ttbar.SetMarkerColor(ROOT.kBlue-4)
ratio_ttbar.SetLineColor(ROOT.kBlue-4)
ratio_ttbar.Draw("pea")
ROOT.gPad.Update()
ratio_ttbar.SetTitle(";t_{0} avg [ns]; trigger efficiency")
ratio_ttbar.GetPaintedGraph().SetMaximum(1.5)
ratio_ttbar.GetPaintedGraph().SetMinimum(0)

#ratio_signal = h_signal_pass.Clone("signal_ratio")
#ratio_signal.Divide(h_signal_pass, h_signal_all,1,1,"B")
ratio_signal = ROOT.TEfficiency(h_signal_pass, h_signal_all)
ratio_signal.SetMarkerColor(ROOT.kRed+1)
ratio_signal.SetLineColor(ROOT.kRed+1)

ratio_signal.Draw("p same")


leg = ROOT.TLegend(0.55,0.80,0.80,0.92)
leg.AddEntry(ratio_ttbar, "ttbar", "p")
leg.AddEntry(ratio_signal, "400 GeV stau, 1ns", "p")
leg.Draw("same")

ROOT.ATLASLabel(0.20,0.88, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.DrawLatex(0.20,0.83, "Muons")

ROOT.gPad.Update()
c.Modified()
c.Update()
c.SaveAs("timing_efficiency.pdf")


#makeRatioPlot(ratio_ttbar, ratio_signal, 

