# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        "photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/user.lhoryn.data17_13TeV.periodI.physics_Main.DAOD_RPVLL_041619_trees.root/user.lhoryn.17749537._000*.root"},
        }

# Define selections
#event_selection = "electron_n > 1 && triggerRegion_pass"
#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 3., abs_val=True)

event_selection = "electron_n > 1 && triggerRegion_pass"
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_pt", 65)
e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 1., abs_val=True)

e_pass = Selection("electron_pass")
e_pass.addCut("electron_dpt", -.5)
# For the future, add isolation? Other quality checks?

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])

    # Make basic event selection
    df = samples[s]["initial_rdf"].Filter(event_selection, "ee selection")
    print "ee selection:", event_selection
    df_baseline = df.Filter(e_baseline.getCutString(0), "e0 baseline").Filter(e_baseline.getCutString(1), "e1 baseline")
    print "e0 selection:", e_baseline.getCutString(0)
    print "e1 selection:", e_baseline.getCutString(1)

    # Define needed variables
    df_tpt = df_baseline.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt")

    # Separate into categories
    samples[s]["rdfs"] = {}

    # For looking at just one lepton
    # --- Both pass (A)
    cut_str = " %s && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["A"] = df_tpt.Filter(cut_str, "A")
    # --- Lep0 pass (B)
    cut_str = " %s && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["B"] = df_tpt.Filter(cut_str, "B")
    # --- Lep1 pass (C)
    cut_str = " !(%s) && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["C"] = df_tpt.Filter(cut_str, "C")
    # --- None pass (D)
    cut_str = " !(%s) && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["D"] = df_tpt.Filter(cut_str, "D")

    #h = samples[s]["rdfs"]["A"].Histo1D("electron_d0")
    #h.Draw()
    #raw_input("...")

    # Save histograms
    samples[s]["hists"] = {}
    model = ROOT.RDF.TH1DModel("m_ht", "m_ht", 100, 0., 1000.)
    samples[s]["hists"]["A"] = samples[s]["rdfs"]["A"].Histo1D(model, "HT", "normweight")
    samples[s]["hists"]["B"] = samples[s]["rdfs"]["B"].Histo1D(model, "HT", "normweight")
    samples[s]["hists"]["C"] = samples[s]["rdfs"]["C"].Histo1D(model, "HT", "normweight")
    samples[s]["hists"]["D"] = samples[s]["rdfs"]["D"].Histo1D(model, "HT", "normweight")

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)
    ratio_cd = tot_C / tot_D
    h_est = samples[s]["hists"]["B"].Clone("h_est").Scale(ratio_cd)

    print "\nResults"
    #print "Estim in A:", h_est.Integral(0, h_est.GetNbinsX()+1)
    print "Total in A:", tot_A
    print "Total in B:", tot_B
    print "Total in C:", tot_C
    print "Total in D:", tot_D

    # Cutflow for fun
    #print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


