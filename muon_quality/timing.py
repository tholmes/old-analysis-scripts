import ROOT
import math
import glob
import os
import sys

#ROOT.ROOT.EnableImplicitMT()

execfile("../cosmics/cosmic_helpers.py")				       
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

append = "narrow_"


nbins = 200
high = 30
low = -30



names = {

     
    "all_cosmic_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlack},   
    "all_signal_mc": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue-7},   
    "all_prompt_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color":  ROOT.kCyan+1},   
    "all_prompt_mc": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    "all_1mu_precos_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    "all_1mu_postcos_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    "all_1mu_highd0_precos_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    "all_1mu_highd0_postcos_data": { "nb": nbins, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "allzoom_cosmic_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlack},   
    #"allzoom_signal_mc": { "nb": 500, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue-7},   
    "allzoom_prompt_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color":  ROOT.kCyan+1},   
    #"allzoom_prompt_mc": { "nb": 500, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "allzoom_1mu_precos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "allzoom_1mu_postcos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "allzoom_1mu_highd0_precos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "allzoom_1mu_highd0_postcos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "allseg_cosmic_data": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlack},   
    "allseg_signal_mc": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue-7},   
    "allseg_prompt_data": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color":  ROOT.kCyan+1},   
    "allseg_prompt_mc": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kBlue},   
    "allseg_alldata_cosmic": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kViolet},   
    "allseg_alldata_noncosmic": { "nb": 60, "low": -800, "high": 800, "label": "t_{0}", "color": ROOT.kMagenta},   
    
    "allsegzoom_cosmic_data": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlack},   
    "allsegzoom_signal_mc": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue-7},   
    "allsegzoom_prompt_data": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color":  ROOT.kCyan+1},   
    "allsegzoom_prompt_mc": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "allsegzoom_alldata_cosmic": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kViolet},   
    "allsegzoom_alldata_noncosmic": { "nb": 40, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kMagenta},   

    "max_cosmic_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, max}", "color": ROOT.kBlack},   
    "max_signal_mc": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, max}", "color":  ROOT.kBlue-7},   
    "max_prompt_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, max}", "color": ROOT.kCyan+1},   
    "max_prompt_mc": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, max}", "color": ROOT.kBlue},   
    "max_1mu_precos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "max_1mu_postcos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "max_1mu_highd0_precos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "max_1mu_highd0_postcos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "mini_cosmic_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, min}", "color": ROOT.kBlack},   
    "mini_signal_mc": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, min}", "color":  ROOT.kBlue-7},   
    "mini_prompt_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, min}", "color": ROOT.kCyan+1},   
    "mini_prompt_mc": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0, min}", "color": ROOT.kBlue},   
    "mini_1mu_precos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "mini_1mu_postcos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "mini_1mu_highd0_precos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    "mini_1mu_highd0_postcos_data": { "nb": nbins, "low": -100, "high": 100, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "delta_cosmic_data": { "nb": nbins, "low": 0, "high": 80, "label": "\Delta t_{0}", "color": ROOT.kBlack},   
    "delta_signal_mc": { "nb": nbins, "low": 0, "high": 80, "label": "\Delta t_{0}", "color":  ROOT.kBlue-7},   
    "delta_prompt_data": { "nb": nbins, "low": 0, "high": 80, "label": "\Delta t_{0}", "color": ROOT.kCyan+1},   
    "delta_prompt_mc": { "nb": nbins, "low": 0, "high": 80, "label": "\Delta t_{0}", "color": ROOT.kBlue},   
    "delta_1mu_precos_data": { "nb": nbins, "low": 0, "high": 80, "label": "t_{0}", "color": ROOT.kBlue},   
    "delta_1mu_postcos_data": { "nb": nbins, "low": 0, "high": 80, "label": "t_{0}", "color": ROOT.kBlue},   
    "delta_1mu_highd0_precos_data": { "nb": nbins, "low": 0, "high": 80, "label": "t_{0}", "color": ROOT.kBlue},   
    "delta_1mu_highd0_postcos_data": { "nb": nbins, "low": 0, "high": 80, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "avg_cosmic_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0, avg}", "color": ROOT.kBlack},   
    "avg_signal_mc": { "nb":nbins, "low": -30, "high": 30, "label": "t_{0, avg}", "color":  ROOT.kBlue-7},   
    "avg_prompt_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0, avg}", "color": ROOT.kCyan+1},   
    "avg_prompt_mc": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0, avg}", "color": ROOT.kBlue},   
    "avg_1mu_precos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_1mu_postcos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_1mu_highd0_precos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_1mu_highd0_postcos_data": { "nb": nbins, "low": -30, "high": 30, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "avg_zoomout_cosmic_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0, avg}", "color": ROOT.kBlack},   
    "avg_zoomout_signal_mc": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0, avg}", "color":  ROOT.kBlue-7},   
    "avg_zoomout_prompt_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0, avg}", "color": ROOT.kCyan+1},   
    "avg_zoomout_prompt_mc": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0, avg}", "color": ROOT.kBlue},   
    "avg_zoomout_1mu_precos_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_zoomout_1mu_postcos_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_zoomout_1mu_highd0_precos_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0}", "color": ROOT.kBlue},   
    "avg_zoomout_1mu_highd0_postcos_data": { "nb": 1000, "low": -400, "high": 400, "label": "t_{0}", "color": ROOT.kBlue},   
    
    "n_cosmic_data": { "nb": 15, "low": 0, "high": 15, "label": "n_{segments}", "color": ROOT.kBlack},   
    "n_signal_mc": { "nb": 15, "low": 0, "high": 15, "label": "n_{segments}", "color":  ROOT.kBlue-7},   
    "n_prompt_data": { "nb": 15, "low": 0, "high": 15, "label": "n_{segments}", "color": ROOT.kCyan+1},   
    "n_prompt_mc": { "nb": 15, "low": 0, "high": 15, "label": "n_{segments}", "color": ROOT.kBlue},   
    "n_1mu_precos_data": { "nb": 15, "low": 0, "high": 15, "label": "t_{0}", "color": ROOT.kBlue},   
    "n_1mu_postcos_data": { "nb": 15, "low": 0, "high": 15, "label": "t_{0}", "color": ROOT.kBlue},   
    "n_1mu_highd0_precos_data": { "nb": 15, "low": 0, "high": 15, "label": "t_{0}", "color": ROOT.kBlue},   
    "n_1mu_highd0_postcos_data": { "nb": 15, "low": 0, "high": 15, "label": "t_{0}", "color": ROOT.kBlue},   

}

names_2d ={
    "d0_t0_zoom"       : { "nb_x": 50, "low_x": -100, "high_x": 100, "label_x": "d_{0}", "nb_y": 50, "low_y": -30, "high_y": 30, "label_y": "t_{0}", "log":1},
    "d0_t0"       : { "nb_x": 100, "low_x": -300, "high_x": 300, "label_x": "d_{0}", "nb_y": 100, "low_y": -100, "high_y": 100, "label_y": "t_{0}", "log":1},
} 
h = {}
h2 = {}
h = setHistos("t0", names)
h2 = setHistos2D("t0", names_2d)

#tags = ["data_msseg","300_slep_msseg", "ttbar_msseg", "data"]
#tags = ["data_msseg","300_slep_msseg", "ttbar_msseg"]
tags = ["cosmic_data","prompt_data"]
#tags = ["300_slep_msseg","ttbar_msseg"]

failedFits = {}

colors = [0, ROOT.kBlue-5, ROOT.kBlue-7, ROOT.kCyan+1, ROOT.kCyan+3, ROOT.kBlue+1, ROOT.kBlue+3]

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


for tag in tags:
    if tag == "prompt_data":
        #basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/user.lhoryn.data17_13TeV.period*/*'
        basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/prompt_skim.root'
    
    if tag == "cosmic_data":
        basename ='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/1mu/*.root'
    
    if tag == "300_slep_msseg":
        basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.399044.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_400_0_1ns.DAOD_RPVLL_mc16*_v5.1_trees.root/*'
        dsid=399044

    if tag == "ttbar_msseg":
        basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar/*/*.root'
        dsid=410470

    if tag == "data":
        basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar/*/*.root'
    


    AODFiles = glob.glob(basename)
    t = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t.Add(filename)



    print tag
    print t.GetEntries()
    nevents = t.GetEntries()
    
    #lumi = 50610   * .99


    #lumi = 139
    weight = 1
    if "data" not in tag:     
        xs = getXS(dsid)
        weight = xs*lumi / nevents

    eventNum = 0

    failed = 0
    total = 0
    for event in t:
        if eventNum % 10000 == 0: print "Processing event ", eventNum
        eventNum+=1
        iLead = -1
        #if eventNum > 854106: break
        #if eventNum > 10000: break
        
                
        if not event.pass_HLT_mu60_0eta105_msonly: continue
        try:
            event.muon_pt
            if tag != "data": event.muon_msSegment_t0
        except:
            continue
        if not event.muon_pt: continue
        

        if len(event.lepton_pt) != 1: continue
        if len(event.muon_pt) !=1: continue

        cosmic = False
       
       
       
        if not preselect_muon(event, 0, dod0=False, doTiming=False): continue
        cos,ims = cosTag(event, 0, .013, .02)
        if cos or materialVeto(event, 0, innerMap, middleMap, outerMap): cosmic = True
        t0 = t_avg(event, 0)


        
        if tag == "cosmic_data" and abs(event.muon_IDtrack_d0[0]) > 3:
            #t_max, t_min, t_avg = fillMSSeg(event, 0, h["all_1mu_precos_data"], h["allzoom_1mu_precos_data"])
            #if t_min != 0: h["mini_1mu_precos_data"].Fill(t_min)
            #if t_max != 0: h["max_1mu_precos_data"].Fill(t_max)
            #if t_max != 0 and t_min != 0: h["delta_1mu_precos_data"].Fill(abs( abs(t_max) - abs(t_min)))
            if t0 != 0: 
                h["avg_1mu_precos_data"].Fill(t0)
                h["avg_zoomout_1mu_precos_data"].Fill(t0)
            if t0 == 0: failed+=1
            total +=1
            #h["n_1mu_precos_data"].Fill(event.muon_nMSSegments[0])
            if not cosmic:
                #t_max, t_min, t0 = fillMSSeg(event, 0, h["all_1mu_postcos_data"], h["allzoom_1mu_postcos_data"])
                #if t_min != 0: h["mini_1mu_postcos_data"].Fill(t_min)
                #if t_max != 0: h["max_1mu_postcos_data"].Fill(t_max)
                #if t_max != 0 and t_min != 0: h["delta_1mu_postcos_data"].Fill(abs( abs(t_max) - abs(t_min)))
                
                if t0 != 0: 
                    h["avg_1mu_postcos_data"].Fill(t0)
                    h["avg_zoomout_1mu_postcos_data"].Fill(t0)
                #h["n_1mu_postcos_data"].Fill(event.muon_nMSSegments[0])
           
            if cosmic:
                if t0 != 0: 
                    h["avg_cosmic_data"].Fill(t0)
                     
        #prompt muons
        if tag == "prompt_data":
            if not cosmic:
                if abs(event.muon_IDtrack_d0[0]) < 0.1:
                    #t_max, t_min, t_avg = fillMSSeg(event, 0, h["all_prompt_data"], h["allzoom_prompt_data"])
                    #if t_min != 0: h["mini_prompt_data"].Fill(t_min)
                    #if t_max != 0: h["max_prompt_data"].Fill(t_max)
                    #if t_max != 0 and t_min != 0: h["delta_prompt_data"].Fill(abs( abs(t_max) - abs(t_min)))
                    if t0 != 0: 
                        h["avg_prompt_data"].Fill(t0)
                        h["avg_zoomout_prompt_data"].Fill(t0)
                    if t_avg == 0: failed+=1
                    total +=1
                    #h["n_prompt_data"].Fill(event.muon_nMSSegments[0])
                #for ms in xrange(len(event.msSegment_x)):
                #    h["allseg_prompt_data"].Fill(event.msSegment_t0[ms])
                #    h["allsegzoom_prompt_data"].Fill(event.msSegment_t0[ms])

        #prompt MC 
        if tag == "ttbar_msseg":
            for im in xrange(len(event.muon_pt)):
                if preselect_muon(event, im, dod0=False) and abs(event.muon_IDtrack_d0[im]) < 0.1:
                        t_max, t_min, t_avg = fillMSSeg(event, im, h["all_prompt_mc"], h["allzoom_prompt_mc"])
                        h["mini_prompt_mc"].Fill(t_min)
                        h["max_prompt_mc"].Fill(t_max)
                        h["delta_prompt_mc"].Fill(abs( abs(t_max) - abs(t_min)))
                        h["avg_prompt_mc"].Fill(t_avg)
                        h["n_prompt_mc"].Fill(event.muon_nMSSegments[im])
            #for ms in xrange(len(event.msSegment_x)):
            #    h["allseg_prompt_mc"].Fill(event.msSegment_t0[ms])
            #    h["allsegzoom_prompt_mc"].Fill(event.msSegment_t0[ms])
        
        #signal MC
        if tag == "300_slep_msseg":
            for im in xrange(len(event.muon_pt)):
                if preselect_muon(event, im) and abs(event.muon_IDtrack_d0[im]) > 3:
                        t_max, t_min, t_avg = fillMSSeg(event, im, h["all_signal_mc"], h["allzoom_signal_mc"])
                        h["mini_signal_mc"].Fill(t_min)
                        h["max_signal_mc"].Fill(t_max)
                        h["delta_signal_mc"].Fill(abs( abs(t_max) - abs(t_min)))
                        h["avg_signal_mc"].Fill(t_avg)
                        h["n_signal_mc"].Fill(event.muon_nMSSegments[im])
                if preselect_muon(event,im,dod0=False):
                    offset = 0
                    if im != 0: offset = get_offset(event, im)
                    for iseg in xrange(0,event.muon_nMSSegments[im]):
                        ms = iseg + offset
                        h2["d0_t0_zoom"].Fill(event.muon_IDtrack_d0[im], event.muon_msSegment_t0[ms])
                        h2["d0_t0"].Fill(event.muon_IDtrack_d0[im], event.muon_msSegment_t0[ms])
            #for ms in xrange(len(event.msSegment_x)):
            #    h["allseg_signal_mc"].Fill(event.msSegment_t0[ms])
            #    h["allsegzoom_signal_mc"].Fill(event.msSegment_t0[ms])
        '''
        if tag == "data":
            for ms in xrange(len(event.msSegment_x)):
                if cosmic:
                    h["allseg_alldata_cosmic"].Fill(event.msSegment_t0[ms])
                    h["allsegzoom_alldata_cosmic"].Fill(event.msSegment_t0[ms])
                else:
                    h["allseg_alldata_noncosmic"].Fill(event.msSegment_t0[ms])
                    h["allsegzoom_alldata_noncosmic"].Fill(event.msSegment_t0[ms])
        '''
    failedFits[tag] = {failed, total, failed*1.0/total if total!=0 else 999}


if not os.path.exists("outputPlots/timing"): os.makedirs("outputPlots/timing")

outFile = ROOT.TFile("outputFiles/timing_" + append + "histos.root", "RECREATE")
c = ROOT.TCanvas()
for name in names:
    c.Clear()
    h[name].Draw()
    h[name].SetLineColor(names[name]["color"])
    c.SaveAs("outputPlots/timing/"+append+"_"+name+".pdf")
    h[name].Write()

ROOT.gPad.SetLogy(0)
for name in names_2d:
    c.Clear()
    h2[name].Draw("COLZ")
    c.SaveAs("outputPlots/timing/"+append+"_"+name+".pdf")
    h2[name].Write()

outFile.Write()


kinds = ["all_","n_","avg_","max_","mini_","delta_","allseg_", "allzoom_","allsegzoom_"]


n = ["prompt_data", "cosmic_data"]

for kind in kinds:

    c = ROOT.TCanvas(kind,kind)
#    ROOT.gPad.SetLogy()
    leg = ROOT.TLegend(0.60,0.65,0.80,0.92)
    for i, na in enumerate(n):
        name = kind +  na 
        print "drawing " + name + " integral " + str(h[name].Integral() )
        
        print h[name].GetTitle()
        if "prompt" in name: h[name].SetTitle("prompt")
        if "cosmic" in name: h[name].SetTitle("cosmic")
        print h[name].GetTitle()

        leg.AddEntry(h[name],h[name].GetTitle(),"l")
        
        if i == 0: 
        #    h[name].SetMaximum(1)
         #   h[name].SetMinimum(.0001)
            h[name].Draw("hist")
        else:
            h[name].Draw("hist same")
        
    leg.Draw("same")
    c.Update()
    ROOT.gPad.Update()
    c.SaveAs("outputPlots/timing/stack_t0_"+ append + "_" +kind+".pdf")


print failedFits
