import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

execfile("../cosmics/cosmic_helpers.py")				       
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")

h = {}
h2 = {}

tag = "data"

names = { 
    "dPhi": { "nb": 100, "low": 0, "high": 4 , "label": "dPhi", "log": 1},   
    "dR": { "nb": 100, "low": 0, "high": 4 , "label": "dR", "log": 1},   
    "sumEta": { "nb": 100, "low": 0, "high": 5, "label": "sumEta", "log": 1},   
    "topoetcone20": { "nb": 50, "low": 0, "high": .4, "label": "topoetcone20", "log": 1},   
    "ptvarcone20": { "nb": 20, "low": 0, "high": .4, "label": "ptvarcone20", "log": 1},   
}

names_2d ={
    "ps_iso"       : { "nb_x": 4, "low_x": 0, "high_x": 4, "label_x": "good muons", "nb_y": 4, "low_y": 0, "high_y": 4, "label_y": "unisolated muons", "log":0},
    "ps_cos"       : { "nb_x": 4, "low_x": 0, "high_x": 4, "label_x": "good muons", "nb_y": 4, "low_y": 0, "high_y": 4, "label_y": "cosmic muons", "log":0},
    "d0_d0"       : { "nb_x": 100, "low_x": -300, "high_x": 300, "label_x": "lead d0", "nb_y": 100, "low_y": -300, "high_y": 300, "label_y": "sublead d0", "log":0},
    "pt_pt"       : { "nb_x": 50, "low_x": 0, "high_x": 300, "label_x": "lead pt", "nb_y": 50, "low_y": 0, "high_y": 300, "label_y": "sublead pt", "log":0},
    "phi_eta_1"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "lead phi", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "lead eta", "log":0},
    "phi_eta_2"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "sublead phi", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "sublead eta", "log":0},
    "pt_d0_1"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "lead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "lead d0", "log":0},
    "pt_d0_posphi_1"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "lead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "lead d0", "log":0},
    "pt_d0_negphi_1"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "lead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "lead d0", "log":0},
    "pt_d0_2"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "sublead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "sublead d0", "log":0},
    "pt_d0_posphi_2"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "sublead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "sublead d0", "log":0},
    "pt_d0_negphi_2"       : { "nb_x": 100, "low_x": 0, "high_x": 300, "label_x": "sublead pt", "nb_y": 100, "low_y": 0, "high_y": 300, "label_y": "sublead d0", "log":0},
}

h = setHistos(tag, names)
h2 = setHistos2D(tag, names_2d)

basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data*.root'

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)


eventNum = 0
print t.GetEntries()
for event in t:
    if eventNum%100000 == 0 : print "processing ", eventNum
    eventNum+=1
    #if eventNum > 500000: break
            
    n_ps = 0
    n_noniso = 0
    n_cos = 0
    ps_i = []
    ni_i = []
    for im in xrange(len(event.muon_pt)):
        if preselect_muon(event, im, dod0=False,tight=True):
            n_ps += 1
            ps_i.append(im)
            if abs(event.muon_topoetcone20[im]/event.muon_pt[im]) >  0.15 or abs(event.muon_ptvarcone20[im]/event.muon_pt[im]) > 0.04:
                n_noniso += 1
                ni_i.append(im)
    

    if not (n_ps == 2 and n_noniso > 0 ): continue
    
    skip = False 
    for ip in ps_i:  
        cosmicTag_loose = cosTag(event, ip, .012 , .18) 
        matVeto = materialVeto(event, ip, innerMap, middleMap, outerMap)
        if cosmicTag_loose or matVeto : 
            n_cos += 1
           # skip = True
           # break
    #if skip: continue

    
    m1 = ps_i[0]
    m2 = ps_i[1]

    h2["ps_cos"].Fill(n_ps, n_cos)
    h2["ps_iso"].Fill(n_ps, n_noniso)
    
    
    h2["d0_d0"].Fill(event.muon_IDtrack_d0[m1], event.muon_IDtrack_d0[m2])
    h2["pt_pt"].Fill(event.muon_pt[m1], event.muon_pt[m2])
    
    h2["pt_d0_1"].Fill(event.muon_pt[m1],abs(event.muon_IDtrack_d0[m1]))
    h2["pt_d0_2"].Fill(event.muon_pt[m2],abs(event.muon_IDtrack_d0[m2]))
    
    if event.muon_phi[m1] > 0 : h2["pt_d0_posphi_1"].Fill(event.muon_pt[m1],abs(event.muon_IDtrack_d0[m1])) 
    else: h2["pt_d0_negphi_1"].Fill(event.muon_pt[m1],abs(event.muon_IDtrack_d0[m1])) 
   
    if event.muon_phi[m2] > 0 : h2["pt_d0_posphi_2"].Fill(event.muon_pt[m2],abs(event.muon_IDtrack_d0[m2]))
    else: h2["pt_d0_negphi_2"].Fill(event.muon_pt[m2],abs(event.muon_IDtrack_d0[m2]))
    
    h2["phi_eta_1"].Fill(event.muon_phi[m1],event.muon_eta[m1])
    h2["phi_eta_2"].Fill(event.muon_phi[m2],event.muon_eta[m2])

    #learn about unisolated muons
    for ii in ni_i:
        h["topoetcone20"].Fill(event.muon_topoetcone20[ii]/event.muon_pt[ii])
        h["ptvarcone20"].Fill(event.muon_ptvarcone20[ii]/event.muon_pt[ii])
   
   
    v1 = ROOT.TVector3()
    v2 = ROOT.TVector3()
    v1.SetPtEtaPhi(event.muon_pt[m1], event.muon_eta[m1], event.muon_phi[m1])
    v2.SetPtEtaPhi(event.muon_pt[m2], event.muon_eta[m2], event.muon_phi[m2])
    
    h["dPhi"].Fill(abs(v1.DeltaPhi(v2)))
    h["dR"].Fill(abs(v1.DeltaR(v2)))
    h["sumEta"].Fill(abs(event.muon_eta[m1] + event.muon_eta[m2]))
    


c = ROOT.TCanvas()

tag += "_2mu_iso"
if not os.path.exists("outputPlots/"+tag): os.makedirs("outputPlots/"+tag)


for histo in h:
    c.Clear()
    if names[histo]["log"] == 1:
        ROOT.gPad.SetLogy()
    else:
        ROOT.gPad.SetLogy(0)
    h[histo].Draw()
    c.Update()
    c.SaveAs("outputPlots/"+ tag + "/" +histo + "_1.pdf")

ROOT.gPad.SetLogy(0)
for histo in h2:
    c.Clear()
    if names_2d[histo]["log"] == 1:
        ROOT.gPad.SetLogz()
    else:
        ROOT.gPad.SetLogz(0)
    h2[histo].Draw("COLZ")
    c.Update()
    c.SaveAs("outputPlots/"+tag + "/" + histo + "_2.pdf")




