import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#feature = "baseline"
#append = "topbadqual"
append = "topsig"
feature = "v4"

names = { 
    "pt": { "nb": 50, "low": 0, "high": 1000, "label": "p_{T, lead}", "log": 1},   
    "d0": { "nb": 60, "low": -500, "high": 500, "label": "d_{0, lead}", "log": 1},   
    "z0": { "nb": 50, "low": -600, "high": 600, "label": "z_{0, lead}", "log": 1},   
    "eta": { "nb": 50, "low": -5, "high": 5, "label": "#eta_{lead}", "log": 1},   
    "phi": { "nb": 50, "low": -4, "high": 4, "label": "#phi_{lead}", "log": 1},   
    "qoverpsig": { "nb": 50, "low": 0, "high": 10, "label": "Q/P sig_{lead}", "log": 1},   
    "topoetcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "topoetcone20/pt", "log": 1},   
    "ptcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptcone20/pt", "log": 1},   
    "ptvarcone20": { "nb": 100, "low": -0.2, "high": 0.7, "label": "ptvarcone20/pt", "log": 1},   
    "topoetcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "topoetcone20/pt", "log": 1},   
    "ptcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptcone20/pt", "log": 1},   
    "ptvarcone20_zoom": { "nb": 100, "low": 0, "high": 0.2, "label": "ptvarcone20/pt", "log": 1},   
    "RFirstHit": { "nb": 100, "low": 0, "high": 600, "label": "R_{first hit}", "log": 1},   
    "author": { "nb": 10, "low": 0, "high": 10, "label": "author", "log": 1},   
    "nPresLays": { "nb": 11, "low": -.5, "high": 10.5, "label": "N_{Precision Layers}", "log": 0},   
    "nPresLays_good": { "nb": 10, "low": 0, "high": 10, "label": "N_{Good Precision Layers}", "log": 0},   
    "nSi": { "nb": 25, "low": 0, "high": 25, "label": "N_{Si Hits}", "log": 1},   
    "nPix": { "nb": 25, "low": 0, "high": 25, "label": "N_{Pixel Hits}", "log": 1},   
    "nSCT": { "nb": 25, "low": 0, "high": 25, "label": "N_{SCT Hits}", "log": 1},   
    "nTRT": { "nb": 50, "low": 0, "high": 60, "label": "N_{TRT Hits}", "log": 1},   
    "CB_chi2": { "nb": 50, "low": 0, "high": 10, "label": "#chi^{2}_{lead}", "log": 1},   
    "ID_chi2": { "nb": 50, "low": 0, "high": 10, "label": "#chi^{2}_{lead}", "log": 1},   
    "ID_holes": { "nb": 10, "low": 0, "high": 10, "label": "ID track holes", "log": 1},   
    "t_avg": { "nb": 50, "low": -50, "high": 50, "label": "t_{0, avg}", "log": 1},   
    "t_avg_zoomout": { "nb": 100, "low": -200, "high": 200, "label": "t_{0, avg}", "log": 1},   
    "t_med": { "nb": 50, "low": -30, "high": 30, "label": "t_{0, median}", "log": 1},   
    "t_med_zoomout": { "nb": 100, "low": -200, "high": 200, "label": "t_{0, median}", "log": 1},   
    "nMSSeg": { "nb": 10, "low": 0, "high": 10, "label": "N_{MS Segments}", "log": 1},   
    "nPhiLays": { "nb": 11, "low": -.5, "high": 10.5, "label": "N_{#phi Layers}", "log": 1},   
    "nPresHits": { "nb": 20, "low": 0, "high": 20, "label": "N_{Pres Hits}", "log": 1},   
    "nPresHoleLays": { "nb": 10, "low": 0, "high": 10, "label": "N_{Pres Hole Layers}", "log": 1},   
    "clusTime": { "nb": 100, "low": -100, "high": 100, "label": "clusTime", "log": 1},   
    
    "dPhi_int": { "nb": 50, "low": -0.1, "high": .1, "label": "#Delta #phi (muon, muon track)", "log": 1},   
    "dEta_int": { "nb": 50, "low": -0.1, "high": .1, "label": "#Delta #eta (muon, muon track)", "log": 1},   
    
    
    "nSeg_eta105": { "nb": 20, "low": 0, "high": 20, "label": "N seg in #eta < 1.05 with opposite phi", "log": 1},   
    "nSeg_sEtap5": { "nb": 20, "low": 0, "high": 20, "label": "N seg in #Sigma |#eta| < 0.5 with opposite phi", "log": 1},   
    "nSeg_sEtap1": { "nb": 20, "low": 0, "high": 20, "label": "N seg in #Sigma |#eta| < 0.1 with opposite phi", "log": 1},   
    "nSeg_sEtap05": { "nb": 20, "low": 0, "high": 20, "label": "N seg in #Sigma |#eta| < 0.5 with opposite phi", "log": 1},   
    "nSeg_noMS": { "nb": 20, "low": 0, "high": 20, "label": "N seg with no precision hits with opposite phi", "log": 1},   
    "nSeg_noMS_sEtap1": { "nb": 20, "low": 0, "high": 20, "label": "N seg with no precision hits with opposite phi and #Sigma |#eta| < 0.1", "log": 1},   
    "mindR_ms": { "nb": 40, "low": 0, "high": 5, "label": "min #Delta R(muon, segment)", "log": 1},   
    "mindPhi_ms": { "nb": 40, "low": 0, "high": 4, "label": "min #Delta #phi (muon, segment)", "log": 1},   
    "minsEta_ms": { "nb": 40, "low": 0, "high": 5, "label": "min #Sigma #eta (muon, segment)", "log": 1},   
    
    "nPresHits_bb": { "nb": 20, "low": 0, "high": 20, "label": "N_{Pres Hits}", "log": 1},   
    
    "nTracks_ptphi": { "nb": 20, "low": 0, "high": 10, "label": "N tracks w pt > 50 and opp sign phi", "log": 1},   
    "nTracks_d0z0": { "nb": 20, "low": 0, "high": 10, "label": "N tracks w baseline + d0/z0 10%", "log": 1},   
    "nTracks_phi": { "nb": 20, "low": 0, "high": 10, "label": "N tracks w baseline + d0/z0 and dPhi-#pi < 0.1", "log": 1},   
    "nTracks_eta": { "nb": 20, "low": 0, "high": 10, "label": "N tracks w baseline + d0/z0 and sEta < 0.1", "log": 1},   
    "mindR_id": { "nb": 40, "low": 0, "high": 5, "label": "min #Delta R(muon, track)", "log": 1},   
    "mindPhi_id": { "nb": 40, "low": 0, "high": 4, "label": "min #Delta #phi (muon, track)", "log": 1},   
    "minsEta_id": { "nb": 40, "low": 0, "high": 5, "label": "min #Sigma #eta (muon, track)", "log": 1},   
                

}
names_2d ={
    "nSi_RFirstHit"       : { "nb_y": 24, "low_y": 0, "high_y": 25, "label_y": "nSilicon hits", "nb_x": 100, "low_x": 0, "high_x": 600, "label_x": "R_{first hit}", "log":1},
    "ptvarcone20_d0"       : { "nb_x": 50, "low_x": -0.1, "high_x": 0.7, "label_x": "ptvarcone20/pt", "nb_y": 50, "low_y": -300, "high_y": 300, "label_y": "d_{0}", "log":1},
    "chi2_phi"       : { "nb_y": 150, "low_y": 0, "high_y": 60, "label_y": "#chi^{2}", "nb_x": 50, "low_x": -4, "high_x": 4, "label_x": "#phi", "log":0},
    "eta_phi_baseline"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_cosmictag"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},
    "eta_phi_cosmictag_mm"       : { "nb_x": 100, "low_x": -4, "high_x": 4, "label_x": "#phi_{#mu}", "nb_y": 100, "low_y": -5, "high_y": 5, "label_y": "#eta_{#mu}", "log":0},

    
}

h = {}
h2 = {}

data_events = []


#tags = ["300_slep","300_stau","100_slep","100_stau"]
tags = ["data","500_slep", "500_stau","300_slep","300_stau","100_slep","100_stau"]
#tags = ["data_alltracks","500_slep_alltracks", "300_slep_alltracks"]
#tags = ["data_msseg","500_slep_msseg", "300_slep_msseg"]
#tags = ["data_msseg","300_slep_msseg", "ttbar_msseg"]

colors = [0, ROOT.kBlue-5, ROOT.kBlue-7, ROOT.kCyan+1, ROOT.kCyan+3, ROOT.kBlue+1, ROOT.kBlue+3]
#tags = {"300_slep","300_stau","100_slep","100_stau","data"}
#tag = "300_slep"
#tag = "300_stau"
#tag = "100_slep"
#tag = "100_stau"
#tag = "500_slep"
#tag = "500_stau"

def find_segments(event):
    nsegs = {}
    for i in xrange(0,10):
        nsegs[i] = 0 
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[0], event.muon_eta[0], event.muon_phi[0])
    minsEta = 999
    mindPhi = 999
    mindR = 999

    for ms in xrange(len(event.msSegment_x)):
        seg = ROOT.TVector3(event.msSegment_x[ms], event.msSegment_y[ms], event.msSegment_z[ms]) 
        if seg.Phi() < 0:
            sEta_corr = abs (eta_corrected(event, v, ms, event.muon_IDtrack_z0[0]) + seg.Eta() ) 
            dPhi = abs((abs(v.DeltaPhi(seg)) - math.pi))
            
            dR = abs(math.sqrt( (dPhi - math.pi)**2 + sEta_corr**2))

            if sEta_corr < minsEta: minsEta = sEta_corr
            if dPhi < mindPhi: mindPhi = dPhi
            if dR < mindR: mindR = dR


            if abs(seg.Eta()) < 1.05: nsegs[1]+=1
            if sEta_corr < 0.5 and  abs(seg.Eta()) < 1.05 and dPhi < 0.3: nsegs[2]+=1
            if sEta_corr < 0.1 and  abs(seg.Eta()) < 1.05 and dPhi < 0.3: 
                nsegs[3]+=1
                nsegs[7] += event.msSegment_nPresHits[ms]
            if sEta_corr < 0.05 and  abs(seg.Eta()) < 1.05 and dPhi < 0.3: nsegs[4]+=1
            if event.msSegment_nPresHits[ms] == 0 : nsegs[5]+=1
            if event.msSegment_nPresHits[ms] == 0 and sEta_corr < 0.1: nsegs[6]+=1

    nsegs[8] = minsEta
    nsegs[9] = mindPhi 
    nsegs[10] = mindR
    return nsegs


def find_tracks(event):
    ntracks = {}
    for i in xrange(0,6):
        ntracks[i] = 0 
    
    v = ROOT.TVector3()
    v.SetPtEtaPhi(event.muon_pt[0], event.muon_eta[0], event.muon_phi[0])
    minsEta = 999
    mindPhi = 999
    mindR = 999
    for idt in xrange(len(event.idTrack_pt)):
        #high pt
        if event.idTrack_pt[idt] < 50: continue
        # opp side of detector
        if event.idTrack_phi[idt] > 0: continue
        ntracks[0] += 1
        
        trk = ROOT.TVector3()
        trk.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
         
        sEta = abs(event.idTrack_eta[idt] + event.muon_IDtrack_eta[0])
        dPhi = abs(abs(trk.DeltaPhi(v)) - math.pi)        
        dR = abs(math.sqrt( (dPhi - math.pi)**2 + sEta**2))

        if sEta < minsEta: minsEta = sEta
        if dPhi < mindPhi: mindPhi = dPhi
        if dR < mindR: mindR = dR



        #track vars w/in 10%?
        if abs( abs(event.idTrack_d0[idt]) - abs(event.muon_IDtrack_d0[0]) )/ abs(event.muon_IDtrack_d0[0]) < 0.1 and abs( abs(event.idTrack_z0[idt]) - abs(event.muon_IDtrack_z0[0]) )/ abs(event.muon_IDtrack_z0[0]) < 0.1: ntracks[1] += 1
         
        if abs( abs(event.idTrack_phi[idt]) - abs(event.muon_IDtrack_phi[0]))/event.muon_IDtrack_phi[0] < 0.1: ntracks[2] += 1 
        if abs(event.idTrack_eta[idt] + event.muon_IDtrack_eta[0]) < 0.1: ntracks[3] += 1 
   
   
    ntracks[4] = minsEta
    ntracks[5] = mindPhi 
    ntracks[6] = mindR
    return ntracks




maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

#dirname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/"
dirname= '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/'
vers = "v4_Feb27"
#vers = "v5.1"
for tag in tags:
    if tag == "300_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399039

    if tag == "300_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_300_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399014

    if tag == "100_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399030

    if tag == "100_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399007

    if tag == "500_slep":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399048

    if tag == "500_stau":
        basename = dirname + 'signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_*.DAOD_RPVLL_mc16*_'+vers+'_trees.root/*'
        dsid=399023

    if tag == "data":
        #basename= dirname + 'data/already_merged/merged/data1*.root'
        basename= dirname + 'data/skim/1mu/data*.root'


    #if not "data" in tag: AODFiles = glob.glob(basename)
    AODFiles = glob.glob(basename)
    print AODFiles
    t = ROOT.TChain("trees_SR_highd0_")
    for filename in AODFiles:
        t.Add(filename)


    h[tag] = {}
    h2[tag] = {}
    h[tag] = setHistos(tag, names)
    h2[tag] = setHistos2D(tag, names_2d)

    print tag
    print t.GetEntries()
    #nevents = t.GetEntries()


    lumi = 139000
    #lumi = 93000
   # lumi = 3.2 #2015
    weight = 1
    if "data" not in tag:     
        nevents = getNEvents(basename) 
        xs = getXS(dsid)
        xs_weight = xs*lumi / nevents
        print xs_weight

    eventNum = 0
    nPs = 0
    for event in t:
        if eventNum % 10000 == 0: print "Processing event ", eventNum
        eventNum+=1
        iLead = -1
        
        if "data" not in tag: 
            weight = xs_weight * event.mcEventWeight

         
        #if eventNum > 200: break
        
        try:
            event.muon_pt
        except:
            continue
        
        if not (len(event.muon_pt) == 1): continue
        if len(event.electron_pt) != 0: continue
        
         
        if not event.pass_HLT_mu60_0eta105_msonly: continue
        #if event.muon_n_cosmic > 0: continue      
        #if not (event.muon_isSRcuts[0] and event.muon_isIsolated[0] and event.muon_isGoodQual[0] and event.muon_isPassIDtrk[0]): continue
        #if not (event.muon_isSRcuts[0] ): continue
        #phi_layers = phi_lays(event,0) 
        #if not ( preselect_muon_basechi2(event, 0, doQual=False) and event.muon_phi[0] > 0 and (event.muon_MStrack_nPres[0] < 3 or phi_layers < 1) ) : continue 
        if not ( preselect_muon(event, 0) and event.muon_phi[0] > 0  and abs(event.muon_eta[0]) < 1.05) : continue 
        #if not event.muon_author[0] == 1: continue

        h2[tag]["eta_phi_baseline"].Fill(event.muon_phi[0], event.muon_eta[0]) 
        
        #cosmic = not event.muon_isNotCosmic[0]
        #if cosmic: continue 
        
        cosmicTag,ms = cosTag(event, 0, .018 , .25) 
        matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap) 
        cosmic = cosmicTag or matVeto
         
        
         
        #non-cosmic tagged information
        #if not cosmicTag :
        #   h2[tag]["eta_phi_cosmictag"].Fill(event.muon_phi[0], event.muon_eta[0]) 
        #if not matVeto: 
        if not cosmic: 
        #if True: 
            if tag == "data": data_events.append(event.eventNumber)
            h2[tag]["eta_phi_cosmictag_mm"].Fill(event.muon_phi[0], event.muon_eta[0]) 
                     
            v1 = ROOT.TVector3()
            v1.SetPtEtaPhi(event.muon_pt[0], event.muon_eta[0], event.muon_phi[0])
            
            nPhiLays = 0
            nPresHits = 0
            clus_avg = 0
            t0s = []
            t0_med = 0
            t0_avg = 0

            try:
                for ims in xrange(0,event.muon_nMSSegments[0]): #this works bc there's only one muon
                    nPhiLays += event.muon_msSegment_nPhiLays[ims]
                    nPresHits += event.muon_msSegment_nPresHits[ims] 
                    t0s.append(event.muon_msSegment_t0[ims])
                    clus_avg += event.muon_msSegment_clusTime[ims] if event.muon_msSegment_clusTime[ims] < 10**35 else 0
            except:
                print "what!!"
                continue
            
            clus_avg = clus_avg/event.muon_nMSSegments[0] if event.muon_nMSSegments[0] != 0 else -999
            #t0_med = np.median(np.array(t0s))   
            t0_avg = t_avg(event, 0)
            

            #if len(t0s) == 0: print t0_med
            #if abs(t0_med) > 100 : print t0s 
            #print "saving muon info!"
            h[tag]["pt"].Fill(event.muon_pt[0], weight)
            h[tag]["d0"].Fill(event.muon_IDtrack_d0[0], weight)
            h[tag]["z0"].Fill(event.muon_IDtrack_z0[0], weight)
            h[tag]["eta"].Fill(event.muon_eta[0], weight)
            h[tag]["phi"].Fill(event.muon_phi[0], weight)
            h[tag]["qoverpsig"].Fill(event.muon_QoverPsignif[0], weight)
            h[tag]["author"].Fill(event.muon_author[0], weight)
            h[tag]["RFirstHit"].Fill(event.muon_IDtrack_RFirstHit[0], weight)
            h[tag]["nSi"].Fill(event.muon_IDtrack_nSi[0], weight)
            h[tag]["nTRT"].Fill(event.muon_IDtrack_nTRT[0], weight)
            h[tag]["nPix"].Fill(event.muon_IDtrack_nPIX[0], weight)
            h[tag]["nSCT"].Fill(event.muon_IDtrack_nSCT[0], weight)
            h[tag]["nPresLays"].Fill(event.muon_MStrack_nPres[0], weight)
            h[tag]["nPresLays_good"].Fill(event.muon_MStrack_nPresGood[0], weight)
            h[tag]["nPresHoleLays"].Fill(event.muon_MStrack_nPresHole[0], weight)
            h[tag]["CB_chi2"].Fill(event.muon_CBtrack_chi2[0], weight)
            h[tag]["ID_chi2"].Fill(event.muon_IDtrack_chi2[0], weight)
            h[tag]["ID_holes"].Fill(event.muon_IDtrack_nMissingLayers[0], weight)
            h[tag]["t_avg"].Fill(t0_avg, weight)
            h[tag]["t_avg_zoomout"].Fill(t0_avg, weight)
            #h[tag]["t_med"].Fill(t0_med, weight)
            #h[tag]["t_med_zoomout"].Fill(t0_med, weight)
            h[tag]["nMSSeg"].Fill(event.muon_nMSSegments[0], weight)
            h[tag]["nPhiLays"].Fill(nPhiLays, weight)
            h[tag]["nPresHits"].Fill(nPresHits, weight)
            h[tag]["dPhi_int"].Fill(event.muon_phi[0] - event.muon_IDtrack_phi[0], weight)
            h[tag]["dEta_int"].Fill(event.muon_eta[0] - event.muon_IDtrack_eta[0], weight)
            h[tag]["topoetcone20"].Fill(event.muon_topoetcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptvarcone20"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptcone20"].Fill(event.muon_ptcone20[0]/event.muon_pt[0], weight)
            h[tag]["topoetcone20_zoom"].Fill(event.muon_topoetcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptvarcone20_zoom"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], weight)
            h[tag]["ptcone20_zoom"].Fill(event.muon_ptcone20[0]/event.muon_pt[0], weight)
            
            h[tag]["clusTime"].Fill(clus_avg, weight)
            
            
            h2[tag]["chi2_phi"].Fill(event.muon_CBtrack_chi2[0], event.muon_phi[0], weight)
            h2[tag]["nSi_RFirstHit"].Fill(event.muon_IDtrack_RFirstHit[0], event.muon_IDtrack_nSi[0], weight)
            h2[tag]["ptvarcone20_d0"].Fill(event.muon_ptvarcone20[0]/event.muon_pt[0], event.muon_IDtrack_d0[0], weight)
            
            segs = find_segments(event)
            trks = find_tracks(event)
             
            h[tag]["nSeg_eta105"].Fill(segs[1], weight)
            h[tag]["nSeg_sEtap5"].Fill(segs[2], weight)
            h[tag]["nSeg_sEtap1"].Fill(segs[3], weight)
            h[tag]["nSeg_sEtap05"].Fill(segs[4], weight)
            h[tag]["nSeg_noMS"].Fill(segs[5], weight)
            h[tag]["nSeg_noMS_sEtap1"].Fill(segs[6], weight)
            h[tag]["nPresHits_bb"].Fill(segs[7], weight)
            h[tag]["minsEta_ms"].Fill(segs[8], weight)
            h[tag]["mindPhi_ms"].Fill(segs[9], weight)
            h[tag]["mindR_ms"].Fill(segs[10], weight)
    
    
            h[tag]["nTracks_ptphi"].Fill(trks[0], weight)
            h[tag]["nTracks_d0z0"].Fill(trks[1], weight)
            h[tag]["nTracks_phi"].Fill(trks[2], weight)
            h[tag]["nTracks_eta"].Fill(trks[3], weight)
            h[tag]["minsEta_id"].Fill(trks[4], weight)
            h[tag]["mindPhi_id"].Fill(trks[5], weight)
            h[tag]["mindR_id"].Fill(trks[6], weight)
    
                
                 


c = ROOT.TCanvas()
'''

print h 
for tag in tags:

    if not os.path.exists("outputPlots/"+tag): os.makedirs("outputPlots/"+tag)

    outFile = ROOT.TFile("outputFiles/" +tag+ "_histos.root", "RECREATE")


    for i, histo in enumerate(h[tag]):
        h[tag][histo] = addOverflowBin(h[tag][histo])
        h[tag][histo] = addUnderflowBin(h[tag][histo])
        h[tag][histo].Write()
        if names[histo]["log"] == 1:
            ROOT.gPad.SetLogy()
        else:
            ROOT.gPad.SetLogy(0)
        h[tag][histo].Draw("hist")
        c.SaveAs("outputPlots/"+ tag + "/" + tag + "_" + feature + "_1_"+ histo + "_" + append + ".pdf")
        
    

    ROOT.gPad.SetLogy(0)
    for histo in h2[tag]:
        h2[tag][histo].Write()
        if names_2d[histo]["log"] == 1:
            ROOT.gPad.SetLogz()
        else:
            ROOT.gPad.SetLogz(0)
        if histo == "cosTag_nPs": h2[histo].Draw("COLZTEXT")
        else: h2[tag][histo].Draw("COLZ")

        c.SaveAs("outputPlots/"+tag + "/" + tag + "_" + feature + "_2_"+ histo + "_" + append + ".pdf")

    outFile.Write()
'''
if not os.path.exists("outputPlots/stack"): os.makedirs("outputPlots/stack")

print h 

ROOT.gPad.SetLogy()
for i, name in enumerate(names):
    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    for j, tag in enumerate(tags):
        h[tag][name] = addOverflowBin(h[tag][name])
        h[tag][name] = addUnderflowBin(h[tag][name])
        leg.AddEntry(h[tag][name],tag,"p")
        print "drawing " + name + " " + tag
        if "data" not in tag: h[tag][name].SetMarkerStyle(22)
        if j == 0: 
            h[tag][name].SetMaximum(10**9)
            h[tag][name].SetMinimum(10**-4)
            h[tag][name].GetYaxis().SetTitle("Number of muons")
            h[tag][name].Draw("pe")
        else:
            h[tag][name].SetMarkerColor(colors[j])
            h[tag][name].Draw("pesame")
    ROOT.ATLASLabel(0.20,0.88, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.20,0.83, "Run 2 data, 139 fb^{-1}" )
    leg.Draw("same")
    ROOT.gPad.Update()
    c.Update()
    c.SaveAs("outputPlots/stack/stack_" + feature + "_" + name + "_" + append + ".pdf")
    c.SaveAs("outputPlots/stack/stack_" + feature + "_" + name + "_" + append + ".C")

#
print data_events

0
