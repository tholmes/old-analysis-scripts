import ROOT
import math
import glob
import os
import sys


execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.gROOT.SetBatch(1)

tag = "data_segments"
#tag = "data"
append = "philayscut"
#append = tag


if tag == "data":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data*')
if tag == "data_segments":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/*')
if tag == "prompt":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/prompt_skim.root')
if tag == "mc_300":
    AODFiles = glob.glob('/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal_wip/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300*.DAOD_RPVLL_mc16*_v4_Feb27_trees.root/*')


t_cos = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t_cos.Add(filename)


maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")


names = { 
    "d0": { "nb": 50, "low": -300, "high": 300, "label": "d_{0}", "log": 1},   
    "pt": { "nb": 50, "low": 0, "high": 700, "label": "p_{T}", "log": 1},   
    "eta": { "nb": 20, "low": -2.5, "high": 2.5, "label": "#eta", "log": 1},   
    "phi": { "nb": 20, "low": -4, "high": 4, "label": "#phi", "log": 1},   
    "z0": { "nb": 50, "low": -500, "high": 500, "label": "z_{0}", "log": 1},   
    "t0": { "nb": 50, "low": -30, "high": 30, "label": "t_{0}", "log": 1},   
    "dpt_min": { "nb": 50, "low": -2, "high": 2, "label": "#Delta p_{T, min} (muon, track)", "log": 1},   
    "dd0_min": { "nb": 100, "low": 0, "high": 300, "label": "#Sigma d_{0, min} (muon, track)", "log": 1},   
    "dd0_min_zoom": { "nb": 60, "low": 0, "high": 10, "label": "#Sigma d_{0, min} (muon, track)", "log": 1},   
    "dz0_min": { "nb": 100, "low": 0, "high": 300, "label": "#Delta z_{0, min} (muon, track)", "log": 1},   
    "dz0_min_zoom": { "nb": 60, "low": 0, "high": 10, "label": "#Delta z_{0, min} (muon, track)", "log": 1},   
    "ddR_min": { "nb": 50, "low": 0, "high": 7, "label": "#Delta R_{min} (muon, track)", "log": 1},   
    "ddR_min_zoom": { "nb": 50, "low": 0, "high": .5, "label": "#Delta R_{min} (muon, track)", "log": 1},   
    "sEta_min_zoomout": { "nb": 100, "low": 0, "high": 5, "label": "#Sigma #eta_{min} (muon, track)", "log": 1},   
    "sEta_min": { "nb": 100, "low": 0, "high": .2, "label": "#Sigma #eta_{min} (muon, track)", "log": 1},   
    "sEta_min_dphi_lp02": { "nb": 100, "low": 0, "high": .2, "label": "#Sigma #eta_{min} (muon, track, #Delta #phi < 0.02)", "log": 1},   
    "dTheta_min": { "nb": 100, "low": 0, "high": 4, "label": "#Sigma #theta_{min} (muon, track)", "log": 1},   
    "dTheta_trk": { "nb": 100, "low": -.2, "high": .2, "label": "#Delta #theta (muon, muon ID track)", "log": 1},   
    "dEta_trk": { "nb": 100, "low": -.2, "high": .2, "label": "#Delta #eta (muon, muon ID track)", "log": 1},   
    "dPhi_trk": { "nb": 100, "low": -.2, "high": .2, "label": "#Delta #phi (muon, muon ID track)", "log": 1},   
    "dEta_mstrk": { "nb": 100, "low": -.2, "high": .2, "label": "#Delta #eta (muon, muon MS track)", "log": 1},   
    "dPhi_mstrk": { "nb": 100, "low": -.2, "high": .2, "label": "#Delta #phi (muon, muon MS track)", "log": 1},   
    "dPhi_min": { "nb": 100, "low": 0, "high": 4, "label": "#Delta #phi_{min} (muon, track)", "log": 1},   
    "dPhi_min_zoom": { "nb": 100, "low": 0, "high": .01, "label": "#Delta #phi_{min} (muon, track)", "log": 1},   
    "ntracks": { "nb": 20, "low": 0, "high": 20, "label": "N tracks (p_{T} > 10 GeV)", "log": 1},   
    "sEta_bb_IDseg_zoom": { "nb": 100, "low": 0, "high": .2, "label": "#Sigma #eta (bb track, bb seg)", "log": 1},   
    "sEta_bb_IDseg": { "nb": 100, "low": 0, "high": 5, "label": "#Sigma #eta (bb track, bb seg)", "log": 1},   
    "dPhi_bb_IDseg_zoom": { "nb": 100, "low": 0, "high": .2, "label": "#Delta #phi (bb track, bb seg)", "log": 1},   
    "dPhi_bb_IDseg": { "nb": 100, "low": 0, "high": 4, "label": "#Delta #phi (bb track, bb seg)", "log": 1},   
    "nPhiLays": { "nb": 10, "low": 0, "high": 10, "label": "# #phi layers", "log": 1},   
}
names_2d ={
    "sEta_dPhi" : { "nb_x": 50, "low_x": 0, "high_x": .4, "label_x": "#Sigma #eta (muon, track)", "nb_y": 50, "low_y": 0, "high_y": .4, "label_y": "#Delta #phi (muon, track)", "log":1},
    "sEta_dPhi_zoom" : { "nb_x": 50, "low_x": 0, "high_x": .2, "label_x": "#Sigma #eta (muon, track)", "nb_y": 50, "low_y": 0, "high_y": .02, "label_y": "#Delta #phi (muon, track)", "log":1},
    "sEta_dPhi_zoomout" : { "nb_x": 50, "low_x": 0, "high_x": 4, "label_x": "#Sigma #eta (muon, track)", "nb_y": 50, "low_y": 0, "high_y": 3.2, "label_y": "#Delta #phi (muon, track)", "log":1},
    "sTheta_dPhi" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #theta (muon, track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (muon, track)", "log":1},
    "theta_theta" : { "nb_x": 100, "low_x": -3.2, "high_x": 3.2, "label_x": "#theta (muon)", "nb_y": 100, "low_y": 0, "high_y": 3.2, "label_y": "#theta (muon track)", "log":1},
    "dTheta_chi2" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Delta #theta (muon, muon track)", "nb_y": 50, "low_y": 0, "high_y": 15, "label_y": "#chi^{2} (muon track)", "log":1},
    "dTheta_z0" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Delta #theta (muon, muon track)", "nb_y": 50, "low_y": -300, "high_y": 300, "label_y": "z_{0}", "log":1},
    "dTheta_d0" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Delta #theta (muon, muon track)", "nb_y": 50, "low_y": -300, "high_y": 300, "label_y": "d_{0}", "log":1}, 
    "min_delta_eta_z0" : { "nb_x": 50, "low_x": -.5, "high_x": .5, "label_x": "#Delta #eta (corrected - original)", "nb_y": 50, "low_y": -500, "high_y": 500, "label_y": "z_{0} (muon)", "log":1},
    "min_delta_sum_eta_z0" : { "nb_x": 50, "low_x": -.5, "high_x": .5, "label_x": "#Delta #Sigma #eta (corrected - original)", "nb_y": 50, "low_y": -500, "high_y": 500, "label_y": "z_{0} (muon)", "log":1},
    "sEta_dPhi_bb_IDseg" : { "nb_x": 50, "low_x": 0, "high_x": .03, "label_x": "#Sigma #eta (bb segment, bb track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (bb segment, bb track)", "log":1},
    "sEta_dPhi_bb_IDseg_zoom" : { "nb_x": 50, "low_x": 0, "high_x": 2, "label_x": "#Sigma #eta (bb segment, bb track)", "nb_y": 50, "low_y": 0, "high_y": .3, "label_y": "#Delta #phi (bb segment, bb track)", "log":1},
    "sEta_dPhi_bb_IDseg_zoomout" : { "nb_x": 50, "low_x": 0, "high_x": 4, "label_x": "#Sigma #eta (bb segment, bb track)", "nb_y": 50, "low_y": 0, "high_y": 3.2, "label_y": "#Delta #phi (bb segment, bb track)", "log":1}, 
    "res_eta_id_ms" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Delta #eta (muon, bb ID track)", "nb_y": 50, "low_y": 0, "high_y": .5, "label_y": "#Delta #eta (muon, bb MS segment)", "log":1},
    "res_eta_id_ms_zoomout" : { "nb_x": 50, "low_x": 0, "high_x": 4, "label_x": "#Delta #eta (muon, bb id track)", "nb_y": 50, "low_y": 0, "high_y": 4, "label_y": "#Delta #eta (muon, bb ms segment)", "log":1},
    "res_phi_id_ms" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Delta #phi (muon, bb ID track) - #pi", "nb_y": 50, "low_y": 0, "high_y": .5, "label_y": "#Delta #phi (muon, bb MS segment) - #pi", "log":1},
    "res_phi_id_ms_zoomin" : { "nb_x": 50, "low_x": 0, "high_x": 0.02, "label_x": "#Delta #phi (muon, bb id track) - #pi", "nb_y": 50, "low_y": 0, "high_y": .1, "label_y": "#Delta #phi (muon, bb ms segment) - #pi", "log":1},
    "res_phi_id_ms_zoomout" : { "nb_x": 50, "low_x": 0, "high_x": 4, "label_x": "#Delta #phi (muon, bb id track) - #pi", "nb_y": 50, "low_y": 0, "high_y": 4, "label_y": "#Delta #phi (muon, bb ms segment) - #pi", "log":1},
    "res_seta_id_ms" : { "nb_x": 50, "low_x": 0, "high_x": .5, "label_x": "#Sigma #eta (muon, bb ID track)", "nb_y": 50, "low_y": 0, "high_y": .5, "label_y": "#Sigma #eta (muon, bb MS segment)", "log":1},
    "res_seta_id_ms_zoomin" : { "nb_x": 50, "low_x": 0, "high_x": .02, "label_x": "#Sigma #eta (muon, bb ID track)", "nb_y": 50, "low_y": 0, "high_y": .02, "label_y": "#Sigma #eta (muon, bb MS segment)", "log":1},
    "res_seta_id_ms_zoomout" : { "nb_x": 50, "low_x": 0, "high_x": 4, "label_x": "#Sigma #eta (muon, bb id track)", "nb_y": 50, "low_y": 0, "high_y": 4, "label_y": "#Sigma #eta (muon, bb ms segment)", "log":1},
    "dEta_trk_tavg" : { "nb_x": 50, "low_x": -.2, "high_x": .2, "label_x": "#Sigma #eta (muon, id track)", "nb_y": 50, "low_y": -50, "high_y": 50, "label_y": "t_{0, avg}", "log":1},
    "dPhi_trk_tavg" : { "nb_x": 50, "low_x": -.2, "high_x": .2, "label_x": "#Delta #phi (muon, id track)", "nb_y": 50, "low_y": -50, "high_y": 50, "label_y": "t_{0, avg}", "log":1},

    
    
    
}

kinds = ["mv","cosnotmv","mu","all"]

h = {}
h2= {}

for k in kinds:
    label = tag + "_" + k  
    h[k] = setHistos(label, names)
    h2[k] = setHistos2D(label, names_2d)

nevents = 0
print t_cos.GetEntries()
for event in t_cos:
    if nevents % 5000 == 0: print "processing ", nevents
    nevents+=1

    #if nevents > 10000: break
    if "mc" in tag and len(event.muon_pt) == 0: continue
    
    if not "mc" in tag and len(event.lepton_pt) != 1: continue
    if not "mc" in tag and len(event.muon_pt) != 1: continue
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    
    #if not (event.muon_isBaseline[0] and event.muon_isGoodQual[0]): continue
    #cosmic = not event.muon_isNotCosmic[0]
    
     
    if "prompt" in tag and not (preselect_muon(event, 0, dod0=False) and abs(event.muon_IDtrack_d0[0]) < 0.1): continue 
    


    cosmic = False 
    mutype = "mu"
    
    if not preselect_muon(event, 0): continue 
    
    ctag, ms_cos = cosTag(event, 0, .018, .25)
    
    matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap)

    if ctag or matVeto: cosmic = True
    
    if ctag: mutype = "cosnotmv"
    elif matVeto: mutype = "mv"
    
    nPhiLays = 0
    for ims in xrange(0,event.muon_nMSSegments[0]): #this works bc there's only one muon
        nPhiLays += event.muon_msSegment_nPhiLays[ims]

    muon = ROOT.TVector3()
    muon.SetPtEtaPhi(event.muon_pt[0], event.muon_eta[0], event.muon_phi[0])
    muontrk = ROOT.TVector3()
    muontrk.SetPtEtaPhi(event.muon_IDtrack_pt[0], event.muon_IDtrack_eta[0], event.muon_IDtrack_phi[0])

    dR_min = 999 
    mintrk = ROOT.TVector3()
    itrk = -1
    
    if len(event.msSegment_x) == 0 or len(event.idTrack_pt) == 1: continue
        
    for idt in xrange(len(event.idTrack_pt)):
        #ignore muon track
        if event.idTrack_pt[idt] == event.muon_IDtrack_pt[0]: continue
        
        if event.idTrack_pt[idt] < 50 or event.idTrack_chi2[idt] > 2: continue
             
        trk = ROOT.TVector3() 
        trk.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
        
        dPhi = abs(abs(trk.DeltaPhi(muon)) - math.pi)
        sEta = abs(event.muon_IDtrack_eta[0] + event.idTrack_eta[idt])
        dR_cos = math.sqrt( (dPhi)**2 + (sEta)**2)

        
        if dR_cos < dR_min:
            dR_min = dR_cos
            itrk = idt
            mintrk = trk


    if itrk != -1:  
        dpt_min = (event.muon_IDtrack_pt[0] - event.idTrack_pt[itrk])/event.muon_IDtrack_pt[0]
        dd0_min = abs(event.muon_IDtrack_d0[0] + event.idTrack_d0[itrk])
        dz0_min = abs(event.muon_IDtrack_z0[0] - event.idTrack_z0[itrk])
        sEta_min = abs(event.muon_IDtrack_eta[0] + event.idTrack_eta[idt])
        dPhi_min = abs(abs(trk.DeltaPhi(muon)) - math.pi)
        dTheta_min =  abs(abs(muon.Theta() + trk.Theta())-math.pi)
        z0_min = event.idTrack_d0[itrk]
        
        dEta_id_mu = abs(event.muon_eta[0] - event.idTrack_eta[itrk])
        sEta_id_mu = abs(event.muon_eta[0] + event.idTrack_eta[itrk])
        dPhi_id_mu = abs( abs(muon.DeltaPhi(trk)) - math.pi)
    

    if ms_cos != -1 and itrk != -1 :
        segment = ROOT.TVector3(event.msSegment_x[ms_cos], event.msSegment_y[ms_cos], event.msSegment_z[ms_cos])
        
        eta_corr_id = eta_corrected(event, mintrk, ms_cos, z0_min)
        dPhi_seg = abs(mintrk.DeltaPhi(segment)) 
        sEta_seg = abs( segment.Eta() - eta_corr_id )  

        eta_corr_mu = eta_corrected(event, muon, ms_cos, event.muon_IDtrack_z0[0])
        dPhi_seg_mu = abs( abs(muon.DeltaPhi(segment)) - math.pi)
        dEta_seg_mu = abs(segment.Eta() - eta_corr_mu)
        sEta_seg_mu = abs(segment.Eta() + eta_corr_mu)
    
    avg_t = t_avg(event, 0) 
    
    for m in [mutype, "all"]: 
            
        h[m]["nPhiLays"].Fill(nPhiLays)
        if nPhiLays == 0: continue

        h[m]["d0"].Fill(event.muon_IDtrack_d0[0])
        h[m]["pt"].Fill(event.muon_pt[0])
        h[m]["eta"].Fill(event.muon_eta[0])
        h[m]["phi"].Fill(event.muon_phi[0])
        h[m]["z0"].Fill(event.muon_IDtrack_z0[0])
        h[m]["t0"].Fill(t_avg(event, 0))
        
        h[m]["ntracks"].Fill(len(event.idTrack_pt))  
        
        h[m]["dpt_min"].Fill(dpt_min)
        h[m]["dd0_min"].Fill(dd0_min)
        h[m]["dd0_min_zoom"].Fill(dd0_min)
        h[m]["dz0_min"].Fill(dz0_min)
        h[m]["dz0_min_zoom"].Fill(dz0_min)
        h[m]["sEta_min"].Fill(sEta_min)
        if dPhi_min < 0.02 : h[m]["sEta_min_dphi_lp02"].Fill(sEta_min)
        h[m]["sEta_min_zoomout"].Fill(sEta_min)
        h[m]["dTheta_min"].Fill(dTheta_min)
        h[m]["dPhi_min"].Fill(dPhi_min)
        h[m]["ddR_min"].Fill(dR_min)
        h[m]["ddR_min_zoom"].Fill(dR_min)
        
        h[m]["dTheta_trk"].Fill(muon.Theta() - muontrk.Theta())
        h[m]["dEta_trk"].Fill(event.muon_eta[0] - event.muon_IDtrack_eta[0])
        h[m]["dPhi_trk"].Fill(event.muon_phi[0] - event.muon_IDtrack_phi[0])
        h2[m]["dEta_trk_tavg"].Fill(event.muon_eta[0] - event.muon_IDtrack_eta[0], avg_t)
        h2[m]["dPhi_trk_tavg"].Fill(event.muon_phi[0] - event.muon_IDtrack_phi[0], avg_t)
        
        h[m]["dPhi_mstrk"].Fill(event.muon_phi[0] - event.muon_MStrack_phi[0])
        h[m]["dEta_mstrk"].Fill(event.muon_eta[0] - event.muon_MStrack_eta[0])
        
        h2[m]["sEta_dPhi"].Fill(sEta_min, dPhi_min)
        h2[m]["sEta_dPhi_zoom"].Fill(sEta_min, dPhi_min)
        h2[m]["sEta_dPhi_zoomout"].Fill(sEta_min, dPhi_min)
        h2[m]["sTheta_dPhi"].Fill(dTheta_min, dPhi_min)
        h2[m]["theta_theta"].Fill(muon.Theta(), muontrk.Theta())
        h2[m]["dTheta_chi2"].Fill( dTheta_min, event.muon_IDtrack_chi2[0])
        h2[m]["dTheta_z0"].Fill( dTheta_min, event.muon_IDtrack_z0[0])
        h2[m]["dTheta_d0"].Fill( dTheta_min, event.muon_IDtrack_d0[0])

        h[m]["dPhi_bb_IDseg"].Fill(dPhi_seg)
        h[m]["dPhi_bb_IDseg_zoom"].Fill(dPhi_seg)
        h[m]["sEta_bb_IDseg"].Fill(sEta_seg)
        h[m]["sEta_bb_IDseg_zoom"].Fill(sEta_seg)
        h2[m]["sEta_dPhi_bb_IDseg"].Fill(sEta_seg, dPhi_seg)
        h2[m]["sEta_dPhi_bb_IDseg_zoom"].Fill(sEta_seg, dPhi_seg)
        h2[m]["sEta_dPhi_bb_IDseg_zoomout"].Fill(sEta_seg, dPhi_seg)
        
        
        h2[m]["res_eta_id_ms"].Fill(dEta_id_mu, dEta_seg_mu)
        h2[m]["res_eta_id_ms_zoomout"].Fill(dEta_id_mu, dEta_seg_mu)
        
        h2[m]["res_seta_id_ms"].Fill(sEta_id_mu, sEta_seg_mu)
        h2[m]["res_seta_id_ms_zoomin"].Fill(sEta_id_mu, sEta_seg_mu)
        h2[m]["res_seta_id_ms_zoomout"].Fill(sEta_id_mu, sEta_seg_mu)
        
        h2[m]["res_phi_id_ms"].Fill(dPhi_id_mu, dPhi_seg_mu)
        h2[m]["res_phi_id_ms_zoomin"].Fill(dPhi_id_mu, dPhi_seg_mu)
        h2[m]["res_phi_id_ms_zoomout"].Fill(dPhi_id_mu, dPhi_seg_mu)
        

 
c = ROOT.TCanvas()
outFile = ROOT.TFile("outputFiles/1mu"+append+".root", "RECREATE")

for k in kinds:
    for histo in h[k]:
        h[k][histo] = addOverflow(h[k][histo])
        if names[histo]["log"] == 1:
            ROOT.gPad.SetLogy()
        else:
            ROOT.gPad.SetLogy(0)
        h[k][histo].Draw("hist")
        h[k][histo].Write()
        c.SaveAs("outputPlots/1mu/"+ append + "_" + k + "_" + histo + ".pdf")

    ROOT.gStyle.SetPalette(ROOT.kBird)
    ROOT.gPad.SetLogy(0)
    for histo in h2[k]:
        if names_2d[histo]["log"] == 1:
            ROOT.gPad.SetLogz()
        else:
            ROOT.gPad.SetLogz(0)
        #h2[k][histo] = addOverflow2D(h2[k][histo])
        h2[k][histo].Draw("COLZ")
        c.SaveAs("outputPlots/1mu/"+ append + "_"+ k + "_"+ histo + ".pdf")
        h2[k][histo].Write()

outFile.Write()


#ROOT.gPad.SetLogy()    
toplot = ["d0", "dpt_min", "dd0_min", "dz0_min", "ddR_min", "sEta_min", "dPhi_min", "ntracks"]
for p in toplot:
    c.Clear()

    h_max = max(h["mu"][p].GetMaximum(), h["cos"][p].GetMaximum())
    
    h["mu"][p].SetLineColor(ROOT.kRed)
    h["mu"][p].GetYaxis().SetRangeUser(0,h_max+5)
    h["mu"][p].Draw("hist")
    h["cos"][p].Draw("hist same")
    
    leg = ROOT.TLegend(0.65,0.8,0.8,0.9)
    leg.AddEntry(h["mu"][p],"muon","l")
    leg.AddEntry(h["cos"][p],"cosmic","l")
    leg.Draw("same") 
    c.SaveAs("outputPlots/1mu/" + append + "_comp_"+ "_" +p+".pdf")
    


