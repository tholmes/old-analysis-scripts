import ROOT
import math
import glob
import os
import sys

ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

execfile("../cosmics/cosmic_helpers.py")				       
execfile("/afs/cern.ch/user/t/tholmes/LLP/scripts/plot_helpers/basic_plotting.py")

h = {}
h["cutflow_1"] = ROOT.TH1F("cutflow","cutflow",16,0,16)
h["cutflow_2"] = ROOT.TH1F("cutflow_2","cutflow_2",16,0,16)


basename='/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data*.root'

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

eventNum = 0
print t.GetEntries()
for event in t:
    if eventNum%100000 == 0: print "processing ", eventNum
    eventNum+=1
    #if eventNum > 100000: break
    
    h["cutflow_1"].Fill("all",1)
    h["cutflow_2"].Fill("all",1)

    if not event.pass_HLT_mu60_0eta105_msonly: continue
    h["cutflow_1"].Fill("trigger",1)
    h["cutflow_2"].Fill("trigger",1)
    
    if not (len(event.lepton_pt) == 1 and len(event.muon_pt) == 1) : continue  
    h["cutflow_1"].Fill("1 muon",1)
    h["cutflow_2"].Fill("1 muon",1)
    
    try:
        event.muon_pt[0]
    except:
        print "n leptons", event.lepton_n
        print "n muons", event.muon_n
        print "n electrons", event.muon_n
        print "len lepton pt", len(event.lepton_pt)
        print "len muon pt", len(event.muon_pt)

    if event.muon_pt[0] < 65: continue
    h["cutflow_1"].Fill("pt > 65", 1)
    h["cutflow_2"].Fill("pt > 65", 1)
    
    if abs(event.muon_eta[0]) > 2.5: continue
    h["cutflow_1"].Fill("eta < 2.5",1)
    h["cutflow_2"].Fill("eta < 2.5",1)

    if abs(event.muon_IDtrack_d0[0]) < 3: continue
    h["cutflow_1"].Fill("d0 > 3", 1)
    h["cutflow_2"].Fill("d0 > 3", 1)

    if abs(event.muon_IDtrack_z0[0] ) > 500: continue
    h["cutflow_1"].Fill("z0 < 500",1)
    h["cutflow_2"].Fill("z0 < 500",1)

    if abs(event.muon_IDtrack_d0[0] ) > 300: continue
    h["cutflow_1"].Fill("d0 < 300",1)
    h["cutflow_2"].Fill("d0 < 300",1)
    
    if abs(event.muon_CBtrack_chi2[0]) > 3: continue
    h["cutflow_1"].Fill("chi2 < 3",1)
    h["cutflow_2"].Fill("chi2 < 3",1)
    
    if event.muon_MStrack_nPres[0] < 3: continue
    h["cutflow_1"].Fill("nPrec >= 3",1)
    h["cutflow_2"].Fill("nPrec >= 3",1)
    
    cosmicTag_tight = cosTag(event, 0, .008 , .15) 
    cosmicTag_loose = cosTag(event, 0, .012 , .18) 
    matVeto = materialVeto(event, 0, innerMap, middleMap, outerMap)
   
    # isolation, then cosmic 
    if abs(event.muon_topoetcone20[0]/event.muon_pt[0]) < 0.15:
        h["cutflow_1"].Fill("topoetcone < .15",1)
    
        if abs(event.muon_ptvarcone20[0]/event.muon_pt[0]) < 0.04:
            h["cutflow_1"].Fill("ptvarcone20 < .04",1) 

            if not cosmicTag_tight:
                h["cutflow_1"].Fill("tight cosmic",1)
                if not cosmicTag_loose:
                    h["cutflow_1"].Fill("loose cosmic",1)
                    if not matVeto:
                        h["cutflow_1"].Fill("cosmic + mv",1)

    
    #cosmic then isolation
    if not cosmicTag_tight:
        h["cutflow_2"].Fill("tight cosmic",1)
        if not cosmicTag_loose:
            h["cutflow_2"].Fill("loose cosmic",1)
            if not matVeto:
                h["cutflow_2"].Fill("cosmic + mv",1)
                if abs(event.muon_topoetcone20[0]/event.muon_pt[0]) < 0.15:
                    h["cutflow_2"].Fill("topoetcone < .15",1) 
                    if abs(event.muon_ptvarcone20[0]/event.muon_pt[0]) < 0.04:
                        h["cutflow_2"].Fill("ptvarcone20 < .04",1) 


c = ROOT.TCanvas()
c.SetBottomMargin(.30)
ROOT.gPad.SetLogy()
for hist in h:
    h[hist].Draw()
    h[hist].LabelsOption('v')
    h[hist].LabelsDeflate("X");
    h[hist].LabelsDeflate("Y")
    c.Update()
    c.SaveAs("outputPlots/"+hist+".pdf")
    
