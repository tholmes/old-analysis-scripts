import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(0)

c = ROOT.TCanvas()
ROOT.gPad.SetLogy()

#flag = "mc_300"
#data_name = "mc_300"
data_name = "data_segments"
flag = "wider_tag"

f = ROOT.TFile("outputFiles/1mu"+flag+".root")

doRes = False
doRatio = True
doProjection = False

if doRes:
#compare cosmics to muons
    for v in ["dPhi_trk","dEta_trk","dPhi_mstrk","dEta_mstrk"]:
        #h_mu = f.Get("data_segments_mu_"+v)
        #h_cos = f.Get("data_segments_cosnotmv_"+v)
        h_mu = f.Get(data_name + "_mu_"+v)
        h_cos = f.Get(data_name+ "_cosnotmv_"+v)

        h_mu.Scale(1/h_mu.Integral())
        h_cos.Scale(1/h_cos.Integral())

        h_mu.SetLineColor(ROOT.kRed)
        h_mu.SetMaximum(0.7)

        leg = ROOT.TLegend(0.60,0.74,0.80,0.92)
        leg.AddEntry(h_mu, "muon", "l")
        leg.AddEntry(h_cos, "cosmic", "l")


        h_mu.Draw("hist")
        h_cos.Draw("hist same")
        leg.Draw("same")
        
        text = ROOT.TLatex()
        text.SetNDC()
        text.DrawLatex(0.60,0.7, "MS track" if "ms" in v else "ID track" )

        c.Modified()
        c.SaveAs("outputPlots/1mu/"+flag+"_comp_"+v+".pdf")


#compare ID to MS
    for m in ["mu","cosnotmv"]:
        for p in ["Eta","Phi"]:
            
            #h_id = f.Get("data_segments_"+m+"_d"+p+"_trk")
            #h_ms = f.Get("data_segments_"+m+"_d"+p+"_mstrk")
            h_id = f.Get(data_name+"_"+m+"_d"+p+"_trk")
            h_ms = f.Get(data_name+"_"+m+"_d"+p+"_mstrk")

            h_id.Scale(1/h_id.Integral())
            h_ms.Scale(1/h_ms.Integral())

            h_id.SetLineColor(ROOT.kBlue)
            h_ms.SetLineColor(ROOT.kBlack)

            leg = ROOT.TLegend(0.60,0.74,0.80,0.92)
            leg.AddEntry(h_id, "ID track", "l")
            leg.AddEntry(h_ms, "MS track", "l")


            h_id.Draw("hist")
            h_ms.Draw("hist same")
            leg.Draw("same")
            
            text = ROOT.TLatex()
            text.SetNDC()
            text.DrawLatex(0.60,0.7, "untagged muons" if m == "mu" else "cosmic muons" )

            c.Modified()
            c.SaveAs("outputPlots/1mu/"+flag+"_comp_trks_"+m+"_"+p+".pdf")


if doRatio:

    for v in ['d0','pt','eta','phi','z0','t0']:
        
        #h_cos = f.Get("data_segments_cos_"+v)
        #h_mu = f.Get("data_segments_mu_"+v)
        h_cos = f.Get(data_name+"_cos_"+v)
        #h_mu = f.Get(data_name+"_mu_"+v)
        h_tot = f.Get(data_name+"_all_"+v)
        h_tot.SetTitle("total")        
     
        h_cos.SetTitle("cosmic")
        #h_mu.SetTitle("muon")

        #h_tot = h_cos.Clone("total")
        #h_tot.Add(h_mu)
        #h_tot.GetXaxis().SetTitle("")
        #h_tot.SetTitle("total")
        
        '''       
        hists = []
        hists.append(h_tot)
        hists.append(h_cos)
        hists.append(h_mu)
        
        h_ratio = h_cos.Clone("ratio")
        h_ratio.Divide(h_tot)
        h_ratio.GetXaxis().SetTitle(v)
        h_ratio.GetYaxis().SetTitle("cosmic/total")
        '''
        
         
        #drawTwoPads(hists, h_ratio, "outputPlots/1mu/wider_tag_test_lin_"+v+".pdf") 
        #drawTwoPads(hists, h_ratio, "outputPlots/1mu/wider_tag_test_log_"+v+".pdf", True, False) 

        makeRatioPlot(h_cos, h_tot, v, "outputPlots/1mu/"+flag+"_ratio_"+v+".pdf", doEfficiency=True)

ROOT.gPad.SetLogy()

if doProjection:
    
    h2d = f.Get("data_segments_all_res_seta_id_ms_zoomin")
    
    nBins = 50
    nProj = 5
    binSize = nBins/nProj
    
        
    hists = []
    hists_norm = []
    
    
    c.Clear()
    colors = [ROOT.kBlack, ROOT.kBlue-5, ROOT.kBlue-7, ROOT.kCyan+1, ROOT.kCyan+3, ROOT.kBlue+1, ROOT.kBlue+3]
    
    boundaries = [1,2,5,10,15,30,50]
     
    h2d.RebinX(2)    
    for i in xrange(len(boundaries)-1):
        startBin = boundaries[i]
        endBin = boundaries[i+1]
        

        tmp_hist = h2d.ProjectionX("proj_"+str(i), startBin, endBin)
        tmp_hist.SetMarkerColor(colors[i])
        tmp_hist.SetLineColor(colors[i])
        tmp_hist.SetMarkerSize(0)        
        tmp_hist.SetMarkerSize(0)        
        tmp_hist.GetXaxis().SetNdivisions(508)
        
        tmp_hist.SetTitle("#Sigma #eta(#mu,MS) %4.4f - %4.4f"%(h2d.GetYaxis().GetBinLowEdge(startBin), h2d.GetYaxis().GetBinLowEdge(endBin+1)))
        
        hists.append(tmp_hist)
        
        tmp_norm = tmp_hist.Clone(tmp_hist.GetName()+"_norm")
        tmp_norm.Scale(1/tmp_norm.Integral())
        hists_norm.append(tmp_norm)
    
    ymax = 2*getMax(hists) 
    ymax_norm  = 2*getMax(hists_norm) 

    leg = ROOT.TLegend(0.50,0.64,0.80,0.92)
    first = True
    for h in hists: 
        if first:
            h.SetMaximum(ymax)
            h.GetXaxis().SetTitle("#Sigma #eta(#mu,ID)")
            #h.Draw("p e")
            h.Draw("hist e")
            first = False
        else: 
            #h.Draw("p e same")
            h.Draw("hist e same")
        leg.AddEntry(h,h.GetTitle(),"p")
    
    leg.Draw("same")
    c.Update()
    c.Modified()
    c.SaveAs("outputPlots/1mu/sEta_proj.pdf")
    
    leg = ROOT.TLegend(0.50,0.64,0.80,0.92)
    first = True
    for h in hists_norm: 
        if first:
            h.SetMaximum(ymax_norm)
            h.GetXaxis().SetTitle("#Sigma #eta(#mu,ID)")
            #h.Draw("p e")
            h.Draw("hist e")
            first = False
        else: 
            #h.Draw("p e same")
            h.Draw("hist e same")
        leg.AddEntry(h,h.GetTitle(),"l")
    
    leg.Draw("same")
    c.Update()
    c.Modified()
    c.SaveAs("outputPlots/1mu/sEta_proj_norm.pdf")
