import glob
import ROOT
import os

execfile("../../scripts/plot_helpers/basic_plotting.py")
execfile("../../scripts/plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
ROOT.ROOT.EnableImplicitMT()


h = {}
h["original"] = {}
h["postexec"] = {}
h["morleyMR"] = {}
h["morleyMR_noCM"] = {}
'''
h["original"]["true_d0"] = ROOT.TH1F("orig_true_d0","orig_true_d0",15,0,30)
h["original"]["reco_d0"] = ROOT.TH1F("orig_reco_d0","orig_reco_d0",15,0,30)
h["original"]["eff_d0"] = ROOT.TH1F("orig_eff_d0","orige_eff_d0",15,0,30)
h["postexec"]["true_d0"] = ROOT.TH1F("postexec_true_d0","postexec_true_d0",15,0,30)
h["postexec"]["reco_d0"] = ROOT.TH1F("postexec_reco_d0","postexec_reco_d0",15,0,30)
h["postexec"]["eff_d0"] = ROOT.TH1F("postexec_eff_d0","postexece_eff_d0",15,0,30)
h["morleyMR"]["true_d0"] = ROOT.TH1F("morleyMR_true_d0","morleyMR_true_d0",15,0,30)
h["morleyMR"]["reco_d0"] = ROOT.TH1F("morleyMR_reco_d0","morleyMR_reco_d0",15,0,30)
h["morleyMR"]["eff_d0"] = ROOT.TH1F("morleyMR_eff_d0","morleyMRe_eff_d0",15,0,30)
h["morleyMR_noCM"]["true_d0"] = ROOT.TH1F("morleyMR_noCM_true_d0","morleyMR_noCM_true_d0",15,0,30)
h["morleyMR_noCM"]["reco_d0"] = ROOT.TH1F("morleyMR_noCM_reco_d0","morleyMR_noCM_reco_d0",15,0,30)
h["morleyMR_noCM"]["eff_d0"] = ROOT.TH1F("morleyMR_noCM_eff_d0","morleyMR_noCMe_eff_d0",15,0,30)
'''
h["original"]["true_d0"] = ROOT.TH1F("orig_true_d0","orig_true_d0",30,0,300)
h["original"]["reco_d0"] = ROOT.TH1F("orig_reco_d0","orig_reco_d0",30,0,300)
h["original"]["eff_d0"] = ROOT.TH1F("orig_eff_d0","orige_eff_d0",30,0,300)
h["postexec"]["true_d0"] = ROOT.TH1F("postexec_true_d0","postexec_true_d0",30,0,300)
h["postexec"]["reco_d0"] = ROOT.TH1F("postexec_reco_d0","postexec_reco_d0",30,0,300)
h["postexec"]["eff_d0"] = ROOT.TH1F("postexec_eff_d0","postexece_eff_d0",30,0,300)
h["morleyMR"]["true_d0"] = ROOT.TH1F("morleyMR_true_d0","morleyMR_true_d0",30,0,300)
h["morleyMR"]["reco_d0"] = ROOT.TH1F("morleyMR_reco_d0","morleyMR_reco_d0",30,0,300)
h["morleyMR"]["eff_d0"] = ROOT.TH1F("morleyMR_eff_d0","morleyMRe_eff_d0",30,0,300)
h["morleyMR_noCM"]["true_d0"] = ROOT.TH1F("morleyMR_noCM_true_d0","morleyMR_noCM_true_d0",30,0,300)
h["morleyMR_noCM"]["reco_d0"] = ROOT.TH1F("morleyMR_noCM_reco_d0","morleyMR_noCM_reco_d0",30,0,300)
h["morleyMR_noCM"]["eff_d0"] = ROOT.TH1F("morleyMR_noCM_eff_d0","morleyMR_noCMe_eff_d0",30,0,300)

colors = {}
colors["original"] = ROOT.kBlack
colors["postexec"] = ROOT.kCyan+1
colors["morleyMR"] = ROOT.kBlue
colors["morleyMR_noCM"] = ROOT.kViolet

f ={}
f["original"] = ROOT.TFile("/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_nochanges_notracklet_condor.ntup.pool.root")
f["postexec"] = ROOT.TFile("/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_nochanges_preexec_condor.ntup.pool.root")
f["morleyMR"] = ROOT.TFile("/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_AMMR.ntup.pool.root")
f["morleyMR_noCM"] = ROOT.TFile("/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_AMMR_noCM.ntup.pool.root")



t ={}
for names in f:
    t[names] = f[names].Get("trees_SR_highd0_")

for tname in t:
    print "processing", tname
    for event in t[tname]:
        
        true_indices = []
        # truth indices
        for it in xrange(len(event.truthLepton_pt)):
            if abs(event.truthLepton_pdgId[it]) == 11 and event.truthLepton_pt[it] > 65:
                h[tname]["true_d0"].Fill(abs(event.truthLepton_d0[it]))
                true_indices.append(it)
        
        for ie in xrange(len(event.electron_pt)):
            #if event.electron_pt[ie] > 65 and event.electron_dpt[ie] > -0.5 and event.electron_chi2[ie] < 2 and event.electron_nMissingLayers[ie] < 2:
            if event.electron_pt[ie] > 65 :
                for ti in true_indices:
                    if event.electron_truthMatchedBarcode[ie] == event.truthLepton_barcode[ti]:
                        h[tname]["reco_d0"].Fill(abs(event.truthLepton_d0[ti]))


c = ROOT.TCanvas()
first = True
for tname in h:
    print "drawing ", tname
    h[tname]["eff_d0"].Divide(h[tname]["reco_d0"], h[tname]["true_d0"], 1, 1, "B")
    h[tname]["eff_d0"].SetTitle(tname)
    h[tname]["eff_d0"].SetLineColor(colors[tname])
    h[tname]["eff_d0"].SetMarkerColor(colors[tname])
    h[tname]["eff_d0"].SetMaximum(1.2)
    h[tname]["eff_d0"].SetMinimum(0)
    if first:
        h[tname]["eff_d0"].Draw("hist")
        first = False
    else:
        h[tname]["eff_d0"].Draw("hist same")

leg = c.BuildLegend(.6,.6,.8,.8)
leg.Draw()

c.Update()
raw_input("...")
c.SaveAs("outputPlots/versions.pdf")


