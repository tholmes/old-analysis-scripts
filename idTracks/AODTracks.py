#asetup AthAnalysisBase,2.4.38

import ROOT
import os
import argparse
from ROOT.POOL import TEvent
evt = TEvent(TEvent.kAthenaAccess)


#filename = "/eos/user/l/lhoryn/inputFiles/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.merge.DAOD_RPVLL.e6633_e5984_s3307_r10832_r10706/DAOD_RPVLL.15429450._000001.pool.root.1" 

filename = "/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_nochanges_notracklet_1kevt.AOD.pool.root"

AODFile = ROOT.TFile.Open(filename)
h={}
h["true_d0"] = ROOT.TH1F("true_d0", "true_d0", 25 , 0, 200)
h["id_d0"] = ROOT.TH1F("id_d0", "id_d0", 25 , 0, 200)
h["eff_id_d0"] = ROOT.TH1F("eff_d0", "eff_d0", 25 , 0, 200)
evt.readFrom(AODFile)
print "Number of Events " + str(evt.getEntries())
for entry in xrange(evt.getEntries()):
    evt.getEntry(entry)
    evtInfo = evt.retrieve("xAOD::EventInfo","EventInfo")
    eventNumber = evtInfo.eventNumber()
    
    if eventNumber%1000 == 0: print "Processing event " + str(eventNumber)

    truthParticles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")
    trackParticles = evt.retrieve("xAOD::TrackParticleContainer","InDetTrackParticles")
    electrons  = evt.retrieve("xAOD::ElectronContainer","Electrons")
    
    for ie, electron in enumerate(electrons):
        print "this works! pt ", electron.pt()
    
    for iT, truthParticle in enumerate(truthParticles):
        #find slepton
        if abs(truthParticle.pdgId()) > 1**6 and truthParticle.hasDecayVtx() and truthParticle.barcode() < 200000 and truthParticle.status() == 1:
            for x in xrange(0,truthParticle.nChildren()):
                childParticle = truthParticle.child(x)
                if not childParticle: continue
                if abs(childParticle.pdgId()) == 11:

                    #calculate truth d0
                    sleptonPos = ROOT.TVector3(truthParticle.decayVtx().x(), truthParticle.decayVtx().y(), truthParticle.decayVtx().z()) 
                    lepton = ROOT.TVector3()
                    lepton.SetMagThetaPhi(1.0,childParticle.p4().Theta(),childParticle.phi());
                    d0 = sleptonPos.Perp() * ROOT.TMath.Sin(abs(lepton.DeltaPhi(sleptonPos)))
                    h["true_d0"].Fill(d0)
                    
                    for itk, trackParticle in enumerate(trackParticles):
                        if trackParticle.pt()/1000 < 40: continue
                        diff_d0 = abs(abs(d0) - abs(trackParticle.d0()))
                        delta_d0 = abs(abs(d0) - abs(trackParticle.d0()))/abs(d0)
                        if delta_d0 < 0.05 or diff_d0 < 0.1 :
                            h["id_d0"].Fill(d0)
                            break
        
        



                    
c=ROOT.TCanvas()
for hist in h:
    h[hist].Draw()
    raw_input("...")
    c.SaveAs(hist+".pdf")


h["eff_id_d0"].Divide(h["id_d0"], h["true_d0"], 1, 1, "B")
h["eff_id_d0"].SetTitle("ID Track")
h["eff_id_d0"].SetMaximum(1.5)
h["eff_id_d0"].SetMinimum(0)
h["eff_id_d0"].Draw("")
raw_input("...")
c.SaveAs("eff_id_d0.pdf")

# for dirName in os.listdir(inDir):
# 	names = str(dirName).split(".")[2].split("_")
# 	outName = names[-2] + "_" + names[-1]
# 	print outName
# 	h = ROOT.TH1F(outName, outName, 500, 0, 2000)
# 	for filename in os.listdir(inDir+"/"+dirName):
# 		AODFile = ROOT.TFile.Open(inDir+"/"+dirName+"/"+filename)
# 		evt.readFrom(AODFile)
# 	
# 		print "Number of Events " + str(evt.getEntries())
# 			
# 		for entry in xrange(evt.getEntries()):
# 			evt.getEntry(entry)
# 			evtInfo = evt.retrieve("xAOD::EventInfo","EventInfo")
# 			eventNumber = evtInfo.eventNumber()
# 			if eventNumber%1000 == 0:
# 				print "Processing event " + str(eventNumber)
# 
# 			truthParticles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")
# 			for iT, truthParticle in enumerate(truthParticles):
# 				if abs(truthParticle.pdgId()) == 15 and truthParticle.barcode() < 200000:
# 					h.Fill(truthParticle.pt()/1000)
# 		AODFile.Close()
# 
# 	outfile = ROOT.TFile(outName+".root", "RECREATE")
# 	h.Write()
# 	outfile.Write()
