
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
ROOT.ROOT.EnableImplicitMT()

#fname = "/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_MRChanges_v2_1kevt_condor_idt_2.ntup.pool.root"
#fname = "/eos/user/l/lhoryn/electron_fix/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101_MRChanges_v2_1kevt.ntup.pool.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.1_Nov2019_AllTracks/*/*.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/photon.root"
#fname = "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir/data-trees/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101nochanges_1kevt.AOD.pool.root.root"

SRs = ["ee"]

hists = {}
for sr in SRs:
    hists[sr] = {}
    hists[sr]["eff_gsf_d0"] = ROOT.TH1F(sr+"_eff_gsf_d0", "GSF Track", 25, 0., 200.)
    hists[sr]["eff_id_d0"] = ROOT.TH1F(sr+"_eff_id_d0", "ID Track", 25, 0., 200.)
    hists[sr]["true_d0"] = ROOT.TH1F(sr+"_true_d0", sr+"_true_d0", 25, 0., 200.)
    hists[sr]["gsf_d0"] = ROOT.TH1F(sr+"_gsf_d0_matched", sr+"_gsf_d0_matched", 25, 0., 200.)
    hists[sr]["id_d0"] = ROOT.TH1F(sr+"_id_d0_matched", sr+"_id_d0_matched", 25, 0., 200.)
    hists[sr]["pt_diff_v_d0"] = ROOT.TH2F(sr+"_pt_diff", sr+"_pt_diff", 25, 0., 200., 25, -1., 1.)

#append = "_largeR"
append = ""

sr = "ee"
t = getTree(fname)

def getChildIndex(event, tp_index):
    tp_barcode = event.truthLepton_barcode[tp_index]
    for x in xrange(len(event.truthLepton_barcode)):
        if event.truthLepton_parentBarcode[x]==tp_barcode:
            return x
    return -1

def getDecayRadius(event, j):
    return (event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2)

n_events=0
for event in t:

    if n_events%5000==0: print "Processing event", n_events
    n_events += 1

    #if n_events>10: break

    # Find actually good truth leptons
    true_indices = []
    for i in xrange(len(event.truthLepton_pt)):
        if abs(event.truthLepton_pdgId[i]) == 11 and abs(event.truthLepton_parentPdgId[i])>1000000: # and abs(event.truthLepton_eta[i])<1 and abs(event.truthLepton_z0[i])<100:
            #if event.truthLepton_pdgId[i] == 22:
            true_indices.append(i)

    #if len(true_indices) != 2: continue

    for j in true_indices:
        hists[sr]["true_d0"].Fill(abs(event.truthLepton_d0[j]))
        #hists[sr]["true_d0"].Fill((event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2))#abs(event.truthLepton_d0[i]))
        #hists[sr]["true_d0"].Fill(abs(event.truthLepton_pt[j]))

        for i in xrange(len(event.gsfTrack_d0)):
            if event.gsfTrack_pt[i]<40: continue
            if abs(event.truthLepton_d0[j]-abs(event.gsfTrack_d0[i]))/abs(event.truthLepton_d0[j])<0.01:
                hists[sr]["gsf_d0"].Fill(event.truthLepton_d0[j])
                #hists[sr]["gsf_d0"].Fill(event.truthLepton_pt[j])
                #hists[sr]["gsf_d0"].Fill((event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2))#event.truthLepton_d0[j])
                break

        found_idtrk = False
        for i in xrange(len(event.idTrack_d0)):
            if event.idTrack_pt[i]<40: continue
            if abs(event.truthLepton_d0[j]-abs(event.idTrack_d0[i]))/abs(event.truthLepton_d0[j])<0.01:
                hists[sr]["id_d0"].Fill(event.truthLepton_d0[j])
                #hists[sr]["id_d0"].Fill(event.truthLepton_pt[j])
                #hists[sr]["id_d0"].Fill((event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2))#event.truthLepton_d0[j])
                found_idtrk = True
                break

        if not found_idtrk:
            #print "pdgid:", event.truthLepton_pdgId[j]
            #print "d0:", event.truthLepton_d0[j]
            #print "pt:", event.truthLepton_pt[j]
            #print "children:"

            current_index = j
            chain_size = 0
            last_pt = event.truthLepton_pt[j]
            last_d0 = event.truthLepton_d0[j]

            while getChildIndex(event, current_index) != -1 and getDecayRadius(event, current_index)<1000:
                current_index = getChildIndex(event, current_index)
                last_pt = event.truthLepton_pt[current_index]
                last_d0 = event.truthLepton_d0[current_index]

            #print "\tpt:", last_pt
            #print "\td0:", last_d0
            hists[sr]["pt_diff_v_d0"].Fill(event.truthLepton_d0[j], (last_pt-event.truthLepton_pt[j])/event.truthLepton_pt[j])
    '''
    # Find GSF tracks that match them
    for i in xrange(len(event.gsfTrack_d0)):
        if event.gsfTrack_pt[i]<40: continue
        for j in true_indices:
            #if event.truthLepton_barcode[j] == event.gsfTrack_truthMatchedBarcode[i]:
            #if abs(event.truthLepton_pt[j]-event.gsfTrack_pt[i])/event.truthLepton_pt[j]<.6 and abs(event.truthLepton_eta[j]-event.gsfTrack_eta[i])<0.4 and abs(event.truthLepton_d0[j]-event.gsfTrack_d0[i])/abs(event.truthLepton_d0[j])<0.4:
            #if abs(event.truthLepton_eta[j]-event.gsfTrack_eta[i])<0.3 and abs(event.truthLepton_d0[j]-abs(event.gsfTrack_d0[i]))/abs(event.truthLepton_d0[j])<0.05:
            if abs(event.truthLepton_d0[j]-abs(event.gsfTrack_d0[i]))/abs(event.truthLepton_d0[j])<0.05:
                #hists[sr]["gsf_d0"].Fill((event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2))#event.truthLepton_d0[j])
                hists[sr]["gsf_d0"].Fill(event.truthLepton_d0[j])
                break

    # Find ID tracks that match them
    for i in xrange(len(event.idTrack_d0)):
        if event.idTrack_pt[i]<40: continue
        for j in true_indices:
            #if event.truthLepton_barcode[j] == event.idTrack_truthMatchedBarcode[i]:
            if abs(event.truthLepton_d0[j]-abs(event.idTrack_d0[i]))/abs(event.truthLepton_d0[j])<0.05:
                # hists[sr]["id_d0"].Fill((event.truthLepton_VtxX[j]**2+event.truthLepton_VtxY[j]**2)**(1./2))#event.truthLepton_d0[j])
                hists[sr]["id_d0"].Fill(event.truthLepton_d0[j])
                break
    '''
can = ROOT.TCanvas("can", "can")
hists[sr]["pt_diff_v_d0"].GetXaxis().SetTitle("d_{0}[mm]")
hists[sr]["pt_diff_v_d0"].GetYaxis().SetTitle("Energy Change at 1m")
hists[sr]["pt_diff_v_d0"].Draw("colz")
raw_input("...")
#can1 = ROOT.TCanvas("can1", "can1")
#hists[sr]["true_d0"].Draw()
#raw_input("...")
can0 = ROOT.TCanvas("can0", "can0")
#hists[sr]["eff_gsf_d0"].Add(hists[sr]["gsf_d0"])
#hists[sr]["eff_gsf_d0"].Divide(hists[sr]["true_d0"])
hists[sr]["eff_gsf_d0"].Divide(hists[sr]["gsf_d0"], hists[sr]["true_d0"], 1, 1, "B")
hists[sr]["eff_gsf_d0"].SetTitle("GSF Track")
hists[sr]["eff_gsf_d0"].GetXaxis().SetTitle("d_{0}")
#hists[sr]["eff_gsf_d0"].GetXaxis().SetTitle("Radius of Decay [mm]")
#hists[sr]["eff_gsf_d0"].GetXaxis().SetTitle("Lepton p_{T} [GeV]")
hists[sr]["eff_gsf_d0"].GetYaxis().SetTitle("Efficency to Reconstruct Track")
hists[sr]["eff_gsf_d0"].SetFillColor(theme_colors[0])
hists[sr]["eff_gsf_d0"].SetMarkerColor(theme_colors[0])
hists[sr]["eff_gsf_d0"].SetLineColor(theme_colors[0])
hists[sr]["eff_gsf_d0"].SetMaximum(1.5)
hists[sr]["eff_gsf_d0"].SetMinimum(0)
hists[sr]["eff_gsf_d0"].Draw("hist")

#can1 = ROOT.TCanvas("can1", "can1")
hists[sr]["eff_id_d0"].Divide(hists[sr]["id_d0"], hists[sr]["true_d0"], 1, 1, "B")
hists[sr]["eff_id_d0"].SetTitle("ID Track")
hists[sr]["eff_id_d0"].SetLineColor(theme_colors[2])
hists[sr]["eff_id_d0"].SetMarkerColor(theme_colors[2])
hists[sr]["eff_id_d0"].SetMaximum(1.5)
hists[sr]["eff_id_d0"].SetMinimum(0)
hists[sr]["eff_id_d0"].Draw("hist same")

leg = can0.BuildLegend(.5,.65,.8,.90)
leg.Draw()

can0.Update()
raw_input("...")

