# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
import math
import glob
import os


PTCUT = 65
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 3

variables = {
    "all_true_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "all true p_{T} [GeV]"},
    "lead_true_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "leading true p_{T} [GeV]"},
    "sublead_true_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "subleading true p_{T} [GeV]"},
    "all_matched_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "all matched p_{T} [GeV]"},
    "all_unmatched_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "all matched p_{T} [GeV]"},
    "lead_matched_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "leading matched p_{T} [GeV]"},
    "sublead_matched_pt": {"xmin": 0., "xmax": 500., "nbins": 20, "xlab": "subleading matched p_{T} [GeV]"},

    "all_true_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "all true d_{0} [mm]"},
    "lead_true_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "leading true d_{0} [mm]"},
    "sublead_true_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "subleading true d_{0} [mm]"},
    "all_matched_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "all matched d_{0} [mm]"},
    "all_unmatched_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "all matched d_{0} [mm]"},
    "lead_matched_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "leading matched d_{0} [mm]"},
    "sublead_matched_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "subleading matched d_{0} [mm]"},
}



def goodTrue(i):
   # if event.truthLepton_pt[i] < PTCUT: return False
    if abs(event.truthLepton_eta[i]) > ETACUT: return False
    if abs(event.truthLepton_parentPdgId[it]) < 100000 : return False

    return True
    

def initializeLepHistos():
    h = {}
    for flav in ["e","m"]: 
        h[flav] = {}
    
        for var in variables:
            hname = "h_%s_%s"%(var, flav)
            h[flav][var] = ROOT.TH1F(hname, hname, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            h[flav][var].SetDirectory(0)

    return h


wps = {
    "nod0_noSI": "",
    "nod0_yesSI": "",
    "yesd0_yesSI": "",
    "yesd0_noSI": "",
}
masses = ["100","500"]




for wp in wps:
    for mass in masses: 
        print "Processing %s %s"%(wp,mass)

        AODFiles = glob.glob("/eos/user/l/lhoryn/highd0/loose_v2/*%s*_%s_trees.root/*"%(mass,wp))
        chain = ROOT.TChain("trees_SR_highd0_")
        for filename in AODFiles:
            chain.Add(filename)
        
        h=initializeLepHistos()

        print "entries ", chain.GetEntries()
        for event in chain:
            
            leadingLep = False
            subleadingLep = False
            nElecs = 0
            nMus = 0

            for it in xrange(len(event.truthLepton_pdgId)): 
                if abs(event.truthLepton_pdgId[it]) == 11: nElecs +=1
                if abs(event.truthLepton_pdgId[it]) == 13: nMus +=1

                if abs(event.truthLepton_pdgId[it]) == 11 and goodTrue(it):
                        
                    h["e"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["e"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                        
                    if not leadingLep:
                        h["e"]["lead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["e"]["lead_true_pt"].Fill(event.truthLepton_pt[it])
                        leadingLep = True
                    elif not subleadingLep: 
                        h["e"]["sublead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["e"]["sublead_true_pt"].Fill(event.truthLepton_pt[it])
                        subleadingLep = True

                if abs(event.truthLepton_pdgId[it]) == 13 and goodTrue(it):  
                    h["m"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["m"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                        
                    if not leadingLep:
                        h["m"]["lead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["m"]["lead_true_pt"].Fill(event.truthLepton_pt[it])
                        leadingLep = True
                    elif not subleadingLep: 
                        h["m"]["sublead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["m"]["sublead_true_pt"].Fill(event.truthLepton_pt[it])
                        subleadingLep = True


            leadingLep = False
            subleadingLep = False
            nPassedReco = 0
            for ie in xrange(len(event.electron_pt)):
                if event.electron_passOR[ie] == 0 or abs(event.electron_eta[ie]) > 2.5: continue

                if event.electron_truthMatchedBarcode[ie] == -1: 
                    h["e"]["all_unmatched_d0"].Fill(event.electron_d0[ie])
                    h["e"]["all_unmatched_pt"].Fill(event.electron_pt[ie])
                    continue
                
                barcode = event.electron_truthMatchedBarcode[ie]
                
                for ii in xrange(len(event.truthLepton_pt)):
                    if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):
                        truthd0 = abs(event.truthLepton_d0[ii])
                        truthPt = event.truthLepton_pt[ii]

                        h["e"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["e"]["all_matched_pt"].Fill(event.truthLepton_pt[ii])
                        if not leadingLep:
                            h["e"]["lead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["e"]["lead_matched_pt"].Fill(event.truthLepton_pt[ii])
                            leadingLep = True
                        elif not subleadingLep:
                            h["e"]["sublead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["e"]["sublead_matched_pt"].Fill(event.truthLepton_pt[ii])
                            subleadingLep = True
            
            for im in xrange(len(event.muon_pt)):
                if event.muon_passOR[im] == 0 or abs(event.muon_eta[im]) > 2.5: continue

                if event.muon_truthMatchedBarcode[im] == -1: 
                    h["m"]["all_unmatched_d0"].Fill(event.muon_IDtrack_d0[im])
                    h["m"]["all_unmatched_pt"].Fill(event.muon_pt[im])
                    continue

                if event.muon_truthMatchedBarcode[im] == -1: continue
                
                barcode = event.muon_truthMatchedBarcode[im]
                
                for ii in xrange(len(event.truthLepton_pt)):
                    if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):
                        truthd0 = abs(event.truthLepton_d0[ii])
                        truthPt = event.truthLepton_pt[ii]

                        h["m"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["m"]["all_matched_pt"].Fill(event.truthLepton_pt[ii])
                        if not leadingLep:
                            h["m"]["lead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["m"]["lead_matched_pt"].Fill(event.truthLepton_pt[ii])
                            leadingLep = True
                        elif not subleadingLep:
                            h["m"]["sublead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["m"]["sublead_matched_pt"].Fill(event.truthLepton_pt[ii])
                            subleadingLep = True
            
        


        kind = ["lead","sublead","all"]
        param = ["d0","pt"]
        flav = ["e","m"]
        for f in flav:
            for k in kind:
                for p in param:
                    print "making efficency with " + k + " " + p  
                    print "passed: %s total : %s"%(h[f][k+"_matched_"+p].GetEntries(),h[f][k+"_true_"+p].GetEntries())         
                    h[f][k + "_" + p + "_eff"] = ROOT.TEfficiency(h[f][k+"_matched_"+p],h[f][k+"_true_"+p])
                    h[f][k + "_" + p + "_eff"].SetName(f + "_" + k + "_" + p + "_eff")
        print "making d0 fake rate"
        print "passed: %s total : %s"%(h["e"]["all_unmatched_d0"].GetEntries(),h["e"]["all_true_d0"].GetEntries())         
        h["e"]["all_d0_fake"] = ROOT.TEfficiency(h["e"]["all_unmatched_d0"],h["e"]["all_true_d0"])  
        print "making pt fake rate"
        print "passed: %s total: %s"%(h["e"]["all_unmatched_pt"].GetEntries(),h["e"]["all_true_pt"].GetEntries())         
        h["e"]["all_pt_fake"] = ROOT.TEfficiency(h["e"]["all_unmatched_pt"],h["e"]["all_true_pt"])  
        
        print "making d0 fake rate"
        print "passed: %s total : %s"%(h["m"]["all_unmatched_d0"].GetEntries(),h["m"]["all_true_d0"].GetEntries())         
        h["m"]["all_d0_fake"] = ROOT.TEfficiency(h["m"]["all_unmatched_d0"],h["m"]["all_true_d0"])  
        print "making pt fake rate"
        print "passed: %s total: %s"%(h["m"]["all_unmatched_pt"].GetEntries(),h["m"]["all_true_pt"].GetEntries())         
        h["m"]["all_pt_fake"] = ROOT.TEfficiency(h["m"]["all_unmatched_pt"],h["m"]["all_true_pt"])  

        outpfile = "../outputFiles/wpstudies/loose_%s_%s_histos.root"%(mass,wp)
        outFile = ROOT.TFile(outpfile, "RECREATE")
        print "made ", outpfile
        for f in flav:
            for var in h[f]:
                print "writing " + f + " " + var
                if h[f][var]: h[f][var].Write()
        outFile.Write()

