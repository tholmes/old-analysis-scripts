import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#color1 is for ST
color1 = ROOT.TColor.GetColor('#3a0ca3')
#color1 = ROOT.kBlack
color2 = ROOT.TColor.GetColor('#a4036f')


d0s={}
pts={}
leg = ROOT.TLegend(.6,.7,.8,.87)

trackings = ["LRT","ST"]
wps = ["custom", "standard"]
flavs = ["e", "m"]
leps = ["all","lead","sublead"]
var = ["d0","pt"]


labels = { "LRT_custom": "ST+LRT, custom WP", "LRT_standard": "ST+LRT, standard WP", "ST_custom": "ST, custom WP", "ST_standard": "ST, standard WP" }
#for inpfile in os.listdir("outputFiles/"):
indir = "../outputFiles/wpstudies/"
f_LRT_custom = ROOT.TFile(indir + "LRT_custom_histos.root")
f_LRT_standard = ROOT.TFile(indir + "LRT_standard_histos.root")
f_ST_custom = ROOT.TFile(indir + "ST_custom_histos.root")
f_ST_standard = ROOT.TFile(indir + "ST_standard_histos.root")

h = {
    "LRT_custom" : { "e" : { "d0" : { "all": f_LRT_custom.Get('e_all_d0_eff'), "lead" :  f_LRT_custom.Get('e_lead_d0_eff'), "sublead" :  f_LRT_custom.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_custom.Get('e_all_pt_eff'), "lead" :  f_LRT_custom.Get('e_lead_pt_eff'), "sublead" :  f_LRT_custom.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_LRT_custom.Get('m_all_d0_eff'), "lead" :  f_LRT_custom.Get('m_lead_d0_eff'), "sublead" :  f_LRT_custom.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_custom.Get('m_all_pt_eff'), "lead" :  f_LRT_custom.Get('m_lead_pt_eff'), "sublead" :  f_LRT_custom.Get('m_sublead_pt_eff')}}}, 

    "LRT_standard" : { "e" : { "d0" : { "all": f_LRT_standard.Get('e_all_d0_eff'), "lead" :  f_LRT_standard.Get('e_lead_d0_eff'), "sublead" :  f_LRT_standard.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_LRT_standard.Get('e_all_pt_eff'), "lead" :  f_LRT_standard.Get('e_lead_pt_eff'), "sublead" :  f_LRT_standard.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_LRT_standard.Get('m_all_d0_eff'), "lead" :  f_LRT_standard.Get('m_lead_d0_eff'), "sublead" :  f_LRT_standard.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_standard.Get('m_all_pt_eff'), "lead" :  f_LRT_standard.Get('m_lead_pt_eff'), "sublead" :  f_LRT_standard.Get('m_sublead_pt_eff')}}},

    "ST_custom" : { "e" : { "d0" : { "all": f_ST_custom.Get('e_all_d0_eff'), "lead" :  f_ST_custom.Get('e_lead_d0_eff'), "sublead" :  f_ST_custom.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_ST_custom.Get('e_all_pt_eff'), "lead" :  f_ST_custom.Get('e_lead_pt_eff'), "sublead" :  f_ST_custom.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_ST_custom.Get('m_all_d0_eff'), "lead" :  f_ST_custom.Get('m_lead_d0_eff'), "sublead" :  f_ST_custom.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_ST_custom.Get('m_all_pt_eff'), "lead" :  f_ST_custom.Get('m_lead_pt_eff'), "sublead" :  f_ST_custom.Get('m_sublead_pt_eff')}}}, 

    "ST_standard" : { "e" : { "d0" : { "all": f_ST_standard.Get('e_all_d0_eff'), "lead" :  f_ST_standard.Get('e_lead_d0_eff'), "sublead" :  f_ST_standard.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_ST_standard.Get('e_all_pt_eff'), "lead" :  f_ST_standard.Get('e_lead_pt_eff'), "sublead" :  f_ST_standard.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_ST_standard.Get('m_all_d0_eff'), "lead" :  f_ST_standard.Get('m_lead_d0_eff'), "sublead" :  f_ST_standard.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_ST_standard.Get('m_all_pt_eff'), "lead" :  f_ST_standard.Get('m_lead_pt_eff'), "sublead" :  f_ST_standard.Get('m_sublead_pt_eff')}}}
}


for f in flavs:
    for v in var:
        for l in leps:
            if l != "all": continue
            
            c = ROOT.TCanvas()

            h["LRT_standard"][f][v][l].SetMarkerStyle(25) 
            h["LRT_standard"][f][v][l].SetMarkerColor(color2) 
            h["LRT_standard"][f][v][l].SetLineColor(color2) 
            h["ST_standard"][f][v][l].SetMarkerStyle(4) 
            h["ST_standard"][f][v][l].SetMarkerColor(color1) 
            h["ST_standard"][f][v][l].SetLineColor(color1) 

            h["LRT_custom"][f][v][l].SetMarkerColor(color2) 
            h["LRT_custom"][f][v][l].SetLineColor(color2) 
            h["LRT_custom"][f][v][l].SetMarkerStyle(21) 
            h["ST_custom"][f][v][l].SetMarkerColor(color1) 
            h["ST_custom"][f][v][l].SetLineColor(color1) 
            h["ST_custom"][f][v][l].SetMarkerStyle(12) 
            


            leg = ROOT.TLegend(0.60,0.75,0.85,0.92)
            leg.SetFillStyle(0)
            #leg.SetTextSize(0.04) 
            for b in labels.keys():
                leg.AddEntry(h[b][f][v][l],labels[b],"p")
            h["LRT_custom"][f][v][l].Draw("pa")
            ROOT.gPad.Update()
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetYaxis().SetRangeUser(0,1.4)
            if "d0" in v: h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetRangeUser(0,300)
            h["LRT_custom"][f][v][l].SetTitle(";True %s %s; Efficiency"%(("electron" if f == "e" else "muon" ),("d_{0} [mm]" if v == "d0" else "p_{T} [GeV]")) )
            
            h["ST_custom"][f][v][l].Draw("psame")
            h["ST_standard"][f][v][l].Draw("psame")
            h["LRT_standard"][f][v][l].Draw("psame")
            leg.Draw("same")
            
            ROOT.ATLASLabel(0.20,0.88, "Simulation Internal")
ratio_signal.Divide(h_signal_pass, h_signal_all,1,1,"B")
            text = ROOT.TLatex()
            text.SetNDC()
            text.DrawLatex(0.20,0.83, "500 GeV Slepton, 1ns" )

            ROOT.gPad.Update()
            c.Update()
            c.SaveAs("../outputFiles/wpstudies/plots/wp_"+f+"_"+v+"_"+l+"_wip.pdf")
            c.SaveAs("../outputFiles/wpstudies/plots/wp_"+f+"_"+v+"_"+l+"_wip.C")



