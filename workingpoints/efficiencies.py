# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
import math
import glob
import os


execfile("../cosmics/cosmic_helpers.py")				       

ROOT.gROOT.SetBatch(1)


PTCUT = 50
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 2


variables = {
    "all_pt": {"low": 0., "high": 1000., "nb": 40, "label": "all p_{T} [GeV]"},
    "matched_pt": {"low": 0., "high": 1000., "nb": 40, "label": "matched p_{T} [GeV]"},

    "all_d0": {"low": 0., "high": 300., "nb": 40, "label": "all d_{0} [mm]"},
    "matched_d0": {"low": 0., "high": 300., "nb": 40, "label": "matched d_{0} [mm]"},
}



def goodTrue(i):
    if event.truthLepton_pt[i] < PTCUT: return False
    if abs(event.truthLepton_d0[i]) < D0CUT: return False
    if event.truthLepton_eta[i] > ETACUT: return False
    if not (abs(event.truthLepton_parentPdgId[i]) > 100000 or abs(event.truthLepton_parentpPdgId[i] == 17)): return False

    return True
    

def initializeLepHistos():
    h = {}
    for flav in ["e","m"]:
        h[flav] = {}
        for s in ["baseline", "signal","true"]:
            h[flav][s] = {}
            for var in variables:
                hname = "h_%s_%s_%s"%(var, flav, s)
                h[flav][s][var] = ROOT.TH1F(hname, hname, variables[var]["nb"], variables[var]["low"], variables[var]["high"])
                h[flav][s][var].SetDirectory(0)

    return h




dirname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_KIND_directLLP_MASS_0_LIFETIME.DAOD_RPVLL_*_v5.1_trees.root/*"



lifetime_bins = ["0p01", "0p1", "1"]
mass_bins = ["50", "100", "200", "300", "400", "500", "600", "700", "800"]
flavs = ["e","m","true"]
kinds = ["SlepSlep","StauStau"]
probes = ["baseline","signal"]

for m in mass_bins:
    for l in lifetime_bins:
        for d in kinds:
            
            h = initializeLepHistos()
             
            print "working with %s %s %s"%(d,m,l)
            AODFiles = glob.glob( dirname.replace("KIND",d).replace("MASS",m).replace("LIFETIME",l) )
            chain = ROOT.TChain("trees_SR_highd0_")

            for filename in AODFiles:
                chain.Add(filename)
            
            print "entries ", chain.GetEntries()
            for event in chain:

                for it in xrange(len(event.truthLepton_pdgId)): 
                    if abs(event.truthLepton_pdgId[it]) == 11 and goodTrue(it):
                            
                        h["e"]["true"]["all_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["e"]["true"]["all_pt"].Fill(event.truthLepton_pt[it])
                            
                    if abs(event.truthLepton_pdgId[it]) == 13 and goodTrue(it):  
                        h["m"]["true"]["all_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["m"]["true"]["all_pt"].Fill(event.truthLepton_pt[it])
                            
                for ie in xrange(len(event.electron_pt)):
                    if event.electron_truthMatchedBarcode[ie] == -1: continue
                    
                    barcode = event.electron_truthMatchedBarcode[ie]
                    
                    for ii in xrange(len(event.truthLepton_pt)):
                        if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):

                            h["e"]["baseline"]["matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["e"]["baseline"]["matched_pt"].Fill(event.truthLepton_pt[ii])
                            
                            if event.electron_isSignal[ie]:
                                h["e"]["signal"]["matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                                h["e"]["signal"]["matched_pt"].Fill(event.truthLepton_pt[ii])
                
                for im in xrange(len(event.muon_pt)):
                    if event.muon_truthMatchedBarcode[im] == -1: continue
                    
                    barcode = event.muon_truthMatchedBarcode[im]
                    
                    for ii in xrange(len(event.truthLepton_pt)):
                        if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):

                            h["m"]["baseline"]["matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                            h["m"]["baseline"]["matched_pt"].Fill(event.truthLepton_pt[ii])
                            
                            if event.muon_isSignal[im]:
                                h["m"]["signal"]["matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                                h["m"]["signal"]["matched_pt"].Fill(event.truthLepton_pt[ii])
                       


            for f in ["e","m"]:
                for p in ["baseline","signal"]:
                    for k in ["d0","pt"]:
                        #print h
                        print "making efficency with " + f + " " + k + " " + p
                        h[f]["eff"]={}  
                        h[f]["eff"][k + "_" + p + "_eff"] = ROOT.TEfficiency(h[f][p]["matched_"+k],h[f]["true"]["all_"+k])
                        h[f]["eff"][k + "_" + p + "_eff"].SetName(f + "_" + k + "_" + p + "_eff")
                
                print h[f] 
            outpfile = "../outputFiles/wpstudies/sigbase_%s_%s_%s_histos.root"%(m,l,k)
            outFile = ROOT.TFile(outpfile, "RECREATE")

            print "made ", outpfile
            for f in ["e","m"]:
                for p in ["baseline","signal","eff"]:
                    for var in h[f][p]:
                        print "writing " + h[f][p][var].GetName()
                        h[f][p][var].Write()


            outFile.Write()

