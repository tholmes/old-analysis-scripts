# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
import math
import glob
import os

execfile("../cosmics/cosmic_helpers.py")

ROOT.gROOT.SetBatch(1)

PTCUT = 50
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 2

#pt_bins = [0,50,70,90,110,130,150,170,190,210,230,250,270,290,310,330,350,390,410,450,500,550,600,650,700,800,1000]
pt_bins = [-5,50,100,150,200,250,300,350,400,450,500,550,600,650,700,800,1000]
d0_bins = [-5,10,20,30,40,50,60,70,80,100,150,200,300]


variables = {
    #"all_true_pt": {"xmin": 0., "xmax": 1000., "nbins": 40, "xlab": "all true p_{T} [GeV]"},
    #"all_matched_pt": {"xmin": 0., "xmax": 1000., "nbins": 40, "xlab": "all matched p_{T} [GeV]"},
    #"all_true_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "all true d_{0} [mm]"},
    #"all_matched_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "all matched d_{0} [mm]"},

    "all_true_pt": {"bins": pt_bins , "xlab": "all true p_{T} [GeV]"},
    "all_matched_pt": {"bins":pt_bins, "xlab": "all matched p_{T} [GeV]"},

    "all_true_d0": {"bins":d0_bins, "xlab": "all true d_{0} [mm]"},
    "all_matched_d0": {"bins":d0_bins, "xlab": "all matched d_{0} [mm]"},
}



def goodTrue(i):
    if event.truthLepton_pt[i] < PTCUT: return False
    if abs(event.truthLepton_d0[i]) < D0CUT: return False
    if abs(event.truthLepton_eta[i]) > ETACUT: return False
    if abs(event.truthLepton_parentPdgId[it]) < 100000 : return False

    return True
    

def initializeLepHistos():
    h = {}
    for flav in ["e","m"]: 
        h[flav] = {}
    
        for var in variables:
            hname = "h_%s_%s"%(var, flav)
            try:
                h[flav][var] = ROOT.TH1F(hname, hname, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            except:
                 h[flav][var] = ROOT.TH1F(hname, hname, len(variables[var]["bins"])-1, array('d',variables[var]["bins"]))
            h[flav][var].SetDirectory(0)

    return h




dirs = {
    #"LRT_custom": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*ns.DAOD_RPVLL_*_v5.1_trees.root/*",
    #"LRT_standard": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/workingpoints/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*ns.noCustomWP_*_trees.root/*", 
    #"ST_custom": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*ns.DAOD_RPVLL_*_v5.1_trees.root/*",
    #"ST_standard": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/workingpoints/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*ns.noCustomWP_*_trees.root/*", 
    "LRT_custom": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.5_Aug8_alltracks/*SlepSlep*_500_0_1ns*/*",
    "LRT_standard": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/workingpoints/*SlepSlep*500_0_1ns*nocustom*/*", 
    "ST_custom": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.5_Aug8_alltracks/*SlepSlep*_500_0_1ns*/*",
    "ST_standard": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/workingpoints/*SlepSlep*500_0_1ns*nocustom*/*", 
}
    #"LRT_custom": "/eos/user/l/lhoryn/highd0/workingpoints_forsure/LRT_customWP/data-trees/*",
    #"LRT_standard": "/eos/user/l/lhoryn/highd0/workingpoints_forsure/LRT_standardWP/data-trees/*", 


for inp in dirs:

    AODFiles = glob.glob(dirs[inp])
    chain = ROOT.TChain("trees_SR_highd0_")

    for filename in AODFiles:
        chain.Add(filename)
    
    h=initializeLepHistos()
    print inp
    print "entries ", chain.GetEntries()
    for event in chain:
        
        for it in xrange(len(event.truthLepton_pdgId)): 
            
            if abs(event.truthLepton_pdgId[it]) == 11 and goodTrue(it) and event.truthLepton_pt[it] > 50:
                
                h["e"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                h["e"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                
                for ie in xrange(len(event.electron_pt)):
                    if event.electron_truthMatchedBarcode[ie] == event.truthLepton_barcode[it] and ("ST" in inp and event.electron_isLRT[ie]==0 or "LRT" in inp) :
                        h["e"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["e"]["all_matched_pt"].Fill(event.truthLepton_pt[it])
             
                    
            if abs(event.truthLepton_pdgId[it]) == 13 and goodTrue(it) and event.truthLepton_pt[it] > 50:  
                h["m"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                h["m"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                
                for im in xrange(len(event.muon_pt)):
                    if event.muon_truthMatchedBarcode[im] == event.truthLepton_barcode[it] and ("ST" in inp and event.muon_IDtrack_isLRT[im]==0 or "LRT" in inp):
                        h["m"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[it]))
                        h["m"]["all_matched_pt"].Fill(event.truthLepton_pt[it])
                    
    
    flav = ["e","m"]
    for f in flav:
        for hist in h[f]:
            print hist
            print h[f][hist].Integral()


    kind = ["all"]
    param = ["d0","pt"]
    for f in flav:
            for p in param:
                print "making efficency with " + f + " " + p  
                h[f]["all_" + p + "_eff"] = ROOT.TEfficiency(h[f]["all_matched_"+p],h[f]["all_true_"+p])
                h[f]["all_" + p + "_eff"].SetName(f + "_all_" + p + "_eff")
                
    outpfile = "../outputFiles/wpstudies/"+inp+"_histos.root"
    outFile = ROOT.TFile(outpfile, "RECREATE")
    
    print "made ", outpfile
    for f in flav:
        for var in h[f]:
            if h[f][var]: 
                h[f][var].Write()
    
    outFile.Write()

