import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#color1 is for ST
color1 = ROOT.TColor.GetColor('#3a0ca3')
color2 = ROOT.TColor.GetColor('#a4036f')



d0s={}
pts={}

trackings = ["LRT","ST"]
wps = ["custom", "standard"]
#flavs = ["e", "m"]
flavs = ["m", "e"]
var = ["d0","pt"]
#var = ["d0"]


labels = { "LRT_custom": "extended tracking, modified ID alg.", "LRT_standard": "extended tracking, standard ID alg.", "ST_custom": "standard tracking, modified ID alg.", "ST_standard": "standard tracking, standard ID alg." }
#for inpfile in os.listdir("outputFiles/"):
indir = "../outputFiles/wpstudies/"
f_LRT_custom = ROOT.TFile(indir + "LRT_custom_histos.root")
f_LRT_standard = ROOT.TFile(indir + "LRT_standard_histos.root")
f_ST_custom = ROOT.TFile(indir + "ST_custom_histos.root")
f_ST_standard = ROOT.TFile(indir + "ST_standard_histos.root")

h = {
    "LRT_custom" : { "e" : { "d0" : { "all": f_LRT_custom.Get('e_all_d0_eff'), "lead" :  f_LRT_custom.Get('e_lead_d0_eff'), "sublead" :  f_LRT_custom.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_custom.Get('e_all_pt_eff'), "lead" :  f_LRT_custom.Get('e_lead_pt_eff'), "sublead" :  f_LRT_custom.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_LRT_custom.Get('m_all_d0_eff'), "lead" :  f_LRT_custom.Get('m_lead_d0_eff'), "sublead" :  f_LRT_custom.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_custom.Get('m_all_pt_eff'), "lead" :  f_LRT_custom.Get('m_lead_pt_eff'), "sublead" :  f_LRT_custom.Get('m_sublead_pt_eff')}}}, 

    "LRT_standard" : { "e" : { "d0" : { "all": f_LRT_standard.Get('e_all_d0_eff'), "lead" :  f_LRT_standard.Get('e_lead_d0_eff'), "sublead" :  f_LRT_standard.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_LRT_standard.Get('e_all_pt_eff'), "lead" :  f_LRT_standard.Get('e_lead_pt_eff'), "sublead" :  f_LRT_standard.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_LRT_standard.Get('m_all_d0_eff'), "lead" :  f_LRT_standard.Get('m_lead_d0_eff'), "sublead" :  f_LRT_standard.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_LRT_standard.Get('m_all_pt_eff'), "lead" :  f_LRT_standard.Get('m_lead_pt_eff'), "sublead" :  f_LRT_standard.Get('m_sublead_pt_eff')}}},

    "ST_custom" : { "e" : { "d0" : { "all": f_ST_custom.Get('e_all_d0_eff'), "lead" :  f_ST_custom.Get('e_lead_d0_eff'), "sublead" :  f_ST_custom.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_ST_custom.Get('e_all_pt_eff'), "lead" :  f_ST_custom.Get('e_lead_pt_eff'), "sublead" :  f_ST_custom.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_ST_custom.Get('m_all_d0_eff'), "lead" :  f_ST_custom.Get('m_lead_d0_eff'), "sublead" :  f_ST_custom.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_ST_custom.Get('m_all_pt_eff'), "lead" :  f_ST_custom.Get('m_lead_pt_eff'), "sublead" :  f_ST_custom.Get('m_sublead_pt_eff')}}}, 

    "ST_standard" : { "e" : { "d0" : { "all": f_ST_standard.Get('e_all_d0_eff'), "lead" :  f_ST_standard.Get('e_lead_d0_eff'), "sublead" :  f_ST_standard.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_ST_standard.Get('e_all_pt_eff'), "lead" :  f_ST_standard.Get('e_lead_pt_eff'), "sublead" :  f_ST_standard.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_ST_standard.Get('m_all_d0_eff'), "lead" :  f_ST_standard.Get('m_lead_d0_eff'), "sublead" :  f_ST_standard.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_ST_standard.Get('m_all_pt_eff'), "lead" :  f_ST_standard.Get('m_lead_pt_eff'), "sublead" :  f_ST_standard.Get('m_sublead_pt_eff')}}}
}


c = ROOT.TCanvas("c","c",1200,600)
for v in var:
    c.Clear()
    c.Divide(2,0,0,0)
    for f in flavs:
        l = "all" 

        h["LRT_standard"][f][v][l].SetMarkerStyle(25) 
        h["LRT_standard"][f][v][l].SetMarkerColor(color2) 
        h["LRT_standard"][f][v][l].SetLineColor(color2) 
        h["ST_standard"][f][v][l].SetMarkerStyle(4) 
        h["ST_standard"][f][v][l].SetMarkerColor(color1) 
        h["ST_standard"][f][v][l].SetLineColor(color1) 

        h["LRT_custom"][f][v][l].SetMarkerColor(color2) 
        h["LRT_custom"][f][v][l].SetLineColor(color2) 
        h["LRT_custom"][f][v][l].SetMarkerStyle(21) 
        h["ST_custom"][f][v][l].SetMarkerColor(color1) 
        h["ST_custom"][f][v][l].SetLineColor(color1) 
        h["ST_custom"][f][v][l].SetMarkerStyle(12) 
        
        if f=="e":
            c.cd(1)
            h["LRT_custom"][f][v][l].Draw("pa")
            ROOT.gPad.Update()
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetYaxis().SetRangeUser(0,1.1)
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetRangeUser(-5,295)
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetLabelFont(42)
            #h["LRT_custom"][f][v][l].SetTitle(";%s %s; Efficiency"%(("Electron" if f == "e" else "Muon" ),("#left|d_{0}#right| [mm]" if v == "d0" else "p_{T} [GeV]")) )
            h["LRT_custom"][f][v][l].SetTitle(";%s %s; Efficiency"%(("Electron" if f == "e" else "Muon" ),("#left|d_{0}#right| [mm]" if v == "d0" else "p_{T} [GeV]")) )
            
            h["ST_custom"][f][v][l].Draw("psame")
            h["ST_standard"][f][v][l].Draw("psame")
            h["LRT_standard"][f][v][l].Draw("psame")
            ROOT.ATLASLabel(0.20,0.91, "      Simulation Internal")
            text = ROOT.TLatex()
            text.SetNDC()
            text.SetTextSize(0.04)
            text.DrawLatex(0.2,0.86, "m(#tilde{l}) = 500 GeV, #tau(#tilde{l}) = 1 ns,  #tilde{e}/#tilde{#mu}" )
            text2 = ROOT.TLatex()
            text2.SetNDC()
            text2.SetTextSize(0.04)
            text2.DrawLatex(0.20,0.81, "#sqrt{s} = 13 TeV" )

        if f=="m":
            c.cd(2)
            h["LRT_custom"][f][v][l].Draw("pa")
            ROOT.gPad.Update()
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetYaxis().SetRangeUser(0,1.1)
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetRangeUser(-5, 295)
            h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetLabelFont(42)
            #h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetLabelOffset(0.025)
            h["LRT_custom"][f][v][l].SetTitle(";%s %s; Efficiency"%(("Electron" if f == "e" else "Muon" ),("#left|d_{0}#right| [mm]" if v == "d0" else "p_{T} [GeV]")) )
            #h["LRT_custom"][f][v][l].GetPaintedGraph().GetXaxis().SetLabelSize(0.)
            #axis = ROOT.TGaxis( 0, 0, 320, 1.1, 0.01, 1.1, 510)
            #axis.SetLabelFont(43) # Absolute font size in pixel (precision 3)
            #axis.SetLabelSize(17)
            
            h["ST_custom"][f][v][l].Draw("psame")
            h["ST_standard"][f][v][l].Draw("psame")
            h["LRT_standard"][f][v][l].Draw("psame")
            
            leg = ROOT.TLegend(0.20,0.73,0.95,0.95)
            leg.SetFillStyle(0)
            leg.SetTextSize(0.04) 
            leg.SetTextFont(42) 
            for b in ["ST_standard","ST_custom","LRT_standard","LRT_custom"]:
                leg.AddEntry(h[b][f][v][l],labels[b],"p")

            leg.Draw("same")
       
        ROOT.gPad.Update()

    c.Update()
 #   raw_input("...")
    c.SaveAs("../outputFiles/wpstudies/plots/wp_compare_"+v+"_"+l+"_final.pdf")
    c.SaveAs("../outputFiles/wpstudies/plots/wp_compare_"+v+"_"+l+"_final.C")
    c.SaveAs("../outputFiles/wpstudies/plots/wp_compare_"+v+"_"+l+"_final.eps")
    c.SaveAs("../outputFiles/wpstudies/plots/wp_compare_"+v+"_"+l+"_final.root")



