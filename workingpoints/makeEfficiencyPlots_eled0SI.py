import glob
import ROOT
import os
ROOT.gROOT.SetBatch(True)


execfile("../../scripts/plot_helpers/basic_plotting.py")

#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasStyle.C")
#ROOT.gROOT.LoadMacro("/afs/cern.ch/user/t/tholmes/FTK/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(0)


d0s={}
pts={}
leg = ROOT.TLegend(.6,.7,.8,.87)

trackings = ["LRT","ST"]
wps = ["custom", "standard"]
flavs = ["e"]
leps = ["all","lead","sublead"]
var = ["d0","pt"]


labels = { "nod0_noSI": "no d_{0}, no Si", "nod0_yesSI": "no d_{0}, yes Si",  "yesd0_noSI": "yes d_{0}, no Si","yesd0_yesSI": "yes d_{0}, yes Si" }
#for inpfile in os.listdir("outputFiles/"):
tag = "veryloose_100"
indir = "../outputFiles/wpstudies/%s_"%tag
f_nod0_noSI = ROOT.TFile(indir + "nod0_noSI_histos.root")
f_nod0_yesSI = ROOT.TFile(indir + "nod0_yesSI_histos.root")
f_yesd0_noSI = ROOT.TFile(indir + "yesd0_noSI_histos.root")
f_yesd0_yesSI = ROOT.TFile(indir + "yesd0_yesSI_histos.root")

h = {}

h["eff"] = {
    "nod0_noSI" : { "e" : { "d0" : { "all": f_nod0_noSI.Get('e_all_d0_eff'), "lead" :  f_nod0_noSI.Get('e_lead_d0_eff'), "sublead" :  f_nod0_noSI.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_nod0_noSI.Get('e_all_pt_eff'), "lead" :  f_nod0_noSI.Get('e_lead_pt_eff'), "sublead" :  f_nod0_noSI.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_nod0_noSI.Get('m_all_d0_eff'), "lead" :  f_nod0_noSI.Get('m_lead_d0_eff'), "sublead" :  f_nod0_noSI.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_nod0_noSI.Get('m_all_pt_eff'), "lead" :  f_nod0_noSI.Get('m_lead_pt_eff'), "sublead" :  f_nod0_noSI.Get('m_sublead_pt_eff')}}}, 

    "nod0_yesSI" : { "e" : { "d0" : { "all": f_nod0_yesSI.Get('e_all_d0_eff'), "lead" :  f_nod0_yesSI.Get('e_lead_d0_eff'), "sublead" :  f_nod0_yesSI.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_nod0_yesSI.Get('e_all_pt_eff'), "lead" :  f_nod0_yesSI.Get('e_lead_pt_eff'), "sublead" :  f_nod0_yesSI.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_nod0_yesSI.Get('m_all_d0_eff'), "lead" :  f_nod0_yesSI.Get('m_lead_d0_eff'), "sublead" :  f_nod0_yesSI.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_nod0_yesSI.Get('m_all_pt_eff'), "lead" :  f_nod0_yesSI.Get('m_lead_pt_eff'), "sublead" :  f_nod0_yesSI.Get('m_sublead_pt_eff')}}},

    "yesd0_noSI" : { "e" : { "d0" : { "all": f_yesd0_noSI.Get('e_all_d0_eff'), "lead" :  f_yesd0_noSI.Get('e_lead_d0_eff'), "sublead" :  f_yesd0_noSI.Get('e_sublead_d0_eff')},
                             "pt" : { "all": f_yesd0_noSI.Get('e_all_pt_eff'), "lead" :  f_yesd0_noSI.Get('e_lead_pt_eff'), "sublead" :  f_yesd0_noSI.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_yesd0_noSI.Get('m_all_d0_eff'), "lead" :  f_yesd0_noSI.Get('m_lead_d0_eff'), "sublead" :  f_yesd0_noSI.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_yesd0_noSI.Get('m_all_pt_eff'), "lead" :  f_yesd0_noSI.Get('m_lead_pt_eff'), "sublead" :  f_yesd0_noSI.Get('m_sublead_pt_eff')}}}, 

    "yesd0_yesSI" : { "e" : { "d0" : { "all": f_yesd0_yesSI.Get('e_all_d0_eff'), "lead" :  f_yesd0_yesSI.Get('e_lead_d0_eff'), "sublead" :  f_yesd0_yesSI.Get('e_sublead_d0_eff')},
                               "pt" : { "all": f_yesd0_yesSI.Get('e_all_pt_eff'), "lead" :  f_yesd0_yesSI.Get('e_lead_pt_eff'), "sublead" :  f_yesd0_yesSI.Get('e_sublead_pt_eff')}},
                    "m" : { "d0" : { "all": f_yesd0_yesSI.Get('m_all_d0_eff'), "lead" :  f_yesd0_yesSI.Get('m_lead_d0_eff'), "sublead" :  f_yesd0_yesSI.Get('m_sublead_d0_eff')},
                             "pt" : { "all": f_yesd0_yesSI.Get('m_all_pt_eff'), "lead" :  f_yesd0_yesSI.Get('m_lead_pt_eff'), "sublead" :  f_yesd0_yesSI.Get('m_sublead_pt_eff')}}}
}

h["fake"] = {
    "nod0_noSI" : { "e" : { "d0" : { "all": f_nod0_noSI.Get('e_all_d0_fake')},
                             "pt" : { "all": f_nod0_noSI.Get('e_all_pt_fake')}},
                    "m" : { "d0" : { "all": f_nod0_noSI.Get('m_all_d0_fake')},
                             "pt" : { "all": f_nod0_noSI.Get('m_all_pt_fake')}}}, 

    "nod0_yesSI" : { "e" : { "d0" : { "all": f_nod0_yesSI.Get('e_all_d0_fake')},
                               "pt" : { "all": f_nod0_yesSI.Get('e_all_pt_fake')}},
                    "m" : { "d0" : { "all": f_nod0_yesSI.Get('m_all_d0_fake')},
                             "pt" : { "all": f_nod0_yesSI.Get('m_all_pt_fake')}}},

    "yesd0_noSI" : { "e" : { "d0" : { "all": f_yesd0_noSI.Get('e_all_d0_fake')},
                             "pt" : { "all": f_yesd0_noSI.Get('e_all_pt_fake')}},
                    "m" : { "d0" : { "all": f_yesd0_noSI.Get('m_all_d0_fake')},
                             "pt" : { "all": f_yesd0_noSI.Get('m_all_pt_fake')}}}, 

    "yesd0_yesSI" : { "e" : { "d0" : { "all": f_yesd0_yesSI.Get('e_all_d0_fake')},
                               "pt" : { "all": f_yesd0_yesSI.Get('e_all_pt_fake')}},
                    "m" : { "d0" : { "all": f_yesd0_yesSI.Get('m_all_d0_fake')},
                             "pt" : { "all": f_yesd0_yesSI.Get('m_all_pt_fake')}}}
}

for k in h:
    for f in flavs:
        for v in var:
            for l in leps:
                if l != "all": continue
                if k == "fake": continue
                
                c = ROOT.TCanvas()

                h[k]["yesd0_noSI"][f][v][l].SetMarkerStyle(4) 
                h[k]["yesd0_noSI"][f][v][l].SetMarkerColor(ROOT.kBlue) 
                h[k]["yesd0_noSI"][f][v][l].SetLineColor(ROOT.kBlue) 
                h[k]["nod0_yesSI"][f][v][l].SetMarkerStyle(4) 
                h[k]["nod0_yesSI"][f][v][l].SetMarkerColor(ROOT.kRed) 
                h[k]["nod0_yesSI"][f][v][l].SetLineColor(ROOT.kRed) 

                h[k]["nod0_noSI"][f][v][l].SetMarkerColor(ROOT.kBlue+2) 
                h[k]["nod0_noSI"][f][v][l].SetLineColor(ROOT.kBlue+2) 
                h[k]["nod0_noSI"][f][v][l].SetMarkerStyle(12) 
                h[k]["yesd0_yesSI"][f][v][l].SetMarkerColor(ROOT.kRed+2) 
                h[k]["yesd0_yesSI"][f][v][l].SetLineColor(ROOT.kRed+2) 
                h[k]["yesd0_yesSI"][f][v][l].SetMarkerStyle(12) 
                


                leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
                for b in labels.keys():
                    leg.AddEntry(h[k][b][f][v][l],labels[b],"p")
                
                h[k]["nod0_noSI"][f][v][l].Draw("pa")
                ROOT.gPad.Update()
                h[k]["nod0_noSI"][f][v][l].GetPaintedGraph().GetYaxis().SetRangeUser(0,1.4)
                h[k]["nod0_noSI"][f][v][l].SetTitle("; %s %s truth %s; %s"%(l,("electrons" if f == "e" else "muons" ),("d_{0} [mm]" if v == "d0" else "p_{T} [GeV]"), ("Efficiency" if k == "eff" else "Fake Rate") ))
                
                h[k]["yesd0_noSI"][f][v][l].Draw("psame")
                h[k]["nod0_yesSI"][f][v][l].Draw("psame")
                h[k]["yesd0_yesSI"][f][v][l].Draw("psame")
                leg.Draw("same")
                
                ROOT.ATLASLabel(0.20,0.88, "Internal")
                text = ROOT.TLatex()
                text.SetNDC()
                text.DrawLatex(0.20,0.83, ("Muons" if f == "m" else "Electrons") )

                ROOT.gPad.Update()
                c.Update()
               # raw_input("...")
                c.SaveAs("../outputFiles/wpstudies/plots/wp"+tag+"_"+f+"_"+v+"_"+l+"_"+k+".pdf")
                c.SaveAs("../outputFiles/wpstudies/plots/wp"+tag+"_"+f+"_"+v+"_"+l+"_"+k+".C")



