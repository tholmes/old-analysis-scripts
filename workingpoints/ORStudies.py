# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
import math
import glob


stau_masses = ["80","100","120","140"]
lqd_lifetimes = ["10","100","1000"]
#lifetimes = ["10"]

PTCUT = 60
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 3

def deltaPhi(phi1, phi2):
	dphi = abs(phi1-phi2)
	if (dphi > math.pi):
		dphi = 2.*math.pi - dphi
	return dphi
				       
def deltaR(eta1, eta2, phi1, phi2):
	deltaEta = abs(eta1-eta2)
	deltaPhi = abs(phi1-phi2)
	dR = math.sqrt(deltaPhi*deltaPhi + deltaEta*deltaEta)

	return dR


lumi = 100000 #100fb^-1 in pb^-1

#inpfile = "user.lhoryn.mc16_13TeV.399039.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_0p1ns.082818_fixedOR_trees"
inpfile = "nocustom"

#AODFiles = glob.glob('/eos/user/l/lhoryn/highd0/user.lhoryn.mc16_13TeV.399046.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_0p01ns.081718_fixedOR_2_trees.root/*.root')
#AODFiles = glob.glob('/afs/cern.ch/work/l/lhoryn/public/displacedLepton/backgrounds/inputFiles/forORStudies.root')
#AODFiles = glob.glob('/eos/user/l/lhoryn/highd0/'+inpfile+'.root')
AODFiles = glob.glob('/afs/cern.ch/work/l/lhoryn/public/displacedLepton/Analysis/run/output/'+inpfile+'.root')
chain = ROOT.TChain("trees_SR_highd0_")

for filename in AODFiles:
	chain.Add(filename)



h={}
'''
h["cutflow"]= ROOT.TH1F("cutflow", "cutflow", 17, 0, 17)
h["dR_mu"] = ROOT.TH1F("deltaR","deltaR",200,0,5)
h["dphi_mu"] = ROOT.TH1F("deltaPhi","deltaPhi",100,-6,6)
h["lead_pt_mu"] = ROOT.TH1F("lead_pt_mu", "lead_pt_mu",100, 0, 1000)
h["lead_d0_mu"] = ROOT.TH1F("lead_d0_mu", "lead_d0_mu",100, -1000, 1000)
h["sublead_d0_mu"] = ROOT.TH1F("sublead_d0_mu", "sublead_d0_mu",100, -1000, 1000)
h["sublead_pt_mu"] = ROOT.TH1F("sublead_pt_mu", "sublead_pt_mu",100, 0, 1000)
h["lead_eta_mu"] = ROOT.TH1F("lead_eta_mu","lead_eta_mu",20,-5,5)
h["sublead_eta_mu"] = ROOT.TH1F("sublead_eta_mu","sublead_eta_mu",20,-5,5)
h["lead_phi_mu"] = ROOT.TH1F("lead_phi_mu","lead_phi_mu",100,-4,4)
h["sublead_phi_mu"] = ROOT.TH1F("sublead_phi_mu","sublead_phi_mu",100,-4,4)
h["lead_pt_el"] = ROOT.TH1F("lead_pt_el", "lead_pt_el",100, 0, 1000)
h["sublead_pt_el"] = ROOT.TH1F("sublead_pt_el", "sublead_pt_el",100, 0, 1000)
h["lead_eta_el"] = ROOT.TH1F("lead_eta_el","lead_eta_el",20,-5,5)
h["sublead_eta_el"] = ROOT.TH1F("sublead_eta_el","sublead_eta_el",20,-5,5)
h["lead_phi_el"] = ROOT.TH1F("lead_phi_el","lead_phi_el",20,-4,4)
h["sublead_phi_el"] = ROOT.TH1F("sublead_phi_el","sublead_phi_el",20,-4,4)
'''
d0bins = 20
d0end = 20
h["lead_rec_d0"] = ROOT.TH1F("lead_rec_d0", "lead_rec_d0",d0bins, 0, d0end)
h["sublead_rec_d0"] = ROOT.TH1F("sublead_rec_d0", "sublead_rec_d0",d0bins, 0, d0end)
h["all_rec_d0"] = ROOT.TH1F("all_rec_d0", "all_rec_d0",d0bins, 0, d0end)
h["lead_true_d0"] = ROOT.TH1F("lead_true_d0", "lead_true_d0",d0bins, 0, d0end)
h["sublead_true_d0"] = ROOT.TH1F("sublead_true_d0", "sublead_true_d0",d0bins, 0, d0end)
h["all_true_d0"] = ROOT.TH1F("all_true_d0", "all_true_d0",d0bins, 0, d0end)
h["lead_res_d0"] = ROOT.TH1F("lead_res_d0", "lead_res_d0",d0bins, -d0end, d0end)
h["sublead_res_d0"] = ROOT.TH1F("sublead_res_d0", "sublead_res_d0",d0bins, -d0end, d0end)
h["all_res_d0"] = ROOT.TH1F("all_res_d0", "all_res_d0",d0bins, -d0end, d0end)

ptbins = 100
ptend = 1000
h["lead_rec_pt"] = ROOT.TH1F("lead_rec_pt", "lead_rec_pt",ptbins, 0, ptend)
h["sublead_rec_pt"] = ROOT.TH1F("sublead_rec_pt", "sublead_rec_pt",ptbins, 0, ptend)
h["all_rec_pt"] = ROOT.TH1F("all_rec_pt", "all_rec_pt",ptbins, 0, ptend)
h["lead_true_pt"] = ROOT.TH1F("lead_true_pt", "lead_true_pt",ptbins, 0, ptend)
h["sublead_true_pt"] = ROOT.TH1F("sublead_true_pt", "sublead_true_pt",ptbins, 0, ptend)
h["all_true_pt"] = ROOT.TH1F("all_true_pt", "all_true_pt",ptbins, 0, ptend)
h["lead_res_pt"] = ROOT.TH1F("lead_res_pt", "lead_res_pt",ptbins, -ptend, ptend)
h["sublead_res_pt"] = ROOT.TH1F("sublead_res_pt", "sublead_res_pt",ptbins, -ptend, ptend)
h["all_res_pt"] = ROOT.TH1F("all_res_pt", "all_res_pt",ptbins, -ptend, ptend)



dRbins = 100
dRend = 1

h["all_trueelejet_dR"] = ROOT.TH1F("all_trueelejet_dR", "all_trueelejet_dR", dRbins, 0, dRend)
h["all_trueeleph_dR"] = ROOT.TH1F("all_trueeleph_dR", "all_trueeleph_dR", dRbins, 0, dRend)
h["all_trueeleele_dR"] = ROOT.TH1F("all_trueeleele_dR", "all_trueeleele_dR", dRbins, 0, dRend)

h["nPassedReco"] = ROOT.TH1F("nPassedReco","nPassedReco",10,0,9)
h["nPassedTrue"] = ROOT.TH1F("nPassedTrue","nPassedTrue",10,0,9)

h["closest_object"] = ROOT.TH1F("closest_object","closest_object",4,0,4)

print "entries ", chain.GetEntries()
for event in chain:
    
    leadingLep = False
    subleadingLep = False
    nLeps = 0
    nPassedTrue = 0
    for e in xrange(len(event.truthLepton_pdgId)): 
        if abs(event.truthLepton_pdgId[e]) == 11: 
            nLeps +=1
    if nLeps >=2: 
        for it in xrange(len(event.truthLepton_pt)):
            if abs(event.truthLepton_pdgId[it]) == 11 and abs(event.truthLepton_parentPdgId[it]) > 100000 and event.truthLepton_pt[it] > PTCUT and abs(event.truthLepton_eta[it]) < ETACUT:
                nPassedTrue += 1
                h["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                h["all_true_pt"].Fill(event.truthLepton_pt[it])
                
                
                jetclosestdR = 999 
                for ij in xrange(len(event.jet_pt)):
                    thisdR = deltaR(event.truthLepton_eta[it], event.jet_eta[ij], event.truthLepton_phi[it], event.jet_phi[ij])
                    if thisdR < jetclosestdR: jetclosestdR = thisdR
                    h["all_trueelejet_dR"].Fill(jetclosestdR)
                
                phclosestdR = 999 
               # for ip in xrange(len(event.photon_pt)):
               #     thisdR = deltaR(event.truthLepton_eta[it], event.photon_eta[ip], event.truthLepton_phi[it], event.photon_phi[ip])
               #     if thisdR < phclosestdR: phclosestdR = thisdR
               #     h["all_trueeleph_dR"].Fill(phclosestdR)

                elclosestdR = 999
                for ite in xrange(len(event.electron_pt)):
                    thisdR = deltaR(event.truthLepton_eta[it], event.electron_eta[ite], event.truthLepton_phi[it], event.electron_phi[ite])
                    if thisdR < elclosestdR: elclosestdR = thisdR
                    h["all_trueeleele_dR"].Fill(elclosestdR)
                
                mindR = min(jetclosestdR, phclosestdR, elclosestdR)
                
                if mindR < 0.5:
                    if mindR == elclosestdR: h["closest_object"].Fill("electron",1)  
                    elif mindR == jetclosestdR: h["closest_object"].Fill("jet",1)  
                    elif mindR == phclosestdR: h["closest_object"].Fill("photon",1)  
                else: 
                    h["closest_object"].Fill("nothing",1)  
                    print "min dr ", mindR
                    print "elec dr ", elclosestdR
                    print "jet dr ", jetclosestdR
                    print "photon dr ", phclosestdR
                    
                    
                if not leadingLep:
                    h["lead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["lead_true_pt"].Fill(event.truthLepton_pt[it])
                    leadingLep = True
                elif not subleadingLep: 
                    h["sublead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["sublead_true_pt"].Fill(event.truthLepton_pt[it])
                    subleadingLep = True

    h["nPassedTrue"].Fill(nPassedTrue)

	#with OR
    leadLep = False
    subleadLep = False
    nPassedReco = 0
    if len(event.electron_pt) >= 2:
        for ie in xrange(len(event.electron_pt)):
            if event.electron_pt[ie] > PTCUT and abs(event.electron_eta[ie] < ETACUT) and event.electron_truthMatchedBarcode[0] != -1:
                barcode = event.electron_truthMatchedBarcode[ie]
                truthd0 = -1
                truthPt = -999
                for ii in xrange(len(event.truthLepton_pt)):
                    if event.truthLepton_barcode[ii] == barcode and abs(event.truthLepton_parentPdgId[ii]) > 100000 and event.truthLepton_pt[ii] > PTCUT and abs(event.truthLepton_eta[ii]) < ETACUT:
                        truthd0 = abs(event.truthLepton_d0[ii])
                        truthPt = event.truthLepton_pt[ii]

                if not truthd0 == -1:
                    nPassedReco += 1
                    h["all_rec_d0"].Fill(truthd0)
                    h["all_rec_pt"].Fill(truthPt)
                    h["all_res_pt"].Fill(truthPt - event.electron_pt[ie])
                    h["all_res_d0"].Fill(truthd0 - event.electron_IDtrack_d0[ie])
                    if not leadLep:
                        h["lead_rec_d0"].Fill(truthd0)
                        h["lead_rec_pt"].Fill(truthPt)
                        h["lead_res_pt"].Fill(truthPt - event.electron_pt[ie])
                        h["lead_res_d0"].Fill(truthd0 - event.electron_IDtrack_d0[ie])
                        leadLep = True
                    elif not subleadLep:
                        h["sublead_rec_d0"].Fill(truthd0)
                        h["sublead_rec_d0"].Fill(truthPt)
                        h["sublead_res_pt"].Fill(truthPt - event.electron_pt[ie])
                        h["sublead_res_d0"].Fill(truthd0 - event.electron_IDtrack_d0[ie])
                        subleadLep = True
        
    h["nPassedReco"].Fill(nPassedReco)

kind = ["lead","sublead","all"]
param = ["d0","pt"]
for k in kind:
    for p in param:
            print "making efficency with " + k + " " + p  
            h[k + "_" + p + "_eff"] = ROOT.TEfficiency(h[k+"_rec_"+p ],h[k+"_true_"+p])
            h[k + "_" + p + "_eff"].SetName(k + "_" + p +"_eff")
                


outFile = ROOT.TFile("outputFiles/"+inpfile+"signal_histos.root", "RECREATE")
for histo in h:
	h[histo].Write()
outFile.Write()

