# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
import math
import glob
import os


PTCUT = 60
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 3

variables = {
    "all_true_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "all true p_{T} [GeV]"},
    "lead_true_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "leading true p_{T} [GeV]"},
    "sublead_true_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "subleading true p_{T} [GeV]"},
    "all_matched_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "all matched p_{T} [GeV]"},
    "lead_matched_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "leading matched p_{T} [GeV]"},
    "sublead_matched_pt": {"xmin": 0., "xmax": 5000., "nbins": 100, "xlab": "subleading matched p_{T} [GeV]"},

    "all_true_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "all true d_{0} [mm]"},
    "lead_true_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "leading true d_{0} [mm]"},
    "sublead_true_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "subleading true d_{0} [mm]"},
    "all_matched_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "all matched d_{0} [mm]"},
    "lead_matched_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "leading matched d_{0} [mm]"},
    "sublead_matched_d0": {"xmin": 0., "xmax": 300., "nbins": 40, "xlab": "subleading matched d_{0} [mm]"},
    
    "idt_d0": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "ID track d_{0} [mm]"},
    "idt_n": {"xmin": 0., "xmax": 100., "nbins": 20, "xlab": "n ID tracks"},
}


def deltaPhi(phi1, phi2):
	dphi = abs(phi1-phi2)
	if (dphi > math.pi):
		dphi = 2.*math.pi - dphi
	return dphi
				       
def deltaR(eta1, eta2, phi1, phi2):
	deltaEta = abs(eta1-eta2)
	deltaPhi = abs(phi1-phi2)
	dR = math.sqrt(deltaPhi*deltaPhi + deltaEta*deltaEta)

	return dR

def goodTrue(i):
    if event.truthLepton_pt[i] < PTCUT: return False
    if event.truthLepton_eta[i] > ETACUT: return False
    if abs(event.truthLepton_parentPdgId[it]) < 100000 : return False

    return True
    

def initializeLepHistos():
    h = {}
    for flav in ["e","m"]: 
        h[flav] = {}
    
        for var in variables:
            hname = "h_%s_%s"%(var, flav)
            h[flav][var] = ROOT.TH1F(hname, hname, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            h[flav][var].SetDirectory(0)

    return h


inpdir = "/eos/user/l/lhoryn/highd0/workingpoints_forsure/"



for inputfile in os.listdir(inpdir):
    fulldir = inpdir + inputfile + "/data-trees/"
    inpfile = os.listdir(fulldir)[0]
    print "Processing ", fulldir+inpfile

    AODFiles = glob.glob(fulldir+inpfile)
    chain = ROOT.TChain("trees_SR_highd0_")

    for filename in AODFiles:
        chain.Add(filename)
    
    h=initializeLepHistos()

    print "entries ", chain.GetEntries()
    for event in chain:
        
        leadingLep = False
        subleadingLep = False
        nElecs = 0
        nMus = 0

        for it in xrange(len(event.truthLepton_pdgId)): 
            if abs(event.truthLepton_pdgId[it]) == 11: nElecs +=1
            if abs(event.truthLepton_pdgId[it]) == 13: nMus +=1
            if abs(event.truthLepton_pdgId[it]) == 11 and goodTrue(it):
                    
                h["e"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                h["e"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                    
                if not leadingLep:
                    h["e"]["lead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["e"]["lead_true_pt"].Fill(event.truthLepton_pt[it])
                    leadingLep = True
                elif not subleadingLep: 
                    h["e"]["sublead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["e"]["sublead_true_pt"].Fill(event.truthLepton_pt[it])
                    subleadingLep = True

            if abs(event.truthLepton_pdgId[it]) == 13 and goodTrue(it):  
                h["m"]["all_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                h["m"]["all_true_pt"].Fill(event.truthLepton_pt[it])
                    
                if not leadingLep:
                    h["m"]["lead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["m"]["lead_true_pt"].Fill(event.truthLepton_pt[it])
                    leadingLep = True
                elif not subleadingLep: 
                    h["m"]["sublead_true_d0"].Fill(abs(event.truthLepton_d0[it]))
                    h["m"]["sublead_true_pt"].Fill(event.truthLepton_pt[it])
                    subleadingLep = True

        leadingLep = False
        subleadingLep = False
        nPassedReco = 0
        for ie in xrange(len(event.electron_pt)):
            if event.electron_truthMatchedBarcode[ie] == -1: continue
            if event.electron_passOR[ie] == 0 : continue
            
            barcode = event.electron_truthMatchedBarcode[ie]
            
            for ii in xrange(len(event.truthLepton_pt)):
                if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):
                    truthd0 = abs(event.truthLepton_d0[ii])
                    truthPt = event.truthLepton_pt[ii]

                    h["e"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                    h["e"]["all_matched_pt"].Fill(event.truthLepton_pt[ii])
                    if not leadingLep:
                        h["e"]["lead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["e"]["lead_matched_pt"].Fill(event.truthLepton_pt[ii])
                        leadingLep = True
                    elif not subleadingLep:
                        h["e"]["sublead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["e"]["sublead_matched_pt"].Fill(event.truthLepton_pt[ii])
                        subleadingLep = True
        
        for im in xrange(len(event.muon_pt)):
            if event.muon_truthMatchedBarcode[im] == -1: continue
            
            barcode = event.muon_truthMatchedBarcode[im]
            
            for ii in xrange(len(event.truthLepton_pt)):
                if event.truthLepton_barcode[ii] == barcode and goodTrue(ii):
                    truthd0 = abs(event.truthLepton_d0[ii])
                    truthPt = event.truthLepton_pt[ii]

                    h["m"]["all_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                    h["m"]["all_matched_pt"].Fill(event.truthLepton_pt[ii])
                    if not leadingLep:
                        h["m"]["lead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["m"]["lead_matched_pt"].Fill(event.truthLepton_pt[ii])
                        leadingLep = True
                    elif not subleadingLep:
                        h["m"]["sublead_matched_d0"].Fill(abs(event.truthLepton_d0[ii]))
                        h["m"]["sublead_matched_pt"].Fill(event.truthLepton_pt[ii])
                        subleadingLep = True
        
    


    kind = ["lead","sublead","all"]
    param = ["d0","pt"]
    flav = ["e","m"]
    for f in flav:
        for k in kind:
            for p in param:
                print "making efficency with " + k + " " + p  
                h[f][k + "_" + p + "_eff"] = ROOT.TEfficiency(h[f][k+"_matched_"+p],h[f][k+"_true_"+p])
                h[f][k + "_" + p + "_eff"].SetName(f + "_" + k + "_" + p + "_eff")
                
    outpfile = "../outputFiles/wpstudies/"+inputfile+"_histos.root"
    outFile = ROOT.TFile(outpfile, "RECREATE")
    
    print "made ", outpfile
    for f in flav:
        for var in h[f]:
            print "writing " + f + " " + var
            if h[f][var]: 
                h[f][var].Write()
    
    outFile.Write()

