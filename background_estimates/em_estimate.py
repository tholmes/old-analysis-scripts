# Goal: Estimate the fake electron contribution to the SR

import math
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/user.lhoryn.data17_13TeV.periodI.physics_Main.DAOD_RPVLL_041619_trees.root/user.lhoryn.17749537._000*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/data_wip/user.lhoryn.data17_13TeV.periodI.physics_Main.081519_DAODRPVLL_v1_trees.root/user.lhoryn.0033*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data_wip/user.lhoryn.data17_13TeV.period[C,D,E,F,G,H,I,K].physics_Main.DAOD_RPVLL_v3.0_trees.root/*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/data*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data1*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/skim/data1*.root"},
        }

# Define selections
#event_selection = "electron_n > 1 && triggerRegion_pass"
#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 3., abs_val=True)

#blinded = True
blinded = False

event_selection = "electron_n_baseline > 0 && muon_n_basline > 0 && triggerRegion_pass && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24"
#event_selection = "electron_n_baseline > 0 && muon_n_basline > 0 && triggerRegion_pass && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24 && (!electron_FCTightTTVA[0] || !muon_FCTightTTVA[0])"
#event_selection = "electron_n_baseline > 0 && muon_n_basline > 0 && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24"
#event_selection += " && lepton_n==2" # Only used while inverting
#event_selection += " && !(electron_FCTight[0] && muon_topoetcone20[0]/muon_pt[0] < 0.15 && muon_ptvarcone20[0]/muon_pt[0] < 0.04)" # Test with one failing FCTight
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_z0", maxval=500, abs_val=True)
e_baseline.addCut("electron_eta", maxval=2.5, abs_val=True)
e_baseline.addCut("electron_pt", 50)
e_baseline.addCut("electron_d0", minval=2., maxval=300, abs_val=True)
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_d0", minval=3., maxval=300, abs_val=True)
#e_baseline.addCut("electron_FCTightTTVA")

#INVERTING
#e_baseline.addCut("electron_chi2", minval=2)
e_baseline.addCut("electron_dpt", maxval=-0.5)
#e_baseline.addCut("electron_FCTight", maxval=0.5)
#e_baseline.addCut("electron_nMissingLayers", minval=1.1)

#REQUIRING
#e_baseline.addCut("electron_chi2", maxval=2)
#e_baseline.addCut("electron_dpt", minval=-0.5)
#e_baseline.addCut("electron_nMissingLayers", maxval=1.1)

#TESTING
e_pass = Selection("electron_pass")
#e_pass.addCut("electron_pt", 65)
#e_pass.addCut("electron_d0", minval=3., maxval=300, abs_val=True)
#e_pass.addCut("electron_dpt", -0.5)
e_pass.addCut("electron_chi2", maxval=2.0)
e_pass.addCut("electron_nMissingLayers", maxval=1.1)
#e_pass.addCut("electron_FCTightTTVA")

m_baseline = Selection("muon_baseline")
m_baseline.addCut("muon_pt", 50)
m_baseline.addCut("muon_IDtrack_d0", minval=2, maxval=300, abs_val=True)
#m_baseline.addCut("muon_pt", 65)
#m_baseline.addCut("muon_IDtrack_d0", minval=3, maxval=300, abs_val=True)
m_baseline.addCut("muon_IDtrack_z0", maxval=500, abs_val=True)
m_baseline.addCut("muon_eta", maxval=2.5, abs_val=True)
m_baseline.addCut("muon_isNotCosmic")
m_baseline.addCut("muon_t0avg", maxval=30, abs_val=True)
#m_baseline.addCut("muon_FCTightTTVA")

#INVERTING
#m_baseline.addCut("muon_CBtrack_chi2", minval=3.0)

#TESTING
m_pass = Selection("muon_pass")
#m_pass.addCut("muon_pt", 65)
#m_pass.addCut("muon_IDtrack_d0", minval=3, maxval=300, abs_val=True)
m_pass.addCut("muon_IDtrack_chi2", maxval=2)
m_pass.addCut("muon_IDtrack_nMissingLayers", maxval=1.1)
m_pass.addCut("muon_CBtrack_chi2", maxval=3.0)
#m_pass.addCut("muon_nPresHits", minval=2.9)
#m_pass.addCut("muon_nPhiLays", minval=0.1)
#m_pass.addCut("muon_FCTightTTVA")

#INVERTING
#m_baseline.addCut("muon_MStrack_nPres", maxval=2.9)

# Additions for validation
# Varibles we're inverting
#e_baseline.addCut("electron_dpt", maxval=-0.5)  # All electrons have dpt outside of SR
#e_pass.addCut("electron_dpt", minval=-0.8)      # Passing electrons are between -0.8 and -0.5
#m_baseline.addCut("muon_CBtrack_chi2", minval=3)# All muons have chi2 > 3
#m_pass.addCut("muon_CBtrack_chi2", maxval=8)    # Passing muons have chi2 between 3 and 8

# Variables we're loosening
#e_pass.addCut("electron_chi2", maxval=4)        # Passing electrons have chi2 > 4
#e_pass.addCut("electron_nMissingLayers", maxval=2.1) # Passing electrons have no more than 2 missing layers
#m_pass.addCut("muon_MStrack_nPres", minval=1.9) # Passing muons have 2 hits

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    t = getTree(samples[s]["fname"])
    print "em selection:", event_selection
    print "e selection:", e_baseline.getCutString(0)
    print "m selection:", m_baseline.getCutString(0)

    cut_str_A = " %s && (%s && %s) && ((%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_B = " %s && (%s && %s) && ((%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_C = " %s && (%s && %s) && (!(%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_D = " %s && (%s && %s) && (!(%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    print cut_str_B

    samples[s]["hists"] = {}
    if not blinded:
        print "\tGetting region A..."
        samples[s]["hists"]["A"] = getHist(t, "h_nl_%s_A"%s, "lepton_n_signal", binstr="1,0.,1000.", sel_name=cut_str_A)
    print "\tGetting region B..."
    samples[s]["hists"]["B"] = getHist(t, "h_nl_%s_B"%s, "lepton_n_signal", binstr="1,0.,1000.", sel_name=cut_str_B)
    print "\tGetting region C..."
    samples[s]["hists"]["C"] = getHist(t, "h_nl_%s_C"%s, "lepton_n_signal", binstr="1,0.,1000.", sel_name=cut_str_C)
    print "\tGetting region D..."
    samples[s]["hists"]["D"] = getHist(t, "h_nl_%s_D"%s, "lepton_n_signal", binstr="1,0.,1000.", sel_name=cut_str_D)

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    if not blinded: tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)

    # Make asymmerrs to handle things correctly
    samples[s]["hists"]["B"].Sumw2()
    samples[s]["hists"]["C"].Sumw2()
    samples[s]["hists"]["D"].Sumw2()

    # Get ratio of C/D with full errors
    g_R_est = ROOT.TGraphAsymmErrors()
    g_R_est.Divide(samples[s]["hists"]["C"], samples[s]["hists"]["D"])
    r = g_R_est.GetPointY(0)
    r_eup = g_R_est.GetErrorYhigh(0)
    r_edw = g_R_est.GetErrorYlow(0)

    # Combine with events in B
    est_A = r*tot_B
    unc_Aup = est_A*math.sqrt(1./tot_B + (r_eup/r)**2)
    unc_Adw = est_A*math.sqrt(1./tot_B + (r_edw/r)**2)

    #print g_A_est.GetN()
    #g_A_est.Draw("alpe")
    #ROOT.gPad.Update()
    #raw_input("...")

    print "From TG:"
    print "Estim in A:", est_A, "+", unc_Aup, "-", unc_Adw

    # Old errors
    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Total in D:", tot_D, "(neither e or m passes)"
    print "Total in C:", tot_C, "(only muon passes)"
    print "Total in B:", tot_B, "(only electron passes)"
    if not blinded: print "Total in A:", tot_A, "(both pass)"
    print "Estim in A:", est_A, "+/-", math.sqrt(1./tot_B + 1./tot_C + 1./tot_D)*est_A

    # Cutflow for fun
    print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


