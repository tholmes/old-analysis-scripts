# Goal: Invert a series of SR requirements and see how much events pile up against that cut

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/user.lhoryn.data17_13TeV.periodI.physics_Main.DAOD_RPVLL_041619_trees.root/user.lhoryn.17749537._000*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/data_wip/user.lhoryn.data17_13TeV.periodI.physics_Main.081519_DAODRPVLL_v1_trees.root/user.lhoryn.0033*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data_wip/user.lhoryn.data17_13TeV.period[C,D,E,F,G,H,I,K].physics_Main.DAOD_RPVLL_v3.0_trees.root/*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/data*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data1*.root"},
        }

# Select events we care about
event_selection = "electron_n > 0 && muon_n > 0 && triggerRegion_pass && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24 && lepton_pt[0]>65 && lepton_pt[1]>65 && abs(lepton_d0[0])>3 && abs(lepton_d0[1])>3"
event_selection += " && lepton_n==2" # Only used while inverting
#event_selection += " && !(electron_FCTight[0] && muon_FCTight[0])" # Test with one failing FCTight


e_cuts = {  "fake_e": "electron_dpt[0] > -0.5 && electron_chi2[0] < 2 && electron_nMissingLayers <= 1",
            "hf_e": "electron_FCTight[0]==1",
            }

m_cuts = {  "fake_m": "muon_CBtrack_chi2[0] < 3 && muon_MStrack_nPres[0] > 2.5",
            "hf_m": "muon_topoetcone20[0]/muon_pt[0] < 0.15 && muon_ptvarcone20[0]/muon_pt[0] < 0.04",
            "chi2_m": "muon_CBtrack_chi2[0] < 3",
            "nprec_m": "muon_MStrack_nPres[0] > 2.5",
            }

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    t = getTree(samples[s]["fname"])

    hists = {}

    for cut in e_cuts:
        for cut2 in m_cuts:
            hname = "h_tag_%s_probe_%s"%(cut, cut2)
            hists[cut, cut2] = getHist(t, hname, m_cuts[cut2], "2,-0.5, 1.5", " %s && !(%s)"%(event_selection,e_cuts[cut]), verbose=True)

    for cut in m_cuts:
        for cut2 in e_cuts:
            hname = "h_tag_%s_probe_%s"%(cut, cut2)
            hists[cut, cut2] = getHist(t, hname, e_cuts[cut2], "2,-0.5, 1.5", " %s && !(%s)"%(event_selection,m_cuts[cut]), verbose=True)

    cans = {}

    for cut in e_cuts:
        cans[cut] = ROOT.TCanvas(cut, cut)

        ymax = 0
        for i, cut2 in enumerate(m_cuts):
            ymax = max(ymax, hists[cut,cut2].GetMaximum())

        for i, cut2 in enumerate(m_cuts):

            hists[cut,cut2].GetXaxis().SetTitle("Passes cut?")
            hists[cut,cut2].SetLineColor(theme_colors[i])
            hists[cut,cut2].SetMarkerColor(theme_colors[i])
            hists[cut,cut2].SetMaximum(1.5*ymax)
            hists[cut,cut2].SetMinimum(0)
            hists[cut,cut2].SetTitle(cut2)

            if i==0: hists[cut,cut2].Draw()
            else: hists[cut,cut2].Draw("same")

        leg = cans[cut].BuildLegend(.6,.65,.8,.90)
        leg.Draw()
        cans[cut].SaveAs("h_tag_%s.pdf"%cut)

    for cut in m_cuts:
        cans[cut] = ROOT.TCanvas(cut, cut)

        ymax = 0
        for i, cut2 in enumerate(e_cuts):
            ymax = max(ymax, hists[cut,cut2].GetMaximum())

        for i, cut2 in enumerate(e_cuts):

            hists[cut,cut2].GetXaxis().SetTitle("Passes cut?")
            hists[cut,cut2].SetLineColor(theme_colors[i])
            hists[cut,cut2].SetMarkerColor(theme_colors[i])
            hists[cut,cut2].SetMaximum(1.5*ymax)
            hists[cut,cut2].SetMinimum(0)
            hists[cut,cut2].SetTitle(cut2)

            if i==0: hists[cut,cut2].Draw()
            else: hists[cut,cut2].Draw("same")

        leg = cans[cut].BuildLegend(.6,.65,.8,.90)
        leg.Draw()
        cans[cut].SaveAs("h_tag_%s.pdf"%cut)



