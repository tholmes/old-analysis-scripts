# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/user.lhoryn.data17_13TeV.periodI.physics_Main.DAOD_RPVLL_041619_trees.root/user.lhoryn.17749537._000*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/data_wip/user.lhoryn.data17_13TeV.periodI.physics_Main.081519_DAODRPVLL_v1_trees.root/user.lhoryn.0033*.root"},
        }

# Define selections
#event_selection = "electron_n > 1 && triggerRegion_pass"
#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 3., abs_val=True)

event_selection = "electron_n > 1 && triggerRegion_pass"
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_passOR")
e_baseline.addCut("electron_pt", 65)
e_baseline.addCut("electron_d0", 3., abs_val=True)
e_baseline.addCut("electron_fcloose")
e_baseline.addCut("electron_chi2", minval=2)
#e_baseline.addCut("electron_dpt", maxval=-0.5)
#e_baseline.addCut("electron_nholes", minval=0.9)

e_pass = Selection("electron_pass")
#e_pass.addCut("electron_dpt", -0.5)
#e_pass.addCut("electron_chi2", maxval=2.0)
#e_pass.addCut("electron_nholes", minval=0.9)
e_pass.addCut("electron_nholes", maxval=0.1)
# For the future, add isolation? Other quality checks?

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])

    # Make basic event selection
    df = samples[s]["initial_rdf"].Filter(event_selection, "ee selection")

    # Define needed variables
    e_fcloose = '''
            std::vector<double> v;
            for (int i=0; i<electron_pt.size(); i++) {
                float fcloose = 0;
                if (electron_topoetcone20[i]/electron_pt[i] < 0.20 && electron_ptvarcone20[i]/electron_pt[i] < 0.15) fcloose = 1;
                v.push_back((double)fcloose);
            }
            return v;
    '''
    e_nholes = '''
        std::vector<float> si_layers{33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554};
        std::vector<int> n_pix_layers{4, 3, 2, 1, 0, 0, 0, 0};
        std::vector<int> n_sct_layers{8, 8, 8, 8, 8, 6, 4, 2};
        std::vector<double> v;
        for (int i=0; i<electron_pt.size(); i++){
            int n_sct_exp = 0;
            int n_pix_exp = 0;
            for (int j=0; j<si_layers.size(); j++)
                if ( electron_RFirstHit[i] < si_layers[j] + 10 ){
                    n_sct_exp = n_sct_layers[j];
                    n_pix_exp = n_pix_layers[j];
                    break;
                }
            int n_sct_missing = std::max( n_sct_exp - electron_nSCT[i], 0 );
            int n_pix_missing = std::max( n_pix_exp - electron_nPIX[i], 0 );
            int n_holes = n_sct_missing + n_pix_missing;
            v.push_back((double)n_holes);
        }
        return v;
    '''
    df_defs = df.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt") \
            .Define("electron_nholes", e_nholes) \
            .Define("electron_fcloose", e_fcloose)

    print "ee selection:", event_selection
    df_baseline = df_defs.Filter(e_baseline.getCutString(0), "e0 baseline").Filter(e_baseline.getCutString(1), "e1 baseline")
    print "e0 selection:", e_baseline.getCutString(0)
    print "e1 selection:", e_baseline.getCutString(1)


    # Separate into categories
    samples[s]["rdfs"] = {}

    # For looking at just one lepton
    # --- Both pass (A)
    cut_str = " %s && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["A"] = df_baseline.Filter(cut_str, "A")
    # --- Lep0 pass (B)
    cut_str = " %s && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["B"] = df_baseline.Filter(cut_str, "B")
    # --- Lep1 pass (C)
    cut_str = " !(%s) && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["C"] = df_baseline.Filter(cut_str, "C")
    # --- None pass (D)
    cut_str = " !(%s) && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["D"] = df_baseline.Filter(cut_str, "D")

    #h = samples[s]["rdfs"]["A"].Histo1D("electron_d0")
    #h.Draw()
    #raw_input("...")

    # Save histograms
    samples[s]["hists"] = {}
    model = ROOT.RDF.TH1DModel("m_ht", "m_ht", 100, 0., 1000.)
    samples[s]["hists"]["A"] = samples[s]["rdfs"]["A"].Histo1D(model, "HT")#, "normweight")
    samples[s]["hists"]["B"] = samples[s]["rdfs"]["B"].Histo1D(model, "HT")#, "normweight")
    samples[s]["hists"]["C"] = samples[s]["rdfs"]["C"].Histo1D(model, "HT")#, "normweight")
    samples[s]["hists"]["D"] = samples[s]["rdfs"]["D"].Histo1D(model, "HT")#, "normweight")

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Estim in A:", est_A
    print "Total in A:", tot_A
    print "Total in B:", tot_B
    print "Total in C:", tot_C
    print "Total in D:", tot_D

    # Cutflow for fun
    print ""
    report = samples[s]["initial_rdf"].Report()
    report.Print()


