# Goal: Estimate the fake electron contribution to the SR

import math
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
samples = {
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/skim/data*.root"},
        }

variables = {
        "pt0":       {"accessor": "electron_pt[0]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Leading Lepton p_{T} [GeV]"},
        #"pt1":       {"accessor": "electron_pt[1]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Subleading Lepton p_{T} [GeV]"},
        #"d00":       {"accessor": "electron_d0[0]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Leading Lepton d_{0} [mm]"},
        #"d01":       {"accessor": "electron_d0[1]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Subleading Lepton d_{0} [mm]"},
        #"eta0":      {"accessor": "electron_eta[0]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Leading Lepton #eta"},
        #"eta1":      {"accessor": "electron_eta[1]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Subleading Lepton #eta"},
        }
ref_var = "pt0"

blinded = True

# Define selections
event_selection = "electron_n_baseline > 0 && muon_n_basline > 0 && triggerRegion_pass && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24"

e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_pt", 50)
e_pass = Selection("electron_pass")
e_pass.addCut("electron_isSignal")
m_baseline = Selection("muon_baseline")
m_baseline.addCut("muon_pt", 50)
m_pass = Selection("muon_pass")
m_pass.addCut("muon_isSignal")
m_pass.addCut("muon_t0avg", maxval=30, abs_val=True)

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])
    samples[s]["rdfs"] = {}
    #df_init = samples[s]["initial_rdf"].Filter("lepton_n_baseline>0")
    df_vars = samples[s]["initial_rdf"].Define('pt0', 'electron_pt[0]')

    # Add flat variables for plotting
    #ex_string = "df_vars = df_init"
    #for var in variables:
    #    ex_string += ".Define('%s', '%s')"%(var, variables[var]["accessor"])
    #print ex_string
    #exec(ex_string)

    # STUDY 2L EVENTS ------------------------------------------------------
    # Make baseline event selection
    df = df_vars.Filter(event_selection, "em selection")

    df_baseline = df.Filter(e_baseline.getCutString(0), "e baseline").Filter(m_baseline.getCutString(0), "m baseline")
    print "em selection:", event_selection
    print "e selection:", e_baseline.getCutString(0)
    print "m selection:", m_baseline.getCutString(0)

    # --- Both pass (A)
    cut_str = " %s && %s " % ( e_pass.getCutString(0), m_pass.getCutString(0) )
    if not blinded: samples[s]["rdfs"]["A"] = df_vars.Filter(cut_str, "A")
    # --- Electron pass (B)
    cut_str = " %s && !(%s) " % ( e_pass.getCutString(0), m_pass.getCutString(0) )
    samples[s]["rdfs"]["B"] = df_vars.Filter(cut_str, "B")
    # --- Muon pass (C)
    cut_str = " !(%s) && %s " % ( e_pass.getCutString(0), m_pass.getCutString(0) )
    samples[s]["rdfs"]["C"] = df_vars.Filter(cut_str, "C")
    # --- None pass (D)
    cut_str = " !(%s) && !(%s) " % ( e_pass.getCutString(0), m_pass.getCutString(0) )
    samples[s]["rdfs"]["D"] = df_vars.Filter(cut_str, "D")

    # STUDY 1L EVENTS ------------------------------------------------------
    #samples[s]["rdfs"]["1e"] = df_vars.Filter("triggerRegion_pass && electron_n_baseline==1 && muon_n_basline==0", "1e").Filter(e_baseline.getCutString(0), "e baseline")
    #samples[s]["rdfs"]["1m"] = df_vars.Filter("triggerRegion_pass && muon_n_basline==1  && electron_n_baseline==0", "1m").Filter(m_baseline.getCutString(0), "m baseline")
    #samples[s]["rdfs"]["1e_sig"] = samples[s]["rdfs"]["1e"].Filter(e_pass.getCutString(0), "e pass")
    #samples[s]["rdfs"]["1m_sig"] = samples[s]["rdfs"]["1m"].Filter(m_pass.getCutString(0), "m pass")

    # Save histograms
    samples[s]["hists"] = {}
    for reg in samples[s]["rdfs"]:
        samples[s]["hists"][reg] = {}
        samples[s]["hists"]["scaled_%s"%reg] = {}

        for var in variables:
            model = ROOT.RDF.TH1DModel(var, var, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            samples[s]["hists"][reg][var] = samples[s]["rdfs"][reg].Histo1D(model, var)

for s in samples:
    print "Working with sample", s

    if not blinded: tot_A = samples[s]["hists"]["A"][ref_var].Integral(0, samples[s]["hists"]["A"][ref_var].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"][ref_var].Integral(0, samples[s]["hists"]["B"][ref_var].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"][ref_var].Integral(0, samples[s]["hists"]["C"][ref_var].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"][ref_var].Integral(0, samples[s]["hists"]["D"][ref_var].GetNbinsX()+1)

    tot_1e = samples[s]["hists"]["1e"][ref_var].Integral(0, samples[s]["hists"]["1e"][ref_var].GetNbinsX()+1)
    tot_1m = samples[s]["hists"]["1m"][ref_var].Integral(0, samples[s]["hists"]["1m"][ref_var].GetNbinsX()+1)
    tot_1e_sig = samples[s]["hists"]["1e_sig"][ref_var].Integral(0, samples[s]["hists"]["1e_sig"][ref_var].GetNbinsX()+1)
    tot_1m_sig = samples[s]["hists"]["1m_sig"][ref_var].Integral(0, samples[s]["hists"]["1m_sig"][ref_var].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D
    unc_A = 0
    if tot_D > 0 and tot_B > 0 and tot_C > 0: unc_A = math.sqrt(1./tot_B + 1./tot_C + 1./tot_D)*est_A

    print "\nResults"
    print "Total in D:", tot_D, "(neither e or m passes)"
    print "Total in C:", tot_C, "(only muon passes)"
    print "Total in B:", tot_B, "(only electron passes)"
    if not blinded: print "Total in A:", tot_A, "(both pass)"
    print "Estim in A:", est_A, "+/-", unc_A

    #pass_ratio_e = tot_1e_sig/(tot_1e - tot_1e_sig)
    #pass_ratio_m = tot_1m_sig/(tot_1m - tot_1m_sig)
    #print ""
    #print "Pass ratio e:", pass_ratio_e
    #print "Pass ratio m:", pass_ratio_m

    #print "\nPrediction from 1L"
    #print tot_D*pass_ratio_e*pass_ratio_m + tot_C*pass_ratio_e + tot_B*pass_ratio_m

