# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/data1*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/datasmall.root"},
        }

# Define selections

event_selection = "muon_n > 1 && triggerRegion_pass && muon_phi[0]*muon_phi[1]>0"
#event_selection += " && lepton_n==2" # Only used while inverting
#event_selection += " && !(electron_FCTight[0] && muon_topoetcone20[0]/muon_pt[0] < 0.15 && muon_ptvarcone20[0]/muon_pt[0] < 0.04)" # Test with one failing FCTight

m_baseline = Selection("muon_baseline")
m_baseline.addCut("muon_pt", 65)
#m_baseline.addCut("muon_IDtrack_d0", minval=3, abs_val=True)
m_baseline.addCut("muon_IDtrack_d0", minval=3, maxval=300, abs_val=True)
m_baseline.addCut("muon_FCTightTTVA")
m_baseline.addCut("muon_IDtrack_z0", maxval=500, abs_val=True)
m_baseline.addCut("!muon_cosmicTag_MV")
#m_baseline.addCut("muon_MStrack_nPres", minval=2.9)
m_baseline.addCut("muon_eta", maxval=2.5, abs_val=True)
m_baseline.addCut("muon_hasMStrack")
m_baseline.addCut("muon_hasCBtrack")
m_baseline.addCut("muon_hasIDtrack")
#m_baseline.addCut("muon_isNotCosmic")

m_pass = Selection("muon_pass")
m_pass.addCut("muon_IDtrack_chi2", maxval=2)
m_pass.addCut("muon_IDtrack_nMissingLayers", maxval=1.1)
m_pass.addCut("muon_CBtrack_chi2", maxval=3.0)
m_pass.addCut("muon_MStrack_nPres", minval=2.9)
# TODO: Add cut on phi layers

# Additions for validation
# Varibles we're inverting
#m_baseline.addCut("muon_FCTightTTVA", maxval=0.5)
#m_baseline.addCut("muon_CBtrack_chi2", minval=3)# All muons have chi2 > 3
#m_pass.addCut("muon_CBtrack_chi2", maxval=8)    # Passing muons have chi2 between 3 and 8

# Variables we're loosening
#m_pass.addCut("muon_MStrack_nPres", minval=1.9) # Passing muons have 2 hits

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    t = getTree(samples[s]["fname"])

    #cut_str_A = " %s && (%s && %s) && ((%s) && (%s)) " % ( event_selection, m_baseline.getCutString(0), m_baseline.getCutString(1), m_pass.getCutString(0), m_pass.getCutString(1) )
    cut_str_B = " %s && (%s && %s) && ((%s) && !(%s)) " % ( event_selection, m_baseline.getCutString(0), m_baseline.getCutString(1), m_pass.getCutString(0), m_pass.getCutString(1) )
    cut_str_C = " %s && (%s && %s) && (!(%s) && (%s)) " % ( event_selection, m_baseline.getCutString(0), m_baseline.getCutString(1), m_pass.getCutString(0), m_pass.getCutString(1) )
    cut_str_D = " %s && (%s && %s) && (!(%s) && !(%s)) " % ( event_selection, m_baseline.getCutString(0), m_baseline.getCutString(1), m_pass.getCutString(0), m_pass.getCutString(1) )
    print cut_str_B

    samples[s]["hists"] = {}
    #print "\tGetting region A..."
    #samples[s]["hists"]["A"] = getHist(t, "h_met_%s_A"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_A)
    print "\tGetting region B..."
    samples[s]["hists"]["B"] = getHist(t, "h_met_%s_B"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_B)
    print "\tGetting region C..."
    samples[s]["hists"]["C"] = getHist(t, "h_met_%s_C"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_C)
    print "\tGetting region D..."
    samples[s]["hists"]["D"] = getHist(t, "h_met_%s_D"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_D)
    #h = getHist(t, "h_phi", "muon_phi[0]:muon_phi[1]", binstr="20,-3.2,3.2,20,-3.2,3.2", sel_name="(%s) || (%s) || (%s)"%(cut_str_B, cut_str_C, cut_str_D))
    h = getHist(t, "h_phi", "muon_isNotCosmic[0]:muon_isNotCosmic[1]", binstr="20,-3.2,3.2,20,-3.2,3.2", sel_name="(%s) || (%s) || (%s)"%(cut_str_B, cut_str_C, cut_str_D))
    h.Draw("colz")
    raw_input("...")

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    #tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Estim in A:", est_A
    #print "Total in A:", tot_A, "(both pass)"
    print "Total in B:", tot_B, "(leading muon passes)"
    print "Total in C:", tot_C, "(subleading muon passes)"
    print "Total in D:", tot_D, "(neither muons pass)"

    # Cutflow for fun
    print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


