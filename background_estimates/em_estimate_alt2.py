# Goal: Estimate the fake electron contribution to the SR

import math
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gStyle.SetErrorX(0.45)
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
samples = {
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/skim/data*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root"},
        }

blinded = True

# Define selections
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_z0", maxval=500, abs_val=True)
e_baseline.addCut("electron_eta", maxval=2.5, abs_val=True)
e_baseline.addCut("electron_pt", 50)
e_baseline.addCut("electron_d0", minval=2., maxval=300, abs_val=True)
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_d0", minval=3., maxval=300, abs_val=True)
e_baseline.addCut("electron_FCTightTTVA")

#TESTING
e_pass = Selection("electron_pass")
e_pass.addCut("electron_pt", 65)
e_pass.addCut("electron_d0", minval=3., maxval=300, abs_val=True)
e_pass.addCut("electron_dpt", -0.5)
e_pass.addCut("electron_chi2", maxval=2.0)
e_pass.addCut("electron_nMissingLayers", maxval=1.1)
#e_pass.addCut("electron_FCTightTTVA")

m_baseline = Selection("muon_baseline")
m_baseline.addCut("muon_pt", 50)
m_baseline.addCut("muon_IDtrack_d0", minval=2, maxval=300, abs_val=True)
#m_baseline.addCut("muon_pt", 65)
#m_baseline.addCut("muon_IDtrack_d0", minval=3, maxval=300, abs_val=True)
m_baseline.addCut("muon_IDtrack_z0", maxval=500, abs_val=True)
m_baseline.addCut("muon_eta", maxval=2.5, abs_val=True)
m_baseline.addCut("muon_isNotCosmic")
m_baseline.addCut("muon_t0avg", maxval=30, abs_val=True)
m_baseline.addCut("muon_FCTightTTVA")

#TESTING
m_pass = Selection("muon_pass")
m_pass.addCut("muon_pt", 65)
m_pass.addCut("muon_IDtrack_d0", minval=3, maxval=300, abs_val=True)
m_pass.addCut("muon_IDtrack_chi2", maxval=2)
m_pass.addCut("muon_IDtrack_nMissingLayers", maxval=1.1)
m_pass.addCut("muon_CBtrack_chi2", maxval=3.0)
m_pass.addCut("muon_nPresHits", minval=2.9)
m_pass.addCut("muon_nPhiLays", minval=0.1)
#m_pass.addCut("muon_FCTightTTVA")

event_selection = "electron_n_baseline > 0 && muon_n_basline > 0 && triggerRegion_pass && (abs(lepton_pdgId[0]) + abs(lepton_pdgId[1]))==24"
e1_selection = "triggerRegion_pass && electron_n_baseline==1 && muon_n_basline==0"
m1_selection = "triggerRegion_pass && muon_n_basline==1  && electron_n_baseline==0"

#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 50)
#e_pass = Selection("electron_pass")
#e_pass.addCut("electron_isSignal")
#m_baseline = Selection("muon_baseline")
#m_baseline.addCut("muon_pt", 50)
#m_pass = Selection("muon_pass")
#m_pass.addCut("muon_isSignal")
#m_pass.addCut("muon_t0avg", maxval=30, abs_val=True)

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    t = getTree(samples[s]["fname"])

    cut_str_A = " %s && (%s && %s) && ((%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_B = " %s && (%s && %s) && ((%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_C = " %s && (%s && %s) && (!(%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_D = " %s && (%s && %s) && (!(%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), m_baseline.getCutString(0), e_pass.getCutString(0), m_pass.getCutString(0) )
    cut_str_1e = " %s && (%s) " % ( e1_selection, e_baseline.getCutString(0))
    cut_str_1m = " %s && (%s) " % ( m1_selection, m_baseline.getCutString(0))
    cut_str_1e_sig = " %s && (%s) && (%s)" % ( e1_selection, e_baseline.getCutString(0), e_pass.getCutString(0))
    cut_str_1m_sig = " %s && (%s) && (%s)" % ( m1_selection, m_baseline.getCutString(0), m_pass.getCutString(0))
    print cut_str_B

    # STUDY 2L EVENTS ------------------------------------------------------
    print "em selection:", event_selection
    print "e selection:", e_baseline.getCutString(0)
    print "m selection:", m_baseline.getCutString(0)

    samples[s]["hists"] = {}
    if not blinded:
        print "\tGetting region A..."
        samples[s]["hists"]["A"] = getHist(t, "h_met_%s_A"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_A)
    print "\tGetting region B..."
    samples[s]["hists"]["B"] = getHist(t, "h_met_%s_B"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_B)
    print "\tGetting region C..."
    samples[s]["hists"]["C"] = getHist(t, "h_met_%s_C"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_C)
    print "\tGetting region D..."
    samples[s]["hists"]["D"] = getHist(t, "h_met_%s_D"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_D)

    # STUDY 1L EVENTS ------------------------------------------------------
    print "\tGetting region 1e..."
    samples[s]["hists"]["1e"] = getHist(t, "h_met_%s_1e"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_1e)
    print "\tGetting region 1m..."
    samples[s]["hists"]["1m"] = getHist(t, "h_met_%s_1m"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_1m)
    print "\tGetting region 1e_sig..."
    samples[s]["hists"]["1e_sig"] = getHist(t, "h_met_%s_1e_sig"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_1e_sig)
    print "\tGetting region 1m_sig..."
    samples[s]["hists"]["1m_sig"] = getHist(t, "h_met_%s_1m_sig"%s, "MET", binstr="1,0.,1000.", sel_name=cut_str_1m_sig)

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    if not blinded: tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)

    tot_1e = samples[s]["hists"]["1e"].Integral(0, samples[s]["hists"]["1e"].GetNbinsX()+1)
    tot_1m = samples[s]["hists"]["1m"].Integral(0, samples[s]["hists"]["1m"].GetNbinsX()+1)
    tot_1e_sig = samples[s]["hists"]["1e_sig"].Integral(0, samples[s]["hists"]["1e_sig"].GetNbinsX()+1)
    tot_1m_sig = samples[s]["hists"]["1m_sig"].Integral(0, samples[s]["hists"]["1m_sig"].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D
    unc_A = 0
    if tot_D > 0 and tot_B > 0 and tot_C > 0: unc_A = math.sqrt(1./tot_B + 1./tot_C + 1./tot_D)*est_A

    print "\nResults"
    print "Total in D:", tot_D, "(neither e or m passes)"
    print "Total in C:", tot_C, "(only muon passes)"
    print "Total in B:", tot_B, "(only electron passes)"
    if not blinded: print "Total in A:", tot_A, "(both pass)"
    print "Estim in A:", est_A, "+/-", unc_A

    print "\nTotal in 1e:", tot_1e
    print "Total in 1m:", tot_1m
    print "Total in 1e_sig:", tot_1e_sig
    print "Total in 1m_sig:", tot_1m_sig

    pass_ratio_e = tot_1e_sig/(tot_1e - tot_1e_sig)
    pass_ratio_m = tot_1m_sig/(tot_1m - tot_1m_sig)
    unc_e = pass_ratio_e * math.sqrt(1./tot_1e_sig +1./(tot_1e - tot_1e_sig))
    unc_m = pass_ratio_m * math.sqrt(1./tot_1m_sig +1./(tot_1m - tot_1m_sig))

    print ""
    print "Pass ratio e:", pass_ratio_e, "+/-", unc_e
    print "Pass ratio m:", pass_ratio_m, "+/-", unc_m

    unc_from_D = 0
    unc_from_C = 0
    unc_from_B = 0
    est_from_D = pass_ratio_e * pass_ratio_m * tot_D
    est_from_C = pass_ratio_e * tot_C
    est_from_B = pass_ratio_m * tot_B
    if tot_D != 0: unc_from_D = est_from_D * math.sqrt( (unc_e/pass_ratio_e)**2 + (unc_m/pass_ratio_m)**2 + 1./tot_D )
    if tot_C != 0: unc_from_C = est_from_C * math.sqrt( (unc_e/pass_ratio_e)**2 + 1./tot_C )
    if tot_B != 0: unc_from_B = est_from_B * math.sqrt( (unc_m/pass_ratio_m)**2 + 1./tot_B )

    print "\nPrediction from 1L"
    print "A = Re * Rm * D"
    print est_from_D, "+/-", unc_from_D
    print "A = Re * C"
    print est_from_C, "+/-", unc_from_C
    print "A = Rm * B"
    print est_from_B, "+/-", unc_from_B

