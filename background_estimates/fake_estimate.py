# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data1*.root"},
        #"photon":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/photon.root"},
        }

variables = {
        "pt0":       {"nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Leading Lepton p_{T} [GeV]"},
        "pt1":       {"nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Subleading Lepton p_{T} [GeV]"},
        "d00":       {"nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Leading Lepton d_{0} [mm]"},
        "d01":       {"nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Subleading Lepton d_{0} [mm]"},
        "eta0":      {"nbins": 10,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Leading Lepton #eta"},
        "eta1":      {"nbins": 10,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Subleading Lepton #eta"},
        }

# Define selections
#event_selection = "electron_n > 1 && triggerRegion_pass"
#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 3., abs_val=True)

#event_selection = "electron_n > 1 && triggerRegion_pass" FIXME
event_selection = "electron_n == 2 && triggerRegion_pass"
#event_selection += " && (electron_topoetcone20[1]/electron_pt[1] > 0.10 || electron_ptvarcone20[1]/electron_pt[1] > 0.10 || electron_topoetcone20[0]/electron_pt[0] > 0.10 || electron_ptvarcone20[0]/electron_pt[1] > 0.10)"
event_selection += " && !(electron_FCTight[0] && electron_FCTight[1])"
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_pt", 50)
e_baseline.addCut("electron_d0", 2., abs_val=True)
#e_baseline.addCut("electron_FCTight") # FIXME
#INVERTING
#e_baseline.addCut("electron_ptvarcone20/electron_pt", minval=0.2)
#e_baseline.addCut("electron_topoetcone20/electron_pt", minval=0.2)
#e_baseline.addCut("electron_chi2", minval=2)
#e_baseline.addCut("electron_dpt", maxval=-0.5)
#e_baseline.addCut("electron_nMissingLayers", minval=1.1)
#REQUIRING
#e_baseline.addCut("electron_chi2", maxval=2)
#e_baseline.addCut("electron_dpt", minval=-0.5)
#e_baseline.addCut("electron_nMissingLayers", maxval=1.1)


e_pass = Selection("electron_pass")
#TESTING
e_pass.addCut("electron_dpt", -0.9) #FIXME 0.5
e_pass.addCut("electron_chi2", maxval=2.0)
e_pass.addCut("electron_nMissingLayers", maxval=1.1)
# For the future, add isolation? Other quality checks?

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    t = getTree(samples[s]["fname"])

    cut_str_A = " %s && (%s && %s) && ((%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), e_baseline.getCutString(1), e_pass.getCutString(0), e_pass.getCutString(1) )
    cut_str_B = " %s && (%s && %s) && ((%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), e_baseline.getCutString(1), e_pass.getCutString(0), e_pass.getCutString(1) )
    cut_str_C = " %s && (%s && %s) && (!(%s) && (%s)) " % ( event_selection, e_baseline.getCutString(0), e_baseline.getCutString(1), e_pass.getCutString(0), e_pass.getCutString(1) )
    cut_str_D = " %s && (%s && %s) && (!(%s) && !(%s)) " % ( event_selection, e_baseline.getCutString(0), e_baseline.getCutString(1), e_pass.getCutString(0), e_pass.getCutString(1) )
    print cut_str_B

    samples[s]["hists"] = {}
    print "\tGetting region A..."
    samples[s]["hists"]["A"] = getHist(t, "Both electrons pass", "electron_pt[0]", binstr="10,0.,1000.", sel_name=cut_str_A)
    print "\tGetting region B..."
    samples[s]["hists"]["B"] = getHist(t, "Leading electron passes", "electron_pt[0]", binstr="10,0.,500.", sel_name=cut_str_B)
    print "\tGetting region C..."
    samples[s]["hists"]["C"] = getHist(t, "Subleading electron passes", "electron_pt[0]", binstr="10,0.,500.", sel_name=cut_str_C)
    print "\tGetting region D..."
    samples[s]["hists"]["D"] = getHist(t, "Neither electron passes", "electron_pt[0]", binstr="10,0.,500.", sel_name=cut_str_D)

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    tot_A = samples[s]["hists"]["A"].Integral(0, samples[s]["hists"]["A"].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"].Integral(0, samples[s]["hists"]["B"].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"].Integral(0, samples[s]["hists"]["C"].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"].Integral(0, samples[s]["hists"]["D"].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Estim in A:", est_A
    print "Total in A:", tot_A
    print "Total in B:", tot_B
    print "Total in C:", tot_C
    print "Total in D:", tot_D

    # Cutflow for fun
    print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


