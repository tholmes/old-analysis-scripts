# Goal: Estimate the fake electron contribution to the SR

import math
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gStyle.SetErrorX(0.45)
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data1*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data17_periodB.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/data18.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4.1_Apr2_NewReco/data18.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/skim/*2L.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/skim/data1*.root"},
        }

variables = {
        "nlep":      {"accessor": "lepton_n_baseline", "nbins": 1, "xmin": 0., "xmax": 1000., "xlabel": "Number of Leptons"},
        "pt0":       {"accessor": "electron_pt[0]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Leading Lepton p_{T} [GeV]"},
        "pt1":       {"accessor": "electron_pt[1]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Subleading Lepton p_{T} [GeV]"},
        "d00":       {"accessor": "electron_d0[0]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Leading Lepton d_{0} [mm]"},
        "d01":       {"accessor": "electron_d0[1]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Subleading Lepton d_{0} [mm]"},
        "eta0":      {"accessor": "electron_eta[0]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Leading Lepton #eta"},
        "eta1":      {"accessor": "electron_eta[1]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Subleading Lepton #eta"},
        }
ref_var = "nlep"

# Define selections
#event_selection = "electron_n > 1 && triggerRegion_pass"
#e_baseline = Selection("electron_baseline")
#e_baseline.addCut("electron_pt", 65)
#e_baseline.addCut("electron_eta", -2.5, 2.5)
#e_baseline.addCut("electron_d0", 3., abs_val=True)

blinded = True

event_selection = "electron_n_baseline > 1 && triggerRegion_pass && deltaR > 0.2"
#event_selection = "electron_n_baseline > 1 && triggerRegion_pass && (!electron_FCTightTTVA[0] || !electron_FCTightTTVA[1])"
e_baseline = Selection("electron_baseline")
e_baseline.addCut("electron_z0", maxval=500, abs_val=True)
e_baseline.addCut("electron_eta", maxval=2.47, abs_val=True)
e_baseline.addCut("electron_pt", 65)
e_baseline.addCut("electron_d0", minval=3., maxval=300, abs_val=True)
e_baseline.addCut("electron_FCTightTTVA")
#e_baseline.addCut("electron_pt", 50)
#e_baseline.addCut("electron_d0", minval=2., maxval=300, abs_val=True)


#INVERTING
#e_baseline.addCut("electron_chi2", minval=2)
#e_baseline.addCut("electron_dpt", maxval=-0.5)
#e_baseline.addCut("electron_nMissingLayers", minval=1.1)
#REQUIRING
#e_baseline.addCut("electron_dpt", minval=-0.5)
#e_baseline.addCut("electron_chi2", maxval=2)
#e_baseline.addCut("electron_nMissingLayers", maxval=1.1)

e_pass = Selection("electron_pass")
#TESTING
#e_pass.addCut("electron_dpt", -0.8) # For HF inversion
e_pass.addCut("electron_dpt", -0.5)
e_pass.addCut("electron_chi2", maxval=2.0)
e_pass.addCut("electron_nMissingLayers", maxval=1.1)
#e_pass.addCut("electron_FCTightTTVA")

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])
    samples[s]["rdfs"] = {}

    # Make baseline event selection
    df = samples[s]["initial_rdf"].Filter(event_selection, "ee selection")
    df_baseline = df.Filter(e_baseline.getCutString(0), "e0 baseline").Filter(e_baseline.getCutString(1), "e1 baseline")
    print "ee selection:", event_selection
    print "e0 selection:", e_baseline.getCutString(0)
    print "e1 selection:", e_baseline.getCutString(1)
    print "e0 pass:", e_pass.getCutString(0)

    # Add flat variables for plotting
    ex_string = "df_vars = df_baseline"
    for var in variables:
        ex_string += ".Define('%s', '%s')"%(var, variables[var]["accessor"])
    print ex_string
    exec(ex_string)

    # --- Both pass (A)
    cut_str = " %s && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    if not blinded: samples[s]["rdfs"]["A"] = df_vars.Filter(cut_str, "A")
    # --- Lep0 pass (B)
    cut_str = " %s && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["B"] = df_vars.Filter(cut_str, "B")
    # --- Lep1 pass (C)
    cut_str = " !(%s) && %s " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["C"] = df_vars.Filter(cut_str, "C")
    # --- None pass (D)
    cut_str = " !(%s) && !(%s) " % ( e_pass.getCutString(0), e_pass.getCutString(1) )
    samples[s]["rdfs"]["D"] = df_vars.Filter(cut_str, "D")

    # Save histograms
    samples[s]["hists"] = {}
    for reg in samples[s]["rdfs"]:
        samples[s]["hists"][reg] = {}
        samples[s]["hists"]["scaled_%s"%reg] = {}

        for var in variables:
            model = ROOT.RDF.TH1DModel(var, var, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            samples[s]["hists"][reg][var] = samples[s]["rdfs"][reg].Histo1D(model, var)

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    if not blinded: tot_A = samples[s]["hists"]["A"][ref_var].Integral(0, samples[s]["hists"]["A"][ref_var].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"][ref_var].Integral(0, samples[s]["hists"]["B"][ref_var].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"][ref_var].Integral(0, samples[s]["hists"]["C"][ref_var].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"][ref_var].Integral(0, samples[s]["hists"]["D"][ref_var].GetNbinsX()+1)

    # Make asymmerrs to handle things correctly
    samples[s]["hists"]["B"][ref_var].Sumw2()
    samples[s]["hists"]["C"][ref_var].Sumw2()
    samples[s]["hists"]["D"][ref_var].Sumw2()

    # Get ratio of C/D with full errors
    g_R_est = ROOT.TGraphAsymmErrors()
    g_R_est.Divide(samples[s]["hists"]["C"][ref_var].Clone(), samples[s]["hists"]["D"][ref_var].Clone())
    r = g_R_est.GetPointY(0)
    r_eup = g_R_est.GetErrorYhigh(0)
    r_edw = g_R_est.GetErrorYlow(0)

    # Combine with events in B
    est_A = r*tot_B
    unc_Aup = est_A*math.sqrt(1./tot_B + (r_eup/r)**2)
    unc_Adw = est_A*math.sqrt(1./tot_B + (r_edw/r)**2)

    #print g_A_est.GetN()
    #g_A_est.Draw("alpe")
    #ROOT.gPad.Update()
    #raw_input("...")

    print "From TG:"
    print "Estim in A:", est_A, "+", unc_Aup, "-", unc_Adw

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Estim in A:", est_A, "+/-", math.sqrt(1./tot_B + 1./tot_C + 1./tot_D)*est_A
    if not blinded: print "Total in A:", tot_A
    print "Total in B:", tot_B
    print "Total in C:", tot_C
    print "Total in D:", tot_D

    # Make plots
    cans = {}
    for var in variables:

        cans[var] = ROOT.TCanvas("can%s"%var, "can%s"%var)
        ymax = 0
        for x, region in enumerate(["B", "C", "D"]):
            samples[s]["hists"]["scaled_%s"%region][var] = samples[s]["hists"][region][var].Clone()
            samples[s]["hists"]["scaled_%s"%region][var].Scale(eval("1./tot_%s"%region))
            samples[s]["hists"]["scaled_%s"%region][var].SetMinimum(0)
            #samples[s]["hists"]["scaled_%s"%region][var].SetMaximum(1.5)
            samples[s]["hists"]["scaled_%s"%region][var].SetMarkerColor(theme_colors[x])
            samples[s]["hists"]["scaled_%s"%region][var].SetLineColor(theme_colors[x])
            #samples[s]["hists"]["scaled_%s"%region][var].SetFillColorAlpha(theme_colors[x], 0.25)
            samples[s]["hists"]["scaled_%s"%region][var].SetMarkerStyle(8)
            samples[s]["hists"]["scaled_%s"%region][var].GetXaxis().SetTitle(variables[var]["xlabel"])
            samples[s]["hists"]["scaled_%s"%region][var].GetYaxis().SetTitle("Arbitrary Units")
            ymax = max(ymax, samples[s]["hists"]["scaled_%s"%region][var].GetMaximum())


        samples[s]["hists"]["scaled_D"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_C"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_B"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_D"][var].SetTitle("Neither electron passes")
        samples[s]["hists"]["scaled_C"][var].SetTitle("Subleading electron passes")
        samples[s]["hists"]["scaled_B"][var].SetTitle("Leading electron passes")

        samples[s]["hists"]["scaled_D"][var].Draw("e")
        samples[s]["hists"]["scaled_C"][var].Draw("e same")
        samples[s]["hists"]["scaled_B"][var].Draw("e same")

        leg = cans[var].BuildLegend(.48,.68,.8,.90)
        leg.Draw()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.80, "Data, %d fb^{-1}"%(round(lumi/1000)))
        text.DrawLatex(0.2,0.75, "ee ABCD estimate")

        #raw_input("...")
        cans[var].SaveAs("plots/region_comparison_%s.pdf"%var)

        makeRatioPlot(samples[s]["hists"]["scaled_B"][var], samples[s]["hists"]["scaled_D"][var], variables[var]["xlabel"], "plots/region_comparison_%s_ratio.pdf"%var, h11=samples[s]["hists"]["scaled_C"][var], rmin=0, rmax=3.5)

    # Cutflow for fun
    print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


