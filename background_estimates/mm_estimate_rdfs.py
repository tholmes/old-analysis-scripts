# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gStyle.SetErrorX(0.45)
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/data1*.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/datasmall.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/skim/data1*.root"},
        }

variables = {
        "pt0":       {"accessor": "muon_pt[0]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Leading Lepton p_{T} [GeV]"},
        #"pt1":       {"accessor": "electron_pt[1]", "nbins": 10,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Subleading Lepton p_{T} [GeV]"},
        #"d00":       {"accessor": "electron_d0[0]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Leading Lepton d_{0} [mm]"},
        #"d01":       {"accessor": "electron_d0[1]", "nbins": 10,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Subleading Lepton d_{0} [mm]"},
        #"eta0":      {"accessor": "electron_eta[0]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Leading Lepton #eta"},
        #"eta1":      {"accessor": "electron_eta[1]", "nbins": 6,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Subleading Lepton #eta"},
        }
ref_var = "pt0"
blinded = True

# Define selections

#event_selection = "muon_n > 1 && triggerRegion_pass && muon_phi[0]*muon_phi[1]>0"# && muon_isNotCosmic[0] && muon_isNotCosmic[1]"
#event_selection = "muon_n > 1 && triggerRegion_pass && muon_isNotCosmic[0] && muon_isNotCosmic[1]"
#event_selection = "muon_n > 1 && muon_isNotCosmic[0] && muon_isNotCosmic[1] && pass_HLT_mu60_0eta105_msonly"
#event_selection = "muon_n_basline > 1 && triggerRegion_pass && muon_isNotCosmic[0] && muon_isNotCosmic[1]"
#event_selection = "muon_n_basline > 1 && triggerRegion_pass && muon_isNotCosmic[0] && muon_isNotCosmic[1]"
#event_selection = "muon_n_basline > 1 && !cosmicEvent"
event_selection = "muon_n_basline > 1 && !cosmicEvent && triggerRegion_pass"
#event_selection += " && lepton_n==2" # Only used while inverting
#event_selection += " && (!muon_FCTightTTVA[0] || !muon_FCTightTTVA[1])"
#event_selection += " && !(electron_FCTight[0] && muon_topoetcone20[0]/muon_pt[0] < 0.15 && muon_ptvarcone20[0]/muon_pt[0] < 0.04)" # Test with one failing FCTight

m_baseline = Selection("muon_baseline")
m_baseline.addCut("muon_pt", 50)
m_baseline.addCut("muon_IDtrack_d0", minval=2, maxval=300, abs_val=True)
m_baseline.addCut("muon_IDtrack_z0", maxval=500, abs_val=True)
m_baseline.addCut("muon_eta", maxval=2.5, abs_val=True)
m_baseline.addCut("muon_hasMStrack")
m_baseline.addCut("muon_hasCBtrack")
m_baseline.addCut("muon_hasIDtrack")
m_baseline.addCut("muon_t0avg", maxval=30, abs_val=True)
m_baseline.addCut("muon_FCTightTTVA")

# REQUIRING
#m_baseline.addCut("muon_IDtrack_chi2", maxval=2)
#m_baseline.addCut("muon_IDtrack_nMissingLayers", maxval=1.1)
#m_baseline.addCut("muon_CBtrack_chi2", maxval=3.0)

#TESTING
m_pass = Selection("muon_pass")
#m_pass.addCut("muon_FCTightTTVA")
m_pass.addCut("muon_IDtrack_chi2", maxval=2)
m_pass.addCut("muon_IDtrack_nMissingLayers", maxval=1.1)
m_pass.addCut("muon_CBtrack_chi2", maxval=3.0)
m_pass.addCut("muon_nPresHits", minval=2.9)
m_pass.addCut("muon_nPhiLays", minval=0.1)

# Additions for validation
# Varibles we're inverting
#m_baseline.addCut("muon_FCTightTTVA", maxval=0.5)
#m_baseline.addCut("muon_CBtrack_chi2", minval=3)# All muons have chi2 > 3
#m_pass.addCut("muon_CBtrack_chi2", maxval=8)    # Passing muons have chi2 between 3 and 8

# Variables we're loosening
#m_pass.addCut("muon_MStrack_nPres", minval=1.9) # Passing muons have 2 hits

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])
    samples[s]["rdfs"] = {}

    # Add cosmic tag


    # Make baseline event selection
    df = samples[s]["initial_rdf"].Filter(event_selection, "mm selection")
    df_baseline = df.Filter(m_baseline.getCutString(0), "m0 baseline").Filter(m_baseline.getCutString(1), "m1 baseline")
    print "mm selection:", event_selection
    print "m0 selection:", m_baseline.getCutString(0)
    print "m1 selection:", m_baseline.getCutString(1)
    print "m0 pass:", m_pass.getCutString(0)
    print "m1 pass:", m_pass.getCutString(1)

    # Add flat variables for plotting
    ex_string = "df_vars = df_baseline"
    for var in variables:
        ex_string += ".Define('%s', '%s')"%(var, variables[var]["accessor"])
    print ex_string
    exec(ex_string)

    # --- Both pass (A)
    cut_str = " %s && %s " % ( m_pass.getCutString(0), m_pass.getCutString(1) )
    if not blinded: samples[s]["rdfs"]["A"] = df_vars.Filter(cut_str, "A")
    # --- Lep0 pass (B)
    cut_str = " %s && !(%s) " % ( m_pass.getCutString(0), m_pass.getCutString(1) )
    samples[s]["rdfs"]["B"] = df_vars.Filter(cut_str, "B")
    # --- Lep1 pass (C)
    cut_str = " !(%s) && %s " % ( m_pass.getCutString(0), m_pass.getCutString(1) )
    samples[s]["rdfs"]["C"] = df_vars.Filter(cut_str, "C")
    # --- None pass (D)
    cut_str = " !(%s) && !(%s) " % ( m_pass.getCutString(0), m_pass.getCutString(1) )
    samples[s]["rdfs"]["D"] = df_vars.Filter(cut_str, "D")

    # Save histograms
    samples[s]["hists"] = {}
    for reg in samples[s]["rdfs"]:
        samples[s]["hists"][reg] = {}
        samples[s]["hists"]["scaled_%s"%reg] = {}

        for var in variables:
            model = ROOT.RDF.TH1DModel(var, var, variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"])
            samples[s]["hists"][reg][var] = samples[s]["rdfs"][reg].Histo1D(model, var)

for s in samples:
    print "Working with sample", s

    # Print out weighted values
    #can = ROOT.TCanvas("can", "can")
    #samples[s]["hists"]["A"].Draw()
    #raw_input("...")

    if not blinded: tot_A = samples[s]["hists"]["A"][ref_var].Integral(0, samples[s]["hists"]["A"][ref_var].GetNbinsX()+1)
    tot_B = samples[s]["hists"]["B"][ref_var].Integral(0, samples[s]["hists"]["B"][ref_var].GetNbinsX()+1)
    tot_C = samples[s]["hists"]["C"][ref_var].Integral(0, samples[s]["hists"]["C"][ref_var].GetNbinsX()+1)
    tot_D = samples[s]["hists"]["D"][ref_var].Integral(0, samples[s]["hists"]["D"][ref_var].GetNbinsX()+1)

    est_A = 0
    if tot_D > 0: est_A = tot_B * tot_C / tot_D

    print "\nResults"
    print "Estim in A:", est_A
    if not blinded: print "Total in A:", tot_A, "(both pass)"
    print "Total in B:", tot_B, "(leading muon passes)"
    print "Total in C:", tot_C, "(subleading muon passes)"
    print "Total in D:", tot_D, "(neither muons pass)"


    # Make plots
    cans = {}
    for var in variables:
        cans[var] = ROOT.TCanvas("can%s"%var, "can%s"%var)
        ymax = 0
        for x, region in enumerate(["B", "C", "D"]):
            samples[s]["hists"]["scaled_%s"%region][var] = samples[s]["hists"][region][var].Clone()
            samples[s]["hists"]["scaled_%s"%region][var].Scale(eval("1./tot_%s"%region))
            samples[s]["hists"]["scaled_%s"%region][var].SetMinimum(0)
            #samples[s]["hists"]["scaled_%s"%region][var].SetMaximum(1.5)
            samples[s]["hists"]["scaled_%s"%region][var].SetMarkerColor(theme_colors[x])
            samples[s]["hists"]["scaled_%s"%region][var].SetLineColor(theme_colors[x])
            #samples[s]["hists"]["scaled_%s"%region][var].SetFillColorAlpha(theme_colors[x], 0.25)
            samples[s]["hists"]["scaled_%s"%region][var].SetMarkerStyle(8)
            samples[s]["hists"]["scaled_%s"%region][var].GetXaxis().SetTitle(variables[var]["xlabel"])
            samples[s]["hists"]["scaled_%s"%region][var].GetYaxis().SetTitle("Arbitrary Units")
            ymax = max(ymax, samples[s]["hists"]["scaled_%s"%region][var].GetMaximum())

        samples[s]["hists"]["scaled_D"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_C"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_B"][var].SetMaximum(2*ymax)
        samples[s]["hists"]["scaled_D"][var].SetTitle("Neither electron passes")
        samples[s]["hists"]["scaled_C"][var].SetTitle("Subleading electron passes")
        samples[s]["hists"]["scaled_B"][var].SetTitle("Leading electron passes")

        samples[s]["hists"]["scaled_D"][var].Draw("e")
        samples[s]["hists"]["scaled_C"][var].Draw("e same")
        samples[s]["hists"]["scaled_B"][var].Draw("e same")

        leg = cans[var].BuildLegend(.48,.68,.8,.90)
        leg.Draw()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.80, "Data, 140 fb^{-1}")
        text.DrawLatex(0.2,0.75, "ee ABCD estimate")

        #raw_input("...")
        cans[var].SaveAs("plots/region_comparison_%s.pdf"%var)

    # Cutflow for fun
    print ""
    #report = samples[s]["initial_rdf"].Report()
    #report.Print()


