
import glob
import ROOT
import os
import math
import signalMap

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/"

#1e, 1m
sr = '1m'

cuts = []

common_cuts = 'lepton_n_baseline == 1 && (pass_HLT_g140_loose||pass_HLT_mu60_0eta105_msonly)'

e1_lrtAcc = 'electron_d0l300[0] && electron_z0l500[0]'

m1_lrtAcc = 'muon_d0l300[0] && muon_z0l500[0]'

# common cuts
cuts.append( common_cuts)

if sr == '1e':
    cuts.append( '&& electron_n_baseline == 1 && electron_etal247[0]')
    cuts.append( ' &&  electron_pt[0] > 65.')
    cuts.append( ' && electron_d0g3[0] ')
    cuts.append( ' && electron_isIsolated[0] ')
    cuts.append( ' && electron_dptgt0p5[0]')
    cuts.append( ' && electron_chi2l2[0] ')
    cuts.append( ' && electron_nmissl2[0]')

elif sr == '1m':
    cuts.append( '&& muon_n_basline == 1')
    cuts.append( '&& muon_n_cosmic == 1 ')
    cuts.append( ' &&  muon_pt[0] > 65. ')
    cuts.append( ' && muon_d0g3[0]')
    cuts.append( ' && muon_FCTightTTVA[0]')
    cuts.append( ' && muon_idchi2[0]')
    cuts.append( ' && muon_idMissLayer[0]')
    cuts.append( ' && muon_npresg2[0] ')
    cuts.append( ' && muon_chi2l3[0] ')
    cuts.append( ' && muon_hasPhiLays[0] ')
    cuts.append( ' && abs(muon_t0avg[0]) < 30 ')

# common cuts

# Hand-done cross-sections [pb], per flavor
lumi = 139000.

# Choose signals
signalList = []

cut_str = ''
for i in range (0, len(cuts)):
    cut_str += cuts[i]
    print cut_str
    outstr = ''
    print d+"data*.root"

    t = getTree(d+"data*.root")


    # get number of events
    sigName = "cut_"+str(i)

    t.Draw('lepton_n_baseline >> %s(2,0,2)'%(sigName), '('+cut_str+')')
    h = ROOT.gDirectory.Get(sigName)

    total = h.GetBinContent(2)
    err = h.GetBinError(2)
    outstr += ' & '+ '%0.3f'%total +' $\pm$ '+ '%0.3f'%err

    print outstr
    

