
import glob
import ROOT
import os

import signalMap

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.gStyle.SetPaintTextFormat(".3f");

signal = 'stau'
# Define grid
lifetimes = ["0p01", "0p1", "1", "10"]
masses = ["50", "100", "200", "300", "400", "500", "600", "700", "800", "900", "1000"]
#lifetimes = ["0p01"]
#masses = ["100"]

# Hand-done cross-sections [pb], per flavor
lumi = 139000.

# Define SRs
#SRs = [ "ee_2mm_50",  "mm_2mm_50",  "ee_3mm_50",  "mm_3mm_50",
#        "ee_2mm_100", "mm_2mm_100", "ee_3mm_100", "mm_3mm_100"]
#SRs = [ "ee_3mm_100", "mm_3mm_100"]
SRs = [ "ee_3mm_65", "mm_3mm_65", "em_3mm_65"]
#SRs = [ "ee_3mm_100", "mm_3mm_100", "em_3mm_100", "ge_3mm_100", "gg_3mm_100"]
#SRs = [ "ee_3mm_100", "ge_3mm_100", "gg_3mm_100" ]
#SRs = [ "ee_3mm_100_50", "mm_3mm_100_50", "em_3mm_100_50", "me_3mm_100_50"]
#SRs = [ "ee_3mm_50", "mm_3mm_50", "em_3mm_50", "me_3mm_50"]
#SRs = [ "ee_3mm_145_100", "mm_3mm_100"]
SR_sels = {}

include_trigger_requirements = True
include_mi_requirements = True
include_idtrack_requirements = True

for sr in SRs:
    sel = Selection(sr)
    e_sel = Selection()
    m_sel = Selection()

    obj0 = ""
    obj1 = ""
    if "ee_" in sr:
        obj0 = "electron_"
        obj1 = "electron_"
    elif sr.startswith("mm_"):
        obj0 = "muon_"
        obj1 = "muon_"
    elif "em_" in sr:
        obj0 = "electron_"
        obj1 = "muon_"
    elif "me_" in sr:
        obj0 = "muon_"
        obj1 = "electron_"
    elif "ge_" in sr:
        obj0 = "photon_"
        obj1 = "electron_"
    elif "eg_" in sr:
        obj0 = "electron_"
        obj1 = "photon_"
    elif "gg_" in sr:
        obj0 = "photon_"
        obj1 = "photon_"

    min_d0 = ""
    if "2mm" in sr: min_d0 = 2
    elif "3mm" in sr: min_d0 = 3

    min_pt = int(sr.split("_")[2])
    min_pt2=min_pt

    if len(sr.split("_"))>3: min_pt2 = int(sr.split("_")[3])

    obj1_index = 0
    if obj1==obj0: obj1_index = 1

    #sel.addCut(obj0+"pt[0]", min_pt)
    #sel.addCut(obj1+"pt[%d]"%obj1_index, min_pt2)
    if obj0 == "electron_":
        #sel.addCut("electron_dpt[0]", -0.5)
        #sel.addCut("electron_chi2[0]", maxval=2.0)
        #sel.addCut("electron_nMissingLayers[0]", maxval=2)
        #sel.addCut("electron_FCTightTTVA[0]")
        #sel.addCut("electron_d0[0]", min_d0, abs_val=True)
        sel.addCut("electron_isSignal[0]")
    if obj0 == "muon_":
        #sel.addCut("muon_cosmicTag_CR[0]")
        #sel.addCut("muon_cosmicTag_VR[0]")
        #sel.addCut("muon_qual_good[0]")
        #sel.addCut("muon_qual_chi2_l3[0]")
        #sel.addCut("muon_qual_nPres_g2[0]")
        sel.addCut("muon_isSignal[0]")
        #sel.addCut("muon_FCTightTTVA[0]")
        #sel.addCut("muon_t0avg[0]", maxval=30, abs_val=True)
        #sel.addCut("muon_isNotCosmic[0]")
        #sel.addCut("muon_topoetcone20[0]/muon_pt[0]<0.15 && muon_ptvarcone20[0]/muon_pt[0]<0.04") #Not quite right, should be 30
        #sel.addCut("muon_IDtrack_d0[0]", min_d0, abs_val=True)
    if obj1 == "electron_":
        #sel.addCut("electron_dpt[%d]"%obj1_index, -0.5)
        #sel.addCut("electron_chi2[%d]"%obj1_index, maxval=2.0)
        #sel.addCut("electron_nMissingLayers[%d]"%obj1_index, maxval=2.0)
        #sel.addCut("electron_FCTightTTVA[%d]"%obj1_index)
        #sel.addCut("electron_d0[%d]"%obj1_index, min_d0, abs_val=True)
        sel.addCut("electron_isSignal[%d]"%obj1_index)
    if obj1 == "muon_":
        #sel.addCut("muon_cosmicTag_CR[%d]"%obj1_index)
        #sel.addCut("muon_cosmicTag_VR[%d]"%obj1_index)
        #sel.addCut("muon_qual_good[%d]"%obj1_index)
        #sel.addCut("muon_qual_chi2_l3[%d]"%obj1_index)
        #sel.addCut("muon_qual_nPres_g2[%d]"%obj1_index)
        #sel.addCut("muon_topoetcone20[%d]/muon_pt[%d]<0.15 && muon_ptvarcone20[%d]/muon_pt[%d]<0.04"%(obj1_index, obj1_index, obj1_index, obj1_index)) #Not quite right, should be 30
        #sel.addCut("muon_isNotCosmic[%d]"%obj1_index)
        sel.addCut("muon_isSignal[%d]"%obj1_index)
        #sel.addCut("muon_FCTightTTVA[%d]"%obj1_index)
        #sel.addCut("muon_IDtrack_d0[%d]"%obj1_index, min_d0, abs_val=True)
        #sel.addCut("muon_t0avg[%d]"%obj1_index, maxval=30, abs_val=True)
    #sel.addCut(obj0+"passOR[0]")
    #sel.addCut(obj1+"passOR[%d]"%obj1_index)

    if include_trigger_requirements:
        sel.addCut("triggerRegion_pass")
        sel.addCut("muon_n_cosmic", maxval=0.1)

    if include_mi_requirements:
        sel.addCut("deltaR", 0.2)
    '''
    if include_idtrack_requirements:
        if obj0 == "muon_":
            sel.addCut("muon_IDtrack_chi2[0]", maxval = 2.)
            sel.addCut("muon_IDtrack_nMissingLayers[0]", maxval = 2)
        if obj1 == "muon_":
            sel.addCut("muon_IDtrack_chi2[%d]"%obj1_index, maxval = 2.)
            sel.addCut("muon_IDtrack_nMissingLayers[%d]"%obj1_index, maxval = 2)
    '''
    SR_sels[sr] = sel
    print "\nAdding selection:"
    print SR_sels[sr].getCutString()

# Set up hists for acceptance
hists = {}
for sr in SRs + ["all"]:
    hists[sr] = {}
    hists[sr]["pass"] =     ROOT.TH2F(sr+"_pass", sr+"_pass",   21, 0, 1050, 4, -2., 2.)
    hists[sr]["total"] =    ROOT.TH2F(sr+"_total", sr+"_total", 21, 0, 1050, 4, -2., 2.)

# Loop over files and make plots
if signal == 'slep':
    append = "_slep_v5p7"
    dsid = ['399026', '399027', '399028', '399030', '399031', '399032', '399034', '399035', '399036', '399038', '399039', '399040', '399042', '399043', '399044', '399046', '399047', '399048', '399050', '399051', '399052', '399054', '399055', '399056', '399057', '399107', '399108', '399109', '399110', '399111', '399112', '399113', '399114', '399115', '399116', '399117', '399118', '399119', '399120', '399121', '399122']
else:
    append = "_stau_v5p7"
    dsid = ['399001', '399002', '399003', '399005', '399006', '399007', '399009', '399010', '399011', '399013', '399014', '399015', '399017', '399018', '399019', '399021', '399022', '399023']

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/"

#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/*SlepSlep*"
#d= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/*StauStau*"
#d =  "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/*StauStau*"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/*SlepSlep*"
#append = "stau_baseline_v3"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/*StauStau*"
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/pre_IDtrk_fix/"
#d = "/eos/user/l/lhoryn/highd0/111918"
#d = "/eos/user/l/lhoryn/highd0/stau/"
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/staus/"
#d = "/eos/user/l/lhoryn/highd0/120318"

weight='lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*LRT_sf*disp_sf*el_reco_sf'

for i in range(0, 1):
    for dn in dsid:

        ds = d+dn+'*.root'

        t = getTree(ds)
        if t==-1: continue

        sigName = signalMap.signals.get(dn)[0]
        m = sigName.split('_0_')[0].split('_')[1]
        mass = int(m)
        lt = sigName.split('_0_')[1].split('ns')[0]

        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1

        print "Working with mass", m, "and lifetime", lt

        # Loop over SRs to get integrals
        # Using eventNumber as an arbitrary event-level variable

        for sr in SRs:
            h_total = getHist(t, "h_ev_%s_%s_%s_total"%(sr,m,lt), "lepton_isSignal[0]", sel_name=weight)
            h_pass = getHist(t, "h_ev_%s_%s_%s_pass"%(sr,m,lt), "lepton_isSignal[0]", sel_name=weight+'*(%s)'%(SR_sels[sr].getCutString()))
            
            hists[sr]["total"].Fill(mass, lifetime, h_total.Integral())
            hists[sr]["pass"].Fill(mass, lifetime, h_pass.Integral())
            print "Passing for", sr, h_pass.Integral()

for sr in SRs + ["all"]:

    print sr

    #TODO: Acceptance only works for "all" now
    if sr == "all":
        hists[sr]["pass"] = hists[SRs[0]]["pass"].Clone("pass_all")
        hists[sr]["total"] = hists[SRs[0]]["total"].Clone("total_all")
        hists[sr]["total"].Draw("text90 same")
        raw_input("...")
        for sub_sr in SRs:
            if sub_sr == SRs[0]: continue
            hists[sr]["pass"].Add(hists[sub_sr]["pass"])
    h = hists[sr]["pass"]

    h.GetXaxis().SetTitle("slepton mass [GeV]")
    h.GetYaxis().SetTitle("slepton lifetime [log10(ns)]")
    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_pass_"+sr, "can_pass_"+sr)
    ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kWhite)
    h_temp.Draw("text90 same")
    #raw_input("...")
    savename = "plots/"+sr+append+"_pass.pdf"
    if include_trigger_requirements and not include_mi_requirements and not include_idtrack_requirements:
        savename = "plots/"+sr+append+"_trig_pass.pdf"
    if include_idtrack_requirements and not include_mi_requirements:
        savename = "plots/"+sr+append+"_idtrack_pass.pdf"
    if include_mi_requirements:
        savename = "plots/"+sr+append+"_dr_pass.pdf"
    can.SaveAs(savename)

    # TODO: this needs to be wrt truth
    hists[sr]["acc"]=hists[sr]["pass"].Clone(sr+"_acc")
    hists[sr]["acc"].Divide(hists[sr]["total"])
    hists[sr]["acc"].GetXaxis().SetTitle("slepton mass [GeV]")
    hists[sr]["acc"].GetYaxis().SetTitle("slepton lifetime [log10(ns)]")

    can = ROOT.TCanvas("can_"+sr, "can_"+sr)
    #ROOT.gPad.SetLogz(0)
    hists[sr]["acc"].SetMinimum(0)
    hists[sr]["acc"].SetMaximum(0.3)
    hists[sr]["acc"].Draw("colz")
    h_temp = hists[sr]["acc"].Clone("text_acc"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kWhite)
    h_temp.Draw("text90 same")
    savename = "plots/"+sr+append+"_acc.pdf"
    if include_trigger_requirements and not include_mi_requirements and not include_idtrack_requirements:
        savename = "plots/"+sr+append+"_trig_acc.pdf"
    if include_idtrack_requirements and not include_mi_requirements:
        savename = "plots/"+sr+append+"_idtrack_acc.pdf"
    if include_mi_requirements:
        savename = "plots/"+sr+append+"_dr_acc.pdf"

    can.SaveAs(savename)



