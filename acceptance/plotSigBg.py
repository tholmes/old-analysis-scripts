
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()

# Define grid
lifetimes = ["0p01", "0p1", "1"]
masses = ["50", "100", "200", "300", "400", "500", "700"]
channels = ["ee", "mm", "true"]

# Define baseline selection
selection = "Length$(OBJ_pt)>1 && OBJ_pt[0]>100 && OBJ_pt[1]>100"

append = "_staus_ae"

# cross-sections [pb], per flavor
lumi = 100000

# Choose variables
variables = {
        "pt":  {"attr": "OBJ_pt",            "nbins": 20,    "xmin": 0.,     "xmax": 1000.,     "xlabel": "lepton p_{T} [GeV]"},
        "d0":  {"attr": "abs(OBJ_d0)",       "nbins": 20,    "xmin": 0.,   "xmax": 100.,       "xlabel": "lepton d_{0} [mm]"},
        "pt_0":  {"attr": "OBJ_pt[0]",            "nbins": 20,    "xmin": 0.,     "xmax": 1000.,     "xlabel": "leading lepton p_{T} [GeV]"},
        "pt_1":  {"attr": "OBJ_pt[1]",            "nbins": 20,    "xmin": 0.,     "xmax": 1000.,     "xlabel": "subleading lepton p_{T} [GeV]"},
        "d0_0":  {"attr": "abs(OBJ_d0[0])",       "nbins": 20,    "xmin": 0.,   "xmax": 100.,       "xlabel": "leading lepton d_{0} [mm]"},
        "d0_1":  {"attr": "abs(OBJ_d0[1])",       "nbins": 20,    "xmin": 0.,   "xmax": 100.,       "xlabel": "subleading lepton d_{0} [mm]"},
        "eta_0": {"attr": "OBJ_eta[0]",           "nbins": 20,    "xmin": -3.,    "xmax": 3.,        "xlabel": "leading lepton #eta"},
        "eta_1": {"attr": "OBJ_eta[1]",           "nbins": 20,    "xmin": -3.,    "xmax": 3.,        "xlabel": "subleading lepton #eta"},
        "phi_0": {"attr": "OBJ_phi[0]",           "nbins": 20,    "xmin": -3.5,   "xmax": 3.5,       "xlabel": "leading lepton #phi"},
        "phi_1": {"attr": "OBJ_phi[1]",           "nbins": 20,    "xmin": -3.5,   "xmax": 3.5,       "xlabel": "subleading lepton #phi"},
        "R_0": {"attr": "OBJ_phi[1]",           "nbins": 20,    "xmin": -3.5,   "xmax": 3.5,       "xlabel": "subleading lepton #phi"},
        }

# Set up histogram map
hists = {}
for lt in lifetimes:
    hists[lt] = {}
    for m in masses:
        hists[lt][m] = {}
        for c in channels:
            hists[lt][m][c] = {}

# Loop over files and make plots
#d = "/eos/user/l/lhoryn/highd0/111918"
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/staus"
d= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/*StauStau*"
for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt
        t = getTree("%s*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        if t==-1: continue

        # Normalize
        ds_name = glob.glob("%s*_%s_*_%sns*_trees.root"%(d,m,lt))[0].split("/")[-1]
        print ds_name
        xs = getXS(ds_name.split(".")[3])
        n_events = getNEvents("%s*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        print xs, n_events
        ev_weight = lumi*2.*xs/n_events

        for c in channels:

            obj_name = "electron"
            if c == "mm": obj_name = "muon"
            elif c == "true": obj_name = "truthLepton"

            sel = "%f * (%s)" % (ev_weight, selection.replace("OBJ",obj_name))
            if c == "true":
                sel = "%s * (%s)"%(sel, "abs(truthLepton_pdgId)<15")

            for var in variables:
                varname = variables[var]["attr"].replace("OBJ",obj_name)
                print varname
                if varname == "abs(muon_d0[0])": varname="abs(muon_IDtrack_d0[0])"
                if varname == "abs(muon_d0[1])": varname="abs(muon_IDtrack_d0[1])"
                if varname == "abs(muon_d0)": varname="abs(muon_IDtrack_d0)"
                hname = "h_%s_%s_%s_%s"%(var,m,lt,c)
                bins = "%d,%f,%f"%(variables[var]["nbins"],variables[var]["xmin"],variables[var]["xmax"])
                hists[lt][m][c][var] = getHist(t, hname, varname, sel_name=sel, binstr=bins)

                #hists[lt][m][c][var].Draw()
                #raw_input("...")

# Draw hists together
for c in channels:
    for var in variables:

        can = ROOT.TCanvas("can_%s"%var, "can_%s"%var)
        leg = ROOT.TLegend(.2,.75,.82,.91)
        leg.SetNColumns(3)
        #leg.SetNColumns(4)
        ROOT.gPad.SetLogy(1)
        for i, lt in enumerate(lifetimes):

            # Make the coloring the same for all lifetimes
            lt_hists = []
            for j, m in enumerate(masses):
                if len(hists[lt][m][c])==0: continue
                lt_hists.append(hists[lt][m][c][var])
                # Identify lifetimes with line styles
                print lt, m, c, var
                hists[lt][m][c][var].SetLineStyle(i+1)
                hists[lt][m][c][var].GetXaxis().SetTitle(variables[var]["xlabel"])
                hists[lt][m][c][var].SetMinimum(.01)
                hists[lt][m][c][var].SetMaximum(10000000)
                if i==0 and j==0:
                    hists[lt][m][c][var].Draw("hist")
                else:
                    hists[lt][m][c][var].Draw("hist same")
                leg.AddEntry(hists[lt][m][c][var], "%s GeV, #tau %s"%(m,lt), "l")

            colorHists(lt_hists)
        leg.Draw()
        can.Update()
        #raw_input("...")
        can.SaveAs("plots/h_%s_%s%s.pdf"%(c,var,append))

