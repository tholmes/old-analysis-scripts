import glob
import ROOT
import os
import math


execfile("../plot_helpers/basic_plotting.py")
execfile("signalMap.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)
#ROOT.TColor.InvertPalette()
#ROOT.TH1.SetDefaultSumw2(1)
ROOT.gStyle.SetPaintTextFormat("#.3g");

NCont = 99
stops = [ 0.0, 0.20, 0.40, 0.60, 0.80, 1.0]

#red = [   (236./255.), (27./255.), (23./255.)  ]
#green = [ (228./255.), (154./255.), (127./255.)]
#blue = [  (241./255.), (170./255.), (140./255.)]

red = [   (236./255.), (33./255.), (23./255.)  ]
green = [ (228./255.), (191./255.), (127./255.)]
blue = [  (241./255.), (212./255.), (140./255.)]
NRGBs = len(red)

#red = [   (236./255.), (27./255.),   (6./255.), (255./255.), (239./255.)  ]
#green = [ (228./255.), (154./255.), (214./255.), (196./255.), (71./255.)]
#blue = [  (241./255.), (170./255.), (160./255.), (61./255.), (111./255.)]

stopsArray = array('d', stops)
redArray = array('d', red)
greenArray = array('d', green)
blueArray = array('d', blue)
ROOT.TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)
ROOT.gStyle.SetNumberContours(NCont)


def format2d(hist):
    hist.GetZaxis().SetTitleSize(0.04);
    hist.GetZaxis().SetLabelSize(0.04);
    hist.GetZaxis().SetTitleOffset(1.4);
    
    hist.GetXaxis().SetTitleSize(0.04);
    hist.GetXaxis().SetTitleOffset(1.4);
    hist.GetXaxis().SetLabelSize(0.04);
    
    hist.GetYaxis().SetTitleSize(0.04);
    hist.GetYaxis().SetTitleOffset(1.4);
    hist.GetYaxis().SetLabelSize(0.04);

def firstLep(event, i_ignore):
    #print i_ignore 
    l0 = -1
    pt0 = -1

    for il in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_pt[il] > pt0 and (abs(event.truthLepton_pdgId[il]) == 11 or abs(event.truthLepton_pdgId[il]) == 13) and il!= i_ignore:
            pt0 = event.truthLepton_pt[il]
            l0 = il

    return l0





reco_weight_t = "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight"

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/stat_ext_new/signal/"

append = "slep"
if 'slep' in append:
    lifetimes = ["0p01", "0p1", "1", "10"]
    lifetimes = ["1"]
    masses = ["50","100","200",  "300", "400", "500","600","700","800","900","1000"]
    masses = ["400"]
else:
    lifetimes = ["0p01", "0p1", "1"]
    #lifetimes = ["1"]
    masses = ["50","100","200",  "300", "400", "500"]
    #masses = ["300", "400", "500"]
    #masses = ["400"]

SRs = ["ee","em","mm"]




# Set up hists for acceptance
hists = {}
for sr in SRs:
    hists[sr] = {}

    if 'slep' in append:
        xmax = 1000 #1050
        nbin = 20 #21
        hists[sr]["accepted"] =     ROOT.TH2F(sr+"_accepted", sr+"_accepted",   nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["selected"] =    ROOT.TH2F(sr+"_selected", sr+"_selected", nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["selected_weight"] =    ROOT.TH2F(sr+"_selected_weight", sr+"_selected_weight", nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["total"] =     ROOT.TH2F(sr+"_total", sr+"_total",   nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["yield"] =     ROOT.TH2F(sr+"_yield", sr+"_yield",   nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["weight"] =     ROOT.TH2F(sr+"_evweight", sr+"_evweight",   nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["valid"] =     ROOT.TH2F(sr+"_valid", sr+"_valid",   nbin, 0, xmax, 5, -2., 3.)
        hists[sr]["valid_yld"] =     ROOT.TH2F(sr+"_valid_yld", sr+"_valid_yld",   nbin, 0, xmax, 5, -2., 3.)
    else:
        xmax = 500 #550
        nbin = 10 #11
        hists[sr]["accepted"] =     ROOT.TH2F(sr+"_pass", sr+"_pass",   nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["selected"] =    ROOT.TH2F(sr+"_total", sr+"_total", nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["selected_weight"] =    ROOT.TH2F(sr+"_selected_weight", sr+"_selected_weight", nbin, 0, xmax, 4, -2., 4.)
        hists[sr]["total"] =     ROOT.TH2F(sr+"_truth", sr+"_truth", nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["yield"] =     ROOT.TH2F(sr+"_yield", sr+"_yield", nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["weight"] =     ROOT.TH2F(sr+"_evweight", sr+"_evweight", nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["valid"] =     ROOT.TH2F(sr+"_valid", sr+"_valid", nbin, 0, xmax, 4, -2., 2.)
        hists[sr]["valid_yld"] =     ROOT.TH2F(sr+"_valid_yld", sr+"_valid_yld", nbin, 0, xmax, 4, -2., 2.)

    hists[sr]["cutflow"] =     ROOT.TH1F(sr+"_cutflow", sr+"_cutflow", 20, 0, 20)
    hists[sr]["pt_pt_accepted"] =     ROOT.TH2F(sr+"_pt_pt_acc", sr+"_pt_pt_acc", 10, 0, 800, 10, 0., 800)
    hists[sr]["pt_pt_total"] =     ROOT.TH2F(sr+"_pt_pt_tot", sr+"_pt_pt_tot", 10, 0, 800, 10, 0., 800)
    hists[sr]["pt_d0_accepted"] =     ROOT.TH2F(sr+"_pt_d0_acc", sr+"_pt_d0_acc", 10, 0, 800, 10, 0, 300)
    hists[sr]["pt_d0_selected"] =     ROOT.TH2F(sr+"_pt_d0_sel", sr+"_pt_d0_sel", 10, 0, 800, 10, 0, 300)
    hists[sr]["pt_d0_total"] =     ROOT.TH2F(sr+"_pt_d0_tot", sr+"_pt_d0_tot", 10, 0, 800, 10, 0, 300)

    hists[sr]["accepted"].Sumw2()
    hists[sr]["selected"].Sumw2()
    hists[sr]["total"].Sumw2()


for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt

        mass = int(m)
        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1
        
        slep_str = "SlepSlep_"
        if "stau" in append: slep_str = "StauStau_"
        sig_str = slep_str + m + "_0_" + lt + "ns"
        print sig_str

         
        try:
            dsid = signals_inv[sig_str]
        except:
            print "no dsid found for ", sig_str
            continue

        fstr = "%s%s*.root"%(d,dsid)
        t = getTree(fstr)  
        if t==-1: continue
        print "Found events:", t.GetEntries()

        maxEntries = t.GetEntries()
        
        filename = glob.glob(fstr)[0]
        xs = getXS(dsid)
        if "slep" in append: xs = xs/2.0
        print xs
        
        
        for sr in SRs:
            sel_string = "acceptEvent"
            if sr == "ee": sel_string = "SRee" 
            if sr == "em": sel_string = "SRem" 
            if sr == "mm": sel_string = "SRmm" 
            h_pass = getHist(t, "h_ev_%s_%s_%s_pass"%(sr,m,lt), "acceptEvent", sel_name="%s*(%s)"%(reco_weight_t,sel_string))
            hists[sr]["yield"].Fill(mass, lifetime, h_pass.Integral())
            
            hists[sr]["weight"].Fill(mass, lifetime, xs*139000)
            


        eventNum = 0
        for event in t:
            
            reco_weight = event.mcEventWeight*event.muSF*event.mu_sel_sf*event.mu_trig_sf*event.disp_sf*event.el_reco_sf*event.pileupWeight*event.lumi_weight/(139000.*xs)
            #if eventNum > 1000: break
            eventNum += 1
            
            ## use all stats in MC for staus, but slepton samples are split into selectron/smuon, so only count the appropriate evetns in the toal
            if "stau" in append:
                for s in SRs :
                    hists[s]["total"].Fill(mass,lifetime)

            if "slep" in append: 
                s  = ""
                # throwaway
                hists["em"]["total"].Fill(mass,lifetime)
                for islep in xrange(len(event.truthSparticle_pdgId)):
                    if abs(event.truthSparticle_pdgId[islep]) == 1000011 or abs(event.truthSparticle_pdgId[islep]) == 2000011:
                    #if abs(event.truthSparticle_pdgId[islep]) == 1000011:
                        s = "ee"
                        break
                    if abs(event.truthSparticle_pdgId[islep]) == 1000013 or abs(event.truthSparticle_pdgId[islep]) == 2000013:
                        s = "mm"
                        break
                if s == "":
                    #print "alert!!"
                    continue
                hists[s]["total"].Fill(mass,lifetime)
            
            ##### efficiency numerator 
            sr = ""
            if event.SRmm: sr = "mm"
            if event.SRee: sr = "ee" 
            if event.SRem: sr = "em"
            if sr != "":
                hists[sr]["selected"].Fill(mass,lifetime,reco_weight)
                hists[sr]["selected_weight"].Fill(mass,lifetime,reco_weight*event.lumi_weight*event.mcEventWeight)
                #hists["all"]["selected"].Fill(mass,lifetime,reco_weight)
                
                
                
            sr = ""

            il0 = -1
            il1 = -1

            if len(event.truthLepton_pt) < 2 : continue

            ## define signal regions based on the two highest pt electrons/muons
            il0 = firstLep(event, -1) 
            il1 = firstLep(event, il0) 
            
            
            if il0 == -1 or il1 == -1: continue
           
            ## in practice, there is only one different flavor region, SR-em (which will be collapsed and is only separated to make the logic in this script easier) 
            if abs(event.truthLepton_pdgId[il0]) == 11 and abs(event.truthLepton_pdgId[il1]) == 11: sr = "ee"
            if abs(event.truthLepton_pdgId[il0]) == 13 and abs(event.truthLepton_pdgId[il1]) == 13: sr = "mm"
            if abs(event.truthLepton_pdgId[il0]) == 11 and abs(event.truthLepton_pdgId[il1]) == 13: sr = "em"
            if abs(event.truthLepton_pdgId[il0]) == 13 and abs(event.truthLepton_pdgId[il1]) == 11: sr = "me"
            

            ## collapse SRem and SRme for histogram filling
            srf = sr
            if sr == "me": srf = "em"
            

            if mass == 400 and lifetime == 0:
                ## pt pt acceptance denominator  
                hists[srf]["pt_pt_total"].Fill(event.truthLepton_pt[il0], event.truthLepton_pt[il1], 1)
                #hists["all"]["pt_pt_total"].Fill(event.truthLepton_pt[il0], event.truthLepton_pt[il1], 1)
                
                hists[srf]["pt_d0_total"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), 1)
                hists[srf]["pt_d0_total"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), 1)
                #hists["all"]["pt_d0_total"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), 1)
                #hists["all"]["pt_d0_total"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), 1)
                
                ## pt d0 efficiency numerator 
                if (event.SRee and srf == "ee") or (event.SRmm and srf == "mm") or (event.SRem and srf == "em"):
                    hists[srf]["pt_d0_selected"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), reco_weight)
                    hists[srf]["pt_d0_selected"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), reco_weight)
                    #hists["all"]["pt_d0_selected"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), reco_weight)
                    #hists["all"]["pt_d0_selected"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), reco_weight)
            
            
            ##### now calculate acceptance
            
            # Different eta cuts for electrons and muons -- based on atlas recommendations
            if (sr == "ee" or sr == "em") and abs(event.truthLepton_eta[il0]) > 2.47: continue 
            if (sr == "ee" or sr == "me") and abs(event.truthLepton_eta[il1]) > 2.47: continue 
            if (sr == "mm" or sr == "me") and abs(event.truthLepton_eta[il0]) > 2.5: continue 
            if (sr == "mm" or sr == "em") and abs(event.truthLepton_eta[il1]) > 2.5: continue 
            

            
            ## signal pt and d0 cuts 
            if event.truthLepton_pt[il0] < 65 or event.truthLepton_pt[il1] < 65: continue

            if abs(event.truthLepton_d0[il0]) < 3 or abs(event.truthLepton_d0[il1]) < 3: continue

           

            ## triggers are applied depending on the kinematics of the event
            ##   the pt cuts are higher than those in the trigger to match the cuts used in 
            ##   the filters that determine which events get reconstructed with special tracking
            ##                  - HLT_g140_loose if there is an electron with pt > 160 GeV 
            ##                  - HLT_2g50_loose if there are 2 electrons each with pt > 60 GeV 
            ##                  - HLT_m60_0eta105_msonly if there is a muon with pt > 60 GeV and |eta| < 1.07
            ## Events must fall into one of these regions to be accepted, and then the appropriate trigger must pass for the event to be selected 
            tr = False
            if (sr == "ee" or sr == "em") and event.truthLepton_pt[il0] > 160: tr = True
            if (sr == "ee" or sr == "me") and event.truthLepton_pt[il1] > 160: tr = True
            if sr == "ee" and event.truthLepton_pt[il0] > 60 and event.truthLepton_pt[il1] > 60: tr = True
            if (sr == "mm" or sr == "me") and (event.truthLepton_pt[il0] > 60 and abs(event.truthLepton_eta[il0]) < 1.07): tr = True
            if (sr == "mm" or sr == "em") and (event.truthLepton_pt[il1] > 60 and abs(event.truthLepton_eta[il1]) < 1.07): tr = True
            if not tr: continue 
            


            ## delta R cut between two leptons to minimize material interactions
            lep0 = ROOT.TVector3()
            lep0.SetPtEtaPhi(event.truthLepton_pt[il0],event.truthLepton_eta[il0], event.truthLepton_phi[il0])
            lep1 = ROOT.TVector3()
            lep1.SetPtEtaPhi(event.truthLepton_pt[il1], event.truthLepton_eta[il1], event.truthLepton_phi[il1])
            
            if abs(lep0.DeltaR(lep1)) < 0.2: continue

            

            if sr == "em" or sr == "me": sr = "em"
            
            ## acceptance numerator/reco denominator 
            hists[srf]["accepted"].Fill(mass,lifetime, event.mcEventWeight)
            #hists["all"]["accepted"].Fill(mass,lifetime, event.mcEventWeight)
            
            
            if mass == 400 and lifetime == 0:
                ## pt pt acceptance numerator
                hists[srf]["pt_pt_accepted"].Fill(event.truthLepton_pt[il0], event.truthLepton_pt[il1], event.mcEventWeight)
                #hists["all"]["pt_pt_accepted"].Fill(event.truthLepton_pt[il0], event.truthLepton_pt[il1], event.mcEventWeight)
                
                ## pt d0 efficency denominator
                hists[srf]["pt_d0_accepted"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), event.mcEventWeight)
                hists[srf]["pt_d0_accepted"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), event.mcEventWeight)
                #hists["all"]["pt_d0_accepted"].Fill(event.truthLepton_pt[il0], abs(event.truthLepton_d0[il0]), event.mcEventWeight)
                #hists["all"]["pt_d0_accepted"].Fill(event.truthLepton_pt[il1], abs(event.truthLepton_d0[il1]), event.mcEventWeight)



c = ROOT.TCanvas()

for sr in SRs:

    sr_string = "all SRs"
    if sr == "ee": sr_string = "SR-ee"
    elif sr == "em": sr_string = "SR-e#mu"
    elif sr == "mm": sr_string = "SR-#mu#mu"

    slep_string = "all sleptons"
    if "stau" in append: slep_string = "#tilde{#tau}"
    elif "slep" in append and sr == "ee": slep_string = "#tilde{e}"
    elif "slep" in append  and sr == "mm": slep_string = "#tilde{#mu}"
    
    print "sr", sr
    print "acc numerator", hists[sr]["accepted"].GetBinContent(3,1) 
    print "acc denominator", hists[sr]["total"].GetBinContent(3,1) 
    ### make acceptance
    hists[sr]["acc"] = hists[sr]["accepted"].Clone(sr+"_acc")
    hists[sr]["acc"].Divide(hists[sr]["accepted"], hists[sr]["total"], 1, 1, "b")
    hists[sr]["acc"].GetXaxis().SetTitle("Mass [GeV]")
    hists[sr]["acc"].GetYaxis().SetTitle("Lifetime [log_{10}(ns)]")
    hists[sr]["acc"].GetZaxis().SetTitle("Acceptance")
    print "acc value", hists[sr]["acc"].GetBinContent(3,1) 
    
    
    for m in masses:
        for lt in lifetimes:
            mass = int(m)
            if lt == "0p01": lifetime = -2
            elif lt == "0p1": lifetime = -1
            elif lt == "1": lifetime = 0
            elif lt == "10": lifetime = 1
        
        #should be the same bin for everything
            binx = hists[sr]["acc"].GetXaxis().FindBin(mass)
            biny = hists[sr]["acc"].GetYaxis().FindBin(lifetime)

            acc = hists[sr]["acc"].GetBinContent(binx,biny)
            err = hists[sr]["acc"].GetBinError(binx,biny)

            print "mass: {:<5s} lifetime: {:<5s} acceptance: {:<3.6f} error: {:<3.6f}".format(m, lt, acc, err)
            #print "mass: {:<5s} lifetime: {:<5s} acceptance: {:<3.6f} error: {:<3.6f}".format(m, lt, 20., 5.)
    
     
    format2d(hists[sr]["acc"])
    hists[sr]["acc"].Draw("colz")
    h_temp = hists[sr]["acc"].Clone("text_acc"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.89, "Simulation Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.85, slep_string + ": " + sr_string)
    text2 = ROOT.TLatex()
    text2.SetNDC()
    text2.SetTextSize(0.04)
    text2.DrawLatex(0.2,0.81, "#sqrt{s} = 13 TeV")
    
    c.SaveAs('plots/'+append+'_acc_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_acc_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_acc_'+sr+'.root')
    c.SaveAs('plots/'+append+'_acc_'+sr+'.C')
    c.Clear()
    
    ## divide out weight
    #hists[sr]["selected"].Divide(hists[sr]["selected"], hists[sr]["weight"],1,1,"b")
    
    print "eff numerator", hists[sr]["selected"].GetBinContent(3,1) 
    print "eff denominator", hists[sr]["accepted"].GetBinContent(3,1) 
    ## make efficiency
    hists[sr]["eff"] = hists[sr]["selected"].Clone(sr+"_eff")
    hists[sr]["eff"].Divide(hists[sr]["selected"], hists[sr]["acc"],1,1,"b")
    hists[sr]["eff"].GetXaxis().SetTitle("Mass [GeV]")
    hists[sr]["eff"].GetYaxis().SetTitle("Lifetime [log_{10}(ns)]")
    hists[sr]["eff"].GetZaxis().SetTitle("Efficiency")
    format2d(hists[sr]["eff"])
    print "eff value", hists[sr]["eff"].GetBinContent(3,1) 
     
    hists[sr]["eff"].Draw("colz")
    h_temp = hists[sr]["eff"].Clone("text_eff"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.89, "Simulation Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.85, slep_string + ": " + sr_string)
    text2 = ROOT.TLatex()
    text2.SetNDC()
    text2.SetTextSize(0.04)
    text2.DrawLatex(0.2,0.81, "#sqrt{s} = 13 TeV")
    
    c.SaveAs('plots/'+append+'_eff_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_eff_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_eff_'+sr+'.root')
    c.SaveAs('plots/'+append+'_eff_'+sr+'.C')
    c.Clear() 

    

    ## ptpt acceptance
    hists[sr]["ptpt_acc"] = hists[sr]["pt_pt_accepted"].Clone(sr+"_ptptacc")
    hists[sr]["ptpt_acc"].Divide(hists[sr]["pt_pt_total"])
    hists[sr]["ptpt_acc"].GetXaxis().SetTitle("leading lepton p_{T} [GeV]")
    hists[sr]["ptpt_acc"].GetYaxis().SetTitle("subleading lepton p_{T} [GeV]")
    #hists[sr]["ptpt_acc"].GetYaxis().SetRangeUser(0,1200)
    #hists[sr]["ptpt_acc"].GetXaxis().SetRangeUser(0,1200)
    hists[sr]["ptpt_acc"].GetZaxis().SetTitle("Acceptance")
    hists[sr]["ptpt_acc"].SetMaximum(1)
    
    format2d(hists[sr]["ptpt_acc"])
    hists[sr]["ptpt_acc"].Draw("colz")

    ROOT.ATLASLabel(0.20,0.88, "Simulation Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.83, slep_string+ " in " + sr_string +  ": m(#tilde{l}) = 400 GeV, #tau(#tilde{l}) = 1 ns")
    
    text2 = ROOT.TLatex()
    text2.SetNDC()
    text2.SetTextSize(0.04)
    text2.DrawLatex(0.2,0.79, "#sqrt{s} = 13 TeV")
    
    c.SaveAs('plots/'+append+'_ptpt_acc_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_ptpt_acc_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_ptpt_acc_'+sr+'.root')
    c.SaveAs('plots/'+append+'_ptpt_acc_'+sr+'.C')
    c.Clear()
    
    '''     
    hists[sr]["accepted"].Draw("colz")    
    h_temp = hists[sr]["accepted"].Clone("text_accepted"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")
    c.SaveAs('plots/accepted_'+sr+'.pdf')
    
    
    hists[sr]["total"].Draw("colz")    
    h_temp = hists[sr]["total"].Clone("text_total"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")
    c.SaveAs('plots/total_'+sr+'.pdf')
    
    hists[sr]["selected"].Draw("colz")    
    h_temp = hists[sr]["selected"].Clone("text_selected"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")
    c.SaveAs('plots/selected_'+sr+'.pdf')

    hists[sr]["selected_weight"].Draw("colz")    
    h_temp = hists[sr]["selected_weight"].Clone("text_selected_weight"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")
    c.SaveAs('plots/selected_weight_'+sr+'.pdf')
    
    hists[sr]["cutflow"].Draw("hist")    
    c.SaveAs('plots/cutflow_'+sr+'.pdf')
    
    ## event yields
    hists[sr]["yield"].Draw("colz")
    h_temp = hists[sr]["yield"].Clone("text_yield"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.88, "Simulation")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.2,0.82, slep_string + sr_string  )
    
    c.SaveAs('plots/'+append+'_yield_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_yield_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_yield_'+sr+'.root')
    c.Clear()

    ## event weights
    hists[sr]["weight"].Draw("colz")
    h_temp = hists[sr]["weight"].Clone("text_weight"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.90, "Simulation")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.2,0.85, slep_string + sr_string  )
    
    c.SaveAs('plots/'+append+'_weight_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_weight_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_weight_'+sr+'.root')
    c.Clear()
    
### do validation
for sr in SRs:
    for lt in lifetimes:
        for m in masses:

        #    print "Working with mass", m, "and lifetime", lt

            mass = int(m)
            if lt == "0p01": lifetime = -2
            elif lt == "0p1": lifetime = -1
            elif lt == "1": lifetime = 0
            elif lt == "10": lifetime = 1
        
        #should be the same bin for everything
            binx = hists[sr]["acc"].GetXaxis().FindBin(mass)
            biny = hists[sr]["acc"].GetYaxis().FindBin(lifetime)

            acc = hists[sr]["acc"].GetBinContent(binx,biny)
            eff = hists[sr]["eff"].GetBinContent(binx,biny)
            norm = hists[sr]["weight"].GetBinContent(binx,biny)
            tot = hists[sr]["total"].GetBinContent(binx,biny)
            yld = hists[sr]["yield"].GetBinContent(binx,biny)
            

        #    print binx,biny
        #    print "acc",acc
        #    print "eff",eff
        #    print "tot",tot
             
            calc = norm*acc*eff
            perc = 100
            if yld!=0: perc = (yld-calc)/yld * 100
             
            hists[sr]["valid"].Fill(mass, lifetime, perc)
            hists[sr]["valid_yld"].Fill(mass, lifetime, calc)

    c.Clear()
    ## make plot
    
    sr_string = " all SRs "
    if sr == "ee": sr_string = " SR-ee "
    elif sr == "em": sr_string = " SR-e#mu "
    elif sr == "mm": sr_string = " SR-#mu#mu "

    slep_string = "all sleptons"
    if "stau" in append: slep_string = "#tilde{#tau}"
    elif "slep" in append and sr == "ee": slep_string = "#tilde{e}"
    elif "slep" in append  and sr == "mm": slep_string = "#tilde{#mu}"
    
    hists[sr]["valid"].GetXaxis().SetTitle("Mass [GeV]")
    hists[sr]["valid"].GetYaxis().SetTitle("Lifetime [log10(ns)]")
    hists[sr]["valid"].GetZaxis().SetTitle("% difference")
    format2d(hists[sr]["valid"])
     
    hists[sr]["valid"].Draw("colz")
    h_temp = hists[sr]["valid"].Clone("text_valid"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.90, "Simulation")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.2,0.85, slep_string + sr_string  )

    c.SaveAs('plots/'+append+'_valid_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_valid_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_valid_'+sr+'.root')
            

    c.Clear()
    ## make plot
    hists[sr]["valid_yld"].GetXaxis().SetTitle("Mass [GeV]")
    hists[sr]["valid_yld"].GetYaxis().SetTitle("Lifetime [log10(ns)]")
    hists[sr]["valid_yld"].GetZaxis().SetTitle("calculated yield")
    format2d(hists[sr]["valid"])
     
    hists[sr]["valid_yld"].Draw("colz")
    h_temp = hists[sr]["valid_yld"].Clone("text_valid_yld"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    h_temp.Draw("text90 same")

    ROOT.ATLASLabel(0.20,0.90, "Simulation")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.2,0.85, slep_string + sr_string  )

    c.SaveAs('plots/'+append+'_valid_yld_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_valid_yld_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_valid_yld_'+sr+'.root')
    '''





