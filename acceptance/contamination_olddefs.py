# Goal: Make plots of photon-related electron backgrounds compared to signal

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

variables = {
        "pt":       {"nbins": 25,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Signal Lepton p_{T} [GeV]"},
        "d0":       {"nbins": 100,    "xmin": -50.,   "xmax": 50.,       "xlabel": "Signal Lepton d_{0} [mm]"},
        "eta":      {"nbins": 20,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Signal Lepton #eta"},
        "phi":      {"nbins": 20,    "xmin": -3.2,   "xmax": 3.2,       "xlabel": "Signal Lepton #phi"},
        "rhit":     {"nbins": 200,    "xmin": 0.,   "xmax": 600.,       "xlabel": "Radius of First Hit"},
        "npix":     {"nbins": 6,    "xmin": 0.,   "xmax": 6.,       "xlabel": "Number of Pixel Hits"},
        "nsct":     {"nbins": 16,    "xmin": 0.,   "xmax": 16.,       "xlabel": "Number of SCT Hits"},
        "nsi":      {"nbins": 22,    "xmin": 0.,   "xmax": 22.,       "xlabel": "Number of Silicon Hits"},
        "nholes":   {"nbins": 12,    "xmin": -0.5,   "xmax": 12.5,     "xlabel": "Number of missing hits after first hit"},
        "dpt":      {"nbins": 20,    "xmin": -1.,   "xmax": 4,          "xlabel": "Signal Lepton (p_{T}^{track}-p_{T})/p_{T}"},
        "chi2":     {"nbins": 20,    "xmin": 0.,   "xmax": 10,          "xlabel": "Signal Lepton #chi^{2}"},
        "fcloose":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCLoose"},
        }

snames = {
        "periodI":          "Data, 2017 Period I",
        "photon":           "Photons, p_{T} > 70 GeV, BFilter",
        "SlepSlep*100*0p1": "Sleptons, m = 100, #tau=0.1 ns",
        "StauStau*100*0p1": "Staus, m = 100, #tau=0.1 ns",
        "StauStau*300*0p1": "Staus, m = 300, #tau=0.1 ns",
        "SlepSlep*300*1ns": "Sleptons, m = 300, #tau=1 ns",
        "SlepSlep*500*0p1": "Sleptons, m = 500, #tau=0.1 ns",
        }

def getVarHist(df, var, varname, sample_label, var2 = "", varname2 = ""):
    if sample_label == "photon":
        weight_branch = "weight_m"
        if varname.startswith("e"): weight_branch = "weight_e"
        if var2=="":
            h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname, weight_branch)
        else:
            h = df.Histo2D((sample_label+varname+varname2, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"], variables[var2]["nbins"], variables[var2]["xmin"], variables[var2]["xmax"]), varname, varname2, weight_branch)
    else:
        if var2=="":
            h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname)
        else:
            h = df.Histo2D((sample_label+varname+varname2, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"], variables[var2]["nbins"], variables[var2]["xmin"], variables[var2]["xmax"]), varname, varname2)
        h.Scale(samples[sample_label]["weight"])
    h.GetXaxis().SetTitle(variables[var]["xlabel"])
    if var2!="": h.GetYaxis().SetTitle(variables[var2]["xlabel"])
    else: h.GetYaxis().SetTitle("Events")
    return h

# Settings
lumi = 5280.591500 # Period I, 2017
signalstau_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"
signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"
photon_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/photons"
#ttbar_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/ttbar"
data_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/data_wip"
#data_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/082019_photon_studies"

#signalstau_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal"
#signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal"
#photon_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/"
#ttbar_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/ttbar"
#data_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data_wip/merge_test/Data"




# Select signals for comparison
signals = ["SlepSlep*100*0p1", "StauStau*100*0p1", "SlepSlep*300*1ns", "SlepSlep*500*0p1", "StauStau*300*0p1"]
#signals = ["SlepSlep*100*0p1"]
photons = []
photons = ["photon"]
#data = []
data = ["periodI"]

# Collect samples and weights
samples = {}
for samplename in signals+photons+data:
    # Get filename
    base_name = ""  # To be used for getting xs
    file_names = [] # To be used for collecting all samples
    for d in [data_d, signalstau_d, signal_d, photon_d]:
        #TODO use the directories correctly
        sets = glob.glob("%s/*%s*"%(d,samplename))
        if len(sets)>=1:
            base_name = sets[0].split("/")[-1]
            file_names = sets
            print "Found", len(file_names), "samples(s):\n\t", file_names
            break
    if base_name == "":
        print "Couldn't find file for sample", samplename
        exit()

    # Normalize
    event_weight = 1
    if samplename in signals:
        xs = getXS(base_name.split(".")[3])
        n_events = 0
        for f in file_names:
            n_events += getNEvents(f+"/*.root")
        event_weight = lumi*xs/n_events

    # Add to tree
    is_signal = False
    if samplename in signals: is_signal = True
    is_data = False
    if samplename in data: is_data = True
    samples[samplename] = {"files": file_names, "weight": event_weight, "hists": {}, "2dhists": {}, "is_signal": is_signal, "is_data": is_data}

# Define selection
include_filter_cuts = True

#append = "pt65_d03"
#append = "pt65_d03_dptcut"
append = "pt65_d03_dptcut_chi2cut_hitcut"
if include_filter_cuts: append += "_filters"

# Define selections
CRs = {
        "e_only": "e_sig_n == 1 && (e_prompt_n + m_sig_n + m_prompt_n) == 0 && triggerRegion_pass",
        #"m_only": "m_sig_n == 1 && (e_prompt_n + e_sig_n + m_prompt_n) == 0 && triggerRegion_pass",
        #"e_prompte": "e_sig_n ==1 && e_prompt_n == 1 && (m_sig_n + m_prompt_n) == 0 && triggerRegion_pass",
        #"e_promptm": "e_sig_n ==1 && m_prompt_n == 1 && (m_sig_n + e_prompt_n) == 0 && triggerRegion_pass",
        #"m_prompte": "m_sig_n ==1 && e_prompt_n == 1 && (e_sig_n + m_prompt_n) == 0 && triggerRegion_pass",
        #"m_promptm": "m_sig_n ==1 && m_prompt_n == 1 && (e_sig_n + e_prompt_n) == 0 && triggerRegion_pass",
        }

e_sig = Selection("e_sig")
e_sig.addCut("electron_pt", 65)
e_sig.addCut("electron_passOR")
e_sig.addCut("electron_d0", 3, abs_val=True)
e_sig.addCut("electron_dpt", -0.5)
e_sig.addCut("electron_chi2", maxval=2.0)
e_sig.addCut("electron_nholes", maxval=1.1)
if include_filter_cuts:
    e_sig.addCut("electron_pt", 160)
e_sig_def = e_sig.getCutString()
print "\nSelection for a signal electron:", e_sig_def

e_prompt = Selection("e_prompt")
e_prompt.addCut("electron_pt", 65)
e_prompt.addCut("electron_passOR")
e_prompt.addCut("electron_d0", maxval=0.1, abs_val=True)
e_prompt.addCut("electron_dpt", -0.5)
e_prompt.addCut("electron_chi2", maxval=2.0)
e_prompt.addCut("electron_nholes", maxval=1.1)
e_prompt_def = e_prompt.getCutString()
print "\nSelection for a prompt electron:", e_prompt_def

m_sig = Selection("m_sig")
m_sig.addCut("muon_pt", 65)
m_sig.addCut("muon_passOR")
m_sig.addCut("muon_IDtrack_d0", 3, abs_val=True)
if include_filter_cuts:
    m_sig.addCut("muon_pt", 80)
    m_sig.addCut("muon_eta", maxval=1.08, abs_val=True)
m_sig_def = m_sig.getCutString()
print "\nSelection for a signal muon:", m_sig_def

m_prompt = Selection("m_prompt")
m_prompt.addCut("muon_pt", 65)
m_prompt.addCut("muon_passOR")
m_prompt.addCut("muon_IDtrack_d0", maxval=0.1, abs_val=True)
m_prompt_def = m_prompt.getCutString()
print "\nSelection for a prompt muon:", m_prompt_def

for s in samples:

    print "\nWorking with sample", s

    # Get dataframe
    t = ROOT.TChain("trees_SR_highd0_")
    if s == "photon":
        for f in samples[s]["files"]: t.Add(f)
    else:
        for f in samples[s]["files"]: t.Add(f+"/*.root")
    dframe = ROOT.ROOT.RDataFrame(t)
    print "Found events:", t.GetEntries()

    # Define selection variables
    #print e_sig.nSelectedString("electron_pt")
    #print ""
    #print e_sig.selectedVectorString("electron_pt")
    #print ""
    #print m_sig.selectedVectorString("muon_IDtrack_d0")

    e_nholes = '''
        std::vector<float> si_layers{33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554};
        std::vector<int> n_pix_layers{4, 3, 2, 1, 0, 0, 0, 0};
        std::vector<int> n_sct_layers{8, 8, 8, 8, 8, 6, 4, 2};
        std::vector<double> v;
        for (int i=0; i<electron_pt.size(); i++){
            int n_sct_exp = 0;
            int n_pix_exp = 0;
            for (int j=0; j<si_layers.size(); j++)
                if ( electron_RFirstHit[i] < si_layers[j] + 10 ){
                    n_sct_exp = n_sct_layers[j];
                    n_pix_exp = n_pix_layers[j];
                    break;
                }
            int n_sct_missing = std::max( n_sct_exp - electron_nSCT[i], 0 );
            int n_pix_missing = std::max( n_pix_exp - electron_nPIX[i], 0 );
            int n_holes = n_sct_missing + n_pix_missing;
            v.push_back((double)n_holes);
        }
        return v;
    '''

    df_dpt = dframe.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt") \
                   .Define("electron_nholes", e_nholes)

    df_defs = df_dpt.Define("e_sig_n", e_sig.nSelectedString("electron_pt")) \
                    .Define("e_prompt_n", e_prompt.nSelectedString("electron_pt")) \
                    .Define("m_sig_n", m_sig.nSelectedString("muon_pt")) \
                    .Define("m_prompt_n", m_prompt.nSelectedString("muon_pt"))
    df_lep = df_defs.Define("e_sig_pt", e_sig.selectedVectorString("electron_pt")) \
                    .Define("m_sig_pt", m_sig.selectedVectorString("muon_pt")) \
                    .Define("e_sig_eta", e_sig.selectedVectorString("electron_eta")) \
                    .Define("m_sig_eta", m_sig.selectedVectorString("muon_eta")) \
                    .Define("e_sig_phi", e_sig.selectedVectorString("electron_phi")) \
                    .Define("m_sig_phi", m_sig.selectedVectorString("muon_phi")) \
                    .Define("e_sig_d0", e_sig.selectedVectorString("electron_d0")) \
                    .Define("m_sig_d0", m_sig.selectedVectorString("muon_IDtrack_d0")) \
                    .Define("e_sig_dpt", e_sig.selectedVectorString("electron_dpt")) \
                    .Define("e_sig_chi2", e_sig.selectedVectorString("electron_chi2")) \
                    .Define("e_sig_topoetcone20", e_sig.selectedVectorString("electron_topoetcone20")) \
                    .Define("e_sig_ptvarcone20", e_sig.selectedVectorString("electron_ptvarcone20")) \
                    .Define("m_sig_topoetcone20", e_sig.selectedVectorString("muon_topoetcone20")) \
                    .Define("m_sig_ptvarcone30", e_sig.selectedVectorString("muon_ptvarcone30"))

    df_tracks = df_lep.Define("e_sig_rhit", e_sig.selectedVectorString("electron_RFirstHit")) \
                    .Define("e_sig_npix", e_sig.selectedVectorString("electron_nPIX")) \
                    .Define("e_sig_nsct", e_sig.selectedVectorString("electron_nSCT")) \
                    .Define("e_sig_nsi", e_sig.selectedVectorString("electron_nSi")) \
                    .Define("m_sig_rhit", m_sig.selectedVectorString("muon_IDtrack_RFirstHit")) \
                    .Define("m_sig_npix", m_sig.selectedVectorString("muon_IDtrack_nPIX")) \
                    .Define("m_sig_nsct", m_sig.selectedVectorString("muon_IDtrack_nSCT")) \
                    .Define("m_sig_nsi", m_sig.selectedVectorString("muon_IDtrack_nSi"))

    e_sig_nholes = '''
        std::vector<float> si_layers{33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554};
        std::vector<int> n_pix_layers{4, 3, 2, 1, 0, 0, 0, 0};
        std::vector<int> n_sct_layers{8, 8, 8, 8, 8, 6, 4, 2};
        std::vector<double> v;
        for (int i=0; i<e_sig_pt.size(); i++){
            auto npix = e_sig_npix[i];
            int n_sct_exp = 0;
            int n_pix_exp = 0;
            for (int j=0; j<si_layers.size(); j++)
                if ( e_sig_rhit[i] < si_layers[j] + 10 ){
                    n_sct_exp = n_sct_layers[j];
                    n_pix_exp = n_pix_layers[j];
                    break;
                }
            int n_sct_missing = std::max( n_sct_exp - e_sig_nsct[i], 0 );
            int n_pix_missing = std::max( n_pix_exp - e_sig_npix[i], 0 );
            int n_holes = n_sct_missing + n_pix_missing;
            v.push_back((double)n_holes);
        }
        return v;
    '''
   e_fcloose = '''
            std::vector<double> v;
            for (int i=0; i<e_sig_pt.size(); i++) {
                float fcloose = 0;
                if (e_sig_topoetcone20[i]/e_sig_pt[i] < 0.20 && e_sig_ptvarcone20[i]/e_sig_pt[i] < 0.15) fcloose = 1;
                v.push_back((double)fcloose);
            }
            return v;
    '''
    m_fcloose = '''
            std::vector<double> v;
            for (int i=0; i<m_sig_pt.size(); i++) {
                float fcloose = 0;
                if (m_sig_topoetcone20[i]/m_sig_pt[i] < 0.30 && m_sig_ptvarcone30[i]/m_sig_pt[i] < 0.15) fcloose = 1;
                v.push_back((double)fcloose);
            }
            return v;
    '''
    m_sig_nholes = e_sig_nholes.replace("e_sig", "m_sig")
    df_hits = df_tracks.Define("e_sig_nholes", e_sig_nholes).Define("m_sig_nholes", m_sig_nholes).Define("e_sig_fcloose", e_fcloose).Define("m_sig_fcloose", m_fcloose)

    # Do stupid vectorizing of event weights for photons
    df_weights = None
    if s == "photon":
        e_weight_str = '''
            std::vector<float> weights;
            for (int i=0; i<%s.size(); i++)
                weights.push_back(normweight*%f);
            return weights;'''%("e_sig_pt", lumi)
        m_weight_str = e_weight_str.replace("e_sig_pt", "m_sig_pt")
        df_weights = df_hits.Define("weight_e", e_weight_str).Define("weight_m", m_weight_str)
    else: df_weights = df_hits

    # Do basic event skimming
    dfs = {}
    for cr in CRs:
        samples[s]["hists"][cr] = {}
        samples[s]["2dhists"][cr] = {}
        dfs[cr] = df_weights.Filter(CRs[cr], cr)

        # Make plots of basic variables
        if cr.startswith("e"):
            samples[s]["hists"][cr]["e_pt"] = getVarHist(dfs[cr], "pt", "e_sig_pt", s)
            samples[s]["hists"][cr]["e_eta"] = getVarHist(dfs[cr], "eta", "e_sig_eta", s)
            samples[s]["hists"][cr]["e_phi"] = getVarHist(dfs[cr], "phi", "e_sig_phi", s)
            samples[s]["hists"][cr]["e_d0"] = getVarHist(dfs[cr], "d0", "e_sig_d0", s)
            samples[s]["hists"][cr]["e_rhit"] = getVarHist(dfs[cr], "rhit", "e_sig_rhit", s)
            samples[s]["hists"][cr]["e_npix"] = getVarHist(dfs[cr], "npix", "e_sig_npix", s)
            samples[s]["hists"][cr]["e_nsct"] = getVarHist(dfs[cr], "nsct", "e_sig_nsct", s)
            samples[s]["hists"][cr]["e_nsi"] = getVarHist(dfs[cr], "nsi", "e_sig_nsi", s)
            samples[s]["hists"][cr]["e_nholes"] = getVarHist(dfs[cr], "nholes", "e_sig_nholes", s)
            samples[s]["hists"][cr]["e_fcloose"] = getVarHist(dfs[cr], "fcloose", "e_sig_fcloose", s)
            samples[s]["hists"][cr]["e_dpt"] = getVarHist(dfs[cr], "dpt", "e_sig_dpt", s)
            samples[s]["hists"][cr]["e_chi2"] = getVarHist(dfs[cr], "chi2", "e_sig_chi2", s)

            samples[s]["2dhists"][cr]["e_rhit_npix"] = getVarHist(dfs[cr], "rhit", "e_sig_rhit", s, "npix", "e_sig_npix")
            samples[s]["2dhists"][cr]["e_rhit_nsct"] = getVarHist(dfs[cr], "rhit", "e_sig_rhit", s, "nsct", "e_sig_nsct")
            samples[s]["2dhists"][cr]["e_rhit_nsi"] = getVarHist(dfs[cr], "rhit", "e_sig_rhit", s, "nsi", "e_sig_nsi")
            samples[s]["2dhists"][cr]["e_rhit_eta"] = getVarHist(dfs[cr], "rhit", "e_sig_rhit", s, "eta", "e_sig_eta")

        else:
            samples[s]["hists"][cr]["m_pt"] = getVarHist(dfs[cr], "pt", "m_sig_pt", s)
            samples[s]["hists"][cr]["m_eta"] = getVarHist(dfs[cr], "eta", "m_sig_eta", s)
            samples[s]["hists"][cr]["m_phi"] = getVarHist(dfs[cr], "phi", "m_sig_phi", s)
            samples[s]["hists"][cr]["m_d0"] = getVarHist(dfs[cr], "d0", "m_sig_d0", s)
            samples[s]["hists"][cr]["m_rhit"] = getVarHist(dfs[cr], "rhit", "m_sig_rhit", s)
            samples[s]["hists"][cr]["m_npix"] = getVarHist(dfs[cr], "npix", "m_sig_npix", s)
            samples[s]["hists"][cr]["m_nsct"] = getVarHist(dfs[cr], "nsct", "m_sig_nsct", s)
            samples[s]["hists"][cr]["m_nsi"] = getVarHist(dfs[cr], "nsi", "m_sig_nsi", s)
            samples[s]["hists"][cr]["m_nholes"] = getVarHist(dfs[cr], "nholes", "m_sig_nholes", s)
            samples[s]["hists"][cr]["m_fcloose"] = getVarHist(dfs[cr], "fcloose", "m_sig_fcloose", s)

            samples[s]["2dhists"][cr]["m_rhit_npix"] = getVarHist(dfs[cr], "rhit", "m_sig_rhit", s, "npix", "m_sig_npix")
            samples[s]["2dhists"][cr]["m_rhit_nsct"] = getVarHist(dfs[cr], "rhit", "m_sig_rhit", s, "nsct", "m_sig_nsct")
            samples[s]["2dhists"][cr]["m_rhit_nsi"] = getVarHist(dfs[cr], "rhit", "m_sig_rhit", s, "nsi", "m_sig_nsi")
            samples[s]["2dhists"][cr]["m_rhit_eta"] = getVarHist(dfs[cr], "rhit", "m_sig_rhit", s, "eta", "m_sig_eta")

    # Cutflow for fun
    report = dframe.Report()
    report.Print()

# Collect hists
for cr in CRs:
    hists = samples[s]["hists"][cr].keys() # This is embarrassing, but also always filled
    cans = {}
    for htype in hists:
        print "htype:", htype
        cans[htype] = ROOT.TCanvas(htype, htype)

        y_max = 0
        for s in samples:
            y_max = max(y_max, samples[s]["hists"][cr][htype].GetMaximum())
            if samples[s]["is_signal"]:
                samples[s]["hists"][cr][htype].SetMarkerStyle(4)

        for i, s in enumerate(samples):
            if i==0:
                samples[s]["hists"][cr][htype].SetMaximum(10**12)
                samples[s]["hists"][cr][htype].SetMinimum(10**-4)
                #samples[s]["hists"][cr][htype].SetMaximum(10000*y_max)
                samples[s]["hists"][cr][htype].Draw("e PLC PMC")
            else:
                if samples[s]["is_data"]:
                    samples[s]["hists"][cr][htype].Draw("same e")
                else:
                    samples[s]["hists"][cr][htype].Draw("same e PLC PMC")

        leg = cans[htype].BuildLegend(.5,.65,.8,.90)
        leg.Draw()

        cans[htype].SetLogy()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.78, "2017 Period I, %.2f fb^{-1}"%(lumi*0.001))

        #raw_input("...")
        cans[htype].SaveAs("plots/%s_%s_%s.pdf"%(cr, htype, append))

    hists = samples[s]["2dhists"][cr].keys()
    for htype in hists:
        for s in samples:
            cans[htype+s] = ROOT.TCanvas(htype+s, htype+s)
            samples[s]["2dhists"][cr][htype].Draw("colz")
            cans[htype+s].SaveAs("plots/%s_%s_%s_%s.pdf"%(cr, htype, s, append))

    # Try to estimate signal contamination
    hists = samples[s]["hists"][cr].keys() # This is embarrassing, but also always filled
    h_data = samples["periodI"]["hists"][cr][hists[0]]
    h_slep100 = samples["SlepSlep*100*0p1"]["hists"][cr][hists[0]]
    h_slep500 = samples["SlepSlep*500*0p1"]["hists"][cr][hists[0]]
    #print type(h_data)
    #print h_data
    h_slep100_frac = h_slep100.Clone().Rebin(h_slep100.GetNbinsX())
    h_slep100_frac.Divide(h_data.Clone().Rebin(h_data.GetNbinsX()))
    h_slep500_frac = h_slep500.Clone().Rebin(h_slep500.GetNbinsX())
    h_slep500_frac.Divide(h_data.Clone().Rebin(h_data.GetNbinsX()))

    n_data = h_data.Integral(0, h_data.GetNbinsX()+1)
    n_slep100 = h_slep100.Integral(0, h_slep100.GetNbinsX()+1)
    n_slep500 = h_slep500.Integral(0, h_slep500.GetNbinsX()+1)
    e_slep100 = ROOT.Double(0)
    e_slep500 = ROOT.Double(0)
    f_slep100 = h_slep100_frac.IntegralAndError(0, h_slep100_frac.GetNbinsX()+1, e_slep100)
    f_slep500 = h_slep500_frac.IntegralAndError(0, h_slep500_frac.GetNbinsX()+1, e_slep500)

    print ""
    print "In CR", cr
    print "\tTotal data in period I:  ", n_data
    print "\tTotal in 100 GeV, 0.1 ns: %.1f \tFraction signal: %.3f +/- %.3f"%(n_slep100, f_slep100, e_slep100)
    print "\tTotal in 500 GeV, 0.1 ns: %.1f \tFraction signal: %.3f +/- %.3f"%(n_slep500, f_slep500, e_slep500)
    #print "\tTotal in 100 GeV, tau = 0.1 ns:", n_slep100, "Fraction signal:", f_slep100, "+/-", e_slep100
    #print "\tTotal in 500 GeV, tau = 0.1 ns:", n_slep500, "Fraction signal:", f_slep500, "+/-", e_slep500
