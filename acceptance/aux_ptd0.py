import glob
import ROOT
import os
import math


execfile("../plot_helpers/basic_plotting.py")
execfile("signalMap.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBeach)
#ROOT.TColor.InvertPalette()
#ROOT.TH1.SetDefaultSumw2(1)
ROOT.gStyle.SetPaintTextFormat("#.3g");

NCont = 99
stops = [ 0.0, 0.20, 0.40, 0.60, 0.80, 1.0]

#red = [   (236./255.), (27./255.), (23./255.)  ]
#green = [ (228./255.), (154./255.), (127./255.)]
#blue = [  (241./255.), (170./255.), (140./255.)]

red = [   (236./255.), (33./255.), (23./255.)  ]
green = [ (228./255.), (191./255.), (127./255.)]
blue = [  (241./255.), (212./255.), (140./255.)]
NRGBs = len(red)

#red = [   (236./255.), (27./255.),   (6./255.), (255./255.), (239./255.)  ]
#green = [ (228./255.), (154./255.), (214./255.), (196./255.), (71./255.)]
#blue = [  (241./255.), (170./255.), (160./255.), (61./255.), (111./255.)]

stopsArray = array('d', stops)
redArray = array('d', red)
greenArray = array('d', green)
blueArray = array('d', blue)
ROOT.TColor.CreateGradientColorTable(NRGBs, stopsArray, redArray, greenArray, blueArray, NCont)
ROOT.gStyle.SetNumberContours(NCont)


sf_file = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/ElectronSelectionSFs.root")
direc = sf_file.Get("VeryLooseLLH2017_nod0_Smooth_iso")
direc.cd()
h_sf = direc.Get("SF_CentralValue_VeryLooseLLH2017_nod0_Smooth_iso")
h_err = direc.Get("SF_TotalError_VeryLooseLLH2017_nod0_Smooth_iso")
h_eff = direc.Get("EffMC_CentralValue_VeryLooseLLH2017_nod0_Smooth_iso")
maxpt = h_sf.GetXaxis().GetBinLowEdge(h_sf.GetXaxis().GetNbins()+1)

def getElSF(event, i):
    eta = event.lepton_eta[i]
    et = event.lepton_pt[i]*1000
    if et > maxpt: et = maxpt-100

    bin_x = h_sf.GetXaxis().FindBin(et)
    bin_y = h_sf.GetYaxis().FindBin(eta)
    val = h_sf.GetBinContent(bin_x, bin_y)
    return val

mu_sfs = {
        "a": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_a.root",
        "d": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_d.root",
        "e": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_e.root",
        }

mu_sf_hists = {}
for year in mu_sfs:
    print mu_sfs[year]
    f = ROOT.TFile.Open(mu_sfs[year], "READ")
    mu_sf_hists[year] = {}
    mu_sf_hists[year]["sf"]=f.Get("h_sf")
    mu_sf_hists[year]["sf"].SetDirectory(0)

def getMuSF(event, i, year):
    phi = event.lepton_phi[i]
    eta = event.lepton_eta[i]
    bin_n = mu_sf_hists[year]["sf"].FindBin(eta,phi)
    val = mu_sf_hists[year]["sf"].GetBinContent(bin_n)
    return val

def format2d(hist):
    hist.GetZaxis().SetTitleSize(0.04);
    hist.GetZaxis().SetTitleOffset(1.4);
    hist.GetXaxis().SetTitleSize(0.04);
    hist.GetXaxis().SetTitleOffset(1.4);
    hist.GetYaxis().SetTitleSize(0.04);
    hist.GetYaxis().SetTitleOffset(1.4);
    hist.GetXaxis().SetLabelSize(0.04);
    hist.GetYaxis().SetLabelSize(0.04);

def firstLep(event, i_ignore):
    #print i_ignore 
    l0 = -1
    pt0 = -1
    kind = ""

    for il in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_pt[il] > pt0 and (abs(event.truthLepton_pdgId[il]) == 11 or abs(event.truthLepton_pdgId[il]) == 13) and il!= i_ignore:
            pt0 = event.truthLepton_pt[il]
            l0 = il
    return l0

def getTruthMatch(barcode):
    for il in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_barcode[il] == barcode:
            return il
    return -1


def recoMatch(barcode):
    for il in xrange(len(event.lepton_pt)):
        if event.lepton_truthMatchedBarcode[il] == barcode and event.lepton_isSignal[il]:
            return il 
    return -1




reco_weight_t = "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight"

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/stat_ext_new/signal/"

append = "slep_400"
if 'slep' in append:
    lifetimes = ["0p01", "0p1", "1", "10"]
    lifetimes = ["1"]
    masses = ["50","100","200",  "300", "400", "500","600","700","800","900","1000"]
    masses = ["400"]
else:
    lifetimes = ["0p01", "0p1", "1"]
    #lifetimes = ["0p1"]
    masses = ["50","100","200",  "300", "400", "500"]
    #masses = ["300"]


SRs = ["e","m"]
    
hists = {}
for sr in SRs:
    hists[sr] = {}
    
    hists[sr]["pt_d0_accepted"] =     ROOT.TH2F(sr+"_pt_d0_acc_l", sr+"_pt_d0_acc_l", 10, 65, 765, 8, 0, 400)
    hists[sr]["pt_d0_accepted"].SetDirectory(0)
    hists[sr]["pt_d0_selected"] =     ROOT.TH2F(sr+"_pt_d0_sel_l", sr+"_pt_d0_sel_l", 10, 65, 765, 8, 0, 400)
    hists[sr]["pt_d0_selected"].SetDirectory(0)
    hists[sr]["pt_d0_total"] =     ROOT.TH2F(sr+"_pt_d0_tot_l", sr+"_pt_d0_tot_l", 10, 65, 765, 8, 0, 400)
    hists[sr]["pt_d0_total"].SetDirectory(0)


print hists

for lt in lifetimes:
    for m in masses:
        for year in ["a","d","e"]:

            print "Working with mass", m, "and lifetime", lt

            mass = int(m)
            if lt == "0p01": lifetime = -2
            elif lt == "0p1": lifetime = -1
            elif lt == "1": lifetime = 0
            elif lt == "10": lifetime = 1
            
            slep_str = "SlepSlep_"
            if "stau" in append: slep_str = "StauStau_"
            sig_str = slep_str + m + "_0_" + lt + "ns"
            print sig_str

             
            try:
                dsid = signals_inv[sig_str]
            except:
                print "no dsid found for ", sig_str
                continue

            fstr = "%s%s%s.root"%(d,dsid,year)
            print fstr
            t = getTree(fstr)  
            if t==-1: continue
            print "Found events:", t.GetEntries()

            maxEntries = t.GetEntries()
            
            filename = glob.glob(fstr)[0]
            xs = getXS(dsid)
            print xs

            for event in t:
                
                il0 = firstLep(event, -1) 
                il1 = firstLep(event, il0)
                
                #print il0, il1 
                for ii in [il0, il1]:
                    #print ii    
                    if ii != -1:
                        #print abs(event.truthLepton_pdgId[ii]) 
                        if (abs(event.truthLepton_pdgId[ii]) == 11 and abs(event.truthLepton_eta[ii]) < 2.47) or (abs(event.truthLepton_pdgId[ii]) == 13 and abs(event.truthLepton_eta[ii]) < 2.5):
                            
                            if event.truthLepton_pt[ii] > 65 and abs(event.truthLepton_d0[ii]) > 3:
                                
                                f = ""
                                if abs(event.truthLepton_pdgId[ii]) == 11: f = "e"
                                if abs(event.truthLepton_pdgId[ii]) == 13: f = "m"
                                 
                               # print f, abs(event.truthLepton_pdgId[ii])
                                
                                hists[f]["pt_d0_accepted"].Fill( event.truthLepton_pt[ii],  abs(event.truthLepton_d0[ii]))

                                ir = recoMatch(event.truthLepton_barcode[ii])
                                if ir != -1:
                                    sf = 1
                                    if f == "e": sf = getElSF(event, ir)
                                    if f == "m": sf = getMuSF(event, ir, year)
                                    
                                    hists[f]["pt_d0_selected"].Fill( event.truthLepton_pt[ii],  abs(event.truthLepton_d0[ii]), sf)


c = ROOT.TCanvas()

for sr in SRs:
    
    sr_string = ""
    if sr == "e": sr_string = "electron"
    elif sr == "m": sr_string = "muon"

    slep_string = ""
    if "stau" in append: slep_string = "#tilde{#tau}"
    elif "slep" in append and sr == "e": slep_string = "#tilde{e}"
    elif "slep" in append  and sr == "m": slep_string = "#tilde{#mu}"

    hists[sr]["ptd0_eff"] = hists[sr]["pt_d0_selected"].Clone(sr+"_ptptacc")
    hists[sr]["ptd0_eff"].Divide(hists[sr]["pt_d0_accepted"])
    hists[sr]["ptd0_eff"].GetXaxis().SetTitle(sr_string+ " p_{T} [GeV]")
    hists[sr]["ptd0_eff"].GetYaxis().SetTitle(sr_string+ " #left|d_{0}#right| [mm]")
    #hists[sr]["ptd0_eff"].GetYaxis().SetRangeUser(0,800)
    hists[sr]["ptd0_eff"].GetZaxis().SetTitle("Reconstrucion Efficiency")
#    hists[sr]["ptd0_eff_lead"].SetMaximum(1)
    
    format2d(hists[sr]["ptd0_eff"])
    hists[sr]["ptd0_eff"].Draw("colz")

    ROOT.ATLASLabel(0.20,0.88, "Simulation Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.83, sr_string + "s from " + slep_string + ": m(#tilde{l}) = 400 GeV, #tau(#tilde{l}) = 1 ns")
    
    text2 = ROOT.TLatex()
    text2.SetNDC()
    text2.SetTextSize(0.04)
    text2.DrawLatex(0.2,0.79, "#sqrt{s} = 13 TeV")
    
    c.SaveAs('plots/'+append+'_ptd0_eff_'+sr+'.pdf')
    c.SaveAs('plots/'+append+'_ptd0_eff_'+sr+'.eps')
    c.SaveAs('plots/'+append+'_ptd0_eff_'+sr+'.root')
    c.SaveAs('plots/'+append+'_ptd0_eff_'+sr+'.C')
    c.Clear()
             
            

