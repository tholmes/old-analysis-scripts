
import glob
import ROOT
import os
import math
from ROOT import TLatex

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.TH1.SetDefaultSumw2(1)

append = "stau"
if append == 'slep':
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/*SlepSlep*"
else:
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/*StauStau*"

def getTree(file_name, tree_name="trees_SR_highd0_", verbose=False):
    files = glob.glob(file_name)
    if len(files)==0:
        print "Couldn't find any files matching string", file_name
        return -1

    print "Found %s files"%len(files)
    t = ROOT.TChain(tree_name)
    for f in files:
        if verbose: print "Adding", f
        t.Add(f)
    return t

def getHist(t, hname, value_name, binstr="", sel_name="", verbose=False):
    hname = hname.replace("(","-").replace(")","-")
    draw_cmd = '%s>>%s' % (value_name, hname)
    if binstr != "":
        draw_cmd += "(%s)" % (binstr)
    if sel_name != "": t.Draw( draw_cmd, '(%s)' % (sel_name) )
    else: t.Draw( draw_cmd )
    if verbose: print "\"%s\", \"%s\""%(draw_cmd, sel_name)
    h = ROOT.gROOT.FindObject(hname)
    return h

def ATLAS_LABEL(x,y,color,tsize=0.03):
  l = TLatex()
  l.SetTextAlign(12) 
  l.SetTextSize(tsize)
  l.SetNDC();
  l.SetTextFont(72);  #72
  l.SetTextColor(color);
  l.DrawLatex(x,y,"ATLAS");
  
def myText(x,y,color,text,tsize=0.03):
  l = TLatex(); 
  l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(42);#50
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);


# Define grid
if append == 'slep':
    lifetimes = ["0p01", "0p1", "1", "10"]
    masses = ["50","100","200",  "300", "400", "500", "600","700","800", "900", "1000"]
else:
    lifetimes = ["0p01", "0p1", "1"]
    masses = ["50","100","200",  "300", "400", "500"]

# Define SRs
if append == 'slep':
    SRs = [ "ee_", "mm_"]
else:
    SRs = [ "ee_", "mm_", "em_"]


# Set up hists for acceptance
hists = {}
for sr in SRs + ["all"]:
    hists[sr] = {}

    if append == 'slep':
        hists[sr]["total"] =    ROOT.TH2F(sr+"_total", sr+"_total", 21, 0, 1050, 5, -2., 3.)
        hists[sr]["truth"] =     ROOT.TH2F(sr+"_truth", sr+"_truth",   21, 0, 1050, 5, -2., 3.)
    else:
        hists[sr]["total"] =    ROOT.TH2F(sr+"_total", sr+"_total", 11, 0, 550, 4, -2., 2.)
        hists[sr]["truth"] =     ROOT.TH2F(sr+"_truth", sr+"_truth", 11, 0, 550, 4, -2., 2.)

    hists[sr]["total"].Sumw2()
    hists[sr]["truth"].Sumw2()

# Loop over files and make plots

for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt

        mass = int(m)
        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1


        t = getTree("%s*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))  
        if t==-1: continue
        print "Found events:", t.GetEntries()

        maxEntries = t.GetEntries()

        for entry in xrange(maxEntries):
            t.GetEntry(entry)
            e_idx = []
            m_idx = []
            idx = []
            
            pass_ee=False
            pass_mm=False
            pass_em=False

            for i in range(0, len(t.truthLepton_pdgId)):
                if abs(t.truthLepton_pdgId[i]) == 11 or abs(t.truthLepton_pdgId[i]) == 13: idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 11: e_idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 13: m_idx.append(i)
            
            # require 2 leptons, pt, d0 cuts
            if not(len(e_idx) + len(m_idx) >= 2): continue
            if not(t.truthLepton_pt[idx[0]] > 65 and t.truthLepton_pt[idx[1]] > 65):continue
            if not(abs(t.truthLepton_d0[idx[0]]) > 3.0 and abs(t.truthLepton_d0[idx[1]]) > 3.0):continue
            if not(abs(t.truthLepton_d0[idx[0]]) < 300 and abs(t.truthLepton_d0[idx[1]]) < 300): continue

            if abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 11: pass_ee = True
            elif abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_pdgId[idx[1]]) == 13: pass_mm =True
            elif (abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 13) or (abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_pdgId[idx[0]]) == 13): pass_em =True

            # select trigger region, don't include |d0| > 2 cut since above ensures it
            if len(e_idx) >=1 and t.truthLepton_pt[e_idx[0]] > 160: trigPass=True #single photon region, should always be true at truth level
            elif len(e_idx)>=2 and t.truthLepton_pt[e_idx[0]] > 60 and t.truthLepton_pt[e_idx[1]] > 60: trigPass=True #diphoton region, should always be true at truth level
            elif (len(m_idx)>=1 and t.truthLepton_pt[m_idx[0]] > 60 and abs(t.truthLepton_eta[m_idx[0]]) < 1.07) or (len(m_idx)>=2 and t.truthLepton_pt[m_idx[1]] > 60 and abs(t.truthLepton_eta[m_idx[1]]) < 1.07): trigPass = True #single muon trigger
            else: trigPass=False

            if not trigPass: continue

            # delta R cut
            if not (math.sqrt((t.truthLepton_eta[idx[0]]-t.truthLepton_eta[idx[1]])**2 + (t.truthLepton_phi[idx[0]]-t.truthLepton_phi[idx[1]])**2) > 0.2) :continue

            if pass_ee and (abs(t.truthLepton_eta[idx[0]]) < 2.47 and abs(t.truthLepton_eta[idx[1]]) < 2.47): #ee
                hists['ee_']["truth"].Fill(mass, lifetime)
            elif pass_mm and (abs(t.truthLepton_eta[idx[0]]) < 2.5 and abs(t.truthLepton_eta[idx[1]]) < 2.5): #mm
                hists['mm_']["truth"].Fill(mass, lifetime)
            elif pass_em and len(m_idx) >= 1 and (abs(t.truthLepton_eta[e_idx[0]]) < 2.47 and abs(t.truthLepton_eta[m_idx[0]]) < 2.5): #em
                hists['em_']["truth"].Fill(mass, lifetime)

        # get total
        for sr in SRs:
            h_total = getHist(t, "h_ev_%s_%s_%s_total"%(sr,m,lt), "eventNumber", sel_name=str("1"))
            hists[sr]["total"].Fill(mass, lifetime, h_total.Integral())

for sr in SRs:


    h = hists[sr]["truth"]
    h.GetXaxis().SetTitle("slepton mass [GeV]")
    h.GetYaxis().SetTitle("slepton lifetime [log10(ns)]")
    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_truth_"+sr, "can_truth_"+sr)
    ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kGray+2)
    h_temp.Draw("text90 same")
    savename = "plots/"+sr+append+"_truthyields"

    can.SaveAs(savename+'.pdf')


    h = hists[sr]["total"]
    h.GetXaxis().SetTitle(append+" mass [GeV]")
    h.GetYaxis().SetTitle(append+" lifetime [log10(ns)]")
    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_total_"+sr, "can_total_"+sr)
    ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kGray+2)
    h_temp.Draw("text90 same")
    savename = "plots/"+sr+append+"_total.pdf"

    can.SaveAs(savename)


for sr in SRs:

    ROOT.gStyle.SetPaintTextFormat("#.3g");

    if sr =='ee_': srname = 'SR-ee'
    elif sr =='em_': srname = 'SR-e#mu'
    elif sr == 'mm_': srname = 'SR-#mu#mu'
 
    # acceptance: truth / total
    if append == 'slep':
        hists[sr]["acc"]=ROOT.TH2F(sr+"_acc", sr+"_acc", 21, 0, 1050, 5, -2., 3.)
    else: hists[sr]["acc"]=ROOT.TH2F(sr+"_acc", sr+"_acc", 11, 0, 550, 4, -2., 2.)

    hists[sr]["acc"].Sumw2()
    hists[sr]["acc"].Divide(hists[sr]["truth"],hists[sr]["total"],1,1, "b")
    hists[sr]["acc"].GetXaxis().SetTitle(append+" mass [GeV]")
    hists[sr]["acc"].GetYaxis().SetTitle(append+" lifetime [log10(ns)]")
    hists[sr]["acc"].GetZaxis().SetTitle("Acceptance")
    hists[sr]["acc"].GetZaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetZaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetXaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetXaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetYaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetYaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetZaxis().SetLabelSize(0.04);
    hists[sr]["acc"].SetMarkerSize(0.6)
    hists[sr]["acc"].GetXaxis().SetLabelSize(0.04);
    hists[sr]["acc"].GetYaxis().SetLabelSize(0.04);

    can = ROOT.TCanvas("can_"+sr, "can_"+sr)
    hists[sr]["acc"].SetMinimum(0)
    if append == 'slep':
        hists[sr]["acc"].SetMaximum(0.3)
    else: hists[sr]["acc"].SetMaximum(0.005)
    hists[sr]["acc"].Draw("colz")
    h_temp = hists[sr]["acc"].Clone("text_acc"+sr)
    h_temp.SetMarkerColor(ROOT.kBlack)
    h_temp.Draw("text90 same")
    savename = "plots/"+sr+append+"_acc"

    ATLAS_LABEL(0.2,0.88,1);
    myText(0.27,0.88,1,"Simulation Internal");
    myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    myText(0.2,0.80,1,srname);

    can.SaveAs(savename+'.pdf')
    can.SaveAs(savename+'.eps')
    can.SaveAs(savename+'.root')


