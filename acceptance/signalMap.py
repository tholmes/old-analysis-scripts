signals= { '399001': ['StauStau_50_0_0p01ns']
           ,'399002': ['StauStau_50_0_0p1ns']
           ,'399003': ['StauStau_50_0_1ns']
           ,'399005': ['StauStau_100_0_0p01ns']
           ,'399006': ['StauStau_100_0_0p1ns']
           ,'399007': ['StauStau_100_0_1ns']
           ,'399009': ['StauStau_200_0_0p01ns']
           ,'399010': ['StauStau_200_0_0p1ns']
           ,'399011': ['StauStau_200_0_1ns']
           ,'399013': ['StauStau_300_0_0p01ns']
           ,'399014': ['StauStau_300_0_0p1ns']
           ,'399015': ['StauStau_300_0_1ns']
           ,'399017': ['StauStau_400_0_0p01ns']
           ,'399018': ['StauStau_400_0_0p1ns']
           ,'399019': ['StauStau_400_0_1ns']
           ,'399021': ['StauStau_500_0_0p01ns']
           ,'399022': ['StauStau_500_0_0p1ns']
           ,'399023': ['StauStau_500_0_1ns']
           ,'399026': ['SlepSlep_50_0_0p01ns']
           ,'399027': ['SlepSlep_50_0_0p1ns']
           ,'399028': ['SlepSlep_50_0_1ns']
           ,'399030': ['SlepSlep_100_0_0p01ns']
           ,'399031': ['SlepSlep_100_0_0p1ns']
           ,'399032': ['SlepSlep_100_0_1ns']
           ,'399034': ['SlepSlep_200_0_0p01ns']
           ,'399035': ['SlepSlep_200_0_0p1ns']
           ,'399036': ['SlepSlep_200_0_1ns']
           ,'399038': ['SlepSlep_300_0_0p01ns']
           ,'399039': ['SlepSlep_300_0_0p1ns']
           ,'399040': ['SlepSlep_300_0_1ns']
           ,'399042': ['SlepSlep_400_0_0p01ns']
           ,'399043': ['SlepSlep_400_0_0p1ns']
           ,'399044': ['SlepSlep_400_0_1ns']
           ,'399046': ['SlepSlep_500_0_0p01ns']
           ,'399047': ['SlepSlep_500_0_0p1ns']
           ,'399048': ['SlepSlep_500_0_1ns']
           ,'399050': ['SlepSlep_600_0_0p01ns']
           ,'399051': ['SlepSlep_600_0_0p1ns']
           ,'399052': ['SlepSlep_600_0_1ns']
           ,'399054': ['SlepSlep_700_0_0p01ns']
           ,'399055': ['SlepSlep_700_0_0p1ns']
           ,'399056': ['SlepSlep_700_0_1ns']
           ,'399057': ['SlepSlep_800_0_0p1ns']
           ,'399107': ['SlepSlep_800_0_0p01ns']
           ,'399108': ['SlepSlep_800_0_1ns']
           ,'399109': ['SlepSlep_900_0_0p01ns']
           ,'399110': ['SlepSlep_900_0_0p1ns']
           ,'399111': ['SlepSlep_900_0_1ns']
           ,'399112': ['SlepSlep_1000_0_0p01ns']
           ,'399113': ['SlepSlep_1000_0_0p1ns']
           ,'399114': ['SlepSlep_50_0_10ns']
           ,'399115': ['SlepSlep_100_0_10ns']
           ,'399116': ['SlepSlep_200_0_10ns']
           ,'399117': ['SlepSlep_300_0_10ns']
           ,'399118': ['SlepSlep_400_0_10ns']
           ,'399119': ['SlepSlep_500_0_10ns']
           ,'399120': ['SlepSlep_600_0_10ns']
           ,'399121': ['SlepSlep_700_0_10ns']
           ,'399122': ['SlepSlep_800_0_10ns']
       }

signals_inv= {  
            'StauStau_50_0_0p01ns' : '399001'
           ,'StauStau_50_0_0p1ns' : '399002'
           ,'StauStau_50_0_1ns' : '399003'
           ,'StauStau_100_0_0p01ns' : '399005'
           ,'StauStau_100_0_0p1ns' : '399006'
           ,'StauStau_100_0_1ns'  : '399007'
           ,'StauStau_200_0_0p01ns' : '399009'
           ,'StauStau_200_0_0p1ns' : '399010'
           ,'StauStau_200_0_1ns' : '399011'
           ,'StauStau_300_0_0p01ns': '399013'
           ,'StauStau_300_0_0p1ns' : '399014'
           ,'StauStau_300_0_1ns' : '399015'
           ,'StauStau_400_0_0p01ns' : '399017'
           ,'StauStau_400_0_0p1ns' : '399018'
           ,'StauStau_400_0_1ns' : '399019'
           ,'StauStau_500_0_0p01ns' : '399021'
           ,'StauStau_500_0_0p1ns' : '399022'
           ,'StauStau_500_0_1ns' : '399023'
           ,'SlepSlep_50_0_0p01ns' : '399026'
           ,'SlepSlep_50_0_0p1ns' : '399027'
           ,'SlepSlep_50_0_1ns' : '399028'
           ,'SlepSlep_100_0_0p01ns' : '399030'
           ,'SlepSlep_100_0_0p1ns' : '399031'
           ,'SlepSlep_100_0_1ns' : '399032'
           ,'SlepSlep_200_0_0p01ns' : '399034'
           ,'SlepSlep_200_0_0p1ns' : '399035'
           ,'SlepSlep_200_0_1ns' : '399036'
           ,'SlepSlep_300_0_0p01ns' : '399038'
           ,'SlepSlep_300_0_0p1ns' : '399039'
           ,'SlepSlep_300_0_1ns' : '399040'
           ,'SlepSlep_400_0_0p01ns' : '399042'
           ,'SlepSlep_400_0_0p1ns' : '399043'
           ,'SlepSlep_400_0_1ns' : '399044'
           ,'SlepSlep_500_0_0p01ns' : '399046'
           ,'SlepSlep_500_0_0p1ns' : '399047'
           ,'SlepSlep_500_0_1ns' : '399048'
           ,'SlepSlep_600_0_0p01ns' : '399050'
           ,'SlepSlep_600_0_0p1ns' : '399051'
           ,'SlepSlep_600_0_1ns' : '399052'
           ,'SlepSlep_700_0_0p01ns' : '399054'
           ,'SlepSlep_700_0_0p1ns' : '399055'
           ,'SlepSlep_700_0_1ns' : '399056'
           ,'SlepSlep_800_0_0p1ns' : '399057'
           ,'SlepSlep_800_0_0p01ns' : '399107'
           ,'SlepSlep_800_0_1ns' : '399108'
           ,'SlepSlep_900_0_0p01ns' : '399109'
           ,'SlepSlep_900_0_0p1ns' : '399110'
           ,'SlepSlep_900_0_1ns' : '399111'
           ,'SlepSlep_1000_0_0p01ns' : '399112'
           ,'SlepSlep_1000_0_0p1ns' : '399113'
           ,'SlepSlep_50_0_10ns' : '399114'
           ,'SlepSlep_100_0_10ns' : '399115'
           ,'SlepSlep_200_0_10ns' : '399116'
           ,'SlepSlep_300_0_10ns' : '399117'
           ,'SlepSlep_400_0_10ns' : '399118'
           ,'SlepSlep_500_0_10ns' : '399119'
           ,'SlepSlep_600_0_10ns' : '399120'
           ,'SlepSlep_700_0_10ns' : '399121'
           ,'SlepSlep_800_0_10ns' : '399122'
       }
