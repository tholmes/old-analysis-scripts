import glob
import ROOT
import os
import math

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.TH1.SetDefaultSumw2(1)

append = 'slep'
if append == 'slep':
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/*SlepSlep*"
else:
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/*StauStau*"


def ATLAS_LABEL(x,y,color,tsize=0.03):
  l = TLatex()
  l.SetTextAlign(12) 
  l.SetTextSize(tsize)
  l.SetNDC();
  l.SetTextFont(72);  #72
  l.SetTextColor(color);
  l.DrawLatex(x,y,"ATLAS");
  
def myText(x,y,color,text,tsize=0.03):
  l = TLatex(); 
  l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(42);#50
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);

# Define grid
if append == 'slep':
    lifetimes = ["0p01", "0p1", "1", "10"]
    masses = ["50","100","200",  "300", "400", "500", "600","700","800", "900", "1000"]
    lifetimes =["1"]
    masses=["400"]
else:
    lifetimes = ["0p01", "0p1", "1"]
    masses = ["50","100","200",  "300", "400", "500"]

# Hand-done cross-sections [pb], per flavor
lumi = 139000

# Define SRs
SRs = [ "ee_", "mm_", "em_"]


# Set up hists for acceptance
hists = {}
for sr in SRs + ["all"]:
    hists[sr] = {}

    hists[sr]["pass"] =     ROOT.TH2F(sr+"_truthpt_cut", sr+"_cut",   20, 0, 1000, 20, 0, 1000)
    hists[sr]["total"] =    ROOT.TH2F(sr+"_truthpt_total", sr+"_total", 20, 0, 1000, 20, 0, 1000)
    hists[sr]["truth"] =     ROOT.TH2F(sr+"_truthpt_noncut", sr+"_truth",  20, 0, 1000, 20, 0, 1000)
    
    hists[sr]["pass"].Sumw2()
    hists[sr]["total"].Sumw2()
    hists[sr]["truth"].Sumw2()

print hists
# Loop over files and make plots

for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt

        mass = int(m)
        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1

        t = getTree("%s*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))  
        if t==-1: continue
        print "Found events:", t.GetEntries()

        maxEntries = t.GetEntries()

        for entry in xrange(maxEntries):
            t.GetEntry(entry)
            e_idx = []
            m_idx = []
            idx = []

            pass_ee= False
            pass_mm =False
            pass_em = False
            
            for i in range(0, len(t.truthLepton_pdgId)):
                if abs(t.truthLepton_pdgId[i]) == 11 or abs(t.truthLepton_pdgId[i]) == 13: idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 11: e_idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 13: m_idx.append(i)
            
            # require 2 leptons, pt, d0 cuts
            if not(len(e_idx) + len(m_idx) >= 2): continue
            
            if abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 11:
                hists['ee_']["truth"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]])
            elif abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_pdgId[idx[1]]) == 13:
                hists['mm_']["truth"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]])
            elif (abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 13) or (abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_pdgId[idx[0]]) == 13):
                hists['em_']["truth"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]])

            if abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_eta[idx[0]]) < 2.47 and abs(t.truthLepton_eta[idx[1]]) < 2.47: pass_ee = True
            elif abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_pdgId[idx[1]]) == 13 and abs(t.truthLepton_eta[idx[0]]) < 2.5 and  abs(t.truthLepton_eta[idx[0]]) < 2.5: pass_mm =True
            elif (abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 13 and abs(t.truthLepton_eta[idx[0]]) < 2.47 and abs(t.truthLepton_eta[idx[1]]) < 2.5) or (abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_eta[idx[1]]) < 2.47 and abs(t.truthLepton_eta[idx[0]]) < 2.5): pass_em =True


            
            if not(t.truthLepton_pt[idx[0]] > 65 and t.truthLepton_pt[idx[1]] > 65):continue
            if not(abs(t.truthLepton_d0[idx[0]]) > 3.0 and abs(t.truthLepton_d0[idx[1]]) > 3.0):continue
            if not(abs(t.truthLepton_d0[idx[0]]) < 300 and abs(t.truthLepton_d0[idx[1]]) < 300): continue

            # select trigger region, don't include |d0| > 2 cut since above ensures it
            if len(e_idx) >=1 and t.truthLepton_pt[e_idx[0]] > 160: trigPass=True #single photon region, should always be true at truth level
            elif len(e_idx)>=2 and t.truthLepton_pt[e_idx[0]] > 60 and t.truthLepton_pt[e_idx[1]] > 60: trigPass=True #diphoton region, should always be true at truth level
            elif (len(m_idx)>=1 and t.truthLepton_pt[m_idx[0]] > 60 and abs(t.truthLepton_eta[m_idx[0]]) < 1.07) or (len(m_idx)>=2 and t.truthLepton_pt[m_idx[0]] > 60 and abs(t.truthLepton_eta[m_idx[0]]\
) < 1.07 and t.truthLepton_pt[m_idx[1]] > 60 and abs(t.truthLepton_eta[m_idx[1]]) < 1.07): trigPass = True #single muon trigger
            else: trigPass=False

            if not trigPass: continue

            # delta R cut
            if not (math.sqrt((t.truthLepton_eta[idx[0]]-t.truthLepton_eta[idx[1]])**2 + (t.truthLepton_phi[idx[0]]-t.truthLepton_phi[idx[1]])**2) > 0.2) :continue

            if (pass_ee):
                hists['ee_']["pass"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]], t.mcEventWeight) 
            if (pass_mm):
                hists['mm_']["pass"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]], t.mcEventWeight)
            if (pass_em):
                hists['em_']["pass"].Fill(t.truthLepton_pt[idx[0]],t.truthLepton_pt[idx[1]], t.mcEventWeight)
            

        for sr in SRs:
                        

            if sr =='ee_': srname = 'SR-ee'
            elif sr =='em_': srname = 'SR-e#mu'
            elif sr == 'mm_': srname = 'SR-#mu#mu'

            h_temp = addOverflow2D(hists[sr]["pass"])
            h = h_temp.Clone("pass"+sr)
            h.GetXaxis().SetTitle('Lead lepton p_{T} [GeV]')
            h.GetYaxis().SetTitle('Sublead lepton p_{T} [GeV]')
            #h.SetMinimum(.001)
            #h.SetMaximum(10000)
            h.Scale(1/float(h.Integral(-1,-1,-1,-1)))
            h.GetZaxis().SetTitle("Norm. Yields")
            h.GetZaxis().SetTitleSize(0.04);
            h.GetZaxis().SetTitleOffset(1.4);
            h.GetXaxis().SetTitleSize(0.04);
            h.GetXaxis().SetTitleOffset(1.4);
            h.GetYaxis().SetTitleSize(0.04);
            h.GetYaxis().SetTitleOffset(1.4);
            h.GetZaxis().SetLabelSize(0.04);
            h.SetMarkerSize(0.6)
            h.GetXaxis().SetLabelSize(0.04);
            h.GetYaxis().SetLabelSize(0.04);


            can = ROOT.TCanvas("can_pass_"+sr, "can_pass_"+sr)
            #ROOT.gPad.SetLogz(1)
            h.Draw("colz")
            h_temp = h.Clone("text_"+sr)
            h_temp.SetMarkerSize(2)
            h_temp.SetMarkerColor(ROOT.kGray+2)
            #h_temp.Draw("text90 same")
            #raw_input("...")

            savename = "plots/"+sr+append+'_'+m+'_'+lt+"_truthpt_srcuts"
 
            ATLAS_LABEL(0.2,0.97,1);
            myText(0.27,0.97,1,"Simulation Internal");
            #myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
            #myText(0.2,0.80,1,srname);
            
            can.SaveAs(savename+'.pdf')
            can.SaveAs(savename+'.eps')
            can.SaveAs(savename+'.root')


            h_temp = addOverflow2D(hists[sr]["truth"])
            h = h_temp.Clone("truth"+sr)
            h.GetXaxis().SetTitle('Lead lepton p_{T} [GeV]')
            h.GetYaxis().SetTitle('Sublead lepton p_{T} [GeV]')
            h.Scale(1/float(h.Integral(-1,-1,-1,-1)))
            h.GetZaxis().SetTitle("Norm. Yields")
            h.GetZaxis().SetTitleSize(0.04);
            h.GetZaxis().SetTitleOffset(1.4);
            h.GetXaxis().SetTitleSize(0.04);
            h.GetXaxis().SetTitleOffset(1.4);
            h.GetYaxis().SetTitleSize(0.04);
            h.GetYaxis().SetTitleOffset(1.4);
            h.GetZaxis().SetLabelSize(0.04);
            h.SetMarkerSize(0.6)
            h.GetXaxis().SetLabelSize(0.04);
            h.GetYaxis().SetLabelSize(0.04);

            
            can = ROOT.TCanvas("can_truth_"+sr, "can_truth_"+sr)
            #ROOT.gPad.SetLogz(1)
            h.Draw("colz")
            h_temp = h.Clone("text_"+sr)
            h_temp.SetMarkerSize(2)
            h_temp.SetMarkerColor(ROOT.kGray+2)
            #h_temp.Draw("text90 same")
            #raw_input("...")                                                                                                                     
            savename = "plots/"+sr+append+'_'+m+'_'+lt+"_truthpt_nocuts"

            ATLAS_LABEL(0.2,0.97,1);
            myText(0.27,0.97,1,"Simulation Internal");
            #myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
            #myText(0.2,0.80,1,srname);


            can.SaveAs(savename+'.pdf')
            can.SaveAs(savename+'.eps')
            can.SaveAs(savename+'.root')
        
                        
            ROOT.gStyle.SetPaintTextFormat("#.3g");
 
            # acceptance: truth / total
            hists[sr]["acc"]=ROOT.TH2F(sr+"_acc", sr+"_acc",  20, 0, 1000, 20, 0, 1000)
            
            #hists[sr]["truth"].Clone(sr+"_acc")
            hists[sr]["acc"].Sumw2()
            hists[sr]["acc"].Divide(addOverflow2D(hists[sr]["pass"]),addOverflow2D(hists[sr]["truth"]),1,1, "b")
            hists[sr]["acc"].GetXaxis().SetTitle('Lead lepton p_{T} [GeV]')
            hists[sr]["acc"].GetYaxis().SetTitle('Sublead lepton p_{T} [GeV]')
            hists[sr]["acc"].GetZaxis().SetTitle("Acceptance")
            hists[sr]["acc"].GetZaxis().SetTitleSize(0.04);
            hists[sr]["acc"].GetZaxis().SetTitleOffset(1.4);
            hists[sr]["acc"].GetXaxis().SetTitleSize(0.04);
            hists[sr]["acc"].GetXaxis().SetTitleOffset(1.4);
            hists[sr]["acc"].GetYaxis().SetTitleSize(0.04);
            hists[sr]["acc"].GetYaxis().SetTitleOffset(1.4);
            hists[sr]["acc"].GetZaxis().SetLabelSize(0.04);
            hists[sr]["acc"].SetMarkerSize(0.6)
            hists[sr]["acc"].GetXaxis().SetLabelSize(0.04);
            hists[sr]["acc"].GetYaxis().SetLabelSize(0.04);
            
            can = ROOT.TCanvas("can_"+sr, "can_"+sr)
            hists[sr]["acc"].SetMinimum(0)
            hists[sr]["acc"].SetMaximum(0.9)
            hists[sr]["acc"].Draw("colz")
            h_temp = hists[sr]["acc"].Clone("text_acc"+sr)
            
            h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
            h_temp.Draw("text45 same")
            savename = "plots/"+sr+append+'_'+m+'_'+lt+"_acc_truthpt"

            ATLAS_LABEL(0.2,0.97,1);
            myText(0.27,0.97,1,"Simulation Internal");
            #myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
            #myText(0.2,0.80,1,srname);

            can.SaveAs(savename+'.pdf')
            can.SaveAs(savename+'.eps')
            can.SaveAs(savename+'.root')
