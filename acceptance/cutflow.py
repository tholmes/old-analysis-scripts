
import glob
import ROOT
import os
import math
import signalMap

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/stat_ext_new/signal/"

#ee,mm, emu
sr = 'emu'

cuts = []

common_cuts = 'lepton_n_baseline >= 2 &&  triggerRegion_pass'

ee_select = 'channel == 0'
emu_select  = '(channel == 2 || channel == 3)'
mumu_select = 'channel == 1'

e1_lrtAcc = 'electron_d0l300[0] && electron_z0l500[0]'
e2_lrtAcc = 'electron_d0l300[1] && electron_z0l500[1]'

m1_lrtAcc = 'muon_d0l300[0] && muon_z0l500[0]'
m2_lrtAcc = 'muon_d0l300[1] && muon_z0l500[1]'

# common cuts
cuts.append( common_cuts)
if sr == 'ee':
    cuts.append( ' && ' + ee_select +'&&electron_etal247[0] && electron_etal247[1]')
    cuts.append( ' &&  electron_pt[0] > 65. && electron_pt[1] > 65.')
    cuts.append( ' && electron_d0g3[0] && electron_d0g3[1]')
    cuts.append( ' && electron_isIsolated[0] && electron_isIsolated[1]')
    cuts.append( ' && electron_dptgt0p5[0] && electron_dptgt0p5[1]')
    cuts.append( ' && electron_chi2l2[0] && electron_chi2l2[1]')
    cuts.append( ' && electron_nmissl2[0] && electron_nmissl2[1]')
   
elif sr == 'mm':
    cuts.append( ' && ' + mumu_select)
    cuts.append( ' &&  muon_pt[0] > 65. && muon_pt[1] > 65.')
    cuts.append( ' && muon_d0g3[0] && muon_d0g3[1]')
    cuts.append( ' && muon_FCTightTTVA[0] && muon_FCTightTTVA[1]')
    cuts.append( ' && muon_isNotCosmic[0] && muon_isNotCosmic[1]')
    cuts.append( ' && muon_isPassTiming[0] && muon_isPassTiming[1]')
    cuts.append( ' && muon_idchi2[0]  && muon_idchi2[1]')
    cuts.append( ' && muon_idMissLayer[0] && muon_idMissLayer[1]')
    cuts.append( ' && muon_npresg2[0]  && muon_npresg2[1] ')
    cuts.append( ' && muon_chi2l3[0]  && muon_chi2l3[1] ')
    cuts.append( ' && muon_hasPhiLays[0] && muon_hasPhiLays[1]')
else: #emu
    cuts.append( ' && ' + emu_select + '&&electron_etal247[0]')
    cuts.append( ' &&  electron_pt[0] > 65. && muon_pt[0] > 65.')
    cuts.append( ' && electron_d0g3[0] &&  muon_d0g3[0]')
    cuts.append( ' && electron_FCTightTTVA[0] && muon_FCTightTTVA[0]')
 
    cuts.append( ' && muon_isNotCosmic[0]')
    cuts.append( ' && muon_isPassTiming[0]')
    cuts.append( ' && muon_idchi2[0]')
    cuts.append( ' && muon_idMissLayer')
    cuts.append( ' && muon_npresg2[0] ')
    cuts.append( ' && muon_chi2l3[0] ')
    cuts.append( ' && muon_hasPhiLays[0]')
    
    cuts.append( ' && electron_dptgt0p5[0]')
    cuts.append( ' && electron_chi2l2[0]')
    cuts.append( ' && electron_nmissl2[0]')
# common cuts
cuts.append( ' && deltaR > 0.2')
cuts.append( ' && muon_n_cosmic == 0')

lumi = 139000.

# Choose signals
signalList = []
if not(sr == 'emu'): signalList = ['399030', '399040', '399047']
signalList += ['399010', '399014']

cut_str = ''
for i in range (0, len(cuts)):
    cut_str += cuts[i]
    print cut_str
    outstr = ''
 
    for j in range(0,len(signalList)):
        #print signalList[j]
        ntup = signalList[j] 

        t = getTree(d+"/"+signalList[j]+"*.root")

        # get number of events
        sigName = signalList[j]+'_'+str(i)

        t.Draw('triggerRegion_pass >> %s(2,0,2)'%(sigName), 'lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight*('+cut_str+')')
        h = ROOT.gDirectory.Get(sigName)

        total = h.GetBinContent(2)
        err = h.GetBinError(2)
        outstr += ' & '+ '%0.3g'%total #+' $\pm$ '+ '%0.2g'%err

    print outstr
    

