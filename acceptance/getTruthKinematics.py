
import glob
import ROOT
import os
import math

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

append = "stau"
if append == 'slep':
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/*SlepSlep*"
else:
    d =  "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/*StauStau*"


# Define grid
lifetimes = ["0p01", "0p1", "1"]
masses = ["100",  "300", "400", "500", "700"]

# Hand-done cross-sections [pb], per flavor
lumi = 140000

# Define SRs
if append == 'slep':
    SRs = [ "ee_", "mm_"]
else:
    SRs = [ "ee_", "mm_", "em_"]


for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt

        t = getTree("%s*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))  
        if t==-1: continue
        print "Found events:", t.GetEntries()

        maxEntries = t.GetEntries()

        for entry in xrange(maxEntries):
            t.GetEntry(entry)

            if not (t.runNumber == 300000 and t.eventNumber == 8883 or t.runNumber == 310000 and t.eventNumber == 4144 or t.runNumber == 284500 and t.eventNumber == 2457): continue
            print t.runNumber, t.eventNumber

            print 'pdg id', t.truthLepton_pdgId
            for i in range(0, len(t.truthLepton_pdgId)):
                print 'pdg id', t.truthLepton_pdgId[i], 'lepton pt', t.truthLepton_pt[i], 'eta', t.truthLepton_eta[i], 'phi',t.truthLepton_phi[i], 'd0', t.truthLepton_d0[i]

 

            #if not (math.sqrt((t.truthLepton_eta[idx[0]]-t.truthLepton_eta[idx[1]])**2 + (t.truthLepton_phi[idx[0]]-t.truthLepton_phi[idx[1]])**2) > 0.2) :continue

            
        
