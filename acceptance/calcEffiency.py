  
import glob
import ROOT
import os
import math

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.TH1.SetDefaultSumw2(1)

append = "slep"
if append == 'slep':
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.8_PRW/signal/*399044*root"
else:
    d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/*StauStau*"


def ATLAS_LABEL(x,y,color,tsize=0.04):
  l = TLatex()
  l.SetTextAlign(12) 
  l.SetTextSize(tsize)
  l.SetNDC();
  l.SetTextFont(72);  #72
  l.SetTextColor(color);
  l.DrawLatex(x,y,"ATLAS");
  
def myText(x,y,color,text,tsize=0.04):
  l = TLatex(); 
  l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(42);#50
  l.SetTextColor(color);
  l.DrawLatex(x,y,text);


# Define grid
if append == 'slep':
    lifetimes = ["0p01", "0p1", "1", "10"]
    masses = ["50","100","200",  "300", "400", "500", "600","700","800", "900", "1000"]
    lifetimes = ["1"]
    masses = ["400"]
else:
    lifetimes = ["0p01", "0p1", "1"]
    masses = ["50","100","200",  "300", "400", "500"]

# Hand-done cross-sections [pb], per flavor
lumi = 139000

# Define SRs
if append == 'slep':
    SRs = [ "ee_", "mm_"]
else:
    SRs = [ "ee_", "mm_", "em_"]
SR_sels = {}
truthSR_sels = {}


for sr in SRs:
    sel = Selection(sr)
    e_sel = Selection()
    m_sel = Selection()

    obj0 = ""
    obj1 = ""
    if "ee_" in sr:
        obj0 = "electron_"
        obj1 = "electron_"
    elif sr.startswith("mm_"):
        obj0 = "muon_"
        obj1 = "muon_"
    elif "em_" in sr:
        obj0 = "electron_"
        obj1 = "muon_"
    elif "me_" in sr:
        obj0 = "muon_"
        obj1 = "electron_"
    elif "ge_" in sr:
        obj0 = "photon_"
        obj1 = "electron_"
    elif "eg_" in sr:
        obj0 = "electron_"
        obj1 = "photon_"
    elif "gg_" in sr:
        obj0 = "photon_"
        obj1 = "photon_"

    min_d0 = 3

    min_pt = 65
    min_pt2=min_pt

    if len(sr.split("_"))>3: min_pt2 = int(sr.split("_")[3])

    obj1_index = 0
    if obj1==obj0: obj1_index = 1

    if obj0 == "electron_":
        sel.addCut("electron_isSignal[0]")

    if obj0 == "muon_":
        sel.addCut("muon_isSignal[0]")
        #sel.addCut("muon_t0avg[0]", maxval=30, abs_val=True)


    if obj1 == "electron_":
        sel.addCut("electron_isSignal[%d]"%obj1_index)
        
    if obj1 == "muon_":
        sel.addCut("muon_isSignal[%d]"%obj1_index)
        #sel.addCut("muon_t0avg[%d]"%obj1_index, maxval=30, abs_val=True)

        
    # additional reco cuts
    sel.addCut("triggerRegion_pass")
    sel.addCut("muon_n_cosmic", maxval=0.1)
    sel.addCut("deltaR", 0.2, abs_val=True)


    SR_sels[sr] = sel
    print "\nAdding selection:"
    print SR_sels[sr].getCutString()
    

# Set up hists for acceptance
hists = {}
for sr in SRs + ["all"]:
    hists[sr] = {}

    hists[sr]["pass"] =     ROOT.TH2F(sr+"_pass", sr+"_pass",   19, 50, 1000, 30, 0, 300)
    hists[sr]["total"] =    ROOT.TH2F(sr+"_total", sr+"_total", 19, 50, 1000, 30, 0, 300)
    hists[sr]["truth"] =     ROOT.TH2F(sr+"_truth", sr+"_truth", 19, 50, 1000, 30, 0, 300)

    hists[sr]["pass"].Sumw2()
    hists[sr]["total"].Sumw2()
    hists[sr]["truth"].Sumw2()

# Loop over files and make plots

for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt

        mass = int(m)
        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1


        t = getTree("%s"%(d))  
        if t==-1: continue
        print "Found events:", t.GetEntries()

        maxEntries = t.GetEntries()

        for entry in xrange(maxEntries):
            t.GetEntry(entry)
            e_idx = []
            m_idx = []
            idx = []

            pass_ee=False
            pass_mm=False
            pass_em=False
            
            for i in range(0, len(t.truthLepton_pdgId)):
                if abs(t.truthLepton_pdgId[i]) == 11 or abs(t.truthLepton_pdgId[i]) == 13: idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 11: e_idx.append(i)
                if abs(t.truthLepton_pdgId[i]) == 13: m_idx.append(i)

            
            # require 2 leptons, pt, d0 cuts
            if not(len(e_idx) + len(m_idx) >= 2): continue
            if not(t.truthLepton_pt[idx[0]] > 65 and t.truthLepton_pt[idx[1]] > 65):continue
            if not(abs(t.truthLepton_d0[idx[0]]) > 3.0 and abs(t.truthLepton_d0[idx[1]]) > 3.0):continue
            if not(abs(t.truthLepton_d0[idx[0]]) < 300 and abs(t.truthLepton_d0[idx[1]]) < 300): continue

            if abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_eta[idx[0]]) < 2.47 and abs(t.truthLepton_eta[idx[1]]) < 2.47: pass_ee = True
            elif abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_pdgId[idx[1]]) == 13 and abs(t.truthLepton_eta[idx[0]]) < 2.5 and  abs(t.truthLepton_eta[idx[0]]) < 2.5: pass_mm =True
            elif (abs(t.truthLepton_pdgId[idx[0]]) == 11 and abs(t.truthLepton_pdgId[idx[1]]) == 13 and abs(t.truthLepton_eta[idx[0]]) < 2.47 and abs(t.truthLepton_eta[idx[1]]) < 2.5) or (abs(t.truthLepton_pdgId[idx[1]]) == 11 and abs(t.truthLepton_pdgId[idx[0]]) == 13 and abs(t.truthLepton_eta[idx[1]]) < 2.47 and abs(t.truthLepton_eta[idx[0]]) < 2.5): pass_em =True

            # select trigger region, don't include |d0| > 2 cut since above ensures it
            if len(e_idx) >=1 and t.truthLepton_pt[e_idx[0]] > 160: trigPass=True #single photon region, should always be true at truth level
            elif len(e_idx)>=2 and t.truthLepton_pt[e_idx[0]] > 60 and t.truthLepton_pt[e_idx[1]] > 60: trigPass=True #diphoton region, should always be true at truth level
            elif (len(m_idx)>=1 and t.truthLepton_pt[m_idx[0]] > 60 and abs(t.truthLepton_eta[m_idx[0]]) < 1.07) or (len(m_idx)>=2 and t.truthLepton_pt[m_idx[0]] > 60 and abs(t.truthLepton_eta[m_idx[0]]) < 1.07 and t.truthLepton_pt[m_idx[1]] > 60 and abs(t.truthLepton_eta[m_idx[1]]) < 1.07): trigPass = True #single muon trigger
            else: trigPass=False

            if not trigPass: continue

            # delta R cut
            if not (math.sqrt((t.truthLepton_eta[idx[0]]-t.truthLepton_eta[idx[1]])**2 + (t.truthLepton_phi[idx[0]]-t.truthLepton_phi[idx[1]])**2) > 0.2) :continue

            if pass_ee:
                hists['ee_']["truth"].Fill(t.truthLepton_pt[idx[0]],  abs(t.truthLepton_d0[idx[0]]),t.mcEventWeight)
            elif pass_mm :
                hists['mm_']["truth"].Fill(t.truthLepton_pt[idx[0]],  abs(t.truthLepton_d0[idx[0]]),t.mcEventWeight)
            elif pass_em :
                hists['em_']["truth"].Fill(t.truthLepton_pt[idx[0]],  abs(t.truthLepton_d0[idx[0]]),t.mcEventWeight)

for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt
        t = getTree("%s"%(d))
        if t==-1: continue

        #ds_name = glob.glob("%s*_%s_*_%sns*_trees.root"%(d,m,lt))[0].split("/")[-1]
        #print "DS name:", ds_name

        mass = int(m)
        lifetime = 0
        if lt == "0p01": lifetime = -2
        elif lt == "0p1": lifetime = -1
        elif lt == "1": lifetime = 0
        elif lt == "10": lifetime = 1

        
        # Loop over SRs to get integrals
        # Using eventNumber as an arbitrary event-level variable
        for sr in SRs:
            ev_weight = 1 
            h_total = getHist(t, "h_ev_%s_%s_%s_total"%(sr,m,lt), "eventNumber", sel_name=str(ev_weight))
            hists[sr]["total"].Fill(mass, lifetime, h_total.Integral())


        t = getTree("%s"%(d))
        if t==-1: continue

        maxEntries = t.GetEntries()
        print maxEntries

        for entry in xrange(maxEntries):
            t.GetEntry(entry)

            if not(t.lepton_n_baseline >= 2):continue 
            if not(t.triggerRegion_pass):continue 
            if not (t.muon_n_cosmic == 0): continue
 
            if t.electron_n_signal >=2 and  t.electron_isSignal[0] and t.electron_isSignal[1] and t.deltaR > 0.2 and t.channel == 0:
                hists['ee_']["pass"].Fill(t.electron_pt[0], abs(t.electron_d0[0]),(t.muSF*t.mu_sel_sf*t.mu_trig_sf*t.el_reco_sf*t.pileupWeight))
            elif t.muon_n_signal >= 2 and t.muon_isSignal[0] and t.muon_isSignal[1] and t.deltaR > 0.2 and t.channel == 1:
                hists['mm_']["pass"].Fill(t.muon_pt[0], abs(t.muon_IDtrack_d0[0]),t.muSF*t.mu_sel_sf*t.mu_trig_sf*t.el_reco_sf*t.pileupWeight)


for sr in SRs:

    print sr

    h = addOverflow2D(hists[sr]["pass"])
    h.GetXaxis().SetTitle("p_{T} [GeV]")
    h.GetYaxis().SetTitle("|d_{0}| [mm]")

    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_pass_"+sr, "can_pass_"+sr)
    #ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kGray+2)
    h_temp.Draw("text90 same")
    #raw_input("...")
    savename = "plots/"+sr+append+"_reco_d0_pt"
 
    can.SaveAs(savename+'.pdf')

    h = addOverflow2D(hists[sr]["truth"])
    h.GetXaxis().SetTitle("p_{T} [GeV]")
    h.GetYaxis().SetTitle("|d_{0}| [mm]")
    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_truth_"+sr, "can_truth_"+sr)
    #ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kGray+2)
    h_temp.Draw("text90 same")
    #raw_input("...")                                                                                                                     
    savename = "plots/"+sr+append+"_truth_d0_pt"

    can.SaveAs(savename+'.pdf')


    h = addOverflow2D(hists[sr]["total"])
    h.GetXaxis().SetTitle("p_{T} [GeV]")
    h.GetYaxis().SetTitle("|d_{0}| [mm]")

    h.SetMinimum(.001)
    h.SetMaximum(10000)

    can = ROOT.TCanvas("can_total_"+sr, "can_total_"+sr)
    #ROOT.gPad.SetLogz(1)
    h.Draw("colz")
    h_temp = h.Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kGray+2)
    h_temp.Draw("text90 same")
    #raw_input("...")                                                                                                                     
    savename = "plots/"+sr+append+"_total_d0_pt.pdf"

    can.SaveAs(savename)


for sr in SRs:

    ROOT.gStyle.SetPaintTextFormat("#.3g");

    if sr =='ee_': srname = 'electron'
    #elif sr =='em_': srname = 'muon'
    elif sr == 'mm_': srname = 'muon'
 
    # acceptance: truth / total
    hists[sr]["acc"]=ROOT.TH2F(sr+"_acc", sr+"_acc", 11, 50, 600, 30, 0, 300)

    
    #hists[sr]["truth"].Clone(sr+"_acc")
    hists[sr]["acc"].Sumw2()
    hists[sr]["acc"].Divide(addOverflow2D(hists[sr]["truth"]),addOverflow2D(hists[sr]["total"]),1,1, "b")
    hists[sr]["acc"].GetXaxis().SetTitle("p_{T} [GeV]")
    hists[sr]["acc"].GetYaxis().SetTitle("|d_{0}| [mm]")

    hists[sr]["acc"].GetZaxis().SetTitle("Acceptance")
    hists[sr]["acc"].GetZaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetZaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetXaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetXaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetYaxis().SetTitleSize(0.04);
    hists[sr]["acc"].GetYaxis().SetTitleOffset(1.4);
    hists[sr]["acc"].GetZaxis().SetLabelSize(0.04);
    hists[sr]["acc"].SetMarkerSize(0.6)
    hists[sr]["acc"].GetXaxis().SetLabelSize(0.04);
    hists[sr]["acc"].GetYaxis().SetLabelSize(0.04);

    can = ROOT.TCanvas("can_"+sr, "can_"+sr)
    hists[sr]["acc"].SetMinimum(0)
    if append == 'slep':
        hists[sr]["acc"].SetMaximum(0.3)
    else: hists[sr]["acc"].SetMaximum(0.005)
    hists[sr]["acc"].Draw("colz")
    h_temp = hists[sr]["acc"].Clone("text_acc"+sr)
    #h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    #h_temp.Draw("text45 same")
    savename = "plots/"+sr+append+"_acc_pt_d0"

    ATLAS_LABEL(0.2,0.88,1);
    myText(0.28,0.88,1,"Simulation Internal");
    #myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    myText(0.2,0.84,1,srname);

    #can.SaveAs(savename+'.pdf')
    #can.SaveAs(savename+'.eps')
    #can.SaveAs(savename+'.root')


    # efficiency: pass/ truth
    hists[sr]["eff"]=ROOT.TH2F(sr+"_eff", sr+"_eff", 19, 50, 1000, 30, 0, 300)

    hists[sr]["eff"].Sumw2()
    hists[sr]["eff"].Divide(hists[sr]["pass"],hists[sr]["truth"],1,1,"b")
    #hists[sr]["eff"].Divide(hists[sr]["truth"], "b")
    hists[sr]["eff"].GetXaxis().SetTitle("p_{T} [GeV]")
    hists[sr]["eff"].GetYaxis().SetTitle("|d_{0}| [mm]")
    hists[sr]["eff"].GetZaxis().SetTitle("Efficiency")

    can = ROOT.TCanvas("can_eff"+sr, "can_eff"+sr)
    hists[sr]["eff"].SetMinimum(0)
    hists[sr]["eff"].SetMaximum(0.4)
    hists[sr]["eff"].Draw("colz")
    h_temp = hists[sr]["eff"].Clone("text_eff"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kBlack)#kGray+2)
    #h_temp.Draw("text45 same")
    savename = "plots/"+sr+append+"_eff_pt_d0"

    ATLAS_LABEL(0.2,0.88,1);
    myText(0.27,0.88,1,"Simulation Internal");
    #myText(0.2,0.84,1,"#sqrt{s} = 13 TeV, 139 fb^{-1}")
    myText(0.2,0.84,1,srname);

    can.SaveAs(savename+'.pdf')
    can.SaveAs(savename+'.eps')
    can.SaveAs(savename+'.root')
