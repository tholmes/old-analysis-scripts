import glob
import ROOT
import os
import math

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.gStyle.SetPaintTextFormat(".2f");

reweight_by = 0.1
xmin = -12
xmax = -1
nbins= xmax - xmin

#t = getTree("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/399052.root") # 600 GeV, 1ns
#t = getTree("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/399050.root") # 600 GeV, 0.01 ns
#t = getTree("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/399051.root") # 600 GeV, 0.1 ns
#t = getTree("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/399036.root")
t = getTree("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/399017.root")

h_lt_1ns = ROOT.TH2F("h_lt_1ns", "h_lt_1ns",    nbins, xmin, xmax, nbins, xmin, xmax)
h_lt_10ns = ROOT.TH2F("h_lt_10ns", "h_lt_10ns", nbins, xmin, xmax, nbins, xmin, xmax)

for event in t:
    if len(event.truthSparticle_properDecayTime) != 2:
        print len(event.truthSparticle_properDecayTime)
        continue
    h_lt_1ns.Fill(math.log10(event.truthSparticle_properDecayTime[0]), math.log10(event.truthSparticle_properDecayTime[1]))
    h_lt_10ns.Fill(math.log10(event.truthSparticle_properDecayTime[0]*reweight_by), math.log10(event.truthSparticle_properDecayTime[1]*reweight_by))

h_weight = h_lt_10ns.Clone("weight")
h_weight.Divide(h_lt_1ns)

h_pass = ROOT.TH1F("h_pass","h_pass",1,0,1)
h_pass_weighted = ROOT.TH1F("h_pass_weighted", "h_pass_weighted", 1,0,1)
for event in t:
    if event.lepton_n_signal>=2:
        h_pass.Fill(0.5)
        h_pass_weighted.Fill(0.5, h_weight.GetBinContent(h_weight.GetXaxis().FindBin(math.log10(event.truthSparticle_properDecayTime[0])), h_weight.GetYaxis().FindBin(math.log10(event.truthSparticle_properDecayTime[1]))))

print "Reweighting factor:", reweight_by
print "Unweighted passing: %.2f +/- %.2f"%(h_pass.GetBinContent(1), h_pass.GetBinError(1))
print "Unweighted passing: %.2f +/- %.2f"%(h_pass_weighted.GetBinContent(1), h_pass_weighted.GetBinError(1))

can = ROOT.TCanvas("can", "can")
h_weight.Draw("colz")
raw_input("...")



