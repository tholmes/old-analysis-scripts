# Goal: Study whether or not using objects in the photon container is useful

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
#ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/staus"
#d = "/eos/user/l/lhoryn/highd0/stau/"
d = "/eos/user/l/lhoryn/highd0/120318/"

lep0_pt = 75
lep1_pt = 75
lep_d0 = 3.

# Define grid
lifetimes = ["0p01", "0p1", "1"]
lifetimes = ["0p1"]
#masses = ["50", "100", "200", "300", "400", "500", "700"]
masses = ["100", "300", "500"]
masses = ["300", "500"]
#maximums = {"100":22, "300":7.5, "500":2.2}

# Channels
# 0 = ee, 1 = mm, 2 = em, 3 = me

variables = {
        "pt":  {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "True Electron p_{T} [GeV]"},
        "d0":  {"attr": "abs(OBJ_d0[0])",       "nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "True Electron d_{0} [mm]"},
        }

# Loop over samples
cans = {}
for lt in lifetimes:
    for m in masses:

        #print "\nWorking with point lt =",lt,"m =",m
        #files = glob.glob("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        #print "Found", len(files), "files."
        #t = getTree("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        #dframe = ROOT.ROOT.RDataFrame(t)

        # Get crossection
        #xs = getXS(files[0].split("/")[-2].split(".")[3])
        #n_events = getNEvents("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))

        print "\nWorking with point lt =",lt,"m =",m
        files = glob.glob("%s/*_%s_*_trees.root/*.root"%(d,m))
        print "Found", len(files), "files."
        t = getTree("%s/*_%s_*_trees.root/*.root"%(d,m))
        dframe = ROOT.ROOT.RDataFrame(t)

        # Get crossection
        xs = getXS(files[0].split("/")[-2].split(".")[3])
        n_events = getNEvents("%s/*_%s_*_trees.root/*.root"%(d,m))
        print "Cross section:", xs, "N events:", n_events
        ev_weight = lumi*xs/n_events
        print "Found", dframe.Count().GetValue(), "events."

        # Do basic event skimming (only ee events)
        n_e = "int n_e = 0; for (int i=0; i<truthLepton_pt.size(); i++) { if (abs(truthLepton_pdgId[i])==11) n_e++; } return n_e;"
        df_ne = dframe.Define("n_true_e", n_e)
        df_ee = df_ne.Filter("n_true_e > 1", "ee selection")

        # Select all truth-matched electrons
        matched_e_pt = '''auto v = electron_pt; v.clear();
        for (int i=0; i<electron_pt.size(); i++)
            for (int j=0; j<truthLepton_barcode.size(); j++)
                if ( abs(truthLepton_pdgId[j])==11 && electron_passOR[i] && electron_truthMatchedBarcode[i] == truthLepton_barcode[j] )
                    v.push_back((double)truthLepton_pt[i]);
        return v;'''
        matched_e_d0 = matched_e_pt.replace("truthLepton_pt", "truthLepton_d0")
        df_pt = df_ee.Define("tm_electron_pt", matched_e_pt)
        df_d0 = df_pt.Define("tm_electron_d0", matched_e_d0)

        # Select all truth-matched photons. To start, just use a similar pT (within 10%)
        matched_g_pt_conv = '''auto v = photon_pt; v.clear();
        for (int i=0; i<photon_pt.size(); i++)
            for (int j=0; j<truthLepton_pt.size(); j++)
                if ( abs(truthLepton_pdgId[j])==11 && photon_isConversion[i] && photon_passOR[i] && photon_truthMatchedBarcode[i]==-1 && abs(photon_pt[i] - truthLepton_pt[j])/truthLepton_pt[j] <.1 )
                    v.push_back((double)truthLepton_pt[i]);
        return v;'''
        matched_g_d0_conv = matched_g_pt_conv.replace("(double)truthLepton_pt", "(double)truthLepton_d0")
        matched_g_pt = matched_g_pt_conv.replace("photon_isConversion[i]", "!photon_isConversion[i]")
        matched_g_d0 = matched_g_d0_conv.replace("photon_isConversion[i]", "!photon_isConversion[i]")
        df_gpt = df_d0.Define("tm_photon_pt", matched_g_pt)
        df_gd0 = df_gpt.Define("tm_photon_d0", matched_g_d0)
        df_cgpt = df_gd0.Define("tm_conv_photon_pt", matched_g_pt_conv)
        df_cgd0 = df_cgpt.Define("tm_conv_photon_d0", matched_g_d0_conv)

        df = df_cgd0
        h_matched_e_pt = df.Histo1D(("Reconstructed Electrons", variables["d0"]["xlabel"], variables["d0"]["nbins"], variables["d0"]["xmin"], variables["d0"]["xmax"]), "tm_electron_d0")
        h_matched_g_pt = df.Histo1D(("Reconstructed Photons", variables["d0"]["xlabel"], variables["d0"]["nbins"], variables["d0"]["xmin"], variables["d0"]["xmax"]), "tm_photon_d0")
        h_matched_gc_pt = df.Histo1D(("Reconstructed Conversions", variables["d0"]["xlabel"], variables["d0"]["nbins"], variables["d0"]["xmin"], variables["d0"]["xmax"]), "tm_conv_photon_d0")
        h_true_e_pt = df.Histo1D(("truthLepton_d0", variables["d0"]["xlabel"], variables["d0"]["nbins"], variables["d0"]["xmin"], variables["d0"]["xmax"]), "truthLepton_d0")
        h_matched_g_pt.SetLineColor(ROOT.kBlue-4)
        h_matched_gc_pt.SetLineColor(ROOT.kViolet-4)
        h_matched_e_pt.SetLineColor(ROOT.kRed-4)

        #h_true_e_pt.Draw()
        #h_matched_e_pt.Draw("same")
        #h_matched_g_pt.Draw("same")

        h_g_eff_pt = h_matched_g_pt.Clone("h_g_eff_pt")
        h_gc_eff_pt = h_matched_gc_pt.Clone("h_gc_eff_pt")
        h_e_eff_pt = h_matched_e_pt.Clone("h_e_eff_pt")
        h_e_true = h_true_e_pt.Clone("h_e_true")

        h_g_eff_pt.Divide(h_e_true)
        h_gc_eff_pt.Divide(h_e_true)
        h_e_eff_pt.Divide(h_e_true)
        h_all_eff = h_g_eff_pt.Clone("h_all_eff")
        h_all_eff.Add(h_gc_eff_pt)
        h_all_eff.Add(h_e_eff_pt)
        h_all_eff.SetLineColor(ROOT.kGreen-4)

        h_all_eff.GetXaxis().SetTitle(variables["d0"]["xlabel"])
        h_all_eff.SetMinimum(0)
        h_all_eff.SetMaximum(1)
        h_all_eff.Draw()
        h_e_eff_pt.Draw("same")
        h_g_eff_pt.Draw("same")
        h_gc_eff_pt.Draw("same")

        raw_input("...")


        # Cutflow for fun
        report = dframe.Report()
        report.Print()

        exit()
