
import ROOT
import glob
import os
import math

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")

d = '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/already_merged/user.tholmes.data18_13TeV.periodO.physics_Main.SUSY15_v4_Feb27_trees.root/*.root'

files = glob.glob(d)

for fileName in files:

    t = getTree(fileName)  
    if t==-1: continue

    maxEntries = t.GetEntries()

    n_0l = 0
    n_gt0l = 0
    for entry in xrange(maxEntries):
        t.GetEntry(entry)

        if not (t.runNumber == 361862): continue

        '''
        if not (t.eventNumber == 441475007 or 
                t.eventNumber == 445814339 or 
                t.eventNumber == 416795593 or
                t.eventNumber == 468047277 or
                t.eventNumber == 487073738 or
                t.eventNumber == 505797189 or
                t.eventNumber == 519450183 or
                t.eventNumber == 537637596 or
                t.eventNumber == 554986507 or
                t.eventNumber == 562214952 or
                t.eventNumber == 573197160 or
                t.eventNumber == 584967730 or
                t.eventNumber == 607014279 or
                t.eventNumber == 632179185 or
                t.eventNumber == 653251390 or
                t.eventNumber == 652868456 or
                t.eventNumber == 706298735 or
                t.eventNumber == 721924519 or
                t.eventNumber == 737786855 or
                t.eventNumber == 740525181 or
                t.eventNumber == 760173863 or
                t.eventNumber == 779129052 or
                t.eventNumber == 819483003 or
                t.eventNumber == 859549611 or
                t.eventNumber == 879879961 or
                t.eventNumber == 886620486 or
                t.eventNumber == 915642578 or
                t.eventNumber == 973928096 or
                t.eventNumber == 986364228 or
                t.eventNumber == 1034807511 or
                t.eventNumber == 1032389532 or
                t.eventNumber == 1074011648 or
                t.eventNumber == 1071588514 or
                t.eventNumber == 1236689000) : continue
        '''
        if (t.lepton_n==0): n_0l +=1
        else: n_gt0l += 1
        #print t.runNumber, t.eventNumber, t.MET, t.lepton_n, t.muon_n, t.electron_n
        '''
        for i in range (0, len(t.muon_pt)):
            print 'muon'
            print t.muon_pt[i], t.muon_eta[i], t.muon_phi[i], t.muon_IDtrack_d0[i]

        for i in range (0, len(t.electron_pt)):
            print 'electron'
            print t.electron_pt[i], t.electron_eta[i], t.electron_phi[i], t.electron_d0[i]

        '''

    print t.runNumber, 'events with 0 leptons', n_0l
    print t.runNumber, 'events with at least 1 lepton', n_gt0l
        
