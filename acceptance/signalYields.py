
import glob
import ROOT
import os
import math
import signalMap

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/"

signal = 'slep'

cuts = []

common_cuts = 'lepton_n_baseline >= 2 &&  triggerRegion_pass'

ee_select = 'channel == 0'
emu_select  = '(channel == 2 || channel == 3)'
mumu_select = 'channel == 1'


sr_mm = common_cuts + '&& muon_isSignal[0] && muon_isSignal[1] && deltaR > 0.2 && abs(muon_t0avg[0]) < 30 && abs(muon_t0avg[1]) < 30'+' && ' + mumu_select
sr_ee = common_cuts + '&& electron_isSignal[0] && electron_isSignal[1] && deltaR > 0.2 && ' + ee_select
sr_em = common_cuts + '&&'+ emu_select +  '&& electron_isSignal[0] && muon_isSignal[0] && abs(muon_t0avg[0]) < 30 && deltaR > 0.2 &&' + emu_select

# Hand-done cross-sections [pb], per flavor
lumi = 139000.

lifetimes = ["0p01", "0p1", "1"]
masses = ["50", "100", "200", "300", "400", "500", "600", "700", "800"]

# Choose signals
signalList = []
if signal == 'slep':
    cuts.append(sr_ee)
    cuts.append(sr_mm)
    ntup = 'Slep'
elif signal == 'stau':
    cuts.append(sr_ee)
    cuts.append(sr_mm)
    cuts.append(sr_em)
    ntup = 'Stau'

cut_str = ''

ofile = open(ntup + '.csv', 'w')

for lt in lifetimes:
    for m in masses:

        outstr = ''
 
        for j in range(0,len(cuts)):
            cut_str = cuts[j]
 
            ds = d+'*'+ ntup +'_*'+m+'_0_'+lt+'ns*.root'
        
            t = getTree(ds+'/*.root')
            if t==-1: continue

            #normalize
            ds_name = glob.glob(ds)[0].split("/")[-1]
            xs = getXS(ds_name.split(".")[3])
            n_events = getNEvents(ds+'/*root')
            ev_weight = lumi*xs/n_events
            
            dsid = ds_name.split(".")[3]
            mass = int(m)
            lifetime = 0
            if lt == "0p01": lifetime = -2
            elif lt == "0p1": lifetime = -1
            elif lt == "1": lifetime = 0    


            # get number of events
            sigName = str(j)+'_'+lt+'_'+m
            
            t.Draw('triggerRegion_pass >> %s(2,0,2)'%(sigName), str(ev_weight)+'*mcEventWeight*('+cut_str+')')
            h = ROOT.gDirectory.Get(sigName)
            
            total = h.GetBinContent(2)
            err = h.GetBinError(2)

            if j == 0: outstr += dsid + ' , '+ m + ' , '+ str(lifetime)
            outstr += ' , '+ str(total) 

        if len(outstr) > 0: ofile.write(outstr+'\n')

