
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")

# Define grid
lifetimes = ["0p01", "0p1"]
masses = ["50", "100", "200", "300", "400", "500"]

# Define SRs
signal_regions = {"SR0": ""}

for sr in signal_regions:

    for lt in lifetimes:
        for m in masses:

            files = glob.glob("/afs/cern.ch/work/t/tholmes/LLP/user.lhoryn.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_%s_0_%sns.062018_trees.root/*.root"%(m, lt))
            if len(files)==0:
                #print "Couldn't find files for mass %s, lifetime %s"%(m, lt)
                continue

            print "Using file with mass %s, lifetime %s"%(m, lt)
            t = ROOT.TChain("trees_SR_highd0_")
            for f in files: t.Add(f)

            h_mu_d0  = ROOT.TH1F("Muon_d0", "Muon_d0", 50, 0., 10.)
            h_el_d0  = ROOT.TH1F("Electron_d0", "Electron_d0", 50, 0., 10.)
            h_mu_pt  = ROOT.TH1F("Muon_pt", "Muon_pt", 50, 0., 500.)
            h_el_pt  = ROOT.TH1F("Electron_pt", "Electron_pt", 50, 0., 500.)
            h_mu_eta = ROOT.TH1F("Muon_eta", "Muon_eta", 36, -3., 3.)
            h_el_eta = ROOT.TH1F("Electron_eta", "Electron_eta", 36, -3., 3.)

            h_tmu_d0  = ROOT.TH1F("TMuon_d0", "TMuon_d0", 50, 0., 10.)
            h_tel_d0  = ROOT.TH1F("TElectron_d0", "TElectron_d0", 50, 0., 10.)
            h_tmu_pt  = ROOT.TH1F("TMuon_pt", "TMuon_pt", 50, 0., 500.)
            h_tel_pt  = ROOT.TH1F("TElectron_pt", "TElectron_pt", 50, 0., 500.)
            h_tmu_eta = ROOT.TH1F("TMuon_eta", "TMuon_eta", 36, -3., 3.)
            h_tel_eta = ROOT.TH1F("TElectron_eta", "TElectron_eta", 36, -3., 3.)

            t.Draw("truthleptond0>>TMuon_d0",       "(truthLeptonPdgId==13 || truthLeptonPdgId==-13) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            t.Draw("truthleptonPt>>TMuon_pt",       "(truthLeptonPdgId==13 || truthLeptonPdgId==-13) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            t.Draw("truthleptonEta>>TMuon_eta",     "(truthLeptonPdgId==13 || truthLeptonPdgId==-13) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            t.Draw("truthleptond0>>TElectron_d0",   "(truthLeptonPdgId==11 || truthLeptonPdgId==-11) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            t.Draw("truthleptonPt>>TElectron_pt",   "(truthLeptonPdgId==11 || truthLeptonPdgId==-11) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            t.Draw("truthleptonEta>>TElectron_eta", "(truthLeptonPdgId==11 || truthLeptonPdgId==-11) && (truthleptonEta<2.5 && truthleptonEta>-2.5)")
            #h_mu_d0.Scale(1./h_mu_d0.Integral())
            #h_el_d0.Scale(1./h_el_d0.Integral())

            for event in t:

                n_muons = len(event.muon_pt)
                n_els = len(event.electron_pt)

                for i in xrange(n_muons):
                    #if event.muon_pt[i]<150: continue
                    barcode = event.muon_truthMatchedBarcode[i]
                    found_match = False
                    true_d0 = 0
                    if barcode>=0:
                        for j in xrange(len(event.truthleptonBarcode)):
                            if barcode == event.truthleptonBarcode[j] and abs(event.truthleptonEta[j])<2.5:
                                found_match = True
                                true_d0 = event.truthleptond0[j]
                                break
                    if not found_match: continue

                    #h_mu_d0.Fill(abs(event.muon_d0[i]))
                    h_mu_d0.Fill(true_d0)
                    h_mu_pt.Fill(event.muon_pt[i])
                    h_mu_eta.Fill(event.muon_eta[i])

                for i in xrange(n_els):
                    #if event.electron_pt[i]<150: continue
                    barcode = event.electron_truthMatchedBarcode[i]
                    found_match = False
                    true_d0 = 0
                    if barcode>=0:
                        for j in xrange(len(event.truthleptonBarcode)):
                            if barcode == event.truthleptonBarcode[j] and abs(event.truthleptonEta[j])<2.5:
                                found_match = True
                                true_d0 = event.truthleptond0[j]
                                break
                    if not found_match: continue

                    #h_el_d0.Fill(abs(event.electron_d0[i]))
                    h_el_d0.Fill(true_d0)
                    h_el_pt.Fill(event.electron_pt[i])
                    h_el_eta.Fill(event.electron_eta[i])

            for var in ["d0", "pt", "eta"]:

                h_mu = eval("h_mu_%s"%var)
                h_el = eval("h_el_%s"%var)
                h_tmu = eval("h_tmu_%s"%var)
                h_tel = eval("h_tel_%s"%var)

                can = ROOT.TCanvas("can_%s"%var, "can_%s"%var)

                ymax = getMax([h_mu, h_el])
                h_mu.SetMaximum(1.1*ymax)
                h_mu.SetLineColor(ROOT.kBlue)
                h_el.SetLineColor(ROOT.kRed)
                h_tmu.SetLineColor(ROOT.kBlue)
                h_tel.SetLineColor(ROOT.kRed)
                h_tmu.SetLineStyle(5)
                h_tel.SetLineStyle(5)
                h_mu.GetXaxis().SetTitle(var)

                h_mu.Draw("hist")
                h_el.Draw("hist same")
                h_tmu.Draw("hist same")
                h_tel.Draw("hist same")

                leg = ROOT.TLegend(.6,.7,.8,.87)
                leg.AddEntry(h_mu, "Reco Muons", "l")
                leg.AddEntry(h_el, "Reco Electrons", "l")
                leg.AddEntry(h_tmu, "Truth Muons", "l")
                leg.AddEntry(h_tel, "Truth Electrons", "l")
                leg.Draw("same")

                raw_input("...")
                can.SaveAs("plots/%s_%s_%s.pdf"%(var, m, lt))

