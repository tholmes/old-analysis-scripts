# Goal: Make plots of photon-related electron backgrounds compared to signal

import collections
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kRainBow)

lumi = 140000
variables = {
        "pt":       {"nbins": 12,    "xmin": 0.,     "xmax": 120.,     "xlabel": "Signal Lepton p_{T} [GeV]"},
        "pt0":       {"nbins": 12,    "xmin": 0.,     "xmax": 120.,     "xlabel": "Leading Lepton p_{T} [GeV]"},
        "pt1":       {"nbins": 12,    "xmin": 0.,     "xmax": 120.,     "xlabel": "Subleading Lepton p_{T} [GeV]"},
        "d0":       {"nbins": 10,    "xmin": 0.,   "xmax": 50.,       "xlabel": "Signal Lepton d_{0} [mm]"},
        "d00":       {"nbins": 10,    "xmin": 0.,   "xmax": 50.,       "xlabel": "Leading Lepton d_{0} [mm]"},
        "d01":       {"nbins": 10,    "xmin": 0.,   "xmax": 50.,       "xlabel": "Subleading Lepton d_{0} [mm]"},
        "eta":      {"nbins": 10,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Signal Lepton #eta"},
        "eta0":      {"nbins": 10,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Leading Lepton #eta"},
        "eta1":      {"nbins": 10,    "xmin": -2.5,   "xmax": 2.5,       "xlabel": "Subleading Lepton #eta"},
        "phi":      {"nbins": 20,    "xmin": -3.2,   "xmax": 3.2,       "xlabel": "Signal Lepton #phi"},
        "rhit":     {"nbins": 200,    "xmin": 0.,   "xmax": 600.,       "xlabel": "Radius of First Hit"},
        "npix":     {"nbins": 6,    "xmin": 0.,   "xmax": 6.,       "xlabel": "Number of Pixel Hits"},
        "nsct":     {"nbins": 16,    "xmin": 0.,   "xmax": 16.,       "xlabel": "Number of SCT Hits"},
        "nsi":      {"nbins": 22,    "xmin": 0.,   "xmax": 22.,       "xlabel": "Number of Silicon Hits"},
        "nholes":   {"nbins": 12,    "xmin": -0.5,   "xmax": 12.5,     "xlabel": "Number of missing hits after first hit"},
        "dpt":      {"nbins": 20,    "xmin": -1.,   "xmax": 4,          "xlabel": "Signal Lepton (p_{T}^{track}-p_{T})/p_{T}"},
        "chi2":     {"nbins": 20,    "xmin": 0.,   "xmax": 10,          "xlabel": "Signal Lepton #chi^{2}"},
        "fcloose":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCLoose"},
        "fctight":  {"nbins": 2,     "xmin": -0.5, "xmax": 1.5,            "xlabel": "Signal Lepton Passes FCTight"},
        "met":  {"nbins": 100,     "xmin": 0., "xmax": 1000,            "xlabel": "Missing ET [GeV]"},
        }


#signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal"
signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"

# Select signals for comparison
signals = ["SlepSlep*100*_0p1", "StauStau*100*_0p1"] #, "SlepSlep*300*_1ns", "SlepSlep*500*_0p1", "StauStau*300*_0p1"]

def getVarHist(df, var, varname, s, sample_label, var2 = "", varname2 = ""):
    h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname)
    h.Scale(samples[s]["weight"])
    h.GetXaxis().SetTitle(variables[var]["xlabel"])
    h.GetYaxis().SetTitle("Events")
    return h

# Collect samples and weights
cans = {}
dfs = {}
samples = {}
for samplename in signals:
    # Get filename
    base_name = ""  # To be used for getting xs
    file_names = [] # To be used for collecting all samples
    for d in [signal_d]:
        sets = glob.glob("%s/*%s*.root"%(d,samplename))
        if len(sets)>=1:
            base_name = sets[0].split("/")[-1]
            file_names = sets
            print "Found", len(file_names), "samples(s):\n\t", file_names
            break
    if base_name == "":
        print "Couldn't find file for sample", samplename
        exit()

    # Normalize
    event_weight = 1
    if samplename in signals:
        xs = getXS(base_name.split(".")[3])
        n_events = 0
        for f in file_names:
            n_events += getNEvents(f+"/*.root")
        event_weight = lumi*xs/n_events

    # Add to tree
    is_signal = False
    if samplename in signals: is_signal = True
    is_data = False
    #if samplename in data: is_data = True
    samples[samplename] = {"files": file_names, "weight": event_weight, "hists": {}, "effhists": {}, "is_signal": is_signal, "is_data": is_data}

# Define selection
append = ""

# Define selections
trigger_sels = collections.OrderedDict()
trigger_sels["all"] = "1==1"
trigger_sels["all2l"] = "(muon_n + electron_n) > 1"
trigger_sels["current_trigger"] = "triggerRegion_pass==1"
trigger_sels["2mu40"] =  "muon_n > 1 && muon_pt[1] > 40"
trigger_sels["mu40g40"] =  "muon_n > 0 && electron_n > 0 && electron_pt[0] > 40 && muon_pt[0] > 40"
trigger_sels["current_plus"] = "(triggerRegion_pass==1) || (muon_n > 1 && muon_pt[1] > 40) || (muon_n > 0 && electron_n > 0 && electron_pt[0] > 40 && muon_pt[0] > 40)"

snames = {
        "all":              "2 true leptons",
        "all2l":            "2 reco leptons, pT > 20",
        "current_trigger":  "Pass current triggers",
        "2mu40":            "2 muons, pT > 40 GeV",
        "mu40g40":          "Muon and electron, pT > 40",
        "current_plus":     "Current || 2 lepton, pT > 40"
        }
signalsamples = {
        "periodI":          "Data, 2017 Period I",
        "photon":           "Photons, p_{T} > 70 GeV, BFilter",
        "SlepSlep*100*_0p01": "100 GeV Sleptons, #tau=0.01 ns",
        "SlepSlep*100*_0p1": "100 GeV Sleptons, #tau=0.1 ns",
        "StauStau*100*_0p1": "100 GeV Staus, #tau=0.1 ns",
        "StauStau*300*_0p1": "Staus, m = 300, #tau=0.1 ns",
        "SlepSlep*300*_1ns": "Sleptons, m = 300, #tau=1 ns",
        "SlepSlep*500*_0p1": "Sleptons, m = 500, #tau=0.1 ns",
        }

for s in samples:

    dfs[s] = {}
    print "\nWorking with sample", s

    # Get dataframe
    t = ROOT.TChain("trees_SR_highd0_")
    for f in samples[s]["files"]: t.Add(f+"/*.root")
    dfs[s]["init"] = ROOT.ROOT.RDataFrame(t)
    print "Found events:", t.GetEntries()

    #df_vars = dframe
    true_lep_pt = '''
            std::vector<float> v;
            for (int i=0; i<truthLepton_eta.size(); i++)
                if (abs(truthLepton_pdgId[i]) == 11 || abs(truthLepton_pdgId[i]) == 13)
                    v.push_back((float)truthLepton_pt[i]);
            return v;'''
    true_lep_d0 = true_lep_pt.replace("truthLepton_pt", "truthLepton_d0")
    true_lep_eta = true_lep_pt.replace("truthLepton_pt", "truthLepton_eta")

    dfs[s]["vars"] = dfs[s]["init"].Define("true_lep_pt", true_lep_pt).Define("true_lep_d0", true_lep_d0).Define("true_lep_eta", true_lep_eta) \
                    .Filter("true_lep_pt.size()>1", "2 true leptons") \
                    .Define("lep0_pt", "true_lep_pt[0]") \
                    .Define("lep1_pt", "true_lep_pt[1]") \
                    .Define("lep0_eta", "true_lep_eta[0]") \
                    .Define("lep1_eta", "true_lep_eta[1]") \
                    .Define("lep0_d0", "true_lep_d0[0]") \
                    .Define("lep1_d0", "true_lep_d0[1]")

    # Do basic event skimming
    for sel in trigger_sels:
        print sel
        samples[s]["hists"][sel] = {}
        samples[s]["effhists"][sel] = {}
        print trigger_sels[sel]
        dfs[s][sel] = dfs[s]["vars"].Filter(trigger_sels[sel], sel)

        # Make plots of basic variables
        #samples[s]["hists"][sel]["met"] = getVarHist(dfs[s][sel], "met", "MET", s, sel)

        #h = dfs[s][sel].Histo1D((sel+"_met", snames[sel], variables["met"]["nbins"], variables["met"]["xmin"], variables["met"]["xmax"]), "MET")
        #h = dfs[s][sel].Histo1D((sel+"_pt0", snames[sel], variables["pt0"]["nbins"], variables["pt0"]["xmin"], variables["pt0"]["xmax"]), "lep0_pt")
        #h.Draw()
        #raw_input("...")
        samples[s]["hists"][sel]["leading_lep_pt"] = getVarHist(dfs[s][sel], "pt0", "lep0_pt", s, sel)
        samples[s]["hists"][sel]["subleading_lep_pt"] = getVarHist(dfs[s][sel], "pt1", "lep1_pt", s, sel)
        samples[s]["hists"][sel]["leading_lep_d0"] = getVarHist(dfs[s][sel], "d00", "lep0_d0", s, sel)
        samples[s]["hists"][sel]["subleading_lep_d0"] = getVarHist(dfs[s][sel], "d01", "lep1_d0", s, sel)
        samples[s]["hists"][sel]["leading_lep_eta"] = getVarHist(dfs[s][sel], "eta0", "lep0_eta", s, sel)
        samples[s]["hists"][sel]["subleading_lep_eta"] = getVarHist(dfs[s][sel], "eta1", "lep1_eta", s, sel)

    # Cutflow for fun
    report = dfs[s]["init"].Report()
    report.Print()

# Collect hists
for s in samples:
    print "sample:", s
    hists = samples[s]["hists"][sel].keys() # This is embarrassing, but also always filled
    for htype in hists:
        print "htype:", htype

        cans[htype] = ROOT.TCanvas(htype, htype)
        y_max = 0
        for sel in trigger_sels:
            y_max = max(y_max, samples[s]["hists"][sel][htype].GetMaximum())
            samples[s]["hists"][sel][htype].SetMarkerStyle(4)
            if not sel=="all":
                samples[s]["effhists"][sel][htype] = samples[s]["hists"][sel][htype].Clone("eff_"+samples[s]["hists"][sel][htype].GetName())
                samples[s]["effhists"][sel][htype].Divide(samples[s]["hists"]["all2l"][htype].Clone())

        for i, sel in enumerate(trigger_sels):
            if i==0:
                #samples[s]["hists"][sel][htype].SetMaximum(10e8)
                #samples[s]["hists"][sel][htype].SetMinimum(10e-1)
                samples[s]["hists"][sel][htype].SetMinimum(0)
                samples[s]["hists"][sel][htype].SetMaximum(1.8*y_max)
                samples[s]["hists"][sel][htype].Draw("e PLC PMC")
            else:
                samples[s]["hists"][sel][htype].Draw("same e PLC PMC")

        leg = cans[htype].BuildLegend(.53,.6,.8,.90)
        leg.Draw()
        #cans[htype].SetLogy()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.80, "Simulation, 140 fb^{-1}")
        text.DrawLatex(0.2,0.76, signalsamples[s])

        #raw_input("...")
        cans[htype].SaveAs("plots/%s_%s_%s.pdf"%(s, htype, append))

        cans[htype+"_eff"] = ROOT.TCanvas(htype+"_eff", htype+"_eff")
        for i, sel in enumerate(trigger_sels):
            if "all" in sel: continue
            samples[s]["effhists"][sel][htype].SetMarkerColor(samples[s]["hists"][sel][htype].GetMarkerColor())
            samples[s]["effhists"][sel][htype].SetLineColor(samples[s]["hists"][sel][htype].GetMarkerColor())
            samples[s]["effhists"][sel][htype].SetMaximum(2)
            samples[s]["effhists"][sel][htype].GetYaxis().SetTitle("Efficiency")
            if "Stau" in s: samples[s]["effhists"][sel][htype].SetMaximum(1.5)
            samples[s]["effhists"][sel][htype].SetMinimum(0)
            if i==0:
                samples[s]["effhists"][sel][htype].Draw("e")
            else:
                samples[s]["effhists"][sel][htype].Draw("same")

        leg = cans[htype+"_eff"].BuildLegend(.53,.6,.8,.90)
        leg.Draw()

        ROOT.ATLASLabel(0.2,0.85, "Internal")
        text = ROOT.TLatex()
        text.SetNDC()
        text.SetTextSize(0.04)
        text.DrawLatex(0.2,0.80, "Simulation, 140 fb^{-1}")
        text.DrawLatex(0.2,0.76, signalsamples[s])

        cans[htype+"_eff"].SaveAs("plots/eff_%s_%s_%s.pdf"%(s, htype, append))

