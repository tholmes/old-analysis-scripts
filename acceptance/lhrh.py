import glob
import ROOT
import os
import math


execfile("../plot_helpers/basic_plotting.py")
execfile("../cosmics/cosmic_helpers.py")
execfile("signalMap.py")

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.gStyle.SetPaintTextFormat("#.3g");


def format2d(hist):
    hist.GetZaxis().SetTitleSize(0.04);
    hist.GetZaxis().SetTitleOffset(1.4);
    hist.GetXaxis().SetTitleSize(0.04);
    hist.GetXaxis().SetTitleOffset(1.4);
    hist.GetYaxis().SetTitleSize(0.04);
    hist.GetYaxis().SetTitleOffset(1.4);
    hist.GetXaxis().SetLabelSize(0.04);
    hist.GetYaxis().SetLabelSize(0.04);

def firstLep(event, i_ignore):
    #print i_ignore 
    l0 = -1
    pt0 = -1

    for il in xrange(len(event.truthLepton_pt)):
        if event.truthLepton_pt[il] > pt0 and (abs(event.truthLepton_pdgId[il]) == 11 or abs(event.truthLepton_pdgId[il]) == 13) and il!= i_ignore:
            pt0 = event.truthLepton_pt[il]
            l0 = il

    return l0

def findRecoMatch(event, barcode):
    for ir in xrange(len(event.lepton_pt)):
        if event.lepton_truthMatchedBarcode[ir] == barcode:
            return ir
    return -1 



names = { 
    "l_mu_d0": { "nb": 10, "low": 0, "high": 300, "label": "true d0", "log": 1},   
    "r_mu_d0": { "nb": 10, "low": 0, "high": 300, "label": "true d0", "log": 1},   
    "l_el_d0": { "nb": 10, "low": 0, "high": 300, "label": "true d0", "log": 1},   
    "r_el_d0": { "nb": 10, "low": 0, "high": 300, "label": "true d0", "log": 1},   
    
    "l_mu_pt": { "nb": 10, "low": 0, "high": 500, "label": "true pt", "log": 1},   
    "r_mu_pt": { "nb": 10, "low": 0, "high": 500, "label": "true pt", "log": 1},   
    "l_el_pt": { "nb": 10, "low": 0, "high": 500, "label": "true pt", "log": 1},   
    "r_el_pt": { "nb": 10, "low": 0, "high": 500, "label": "true pt", "log": 1},   
    
    "l_mu_eta": { "nb": 10, "low": -4, "high": 4, "label": "true eta", "log": 1},   
    "r_mu_eta": { "nb": 10, "low": -4, "high": 4, "label": "true eta", "log": 1},   
    "l_el_eta": { "nb": 10, "low": -4, "high": 4, "label": "true eta", "log": 1},   
    "r_el_eta": { "nb": 10, "low": -4, "high": 4, "label": "true eta", "log": 1},   
    
    "l_mu_phi": { "nb": 10, "low": -4, "high": 4, "label": "true phi", "log": 1},   
    "r_mu_phi": { "nb": 10, "low": -4, "high": 4, "label": "true phi", "log": 1},   
    "l_el_phi": { "nb": 10, "low": -4, "high": 4, "label": "true phi", "log": 1},   
    "r_el_phi": { "nb": 10, "low": -4, "high": 4, "label": "true phi", "log": 1},   
    
    "reco_l_mu_d0": { "nb": 10, "low": 0, "high": 300, "label": "reco d0", "log": 1},   
    "reco_r_mu_d0": { "nb": 10, "low": 0, "high": 300, "label": "reco d0", "log": 1},   
    "reco_l_el_d0": { "nb": 10, "low": 0, "high": 300, "label": "reco d0", "log": 1},   
    "reco_r_el_d0": { "nb": 10, "low": 0, "high": 300, "label": "reco d0", "log": 1},   
    
    "reco_l_mu_pt": { "nb": 10, "low": 0, "high": 500, "label": "reco pt", "log": 1},   
    "reco_r_mu_pt": { "nb": 10, "low": 0, "high": 500, "label": "reco pt", "log": 1},   
    "reco_l_el_pt": { "nb": 10, "low": 0, "high": 500, "label": "reco pt", "log": 1},   
    "reco_r_el_pt": { "nb": 10, "low": 0, "high": 500, "label": "reco pt", "log": 1},   
    
    "reco_l_mu_eta": { "nb": 10, "low": -4, "high": 4, "label": "reco eta", "log": 1},   
    "reco_r_mu_eta": { "nb": 10, "low": -4, "high": 4, "label": "reco eta", "log": 1},   
    "reco_l_el_eta": { "nb": 10, "low": -4, "high": 4, "label": "reco eta", "log": 1},   
    "reco_r_el_eta": { "nb": 10, "low": -4, "high": 4, "label": "reco eta", "log": 1},   
    
    "reco_l_mu_phi": { "nb": 10, "low": -4, "high": 4, "label": "reco phi", "log": 1},   
    "reco_r_mu_phi": { "nb": 10, "low": -4, "high": 4, "label": "reco phi", "log": 1},   
    "reco_l_el_phi": { "nb": 10, "low": -4, "high": 4, "label": "reco phi", "log": 1},   
    "reco_r_el_phi": { "nb": 10, "low": -4, "high": 4, "label": "reco phi", "log": 1},   
}


h = {}
h = setHistos("", names)

mass = "800"
lifetime = "1"

d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/stat_ext_new/signal/"


sig_str = "SlepSlep_" + mass + "_0_" + lifetime + "ns"
print sig_str

try:
    dsid = signals_inv[sig_str]
except:
    print "no dsid found for ", sig_str
    sys.exit()

fstr = "%s%s*.root"%(d,dsid)
t = getTree(fstr)  
print "Found events:", t.GetEntries()

for event in t:
    
    if len(event.truthLepton_pt) < 2 : continue
     
    il0 = -1
    il1 = -1

    ## define signal regions based on the two highest pt electrons/muons
    il0 = firstLep(event, -1) 
    il1 = firstLep(event, il0) 
    
    if il0 == -1 or il1 == -1: continue
    
    for i in [il0, il1]:
        k = ""
        if abs(event.truthLepton_pdgId[i]) == 11:
            if abs(event.truthLepton_generatorParent_pdgId[i])  == 1000011:
                k = "l_el"
            if abs(event.truthLepton_generatorParent_pdgId[i])  == 2000011:
                k = "r_el"
        if abs(event.truthLepton_pdgId[i]) == 13:
            if abs(event.truthLepton_generatorParent_pdgId[i])  == 1000013:
                k = "l_mu"
            if abs(event.truthLepton_generatorParent_pdgId[i])  == 2000013:
                k = "r_mu"

        if k == "":
            print "alert~"
            print abs(event.truthLepton_pdgId[i])
            print abs(event.truthLepton_generatorParent_pdgId[i])
            continue

        if event.SRee or event.SRmm: 
            h[k+"_d0"].Fill(abs(event.truthLepton_d0[i]))
            h[k+"_pt"].Fill(event.truthLepton_pt[i])
            h[k+"_eta"].Fill(event.truthLepton_eta[i])
            h[k+"_phi"].Fill(event.truthLepton_phi[i])
        
        ir = findRecoMatch(event, event.truthLepton_barcode[i])
        if ir != -1 and (event.SRmm or event.SRee):
            h["reco_"+k+"_d0"].Fill(abs(event.lepton_d0[ir]))
            h["reco_"+k+"_pt"].Fill(event.lepton_pt[ir])
            h["reco_"+k+"_eta"].Fill(event.lepton_eta[ir])
            h["reco_"+k+"_phi"].Fill(event.lepton_phi[ir])



vv = ["d0","pt","eta","phi"]
leps = ["el","mu"]
c = ROOT.TCanvas()

for v in vv: 
    for l in leps:

        h["r_%s_%s"%(l, v)].Scale(1./ h["r_%s_%s"%(l, v)].Integral())
        h["l_%s_%s"%(l, v)].Scale(1./ h["l_%s_%s"%(l, v)].Integral())
        
        h["r_%s_%s"%(l, v)].SetLineColor(ROOT.kRed)

        leg = ROOT.TLegend(0.70,0.80,0.90,0.92)
        leg.AddEntry(h["r_%s_%s"%(l, v)], "RH "+l, "l")
        leg.AddEntry(h["l_%s_%s"%(l, v)], "LH "+l, "l")
        leg.Draw("same")
        
        h["l_%s_%s"%(l, v)].SetMaximum(getMaximum([h["l_%s_%s"%(l, v)],h["r_%s_%s"%(l, v)]]))
        h["l_%s_%s"%(l, v)].Draw("histe")
        h["r_%s_%s"%(l, v)].Draw("histesame")
        leg.Draw("same")
                 
        c.SaveAs("plots/LRcomp_%s_%s_%s_%s.pdf"%(mass, lifetime, l, v))
        
        h["reco_r_%s_%s"%(l, v)].Scale(1./ h["reco_r_%s_%s"%(l, v)].Integral())
        h["reco_l_%s_%s"%(l, v)].Scale(1./ h["reco_l_%s_%s"%(l, v)].Integral())
        
        h["reco_r_%s_%s"%(l, v)].SetLineColor(ROOT.kRed)

        leg = ROOT.TLegend(0.70,0.80,0.90,0.92)
        leg.AddEntry(h["reco_r_%s_%s"%(l, v)], "RH "+l, "l")
        leg.AddEntry(h["reco_l_%s_%s"%(l, v)], "LH "+l, "l")
        leg.Draw("same")
        
        h["reco_l_%s_%s"%(l, v)].SetMaximum(getMaximum([h["reco_l_%s_%s"%(l, v)],h["reco_r_%s_%s"%(l, v)]]))
        h["reco_l_%s_%s"%(l, v)].Draw("histe")
        h["reco_r_%s_%s"%(l, v)].Draw("histesame")
        leg.Draw("same")
                 
        c.SaveAs("plots/LRcomp_reco_%s_%s_%s_%s.pdf"%(mass, lifetime, l, v))



c.Clear()
h1 = getHist(t, "tmp1", "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight", sel_name="SRee && abs(truthSparticle_pdgId[0]) == 1000011")
h2 = getHist(t, "tmp2", "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight", sel_name="SRee && abs(truthSparticle_pdgId[0]) == 2000011")

h1.GetXaxis().SetTitle("all weights")
h2.SetLineColor(ROOT.kRed)
h1.SetLineColor(ROOT.kBlack)
leg = ROOT.TLegend(0.70,0.80,0.90,0.92)
leg.AddEntry(h1, "LH e", "l")
leg.AddEntry(h2, "RH e", "l")
h1.Draw("hist")
h2.Draw("hist same")
leg.Draw("same")
c.SaveAs("plots/weight_e.pdf")
c.Clear()

#h3 = getHist(t, "tmp3", "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight", sel_name="SRmm && abs(truthSparticle_pdgId[0]) == 1000013")
#h4 = getHist(t, "tmp4", "lumi_weight*mcEventWeight*muSF*mu_sel_sf*mu_trig_sf*disp_sf*el_reco_sf*pileupWeight", sel_name="SRmm && abs(truthSparticle_pdgId[0]) == 2000013")
h3 = getHist(t, "tmp3", "muSF*mu_sel_sf*mu_trig_sf", sel_name="SRmm && abs(truthSparticle_pdgId[0]) == 1000013")
h4 = getHist(t, "tmp4", "muSF*mu_sel_sf*mu_trig_sf", sel_name="SRmm && abs(truthSparticle_pdgId[0]) == 2000013")

h3.GetXaxis().SetTitle("all weights")
h4.SetLineColor(ROOT.kRed)
h4.Scale(1./h4.Integral())
h3.Scale(1./h3.Integral())
h3.SetLineColor(ROOT.kBlack)
leg = ROOT.TLegend(0.70,0.80,0.90,0.92)
leg.AddEntry(h3, "LH mu", "l")
leg.AddEntry(h4, "RH mu", "l")
h3.Draw("hist")
h4.Draw("hist same")
leg.Draw("same")
c.SaveAs("plots/weight_m.pdf")
c.Clear()
