# Goal: Make plots of selection efficiency as a function of various cuts

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/staus"
#d = "/eos/user/l/lhoryn/highd0/stau/"
d = "/eos/user/l/lhoryn/highd0/120318/"

lep0_pt = 65
lep1_pt = 65
lep_d0 = 3.


# Define grid
lifetimes = ["0p01", "0p1", "1"]
#masses = ["50", "100", "200", "300", "400", "500", "700"]
masses = ["100", "300", "500"]
maximums = {"100":22, "300":7.5, "500":2.2}
channels = ["ee", "mm", "em", "me", "all"]

# Channels
# 0 = ee, 1 = mm, 2 = em, 3 = me

variables = {
        "pt_0":  {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "leading lepton p_{T} [GeV]"},
        "pt_1":  {"attr": "OBJ_pt[1]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "subleading lepton p_{T} [GeV]"},
        "d0_0":  {"attr": "abs(OBJ_d0[0])",       "nbins": 20,    "xmin": 0.,   "xmax": 100.,       "xlabel": "leading lepton d_{0} [mm]"},
        "d0_1":  {"attr": "abs(OBJ_d0[1])",       "nbins": 20,    "xmin": 0.,   "xmax": 100.,       "xlabel": "subleading lepton d_{0} [mm]"},
        "eta_0": {"attr": "OBJ_eta[0]",           "nbins": 20,    "xmin": -3.,    "xmax": 3.,        "xlabel": "leading lepton #eta"},
        "eta_1": {"attr": "OBJ_eta[1]",           "nbins": 20,    "xmin": -3.,    "xmax": 3.,        "xlabel": "subleading lepton #eta"},
        "phi_0": {"attr": "OBJ_phi[0]",           "nbins": 20,    "xmin": -3.5,   "xmax": 3.5,       "xlabel": "leading lepton #phi"},
        "phi_1": {"attr": "OBJ_phi[1]",           "nbins": 20,    "xmin": -3.5,   "xmax": 3.5,       "xlabel": "subleading lepton #phi"},
        }

# Loop over samples
cans = {}
for lt in lifetimes:
    for m in masses:

        print "\nWorking with point lt =",lt,"m =",m
        files = glob.glob("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        print "Found", len(files), "files."
        t = getTree("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        dframe = ROOT.ROOT.RDataFrame(t)
        #dframe = ROOT.ROOT.RDataFrame("trees_SR_highd0_", "%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        #dframe = ROOT.ROOT.RDataFrame("trees_SR_highd0_", "/eos/user/l/lhoryn/highd0/stau/user.lhoryn.mc16_13TeV.399005.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_100_0_0p01ns.022219_mc16*_trees.root/*.root")
        #dframe = ROOT.ROOT.RDataFrame("trees_SR_highd0_", files[0])

        # Get crossection
        xs = getXS(files[0].split("/")[-2].split(".")[3])
        n_events = getNEvents("%s/*_%s_*_%sns*_trees.root/*.root"%(d,m,lt))
        print "Cross section:", xs, "N events:", n_events
        ev_weight = lumi*xs/n_events

        print "Found", dframe.Count().GetValue(), "events."

        # Do basic event skimming
        n_lep_cut = "lepton_n>1"
        pt_cut = "lepton_pt.at(0)>%d && lepton_pt.at(1)>%d"%(lep0_pt, lep1_pt)
        d0_cut = "abs(lepton_d0.at(0))>%f && abs(lepton_d0.at(1))>%f"%(lep_d0, lep_d0)
        df_lepcut = dframe.Filter(n_lep_cut, "n lep cut")
        df_lepptcut = df_lepcut.Filter(pt_cut, "pt cut")
        df_lepd0cut = df_lepptcut.Filter(d0_cut, "d0 cut")

        # More sophisticated triggers
        sielectron_cat = "if (channel<0 || channel==1) return 0; if (electron_pt.at(0) < 160) return 0; return 1;"
        dielectron_cat = "if (channel != 0) return 0; if (electron_pt.at(0) > 160) return 0; return 1;"
        simuon_cat     = "if (channel <0 || channel==0) return 0; if (channel!=1) if (electron_pt.at(0) > 160) return 0; return 1;"

        df_trigcat = df_lepd0cut.Define("sielectron_cat", sielectron_cat).Define("dielectron_cat", dielectron_cat).Define("simuon_cat", simuon_cat)
        df_trig = df_trigcat.Define("pass_trig_e", "sielectron_cat && pass_HLT_g140_loose").Define("pass_trig_ee", "dielectron_cat && pass_HLT_2g50_loose_L12EM20VH").Define("pass_trig_m", "simuon_cat && pass_HLT_mu60_0eta105_msonly")
        df_alltrig = df_trig.Define("pass_trig", "pass_trig_e || pass_trig_ee || pass_trig_m")

        df_trigcut_e = df_alltrig.Filter("pass_trig_e", "e trigger")
        df_trigcut_ee = df_alltrig.Filter("pass_trig_ee", "ee trigger")
        df_trigcut_m = df_alltrig.Filter("pass_trig_m", "m trigger")
        df_trigcut = df_alltrig.Filter("pass_trig", "all trig")

        # Cutflow for fun
        report = dframe.Report()
        report.Print()

        # Get pT distribution
        df_deflep = df_trigcut.Define("lepton_pt0", "lepton_pt.at(0)").Define("lepton_pt1", "lepton_pt.at(1)")
        df = df_deflep

        hname = "h_pt_%s_%s"%(m,lt)
        cname = "c_pt_%s_%s"%(m,lt)
        cans[cname] = ROOT.TCanvas(cname, cname)
        m_pt = ROOT.RDF.TH2DModel(hname, hname, variables["pt_0"]["nbins"], variables["pt_0"]["xmin"], variables["pt_0"]["xmax"], variables["pt_1"]["nbins"], variables["pt_1"]["xmin"], variables["pt_1"]["xmax"])
        h_pt = df_deflep.Histo2D(m_pt, "lepton_pt0", "lepton_pt1")
        h_pt.Scale(ev_weight)
        h_pt.GetXaxis().SetTitle(variables["pt_0"]["xlabel"])
        h_pt.GetYaxis().SetTitle(variables["pt_1"]["xlabel"])
        h_pt.SetMaximum(maximums[m])
        h_pt.SetMinimum(0)
        h_pt.Draw("colz")
        #raw_input("...")
        cans[cname].SaveAs("plots/slep_pts_%s_%s.pdf"%(m,lt))
        cans[cname].Delete()

        # Make integral distribution
        cname+="_int"
        cans[cname] = ROOT.TCanvas(cname, cname)
        h_pt_int = h_pt.Clone(hname+"_int")
        h_pt_int.Clear()
        for x in xrange(variables["pt_0"]["nbins"]+2):
            for y in xrange(variables["pt_1"]["nbins"]+2):
                h_pt_int.SetBinContent(x, y, h_pt.Integral(x, variables["pt_0"]["nbins"]+1, y, variables["pt_1"]["nbins"]+1))
        h_pt_int.Draw("colz")
        #cans[cname].Update()
        #raw_input("...")
        cans[cname].SaveAs("plots/slep_acceptance_%s_%s.pdf"%(m,lt))
        cans[cname].Delete()

        #exit()



