import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/cos/data*.root"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)


c = ROOT.TCanvas()
ROOT.gPad.SetLogy()


h1 = getHist(t, "cos_sd0","abs(muon_IDtrack_d0[0] + muon_IDtrack_d0[1])", sel_name="Length$(muon_pt) == 2 && muon_phi[1]*muon_phi[0]<0")
h1.Draw()
c.SaveAs("outputPlots/cos_sd0.pdf")

h1 = getHist(t, "cos_dz0","abs(muon_IDtrack_z0[0] - muon_IDtrack_z0[1])", sel_name="Length$(muon_pt) == 2 && muon_phi[1]*muon_phi[0]<0")
h1.Draw()
c.SaveAs("outputPlots/cos_dz0.pdf")


h1 = getHist(t, "cos_dPhi","abs(abs(muon_IDtrack_phi[0] - muon_IDtrack_phi[1]) - TMath::Pi())", sel_name="Length$(muon_pt) == 2 && muon_phi[1]*muon_phi[0]<0")
h1.Draw()
c.SaveAs("outputPlots/cos_dPhi.pdf")

h1 = getHist(t, "cos_sEta","abs(muon_IDtrack_eta[0] + muon_IDtrack_eta[1])", sel_name="Length$(muon_pt) == 2 && muon_phi[1]*muon_phi[0]<0")
h1.Draw()
c.SaveAs("outputPlots/cos_sEta.pdf")

ROOT.gPad.SetLogy(0)
h1 = getHist(t, "cos_d0_z0","abs(muon_IDtrack_z0[0] - muon_IDtrack_z0[1]):abs(muon_IDtrack_d0[0] + muon_IDtrack_d0[1])", sel_name="Length$(muon_pt) == 2 && abs(muon_IDtrack_z0[0] - muon_IDtrack_z0[1]) < 20 && abs(muon_IDtrack_d0[0] + muon_IDtrack_d0[1]) < 20 && muon_phi[1]*muon_phi[0]<0")
h1.Draw("colz")
c.SaveAs("outputPlots/cos_d0_z0.pdf")
