import gc
import os
import array
import glob
import sys


gc.disable()
execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../plot_helpers/region_defs_py.py")
ROOT.gROOT.SetBatch(1)

sfs = {
        "a": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_a.root",
        "d": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_d.root",
        "e": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_selection_e.root",
        }

sf_hists = {}
for year in sfs:
    print sfs[year]
    f = ROOT.TFile.Open(sfs[year], "READ")
    sf_hists[year] = {}
    sf_hists[year]["eff"]=f.Get("h_eff_mc")
    sf_hists[year]["sf"]=f.Get("h_sf")
    sf_hists[year]["eff"].SetDirectory(0)
    sf_hists[year]["sf"].SetDirectory(0)

#fname= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/mutrig"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal"
fname = str(sys.argv[1])

def getSFErr(event, i, year):
    phi = event.muon_phi[i]
    eta = event.muon_eta[i]
    bin_n = sf_hists[year]["sf"].FindBin(eta,phi)
    val = sf_hists[year]["sf"].GetBinContent(bin_n)
    err = sf_hists[year]["sf"].GetBinError(bin_n)
    eff = sf_hists[year]["eff"].GetBinContent(bin_n)
    return val,err,eff

def getSFFor2(s0,s1,eff0,eff1):
    return s0*s1

files = os.listdir(fname)
print files
for f in files:
    for year in sfs:
        if "%s.root"%year not in f: continue
        if ".root" not in f: continue
        if not f.endswith(".root"): continue
        #if "399115" not in f: continue

        print "processing", f
        fullname = os.path.join(fname,f)
        thisFile = ROOT.TFile(fullname, "UPDATE")
        t = thisFile.Get("trees_SR_highd0_")

        mu_sel_sf = array("d", [0])
        mu_sel_dw = array("d", [0])
        mu_sel_up = array("d", [0])
        sf = t.Branch("mu_sel_sf", mu_sel_sf, "mu_sel_sf/D")
        dw = t.Branch("mu_sel_dw", mu_sel_dw, "mu_sel_dw/D")
        up = t.Branch("mu_sel_up", mu_sel_up, "mu_sel_up/D")

        N = t.GetEntries()
        for i in xrange(N):
            t.GetEntry(i)
            if i%100000 == 0: print "Processing event", i, "/", N
            mu_sel_sf[0] = 1
            mu_sel_dw[0] = 1
            mu_sel_up[0] = 1

            if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.muon_n_signal > 0 and t.electron_n_signal > 0:
                val,err,eff = getSFErr(t, 0, year)
                mu_sel_sf[0] = val
                mu_sel_dw[0] = val - abs(err)
                mu_sel_up[0] = val + abs(err)

            if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.muon_n_signal > 1:
                val0,err0,eff0 = getSFErr(t,0,year)
                val1,err1,eff1 = getSFErr(t,1,year)
                # mu_sel_sf[0] is the relative probability that both pass
                # don't actually need the eff in this case -- SF = s0 * s1
                mu_sel_sf[0] = getSFFor2(val0,val1,eff0,eff1)
                mu_sel_dw[0] = getSFFor2(val0-err0,val1-err1,eff0,eff1)
                mu_sel_up[0] = getSFFor2(val0+err0,val1+err1,eff0,eff1)

                #print ""
                #print mu_sel_sf[0]
                #print mu_sel_dw[0]
                #print mu_sel_up[0]

            sf.Fill()
            up.Fill()
            dw.Fill()
            #t.Fill()

        thisFile.Write("", ROOT.TFile.kOverwrite)
        #t.Scan("mu_sel_sf:mu_sel_up:mu_sel_dw")
        thisFile.Close()
        #newfile.Close()
