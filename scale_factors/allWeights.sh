dirname=/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/stat_ext/signal/

echo "applying muon selection sf weights"
python getMuonSelSF.py $dirname
echo "applying muon trigger sf weights"
python getMuonTrigSF.py $dirname
echo "applying electron selection sf weights"
python getElectronSF.py $dirname
echo "applying lumi weights"
python applyLumiWeight.py $dirname
echo "applying displaced syst weights"
python applyDispSyst.py $dirname
