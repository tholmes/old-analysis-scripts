import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

append = "z0120_Rgd0_timing_idcuts_2dweight"

cosname = "rp3_dzp20_zzp20"
#cosname = "dzp10_rp3"

to_plot = {
    "d0":"d_{0}",
    "pt":"p_{T}",
    "eta": "#eta",
    "phi": "#phi"
}
f_sig = ROOT.TFile("outputFiles/signal_LRT_eff_allmu_rp1_pt30_Rd0_400_morebins.root")
f_cos = ROOT.TFile("outputFiles/cosmics_LRT_eff_%s.root"%append)


c = ROOT.TCanvas()

for p in to_plot:
    c.Clear()
    g_sig = f_sig.Get("_muon_%s_clone"%p)
    g_cos = f_cos.Get("%s_eff_%s"%(cosname,p))


    g_sig.SetMarkerStyle(21)
    g_sig.SetMarkerColor(ROOT.TColor.GetColor("#6E44FF"))
    g_sig.SetLineColor(ROOT.TColor.GetColor("#6E44FF"))
    g_cos.SetMarkerStyle(20)
    
    h_cos = getHistFromEff(g_cos)
    h_sig = getHistFromEff(g_sig)
    h_ratio = h_cos.Clone("tmp%s"%p)
    h_ratio.Divide(h_cos, h_sig, 1, 1, "B")
    
    h_ratio.SetMarkerColor(ROOT.kGray) 
    h_ratio.SetLineColor(ROOT.kGray) 
    for i in xrange(1,h_ratio.GetNbinsX()+1):
        print "x bin %i  syst %f "%(i,h_ratio.GetBinContent(i))

    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    leg.AddEntry(g_sig, "signal MC", "p")
    leg.AddEntry(g_cos, "cosmic data", "p")
    leg.AddEntry(h_ratio, "ratio", "l")
    
    g_sig.Draw("pa")
    ROOT.gPad.Update()
    g_sig.GetPaintedGraph().GetYaxis().SetRangeUser(0,1.5)
    g_sig.SetTitle(";tag muon %s; Efficiency to reconstruct track"% to_plot[p])
    
    g_cos.Draw("psame")
    h_ratio.Draw("hist e same")
    leg.Draw("same") 
    
    #ROOT.ATLASLabel(0.20,0.88, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.20,0.83, "muons, 300 GeV slepton" )
    
    ROOT.gPad.Update()
    c.Update()
    c.SaveAs("outputPlots/compare_%s_%s.pdf"%(p, append))
    

c.Clear()
h2_sig = f_sig.Get("eff_pt_d0")
h2_cos = f_cos.Get("%s_eff_pt_d0"%cosname)

tmp = h2_sig.Clone("ones")
tmp.Reset()
for i in xrange(1,tmp.GetNbinsX()+1):
    for j in xrange(1,tmp.GetNbinsY()+1):
        tmp.SetBinContent(i, j, 1)


h2_sf = h2_cos.Clone("sf_pt_d0")
h2_sf.Divide(h2_cos, h2_sig, 1, 1, "B")
ratio = h2_sf.Clone("ratio")
err_ratio = ratio.Clone()
err_ratio.Reset()

ROOT.gStyle.SetPalette(ROOT.kBird)
h2_sf.Add(tmp,-1)
h2_sf_abs = h2_sf.Clone()
h2_sf_abs.Reset()
err_cos = h2_sf.Clone()
err_cos.Reset()
err_sig = h2_sf.Clone()
err_sig.Reset()

for i in xrange(1,tmp.GetNbinsX()+1):
    for j in xrange(1,tmp.GetNbinsY()+1):
        if h2_sf.GetBinContent(i,j) == -1:
             h2_sf_abs.SetBinContent(i, j, -1)
        else:
            h2_sf_abs.SetBinContent(i, j, abs(h2_sf.GetBinContent(i,j)))
        err_cos.SetBinContent(i,j, h2_cos.GetBinError(i,j))
        err_sig.SetBinContent(i,j, h2_sig.GetBinError(i,j))
        err_ratio.SetBinContent(i, j, ratio.GetBinError(i,j))


h2_sf_abs.SetMaximum(.2)
h2_sf_abs.SetMinimum(0)
h2_sf_abs.GetZaxis().SetTitle("systematic uncertainty")
h2_sf_abs.Draw("colz")

tmp = h2_sf_abs.Clone()
tmp.SetMarkerColor(ROOT.kBlack) 
ROOT.gStyle.SetPaintTextFormat("4.3f")
tmp.Draw("text same")

for i in xrange(1,tmp.GetNbinsX()+1):
    for j in xrange(1,tmp.GetNbinsY()+1):
        print "x bin %i   ybin %i  syst %f "%(i,j,h2_sf.GetBinContent(i,j))
 
ROOT.gPad.Update()
c.Update()
c.SaveAs("outputPlots/compare_pt_d0_%s.pdf"%(append))

c.Clear()
err_cos.Draw("colz")
tmpec = err_cos.Clone()
tmpec.SetMarkerColor(ROOT.kBlack) 
ROOT.gStyle.SetPaintTextFormat("4.3f")
tmpec.Draw("text same")
c.Update()
c.SaveAs("outputPlots/compare_pt_d0_coserr_%s.pdf"%(append))

c.Clear()
err_sig.Draw("colz")
tmpes = err_sig.Clone()
tmpes.SetMarkerColor(ROOT.kBlack) 
ROOT.gStyle.SetPaintTextFormat("4.3f")
tmpes.Draw("text same")
c.Update()
c.SaveAs("outputPlots/compare_pt_d0_sigerr_%s.pdf"%(append))

c.Clear()
err_ratio.Draw("colz")
tmper = err_ratio.Clone()
tmper.SetMarkerColor(ROOT.kBlack) 
ROOT.gStyle.SetPaintTextFormat("4.3f")
tmper.Draw("text same")
c.Update()
c.SaveAs("outputPlots/compare_pt_d0_ratioerr_%s.pdf"%(append))



f = ROOT.TFile("outputFiles/LRT_syst.root","RECREATE")
h2_sf_abs.Write()
f.Write()
#f.Close()


c.Clear()
h2_sig = f_sig.Get("eff_eta_d0")
h2_cos = f_cos.Get("%s_eff_eta_d0"%cosname)

h2_sf = h2_cos.Clone("sf_eta_d0")
h2_sf.Divide(h2_sig)
ROOT.gStyle.SetPalette(ROOT.kBird)
h2_sf.SetMaximum(1)
h2_sf.SetMinimum(0)
h2_sf.Draw("colz")

tmp = h2_sf.Clone()
tmp.SetMarkerColor(ROOT.kBlack) 
tmp.Draw("text same")

    
ROOT.gPad.Update()
c.Update()
c.SaveAs("outputPlots/compare_eta_d0_%s.pdf"%(append))

