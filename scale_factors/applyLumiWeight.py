import gc
import os
import array
#import numpy
import glob
import math
import sys

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/region_defs_py.py")

ROOT.gROOT.SetBatch(1)

lumis = {
    "a": 36100.,
    "d": 43900.,
    "e": 59000.,
}

sf_name = "lumi"

#fname= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/mutrig"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/user.lhoryn.mc16_13TeV.399047.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_0p1ns.v5.6_Jul31_fullgrid_a_trees.root"

fname = str(sys.argv[1])

files = os.listdir(fname)
print files
for f in files:
    if not f.endswith(".root"): continue
    fullname = os.path.join(fname,f)
    thisFile = ROOT.TFile(fullname, "UPDATE")
    t = thisFile.Get("trees_SR_highd0_")
    
    year = f.split(".")[0][-1]
    dsid = f[0:6]

    arr_sf = array("d", [0])
    
    sf = t.Branch("%s_weight"%sf_name, arr_sf, "%s_weight/D"%sf_name)
    
    lumi = lumis[year] 
    
    N = t.GetEntries()
    weight = 1. 
    for i in xrange(N):
        t.GetEntry(i)
        if i%100000 == 0: print "Processing event", i, "/", N
           
        weight = t.normweight*lumi
        arr_sf[0] = weight


        sf.Fill()
        #t.Fill()
     
    thisFile.Write("", ROOT.TFile.kOverwrite)
    #t.Scan("lumi_weight")
    thisFile.Close()
    #newfile.Close()
