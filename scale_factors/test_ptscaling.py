import ROOT

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

f_cos = ROOT.TFile("outputFiles/cosmics_LRT_eff_z0120_Rgd0_timing_idcuts_2dweight.root")
f_sig = ROOT.TFile("outputFiles/signal_LRT_eff_allmu_rp05_pt30_Rd0_400.root")

vals = ["pt","d0"]

for v in vals:
    h_cos = f_cos.Get("all_muon_%s"%v)
    h_sig = f_sig.Get("_muon_%s"%v)


    h_sig.Scale(1/h_sig.Integral())
    h_sig.SetLineColor(ROOT.kBlue)

    h_cos.Scale(1/h_cos.Integral())
    h_cos.SetLineColor(ROOT.kRed)
    h_cos.SetLineStyle(2)

    c = ROOT.TCanvas()


    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    leg.AddEntry(h_sig, "signal muons (normalized)", "l")
    leg.AddEntry(h_cos, "cosmic muons (normalized)", "l")


    h_sig.Draw("hist")
    h_cos.Draw("hist same")
    leg.Draw("same")
    c.SaveAs("outputPlots/reweighted_%s_comp.pdf"%v)

