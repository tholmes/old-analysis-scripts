import os
import array
import glob
import ROOT
import sys

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../plot_helpers/region_defs_py.py")
ROOT.gROOT.SetBatch(1)


sf_file = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/ElectronSelectionSFs.root")
direc = sf_file.Get("VeryLooseLLH2017_nod0_Smooth_iso")
direc.cd()
h_sf = direc.Get("SF_CentralValue_VeryLooseLLH2017_nod0_Smooth_iso")
h_err = direc.Get("SF_TotalError_VeryLooseLLH2017_nod0_Smooth_iso")
h_eff = direc.Get("EffMC_CentralValue_VeryLooseLLH2017_nod0_Smooth_iso")

maxpt = h_sf.GetXaxis().GetBinLowEdge(h_sf.GetXaxis().GetNbins()+1)
print maxpt
sf_name = "el_reco"

#fname= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/mutrig"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/"
fname = str(sys.argv[1])

def getSFErr(event, i):
    eta = event.electron_eta[i]
    et = event.electron_pt[i]*1000
    if et > maxpt: et = maxpt-100

    bin_x = h_sf.GetXaxis().FindBin(et)
    bin_y = h_sf.GetYaxis().FindBin(eta)
    val = h_sf.GetBinContent(bin_x, bin_y)
    err = h_err.GetBinContent(bin_x, bin_y)
    eff = h_eff.GetBinContent(bin_x, bin_y)
    return val,err,eff

def getSFFor2(s0,s1,eff0,eff1):
    eff = eff0 + eff1 - eff0*eff1
    eff_corr = s0*eff0 + s1*eff1 - s0*s1*eff0*eff1
    return 1.*eff_corr/eff

files = os.listdir(fname)
print files
for f in files:
    
    if not f.endswith(".root"): continue
    fullname = os.path.join(fname,f)
    thisFile = ROOT.TFile(fullname, "UPDATE")
    t = thisFile.Get("trees_SR_highd0_")
    
    arr_sf = array("d", [0])
    arr_dw = array("d", [0])
    arr_up = array("d", [0])
    
    sf = t.Branch("%s_sf"%sf_name, arr_sf, "%s_sf/D"%sf_name)
    dw = t.Branch("%s_dw"%sf_name, arr_dw, "%s_dw/D"%sf_name)
    up = t.Branch("%s_up"%sf_name, arr_up, "%s_up/D"%sf_name)


    N = t.GetEntries()
    for i in xrange(N):
        t.GetEntry(i)
        if i%100000 == 0: print "Processing event", i, "/", N
        arr_sf[0] = 1
        arr_dw[0] = 1
        arr_up[0] = 1
        #print "n signal", t.electron_n_signal
         
        ### 1 electron case
        if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.muon_n_signal > 0 and t.electron_n_signal > 0:
            val,err,eff = getSFErr(t, 0)
            arr_sf[0] = val
            arr_dw[0] = val - abs(err)
            arr_up[0] = val + abs(err)

        if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.electron_n_signal > 1:
            
            val0,err0,eff0 = getSFErr(t,0)
            val1,err1,eff1 = getSFErr(t,1)
            # mu_trig_sf[0] is the relative probability that either passes
            # eff is efficiency to pass in mc
            arr_sf[0] = getSFFor2(val0,val1,eff0,eff1)
            arr_dw[0] = getSFFor2(val0-err0,val1-err1,eff0,eff1)
            arr_up[0] = getSFFor2(val0+err0,val1+err1,eff0,eff1)

            #print ""
            #print mu_trig_sf[0]
            #print mu_trig_dw[0]
            #print mu_trig_up[0]

        sf.Fill()
        up.Fill()
        dw.Fill()
        #t.Fill()

    thisFile.Write("", ROOT.TFile.kOverwrite)
    #t.Scan("el_reco_sf:el_reco_up:el_reco_dw")
    thisFile.Close()
    #newfile.Close()
