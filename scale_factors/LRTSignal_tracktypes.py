import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#append = "truthMatch"
append = "muon"

names = {
    "muon_eta": { "nb": 20, "low": -2.5, "high": 2.5, "label": "true #mu #eta"}, 
    "track_eta": { "nb": 20, "low": -2.5, "high": 2.5, "label": "track #mu #eta"}, 
    
    "muon_phi": { "nb": 20, "low": -4, "high": 4, "label": "true #mu #phi"}, 
    "track_phi": { "nb": 20, "low": -4, "high": 4, "label": "true #mu #phi"}, 
    
    "muon_pt": { "nb": 20, "low": 0, "high": 1000, "label": "true #mu p_{t}"}, 
    "track_pt": { "nb": 20, "low": 0, "high": 1000, "label": "track #mu p_{t}"}, 
    
    "muon_d0": { "nb": 10, "low": 0, "high": 300, "label": "true #mu d_{0}"}, 
    "track_d0": { "nb": 10, "low": 0, "high": 300, "label": "track #mu d_{0}"}, 
    
    "muon_d0_zoom": { "nb": 30, "low": 0, "high": 60, "label": "true #mu d_{0}"}, 
    "track_d0_zoom": { "nb": 30, "low": 0, "high": 60, "label": "track #mu d_{0}"}, 
    
    "muon_z0": { "nb": 20, "low": 0, "high": 500, "label": "true #mu z_{0}"}, 
    "track_z0": { "nb": 20, "low": 0, "high": 500, "label": "track #mu z_{0}"}, 
}

names_2d = {
    "muon_pt_d0": { "nb_x": 10, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 10, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "track_pt_d0": { "nb_x": 10, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 10, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
}

h = {}
h2 = {}

selecs = ["ST","LRT","all","qual"]
for s in selecs:
    h[s] = {} 
    h[s]= setHistos(s, names)
    
    h2[s] = {} 
    h2[s]= setHistos2D(s, names_2d)

#basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v4_Feb27_trees.root/*"
basename = "/afs/cern.ch/user/e/eressegu/Highd0Lep_ANA-SUSY-2018-14/v5.2_June4_NewReco_withDVs/signal/user.eressegu.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v5.2g_trees.root/*"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

print t.GetEntries()
eventNum = 0
for event in t:
    
    if eventNum%10000==0: print "processing ", eventNum
    eventNum += 1

    #if eventNum > 200000: break
    #if len(event.muon_pt) != 1: continue
    
    try:
        event.idTrack_pt
    except:
        continue
    
       
    #if abs(event.muon_eta[0]) > 1 or abs(event.muon_phi[0]) > 2 or abs(event.muon_phi[0]) < 1 or event.muon_phi[0] > 0: continue 
    #print "passes fiducial selection"

    v_mu = ROOT.TVector3()
    im = -1
    barcode = -1
    for it in xrange(len(event.truthLepton_pt)):
        if abs(event.truthLepton_pdgId[it]) == 13 and event.truthLepton_pt[it] > 20 and abs(event.truthLepton_eta[it]) < 1.05 and abs(event.truthLepton_parentPdgId[it]) > 100000: 
             
            h["all"]["muon_eta"].Fill(event.truthLepton_eta[it])
            h["all"]["muon_phi"].Fill(event.truthLepton_phi[it])
            h["all"]["muon_pt"].Fill(event.truthLepton_pt[it])
            h["all"]["muon_d0"].Fill(abs(event.truthLepton_d0[it]))
            h["all"]["muon_d0_zoom"].Fill(abs(event.truthLepton_d0[it]))
            h2["all"]["muon_pt_d0"].Fill(event.truthLepton_pt[it], abs(event.truthLepton_d0[it]))
            #h["muon_z0"].Fill(abs(event.muon_IDtrack_z0[0])) # no truth z0

            v_mu.SetPtEtaPhi(event.truthLepton_pt[it], event.truthLepton_eta[it], event.truthLepton_phi[it])
            # for now, only do this for the first muon
            im = it
            barcode = event.truthLepton_barcode[it]
        

            for idt in xrange(len(event.idTrack_pt)):    
                v_trk = ROOT.TVector3()
                v_trk.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
                 
                #if abs( (event.idTrack_pt[idt] - v_mu.Pt())/v_mu.Pt()) < 0.20 and abs(v_trk.DeltaR(v_mu)) < 0.1:
                if event.idTrack_truthMatchedBarcode[idt] == barcode: 
                    h["all"]["track_eta"].Fill(event.truthLepton_eta[im])
                    h["all"]["track_phi"].Fill(event.truthLepton_phi[im])
                    h["all"]["track_pt"].Fill(event.truthLepton_pt[im])
                    h["all"]["track_d0"].Fill(abs(event.truthLepton_d0[im]))
                    h["all"]["track_d0_zoom"].Fill(abs(event.truthLepton_d0[im]))
                    h2["all"]["track_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                    
                    if abs(event.idTrack_chi2[idt]) < 2 and (event.idTrack_NPix_Holes[idt] + event.idTrack_NSct_Holes[idt]) <= 1:
                        h["qual"]["track_eta"].Fill(event.truthLepton_eta[im])
                        h["qual"]["track_phi"].Fill(event.truthLepton_phi[im])
                        h["qual"]["track_pt"].Fill(event.truthLepton_pt[im])
                        h["qual"]["track_d0"].Fill(abs(event.truthLepton_d0[im]))
                        h["qual"]["track_d0_zoom"].Fill(abs(event.truthLepton_d0[im]))
                        h2["qual"]["track_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                    
                    trackKind = "ST"
                    if event.idTrack_isLRT[idt]: trackKind = "LRT"
                    h[trackKind]["track_eta"].Fill(event.truthLepton_eta[im])
                    h[trackKind]["track_phi"].Fill(event.truthLepton_phi[im])
                    h[trackKind]["track_pt"].Fill(event.truthLepton_pt[im])
                    h[trackKind]["track_d0"].Fill(abs(event.truthLepton_d0[im]))
                    h[trackKind]["track_d0_zoom"].Fill(abs(event.truthLepton_d0[im]))
                    h2[trackKind]["track_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                    
                    
                    
                    #h["track_z0"].Fill(abs(event.muon_IDtrack_z0[im]))
                    break




vs = ["eta","phi","d0","d0_zoom","pt"]
for s in selecs:
    for v in vs:
        h[s]["eff_"+v] = ROOT.TEfficiency(h[s]["track_"+v], h["all"]["muon_"+v])
        h[s]["eff_"+v].SetName("%s_eff_%s"%(s,v))
#h["eff_z0"] = ROOT.TEfficiency(h["track_z0"], h["muon_z0"])


outFile = ROOT.TFile("outputFiles/signal_%s_LRT_eff_firstpass.root"%append, "RECREATE")

c = ROOT.TCanvas()
for s in selecs:
    for histo in h[s]:

        h[s][histo].Write()
        if "eff" in histo:
            h[s][histo].Draw()
            ROOT.gPad.Update() 
            graph = h[s][histo].GetPaintedGraph()
            graph.SetMinimum(0)
            graph.SetMaximum(1) 
            ROOT.gPad.Update()
            c.SaveAs("outputPlots/signal_%s_%s_%s.pdf"%(append,s,histo))
