## return % uncertainty due to displacement
def getDispSyst_el(d0):
    d0 = abs(d0)
    
    if d0 > 0 and d0 < 3: return 0 
    if d0 > 3 and d0 < 10: return 2.4758
    if d0 > 10 and d0 < 20: return 4.4428
    if d0 > 20 and d0 < 50: return 9.6254
    if d0 > 50 and d0 < 100: return 10.6256
    if d0 > 100 and d0 < 300: return 24.1541


## return % uncertainty due to displacement
def getDispSyst_mu(d0):
    d0 = abs(d0)
    
    if d0 > 0 and d0 < 3: return 0 
    if d0 > 3 and d0 < 10: return -1.9906
    if d0 > 10 and d0 < 20: return -0.1018
    if d0 > 20 and d0 < 50: return 1.7348
    if d0 > 50 and d0 < 100: return 2.1062
    if d0 > 100 and d0 < 300: return 2.8360


