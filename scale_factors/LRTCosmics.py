import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)


app = "z0120_Rgd0_timing_idcuts_2dweight"

f = ROOT.TFile("outputFiles/pt_d0_scaling.root")
h_scaling = f.Get("scaling")



names = {
    "muon_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "cosmic #mu #eta"}, 
    "track_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "track #mu #eta"}, 
    
    "muon_phi": { "nb": 10, "low": -4, "high": 4, "label": "cosmic #mu #phi"}, 
    "track_phi": { "nb": 10, "low": -4, "high": 4, "label": "cosmic #mu #phi"}, 
    
    #"muon_pt": { "bins": [0,65,200,400,1000], "label": "true #mu p_{t}"}, 
    #"track_pt": { "bins": [0,65,200,400,1000], "label": "track #mu p_{t}"}, 
    "muon_pt": { "bins": [0,65,150,250,350,450,750,1000], "label": "true #mu p_{t}"}, 
    "track_pt": { "bins": [0,65,150,250,350,450,750,1000], "label": "track #mu p_{t}"}, 
    
    "muon_d0": { "bins": [0,3,50,150,300], "label": "true #mu d_{0}"}, 
    "track_d0": { "bins":  [0,3,50,150,300],  "label": "track #mu d_{0}"}, 
    
    "muon_z0": { "nb": 10, "low": 0, "high": 500, "label": "cosmic #mu z_{0}"}, 
    "track_z0": { "nb": 10, "low": 0, "high": 500, "label": "track #mu z_{0}"}, 
    
    "muon_t0": { "nb": 9, "low": -35, "high": 35, "label": "cosmic #mu t_{0}"}, 
    "track_t0": { "nb": 9, "low": -35, "high": 35, "label": "track #mu t_{0}"}, 
    
    "n_matched": { "nb": 10, "low": 0, "high": 10, "label": "N matched tracks"}, 
}

names_2d = {
    "muon_pt_d0": { "bins_x": [0,65,150,250,350,450,750,1000], "label_x": "p_{T}", "bins_y": [0,3,50,100,200,300], "label_y": "d_{0}", "log":0},
    "track_pt_d0": { "bins_x": [0,65,150,250,350,450,750,1000], "label_x": "p_{T}", "bins_y": [0,3,50,100,200,300], "label_y": "d_{0}", "log":0},
    
    "muon_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "track_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
}

## these cuts have too many tracks per muon 
    #"rp3_pt30": "abs(abs(v_mu.DeltaR(v_trk)) - math.pi) < 0.3 and event.idTrack_pt[idt] > 30",
    #"zzp20": "abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 20 ",
    #"dzp20": "abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 20 ",

## these cuts dont select enough tracks
    #"rp3_pt30_dzp5_zzp5": "abs (abs(v_mu.DeltaR(v_trk)) - math.pi) < 0.3 and event.idTrack_pt[idt] > 30 and abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 0.5 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 0.5",
    #"rp3_pt30_dzp1_zzp1": "abs (abs(v_mu.DeltaR(v_trk)) - math.pi) < 0.3 and event.idTrack_pt[idt] > 30 and abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 1 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 1",
    #"dPhi0p5_sEta0p5": "abs(abs(event.muon_IDtrack_phi[im] - event.idTrack_phi[idt]) - math.pi) < 0.5 and abs(event.muon_IDtrack_eta[im] - event.idTrack_eta[idt]) < 0.5"
    #"rp3": "dR < 0.3 ",
    
    #"rp3_dzp10_zzp10": "dR < 0.3 and abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 20 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 20",
    #"rp04": "dR < 0.05 ",


    #"dzp10_zzp10": "abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 10 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 10",
    #"dzp10_rp3": "abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 10 and dR < 0.3",
    #"dzp20_rp3": "abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 20 and dR < 0.3",
    #"dzp20_zzp20": "abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 20 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 20",


sels = {
    "all" : "True",
    "rp3_dzp20_zzp20": "dR < 0.3 and abs(event.muon_IDtrack_d0[im] + event.idTrack_d0[idt]) < 20 and abs(event.muon_IDtrack_z0[im] - event.idTrack_z0[idt]) < 20",
}


h = {}
h2 = {}

for sel in sels:
    h[sel] = {}
    h2[sel] = {}

    h[sel] = setHistos(sel, names)
    h2[sel] = setHistos2D(sel,names_2d)


dEta = 0.018
dPhi = 0.25
maps = ROOT.TFile("/afs/cern.ch/work/l/lhoryn/public/displacedLepton/ft_clean/src/FactoryTools/data/DV/segmentMap2D_Run2_v4.root")
innerMap = maps.Get("BI_mask") 
middleMap = maps.Get("BM_mask") 
outerMap = maps.Get("BO_mask")

#basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data1*.root"

basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/cos/data*.root"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

print t.GetEntries()
eventNum = 0
for event in t:
    
    if eventNum%10000==0: print "processing ", eventNum
    eventNum += 1

    #if eventNum > 20000: break
    if not event.pass_HLT_mu60_0eta105_msonly: continue
    
    try:
        event.idTrack_pt
    except:
        continue
    
    for im in xrange(len(event.muon_pt)):
        
        ctag, ms_cos = cosTag(event, im, dEta, dPhi)
        
        if not ctag: continue 
        #print "got a cosmic!"
           
        
        #if event.muon_IDtrack_phi[im] < 0 or abs(event.muon_IDtrack_eta[im]) > 1.05 or event.muon_IDtrack_pt[im] <= 50 or abs(event.muon_IDtrack_d0[im] < 3) or abs(event.muon_IDtrack_z0[im]) > 100: continue
        #if abs(event.muon_eta[im]) < 1.05 and event.muon_pt[im] > 65 and abs(event.muon_IDtrack_d0[im]) > 3 and abs(event.muon_IDtrack_z0[im]) < 120: 
        if abs(event.muon_IDtrack_eta[im]) < 1.05 and event.muon_IDtrack_pt[im] > 65 and abs(event.muon_IDtrack_d0[im]) > 3 and abs(event.muon_IDtrack_z0[im]) < 120: 
        #if abs(event.muon_eta[im]) < 1.05 and event.muon_pt[im] > 50 and abs(event.muon_IDtrack_z0[im]) < 120: 
        #if abs(event.muon_IDtrack_eta[im]) > 1.05 or event.muon_IDtrack_pt[im] <= 50 or abs(event.muon_IDtrack_d0[im] < 3) or abs(event.muon_IDtrack_z0[im]) > 100: continue
            t0 = t_avg(event,im)
            
            weight = 1
            if "weight" in app:
                weight = h_scaling.GetBinContent(h_scaling.FindBin(event.muon_IDtrack_pt[im], abs(event.muon_IDtrack_d0[im])))
            
            if ( (event.muon_IDtrack_phi[im] > 0 and t0 < 0) or (event.muon_IDtrack_phi[im] < 0 and t0 > 0) ):
            
                
                h["all"]["muon_eta"].Fill(event.muon_IDtrack_eta[im], weight)
                h["all"]["muon_phi"].Fill(event.muon_IDtrack_phi[im], weight)
                h["all"]["muon_pt"].Fill(event.muon_IDtrack_pt[im], weight)
                h["all"]["muon_d0"].Fill(abs(event.muon_IDtrack_d0[im]), weight)
                h["all"]["muon_z0"].Fill(abs(event.muon_IDtrack_z0[im]), weight)
                h["all"]["muon_t0"].Fill(t0, weight)
                
                h2["all"]["muon_pt_d0"].Fill(event.muon_IDtrack_pt[im], abs(event.muon_IDtrack_d0[im]), weight)
                h2["all"]["muon_eta_d0"].Fill(event.muon_IDtrack_eta[im], abs(event.muon_IDtrack_d0[im]), weight)


                v_mu = ROOT.TVector3()
                v_mu.SetPtEtaPhi(event.muon_IDtrack_pt[im], event.muon_IDtrack_eta[im], event.muon_IDtrack_phi[im])
                
                nmatched = {} 
                for sel in sels: nmatched[sel] = 0
                
                for idt in xrange(len(event.idTrack_pt)):
                    #if event.idTrack_phi[idt] * event.muon_IDtrack_phi[im] > 0: continue
                    if event.idTrack_pt[idt] >  30 and abs(event.idTrack_RFirstHit[idt]) > abs(event.idTrack_d0[idt]): 
                    
                    #if (event.idTrack_NPix_Holes[idt] + event.idTrack_NSct_Holes[idt]) > 1: continue
                    
                    #if (event.idTrack_NPix_Hits[idt] + event.idTrack_NSct_Hits[idt]) < 9: continue

                        v_trk = ROOT.TVector3()
                        v_trk.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
                        
                        dPhi = abs(abs(v_trk.DeltaPhi(v_mu)) - math.pi)
                        sEta = abs(v_trk.Eta() + v_mu.Eta())
                        dR = math.sqrt(dPhi**2 + sEta**2)


                        for sel in sels:
                            if eval(sels[sel]):

                                if nmatched[sel] == 0:
                                    h[sel]["track_eta"].Fill(event.muon_IDtrack_eta[im], weight)
                                    h[sel]["track_phi"].Fill(event.muon_IDtrack_phi[im], weight)
                                    h[sel]["track_pt"].Fill(event.muon_IDtrack_pt[im], weight)
                                    h[sel]["track_d0"].Fill(abs(event.muon_IDtrack_d0[im]), weight)
                                    h[sel]["track_z0"].Fill(abs(event.muon_IDtrack_z0[im]), weight)
                                    h[sel]["track_t0"].Fill(t0, weight)
                                
                                    h2[sel]["track_pt_d0"].Fill(event.muon_IDtrack_pt[im], abs(event.muon_IDtrack_d0[im]), weight)
                                    h2[sel]["track_eta_d0"].Fill(event.muon_IDtrack_eta[im], abs(event.muon_IDtrack_d0[im]), weight)
                                nmatched[sel] += 1

                    for sel in sels: h[sel]["n_matched"].Fill(nmatched[sel])


c = ROOT.TCanvas()
outFile = ROOT.TFile("outputFiles/cosmics_LRT_eff_%s.root"%app, "RECREATE")
for sel in sels:
    print sel
    append = sel + "_"+ app
    
    c.Clear()    
    

    print "making eff_eta"
    h[sel]["eff_eta"] = ROOT.TEfficiency(h[sel]["track_eta"], h["all"]["muon_eta"])
    h[sel]["eff_eta"].SetName("%s_eff_eta"%sel)
    print "making eff_phi"
    h[sel]["eff_phi"] = ROOT.TEfficiency(h[sel]["track_phi"], h["all"]["muon_phi"])
    h[sel]["eff_phi"].SetName("%s_eff_phi"%sel)
    print "making eff_pt"
    h[sel]["eff_pt"] = ROOT.TEfficiency(h[sel]["track_pt"], h["all"]["muon_pt"])
    h[sel]["eff_pt"].SetName("%s_eff_pt"%sel)
    print "making eff_d0"
    h[sel]["eff_d0"] = ROOT.TEfficiency(h[sel]["track_d0"], h["all"]["muon_d0"])
    h[sel]["eff_d0"].SetName("%s_eff_d0"%sel)
    print "making eff_z0"
    h[sel]["eff_z0"] = ROOT.TEfficiency(h[sel]["track_z0"], h["all"]["muon_z0"])
    h[sel]["eff_z0"].SetName("%s_eff_z0"%sel)
    print "making eff_t0"
    h[sel]["eff_t0"] = ROOT.TEfficiency(h[sel]["track_t0"], h["all"]["muon_t0"])
    h[sel]["eff_t0"].SetName("%s_eff_t0"%sel)
    print "making pt_d0"
    h2[sel]["eff_pt_d0"] = h2[sel]["track_pt_d0"].Clone("%s_eff_pt_d0"%sel)
    h2[sel]["eff_pt_d0"].Divide(h2[sel]["eff_pt_d0"], h2["all"]["muon_pt_d0"],1,1,"B")
    print "making eta_d0"
    h2[sel]["eff_eta_d0"] = h2[sel]["track_eta_d0"].Clone("%s_eff_eta_d0"%sel)
    h2[sel]["eff_eta_d0"].Divide(h2["all"]["muon_eta_d0"])
    
    #c.Clear()
    #h2[sel]["eff_pt_d0"].Draw("COLZ")
    #raw_input("...")



    h[sel]["n_matched"].Draw()
    c.SaveAs("outputPlots/cosmics_%s_nmatched.pdf"%append)


    for histo in h[sel]:

        h[sel][histo].Write()
        if "eff" in histo:
            c.Clear()
            h[sel][histo].Draw()
            ROOT.gPad.Update() 
            graph = h[sel][histo].GetPaintedGraph()
            graph.SetMinimum(0)
            graph.SetMaximum(1) 
            ROOT.gPad.Update()
            c.SaveAs("outputPlots/cosmics_"+append+"_"+histo+".pdf")

    ROOT.gStyle.SetPalette(ROOT.kBird)
    for histo in h2[sel]:
        h2[sel][histo].Write()
        if "eff" in histo:
            c.Clear()
            h2[sel][histo].SetMaximum(1)
            h2[sel][histo].SetMinimum(0)
            h2[sel][histo].Draw("COLZ")
            
            tmp = h2[sel][histo].Clone()
            tmp.SetMarkerColor(ROOT.kBlack) 
            tmp.Draw("text same")
            
            c.SaveAs("outputPlots/cosmics_"+append+"_"+histo+".pdf")
            


shs = []
for sel in sels:
    shs.append(h[sel]["n_matched"])
c.Clear()
leg = ROOT.TLegend(0.60,0.60,0.80,0.92)
first=True
for hs in xrange(len(shs)):
    shs[hs].SetLineColor(colors[hs])
    if first:
        shs[hs].SetMaximum(1.2*getMaximum(shs))
        shs[hs].Draw()
        first=False
    else:
        shs[hs].Draw("same")
     
    leg.AddEntry(shs[hs],shs[hs].GetName().replace("_n_matched",""),"l")


leg.Draw("same")
c.SaveAs("outputPlots/cosmics_"+app+"_nmatched_compare.pdf")

outFile.Write()
