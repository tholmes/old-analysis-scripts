import ROOT
import glob

execfile("../plot_helpers/basic_plotting.py")
ROOT.gROOT.SetBatch(1)


dirname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.6_Jul31_fullgrid/signal/*.root"
AODFiles = glob.glob(dirname)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

sfs = ["mu_trig_sf","el_reco_sf","disp_sf","mu_sel_sf"]

for sf in sfs:
    h = getHist(t, sf, sf)
    print sf, h.GetMean(), h.GetRMS()


