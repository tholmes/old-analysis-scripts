import ROOT

execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)


f_cos = ROOT.TFile("outputFiles/cosmics_LRT_eff_z0120_Rgd0_timing_idcuts_morebins.root")
f_sig = ROOT.TFile("outputFiles/signal_LRT_eff_allmu_rp1_pt30_Rd0_400_morebins.root")
c = ROOT.TCanvas()
'''
h_cos = f_cos.Get("all_muon_pt")
h_sig = f_sig.Get("_muon_pt")

h_cos.Scale(1/h_cos.Integral())
h_cos.SetLineColor(ROOT.kRed)

h_sig.Scale(1/h_sig.Integral())
h_sig.SetLineColor(ROOT.kBlue)


h_scaling = h_sig.Clone("scaling")
h_scaling.Divide(h_cos)
h_scaling.SetLineColor(ROOT.kBlack)

h_scaling.Draw("hist")
c.SaveAs("outputPlots/pt_scaling.pdf")

leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
leg.AddEntry(h_sig, "signal muons (normalized)", "l")
leg.AddEntry(h_cos, "cosmic muons (normalized)", "l")


h_cos.Draw("hist")
h_sig.Draw("hist same")
leg.Draw("same")
c.SaveAs("outputPlots/pt_comp.pdf")

f_out = ROOT.TFile("outputFiles/pt_scaling.root", "RECREATE")
h_scaling.Write()
f_out.Write()

'''

h_cos = f_cos.Get("all_muon_pt_d0")
h_sig = f_sig.Get("_muon_pt_d0")

h_cos.Scale(1/h_cos.Integral())
h_sig.Scale(1/h_sig.Integral())

h_scaling = h_sig.Clone("scaling")
h_scaling.Divide(h_cos)

h_cos.Draw("colz")
c.SaveAs("outputPlots/cos_scaling.pdf")

h_sig.Draw("colz")
c.SaveAs("outputPlots/sig_scaling.pdf")

h_scaling.Draw("colz")
c.SaveAs("outputPlots/pt_d0_scaling.pdf")


f_out = ROOT.TFile("outputFiles/pt_d0_scaling.root","RECREATE")
h_scaling.Write()
f_out.Write()

