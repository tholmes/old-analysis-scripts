import gc
import os
import array
import glob
import sys
#import numpy

gc.disable()
execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../plot_helpers/region_defs_py.py")
ROOT.gROOT.SetBatch(1)

sfs = {
        "a": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_trig_a.root",
        "d": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_trig_d.root",
        "e": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/f_muon_trig_e.root",
        }

sf_hists = {}
for year in sfs:
    print sfs[year]
    f = ROOT.TFile.Open(sfs[year], "READ")
    sf_hists[year] = {}
    sf_hists[year]["eff"]=f.Get("h_eff_mc")
    sf_hists[year]["sf"]=f.Get("h_sf")
    sf_hists[year]["eff"].SetDirectory(0)
    sf_hists[year]["sf"].SetDirectory(0)

#fname= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/mutrig"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/"
fname = str(sys.argv[1])


def getSFErr(event, i, year):
    phi = event.muon_phi[i]
    eta = event.muon_eta[i]
    bin_n = sf_hists[year]["sf"].FindBin(eta,phi)
    val = sf_hists[year]["sf"].GetBinContent(bin_n)
    err = sf_hists[year]["sf"].GetBinError(bin_n)
    eff = sf_hists[year]["eff"].GetBinContent(bin_n)
    
    return val,err,eff 

def getSFFor2(s0,s1,eff0,eff1):
    eff = eff0 + eff1 - eff0*eff1
    eff_corr = s0*eff0 + s1*eff1 - s0*s1*eff0*eff1
    return 1.*eff_corr/eff

files = os.listdir(fname)
print files
for f in files:
    for year in sfs:
        if "%s.root"%year not in f: continue
        if not f.endswith(".root"): continue
        fullname = os.path.join(fname,f)
        thisFile = ROOT.TFile(fullname, "UPDATE")
        t = thisFile.Get("trees_SR_highd0_")

        mu_trig_sf = array("d", [0])
        mu_trig_dw = array("d", [0])
        mu_trig_up = array("d", [0])
        #newtree.SetBranchAddress("mu_trig_sf", mu_trig_sf)
        #newtree.SetBranchAddress("mu_trig_dw", mu_trig_dw)
        #newtree.SetBranchAddress("mu_trig_up", mu_trig_up)
        sf = t.Branch("mu_trig_sf", mu_trig_sf, "mu_trig_sf/D")
        dw = t.Branch("mu_trig_dw", mu_trig_dw, "mu_trig_dw/D")
        up = t.Branch("mu_trig_up", mu_trig_up, "mu_trig_up/D")

        N = t.GetEntries()
        for i in xrange(N):
            t.GetEntry(i)
            if i%100000 == 0: print "Processing event", i, "/", N
            mu_trig_sf[0] = 1
            mu_trig_dw[0] = 1
            mu_trig_up[0] = 1

            if t.triggerRegion==2:
                if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.muon_n_signal > 0 and t.electron_n_signal > 0:
                    val,err,eff = getSFErr(t, 0, year)
                    mu_trig_sf[0] = val
                    mu_trig_dw[0] = val - abs(err)
                    mu_trig_up[0] = val + abs(err)

                if t.triggerRegion_pass and abs(t.deltaR) > 0.2 and t.muon_n_signal > 1:
                    if abs(t.muon_eta[0])<1.07 and abs(t.muon_eta[1])<1.07:
                        val0,err0,eff0 = getSFErr(t,0,year)
                        val1,err1,eff1 = getSFErr(t,1,year)
                        # mu_trig_sf[0] is the relative probability that either passes
                        # eff is efficiency to pass in mc
                        mu_trig_sf[0] = getSFFor2(val0,val1,eff0,eff1)
                        mu_trig_dw[0] = getSFFor2(val0-err0,val1-err1,eff0,eff1)
                        mu_trig_up[0] = getSFFor2(val0+err0,val1+err1,eff0,eff1)
                    elif abs(t.muon_eta[0])<1.07:
                        val,err,eff = getSFErr(t, 0, year)
                        mu_trig_sf[0] = val
                        mu_trig_dw[0] = val - abs(err)
                        mu_trig_up[0] = val + abs(err)
                    else:
                        val,err,eff = getSFErr(t, 1, year)
                        mu_trig_sf[0] = val
                        mu_trig_dw[0] = val - abs(err)
                        mu_trig_up[0] = val + abs(err)

                #print ""
                #print mu_trig_sf[0]
                #print mu_trig_dw[0]
                #print mu_trig_up[0]

            sf.Fill()
            up.Fill()
            dw.Fill()
            #t.Fill()

        thisFile.Write("", ROOT.TFile.kOverwrite)
        #t.Scan("mu_trig_sf:mu_trig_up:mu_trig_dw")
        thisFile.Close()
        #newfile.Close()
