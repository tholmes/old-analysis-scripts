import ROOT
import math

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

f = ROOT.TFile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/399043d.root")
t = f.Get("trees_SR_highd0_")

names_2d = {
    "r_r": { "nb_x": 20, "low_x": 0, "high_x": 600, "label_x": "reco R first hit", "nb_y": 20, "low_y": 0, "high_y": 600, "label_y": "truth R decay", "log":0},
    "r_r_d0": { "nb_x": 20, "low_x": 0, "high_x": 600, "label_x": "reco R first hit", "nb_y": 20, "low_y": 0, "high_y": 600, "label_y": "truth R decay", "log":0},
    "rr_d0": { "nb_x": 20, "low_x": 0, "high_x": 100, "label_x": "r decay - r first hit", "nb_y": 20, "low_y": 0, "high_y": 200, "label_y": "reco d0", "log":0},
    "rr_d0_sig": { "nb_x": 20, "low_x": 0, "high_x": 100, "label_x": "r decay - r first hit", "nb_y": 20, "low_y": 0, "high_y": 200, "label_y": "reco d0", "log":0},
}
h2 = {}
h2 = setHistos2D('',names_2d)


total = 0
diff = 0
for event in t:
    
    for im in xrange(len(event.muon_pt)):
        for it in xrange(len(event.truthLepton_pt)):
            if event.muon_truthMatchedBarcode[im] == event.truthLepton_barcode[it]:
                h2["r_r"].Fill(abs(event.muon_IDtrack_RFirstHit[im]), math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2))
                h2["rr_d0"].Fill(abs( abs(event.muon_IDtrack_RFirstHit[im]) - math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2)), abs(event.muon_IDtrack_d0[im]) )
                if event.SRmm and event.muon_isSignal[im]: 
                    h2["r_r_d0"].Fill(abs(event.muon_IDtrack_RFirstHit[im]), math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2))
                    h2["rr_d0_sig"].Fill(abs( abs(event.muon_IDtrack_RFirstHit[im]) - math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2)), abs(event.muon_IDtrack_d0[im]) )
                    
                    total +=1
                    if abs(event.muon_IDtrack_RFirstHit[im]) < math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2): diff += 1

print total
print diff
c =  ROOT.TCanvas()
for h in h2:
    h2[h].Draw("colz")
    c.SaveAs(h+".pdf")
