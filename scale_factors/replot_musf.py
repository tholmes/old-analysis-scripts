import os
import array
import glob

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
execfile("../plot_helpers/region_defs_py.py")
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

dirname =  "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/"
filenames = ["f_muon_selection_a.root","f_muon_selection_d.root","f_muon_selection_e.root","f_muon_trig_a.root","f_muon_trig_d.root","f_muon_trig_e.root"]


c = ROOT.TCanvas()
for fle in filenames:
    
    f = ROOT.TFile("%s/%s"%(dirname,fle))

    h_data = f.Get("h_eff_data")
    h_data.RebinX(2)
    h_data.RebinY(2)
    h_mc= f.Get("h_eff_mc")
    h_mc.RebinX(2)
    h_mc.RebinY(2)
    h_sf = h_data.Clone()
    h_sf.Divide(h_sf, h_mc, 1, 1, "B")

    h_sf.GetXaxis().SetTitle("Matched Muon #eta")
    h_sf.GetYaxis().SetTitle("Matched Muon #phi")
    h_sf.SetMinimum(0.7)
    h_sf.SetMaximum(1.3)
    h_sf.Draw("colz")

    ROOT.ATLASLabel(0.2,0.85, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.78, "Signal Muons, Data/Zmm MC, %.f fb^{-1}"%(lumi*0.001))
    text.DrawLatex(0.2,0.71, "Ratio of MS-Only Trigger Efficiencies")

    c.SaveAs("plots/%s.pdf"%fle)
