import gc
import os
import array
#import numpy
import glob
import math
import sys

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/region_defs_py.py")
execfile("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/systematics/disp_systs.py")

ROOT.gROOT.SetBatch(1)


## displaced systematics defined in disp_systs.py, they just get summed in quadrature for the first two leptons (val)
## sf = 1, up = 1+val, dw=1-val
sf_name = "disp"

#fname= "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal/mutrig"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.7_Aug12_lifetimefix/signal/"
fname = str(sys.argv[1])


files = os.listdir(fname)
print files
for f in files:
    if not f.endswith(".root"): continue
    fullname = os.path.join(fname,f)
    thisFile = ROOT.TFile(fullname, "UPDATE")
    t = thisFile.Get("trees_SR_highd0_")

    arr_sf = array("d", [0])
    arr_dw = array("d", [0])
    arr_up = array("d", [0])
    
    sf = t.Branch("%s_sf"%sf_name, arr_sf, "%s_sf/D"%sf_name)
    dw = t.Branch("%s_dw"%sf_name, arr_dw, "%s_dw/D"%sf_name)
    up = t.Branch("%s_up"%sf_name, arr_up, "%s_up/D"%sf_name)

    N = t.GetEntries()
    for i in xrange(N):
        t.GetEntry(i)
        if i%100000 == 0: print "Processing event", i, "/", N
        arr_sf[0] = 1
        arr_dw[0] = 1
        arr_up[0] = 1
        
        ## only apply to SR
        if t.lepton_n_signal > 1 and t.triggerRegion_pass and abs(t.deltaR) > 0.2:
           
           syst0 = getDispSyst(t.lepton_pdgId[0], t.lepton_d0[0])/100
           syst1 = getDispSyst(t.lepton_pdgId[1], t.lepton_d0[1])/100
           
           total = math.sqrt(syst0**2 + syst1**2)
           
           arr_sf[0] = 1.
           arr_dw[0] = 1 - total
           arr_up[0] = 1 + total 


        sf.Fill()
        up.Fill()
        dw.Fill()
        #t.Fill()

    thisFile.Write("", ROOT.TFile.kOverwrite)
    #t.Scan("lepton_n_signal:lepton_d0[0]:lepton_d0[1]:lepton_pdgId[0]:lepton_pdgId[1]:%s_sf:%s_up:%s_dw"%(sf_name, sf_name, sf_name))
    thisFile.Close()
    #newfile.Close()
