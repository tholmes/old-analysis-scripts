from array import array
from ROOT import TFile, TTree, gRandom

# reopen tree file
file = TFile("tree.root",'update')
tree = file.Get("tree_name")

# make new variable and add it as a branch to the tree
py = array('d',[0])
branch = tree.Branch("py", py, 'py/D')

# fill the branch
N = tree.GetEntries()
for i in xrange(N):
    tree.GetEntry(i)
    py[0] = gRandom.Gaus(23,4)
    branch.Fill()

# overwrite the tree in the output file and close the file
file.Write("",TFile.kOverwrite)
file.Close()



