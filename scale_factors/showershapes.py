import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)

append = "signal_pt"


basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/showershapes/user.lhoryn.mc16_13TeV.*.showershapes_new_*_v1_trees.root/*"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)


e_vars = ["Rhad","Reta","weta2","Rphi","f1","f3","Eratio","wtots1"]

c = ROOT.TCanvas()

for v in e_vars:
    
    cut = ""
    if "signal" in append: cut = "electron_isSignal"

    #h1 = getHist(t, "d0_%s"%v,"electron_%s:abs(electron_d0)"%v, sel_name=cut)
    h1 = getHist(t, "d0_%s"%v,"electron_%s:abs(electron_pt)"%v, sel_name=cut)
    #h1.GetXaxis().SetTitle("|d_{0}|")
    h1.GetXaxis().SetTitle("p_{T}")
    h1.GetYaxis().SetTitle(v)
    px = h1.ProfileX("proj_d0_%s"%v)
    px.GetYaxis().SetTitle("proj " + v) 
    c.Clear()
    h1.Draw("colz")
    px.Draw("same")
    c.SaveAs("outputPlots/disp/e_%s_%s_hist.pdf"%(append,v))
    
    fit = ROOT.TF1("pol1","pol1",0,300) 
    px.Fit("pol1","RQ") #line 
    c.Clear()
    px.Draw()
    fit.SetLineColor(ROOT.kRed)
    fit.Draw("same")
    c.SaveAs("outputPlots/disp/e_%s_%s_profile.pdf"%(append,v))
    
m_vars = ["MStrack_ParamELossSigmaM","MStrack_ParamELossSigmaP","MStrack_ParamELoss","MStrack_MeasELossSigma","MStrack_MeasELoss","MStrack_ELossSigma","MStrack_ELoss","MStrack_chi2","MStrack_nPres","MStrack_qop","MStrack_qop_err","msSegment_nPhiLays","msSegment_nPresHits", "msSegment_t0","msSegment_t0Err"]

for v in m_vars:
    
    cut = ""
    if "signal" in append: cut = "muon_isSignal"
        
    print v
    #h1 = getHist(t, "m_d0_%s"%v,"muon_%s:abs(muon_IDtrack_d0)"%v, sel_name=cut)
    h1 = getHist(t, "m_d0_%s"%v,"muon_%s:abs(muon_pt)"%v, sel_name=cut)
    #h1.GetXaxis().SetTitle("|d_{0}|")
    h1.GetXaxis().SetTitle("p_{T}")
    h1.GetYaxis().SetTitle(v)
    px = h1.ProfileX("proj_d0_%s"%v)
    px.GetYaxis().SetTitle("proj " + v) 
    c.Clear()
    h1.Draw("colz")
    px.Draw("same")
    c.SaveAs("outputPlots/disp/m_%s_%s_hist.pdf"%(append,v))
    
    fit = ROOT.TF1("pol1","pol1",0,300) 
    px.Fit("pol1","RQ") #line 
    c.Clear()
    px.Draw()
    fit.SetLineColor(ROOT.kRed)
    fit.Draw("same")
    c.SaveAs("outputPlots/disp/m_%s_%s_profile.pdf"%(append,v))

    
    
    
     
