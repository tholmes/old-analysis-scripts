import glob
import ROOT
import os
import gc
gc.disable()

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPalette(ROOT.kBird)
#ROOT.gStyle.SetPaintTextFormat(".2f");

append = "_d"
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/ScaleFactors/slim/merged/"
#sr = "triggerRegion_pass && lepton_n_signal>1 && lepton_isSignal[0] && lepton_isSignal[1] && deltaR>0.2 && muon_n_cosmic==0"

files = {
    #"Data": "user.tholmes.data1*muon1v3_trees.root/*.root",
    #"Zmm MC": "user.tholmes.mc16_13TeV*muon1v3_trees.root/*.root",
    "Data": "data17*slimmed.root",
    "Zmm MC": "mc*mc16d*slimmed.root",
    #"Zmm MC": "user.tholmes.mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.mc16a_muon1v3_trees.root/user.tholmes.361107.e3601_e5984_s3126_s3136_r9364_r9315_p4145.21754904._000001.trees.root",
    }

# For a given event with two muons (same as signal)
# Require that it passes the support trigger
# What is the probability it passes given pt and eta of leading and subleading muon?

# Select events
support_trigs = "pass_HLT_xe110_pufit_xe70_L1XE50 || pass_HLT_xe110_pufit_xe65_L1XE50 || pass_HLT_xe90_pufit_L1XE50 || pass_HLT_xe110_pufit_L1XE55 || pass_HLT_xe110_pufit_L1XE50 || pass_HLT_xe90_mht_L1XE50 || pass_HLT_xe110_mht_L1XE50 || pass_HLT_xe70_mht"
#support_trigs = "pass_HLT_xe110_pufit_xe65_L1XE50"

m_sel = Selection("passing_muon")
m_sel.addCut("muon_pt", 65)
m_sel.addCut("muon_isNotCosmic")
m_sel.addCut("muon_isGoodQual")
m_sel.addCut("muon_isPassIDtrk")
m_sel.addCut("muon_isIsolated")
m_sel.addCut("invMass", minval=81, maxval=101)

m_sel_str = m_sel.getCutString()
#m_sel_str = "( %s && %s )"%(m_sel.getCutString(0), m_sel.getCutString(1))
sel_str = "( (%s) && %s )"%(support_trigs, m_sel_str)
print sel_str

variables = {
        "pt":       {"nbins": 50,    "xmin": 0.,     "xmax": 250.,     "xlabel": "Matched Muon p_{T} [GeV]",     "acc": "muon_pt"},
        "eta":      {"nbins": 50,    "xmin": -2.5,   "xmax": 2.5,      "xlabel": "Matched Muon #eta",            "acc": "muon_eta"},
        "phi":      {"nbins": 20,    "xmin": -3.2,   "xmax": 3.2,      "xlabel": "Matched Muon #phi",            "acc": "muon_phi"},
        #"pt0":       {"nbins": 25,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Leading Lepton p_{T} [GeV]",     "acc": "muon_pt[0]"},
        #"eta0":      {"nbins": 20,    "xmin": -2.5,   "xmax": 2.5,      "xlabel": "Leading Lepton #eta",            "acc": "muon_eta[0]"},
        #"phi0":      {"nbins": 20,    "xmin": -3.2,   "xmax": 3.2,      "xlabel": "Leading Lepton #phi",            "acc": "muon_phi[0]"},
        #"pt1":       {"nbins": 25,    "xmin": 0.,     "xmax": 500.,     "xlabel": "Subeading Lepton p_{T} [GeV]",   "acc": "muon_pt[1]"},
        #"eta1":      {"nbins": 20,    "xmin": -2.5,   "xmax": 2.5,      "xlabel": "Subeading Lepton #eta",          "acc": "muon_eta[1]"},
        #"phi1":      {"nbins": 20,    "xmin": -3.2,   "xmax": 3.2,      "xlabel": "Subeading Lepton #phi",          "acc": "muon_phi[1]"},
        }

hists = {}
for f in files:
    hists[f] = {}
    t = getTree(os.path.join(d, files[f]))

    allsel = "mcEventWeight * muSF * %s"%sel_str
    passsel = "mcEventWeight * muSF * (%s && pass_HLT_mu60_0eta105_msonly && muon_TrigMatchMSOnly)"%sel_str
    if f=="Data":
        allsel = "muSF * %s"%sel_str
        passsel = "muSF * (%s && pass_HLT_mu60_0eta105_msonly && muon_TrigMatchMSOnly)"%sel_str

    for v in variables:
        hists[f][v] = {}
        binstr = getBinStr(variables[v])
        #hists[f][v]["all"] = getHist(t, "h_%s_%s_all"%(f,v), variables[v]["acc"], binstr, sel_name=allsel)
        #hists[f][v]["pass"] = getHist(t, "h_%s_%s_pass"%(f,v), variables[v]["acc"], binstr, sel_name=passsel)
        #print f, v
        #print "all:", hists[f][v]["all"].Integral(), "pass:", hists[f][v]["pass"].Integral()

    hists[f]["etaphi"] = {}
    binstreta = getBinStr(variables["eta"])
    binstrphi = getBinStr(variables["phi"])
    hists[f]["etaphi"]["all"] = getHist(t, "h_%s_etaphi_all"%(f), "%s:%s"%(variables["phi"]["acc"],variables["eta"]["acc"]), "%s,%s"%(binstreta,binstrphi), sel_name=allsel)
    hists[f]["etaphi"]["pass"] = getHist(t, "h_%s_etaphi_pass"%(f), "%s:%s"%(variables["phi"]["acc"],variables["eta"]["acc"]), "%s,%s"%(binstreta,binstrphi), sel_name=passsel)

for v in []:#variables:
    can = ROOT.TCanvas("c_%s"%(v), "c_%s"%(v))
    effs = {}
    for i,f in enumerate(files):
        print "Drawing", f, "for variable", v
        effs[f] = ROOT.TGraphAsymmErrors(hists[f][v]["pass"], hists[f][v]["all"])
        if i==0:
            effs[f].Draw("alpe")
            effs[f].GetYaxis().SetRangeUser(0,1.4)
            ROOT.gPad.Update()
        else:
            effs[f].Draw("lpe same")
        effs[f].SetTitle("%s;%s;%s"%(f,variables[v]["xlabel"], "Trigger Efficiency"))
        effs[f].SetLineColor(theme_colors[i])
        effs[f].SetMarkerColor(theme_colors[i])

    leg = can.BuildLegend(.5,.75,.8,.90)

    ROOT.ATLASLabel(0.2,0.85, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.78, "Signal Muons, %.f fb^{-1}"%(lumi*0.001))

    can.SaveAs("plots/h_%s%s.pdf"%(v, append))

for v in []: # variables:
    ratios = {}
    for f in files:
        hp = hists[f][v]["pass"]
        ha = hists[f][v]["all"]
        ratios[f]=hp.Clone()
        ratios[f].Divide(hp, ha, 1, 1, "B")
        ratios[f].SetTitle(f)
    print type(ratios["Data"]), type(ratios["Zmm MC"])
    makeRatioPlot(ratios["Data"], ratios["Zmm MC"], variables[v]["xlabel"], "plots/r_%s%s.pdf"%(v, append))

outfile = ROOT.TFile("f%s.root"%append, "RECREATE")

h_data = hists["Data"]["etaphi"]["pass"].Clone("h_eff_data")
h_data.Divide(h_data, hists["Data"]["etaphi"]["all"], 1, 1, "B")
h_mc = hists["Zmm MC"]["etaphi"]["pass"].Clone("h_eff_mc")
h_mc.Divide(h_mc, hists["Zmm MC"]["etaphi"]["all"], 1, 1, "B")
h_sf = h_data.Clone("h_sf")
h_sf.Divide(h_data, h_mc, 1, 1, "B")

h_data.Write()
h_mc.Write()
h_sf.Write()

can = ROOT.TCanvas("c_etaphi", "c_etaphi")
h_sf.GetXaxis().SetTitle(variables["eta"]["xlabel"])
h_sf.GetYaxis().SetTitle(variables["phi"]["xlabel"])
h_sf.SetMinimum(0)
h_sf.SetMaximum(2)
h_sf.Draw("colz")

ROOT.ATLASLabel(0.2,0.85, "Internal")
text = ROOT.TLatex()
text.SetNDC()
text.SetTextSize(0.04)
text.DrawLatex(0.2,0.78, "Signal Muons, Data/Zmm MC, %.f fb^{-1}"%(lumi*0.001))
text.DrawLatex(0.2,0.71, "Ratio of MS-Only Trigger Efficiencies")

can.SaveAs("plots/h2d_etaphi%s.pdf"%append)
#can.SaveAs("plots/h2d_etaphi%s.root"%append)

outfile.Close()

