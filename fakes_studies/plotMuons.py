
import glob
import ROOT
import os
from array import array

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/math_helpers.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBlackBody)
#ROOT.gStyle.SetPalette(ROOT.kSolar)
#ROOT.gStyle.SetPalette(ROOT.kRainBow)

# Loop over files and make plots
#fname = "/eos/user/l/lhoryn/highd0/111918/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.112818_trees.root/*.root"
#fname = "/eos/user/l/lhoryn/highd0/120318/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.120318_v2_trees.root/user.lhoryn.16291880._000001.trees.root"
#fname = "/eos/user/l/lhoryn/highd0/120318/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.120718_trees.root/*.root"
#fname = "/eos/user/l/lhoryn/highd0/photons/*361044*/*.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/*361044*/*.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/ttbar.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar.root"
fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar_lowpt/ttbar.root"

t = getTree(fname)

# Normalize
#ds_name = glob.glob(fname)[0]
#print ds_name
#xs = getXS(ds_name.split(".")[3])
#n_events = getNEvents(fname)
#print xs, n_events
#event_weight = lumi*xs/n_events
#print event_weight
#event_weight = lumi*getXS(410470)/getNEvents(fname)

categories = ["prompt", "heavy flavor", "other"]
variables = ["pt", "d0", "d0g50", "d0g50wide", "d0sig", "d0sigwide"]
bins = {
        #"pt": {"nbins": 10, "xmin": 0., "xmax": 600., "xlab": "p_{T} [GeV]"},
        "pt": {"nbins": 20, "xmin": 0., "xmax": 400., "xlab": "p_{T} [GeV]"},
        #"d0": {"nbins": 20, "xmin": 0., "xmax": 20., "xlab": "d_{0} [mm]"},
        "d0": {"nbins": 40, "xmin": 0., "xmax": 10., "xlab": "d_{0} [mm]"},
        "d0g50": {"nbins": 10, "xmin": 0., "xmax": 10., "xlab": "d_{0} [mm]"},
        "d0g50wide": {"nbins": 30, "xmin": 0., "xmax": 300., "xlab": "d_{0} [mm]"},
        "d0sig": {"nbins": 10, "xmin": 0., "xmax": 10., "xlab": "d_{0} [mm]"},
        "d0sigwide": {"nbins": 30, "xmin": 0., "xmax": 300., "xlab": "d_{0} [mm]"},
        "dR": {"nbins": 10, "xmin": 0., "xmax": 6.3, "xlab": "d_{0} [mm]"},
        }

h_conv_same_pt1 = ROOT.TH1F("h_conv_same_pt1", "Leading e, shared parent", bins["pt"]["nbins"], bins["pt"]["xmin"], bins["pt"]["xmax"])
h_conv_diff_pt1 = ROOT.TH1F("h_conv_diff_pt1", "Leading e, different parents", bins["pt"]["nbins"], bins["pt"]["xmin"], bins["pt"]["xmax"])
h_conv_same_d01 = ROOT.TH1F("h_conv_same_d01", "Leading e, shared parent", bins["d0"]["nbins"], bins["d0"]["xmin"], bins["d0"]["xmax"])
h_conv_diff_d01 = ROOT.TH1F("h_conv_diff_d01", "Leading e, different parents", bins["d0"]["nbins"], bins["d0"]["xmin"], bins["d0"]["xmax"])
h_conv_same_pt2 = ROOT.TH1F("h_conv_same_pt2", "Subleading e, shared parent", bins["pt"]["nbins"], bins["pt"]["xmin"], bins["pt"]["xmax"])
h_conv_diff_pt2 = ROOT.TH1F("h_conv_diff_pt2", "Subleading e, different parents", bins["pt"]["nbins"], bins["pt"]["xmin"], bins["pt"]["xmax"])
h_conv_same_d02 = ROOT.TH1F("h_conv_same_d02", "Subleading e, shared parent", bins["d0"]["nbins"], bins["d0"]["xmin"], bins["d0"]["xmax"])
h_conv_diff_d02 = ROOT.TH1F("h_conv_diff_d02", "Subleading e, different parents", bins["d0"]["nbins"], bins["d0"]["xmin"], bins["d0"]["xmax"])
h_conv_same_dR = ROOT.TH1F("h_conv_same_dR", "Electrons with shared parent", bins["dR"]["nbins"], bins["dR"]["xmin"], bins["dR"]["xmax"])
h_conv_diff_dR = ROOT.TH1F("h_conv_diff_dR", "Electrons with different parents", bins["dR"]["nbins"], bins["dR"]["xmin"], bins["dR"]["xmax"])

append = ""
#append = "_dpt"
#append = "_wide"
hists = {}
for c in categories:
    hists[c] = {}
    for v in variables: hists[c][v] = ROOT.TH1F("h_%s_%s"%(c, v), c, bins[v]["nbins"], bins[v]["xmin"], bins[v]["xmax"])

def getTruthIndex(event, el_i):

    # Find truth particle
    true_barcode = event.muon_truthMatchedBarcode[el_i]
    if true_barcode == -1: return -1

    for t_i in range(len(event.truthLepton_pt)):
        if true_barcode==event.truthLepton_barcode[t_i]: return t_i

    return -2

i_event = 0
for event in t:

    if i_event%10000==0: print "Processing event %d..."%i_event
    #if i_event>20: break

    conv_pts = []
    conv_d0s = []
    conv_phis = []
    conv_etas = []
    conv_fcloose = []
    conv_fctight = []
    photon_parents = []
    n_fakes = 0
    for i in range(len(event.muon_pt)):

        #if event.muon_dpt[i] < -0.5: continue
        ttype = event.muon_truthType[i]
        torigin = event.muon_truthOrigin[i]

        cat = "other"
        if ttype in [13, 14, 15, 16]: cat = "photon"
        elif torigin in xrange(23,36): cat = "heavy flavor"
        elif torigin in [9, 10, 12]: cat = "prompt"
        elif torigin == 5: cat = "conversion"

        truth_i = getTruthIndex(event, i)
        if False and  torigin==10 and ttype==2 and event.muon_pt[i]>50 and abs(event.muon_IDtrack_d0[i])>2:
            print "\nNon-prompt prompt"
            print "pdgid:           ", event.truthLepton_pdgId[truth_i]
            print "parent:          ", event.truthLepton_parentPdgId[truth_i]
            print "barcode:         ", event.truthLepton_barcode[truth_i]
            print "parent barcode:  ", event.truthLepton_parentBarcode[truth_i]
            print "truth pt:        ", event.truthLepton_pt[truth_i]
            print "pt:              ", event.muon_pt[i]
            print "trackpt:         ", event.muon_trackpt[i]
            print "chi2:            ", event.muon_chi2[i]
            print "missing:         ", event.muon_nMissingLayers[i]
            print "d0:              ", event.muon_IDtrack_d0[i]

        '''
        cat = "non-conversion"
        origin = event.muon_truthOrigin[i]
        truth_i = getTruthIndex(event, i)
        if truth_i<0: cat = "no truth match"
        else:
            pdgid = event.truthLepton_pdgId[truth_i]
            parent_pdgid = abs(event.truthLepton_parentPdgId[truth_i])
            barcode = event.truthLepton_barcode[truth_i]
            parent_barcode = event.truthLepton_parentBarcode[truth_i]
            #if abs(pdgid) == 11 and (parent_pdgid > 1000000 or parent_barcode > 200000): cat = "real"
            if abs(pdgid) == 11 and origin in [9,10,12]: cat = "prompt" # https://acode-browser.usatlas.bnl.gov/lxr/source/athena/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
            elif abs(pdgid) == 11 and origin in [xrange(23,36)]: cat = "heavy flavor"
            elif (abs(pdgid)==11 and parent_pdgid==22):
                photon_parents.append(parent_barcode)
                conv_pts.append(event.muon_pt[i])
                conv_d0s.append(event.muon_IDtrack_d0[i])
                conv_phis.append(event.muon_phi[i])
                conv_etas.append(event.muon_eta[i])
                conv_fcloose.append(event.muon_FCLoose[i])
                conv_fctight.append(event.muon_FCTight[i])
                if event.truthLepton_generatorParent_pdgId[truth_i]==22: cat = "converted photon"
                elif abs(event.truthLepton_generatorParent_pdgId[truth_i])==11: cat = "converted muon"
                #print event.truthLepton_generatorParent_pdgId[truth_i]
                #cat = "conversion"
            elif pdgid==22: cat = "photon"

        if cat != "prompt": n_fakes += 1

        '''
        hists[cat]["pt"].Fill(event.muon_pt[i], event.normweight*lumi)
        hists[cat]["d0"].Fill(event.muon_IDtrack_d0[i], event.normweight*lumi)
        if event.muon_pt[i]>50:
            hists[cat]["d0g50"].Fill(event.muon_IDtrack_d0[i], event.normweight*lumi)
            hists[cat]["d0g50wide"].Fill(event.muon_IDtrack_d0[i], event.normweight*lumi)
        if event.muon_isSignal[i] and abs(event.muon_t0avg[i])<30:
            hists[cat]["d0sig"].Fill(event.muon_IDtrack_d0[i], event.normweight*lumi)
            hists[cat]["d0sigwide"].Fill(event.muon_IDtrack_d0[i], event.normweight*lumi)

    '''
    #if n_fakes>1: print "Nfakes = ", n_fakes
    if len(photon_parents)>1:
        print "\nParent barcodes:", photon_parents
        print "pts:", conv_pts
        print "d0s:", conv_d0s

        dR = getDR(conv_phis[0], conv_phis[1], conv_etas[0], conv_etas[1])

        if photon_parents[1]==photon_parents[0]:
            print "\tFound muons from same parent photon."
            print "\tFCLoose?", conv_fcloose
            print "\tFCTight?", conv_fctight
            print "\tdR?", dR

        if photon_parents[0]==photon_parents[1]:
            h_conv_same_dR.Fill(dR)
            h_conv_same_pt1.Fill(conv_pts[0])
            h_conv_same_d01.Fill(conv_d0s[0])
            h_conv_same_pt2.Fill(conv_pts[1])
            h_conv_same_d02.Fill(conv_d0s[1])
        else:
            h_conv_diff_dR.Fill(dR)
            h_conv_diff_pt1.Fill(conv_pts[0])
            h_conv_diff_d01.Fill(conv_d0s[0])
            h_conv_diff_pt2.Fill(conv_pts[1])
            h_conv_diff_d02.Fill(conv_d0s[1])
    '''

    i_event += 1

# Make histograms

can_pt = ROOT.TCanvas("cansamept", "cansamept")
h_conv_same_pt1.SetMaximum(2.5*h_conv_same_pt1.GetMaximum())
h_conv_same_pt1.GetXaxis().SetTitle("p_{T} [GeV]")
h_conv_same_pt1.Draw("e PLC PMC")
h_conv_same_pt2.Draw("same e PLC PMC")
h_conv_diff_pt1.Draw("same e PLC PMC")
h_conv_diff_pt2.Draw("same e PLC PMC")
leg = can_pt.BuildLegend(.5,.65,.8,.90)
leg.Draw()
#raw_input("...")

can_d0 = ROOT.TCanvas("cansamed0", "cansamed0")
h_conv_same_d01.SetMaximum(2.5*h_conv_same_d01.GetMaximum())
h_conv_same_d01.GetXaxis().SetTitle("d_{0} [mm]")
h_conv_same_d01.Draw("e PLC PMC")
h_conv_same_d02.Draw("same e PLC PMC")
h_conv_diff_d01.Draw("same e PLC PMC")
h_conv_diff_d02.Draw("same e PLC PMC")
leg = can_d0.BuildLegend(.5,.65,.8,.90)
leg.Draw()
#raw_input("...")

can_dr = ROOT.TCanvas("candr", "candr")
h_conv_same_dR.SetMaximum(2*h_conv_same_dR.GetMaximum())
h_conv_same_dR.GetXaxis().SetTitle("#Delta R")
h_conv_same_dR.Draw("e PLC PMC")
h_conv_diff_dR.Draw("same e PLC PMC")
leg = can_dr.BuildLegend(.5,.65,.8,.90)
leg.Draw()
#raw_input("...")

cans = {}
for v in variables:

    print v
    cans[v] = ROOT.TCanvas("can%s"%v, "can%s"%v)
    print v

    for i, cat in enumerate(hists):
        h = hists[cat][v]
        h.GetXaxis().SetTitle(bins[v]["xlab"])
        h.GetYaxis().SetTitle("Number of Muons")
        h.SetLineColor(theme_colors2[i])
        h.SetMarkerColor(theme_colors2[i])
        h.SetMaximum(1e8)
        h.SetMinimum(1e-2)
        if i==0: h.Draw("e")# PLC PMC")
        else: h.Draw("same e")# PLC PMC")

    cans[v].SetLogy()

    leg = cans[v].BuildLegend(.5,.65,.8,.90)
    leg.Draw()

    ROOT.ATLASLabel(0.2,0.85, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.78, "ttbar, %.f ifb"%(lumi*0.001))

    cans[v].Update()
    #raw_input("...")
    cans[v].SaveAs("h_m_%s%s.pdf"%(v,append))

