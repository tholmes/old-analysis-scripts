# Goal: Make plots of photon-related electron backgrounds compared to signal

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

variables = {
        "pt":       {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "Electron p_{T} [GeV]"},
        "d0":       {"attr": "abs(OBJ_d0[0])",       "nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "Electron d_{0} [mm]"},
        "id_pt":    {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "Electron ID Track p_{T} [GeV]"},
        "qop_err":  {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 0.0001,   "xlabel": "Electron q/p Error [1/MeV]"},
        "rel_qop_err":  {"attr": "OBJ_pt[0]",        "nbins": 40,    "xmin": 0.,     "xmax": 5.,        "xlabel": "Electron q/p Error / q/p"},
        "rel_pt_err":  {"attr": "OBJ_pt[0]",        "nbins": 40,    "xmin": 0.,     "xmax": 5.,        "xlabel": "Electron p_{T} Error / p_{T}"},
        "sig":      {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -10.,     "xmax": 10.,       "xlabel": "(p_{T} - p_{T}^{track}) / p_{T} Error)"},
        "dpt":      {"attr": "OBJ_pt[0]",            "nbins": 60,    "xmin": -1.,     "xmax": 1.,      "xlabel": "(p_{T}^{track} - p_{T}) / p_{T}"},
        "chi2":     {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 20.,     "xlabel": "#chi^{2}/n_{DoF}"},
        "ptcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptcone20/pt"},
        "ptcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptcone30/pt"},
        "ptcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptcone40/pt"},
        "ptvarcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptvarcone20/pt"},
        "ptvarcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptvarcone30/pt"},
        "ptvarcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "ptvarcone40/pt"},
        "topoetcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "topoetcone20/pt"},
        "topoetcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "topoetcone30/pt"},
        "topoetcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 1.,      "xlabel": "topoetcone40/pt"},
        }

snames = {
        "361044":           "Photons, 70 < p_{T} < 140, BFilter",
        "photon":           "Photons, p_{T} > 70 GeV, BFilter",
        "SlepSlep*100*0p1": "Sleptons, m = 100, #tau=0.1 ns",
        "StauStau*100*0p1": "Staus, m = 100, #tau=0.1 ns",
        "StauStau*300*0p1": "Staus, m = 300, #tau=0.1 ns",
        "SlepSlep*300*1ns": "Sleptons, m = 300, #tau=1 ns",
        "SlepSlep*500*0p1": "Sleptons, m = 500, #tau=0.1 ns",
        }

def getVarHist(df, var, varname, sample_label):
    if sample_label == "photon":
        #weight_str = '''
        #    std::vector<float> weights;
        #    for (int i=0; i<%s.size(); i++)
        #        weights.push_back(normweight*%f);
        #    return weights;'''%(varname, lumi)
        #df_weights = df.Define("weight", weight_str)
        h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname, "weight")
    else:
        h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname)
        h.Scale(samples[sample_label]["weight"])
    h.GetXaxis().SetTitle(variables[var]["xlabel"])
    h.GetYaxis().SetTitle("Events")
    return h

def getPtErr():

    #s(pt)/pt = pt**2 * (s(qop)/qop)**2
    return 0


# Settings
lumi = 140000
signalstau_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal_wip"
signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal_wip"
photon_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/photons"
#ttbar_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/ttbar"

#signalstau_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/"
#signal_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/"
#photon_d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/"

# Select signals for comparison
signals = ["SlepSlep*100*0p1", "StauStau*100*0p1", "SlepSlep*300*1ns", "SlepSlep*500*0p1", "StauStau*300*0p1"]
#photons = ["361044"]
photons = ["photon"]

# Collect samples and weights
samples = {}
for samplename in signals+photons:
    # Get filename
    base_name = ""  # To be used for getting xs
    file_names = [] # To be used for collecting all samples
    for d in [signalstau_d, signal_d, photon_d]:
        sets = glob.glob("%s/*%s*"%(d,samplename))
        if len(sets)>=1:
            base_name = sets[0].split("/")[-1]
            file_names = sets
            print "Found", len(file_names), "samples(s):\n\t", file_names
            break
    if base_name == "":
        print "Couldn't find file for sample", samplename
        exit()

    # Normalize
    event_weight = -1
    if not samplename=="photon":
        xs = getXS(base_name.split(".")[3])
        n_events = 0
        for f in file_names:
            n_events += getNEvents(f+"/*.root")
        event_weight = lumi*xs/n_events

    # Add to tree
    is_signal = False
    if samplename in signals: is_signal = True
    samples[samplename] = {"files": file_names, "weight": event_weight, "hists": {}, "is_signal": is_signal}

# Define selection
#append = "pt65_d03_dptcut_chi2cut"
append = "pt65_d03"

e_sel = Selection()
e_sel.addCut("electron_pt", 65)
e_sel.addCut("electron_passOR")
e_sel.addCut("abs(electron_d0)", 3)
#e_sel.addCut("electron_dpt", -0.5)
#e_sel.addCut("electron_chi2", maxval=2)
e_def = e_sel.getCutString()
print "\nSelection for an electron:", e_def

for s in samples:

    print "\nWorking with sample", s

    # Get dataframe
    t = ROOT.TChain("trees_SR_highd0_")
    if s == "photon":
        for f in samples[s]["files"]: t.Add(f)
    else:
        for f in samples[s]["files"]: t.Add(f+"/*.root")
    dframe = ROOT.ROOT.RDataFrame(t)
    print "Found events:", t.GetEntries()

    # Do basic event skimming
    e_selection = e_sel.nSelectedString("electron_pt")
    df_baselinedpt = dframe.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt")       # Relative difference btw electron pT and track pT
    df_edef = dframe.Define("e_n", e_selection)
    df_ecut = df_edef.Filter("e_n>0", "At least one selected electron")
    #df_ecut = df_edef.Filter("e_n>1", "At least two selected electrons")

    # Apply selection to variables
    e_sel_pt = e_sel.selectedVectorString("electron_pt")
    e_sel_d0 = e_sel.selectedVectorString("electron_d0")
    e_sel_qop_err = e_sel.selectedVectorString("electron_qop_err")
    e_sel_qop = e_sel.selectedVectorString("electron_qop")
    e_sel_chi2 = e_sel.selectedVectorString("electron_chi2")
    e_sel_ptcone20 = e_sel.selectedVectorString("electron_ptcone20")
    e_sel_ptcone30 = e_sel.selectedVectorString("electron_ptcone30")
    e_sel_ptcone40 = e_sel.selectedVectorString("electron_ptcone40")
    e_sel_ptvarcone20 = e_sel.selectedVectorString("electron_ptvarcone20")
    e_sel_ptvarcone30 = e_sel.selectedVectorString("electron_ptvarcone30")
    e_sel_ptvarcone40 = e_sel.selectedVectorString("electron_ptvarcone40")
    e_sel_topoetcone20 = e_sel.selectedVectorString("electron_topoetcone20")
    e_sel_topoetcone30 = e_sel.selectedVectorString("electron_topoetcone30")
    e_sel_topoetcone40 = e_sel.selectedVectorString("electron_topoetcone40")
    e_sel_id_pt = e_sel.selectedVectorString("electron_trackpt")

    # Not needed in newer samples -- > to be removed
    # This one is funky
    #e_sel_id_pt = '''
    #        auto v = electron_IDtrack_pt;
    #        v.clear();
    #        for (int i=0; i<electron_pt.size(); i++)
    #            if (%s)
    #                v.push_back((double)electron_IDtrack_pt[electron_IDtrack_electronindex[i]]);
    #        return v;'''%e_sel.getCutString("i")

    df_ept = df_ecut.Define("e_pt", e_sel_pt).Define("e_d0", e_sel_d0).Define("e_id_pt", e_sel_id_pt).Define("e_qop_err", e_sel_qop_err).Define("e_qop", e_sel_qop).Define("e_chi2", e_sel_chi2)
    df_iso = df_ept.Define("e_ptcone20", e_sel_ptcone20).Define("e_ptcone30", e_sel_ptcone30).Define("e_ptcone40", e_sel_ptcone40).Define("e_ptvarcone20", e_sel_ptvarcone20).Define("e_ptvarcone30", e_sel_ptvarcone30).Define("e_ptvarcone40", e_sel_ptvarcone40).Define("e_topoetcone20", e_sel_topoetcone20).Define("e_topoetcone30", e_sel_topoetcone30).Define("e_topoetcone40", e_sel_topoetcone40)

    # Make combo variables
    df_vars = df_iso.Define("e_dpt", "(e_id_pt - e_pt)/e_pt")       # Relative difference btw electron pT and track pT
    df_rqop = df_vars.Define("e_rel_qop_err", "e_qop_err/e_qop")    # Relative qop uncertainty
    df_pterr = df_rqop.Define("e_rel_pt_err", "e_pt*e_pt * e_rel_qop_err*e_rel_qop_err")  # Relative pT uncertainty
    df_sig = df_pterr.Define("e_sig", "e_dpt/(e_pt*e_rel_pt_err)")       # Significance of difference btw electron pT and track pT

    # Make relative isolation variables
    df_reliso = df_sig.Define("e_rel_ptcone20", "e_ptcone20/e_pt").Define("e_rel_ptcone30", "e_ptcone30/e_pt").Define("e_rel_ptcone40", "e_ptcone40/e_pt").Define("e_rel_ptvarcone20", "e_ptvarcone20/e_pt").Define("e_rel_ptvarcone30", "e_ptvarcone30/e_pt").Define("e_rel_ptvarcone40", "e_ptvarcone40/e_pt").Define("e_rel_topoetcone20", "e_topoetcone20/e_pt").Define("e_rel_topoetcone30", "e_topoetcone30/e_pt").Define("e_rel_topoetcone40", "e_topoetcone40/e_pt")

    # Do stupid vectorizing of event weights for photons
    df_weights = None
    if s == "photon":
        weight_str = '''
            std::vector<float> weights;
            for (int i=0; i<%s.size(); i++)
                weights.push_back(normweight*%f);
            return weights;'''%("e_pt", lumi)
        df_weights = df_reliso.Define("weight", weight_str)
    else: df_weights = df_reliso

    # Make plots of basic variables
    df = df_weights
    samples[s]["hists"]["e_pt"] = getVarHist(df, "pt", "e_pt", s)
    samples[s]["hists"]["e_d0"] = getVarHist(df, "d0", "e_d0", s)
    samples[s]["hists"]["e_id_pt"] = getVarHist(df, "id_pt", "e_id_pt", s)
    samples[s]["hists"]["e_qop_err"] = getVarHist(df, "qop_err", "e_qop_err", s)
    samples[s]["hists"]["e_chi2"] = getVarHist(df, "chi2", "e_chi2", s)
    samples[s]["hists"]["e_rel_qop_err"] = getVarHist(df, "rel_qop_err", "e_rel_qop_err", s)
    samples[s]["hists"]["e_dpt"] = getVarHist(df, "dpt", "e_dpt", s)
    samples[s]["hists"]["e_rel_pt_err"] = getVarHist(df, "rel_pt_err", "e_rel_pt_err", s)
    samples[s]["hists"]["e_sig"] = getVarHist(df, "sig", "e_sig", s)
    samples[s]["hists"]["e_ptcone20"] = getVarHist(df, "ptcone20", "e_rel_ptcone20", s)
    samples[s]["hists"]["e_ptcone30"] = getVarHist(df, "ptcone30", "e_rel_ptcone30", s)
    samples[s]["hists"]["e_ptcone40"] = getVarHist(df, "ptcone40", "e_rel_ptcone40", s)
    samples[s]["hists"]["e_ptvarcone20"] = getVarHist(df, "ptvarcone20", "e_rel_ptvarcone20", s)
    samples[s]["hists"]["e_ptvarcone30"] = getVarHist(df, "ptvarcone30", "e_rel_ptvarcone30", s)
    samples[s]["hists"]["e_ptvarcone40"] = getVarHist(df, "ptvarcone40", "e_rel_ptvarcone40", s)
    samples[s]["hists"]["e_topoetcone20"] = getVarHist(df, "topoetcone20", "e_rel_topoetcone20", s)
    samples[s]["hists"]["e_topoetcone30"] = getVarHist(df, "topoetcone30", "e_rel_topoetcone30", s)
    samples[s]["hists"]["e_topoetcone40"] = getVarHist(df, "topoetcone40", "e_rel_topoetcone40", s)
    #h.Draw()
    #raw_input("...")

    # Cutflow for fun
    report = dframe.Report()
    report.Print()

# Collect hists
hists = samples[s]["hists"].keys()
print hists
cans = {}
for htype in hists:
    cans[htype] = ROOT.TCanvas(htype, htype)

    y_max = 0
    for s in samples:
        y_max = max(y_max, samples[s]["hists"][htype].GetMaximum())
        if samples[s]["is_signal"]:
            samples[s]["hists"][htype].SetMarkerStyle(4)

    for i, s in enumerate(samples):
        if i==0:
            samples[s]["hists"][htype].SetMaximum(10**9)
            samples[s]["hists"][htype].SetMinimum(10**-4)
            #samples[s]["hists"][htype].SetMaximum(100*y_max)
            samples[s]["hists"][htype].Draw("e PLC PMC")
        else: samples[s]["hists"][htype].Draw("same e PLC PMC")

    #leg = cans[htype].BuildLegend(.5,.65,.8,.90)
    leg = cans[htype].BuildLegend(.6,.65,.8,.90)
    leg.Draw()

    cans[htype].SetLogy()

    #ROOT.ATLASLabel(0.2,0.85, "Internal")
    ROOT.ATLASLabel(0.2,0.85, "Work In Progress")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.78, "Simulation, 140 fb^{-1}")

    #raw_input("...")
    cans[htype].SaveAs("plots/%s_%s.pdf"%(htype, append))

exit()
