
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
ROOT.ROOT.EnableImplicitMT()

#ROOT.gStyle.SetPalette(ROOT.kColorPrintableOnGrey)
ROOT.gStyle.SetPalette(ROOT.kBlackBody)

def getFilterString(var, reqs):
    string =    "auto v = %s; v.clear(); "%var
    string +=   "for (int i=0; i<%s.size(); i++) "%var
    string +=   "if (%s) v.push_back((double)%s[i]); "%(reqs, var)
    string +=   "return v;"
    return string

#fname = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/user.lhoryn.mc16_13TeV.399047.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_0p1ns.092618_trees.root/user.lhoryn.15507181._000001.trees.root"
#fname = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/user.lhoryn.mc16_13TeV.399032.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_100_0_1ns.092618_trees.root/user.lhoryn.15507190._000001.trees.root"
fname = "/eos/user/l/lhoryn/highd0/111918/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.111918_trees.root/user.lhoryn.16098865._000001.trees.root"
dframe = ROOT.ROOT.RDataFrame("trees_SR_highd0_", fname)
print "Running on file:", fname
print "N entries:", dframe.Count().GetValue()

# idtrk = filtered collection
#reqs_for_tracks = "idTrack_pt[i]>50 && abs(idTrack_eta[i])<2.5"
#df = dframe.Define("idtrk_pt", getFilterString("idTrack_pt", reqs_for_tracks))
#df = df.Define("idtrk_eta", getFilterString("idTrack_eta", reqs_for_tracks))
#df = df.Define("idtrk_d0", getFilterString("idTrack_d0", reqs_for_tracks))

reqs = "electron_pt[i]>10 && abs(electron_eta[i])<2.5"
fakereqs = reqs + " && electron_truthMatchedBarcode[i]==-1"
df = dframe.Define("e_pt", getFilterString("electron_pt", reqs))
df =    df.Define("e_pt_fake", getFilterString("electron_pt", fakereqs))
df =    df.Define("e_d0", getFilterString("electron_d0", reqs))
df =    df.Define("e_d0_fake", getFilterString("electron_d0", fakereqs))

can = ROOT.TCanvas("can", "can")
h_e_pt = df.Histo1D( ("e_pt", "Electron p_{T}", 50, 0., 500.), "e_pt")
h_e_pt_fake = df.Histo1D( ("e_pt_fake", "Fake Electron p_{T}", 100, 0., 500.), "e_pt_fake")
h_e_pt.Draw("e PLC PMC")
h_e_pt_fake.Draw("same e PLC PMC")
leg = can.BuildLegend()
can.Draw()
raw_input("...")
can.SaveAs("fake_pt.eps")

can1 = ROOT.TCanvas("can1", "can1")
h_e_d0 = df.Histo1D( ("e_d0", "Electron d_{0}", 30, 0., 100.), "e_d0")
h_e_d0_fake = df.Histo1D( ("e_d0_fake", "Fake Electron d_{0}", 100, 0., 500.), "e_d0_fake")
h_e_d0.Draw("e PLC PMC")
h_e_d0_fake.Draw("same e PLC PMC")
leg1 = can1.BuildLegend()
can1.Draw()
raw_input("...")
can1.SaveAs("fake_d0.eps")

'''



d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples"
for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt
        t = getTree("%s/*_%s_*_%sns*/*.root"%(d,m,lt))
        if t==-1: continue

        # Normalize
        n_events = t.GetEntries()
        ev_weight = lumi*2.*cross_sections[m]/n_events

        # Loop over SRs
        for sr in SRs:
            sel = SRs[sr]

            # Loop over events in tree
            n_events=0
            for event in t:
                if n_events%500==0: print "Processing event", n_events
                n_events += 1
                #if n_events>10: break

                # Get indices of true electrons and loop
                true_indices = [i for i in range(len(event.truthLepton_pdgid)) if abs(event.truthLepton_pdgid[i]==11)]
                if len(true_indices)==0: continue

                for i_te in true_indices:

                    barcode = event.truthLepton_barcode[i_te]
                    true_pt = event.truthLepton_pt[i_te]
                    true_d0 = event.truthLepton_d0[i_te]

                    if true_pt < sel.cuts["pt"]["min"]: continue
                    hists[sr]["true_d0"].Fill(true_d0)

                    # Find matching reco electron
                    i_e = -1
                    for i in xrange(len(event.electron_pt)):
                        if barcode == event.electron_truthMatchedBarcode[i]:
                            i_e = i
                            break
                    if i_e == -1: continue

                    hists[sr]["true_d0_reco"].Fill(true_d0)

                    # Get first and best d0s
                    best_d0_i = 0
                    best_pt_i = 0
                    for j in range(len(event.electron_IDtrack_d0)):
                        if abs(true_d0 - abs(event.electron_IDtrack_d0[j])) < abs(true_d0 - abs(event.electron_IDtrack_d0[best_d0_i])):
                            #print "\nj=", j
                            #print "diff here:", abs(true_d0 - event.electron_IDtrack_d0[j])
                            #print "old best :", abs(true_d0 - event.electron_IDtrack_d0[best_d0])
                            best_d0_i = j
                        if abs(true_pt - event.electron_IDtrack_pt[j]) < abs(true_pt - event.electron_IDtrack_pt[best_pt_i]): best_pt_i = j


            n_electrons = len(event.electron_pt)
            if n_electrons==0: continue

            for i in range(n_electrons):

                # Find a truth match
                barcode = event.electron_truthMatchedBarcode[i]
                true_i = -1
                for j in range(len(event.truthLepton_barcode)):
                    if event.truthLepton_barcode[j] == barcode:
                        true_i = j
                        break
                if true_i == -1:
                    print "Couldn't find truth match."
                    continue

                # Compare true d0 to different ID tracks
                true_d0 = event.truthLepton_d0[true_i]
                true_pt = event.truthLepton_pt[true_i]
                first_d0 = abs(event.electron_IDtrack_d0[0])

                best_d0_i = 0
                best_pt_i = 0
                for j in range(len(event.electron_IDtrack_d0)):
                    if abs(true_d0 - abs(event.electron_IDtrack_d0[j])) < abs(true_d0 - abs(event.electron_IDtrack_d0[best_d0_i])):
                        #print "\nj=", j
                        #print "diff here:", abs(true_d0 - event.electron_IDtrack_d0[j])
                        #print "old best :", abs(true_d0 - event.electron_IDtrack_d0[best_d0])
                        best_d0_i = j
                    if abs(true_pt - event.electron_IDtrack_pt[j]) < abs(true_pt - event.electron_IDtrack_pt[best_pt_i]): best_pt_i = j

                #print "\n", i
                #print "True d0:", true_d0
                #print "First d0:", first_d0
                #print "Best d0:", event.electron_IDtrack_d0[best_d0], "(index = %d)"%best_d0

                #if best_d0_i != best_pt_i:
                #    print "Found different results for pt and d0. Skipping for now."
                #    continue
                #best_d0 = abs(event.electron_IDtrack_d0[best_d0_i])
                best_d0 = abs(event.electron_IDtrack_d0[best_pt_i])

                hists[sr]["true_d0"].Fill(
                hists[sr]["true_d0_reco"]
                hists[sr]["true_d0_pass"]
                hists[sr]["true_d0_pass_mod"]
'''
