
import glob
import ROOT
import os

# TruthLepton branch
# Go through all the sparticles and save children
# Go through all the reco leptons and save their truth stuff

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBlackBody)
#ROOT.gStyle.SetPalette(ROOT.kSolar)

# Loop over files and make plots
#fname = "/eos/user/l/lhoryn/highd0/111918/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.111918_trees.root/*.root"
#fname = "/eos/user/l/lhoryn/highd0/111918/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.112818_trees.root/*.root"
fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.DAOD_RPVLL_mc16a_031319_trees.root/user.lhoryn.17414676._00000*.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/072319/user.tholmes.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.DAOD_RPVLL_072319_trees.root/*.root"
#fname = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/072319/user.tholmes.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.DAOD_RPVLL_mc16a_072419_v0_trees.root/*.root"
t = getTree(fname)

# Set up histograms
h_parent_type = ROOT.TH1F("h_parent_type", "All Reco Electrons", 10, 0., 10)
h_parent_type_passOR = ROOT.TH1F("h_parent_type_passOR", "Electrons Passing OR", 10, 0., 10)
h_parent_type_passSel = ROOT.TH1F("h_parent_type_passSel", "Selected Electrons (50 GeV)", 10, 0., 10)
h_parent_type_passSel100 = ROOT.TH1F("h_parent_type_passSel100", "Selected Electrons (100 GeV)", 10, 0., 10)

# photons
#pdgids = [11, 13, 22, 130, 211, 310, 321, 2112, 2212, 3112, 3122, 3222, 3312, 3334, 1000010020, 1000010030, "none", "other"]
#parent_pdgids = [11, 15, 22, 24, 111, 221, 223, 411, 421, 431, 443, 511, 521, 531, 4122, 4232, 4332, 5122, 5132, 5232, 100443, "none", "other"]
pdgids = [11, 13, 22, 211, 321, 2212, "none", "other"]
parent_pdgids = [11, 15, 22, 111,  511, 2212, "none", "other"]

# ttbar
#pdgids = [11, 13, 22, 211, 310, 321, 2112, 2212, 3112, 3222, "other", "none"]
#parent_pdgids = [11, 15, 22, 24, 111, 411, 421, 431, 443, 511, 521, 531, 4122, 5122, 5132, 5232, 100443, "other"]
#parent_pdgids = [11, 15, 22, 24, 511, "other", "none"]

h_pdgid = ROOT.TH1F("h_pdgid", "All Reco Electrons", len(pdgids), 0, len(pdgids))
h_pdgid_passOR = ROOT.TH1F("h_pdgid_passOR", "Electrons Passing OR", len(pdgids), 0, len(pdgids))
h_pdgid_passSel = ROOT.TH1F("h_pdgid_passSel", "Selected Electrons (50 GeV)", len(pdgids), 0, len(pdgids))
h_pdgid_passSel100 = ROOT.TH1F("h_pdgid_passSel100", "Selected Electrons (100 GeV)", len(pdgids), 0, len(pdgids))
for i in xrange(len(pdgids)):
    for h in [h_pdgid, h_pdgid_passOR, h_pdgid_passSel, h_pdgid_passSel100]:
        h.GetXaxis().SetBinLabel(i+1, str(pdgids[i]))

# Make dictionary of hists for the different types
type_hists = {}
for i in xrange(8):
    type_hists[i] = { "pt": ROOT.TH1F("h_pt_%s"%i, "Category %s"%i, 60, 0., 600.),
                      "d0": ROOT.TH1F("h_d0_%s"%i, "Category %s"%i, 10, 0., 20.),
                      "d0_pt50": ROOT.TH1F("h_d0_pt50_%s"%i, "Category %s"%i, 10, 0., 20.),
                      "dpt": ROOT.TH1F("h_dpt_%s"%i, "Category %s"%i, 25, -1., 5.),
                      "dpt_pt50": ROOT.TH1F("h_dpt_pt50_%s"%i, "Category %s"%i, 25, -1., 5.),
                      "dpt_pt50_d03": ROOT.TH1F("h_dpt_pt50_d03_%s"%i, "Category %s"%i, 25, -1., 5.)}
pdgid_hists = {}
for i in xrange(len(pdgids)):
    pdgid_hists[pdgids[i]] = { "pt": ROOT.TH1F("h_pdgid_pt_%s"%i, "PdgID %s"%pdgids[i], 30, 0., 600.),
                      "d0": ROOT.TH1F("h_pdgid_d0_%s"%i, "PdgID %s"%pdgids[i], 10, 0., 20.),
                      "d0_pt50": ROOT.TH1F("h_pdgid_d0_pt50_%s"%i, "PdgID %s"%pdgids[i], 10, 0., 20.),
                      "dpt": ROOT.TH1F("h_pdgid_dpt_%s"%i, "PdgID %s"%pdgids[i], 25, -1., 5.),
                      "dpt_pt50": ROOT.TH1F("h_pdgid_dpt_pt50_%s"%i, "PdgID %s"%pdgids[i], 25, -1., 5.),
                      "dpt_pt50_d03": ROOT.TH1F("h_pdgid_dpt_pt50_d03_%s"%i, "PdgID %s"%pdgids[i], 25, -1., 5.)}

parent_pdgid_hists = {}
for i in xrange(len(parent_pdgids)):
    parent_pdgid_hists[parent_pdgids[i]] = { "pt": ROOT.TH1F("h_parent_pdgid_pt_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 30, 0., 600.),
                      "d0": ROOT.TH1F("h_parent_pdgid_d0_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 10, 0., 20.),
                      "d0_pt50": ROOT.TH1F("h_parent_pdgid_d0_pt50_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 10, 0., 20.),
                      "dpt": ROOT.TH1F("h_parent_pdgid_dpt_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 25, -1., 5.),
                      "dpt_pt50": ROOT.TH1F("h_parent_pdgid_dpt_pt50_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 25, -1., 5.),
                      "dpt_pt50_d03": ROOT.TH1F("h_parent_pdgid_dpt_pt50_d03_%s"%i, "Parent PdgID %s"%parent_pdgids[i], 25, -1., 5.)}

parent_pdgid_list = []
pdgid_list = []
current_pdgid = -1 # I'm so sorry.
current_parent_pdgid = -1 # Oh god I did it twice.

def getType(event, el_i):

    # Figure out what kind of truth particle electrons came from
    # 0: Particle not truth matched
    # 1: Particle is truth matched but not an electron
    # 2: Parent is sparticle
    # 3: Parent is prompt particle and Electron is prompt
    # 4: Parent is prompt particle and Electron is not
    # 5: Parent is non-prompt
    # 6: Couldn't find truth match
    # 7: No parent found

    # Find truth particle
    true_barcode = event.electron_truthMatchedBarcode[el_i]
    if true_barcode == -1: return 0

    global current_pdgid
    global current_parent_pdgid
    current_pdgid = -1
    current_parent_pdgid = -1

    for t_i in range(len(event.truthLepton_pt)):
        if true_barcode==event.truthLepton_barcode[t_i]:
            lep_pdgid = event.truthLepton_pdgId[t_i]
            current_pdgid = abs(lep_pdgid)
            if abs(event.truthLepton_pdgId[t_i])!=11:
                #h_pdgid.Fill(pdgids.index(abs(lep_pdgid)))
                if abs(lep_pdgid) not in pdgid_list: pdgid_list.append(abs(lep_pdgid))
                return 1
            current_parent_pdgid = abs(event.truthLepton_parentPdgId[t_i])
            if event.truthLepton_parentPdgId[t_i] == -1: return 7
            if current_parent_pdgid not in parent_pdgid_list: parent_pdgid_list.append(current_parent_pdgid)
            if current_parent_pdgid>1000000: return 2
            if abs(event.truthLepton_parentBarcode[t_i])<200000:
                if true_barcode<200000: return 3
                else: return 4
            else:
                return 5
                if current_parent_pdgid != 22: print "Found category 5 that doesn't come from photon!", current_parent_pdgid

    # Didn't find a match
    return 6

i_event = 0
for event in t:

    if i_event%10000==0: print "Processing event %d..."%i_event
    #if i_event>5000: break

    weight = 1
    if "photon" in fname: weight = event.normweight*140000

    for i in range(event.electron_n):

        parent_type = getType(event, i)

        if current_parent_pdgid == -1: current_parent_pdgid = "none"
        elif current_parent_pdgid not in parent_pdgids: current_parent_pdgid = "other"
        if current_pdgid == -1: current_pdgid = "none"
        elif current_pdgid not in pdgids: current_pdgid = "other"

        h_parent_type.Fill(parent_type, weight)
        if parent_type == 1: h_pdgid.Fill(pdgids.index(current_pdgid), weight)

        if not event.electron_passOR[i]: continue
        h_parent_type_passOR.Fill(parent_type, weight)

        h_pdgid_passOR.Fill(pdgids.index(current_pdgid), weight)

        pdgid_hists[current_pdgid]["pt"].Fill(event.electron_pt[i], weight)
        pdgid_hists[current_pdgid]["d0"].Fill(abs(event.electron_d0[i]), weight)
        pdgid_hists[current_pdgid]["dpt"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)

        parent_pdgid_hists[current_parent_pdgid]["pt"].Fill(event.electron_pt[i], weight)
        parent_pdgid_hists[current_parent_pdgid]["d0"].Fill(abs(event.electron_d0[i]), weight)
        parent_pdgid_hists[current_parent_pdgid]["dpt"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)

        type_hists[parent_type]["pt"].Fill(event.electron_pt[i], weight)
        type_hists[parent_type]["d0"].Fill(abs(event.electron_d0[i]), weight)
        type_hists[parent_type]["dpt"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)

        # Basic selections
        if event.electron_pt[i]<50 or abs(event.electron_eta[i])>2.5: continue
        #h_parent_type_passSel.Fill(parent_type, weight)
        #h_pdgid_passSel.Fill(pdgids.index(current_pdgid), weight)
        pdgid_hists[current_pdgid]["d0_pt50"].Fill(abs(event.electron_d0[i]), weight)
        parent_pdgid_hists[current_parent_pdgid]["d0_pt50"].Fill(abs(event.electron_d0[i]), weight)
        type_hists[parent_type]["d0_pt50"].Fill(abs(event.electron_d0[i]), weight)
        pdgid_hists[current_pdgid]["dpt_pt50"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)
        parent_pdgid_hists[current_parent_pdgid]["dpt_pt50"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)
        type_hists[parent_type]["dpt_pt50"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)

        # d0 selections
        if abs(event.electron_d0[i])<3: continue
        pdgid_hists[current_pdgid]["dpt_pt50_d03"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)
        parent_pdgid_hists[current_parent_pdgid]["dpt_pt50_d03"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)
        type_hists[parent_type]["dpt_pt50_d03"].Fill((event.electron_trackpt[i] - event.electron_pt[i])/event.electron_pt[i], weight)

        #if event.electron_pt[i]<100: continue
        #h_parent_type_passSel100.Fill(parent_type, weight)
        #if parent_type == 1: h_pdgid_passSel100.Fill(pdgids.index(current_pdgid), weight)

    i_event += 1

print sorted(pdgid_list)
print sorted(parent_pdgid_list)
can1 = ROOT.TCanvas("can1", "can1")

can1.SetLogy()
h_pdgid.SetMaximum(1000000000)
h_pdgid.SetMinimum(.1)

h_pdgid.Draw("e PLC PMC")
h_pdgid_passOR.Draw("same e PLC PMC")
h_pdgid_passSel.Draw("same e PLC PMC")
h_pdgid_passSel100.Draw("same e PLC PMC")

leg1 = can1.BuildLegend(.55,.70,.8,.90)
leg1.Draw()

can1.Update()
#raw_input("...")

can = ROOT.TCanvas("can", "can")

can.SetLogy()
h_parent_type.SetMaximum(1000000000)
h_parent_type.SetMinimum(.1)

h_parent_type.Draw("e PLC PMC")
h_parent_type_passOR.Draw("same e PLC PMC")
h_parent_type_passSel.Draw("same e PLC PMC")
h_parent_type_passSel100.Draw("same e PLC PMC")

leg = can.BuildLegend(.55,.70,.8,.90)
leg.Draw()

can.Update()
#raw_input("...")

for dist in type_hists[0]:
    print "Making hist for dist", dist
    can_pt = ROOT.TCanvas("can%s"%dist, "can%s"%dist)
    for i, cat in enumerate(type_hists):
        h = type_hists[cat][dist]
        h.GetXaxis().SetTitle(dist)
        h.SetMaximum(1000000000)
        h.SetMinimum(.1)
        if i==0: h.Draw("e PLC PMC")
        else: h.Draw("same e PLC PMC")
    leg_pt = can_pt.BuildLegend(.55,.70,.8,.90)
    leg_pt.Draw()
    can_pt.SetLogy()
    can_pt.Update()
    #raw_input("...")
    can_pt.SaveAs("cat_%s.pdf"%dist)

for dist in pdgid_hists[pdgids[0]]:
    print "Making hist for dist", dist
    can_dist = ROOT.TCanvas("can1%s"%dist, "can1%s"%dist)
    for i, cat in enumerate(pdgid_hists):
        h = pdgid_hists[cat][dist]
        h.GetXaxis().SetTitle(dist)
        h.SetMaximum(1000000000)
        h.SetMinimum(.1)
        if i==0: h.Draw("e PLC PMC")
        else: h.Draw("same e PLC PMC")
    leg_dist = can_dist.BuildLegend(.55,.70,.8,.90)
    leg_dist.Draw()
    can_dist.SetLogy()
    can_dist.Update()
    #raw_input("...")
    can_dist.SaveAs("pdgid_%s.pdf"%dist)

for dist in parent_pdgid_hists[parent_pdgids[0]]:
    print "Making hist for dist", dist
    can_dist2 = ROOT.TCanvas("can2%s"%dist, "can2%s"%dist)
    for i, cat in enumerate(parent_pdgid_hists):
        h = parent_pdgid_hists[cat][dist]
        h.GetXaxis().SetTitle(dist)
        h.SetMaximum(1000000000)
        h.SetMinimum(.1)
        if i==0: h.Draw("e PLC PMC")
        else: h.Draw("same e PLC PMC")
    leg_dist2 = can_dist2.BuildLegend(.55,.70,.8,.90)
    leg_dist2.Draw()
    can_dist2.SetLogy()
    can_dist2.Update()
    #raw_input("...")
    can_dist2.SaveAs("parent_pdgid_%s.pdf"%dist)

#for cat in type_hists:
#    for hname in type_hists[cat]:
#        h = type_hists[cat][hname]

#        can = ROOT.TCanvas("can_%s_%s"%(hname, cat), "can_%s_%s"%(hname, cat))
#        h.Draw()
#        can.SaveAs(h.GetName()+".pdf")



