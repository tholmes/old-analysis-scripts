# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        "data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/user.lhoryn.data17_13TeV.periodI.physics_Main.DAOD_RPVLL_041619_trees.root/user.lhoryn.17749537._000*.root"},
        }

# Define selections
event_selection = "electron_n == 2 && muon_n==0 && triggerRegion_pass"

e_signal = Selection("electron_signal")
e_signal.addCut("electron_pt", 65)
#e_signal.addCut("electron_eta", -2.5, 2.5)
e_signal.addCut("electron_d0", 3., abs_val=True)
#e_signal.addCut("electron_dpt", -.5)

e_prompt = Selection("electron_prompt")
e_prompt.addCut("electron_pt", 65)
#e_prompt.addCut("electron_eta", -2.5, 2.5)
e_prompt.addCut("electron_d0", maxval = 0.1, abs_val=True)
#e_prompt.addCut("electron_dpt", -.5)

#e_pass = Selection("electron_pass")
#e_pass.addCut("electron_dpt", -.5)
# For the future, add isolation? Other quality checks?

# Loop over samples
for s in samples:

    # Read in data
    files = glob.glob(samples[s]["fname"])
    print "Found", len(files), "files."
    samples[s]["tree"] = getTree(samples[s]["fname"])
    samples[s]["initial_rdf"] = ROOT.ROOT.RDataFrame(samples[s]["tree"])

    # Make basic event selection
    df = samples[s]["initial_rdf"].Filter(event_selection, "ee selection")
    print "ee selection:", event_selection
    print "prompt selection:", e_prompt.getCutString(0)
    print "displaced selection:", e_signal.getCutString(0)

    # Define needed variables
    df_dpt = df.Define("electron_dpt", "(electron_trackpt - electron_pt)/electron_pt")
    df_dpts = df_dpt.Define("sig_e_dpt", "if (electron_d0[0]>3) return electron_dpt[0]; else return electron_dpt[1];").Define("prompt_e_dpt", "if (electron_d0[0]>3) return electron_dpt[1]; else return electron_dpt[0];")

    # Define CR
    cr_str = "(%s && %s) || (%s && %s)"%(e_prompt.getCutString(0), e_signal.getCutString(1), e_prompt.getCutString(1), e_signal.getCutString(0))
    print "CR string:", cr_str
    df_cr = df_dpts.Filter(cr_str, "CR")

    # Make histograms
    samples[s]["hists"] = {}
    model = ROOT.RDF.TH1DModel("m_dpt", "Signal Electron", 25, -1., 5.)
    samples[s]["hists"]["signal_dpt"] = df_cr.Histo1D(model, "sig_e_dpt")
    model = ROOT.RDF.TH1DModel("m_dpt", "Prompt Electron", 25, -1., 5.)
    samples[s]["hists"]["prompt_dpt"] = df_cr.Histo1D(model, "prompt_e_dpt")

    samples[s]["hists"]["signal_dpt"].SetLineColor(ROOT.kBlue-4)
    samples[s]["hists"]["prompt_dpt"].SetLineColor(ROOT.kRed-4)
    samples[s]["hists"]["signal_dpt"].SetMarkerColor(ROOT.kBlue-4)
    samples[s]["hists"]["prompt_dpt"].SetMarkerColor(ROOT.kRed-4)
    samples[s]["hists"]["signal_dpt"].GetXaxis().SetTitle("(p_{T} - p^{track}_{T}) / p_{T}")

    can = ROOT.TCanvas("can", "can")
    samples[s]["hists"]["signal_dpt"].Draw("ehist")
    samples[s]["hists"]["prompt_dpt"].Draw("ehist same")
    leg = can.BuildLegend(.55,.70,.8,.90)
    leg.Draw()
    raw_input("...")
    can.SaveAs("plots/sig_v_prompt_dpt.png")


    # Cutflow for fun
    print ""
    report = samples[s]["initial_rdf"].Report()
    report.Print()

