# Goal: Make plots of photon-related electron backgrounds compared to signal

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

variables = {
        "pt":       {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "Electron p_{T} [GeV]"},
        "d0":       {"attr": "abs(OBJ_d0[0])",       "nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "Electron d_{0} [mm]"},
        "id_pt":    {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 200.,     "xlabel": "Electron ID Track p_{T} [GeV]"},
        "qop_err":  {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": 0.,     "xmax": 0.0001,   "xlabel": "Electron q/p Error [1/MeV]"},
        "rel_qop_err":  {"attr": "OBJ_pt[0]",        "nbins": 40,    "xmin": 0.,     "xmax": 5.,        "xlabel": "Electron q/p Error / q/p"},
        "rel_pt_err":  {"attr": "OBJ_pt[0]",        "nbins": 40,    "xmin": 0.,     "xmax": 5.,        "xlabel": "Electron p_{T} Error / p_{T}"},
        "sig":      {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -10.,     "xmax": 10.,       "xlabel": "(p_{T} - p_{T}^{track}) / p_{T} Error)"},
        "dpt":      {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 4.,      "xlabel": "(p_{T} - p_{T}^{track}) / p_{T}"},
        "ptcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptcone20"},
        "ptcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptcone30"},
        "ptcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptcone40"},
        "ptvarcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptvarcone20"},
        "ptvarcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptvarcone30"},
        "ptvarcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "ptvarcone40"},
        "topoetcone20": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "topoetcone20"},
        "topoetcone30": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "topoetcone30"},
        "topoetcone40": {"attr": "OBJ_pt[0]",            "nbins": 40,    "xmin": -2.,     "xmax": 100.,      "xlabel": "topoetcone40"},
        }


snames = {
        "361044":           "Photons, 70 < p_{T} < 140",
        "SlepSlep*100*0p1": "Sleptons, m = 100, #tau=0.1 ns",
        "StauStau*100*0p1": "Staus, m = 100, #tau=0.1 ns",
        "StauStau*300*0p1": "Staus, m = 300, #tau=0.1 ns",
        "SlepSlep*300*1ns": "Sleptons, m = 300, #tau=1 ns",
        "SlepSlep*500*0p1": "Sleptons, m = 500, #tau=0.1 ns",
        }

def getVarHist(df, var, varname, sample_label):
    h = df.Histo1D((sample_label+varname, snames[sample_label], variables[var]["nbins"], variables[var]["xmin"], variables[var]["xmax"]), varname)
    h.GetXaxis().SetTitle(variables[var]["xlabel"])
    h.GetYaxis().SetTitle("Events")
    return h

def getPtErr():

    #s(pt)/pt = pt**2 * (s(qop)/qop)**2
    return 0

# Settings
lumi = 140000
f_string = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"

# Collect samples and weights
samples = {}
for samplename in signals+photons:
    # Get filename
    base_name = ""  # To be used for getting xs
    file_names = [] # To be used for collecting all samples
    for d in [signalstau_d, signal_d, photon_d]:
        sets = glob.glob("%s/*%s*"%(d,samplename))
        if len(sets)>=1:
            base_name = sets[0].split("/")[-1]
            file_names = sets
            print "Found", len(file_names), "samples(s):\n\t", file_names
            break
    if base_name == "":
        print "Couldn't find file for sample", samplename
        exit()

    # Normalize
    xs = getXS(base_name.split(".")[3])
    n_events = 0
    for f in file_names:
        n_events += getNEvents(f+"/*.root")
    event_weight = lumi*xs/n_events

    # Add to tree
    is_signal = False
    if samplename in signals: is_signal = True
    samples[samplename] = {"files": file_names, "weight": event_weight, "hists": {}, "is_signal": is_signal}

# Define selection
append = "pt65_d03"

e_sel = Selection()
e_sel.addCut("electron_pt", 65)
e_sel.addCut("electron_passOR")
e_sel.addCut("abs(electron_d0)", 3)
e_def = e_sel.getCutString()
print "\nSelection for an electron:", e_def

for s in samples:

    print "\nWorking with sample", s

    # Get dataframe
    t = ROOT.TChain("trees_SR_highd0_")
    for f in samples[s]["files"]: t.Add(f+"/*.root")
    dframe = ROOT.ROOT.RDataFrame(t)

    # Do basic event skimming
    e_selection = e_sel.nSelectedString("electron_pt")
    df_edef = dframe.Define("e_n", e_selection)
    df_ecut = df_edef.Filter("e_n>0", "At least one selected electron")
    #df_ecut = df_edef.Filter("e_n>1", "At least two selected electrons")

    # Apply selection to variables
    e_sel_pt = e_sel.selectedVectorString("electron_pt")
    e_sel_d0 = e_sel.selectedVectorString("electron_d0")
    e_sel_qop_err = e_sel.selectedVectorString("electron_qop_err")
    e_sel_qop = e_sel.selectedVectorString("electron_qop")
    e_sel_ptcone20 = e_sel.selectedVectorString("electron_ptcone20")
    e_sel_ptcone30 = e_sel.selectedVectorString("electron_ptcone30")
    e_sel_ptcone40 = e_sel.selectedVectorString("electron_ptcone40")
    e_sel_ptvarcone20 = e_sel.selectedVectorString("electron_ptvarcone20")
    e_sel_ptvarcone30 = e_sel.selectedVectorString("electron_ptvarcone30")
    e_sel_ptvarcone40 = e_sel.selectedVectorString("electron_ptvarcone40")
    e_sel_topoetcone20 = e_sel.selectedVectorString("electron_topoetcone20")
    e_sel_topoetcone30 = e_sel.selectedVectorString("electron_topoetcone30")
    e_sel_topoetcone40 = e_sel.selectedVectorString("electron_topoetcone40")

    #e_sel_id_pt = e_sel.selectedVectorString("electron_IDtrack_pt")
    # This one is funky
    e_sel_id_pt = '''
            auto v = electron_IDtrack_pt;
            v.clear();
            for (int i=0; i<electron_pt.size(); i++)
                if (%s)
                    v.push_back((double)electron_IDtrack_pt[electron_IDtrack_electronindex[i]]);
            return v;'''%e_sel.getCutString("i")

    df_ept = df_ecut.Define("e_pt", e_sel_pt).Define("e_d0", e_sel_d0).Define("e_id_pt", e_sel_id_pt).Define("e_qop_err", e_sel_qop_err).Define("e_qop", e_sel_qop)
    ef_iso = df_ept.Define("e_ptcone20", e_sel_ptcone20).Define("e_ptcone30", e_sel_ptcone30).Define("e_ptcone40", e_sel_ptcone40).Define("e_ptvarcone20", e_sel_ptvarcone20).Define("e_ptvarcone30", e_sel_ptvarcone30).Define("e_ptvarcone40", e_sel_ptvarcone40).Define("e_topoetcone20", e_sel_topoetcone20).Define("e_topoetcone30", e_sel_topoetcone30).Define("e_topoetcone40", e_sel_topoetcone40)

    # Make combo variables
    df_vars = df_iso.Define("e_dpt", "(e_id_pt - e_pt)/e_pt")       # Relative difference btw electron pT and track pT
    df_rqop = df_vars.Define("e_rel_qop_err", "e_qop_err/e_qop")    # Relative qop uncertainty
    df_pterr = df_rqop.Define("e_rel_pt_err", "e_pt*e_pt * e_rel_qop_err*e_rel_qop_err")  # Relative pT uncertainty
    df_sig = df_pterr.Define("e_sig", "e_dpt/(e_pt*e_rel_pt_err)")       # Significance of difference btw electron pT and track pT

    # Make plots of basic variables
    df = df_sig
    samples[s]["hists"]["e_pt"] = getVarHist(df, "pt", "e_pt", s)
    samples[s]["hists"]["e_d0"] = getVarHist(df, "d0", "e_d0", s)
    samples[s]["hists"]["e_id_pt"] = getVarHist(df, "id_pt", "e_id_pt", s)
    samples[s]["hists"]["e_qop_err"] = getVarHist(df, "qop_err", "e_qop_err", s)
    samples[s]["hists"]["e_rel_qop_err"] = getVarHist(df, "rel_qop_err", "e_rel_qop_err", s)
    samples[s]["hists"]["e_dpt"] = getVarHist(df, "dpt", "e_dpt", s)
    samples[s]["hists"]["e_rel_pt_err"] = getVarHist(df, "rel_pt_err", "e_rel_pt_err", s)
    samples[s]["hists"]["e_sig"] = getVarHist(df, "sig", "e_sig", s)
    samples[s]["hists"]["e_ptcone20"] = getVarHist(df, "ptcone20", "e_ptcone20", s)
    samples[s]["hists"]["e_ptcone30"] = getVarHist(df, "ptcone30", "e_ptcone30", s)
    samples[s]["hists"]["e_ptcone40"] = getVarHist(df, "ptcone40", "e_ptcone40", s)
    samples[s]["hists"]["e_ptvarcone20"] = getVarHist(df, "ptvarcone20", "e_ptvarcone20", s)
    samples[s]["hists"]["e_ptvarcone30"] = getVarHist(df, "ptvarcone30", "e_ptvarcone30", s)
    samples[s]["hists"]["e_ptvarcone40"] = getVarHist(df, "ptvarcone40", "e_ptvarcone40", s)
    samples[s]["hists"]["e_topoetcone20"] = getVarHist(df, "topoetcone20", "e_topoetcone20", s)
    samples[s]["hists"]["e_topoetcone30"] = getVarHist(df, "topoetcone30", "e_topoetcone30", s)
    samples[s]["hists"]["e_topoetcone40"] = getVarHist(df, "topoetcone40", "e_topoetcone40", s)
    #h.Draw()
    #raw_input("...")

    # Cutflow for fun
    report = dframe.Report()
    report.Print()

# Collect hists
hists = samples[s]["hists"].keys()
print hists
cans = {}
for htype in hists:
    cans[htype] = ROOT.TCanvas(htype, htype)

    y_max = 0
    for s in samples:
        y_max = max(y_max, samples[s]["hists"][htype].GetMaximum())
        if samples[s]["is_signal"]:
            samples[s]["hists"][htype].SetMarkerStyle(4)

    for i, s in enumerate(samples):
        if i==0:
            samples[s]["hists"][htype].SetMaximum(100*y_max)
            samples[s]["hists"][htype].Draw("e PLC PMC")
        else: samples[s]["hists"][htype].Draw("same e PLC PMC")

    leg = cans[htype].BuildLegend(.5,.65,.8,.90)
    leg.Draw()

    cans[htype].SetLogy()

    ROOT.ATLASLabel(0.2,0.85, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.SetTextSize(0.04)
    text.DrawLatex(0.2,0.78, "Simulation, 140 fb^{-1}")

    #raw_input("...")
    cans[htype].SaveAs("plots/%s_%s.pdf"%(htype, append))

exit()
