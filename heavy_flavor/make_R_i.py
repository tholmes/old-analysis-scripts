import glob
import ROOT
import os
import math
import argparse
import sys
from ROOT import *

ROOT.gROOT.LoadMacro("/afs/cern.ch/work/t/tholmes/FTK/FTK_user/scripts/python/atlasstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("/afs/cern.ch/work/t/tholmes/FTK/FTK_user/scripts/python/atlasstyle/AtlasLabels.C");
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(0)


#muon object definition
def preselect_muon(event, i, tight=0):

    if event.muon_isGoodQual[i] == 0: return False
    if event.muon_isPassIDtrk[i] == 0: return False
    if event.muon_isNotCosmic[i] == 0: return False

    pt = event.muon_pt[i]
    d0 = event.muon_IDtrack_d0[i]

    if tight==1:
        if abs( d0 ) <= 0.5: return False
        if abs( pt ) <= 50 : return False

    if tight==2:
        if abs( d0 ) <= 3: return False

    return True


#make R_i plot
files = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/bphys.root")
tb = ROOT.TChain("trees_SR_highd0_")
for f in files: tb.Add(f)

cut = []
for i in range(0,13):
    cut.append(0.01*i)

r = []
m = []
e = []
er = []

for c in cut:

    ni = 0.0
    n = 0.0
    ne = 0.0

    for event in tb:

        e1 = 0

        for i in xrange(len(event.muon_pt)):

            if not(preselect_muon(event,i,tight=0)): continue

            o = event.muon_truthOrigin[i]
            if o >20 and event.muon_pt[i] >65 and abs(event.muon_IDtrack_d0[i]) > c:

                e1 = e1 +1
                n = n +1
                if event.muon_FCTightTTVA[i]==1: ni = ni +1

        if e1 != 0: ne = ne +1

    r1 = float(ni/n)
    er1 = float(r1*(((ni)**0.5/ni)**2+((n)**0.5/n)**2)**0.5)
    r.append(r1)
    m.append(n)
    e.append(ne)
    er.append(er1)

print cut
print r
print m
print e
print er

h = ROOT.TGraphErrors(13)
for i in range(0,13):
    h.SetPoint(i,cut[i],r[i])
    h.SetPointError(i,0.0,er[i])
h.SetTitle("ttbar; cut on d0 [mm]; Isolated/Total ratio")
h.SetMarkerStyle(kCircle)
h.SetMaximum(0.9)
h.SetMinimum(0.0)
can = ROOT.TCanvas()
h.Draw("PELA")
ROOT.ATLASLabel(0.20,0.88, "Internal")
can.SaveAs("make_R_i.png")
