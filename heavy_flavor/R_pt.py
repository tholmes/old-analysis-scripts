#this calculates the ratio to extrapolate from the lowered to the regular pt cuts (R_pt)
nl = 0.0
nr = 0.0

for event in tb:

    for i in xrange(len(event.muon_pt)):

        if not(preselect_muon(event,i,tight=0)): continue

        o = event.muon_truthOrigin[i]
        if o >20:
            if abs(event.muon_pt[i]) > 50: nl = nl +1
            if abs(event.muon_pt[i]) > 65: nr = nr +1

R_pt = double(nr/nl)
dR_pt = R_pt*(nl-nr)/nl*(1/nr+1/nl)**0.5
print ("~~R_pt = " + str(nr) + "/" + str(nl) + " = " + str(R_pt))
print ("~~dR_pt = " + str(dR_pt))
