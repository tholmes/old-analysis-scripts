#this calculates the number of anti-isolated 2-muon events with lowered event selections in data (n_anti))
n_anti = 0.0

for event in t:

    if not (len(event.muon_pt)==2 and (event.pass_HLT_mu60_0eta105_msonly)==1): continue
    if ((event.muon_FCTightTTVA[0])==1 and (event.muon_FCTightTTVA[1])==1): continue
    if not (preselect_muon(event, 0, tight=0) and preselect_muon(event, 1, tight=0)): continue
    if (abs(event.muon_IDtrack_d0[0])< 1.5 and abs(event.muon_IDtrack_d0[1])< 1.5): continue
    if not ((preselect_muon(event, 0, tight=1) and preselect_muon(event, 1, tight=1)) and (preselect_muon(event, 0, tight=2) or preselect_muon(event, 1, tight=2))): continue
    n_anti  = n_anti+1

dn_anti = (n_anti)**0.5
print n_anti
print ("~~dn_anti = " + str(dn_anti))
