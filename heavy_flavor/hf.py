import glob
import ROOT
import os
import math
import argparse
import sys
import random
from ROOT import *


#data n-tuples
files = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.3_lowd0/data_wip/merged/*.root")
t = ROOT.TChain("trees_SR_highd0_")
for f in files: t.Add(f)
#bbmumu n-tuples
files = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/bphys.root")
tb = ROOT.TChain("trees_SR_highd0_")
for f in files: tb.Add(f)
#ttbar n-tuples
#files = glob.glob("/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/ttbar.root")
#tt = ROOT.TChain("trees_SR_highd0_")
#for f in files: tt.Add(f)


#muon object definition
def preselect_muon(event, i, tight=0):

    if event.muon_isGoodQual[i] == 0: return False
    if event.muon_isPassIDtrk[i] == 0: return False
    if event.muon_isNotCosmic[i] == 0: return False

    pt = event.muon_pt[i]
    d0 = event.muon_IDtrack_d0[i]

    if tight==1:
        if abs( d0 ) <= 0.5: return False
        if abs( pt ) <= 50 : return False

    if tight==2:
        if abs( d0 ) <= 3: return False

    return True


#intermediate steps: R_i and dR_i do not come from this code directly, their origins are explained below
execfile("R_pt.py")
execfile("R_d0.py")
R_c = (R_pt)**2*R_d0
dR_c = R_c*((dR_d0/R_d0)**2+4*(dR_pt/R_pt)**2)**0.5
print ("R_c = " + str(R_c))
print ("dR_c = " + str(dR_c))
##R_i and dR_i are estimated by looking at the plot in the picture R_i.png in this folder, this plot could also be reproduced using the code make_R_i.py in this folder
R_i = 0.58
dR_i = 0.12
print ("R_i = " + str(R_i))
print ("dR_i = " + str(dR_i))
execfile("r.py")
execfile("n_anti.py")


#final result: the equations of using the above numbers to calculate the final result are shown in the picture equations.png in this folder
n_sr = (n_anti*R_c*R_i**2*r)/(1-R_i**2*r)
ksi = (1-R_i**2*r)**(-2)
dn_sr = n_sr*((dn_anti/n_anti)**2+(dR_c/R_c)**2+4*ksi*(dR_i/R_i)**2+ksi*(dr/r)**2)**0.5
print ("2-muon  hf background is  " + str(n_sr))
print ("it's uncertainty is  +/-" + str(dn_sr))
