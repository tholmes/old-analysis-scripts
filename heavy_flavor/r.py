#this calculates the correlation factor of the isolation between the two muons from the same event (r)
hpp = 0.0
hpf = 0.0
hfp = 0.0
hff = 0.0

for event in tb:

    if len(event.muon_pt)!=2: continue
    if not (preselect_muon(event, 0, tight=0) and preselect_muon(event, 1, tight=0)): continue

    ran = random.randint(0,1)
    ran2 = 1-ran

    if (event.muon_FCTightTTVA[ran])==1 and (event.muon_FCTightTTVA[ran2])==1: hpp = hpp+1
    if (event.muon_FCTightTTVA[ran])==1 and (event.muon_FCTightTTVA[ran2])==0: hpf = hpf+1
    if (event.muon_FCTightTTVA[ran])==0 and (event.muon_FCTightTTVA[ran2])==1: hfp = hfp+1
    if (event.muon_FCTightTTVA[ran])==0 and (event.muon_FCTightTTVA[ran2])==0: hff = hff+1

r_nu = hpp/(hpp+hpf)
dr_nu = r_nu*hpf/(hpp+hpf)*(1/hpp+1/hpf)**0.5
r_de = (hpp+hfp)/(hpp+hpf+hfp+hff)
dr_de = r_de*(hpf+hff)/(hpp+hpf+hfp+hff)*(1/(hpp+hfp)+1/(hpf+hff))**0.5
r = r_nu/r_de
dr = r*((dr_nu/r_nu)**2+(dr_de/r_de)**2)**0.5

print hpp
print hpf
print hfp
print hff

print ("r = " + str(r))
print ("dr = " + str(dr))
