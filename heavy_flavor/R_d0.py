#this calculates the ratio to extrapolate from the lowered to the regular d0 cuts (R_d0)
nl = 0.0
nr = 0.0

for event in tb:

    for i in xrange(len(event.muon_pt)):

        if not(preselect_muon(event,i,tight=0)): continue

        o = event.muon_truthOrigin[i]
        if o >20:
            if abs(event.muon_IDtrack_d0[i]) > 0.5: nl = nl +1
            if abs(event.muon_IDtrack_d0[i]) > 3: nr = nr +1

R_d0 = double(nr/nl)
dR_d0 = R_d0*(nl-nr)/nl*(1/nr+1/nl)**0.5
print ("~~R_d0 = " + str(nr) + "/" + str(nl) + " = " + str(R_d0))
print ("~~dR_d0 = " + str(dR_d0))
