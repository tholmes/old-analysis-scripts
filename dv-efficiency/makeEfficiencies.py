# 1. plot d0 vs # reco (maybe do this with a graph?)
import ROOT
#import numpy
from array import array
from math import log
#import tabulate
#from beautifultable import BeautifulTable
ROOT.gROOT.LoadMacro("../../plotting/atlasstyle/AtlasStyle.C")



def getSF(pt0, pt1, mass, lifetime, lepton):

	sf_dv = ROOT.TFile("outFiles/SF_lqd.root")
	sf_stau = ROOT.TFile("outFiles/SF_stau.root")

	h_dv = sf_dv.Get("lifetime_"+lifetime+"_"+lepton)
	h_stau = sf_stau.Get("mass_"+mass)

	binx_dv = h_dv.GetXaxis().FindBin(pt0)
	biny_dv = h_dv.GetYaxis().FindBin(pt1)
	weight_dv = h_dv.GetBinContent(binx_dv, biny_dv)
	#print "dv bin content " + str(weight_dv)

	binx_stau = h_stau.GetXaxis().FindBin(pt0)
	biny_stau = h_stau.GetYaxis().FindBin(pt1)
	weight_stau = h_stau.GetBinContent(binx_stau, biny_stau)
	#print "stau bin content " + str(weight_stau)

	weight = weight_stau/weight_dv
	#	print "check me out baby " + str(weight) + "with pt 0 " + str(pt0) + " and pt1 " + str(pt1)
	
	return weight

#def fillD0(d0val, eventCounter, d0Vals):
#	found = False
#	for i in range(1, len(eventCounter)):
#		if d0val > d0Vals[i-1] and d0val < d0Vals[i]:
#			eventCounter[i] +=1
	
	



stau_masses = ["80","100","120","140"]
lifetimes = ["10","100","1000"]
#lifetimes = ["10"]

PTCUT = 60
ETACUT = 2.5
GOODTRUE = 200000
D0CUT = 3

d0Range={"10":100,"100":350,"1000":350}
d0Bins={"10":5,"100":20,"1000":20}

binning={"10":[0,1,2,3,4,5,6,7,8,9,10,12,15,20,50], "100":[0,1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,100,200,300,400], "1000":[0,1,2,3,4,5,6,7,8,9,10,12,15,20,30,40,50,100,200,300,400,500]}

lumi = 100000 #100fb^-1 in pb^-1

#table = BeautifulTable()
#table.column_headers = ["lifetime", "mass", "# Reco Events"]

for lifetime in lifetimes:
	for mass in stau_masses:

 		if mass != "100": continue

		print "lifetime " + lifetime
		print "mass " + mass

		dv_file = ROOT.TFile("../../Analysis/lqd_allfiles/output/ct"+lifetime+".root")
		stau_file = ROOT.TFile("../../Analysis/stau_allfiles/output/mass"+mass+".root")
		dv_tree = dv_file.Get("trees_SRDV_")
		stau_tree = stau_file.Get("trees_SRDV_")
		
		
		#d0Vals = numpy.logspace(-1,2,20)
		#leading_reco_muon = numpy.zeros(20)
		#leading_true_muon = numpy.zeros(20)
		#subleading_true_muon = numpy.zeros(20)
		#subleading_reco_muon = numpy.zeros(20)
		
		
		cutflow = ROOT.TH1F("cutflow", "cutflow", 20, 0, 20)
		cutflow_stau = ROOT.TH1F("cutflow_stau", "cutflow_stau", 20, 0, 20)
		h_nReco_true_lead_elec_noSF = ROOT.TH1F("noSF_nEvents_trued0_lead_elec","noSF_nEvents_trued0_lead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_lead_muon_noSF = ROOT.TH1F("noSF_nEvents_trued0_lead_muon", "noSF_nEvents_trued0_lead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_lead_elec_noSF = ROOT.TH1F("noSF_nEvents_recod0_lead_elec","noSF_nEvents_recod0_lead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_lead_muon_noSF = ROOT.TH1F("noSF_nEvents_recod0_lead_muon","noSF_nEvents_recod0_lead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_lead_elec = ROOT.TH1F("nEvents_trued0_lead_elec","nEvents_trued0_lead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_lead_elec = ROOT.TH1F("nEvents_recod0_lead_elec","nEvents_recod0_lead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_lead_muon = ROOT.TH1F("nEvents_trued0_lead_muon","nEvents_trued0_lead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_lead_muon = ROOT.TH1F("nEvents_recod0_lead_muon","nEvents_recod0_lead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))

		h_nReco_true_sublead_elec_noSF = ROOT.TH1F("noSF_nEvents_trued0_sublead_elec","noSF_nEvents_trued0_sublead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_sublead_elec_noSF = ROOT.TH1F("noSF_nEvents_recod0_sublead_elec","noSF_nEvents_recod0_sublead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_sublead_muon_noSF = ROOT.TH1F("noSF_nEvents_trued0_sublead_muon", "noSF_nEvents_trued0_sublead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_sublead_muon_noSF = ROOT.TH1F("noSF_nEvents_recod0_sublead_muon","noSF_nEvents_recod0_sublead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_sublead_elec = ROOT.TH1F("nEvents_trued0_sublead_elec","nEvents_trued0_sublead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_sublead_elec = ROOT.TH1F("nEvents_recod0_sublead_elec","nEvents_recod0_sublead_elec", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_true_sublead_muon = ROOT.TH1F("nEvents_trued0_sublead_muon","nEvents_trued0_sublead_muon", len(binning[lifetime])-1, array("d",binning[lifetime]))
		h_nReco_reco_sublead_muon = ROOT.TH1F("nEvents_recod0_sublead_muon","nEvents_recod0_sublead_muon", len(binning[lifetime])-1, array("d",binning[lifetime])) 
		

		#h_Efficiency_true_elec_noSF = ROOT.TH1F("noSF_Efficiency_trued0_elec","noSF_Efficiency_trued0_elec", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_reco_elec_noSF = ROOT.TH1F("noSF_Efficiency_recod0_elec","noSF_Efficiency_recod0_elec", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_true_muon_noSF = ROOT.TH1F("noSF_Efficiency_trued0_muon", "noSF_Efficiency_trued0_muon", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_reco_muon_noSF = ROOT.TH1F("noSF_Efficiency_recod0_muon","noSF_Efficiency_recod0_muon", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_true_elec = ROOT.TH1F("Efficiency_trued0_elec","Efficiency_trued0_elec", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_reco_elec = ROOT.TH1F("Efficiency_recod0_elec","Efficiency_recod0_elec", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_true_muon = ROOT.TH1F("Efficiency_trued0_muon","Efficiency_trued0_muon", d0Range[lifetime], 0, d0Range[lifetime])
		#h_Efficiency_reco_muon = ROOT.TH1F("Efficiency_recod0_muon","Efficiency_recod0_muon", d0Range[lifetime], 0, d0Range[lifetime])
		
		pt_dv_sf = ROOT.TH1F("pt_dv_sf", "pt_dv_sf", 10, 0, 500)
		pt_dv = ROOT.TH1F("pt_dv", "pt_dv", 10, 0, 500) 
		pt_stau = ROOT.TH1F("pt_stau", "pt_stau", 10, 0, 500)
		
		h_nReco_reco_lead_muon_noSF.SetDirectory(0)
		h_nReco_true_lead_muon_noSF.SetDirectory(0)
		h_nReco_reco_lead_elec_noSF.SetDirectory(0)
		h_nReco_true_lead_elec_noSF.SetDirectory(0)
		h_nReco_reco_lead_muon.SetDirectory(0)
		h_nReco_true_lead_muon.SetDirectory(0)
		h_nReco_reco_lead_elec.SetDirectory(0)
		h_nReco_true_lead_elec.SetDirectory(0)
		h_nReco_reco_sublead_muon_noSF.SetDirectory(0)
		h_nReco_true_sublead_muon_noSF.SetDirectory(0)
		h_nReco_reco_sublead_elec_noSF.SetDirectory(0)
		h_nReco_true_sublead_elec_noSF.SetDirectory(0)
		h_nReco_reco_sublead_muon.SetDirectory(0)
		h_nReco_true_sublead_muon.SetDirectory(0)
		h_nReco_reco_sublead_elec.SetDirectory(0)
		h_nReco_true_sublead_elec.SetDirectory(0)
		pt_dv_sf.SetDirectory(0)
		pt_dv.SetDirectory(0)
		pt_stau.SetDirectory(0)
		cutflow.SetDirectory(0)
		cutflow_stau.SetDirectory(0)
	
		h_nReco_reco_lead_muon_noSF.Sumw2()
		h_nReco_true_lead_muon_noSF.Sumw2()
		h_nReco_reco_lead_elec_noSF.Sumw2()
		h_nReco_true_lead_elec_noSF.Sumw2()
		h_nReco_reco_lead_muon.Sumw2()
		h_nReco_true_lead_muon.Sumw2()
		h_nReco_reco_lead_elec.Sumw2()
		h_nReco_true_lead_elec.Sumw2()
		h_nReco_reco_sublead_muon_noSF.Sumw2()
		h_nReco_true_sublead_muon_noSF.Sumw2()
		h_nReco_reco_sublead_elec_noSF.Sumw2()
		h_nReco_true_sublead_elec_noSF.Sumw2()
		h_nReco_reco_sublead_muon.Sumw2()
		h_nReco_true_sublead_muon.Sumw2()
		h_nReco_reco_sublead_elec.Sumw2()
		h_nReco_true_sublead_elec.Sumw2()
		pt_dv_sf.Sumw2()
		pt_dv.Sumw2()
		pt_stau.Sumw2()
		#h_Efficiency_reco_muon_noSF.SetDirectory(0)
		#h_Efficiency_true_muon_noSF.SetDirectory(0)
		#h_Efficiency_reco_elec_noSF.SetDirectory(0)
		#h_Efficiency_true_elec_noSF.SetDirectory(0)
		#h_Efficiency_reco_muon.SetDirectory(0)
		#h_Efficiency_true_muon.SetDirectory(0)
		#h_Efficiency_reco_elec.SetDirectory(0)
		#h_Efficiency_true_elec.SetDirectory(0)
		
					
		hists=[]
		final_hists=[]

		for event in stau_tree:
			cutflow_stau.Fill("total",1)
			if len(event.truthTauD0) >=2:
				cutflow_stau.Fill("2 taus",1)
				if event.truthTauPt[0] > PTCUT:
					cutflow_stau.Fill("leading pt",1)
					if event.truthTauPt[1] > PTCUT:
						 cutflow_stau.Fill("subleading pt",1)
						 if abs(event.truthaTauEta[0]) < ETACUT:
							  cutflow_stau.Fill("leading eta",1)
							  if abs(event.truthaTauEta[1]) < ETACUT:
								   cutflow_stau.Fill("subleading eta", 1)
								   if event.truthTauBarcode[0] < GOODTRUE:
									   cutflow_stau.Fill("leading barcode", 1) 
									   if event.truthTauBarcode[1] < GOODTRUE:
										   cutflow_stau.Fill("subleading barcode",1)
										   pt_stau.Fill(event.truthTauPt[0], event.normweight*event.pileupWeight*lumi)

##############################################################################
			
		for event in dv_tree:
			cutflow.Fill("total",1)
			#fiducial cuts
			if len(event.truthMuonPt) >= 2:
				#cutflow.Fill("F:2 true muons",1)
				if event.truthMuonPt[0] > PTCUT:
					#cutflow.Fill("F:true leading muon PT cut", 1)
					if event.truthMuonPt[1] > PTCUT:
						#cutflow.Fill("F:true subleading muon PT cut", 1)
						if abs(event.truthMuonEta[0]) < ETACUT:
							# cutflow.Fill("F:true leading muon eta cut",1) 
							 if abs(event.truthMuonEta[1]) < ETACUT:
								# cutflow.Fill("F:true subleading muon eta cut",1)
								 if event.truthMuonBarcode[0] < GOODTRUE:
									# cutflow.Fill("F:true leading muon barcode",1) 
									 if event.truthMuonBarcode[1] < GOODTRUE:
										#cutflow.Fill("F:true subleading muon barcode",1)
										
									 	SF_mu = getSF(event.truthMuonPt[0], event.truthMuonPt[1], mass, lifetime, "muon")

										cutflow.Fill("fiducial",SF_mu*lumi)	
										
										h_nReco_true_lead_muon_noSF.Fill(event.truthMuonD0[0], lumi)
										h_nReco_true_sublead_muon_noSF.Fill(event.truthMuonD0[1], lumi)
										h_nReco_true_lead_muon.Fill(event.truthMuonD0[0], SF_mu*lumi)
										h_nReco_true_sublead_muon.Fill(event.truthMuonD0[1], SF_mu*lumi)

										pt_dv.Fill(event.truthMuonPt[0])
										pt_dv_sf.Fill(event.truthMuonPt[0],SF_mu*lumi)

										
										nMatched_muon = 0
										for index in event.muon_RecoTrueMatchIndices:
											if index >= 0:
												nMatched_muon += 1
										nBarcode = 0
										'''		
										if(nMatched_muon != 2):
											print "#################################"
											print "true barcodes"
											for it in range(len(event.truthMuonBarcode)):
												print "true particle ", it
												print "true barcode ", event.truthMuonBarcode[it]
												print "true pt ", event.truthMuonPt[it]
												print "true eta ", event.truthMuonEta[it]
												print "true phi ", event.truthMuonPhi[it]
												print ""
											print ""
											print "reco link barcodes"
											for ir  in range(len(event.muon_matchedBarcode)):
												print "reco particle ", ir
												print "reco barcode ", event.muon_matchedBarcode[ir]
												print "reco pt ", event.muon_pt[ir]
												print  "reco eta ", event.muon_eta[ir]
												print  "reco phi ", event.muon_phi[ir]
												print ""
											print "#################################"
										'''



										#selection cuts
										if len(event.muon_pt) >= 2:
											cutflow.Fill("2 reco muons", SF_mu*lumi)
											if event.muon_pt[0] > PTCUT:
												cutflow.Fill("leading muon Pt cut", SF_mu*lumi)
												if event.muon_pt[1] > PTCUT:
													cutflow.Fill("subleading muon Pt cut", SF_mu*lumi)
													#if nMatched_muon >= 2:
													if nMatched_muon >= 2:
														cutflow.Fill("2 matched muons",SF_mu*lumi)
														if(event.pass_muon):
															cutflow.Fill("trigger", SF_mu*lumi)
															if abs(event.muon_eta[0]) < ETACUT:
																cutflow.Fill("leading muon Eta cut", SF_mu*lumi)
																if abs(event.muon_eta[1]) < ETACUT:
																	cutflow.Fill("subleading muon Eta Cut", SF_mu*lumi)
																	#signal cuts
																	if abs(event.muon_d0[0]) > D0CUT and abs(event.muon_d0[1]) > D0CUT:
																		cutflow.Fill("d0 signal",SF_mu*lumi)

														
													
													#fillD0(event.muon_d0[0],leading_reco_muon, d0Vals)
													#fillD0(event.muon_d0[1], subleading_reco_muon, d0Vals)
														
																		h_nReco_reco_lead_muon_noSF.Fill(event.truthMuonD0[0], lumi)
																		h_nReco_reco_sublead_muon_noSF.Fill(event.truthMuonD0[1], lumi)
																		h_nReco_reco_lead_muon.Fill(event.truthMuonD0[0],SF_mu*lumi)
																		h_nReco_reco_sublead_muon.Fill(event.truthMuonD0[1],SF_mu*lumi)


					#fillD0(event.truthMuonD0[0],leading_true_muon, d0Vals)
					#fillD0(event.truthMuonD0[1], subleading_true_muon, d0Vals)

			
		
		hists.append(h_nReco_true_lead_muon_noSF)
		hists.append(h_nReco_reco_lead_muon_noSF)
		hists.append(h_nReco_true_lead_elec_noSF)
		hists.append(h_nReco_reco_lead_elec_noSF)
		
		hists.append(h_nReco_true_lead_muon)
		hists.append(h_nReco_reco_lead_muon)
		hists.append(h_nReco_true_lead_elec)
		hists.append(h_nReco_reco_lead_elec)
		
		hists.append(h_nReco_true_sublead_muon_noSF)
		hists.append(h_nReco_reco_sublead_muon_noSF)
		hists.append(h_nReco_true_sublead_elec_noSF)
		hists.append(h_nReco_reco_sublead_elec_noSF)
		
		hists.append(h_nReco_true_sublead_muon)
		hists.append(h_nReco_reco_sublead_muon)
		hists.append(h_nReco_true_sublead_elec)
		hists.append(h_nReco_reco_sublead_elec)

	#	for histo in hists:
	#		for iBin in xrange(histo.GetNbinsX()):
	#			histo.SetBinContent(iBin, histo.GetBinContent(iBin)/histo.GetBinWidth(iBin))

		
		#hists.append(h_Efficiency_true_muon_noSF)
		#hists.append(h_Efficiency_reco_muon_noSF)
		#hists.append(h_Efficiency_true_elec_noSF)
		#hists.append(h_Efficiency_reco_elec_noSF)
				
		#hists.append(h_Efficiency_true_muon)
		#hists.append(h_Efficiency_reco_muon)
		#hists.append(h_Efficiency_true_elec)
		#hists.append(h_Efficiency_reco_elec)
		
		hists.append(cutflow)	
		hists.append(cutflow_stau)	
		hists.append(pt_stau)
		hists.append(pt_dv)
		hists.append(pt_dv_sf)

		dv_file.Close()
		stau_file.Close()
		
		#eff_leading_muon = [0 for i in xrange(d0Range[lifetime] +1 ) ]
		#eff_subleading_muon = [0 for i in xrange(d0Range[lifetime] +1 )]
		#for i in range(len(leading_true_muon)):
		#	if leading_true_muon[i] == 0 or leading_reco_muon[i] == 0:
		#		eff_leading_muon[i] = 0
		#	else:
		#		eff_leading_muon[i] = (leading_reco_muon[i]*1.0)/leading_true_muon[i]
		#	
		#	if subleading_true_muon[i] == 0 or subleading_reco_muon[i] == 0:
		#		eff_leading_muon[i] = 0
		#	else:
		#		eff_subleading_muon[i] = (subleading_reco_muon[i]*1.0)/subleading_true_muon[i]
		#
		#
		#eff_leading = ROOT.TGraph(len(d0Vals),array('d',d0Vals), array('d',eff_leading_muon))
		#eff_subleading = ROOT.TGraph(len(d0Vals),array('d',d0Vals), array('d',eff_subleading_muon))
		
		
		eff_leading_noSF = ROOT.TGraphAsymmErrors(h_nReco_reco_lead_muon_noSF, h_nReco_true_lead_muon_noSF, "cl=0.683 b(1,1) mode")
		eff_subleading_noSF = ROOT.TGraphAsymmErrors(h_nReco_reco_sublead_muon_noSF, h_nReco_true_sublead_muon_noSF,"cl=0.683 b(1,1) mode")
		eff_leading = ROOT.TGraphAsymmErrors(h_nReco_reco_lead_muon, h_nReco_true_lead_muon,"cl=0.683 b(1,1) mode")
		eff_subleading = ROOT.TGraphAsymmErrors(h_nReco_reco_sublead_muon, h_nReco_true_sublead_muon,"cl=0.683 b(1,1) mode")
		
		eff_leading_noSF.SetTitle("leading muon efficiency (no SF)")
		eff_subleading_noSF.SetTitle("subleading muon efficiency (no SF)")
		eff_leading.SetTitle("leading muon efficiency")
		eff_subleading.SetTitle("subleading muon efficiency")

		
		outFile = ROOT.TFile("outFiles/signal_lifetime_"+lifetime+"_mass_"+mass+"_nEvents.root", "RECREATE")
	        	
		print "Number of events passing all cuts: ",  h_nReco_reco_lead_muon.Integral(0, h_nReco_reco_lead_muon.GetNbinsX()+1)
		print "Number of truth events in fiducial region: ",  h_nReco_true_lead_muon.Integral(0, h_nReco_true_lead_muon.GetNbinsX()+1)
		print "Number of stau events in fiducial region: ", pt_stau.Integral(0, pt_stau.GetNbinsX()+1)
		'''
		print "Number of events passing all cuts: ",  h_nReco_reco_lead_muon.Integral()
		print "Number of truth events in fiducial region: ",  h_nReco_true_lead_muon.Integral()
		print "Number of truth events in fiducial region: ",  pt_dv_sf.Integral()
		print "Number of stau events in fiducial region: ", pt_stau.Integral()
	        '''	
		eff_leading_noSF.Write()
		eff_subleading_noSF.Write()
		eff_leading.Write()
		eff_subleading.Write()

		for hist in hists:
			hist.Write()
		
		outFile.Write()
		outFile.Close()

#print table



'''	
#fiducial cuts
if len(event.truthElectronD0) >=2:
	
	SF_elec = getSF(event.truthElectronPt[0], event.truthElectronPt[1], mass, lifetime, "electron")
	
	nMatched_elec = 0
	for index in event.elec_RecoTrueMatchIndices:
		if index >= 0:
			nMatched_elec += 1
	if nMatched_elec >= 2:
		#selection cuts
		if event.elec_pt[0] > PTCUT and event.elec_pt[1] > PTCUT and abs(event.elec_eta[0]) < ETACUT and abs(event.elec_eta[1]) < ETACUT:
			
			
			#fillD0(event.elec_d0[0],leading_reco_elec, d0Vals)
			#fillD0(event.elec_d0[1], subleading_reco_elec, d0Vals)
			
			h_nReco_reco_lead_elec_noSF.Fill(event.elec_d0[0])
			h_nReco_reco_lead_elec_SF.Fill(event.elec_d0[0],SF_elec)
			h_nReco_reco_sublead_elec_noSF.Fill(event.elec_d0[1])
			h_nReco_reco_sublead_elec.Fill(event.elec_d0[1], SF_elec)
	#selection cuts
	if event.truthElectronPt[0] > PTCUT and event.truthElectronPt[1] > PTCUT and abs(event.truthElectronEta[0]) < ETACUT and abs(event.truthElectronEta) < ETACUT and event.truthElectronBarcode[0] < GOODTRUE and event.truthElectronBarcode[1] < GOODTRUE:
		

		#fillD0(event.truthElectronD0[0],leading_true_elec, d0Vals)
		#fillD0(event.truthElectronD0[1], subleading_true_elec, d0Vals)
		
		h_nReco_true_lead_elec_noSF.Fill(event.truthElectronD0[0])
		h_nReco_true_sublead_elec_noSF.Fill(event.truthElectronD0[1])
		h_nReco_true_lead_elec.Fill(event.truthElectronD0[0], SF)
		h_nReco_true_sublead_elec.Fill(event.truthElectronD0[1], SF)

'''

'''
h_nReco_true_lead_elec_noSF = ROOT.TH1F("noSF_nEvents_trued0_lead_elec","noSF_nEvents_trued0_lead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_lead_muon_noSF = ROOT.TH1F("noSF_nEvents_trued0_lead_muon", "noSF_nEvents_trued0_lead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_lead_elec_noSF = ROOT.TH1F("noSF_nEvents_recod0_lead_elec","noSF_nEvents_recod0_lead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_lead_muon_noSF = ROOT.TH1F("noSF_nEvents_recod0_lead_muon","noSF_nEvents_recod0_lead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_lead_elec = ROOT.TH1F("nEvents_trued0_lead_elec","nEvents_trued0_lead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_lead_elec = ROOT.TH1F("nEvents_recod0_lead_elec","nEvents_recod0_lead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_lead_muon = ROOT.TH1F("nEvents_trued0_lead_muon","nEvents_trued0_lead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_lead_muon = ROOT.TH1F("nEvents_recod0_lead_muon","nEvents_recod0_lead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])

h_nReco_true_sublead_elec_noSF = ROOT.TH1F("noSF_nEvents_trued0_sublead_elec","noSF_nEvents_trued0_sublead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_sublead_elec_noSF = ROOT.TH1F("noSF_nEvents_recod0_sublead_elec","noSF_nEvents_recod0_sublead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_sublead_muon_noSF = ROOT.TH1F("noSF_nEvents_trued0_sublead_muon", "noSF_nEvents_trued0_sublead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_sublead_muon_noSF = ROOT.TH1F("noSF_nEvents_recod0_sublead_muon","noSF_nEvents_recod0_sublead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_sublead_elec = ROOT.TH1F("nEvents_trued0_sublead_elec","nEvents_trued0_sublead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_sublead_elec = ROOT.TH1F("nEvents_recod0_sublead_elec","nEvents_recod0_sublead_elec", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_true_sublead_muon = ROOT.TH1F("nEvents_trued0_sublead_muon","nEvents_trued0_sublead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
h_nReco_reco_sublead_muon = ROOT.TH1F("nEvents_recod0_sublead_muon","nEvents_recod0_sublead_muon", d0Range[lifetime]/d0Bins[lifetime], 0, d0Range[lifetime])
''' 
