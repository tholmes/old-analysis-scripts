import ROOT
ROOT.gROOT.LoadMacro("../../plotting/atlasstyle/AtlasStyle.C")

stau_masses = ["80","100","120","140"]

ROOT.SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)
canvas = ROOT.TCanvas()


#0p0 is 0 GeV Neutralino
pt_file = ROOT.TFile("outFiles/signal_lifetime_100_mass_100_nEvents.root")


stau_pt = pt_file.Get("pt_stau")
divided = pt_file.Get("pt_stau")
dv_pt   = pt_file.Get("pt_dv")
sf_pt 	= pt_file.Get("pt_dv_sf")

for i in xrange(stau_pt.GetNbinsX()):	
	if stau_pt.GetBinContent(i) < dv_pt.GetBinContent(i):
		print "fewer elements in the stau sample than the lqd sample!"
		print "bin content of stau ", stau_pt.GetBinContent(i)
		print "bin content of lqd ", dv_pt.GetBinContent(i)
		print "at pt ", i*20

dv_pt.SetLineColor(ROOT.kBlue)
sf_pt.SetLineColor(ROOT.kRed)
sf_pt.SetLineStyle(8)


canvas.Clear()

legend = ROOT.TLegend(.6, 0.6, 0.9, 0.7)
legend.AddEntry(stau_pt,"From stau","l")
legend.AddEntry(dv_pt, "From LQD","l")
legend.AddEntry(sf_pt, "LQD with scaling","l")


stau_pt.GetXaxis().SetTitle("p_{T}^{true} leading lepton")


stau_pt.Draw("hist")
dv_pt.Draw("histsame")
sf_pt.Draw("histsame")
legend.Draw("same")

canvas.Modified()
canvas.Update()
canvas.SaveAs("outFiles/overlay.pdf")

divided.Divide(sf_pt)

line = ROOT.TLine(0,1,1000,1)
line.SetLineStyle(2)

canvas.Clear()
divided.Draw()
line.Draw("same")
canvas.Modified()
canvas.Update()
canvas.SaveAs("outFiles/divided.pdf")





