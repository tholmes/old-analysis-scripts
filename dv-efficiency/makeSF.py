import ROOT
ROOT.gROOT.LoadMacro("../../plotting/atlasstyle/AtlasStyle.C")

stau_masses = ["80","100","120","140"]
lifetimes = ["10","100","1000"]

lumi = 1 #100fb^-1 in pb^-1


ROOT.SetAtlasStyle()
ROOT.gStyle.SetOptStat(0)



#########################################
#   DV SF
#########################################
hists_dv = []

for lifetime in lifetimes:
#	print "../../Analysis/dv_allfiles/output/ct"+lifetime+".root"
	dv_file = ROOT.TFile("../../Analysis/lqd_allfiles/output/ct"+lifetime+".root")
	#dv_file = ROOT.TFile("../../Analysis/submit_dir/data-trees/mc15_13TeV.402803.MGPy8EG_A14N23LO_SS_RPVDV_700_500_lam12k_10.recon.DAOD_RPVLL.e4332_s2608_s2183_r8788.root")
	dv_tree = dv_file.Get("trees_SRDV_")


	Mdv_elec = ROOT.TH2F("lifetime_"+lifetime+"_electron", "lifetime_"+lifetime+"_electron", 50, 0, 1000, 50, 0, 1000); 
	Mdv_muon = ROOT.TH2F("lifetime_"+lifetime+"_muon", "lifetime_"+lifetime+"_muon", 50, 0, 1000, 50, 0, 1000); 
        Mdv_elec.SetDirectory(0)	#this lets you access the histo outside the loop
        Mdv_muon.SetDirectory(0)	#this lets you access the histo outside the loop
        #Mdv_elec.GetXaxis.SetTitle("leading e P_{T}")
        #Mdv_elec.GetYaxis.SetTitle("subleading e P_{T}")
        #Mdv_muon.GetXaxis.SetTitle("leading #mu P_{T}")
        #Mdv_muon.GetYaxis.SetTitle("subleading #mu P_{T}")
	print "number of events " + str(dv_tree.GetEntries())
	for event in dv_tree:

		
		if len(event.truthElectronPt) >= 2:
			if event.truthElectronPt[0] > 60 and event.truthElectronPt[1] > 60 and abs(event.truthElectronEta[0]) < 2.5 and abs(event.truthElectronEta[1]) < 2.5:
				Mdv_elec.Fill(event.truthElectronPt[0], event.truthElectronPt[1], event.pileupWeight)
	
		if len(event.truthMuonPt) >= 2: 
			if event.truthMuonPt[0] > 60 and event.truthMuonPt[1] > 60 and abs(event.truthMuonEta[0]) < 2.5 and abs(event.truthMuonEta[1]) < 2.5:
				Mdv_muon.Fill(event.truthMuonPt[0], event.truthMuonPt[1])

	hists_dv.append(Mdv_elec)
	hists_dv.append(Mdv_muon)
	#dv_file.Close()


outFile_dv = ROOT.TFile("outFiles/SF_lqd.root","RECREATE")
for hist in hists_dv:
	hist.Write()

outFile_dv.Write()
outFile_dv.Close()


#########################################
#   stau SF
#########################################
hists_stau = []

for mass in stau_masses:

	nEvents = 0

	print "../../Analysis/stau_allfiles/output/mass"+mass+".root"
	stau_file = ROOT.TFile("../../Analysis/stau_allfiles/output/mass"+mass+".root")
	stau_tree = stau_file.Get("trees_SRDV_")


	Mstau = ROOT.TH2F("mass_"+mass, "mass_"+mass, 50, 0, 1000, 50, 0, 1000); 
        #Mdv.GetXaxis.SetTitle("leading #tau P_{T}")
        #Mdv.GetYaxis.SetTitle("subleading #tau P_{T}")
	normWeight = -1
	Mstau.SetDirectory(0)	#this lets you access the histo outside the loop
	print "number of events " + str(stau_tree.GetEntries())
	for event in stau_tree:
		normWeight = event.normweight
		#print "number of taus " + str(len(event.truthTauPt))
		if len(event.truthTauPt) >= 2: 
			if event.truthTauPt[0] > 60 and event.truthTauPt[1] > 60 and abs(event.truthaTauEta[0]) < 2.5 and abs(event.truthaTauEta[1]) < 2.5:
				Mstau.Fill(event.truthTauPt[0], event.truthTauPt[1], event.normweight* event.pileupWeight)
				
				nEvents += 1
	
	print "mass " + mass + " nEvents " + str(Mstau.Integral())
	print "total number of events " + str(stau_tree.GetEntries() * normWeight)
	hists_stau.append(Mstau)
	#dv_file.Close()


outFile_stau = ROOT.TFile("outFiles/SF_stau.root","RECREATE")
for hist in hists_stau:
	hist.Write()

outFile_stau.Write()
outFile_stau.Close()






#for mass in stau_masses:

