import ROOT
ROOT.gROOT.LoadMacro("../../plotting/atlasstyle/AtlasStyle.C")

masses = ["80","100","120","140"]
lifetimes = ["10","100","1000"]
samples = ["stau","lqd"]

ROOT.SetAtlasStyle()
'''
for lifetime in lifetimes:
	canvas = ROOT.TCanvas()


	
	t = ROOT.TText( .3, .8,  "LQD sample with " +lifetime+ "mm lifetime")
	t.SetNDC()
	sf_file = ROOT.TFile("outFiles/SF_lqd.root")
	print sf_file.ls()
	print "lifetime_"+ str(lifetime) +"_muon"
	sf = sf_file.Get("lifetime_"+lifetime+"_muon")

	ROOT.gStyle.SetPalette(51)

	sf.GetYaxis().SetTitle("subleading lepton p_{T}^{truth} [GeV]")
	sf.GetXaxis().SetTitle("leading lepton p_{T}^{truth} [GeV]")

	sf.Draw("COLZ")
	t.Draw("same")
	canvas.Modified()
	canvas.Update()
	canvas.SaveAs("outPlots/lqd_lifetime_"+lifetime+".pdf")



for mass in masses:
	canvas = ROOT.TCanvas()


	t = ROOT.TText( .3, .8,  "Stau sample with " +mass+ " GeV slepton")
	t.SetNDC()
	sf_file = ROOT.TFile("outFiles/SF_stau.root")
	print sf_file.ls()
	print "mass_"+ str(mass) +"_muon"
	sf = sf_file.Get("mass_"+mass)

	ROOT.gStyle.SetPalette(51)

	sf.GetYaxis().SetTitle("subleading lepton p_{T}^{truth} [GeV]")
	sf.GetXaxis().SetTitle("leading lepton p_{T}^{truth} [GeV]")

	sf.Draw("COLZ")
	t.Draw("same")
	canvas.Modified()
	canvas.Update()
	canvas.SaveAs("outPlots/stau_mass_"+mass+".pdf")
'''

for lifetime in lifetimes:
	
	file80 = ROOT.TFile("outFiles/lifetime_"+lifetime+"_mass_80_nEvents.root")
	file100 = ROOT.TFile("outFiles/lifetime_"+lifetime+"_mass_100_nEvents.root")
	file120 = ROOT.TFile("outFiles/lifetime_"+lifetime+"_mass_120_nEvents.root")
	file140 = ROOT.TFile("outFiles/lifetime_"+lifetime+"_mass_140_nEvents.root")
	
	graph80 = file80.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	
	graph100 = file100.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	
	graph120 = file120.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	
	graph140 = file140.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	

	canvas = ROOT.TCanvas()
	legend = ROOT.TLegend(.22, 0.75, 0.60, 0.90)


	canvas.SetLogx()

	ROOT.SetAtlasStyle()
	t = ROOT.TText( .6, 7,  "Leading Muon Efficiency \nlifetime " + lifetime + "mm")
	t.SetNDC()


	graph80.SetLineColor(ROOT.kBlack)
	graph80.SetMarkerColor(ROOT.kBlack)
	graph100.SetLineColor(ROOT.kBlue-3)
	graph100.SetMarkerColor(ROOT.kBlue-3)
	graph120.SetLineColor(ROOT.kMagenta-3)
	graph120.SetMarkerColor(ROOT.kMagenta-3)
	graph140.SetLineColor(ROOT.kCyan+2)
	graph140.SetMarkerColor(ROOT.kCyan+2)

	
	legend.SetHeader("Leading Muon Efficiency, lifetime " + lifetime + "mm")
	legend.AddEntry(graph80, "80  GeV Stau")
	legend.AddEntry(graph100, "100 GeV Stau")
	legend.AddEntry(graph120, "120 GeV Stau")
	legend.AddEntry(graph140, "140 GeV Stau")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	
	graph80.GetYaxis().SetTitle("Efficiency")
	graph80.GetXaxis().SetTitle("truth leading lepton d0")


	graph80.Draw("pela")
	graph100.Draw("pelsame")
	graph120.Draw("pelsame")
	graph140.Draw("pelsame")
	legend.Draw("same")
	t.Draw("same")

	canvas.Modified()
	canvas.Update()
	canvas.SaveAs("outPlots/Efficiency_lifetime_"+lifetime+".pdf")


for mass in masses:
	
	file10 = ROOT.TFile("outFiles/lifetime_10_mass_"+mass+"_nEvents.root")
	file100 = ROOT.TFile("outFiles/lifetime_100_mass_"+mass+"_nEvents.root")
	file1000 = ROOT.TFile("outFiles/lifetime_1000_mass_"+mass+"_nEvents.root")
	
	graph10 = file10.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	
	graph100 = file100.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	
	graph1000 = file1000.Get("divide_nEvents_recod0_lead_muon_by_nEvents_trued0_lead_muon")	

	canvas = ROOT.TCanvas()
	legend = ROOT.TLegend(.22, 0.6, 0.44, 0.85)


	ROOT.SetAtlasStyle()

	canvas.SetLogx()

	graph10.SetLineColor(ROOT.kBlack)
	graph10.SetMarkerColor(ROOT.kBlack)
	graph100.SetLineColor(ROOT.kGreen+3)
	graph100.SetMarkerColor(ROOT.kGreen+3)
	graph1000.SetLineColor(ROOT.kMagenta+2)
	graph1000.SetMarkerColor(ROOT.kMagenta+2)

	
	legend.SetHeader("Leading Muon Efficiency, \n slepton mass " + mass+ "GeV")
	legend.AddEntry(graph10, "10 mm lifetime")
	legend.AddEntry(graph100, "100mm lifetime")
	legend.AddEntry(graph1000, "1000mm lifetime")
	legend.SetFillStyle(0)
	legend.SetBorderSize(0)
	
	graph1000.GetYaxis().SetTitle("Efficiency")
	graph1000.GetYaxis().SetRangeUser(0,1)
	graph1000.GetXaxis().SetTitle("truth leading lepton d0")

	graph1000.Draw("pela")
	graph100.Draw("pelsame")
	graph10.Draw("pelsame")
	legend.Draw("same")

	canvas.Modified()
	canvas.Update()
	canvas.SaveAs("outPlots/Efficiency_mass_"+mass+".pdf")

'''


f_overlay = ROOT.TFile("outFiles/lifetime_100_mass_140_nEvents.root")
canvas = ROOT.TCanvas()

h_stau = f_overlay.Get("pt_stau")
h_lqd = f_overlay.Get("pt_dv")
h_sf = f_overlay.Get("pt_dv_sf")

h_stau.SetLineColor(ROOT.kBlack)
h_lqd.SetLineColor(ROOT.kBlue+2)
h_sf.SetLineColor(ROOT.kRed)
h_sf.SetLineStyle(4)

h_stau.GetXaxis().SetTitle("leading lepton p_{T}^{truth}")

leg = ROOT.TLegend(.6, 0.6, 0.9, 0.85)
leg.AddEntry(h_stau, "from stau","l")
leg.AddEntry(h_lqd, "from lqd","l")
leg.AddEntry(h_sf, "lqd reweighted to stau","l")
leg.SetFillStyle(0)
leg.SetBorderSize(0)

h_stau.Draw("hist")
h_lqd.Draw("histsame")
h_sf.Draw("histsame")
leg.Draw("same")

canvas.Modified()
canvas.Update()
canvas.SaveAs("outPlots/mySFworks.pdf")



'''





