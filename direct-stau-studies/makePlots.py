import ROOT

stau_masses = ["80","100","120","140"]
neutralino_masses = ["0","20","40"]

for neutralino_mass in neutralino_masses:
	histos = []
	canvas = ROOT.TCanvas()
	legend = ROOT.TLegend(.6, 0.6, 0.9, 0.85)
	ROOT.gStyle.SetOptStat(0)

	f_80 = ROOT.TFile("80p0_"+neutralino_mass+"p0.root")
	f_100 = ROOT.TFile("100p0_"+neutralino_mass+"p0.root")
	f_120 = ROOT.TFile("120p0_"+neutralino_mass+"p0.root")
	f_140 = ROOT.TFile("140p0_"+neutralino_mass+"p0.root")

	h_80 = f_80.Get("80p0_"+neutralino_mass+"p0")
	h_100 = f_100.Get("100p0_"+neutralino_mass+"p0")
	if neutralino_mass == "40":
		h_120 = f_120.Get("120p0_20p0")
	else:
		h_120 = f_120.Get("120p0_"+neutralino_mass+"p0")
	h_140 = f_140.Get("140p0_"+neutralino_mass+"p0")

	h_80.SetLineColor(ROOT.kBlack)
	h_100.SetLineColor(ROOT.kRed)
	h_120.SetLineColor(ROOT.kGreen+3)
	h_140.SetLineColor(ROOT.kBlue)

	legend.AddEntry(h_80, "80  GeV Stau")
	legend.AddEntry(h_100, "100 GeV Stau")
	legend.AddEntry(h_120, "120 GeV Stau")
	legend.AddEntry(h_140, "140 GeV Stau")
	legend.SetBorderSize(0)

	h_80.SetTitle("Truth Tau P_{T} spectrum from Stau, with "+neutralino_mass+" GeV Neutralino")
	h_80.GetXaxis().SetRangeUser(0,500)
	h_80.GetXaxis().SetTitle("True P_{T} #tau")	
	h_80.Draw("hist")
	h_100.Draw("histsame")
	h_120.Draw("histsame")
	h_140.Draw("histsame")
	
	legend.Draw("same")
	
	canvas.Modified()
	canvas.Update()
	canvas.SaveAs("trueTauPt_"+neutralino_mass+"neutralino.pdf")



# Figure this otu later, the above is frustratingly dumb
	
# 	for stau_mass in stau_masses:
# 		
# 		name = stau_mass + "p0_" + neutralino_mass + "p0"
# 		
# 		print "Opening file " + name + ".root"
# 		file_histo = ROOT.TFile( name+ ".root")
# 		
# 		
# 		if stau_mass == "120" and neutralino_mass == "40":
# 			neutralino_mass = "20"
# 		
# 		name = stau_mass + "p0_" + neutralino_mass + "p0"
# 		print "Getting histo " + name
# 		histo = file_histo.Get(name)
# 		
# 		if neutralino_mass == "0":
# 			histo.SetLineColor(ROOT.kBlack)
# 		if neutralino_mass == "20":
# 			histo.SetLineColor(ROOT.kBlue-3)
# 		if neutralino_mass == "40":
# 			histo.SetLineColor(ROOT.kMagenta+3)
# 		
# 		histo.SetTitle("Truth P_{T} \tau from "+ stau_mass + " GeV stau")
# 		#histo.GetXaxis.SetRangeUser(0,500)
# 		legend.AddEntry(histo, "Neutralino Mass " + neutralino_mass + " GeV")
# 		histos.append(histo)
# 
# 	for h in histos:
# 		h.Draw("histsame")
# 	legend.Draw("same")
# 
# 	canvas.Modified()
# 	canvas.Update()
# 	canvas.SaveAs(stau_mass + "_tauPt.pdf")
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
# 
