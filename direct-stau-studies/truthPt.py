#asetup AthAnalysisBase,2.4.38

import ROOT
import os
import argparse
from ROOT.POOL import TEvent
evt = TEvent(TEvent.kAthenaAccess)


inDir = "/share/t3data2/horyn/stau"

parser = argparse.ArgumentParser()
parser.add_argument("filename", help="file name", type=str)
args = parser.parse_args()

num = str(args.filename).split(".")[-4]
names = str(args.filename).split(".")[2].split("_")
outName = names[-2] + "_" + names[-1]
print outName
print num


AODFile = ROOT.TFile.Open(args.filename)
h = ROOT.TH1F(outName, outName, 200, 0, 1000)
evt.readFrom(AODFile)
print "Number of Events " + str(evt.getEntries())
for entry in xrange(evt.getEntries()):
	evt.getEntry(entry)
	evtInfo = evt.retrieve("xAOD::EventInfo","EventInfo")
	eventNumber = evtInfo.eventNumber()
	if eventNumber%1000 == 0:
		print "Processing event " + str(eventNumber)
	truthParticles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")
	for iT, truthParticle in enumerate(truthParticles):
		if abs(truthParticle.pdgId()) == 15 and truthParticle.barcode() < 200000:
			h.Fill(truthParticle.pt()/1000)

outfile = ROOT.TFile(outName+num+".root", "RECREATE")
h.Write()
outfile.Write()



# for dirName in os.listdir(inDir):
# 	names = str(dirName).split(".")[2].split("_")
# 	outName = names[-2] + "_" + names[-1]
# 	print outName
# 	h = ROOT.TH1F(outName, outName, 500, 0, 2000)
# 	for filename in os.listdir(inDir+"/"+dirName):
# 		AODFile = ROOT.TFile.Open(inDir+"/"+dirName+"/"+filename)
# 		evt.readFrom(AODFile)
# 	
# 		print "Number of Events " + str(evt.getEntries())
# 			
# 		for entry in xrange(evt.getEntries()):
# 			evt.getEntry(entry)
# 			evtInfo = evt.retrieve("xAOD::EventInfo","EventInfo")
# 			eventNumber = evtInfo.eventNumber()
# 			if eventNumber%1000 == 0:
# 				print "Processing event " + str(eventNumber)
# 
# 			truthParticles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")
# 			for iT, truthParticle in enumerate(truthParticles):
# 				if abs(truthParticle.pdgId()) == 15 and truthParticle.barcode() < 200000:
# 					h.Fill(truthParticle.pt()/1000)
# 		AODFile.Close()
# 
# 	outfile = ROOT.TFile(outName+".root", "RECREATE")
# 	h.Write()
# 	outfile.Write()
