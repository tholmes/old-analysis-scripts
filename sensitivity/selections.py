from math import *

def toGeV(val):
        return val*.001

def deltaPhi(phi1, phi2):
        dphi = abs(phi1-phi2)
        if (dphi > pi):
              dphi = 2.*pi - dphi
        return dphi


#0L SUSY ATLAS-CONF-2016-078
def pass_0L_2j1200(parts, cutflow, met, mef, ht):
        weight = ((.47+.18) * 36.1)/1000 #this study used 36.1fb^-1
        #preselection
        if len(parts) < 2: return 0
        cutflow.Fill("PS: 2 jets",1*weight)

        if toGeV(parts[0].pt()) < 250: return 0
        cutflow.Fill("PS: lead jet pt", 1*weight)

        if toGeV(parts[1].pt()) < 50: return 0 
        cutflow.Fill("PS: sublead jet pt",1*weight)

        if toGeV(met.Pt()) < 200: return 0
        cutflow.Fill("PS: met",1*weight)
        
        if toGeV(mef) < 800: return 0
        cutflow.Fill("PS: meff",1*weight)

        # Signal Region
        if toGeV(met.Pt()) < 250: return 0
        cutflow.Fill("SR:MET",1*weight)

        if toGeV(parts[0].pt()) < 250: return 0
        cutflow.Fill("SR: lead jet pt", 1*weight)

        if len(parts) < 2 or toGeV(parts[1].pt()) < 250: return 0
        cutflow.Fill("SR: next jet pt", 1*weight)

        #if len(parts) < 5 or toGeV(parts[4].pt()) < 50: return 0
        #cutflow.Fill("SR: next next jet pt", 1)

        if fabs(parts[0].eta()) > 0.8 or fabs(parts[1].eta()) > 0.8: return 0
        cutflow.Fill("SR: eta",1*weight)
        
        if deltaPhi(parts[0].phi(), met.Phi()) < .4 or deltaPhi(parts[1].phi(), met.Phi()) < .4 : return 0
        cutflow.Fill("SR: dPhi",1*weight)

        #if deltaPhi(parts[2].phi(), met.Phi()) < .2: return 0
        #cutflow.Fill("SR: 3rd jet dPhi",1)

        if toGeV(met.Pt())/sqrt(toGeV(ht)) < 14: return 0
        cutflow.Fill("SR: met/sqrt(ht)", 1*weight)      

        if toGeV(mef) < 1200: return 0
        cutflow.Fill("SR: meff",1*weight)
        
        return 1*weight


def pass_1L_2J(jets, tau, cutflow, met, meff, ht):
        weight = ((.47+.18) * 36.1)/1000 #this study used 36.1fb^-1
        cutflow.Fill("total", 1*weight) 
        
        if toGeV(tau.pt()) < 7 or toGeV(tau.pt()) > min(5*len(jets), 35): return 0
        cutflow.Fill("SR: lepton pt", 1*weight)
        
        if abs(tau.eta()) > 2.5: return 0
        cutflow.Fill("SR: Lepton eta", 1*weight)

        if len(jets) < 2: return 0
        cutflow.Fill("SR: nJets", 1*weight)

        if toGeV(met.Pt()) < 430: return 0
        cutflow.Fill("SR: MET", 1*weight)
        
        mT = sqrt( 2*toGeV(tau.pt())*toGeV(met.Pt())*(1-cos(deltaPhi(tau.phi(), met.Phi()))))
        if mT < 100: return 0
        cutflow.Fill("SR:mT", 1*weight)

        if toGeV(met.Pt())/toGeV(meff) < .25: return 0
        cutflow.Fill("SR: met/meff", 1*weight)

        if toGeV(meff) > 700: return 0
        cutflow.Fill("SR: meff", 1*weight)


        return 1*weight 


def pass_1L_4J(jets, tau, cutflow, met, meff, ht):
        weight = ((.47+.18) * 36.1)/1000 #this study used 36.1fb^-1
        cutflow.Fill("total", 1*weight) 
        
        if toGeV(tau.pt()) < 35: return 0
        cutflow.Fill("SR: lepton pt", 1*weight)
        
        if abs(tau.eta()) > 2.5: return 0
        cutflow.Fill("SR: Lepton eta", 1*weight)

        if len(jets) < 4 or len(jets) > 5: return 0
        cutflow.Fill("SR: nJets", 1*weight)

        if toGeV(met.Pt()) < 300: return 0
        cutflow.Fill("SR: MET", 1*weight)
        
        mT = sqrt( 2*toGeV(tau.pt())*toGeV(met.Pt())*(1-cos(deltaPhi(tau.phi(), met.Phi()))))
        if mT < 450 : return 0
        cutflow.Fill("SR:mT", 1*weight)

        if toGeV(met.Pt())/toGeV(meff) < .25: return 0
        cutflow.Fill("SR: met/meff", 1*weight)

        if toGeV(meff) < 1000: return 0
        cutflow.Fill("SR: meff", 1*weight)


        return 1*weight 

       
def pass_photon_SRL(jets, photon, cutflow, met, meff):
      weight = ((.47+.18) + 13.3)/1000  #13.3fb^-1
      cutflow.Fill("total", 1*weight)

      if toGeV(photon.pt()) < 145: return 0
      cutflow.Fill("photon pt", 1*weight)

      if len(jets) <= 4: return 0
      cutflow.Fill("nJets", 1*weight)

      if deltaPhi(photon.phi(), met.Phi()) < 0.4: return 0
      cutflow.Fill("dPhi (photon, met)", 1*weight)

      if toGeV(met.Pt()) < 200: return 0
      cutflow.Fill("met", 1*weight)

      if toGeV(meff) < 2000: return 0
      cutflow.Fill("meff", 1*weight)

      return 1*weight

def pass_photon_SRH(jets, photon, cutflow, met, meff):
      weight = ((.47+.18) + 13.3)/1000  #13.3fb^-1
      cutflow.Fill("total", 1*weight)

      if toGeV(photon.pt()) < 400: return 0
      cutflow.Fill("photon pt", 1*weight)

      if len(jets) <= 2: return 0
      cutflow.Fill("nJets", 1*weight)

      if deltaPhi(photon.phi(), met.Phi()) < 0.4: return 0
      cutflow.Fill("dPhi (photon, met)", 1*weight)

      if toGeV(met.Pt()) < 400: return 0
      cutflow.Fill("met", 1*weight)

      if toGeV(meff) < 2000: return 0
      cutflow.Fill("meff", 1*weight)

      return 1*weight
