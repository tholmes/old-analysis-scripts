import ROOT
import os
import argparse
import sys
import glob
from math import *



susy15file = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.mc16_13TeV.399022.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_0p1ns.SUSY15_mc16a_031919_trees.root/user.lhoryn.17484053._000001.trees.root"
SUSY15Files = glob.glob(susy15file)
susy15_chain = ROOT.TChain("trees_SR_highd0_")
for filename in SUSY15Files:
    susy15_chain.Add(filename)

aodfile = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/user.lhoryn.mc16_13TeV.399022.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_0p1ns.mc16a_DAOD_RPVLL_v2_031319_trees.root/user.lhoryn.17431205._000001.trees.root"
aodFiles = glob.glob(aodfile)
aod_chain = ROOT.TChain("trees_SR_highd0_")
for filename in aodFiles:
    aod_chain.Add(filename)


susy15_cutflow = ROOT.TH1F("susy15","susy15",7,0,7)
aod_cutflow = ROOT.TH1F("aod","aod",7,0,7)

susy15_pass = 0
aod_pass = 0

for event in susy15_chain:
    susy15_cutflow.Fill("all",1)
    if len(event.electron_pt) > 1 or len(event.muon_pt) > 1:
        susy15_cutflow.Fill("2 leptons", 1)
        
        if event.pass_HLT_g140_loose == 1 or event.pass_HLT_2g50_loose_L12EM20VH == 1 or event.pass_HLT_mu60_0eta105_msonly == 1:
            susy15_cutflow.Fill("trigger",1)
            
            nGoodElec = 0
            nGoodMu = 0
            for ie in xrange(len(event.electron_pt)): 
                if event.electron_pt[ie] > 100 and event.electron_eta[ie] < 2.5:
                    nGoodElec += 1
            for im in xrange(len(event.muon_pt)): 
                if event.muon_pt[im] > 100 and event.muon_eta[im] < 2.5:
                    nGoodMu += 1
            if nGoodMu + nGoodElec > 1:
                susy15_cutflow.Fill("2 good leptons", 1)
                susy15_pass += 1


for event in aod_chain:
    aod_cutflow.Fill("all",1)
    if len(event.electron_pt) > 1 or len(event.muon_pt) > 1:
        aod_cutflow.Fill("2 leptons", 1)
        
        if event.pass_HLT_g140_loose == 1 or event.pass_HLT_2g50_loose_L12EM20VH == 1 or event.pass_HLT_mu60_0eta105_msonly == 1:
            aod_cutflow.Fill("trigger",1)
            
            nGoodElec = 0
            nGoodMu = 0
            for ie in xrange(len(event.electron_pt)): 
                if event.electron_pt[ie] > 100 and event.electron_eta[ie] < 2.5:
                    nGoodElec += 1
            for im in xrange(len(event.muon_pt)): 
                if event.muon_pt[im] > 100 and event.muon_eta[im] < 2.5:
                    nGoodMu += 1
            if nGoodMu + nGoodElec > 1:
                aod_cutflow.Fill("2 good leptons", 1)
                aod_pass += 1


print "number of passed from AOD    ", aod_pass
print "number of passed from SUSY15 ", susy15_pass
outpfile = "../outputFiles/daodstudies.root"
outFile = ROOT.TFile(outpfile, "RECREATE")
print "made ", outpfile
susy15_cutflow.Write()
aod_cutflow.Write()

outFile.Write()

