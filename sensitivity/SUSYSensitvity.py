#asetup AthAnalysisBase,2.4.38
#ptMaker.sh to run (dumb work around)

import ROOT
import os
import argparse
import sys
import glob
from math import *
from ROOT.POOL import TEvent
from selections import *

#ROOT.gROOT.LoadMacro("../../plotting/atlasstyle/AtlasStyle.C")
#ROOT.SetAtlasStyle()


def goodLepton(particle):
	if abs(particle.pdgId()) != 15 and abs(particle.pdgId()) != 11 and abs(particle.absPdgId()) != 13: return False
	if abs(particle.parent().absPdgId()) not in [1000011, 1000013, 1000015, 2000011, 2000013, 2000015]: return False
        
	return True

def goodJet(particle):
        if particle.pt()*.001 < 30: return False
        if abs(particle.eta()) > 2.5: return False
        #need a jvt cut

        return True


def sortByPt(particles):
       particles.sort(key=lambda x: x.pt(), reverse=True) 

       
evt = TEvent(TEvent.kAthenaAccess)

#AODFiles = glob.glob('/eos/atlas/user/e/ekuwertz/highd0/DAOD_TRUTH1.406105.StauStau_directLLP_500_0_1ns.HITS.root')
AODFiles = glob.glob('/eos/atlas/user/e/ekuwertz/highd0/DAOD_TRUTH1.399013.StauStau_directLLP_300_0_0p01ns.HITS.root')
#AODFiles = glob.glob('/eos/atlas/user/e/ekuwertz/highd0/DAOD_TRUTH1.399014.StauStau_directLLP_300_0_0p1ns.HITS.root')
chain = ROOT.TChain("CollectionTree")


for filename in AODFiles:
	chain.Add(filename)
h={}
h["leadPt"] = ROOT.TH1F("leadPt", "leadPt", 100,0,1000) 
h["leadEta"] = ROOT.TH1F("leadEta", "leadEta", 100,-5,5) 
h["leadPhi"] = ROOT.TH1F("leadPhi", "leadPhi", 50,-4,4) 
h["leadD0"] = ROOT.TH1F("leadD0", "leadD0", 50,-100,100) 
h["leadZ0"] = ROOT.TH1F("leadZ0", "leadZ0", 100,-1000,1000) 
h["leadOpening"] = ROOT.TH1F("leadOpening", "leadOpening", 50,0,6) 
h["leadMetOpening"] = ROOT.TH1F("leadMetOpening", "leadMetOpening", 50,0,6) 
h["leadGravPt"] = ROOT.TH1F("leadGravPt", "leadGravPt", 100,0,2000) 
h["leadGravEta"] = ROOT.TH1F("leadGravEta", "leadGravEta", 100,-5,5) 
h["leadGravPhi"] = ROOT.TH1F("leadGravPhi", "leadGravPhi", 50,-4,4) 


h["subleadPt"] = ROOT.TH1F("subleadPt", "subleadPt", 100,0,1000) 
h["subleadEta"] = ROOT.TH1F("subleadEta", "subleadEta", 100,-5,5) 
h["subleadD0"] = ROOT.TH1F("subleadD0", "subleadD0", 50,-100,100) 
h["subleadZ0"] = ROOT.TH1F("subleadZ0", "subleadZ0", 100,-1000,1000) 
h["subleadPhi"] = ROOT.TH1F("subleadPhi", "subleadPhi", 50,-4,4) 
h["subleadOpening"] = ROOT.TH1F("subleadOpening", "subleadOpening", 50,0,6) 
h["subleadGravPt"] = ROOT.TH1F("subleadGravPt", "subleadGravPt", 100,0,2000) 
h["subleadGravEta"] = ROOT.TH1F("subleadGravEta", "subleadGravEta", 100,-5,5) 
h["subleadGravPhi"] = ROOT.TH1F("subleadGravPhi", "subleadGravPhi", 50,-4,4) 
h["subleadMetOpening"] = ROOT.TH1F("subleadMetOpening", "subleadMetOpening", 50,0,6) 

h["guessMet"] = ROOT.TH1F("guessMet", "guessMet",100,0,2000)
h["deltaR"] = ROOT.TH1F("deltaR","deltaR",50,0,6)
h["deltaPhi"] = ROOT.TH1F("deltaPhi","deltaPhi",50,-6,6)
h["lepMass_inc"] = ROOT.TH1F("lepMass_inc", "lepMass_inc", 20,0,1000) 
h["lepMass_ee"] = ROOT.TH1F("lepMass_ee", "lepMass_ee", 20,0,1000) 
h["lepMass_mm"] = ROOT.TH1F("lepMass_mm", "lepMass_mm", 20,0,1000) 
h["lepMass_em"] = ROOT.TH1F("lepMass_em", "lepMass_em", 20,0,1000) 

h["nJets"] = ROOT.TH1F("nJets","nJets",20, 0, 19)
h["jetHt"] = ROOT.TH1F("jetHt", "jetHt",50,0,4000)
h["jetHt_all"] = ROOT.TH1F("jetHt_all", "jetHt_all",50,0,4000)
h["Meff"]= ROOT.TH1F("Meff","Meff",50,0,4000)
h["leadJetPt"] = ROOT.TH1F("leadJetPt", "leadJetPt", 50,0,2000) 
h["subleadJetPt"] = ROOT.TH1F("subleadJetPt", "subleadJetPt", 50,0,2000) 

h["allD0"] = ROOT.TH1F("allD0","allD0",50,-10,10)
h["d0vspt"] = ROOT.TH2F("d0vspt","pt;d0(mm)",100,0,500,25,0,5)

h["sleptonPt"] = ROOT.TH1F("sleptonPt", "sleptonPt", 50,0,2000) 
h["0LSUSY_2j1200_cutflow"] = ROOT.TH1F("0LSUSY_2j1200_cutflow","0LSUSY_2j1200_cutflow",19,0,19)
h["0LSUSY_2j1200_cutflow_all"] = ROOT.TH1F("0LSUSY_2j1200_cutflow_all","0LSUSY_2j1200_cutflow_all",19,0,19)

h["1LSUSY_2j_cutflow"] = ROOT.TH1F("1LSUSY_2j_cutflow","1LSUSY_2j_cutflow",19,0,19)
h["1LSUSY_4j_cutflow"] = ROOT.TH1F("1LSUSY_4j_cutflow","1LSUSY_4j_cutflow",19,0,19)

h["1gSUSY_SRL_cutflow"] = ROOT.TH1F("1gSUSY_SRL_cutflow","1gSUSY_SRL_cutflow",19,0,19)
h["1gSUSY_SRH_cutflow"] = ROOT.TH1F("1gSUSY_SRH_cutflow","1gSUSY_SRH_cutflow",19,0,19)

evt.readFrom(chain)
print "Number of Events " + str(evt.getEntries())
passed_all_0L_2j1200 = 0
passed_0L_2j1200 = 0
passed_1L_2j = 0
passed_1L_4j = 0
passed_1g_SRL = 0
passed_1g_SRH = 0


for entry in xrange(evt.getEntries()):
	taus = []
	evt.getEntry(entry)
	if entry%50 == 0:
		print "Processing event " + str(entry)
	truthParticles = evt.retrieve("xAOD::TruthParticleContainer","TruthParticles")
        Ht_all = 0	
	particles = []
	for iT, truthParticle in enumerate(truthParticles):
              #  print "this particle is a ", truthParticle.absPdgId()
              
		if goodLepton(truthParticle):
			taus.append(truthParticle)
                        particles.append(truthParticle)
                        Ht_all += truthParticle.pt()
	
        if len(taus) != 2:
	 	print "not 2 taus! There are " + str(len(taus)) +" taus in the event" 
	 	continue

        truthJets = evt.retrieve("xAOD::JetContainer", "TruthJets")
        nJets = 0
        Ht = 0
        jets = []
        for ij, jet in enumerate(truthJets):
                if goodJet(jet):
                      nJets += 1
                      Ht += jet.pt()
                      Ht_all += jet.pt()
                      jets.append(jet)
                      particles.append(jet)
                      #print "good jet parent: ", jet.parent().pdgId()

        if(nJets) > 0:
                h["leadJetPt"].Fill(toGeV(jets[0].pt()))
        if(nJets) > 1:
                h["subleadJetPt"].Fill(toGeV(jets[1].pt()))
        h["nJets"].Fill(nJets)
        h["jetHt"].Fill(toGeV(Ht))
        h["jetHt_all"].Fill(toGeV(Ht_all))
      
	

	gravitinos =[]
        TLVTaus=[]
        eTaus = []
        mTaus = []

        for it,tau in enumerate(taus):
		parent = tau.parent()
                h["sleptonPt"].Fill(toGeV(parent.pt()))

                gravitino = ROOT.TVector3()

                tmpT = ROOT.TLorentzVector()
                tmpT.SetPtEtaPhiM(tau.pt(),tau.eta(),tau.phi(),tau.m())
                
                #decay channels
                for i in xrange(tau.nChildren()):
                      if( abs(tau.child(i).pdgId()) == 11):
                            tmpE = ROOT.TLorentzVector()
                            tmpE.SetPtEtaPhiM(tau.child(i).pt(),tau.child(i).eta(),tau.child(i).phi(),tau.child(i).m())
                            eTaus.append(tmpE) ;
                      if(abs(tau.child(i).pdgId()) == 13):
                            tmpE = ROOT.TLorentzVector()
                            tmpE.SetPtEtaPhiM(tau.child(i).pt(),tau.child(i).eta(),tau.child(i).phi(),tau.child(i).m())
                            mTaus.append(tmpE) ;

                #find gravitino from slepton
                tmpG = ROOT.TLorentzVector()
                for i in xrange(parent.nChildren()):
                      if( parent.child(i).pdgId() == 1000039):
                            grav_pt = parent.child(i).pt()
                            grav_eta = parent.child(i).eta()
                            grav_phi = parent.child(i).phi()
                            tmpG.SetPtEtaPhiM(parent.child(i).pt(), parent.child(i).eta(),  parent.child(i).phi(), parent.child(i).m())
                            gravitinos.append(tmpG)
		
                
                trueVtx = parent.decayVtx()
		sleptonPos= ROOT.TVector3(trueVtx.x(), trueVtx.y(), trueVtx.z())
		lepton = ROOT.TVector3()
		lepton.SetMagThetaPhi(1.0,tau.p4().Theta(),tau.phi())

		d0 = sleptonPos.Perp() * sin(lepton.DeltaPhi(sleptonPos))
		
                opening = tmpG.DeltaR(tmpT)


                TLVTaus.append(tmpT)
		if it == 0:
			h["leadD0"].Fill(d0)
			h["allD0"].Fill(d0)
			#h_leadZ0.Fill(idtrack.z0)
			h["leadPt"].Fill(toGeV(tau.pt()))
			h["leadEta"].Fill(tau.eta())
			h["leadPhi"].Fill(tau.phi())
                        h["leadOpening"].Fill(opening)
                        h["leadGravPt"].Fill(toGeV(grav_pt))
                        h["leadGravEta"].Fill(grav_eta)
                        h["leadGravPhi"].Fill(grav_phi)
                        h["d0vspt"].Fill(abs(d0),toGeV(tau.pt()))

		if it == 1:
			h["subleadD0"].Fill(d0)
			h["allD0"].Fill(d0)
			#h_subleadZ0.Fill(idtrack.z0)
			h["subleadPt"].Fill(toGeV(tau.pt()))
			h["subleadEta"].Fill(tau.eta())
			h["subleadPhi"].Fill(tau.phi())
                        h["subleadOpening"].Fill(opening)
                        h["subleadGravPt"].Fill(toGeV(grav_pt))
                        h["subleadGravEta"].Fill(grav_eta)
                        h["subleadGravPhi"].Fill(grav_phi)
                        h["d0vspt"].Fill(abs(d0),toGeV(tau.pt()))


        if( len(eTaus) + len(mTaus) == 2 ):
            sumLeps = ROOT.TLorentzVector()
            for e in eTaus: 
                sumLeps += e
            for m in mTaus:
                sumLeps += m

            h["lepMass_inc"].Fill(toGeV(sumLeps.M()))
            if len(eTaus) == 2:
                h["lepMass_ee"].Fill(toGeV(sumLeps.M()))
            elif len(mTaus) == 2:
                h["lepMass_mm"].Fill(toGeV(sumLeps.M()))
            else:
                h["lepMass_em"].Fill(toGeV(sumLeps.M()))

        met = gravitinos[0] + gravitinos[1]
        meff = Ht + met.Pt()
        meff_all = Ht_all + met.Pt()
        h["guessMet"].Fill(toGeV(met.Pt()))
        
        for tt in xrange(len(TLVTaus)):
                oa = met.DeltaR(TLVTaus[tt])
                if tt == 0:
                    h["leadMetOpening"].Fill(oa)
                if tt == 1:
                    h["subleadMetOpening"].Fill(oa)

        h["deltaR"].Fill(TLVTaus[0].DeltaR(TLVTaus[1]))
        h["deltaPhi"].Fill(TLVTaus[0].DeltaPhi(TLVTaus[1]))
        h["Meff"].Fill(toGeV(meff))

        sortByPt(particles)

        passed_0L_2j1200 += pass_0L_2j1200(jets,h["0LSUSY_2j1200_cutflow"], met, meff, Ht)
        passed_all_0L_2j1200 += pass_0L_2j1200(particles,h["0LSUSY_2j1200_cutflow_all"], met, meff_all, Ht_all)
        

        particles_1L = particles
        particles_1L.append(taus[1])
        sortByPt(particles_1L)
        
        passed_1L_2j += pass_1L_2J(particles_1L, taus[0], h["1LSUSY_2j_cutflow"], met, meff, Ht)
        passed_1L_4j += pass_1L_4J(particles_1L, taus[0], h["1LSUSY_4j_cutflow"], met, meff, Ht)

        passed_1g_SRL += pass_photon_SRL(particles_1L, taus[0], h["1gSUSY_SRL_cutflow"], met, meff)
        passed_1g_SRH += pass_photon_SRH(particles_1L, taus[0], h["1gSUSY_SRH_cutflow"], met, meff)

print "number passed all cuts 0L 2j1200 ", passed_0L_2j1200
print "number passed all cuts inclusive 0L 2j1200 ", passed_all_0L_2j1200
print
print "number passed all cuts 1L 2j ", passed_1L_2j
print "number passed all cuts 1L 4j ", passed_1L_4j

print "number passed all cuts 1g SRL ", passed_1g_SRL
print "number passed all cuts 1g SRH ", passed_1g_SRH
outfile = ROOT.TFile("outputFiles/signal_1k_300_0p01ns.root", "RECREATE")

for histo in h:
    h[histo].Write()
  
outfile.Write()
outfile.Close()
