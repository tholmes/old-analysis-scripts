# helper script to check for duplicate events
# Definitions of duplicate events are as follows:
# 1. EvtGen duplicates: Events have identical truth values, but differing reconstructed values.
# 2. DxAOD duplicates: Events have the same reconstructed value. Simply the same events are used twice
# 3. Prodsys2 duplicates: Same event number used for multiple events but different event values.
# for MC, recommendation is to keep duplicate events
# Not recommendeed to use this for data since it is slow, use the .cc file instead

import ROOT
from array import array
import glob
import os
import signalMap

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")

pathVersion = '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/'

# should be signal or data
ntupTyp= 'signal'

fout = open (ntupTyp+'_duplicateEvents.txt', 'w')

if ntupTyp == 'signal':
    pathNtuples = pathVersion+'signal/hadded/'
    files=glob.glob(pathNtuples+'*.root')
else: # we are running on data
    pathNtuples = pathVersion+'data/'
    files=glob.glob(pathNtuples+'data1*root')

# ntupFile: ntuple root file
def getDuplicates(ntupFile):

    print 'Processing file:', ntupFile.split(pathNtuples)[1].split('.root')[0]

    # index corresponds to duplicate type
    dup = [0,0,0]

    eventIds = []
    runIds = []
    metLs = []
    truthPtLs = []
    truthEtaLs = []
    truthPhiLs = []

    t = getTree(ntupFile)
    maxEntries = t.GetEntries()
    
    fout.write('    Total number of entries: %s\n'%(maxEntries))
    
    # loop over all entries in tree
    for entry in xrange(maxEntries):
        #if entry % 5000 == 0: print 'Processing event:', entry
        t.GetEntry(entry)

        # look at events with at least 1 lepton
        if int(t.lepton_n) == 0: continue

        evtNo = int(t.eventNumber)
        runNo = int(t.runNumber)
        met = float(t.MET)
        
        if not(ntupTyp=='data'):
            if not (len(t.truthLepton_pt) > 0): continue

            truthLepPt = float(t.truthLepton_pt[0])
            truthLepEta = float(t.truthLepton_eta[0])
            truthLepPhi = float(t.truthLepton_phi[0])

            ####################################################################
            #### Type 1 duplication
            ####################################################################
            if not(truthLepPt in truthPtLs):
                truthPtLs.append(truthLepPt)
                truthEtaLs.append(truthLepEta)
                truthPhiLs.append(truthLepPhi)
            else:
                idx = truthPtLs.index(truthLepPt)
                if truthEtaLs[idx] == truthLepEta and truthPhiLs[idx] == truthLepPhi:
                    dup[0] += 1
                    print runNo, evtNo, truthLepPt, truthLepEta, truthLepPhi

        ####################################################################
        #### Type 2 duplication
        ####################################################################
        
        # check if reco quantity found in list, if not add it
        if not (met in metLs):
            metLs.append(met)
        else:
            dup[1] += 1

        ####################################################################
        #### Type 3 duplication
        ####################################################################

        # check if have seen event, if not, add it to the list
        if not (evtNo in eventIds):
            eventIds.append(evtNo)
            runIds.append(runNo)
            
        else : # check if the runNo + evtNo combination seen before, if not it's a duplicate event
            idx = eventIds.index(evtNo)
            if runIds[idx] == runNo: 
                dup[2] += 1
    
    print ''

    return dup

for ntup in files:

    if ntupTyp == 'signal':
        shortName = ntup.split(pathNtuples)[1].split('.root')[0]
        name = signalMap.signals.get(shortName)[0]
    else:
        name = ntup.split(pathNtuples)[1].split('.root')[0]

    fout.write('Duplicate information for: '+ name +'\n')

    dup = getDuplicates(ntup)

    fout.write('    Number of event generation duplicates: %s\n'%(dup[0]))
    fout.write('    Number of dxAOD duplicates: %s\n'%(dup[1]))
    fout.write('    Number of prodSys duplicates: %s\n'%(dup[2]))
    fout.write('\n')

fout.close()
