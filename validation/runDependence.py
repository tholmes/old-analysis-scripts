#!/usr/bin/env python
import glob
import csv
import ROOT
import os
from math import *
from array import array
execfile("../plot_helpers/basic_plotting.py")

#ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.TH1.SetDefaultSumw2()

years = {
        2015: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        2016: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        2017: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root ",
        2018: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
}

#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/to_be_merged/Data/"
#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/"
#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/"
file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/"


#regions = {
#        "2l":  "lepton_pt[0]>50 && lepton_pt[1]>50",
#        "2l, p_{T} > 65 GeV":  "lepton_pt[0]>65 && lepton_pt[1]>65",
#        }
#ordered_regions = ["2l", "2l, p_{T} > 65 GeV"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

triggers = "triggerRegion_pass==1"
#triggers = "(pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH || pass_HLT_g140_loose || pass_HLT_mu60_0eta105_msonly)"
#triggers = "(pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH)"

regions = {
        #"trigger": triggers,
        "2 baseline l":  "lepton_n_baseline==2",
        #"2l, d0 > 2mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2"%triggers,
        #"2l, d0, ee": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && electron_n==2"%triggers,
        #"2l, d0, mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && muon_n==2"%triggers,
        #"1 signal e": "lepton_n==1 && electron_n==1 && electron_pt[0]>50 && electron_dpt[0]>-0.5 && electron_chi2[0]<2 && abs(electron_d0)>2 && electron_nMissingLayers[0]<1.1",
        #"1 signal m": "lepton_n==1 && muon_n==1 && muon_CBtrack_chi2[0]<3 && muon_pt[0]>50 && abs(muon_IDtrack_d0[0])>2",
        "1 signal e": "electron_n_signal==1 && lepton_n_baseline==1", # && %s"%triggers,
        "1 signal m": "muon_n_signal==1 && abs(muon_t0avg[0])<30 && lepton_n_baseline==1", # && %s"%triggers,
        }
ordered_regions = ["2l", "2l, d0 > 2mm", "2l, d0, ee", "2l, d0, mm", "1e", "1m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]
ordered_regions = ["2 baseline l", "1 signal e", "1 signal m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

total_lumi = 0

for year in years:

    lumi_file = years[year]

    # Read in run luminosities
    lumis = {}
    f = ROOT.TFile(lumi_file, "READ")
    all_objs = f.GetListOfKeys()
    for obj in all_objs:
        if "_intlumi" in obj.GetName():
            run = obj.GetName().split("_")[0].strip("run")
            hist = f.Get(obj.GetName())
            lumi = hist.GetMaximum() # Gives lumi in microb-1
            lumis[int(run)] = lumi/1000000. # Gives lumi in pb-1
            total_lumi += lumi/1000000.

    # Get bin index for each run
    run_bins = {}
    i = 0
    for run in sorted(lumis.keys()):
        i+=1
        run_bins[run]=i

    # Fill hist with lumis
    h_lumis = ROOT.TH1F("h_lumis", "h_lumis", len(lumis.keys()), 0, len(lumis.keys())-1)
    for run in lumis.keys():
        h_lumis.SetBinContent(run_bins[run], lumis[run])
        h_lumis.SetBinError(run_bins[run], lumis[run]*.1)

    # Get events passed in each region
    h_passed = {}
    h_frac = {}
    for reg in regions.keys():
        h_passed[reg] = ROOT.TH1F("h_passed_"+reg, "h_passed_"+reg, len(lumis.keys()), 0, len(lumis.keys())-1)
        h_frac[reg] = ROOT.TH1F(reg, reg, len(lumis.keys()), 0, len(lumis.keys())-1)

    # Fill passing histograms
    fname = "data%d*.root"%(year%2000)
    print fname
    t = getTree(file_loc+fname)
    for reg in regions.keys():
        h_temp = getHist(t, "h_temp", "runNumber", binstr = "150000,250000,400000", sel_name=regions[reg])
        #print reg
        #c = ROOT.TCanvas("can", "can")
        #h_temp.Draw()
        #raw_input("...")
        for run in lumis.keys():
            n_passed = h_temp.GetBinContent(h_temp.FindBin(run))
            h_passed[reg].SetBinContent(run_bins[run], n_passed)
            h_passed[reg].SetBinError(run_bins[run], h_temp.GetBinError(h_temp.FindBin(run)))
            h_frac[reg].GetXaxis().SetBinLabel(run_bins[run], str(run))
            h_passed[reg].GetXaxis().SetBinLabel(run_bins[run], str(run))
            h_lumis.GetXaxis().SetBinLabel(run_bins[run], str(run))
        h_frac[reg].Divide(h_passed[reg], h_lumis)

    # Draw all
    hists = []
    for reg in ordered_regions:
        hists += [h_frac[reg]]
    for h in hists:
        h.GetXaxis().SetTitle("Run Number")
        h.GetYaxis().SetTitle("Events Passed / pb^{-1}")
    h_lumis.GetXaxis().SetTitle("Run Number")
    h_lumis.GetYaxis().SetTitle("Luminosity [pb^{-1}]")

    drawTwoPads(hists, h_lumis, savename="plots/runDependence_%d.pdf"%year, logTop=True, logBottom=False)

