#!/usr/bin/env python
import glob
import csv
import ROOT
import os
from math import *
from array import array
execfile("../plot_helpers/basic_plotting.py")

#ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.TH1.SetDefaultSumw2()

variables = {
        "mu": {"acc": "averageInteractionsPerCrossing", "label": "<#mu>", "nbins":500, "xmin":0, "xmax":80 },
        }

years = {
        2015: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        2016: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        2017: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root ",
        2018: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
}
#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.4_Nov2019_MSSeg/Data/"
#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/"
file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/"

triggers = "triggerRegion_pass==1"

regions = {
        #"trigger": triggers,
        "2 baseline l":  "lepton_n_baseline==2",
        #"2l, d0 > 2mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2"%triggers,
        #"2l, d0, ee": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && electron_n==2"%triggers,
        #"2l, d0, mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && muon_n==2"%triggers,
        #"1 signal e": "lepton_n==1 && electron_n==1 && electron_pt[0]>50 && electron_dpt[0]>-0.5 && electron_chi2[0]<2 && abs(electron_d0)>2 && electron_nMissingLayers[0]<1.1",
        #"1 signal m": "lepton_n==1 && muon_n==1 && muon_CBtrack_chi2[0]<3 && muon_pt[0]>50 && abs(muon_IDtrack_d0[0])>2",
        "1 signal e": "electron_n_signal==1 && lepton_n_baseline==1", # && %s"%triggers,
        "1 signal m": "muon_n_signal==1 && abs(muon_t0avg[0])<30 && lepton_n_baseline==1", # && %s"%triggers,
        }
ordered_regions = ["2l", "2l, d0 > 2mm", "2l, d0, ee", "2l, d0, mm", "1e", "1m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]
ordered_regions = ["2 baseline l", "1 signal e", "1 signal m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

for year in years:

    # Fill passing histograms
    fname = "data%d*.root"%(year%2000)
    print fname
    t = getTree(file_loc+fname)

    # Get mu distribution
    lumi_file = years[year]
    f = ROOT.TFile(lumi_file, "READ")
    mu_dist = f.Get("avgintperbx")
    mu_dist.Rebin(10)
    mu_dist.Scale(0.000001) # Change from mb to pb
    mu_dist.GetXaxis().SetTitle("<#mu>")
    mu_dist.GetYaxis().SetTitle("Luminosity [pb^{-1}]")
    nbins = mu_dist.GetNbinsX()
    xmin = mu_dist.GetXaxis().GetXmin()
    xmax = mu_dist.GetXaxis().GetXmax()

    # Fill yield as a function of mu
    for var in variables:
        h_mu = {}
        for reg in ordered_regions:
            h_mu[reg] = getHist(t, reg, variables[var]["acc"], binstr="%d,%d,%d"%(nbins,xmin,xmax), sel_name=regions[reg])
            print reg, h_mu[reg].Integral()
            h_mu[reg].Divide(mu_dist)

        # Draw all hists for var
        h_frac = []
        for h in ordered_regions:
            h_mu[h].GetXaxis().SetTitle(variables[var]["label"])
            h_mu[h].GetYaxis().SetTitle("Events Passed / pb^{-1}")
            h_frac.append(h_mu[h])

        drawTwoPads(h_frac, mu_dist, savename="plots/muDependence_%d.pdf"%year, logTop=True, logBottom=False)
