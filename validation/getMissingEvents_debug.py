# helper script to check for missing events
# need to run:
# lsetup rucio

import ROOT
from array import array
import glob
import os

# merged variable determines if we are using merged or un-merged files
# un-merged ntuples are by period
merged=False

defaultPath = '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/'
if not(merged):
    pathNtuples = defaultPath+'data_wip/already_merged/'
    strTyp = 'unmerged_'
else:
     pathNtuples = defaultPath+'v4_Feb27/data/'
     strTyp = 'merged_'

dataList = '/afs/cern.ch/user/e/eressegu/LLP/ntupleMaker/run/data_SUSY15.ds'
dataList = '/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v5/WorkArea/run/data_DAOD_RPVLL.ds'
debugList = 'debug.ls'

# Returns the number of events from the metadata tree
# ntup_file: ntuple root file 
def getNEvents(ntup_file):
    files = glob.glob(ntup_file)
    n_events = 0
    
    for fname in files:
        f = ROOT.TFile(fname, "READ")
        h = f.Get("MetaData_EventCount")
        n_events +=  h.GetBinContent(2)
       
    return int(n_events)

# Returns the number of events from rucio
# ntup_file: rucio file
def getRucioEvents(ntup_file):
    amiCmd = 'rucio list-files '+ntup_file+' | grep events'
    amiEvt =  os.popen(amiCmd).read().split('events : ')[1].split('\n')[0]
    
    return int(amiEvt)

fdata = open(dataList, 'r')
fdebug = open(debugList, 'r')
fout = open (strTyp+'missingEvents_debug.txt', 'w')

totalEvt = [0,0,0,0]
totalAMI = [0,0,0,0]

for data in fdata:
    ntup = data.split('\n')[0]

    if not(data.find('data15') > -1):continue
    for db in fdebug:

        nAMI = getRucioEvents(db)

        procNtup = ntup.split('.physics_Main')[0]
        runNumber = db.split('data15_13TeV:data15_13TeV.')[1].split('.physics_Main')[0]
        # check events per period in unmerged files
        if not(merged):
            
            nEvt = getNEvents(pathNtuples+'*'+procNtup+'*.root/'+'*'+runNumber+'*.root')
            #nEvt = getNEvents(pathNtuples+procNtup.split('13TeV.')[0]+procNtup.split('13TeV.')[1]+'*root')

            outStr= runNumber+', missing events: '+str(nAMI-nEvt)+', processed events: '+str(nEvt)+', rucio events: '+str(nAMI)+'\n'
            fout.write(outStr)

        if procNtup.find('data15') > -1:
            if not(merged): totalEvt[0] += nEvt
            totalAMI[0] += nAMI
        elif procNtup.find('data16') > -1:
            if not(merged): totalEvt[1] += nEvt
            totalAMI[1] += nAMI
        elif procNtup.find('data17') > -1:
            if not(merged): totalEvt[2] += nEvt
            totalAMI[2] += nAMI
        elif procNtup.find('data18') > -1:
            if not(merged): totalEvt[3] += nEvt
            totalAMI[3] += nAMI
 
for i in range (0, len(totalEvt)):
    if i == 0: year = '2015'
    elif i == 1: year = '2016'
    elif i == 2: year = '2017'
    else: year = '2018'
    outStr= year+', missing events: '+str(totalAMI[i]-totalEvt[i])+', processed events: '+str(totalEvt[i])+', AMI events: '+str(totalAMI[i])+'\n'
    
    fout.write(outStr)

fdata.close()
fout.close()
