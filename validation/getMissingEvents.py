# helper script to check for missing events
# need to run:
# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase; source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh; localSetupPyAMI

import ROOT
from array import array
import glob
import os

# merged variable determines if we are using merged or un-merged files
# un-merged ntuples are by period
merged=False
oldReco=False

defaultPath = '/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/'
if not(merged):
    if (oldReco): pathNtuples = defaultPath+'v5_May14_OldReco/data_wip/already_merged/'
    else: pathNtuples = defaultPath+'v5.1_May14_NewReco/'
    strTyp = 'unmerged_'
else:
     pathNtuples = defaultPath+'v5.1_May14_NewReco/'
     strTyp = 'merged_'

dataList = '/afs/cern.ch/user/e/eressegu/LLP/ntupleMaker/run/data_SUSY15.ds'
if (oldReco):dataList = '/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v5/WorkArea/run/data_DAOD_RPVLL.ds'
else: dataList = 'test.ds'

# Returns the number of events from the metadata tree
# ntup_file: ntuple root file 
def getNEvents(ntup_file):
    files = glob.glob(ntup_file)
    n_events = 0
    
    for fname in files:
        f = ROOT.TFile(fname, "READ")
        h = f.Get("MetaData_EventCount")
        n_events +=  h.GetBinContent(2)
    
    return int(n_events)

# Returns the number of events from pyAMI
# ntup_file: ntuple root file
def getAMIEvents(ntup_file):
    amiCmd = 'ami command GetDatasetInfo -logicalDatasetName='+ntup_file+' | grep totalEvents'
    amiEvt =  os.popen(amiCmd).read().split('totalEvents = ')[1].split('\n')[0]
    return int(amiEvt)

fdata = open(dataList, 'r')
fout = open (strTyp+'missingEvents.txt', 'w')

totalEvt = [0,0,0,0]
totalAMI = [0,0,0,0]

for data in fdata:
    
    ntup = data.split('\n')[0]
    
    nAMI = getAMIEvents(ntup)
    
    procNtup = ntup.split('.physics_Main')[0]
   
    # check events per period in unmerged files
    if not(merged):
        nEvt = getNEvents(pathNtuples+'*'+procNtup+'*.root/*root')
        #nEvt = getNEvents(pathNtuples+procNtup.split('13TeV.')[0]+procNtup.split('13TeV.')[1]+'*root')

        outStr= procNtup+', missing events: '+str(nAMI-nEvt)+', processed events: '+str(nEvt)+', AMI events: '+str(nAMI)+'\n'
        fout.write(outStr)

    if procNtup.find('data15') > -1:
        if not(merged): totalEvt[0] += nEvt
        totalAMI[0] += nAMI
    elif procNtup.find('data16') > -1:
        if not(merged): totalEvt[1] += nEvt
        totalAMI[1] += nAMI
    elif procNtup.find('data17') > -1:
        if not(merged): totalEvt[2] += nEvt
        totalAMI[2] += nAMI
    elif procNtup.find('data18') > -1:
        if not(merged): totalEvt[3] += nEvt
        totalAMI[3] += nAMI
 
# only look at data15, data16, data17, data18
if merged:
    dataFiles = glob.glob(pathNtuples+'*data*root')
    
    for data in dataFiles:
        nEvt = getNEvents(data)
    
        if data.find('data15') > -1:
            totalEvt[0] = nEvt
        elif data.find('data16') > -1:
            totalEvt[1] = nEvt
        elif data.find('data17') > -1:
            totalEvt[2] = nEvt
        else:
            totalEvt[3] = nEvt

for i in range (0, len(totalEvt)):
    if i == 0: year = '2015'
    elif i == 1: year = '2016'
    elif i == 2: year = '2017'
    else: year = '2018'
    outStr= year+', missing events: '+str(totalAMI[i]-totalEvt[i])+', processed events: '+str(totalEvt[i])+', AMI events: '+str(totalAMI[i])+'\n'
    
    fout.write(outStr)

fdata.close()
fout.close()
