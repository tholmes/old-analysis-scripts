import os
import ROOT
import glob

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/region_defs.py")
execfile("../cosmics/cosmic_helpers.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

baseDir = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/"
mcName = "user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_string.DAOD_RPVLL_mc16a_v5.1_trees.root"


lifetime_bins = ["0p01", "0p1", "1"]
mass_bins = [50, 100]#, 200, 300, 400, 500, 600, 700, 800]

bins_l = len(lifetime_bins)
bins_m = len(mass_bins)

names = {}
for reg in regions:
    names[reg] = { "nb_x": bins_m, "low_x": 0, "high_x": 900, "label_x": "slepton mass [GeV]", "nb_y": bins_l, "low_y": -2, "high_y": 1, "label_y": "slepton lifetime [log(ns)]"}

kinds = ["SlepSlep","StauStau"]

h={}
for k in kinds:
    h[k] = {}
    h[k] = setHistos2D(k, names)

to_process = []
for l in lifetime_bins:
    for m in mass_bins:
        for k in kinds:
            to_process.append(k+"_directLLP_"+str(m)+"_0_"+l+"ns")
to_process.append("data")


counts = {}
for name in names:
    counts[name] = {}
    for p in to_process:
        counts[name][p] = 0


for p in to_process:

    print p

    weight = 1

    if "data" in p:
        files = glob.glob(baseDir+"/data/merged/data*.root")
        tree = ROOT.TChain("trees_SR_highd0_")
        for filename in files:
            tree.Add(filename)

    elif "directLLP" in p:
        files = glob.glob(baseDir+"/signal/*"+p+"*/*.root")
        tree = ROOT.TChain("trees_SR_highd0_")
        for filename in files:
            tree.Add(filename)

        if files == [] : continue
        dsid = files[0].split(".")[4]
        nevents = getNEvents(baseDir+"/signal/*"+p+"*/*.root")
        xs = getXS(dsid)
        lumi = 139000.0
        weight = xs*lumi/nevents
        print xs, lumi, nevents
        print weight

    else:
        print "what are you doing to me"
        continue

    print tree.GetEntries()
    nevents = 0
    for event in tree:
        if nevents % 10000 == 0: print "processing ", nevents
        #if nevents > 1000: break
        nevents+=1


        for name in names:
            if eval(regions[name]):
                counts[name][p]+=weight

print counts

for name in names:

    for m in mass_bins:
        for l in lifetime_bins:
            for k in ["StauStau","SlepSlep"]:

                p = k+"_directLLP_"+str(m)+"_0_"+l+"ns"


                print p
                print counts[name][p]
                print counts[name]["data"]
                frac = 999
                try:
                    frac =  counts[name][p]*1.0/counts[name]["data"]
                except:
                    frac = 0.000001
                print frac

                lbin = 0  #1ns
                if "0p01" in l: lbin = -2
                if "0p1"  in l: lbin = -1

                h[k][name].Fill(m, lbin, frac)




f = ROOT.TFile("contamination_plots/contamination_plots.root","RECREATE")
c = ROOT.TCanvas()

for k in ["StauStau","SlepSlep"]:
    for hist in h[k]:

        h[k][hist].Write()

        h[k][hist].SetMinimum(0)
        h[k][hist].SetMaximum(1)
        h[k][hist].GetZaxis().SetTitle("fraction signal contamination")


        ROOT.gStyle.SetPaintTextFormat("4.4f")
        ROOT.gStyle.SetPalette(ROOT.kBird)

        tmp = h[k][hist].Clone(k+hist+"_tmp")
        tmp.SetMarkerColor(ROOT.kBlack)
        h[k][hist].Draw("colz")
        tmp.Draw("text same")
        c.SaveAs("contamination_plots/"+k+"_"+hist+".pdf")


f.Write()
f.Close()













