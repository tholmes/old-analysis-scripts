/* Script to calculate duplicate events in a data file
 * Definitions of duplicate events are as follows:
 * 1. EvtGen duplicates: Events have identical truth values, but differing reconstructed values.     
 *    Not considered here
 * 2. DxAOD duplicates: Events have the same reconstructed value. Simply the same events are used twice
 * 3. Prodsys2 duplicates: Same event number used for multiple events but different event values.    
 */


#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1.h"
#include "TCut.h"
#include "TROOT.h"
#include "TTreeFormula.h"
#include "TString.h"
#include "TKey.h"
#include "TObject.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <TROOT.h>
#include "TF1.h"
#include <TSystem.h>
#include <TFile.h>
#include <TParameter.h>
#include <TH1.h>
#include <TH2.h>
#include <TTree.h>
#include <iostream>
#include <fstream>
#include <set>
#include <string>

int main(int argc , char** argv){

  bool openHelp=false;
  for(int k=0; k<argc; ++k){
    TString sargv = argv[k];
    if(sargv.Contains("-h") || sargv.Contains("--help")) openHelp=true;
 }
  if(argc<=1) openHelp=true;


  if(openHelp){
    std::cout << "--------------------------------------" << std::endl;
    std::cout << "Usage: get number of duplicate events for a data files" << std::endl;
    std::cout << " $1: Path to the input root file. Regular expression acceptable." << std::endl;
     std::cout << "--------------------------------------" << std::endl;
    return 0;
  }    

  TString path  = argv[1];  

  std::cout << "Processing file: " << path << std::endl;
  
  // get tree
  TFile *f = new TFile(path);
  TTree *t = (TTree*)f->Get("trees_SR_highd0_");

  int nevent = t->GetEntries();

  std::cout << "    Total number of entries: " << nevent <<std::endl;

  // branch declaration
  unsigned int runNumber;
  unsigned long long int eventNumber;
  double MET;
  int lepton_n;
  int lepton_sig;

  t->SetBranchAddress("runNumber", &runNumber);
  t->SetBranchAddress("eventNumber", &eventNumber);
  t->SetBranchAddress("MET", &MET);
  t->SetBranchAddress("lepton_n_baseline", &lepton_n);
  t->SetBranchAddress("lepton_n_signal", &lepton_sig);

  // variable declaration
  int dup[2] = {0,0};
  std::set<std::string> eventIds;
  std::set<double> metLs;

  // loop over events in the tree
  for (int i = 0; i < nevent; i++){

    if (i % 10000 == 0) std::cout << "Processing event: " << i << std::endl;

    t->GetEvent(i);

    // remove events with 0 leptons
    if (lepton_n < 2) continue;
    if (lepton_sig > 1) continue;

    ////////////////////////////////////////////////////
    ////// Type 2 duplication    
    /////////////////////////////////////////////////// 
    if (metLs.count(MET) == 0){
      metLs.insert(MET);
      //std::cout << runNumber << " " << eventNumber << " " << MET << std::endl;
    }
    else{
      dup[0] += 1;
      std::cout << runNumber << " " << eventNumber << " " << MET << std::endl;
    }

    ////////////////////////////////////////////////////
    ////// Type 3 duplication
    ///////////////////////////////////////////////////
    std::string text;
    text = std::to_string(runNumber) + " " + std::to_string(eventNumber);

    if (eventIds.count(text) == 0){
      eventIds.insert(text);
    }
    else{
      dup[1] += 1;
      std::cout << runNumber << " " << eventNumber  << std::endl;
    }
  }

  std::cout << "    Number of dxAOD duplicates: " << dup[0] << std::endl;
  std::cout << "    Number of prodSys duplicates: " << dup[1] << std::endl;
    
}

