# Goal: simple script to compare two versions of the same distribution

import glob
import ROOT
import os
import gc
gc.disable()

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/031519/"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119/"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/direct_compare/susy15"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/direct_compare/rpvll"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/direct_compare/susy15_rel70"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/reco_comp/user.tholmes.data18_13TeV.00349451.physics_Main.OldReco_v4_Comp_trees.root/*.root"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/reco_comp/user.tholmes.data18_13TeV.00349451.physics_Main.NewReco_v4_Comp_trees.root/*.root"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/data18.root"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4.1_Apr2_NewReco/data18.root"

#d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5_May14_OldReco/data/merged/data*.root"
#d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/data*.root"

d_new = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/20-01-08-ReprocessingTest/comparison/user.tholmes.data1*withDynAlign_trees.root/*.root"
d_old = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/20-01-08-ReprocessingTest/comparison/user.tholmes.data1*withoutDynAlign_trees.root/*.root"

# File type
f_search = ""
append = "_alignment_baseline_d03_2l"
#append = "reco"

#f_search = "data"
#append = "_data"


# Define grid
#lifetimes = ["0p01ns", "0p1ns", "1ns"]
#lifetimes = ["0p1ns"]
lifetimes = [""]
#masses = ["100", "300", "500"]
#masses = ["500"]
masses = [""]

# Channels
# 0 = ee, 1 = mm, 2 = em, 3 = me

# Labels
versions = ["old", "new"]
#labels = {"new": "DAOD_RPVLL", "old": "SUSY15"}
labels = {"new": "With Alignment", "old": "Without Alignment"}


variables = {
        "triggerRegion":        {"accessor": "tr",              "nbins": 6,     "xmin": 0.,     "xmax": 5.,     "xlabel": "Trigger Region",    "preSel": "1", "skipTrue": True, "logy": True},
        "year":                 {"accessor": "year",            "nbins": 4,     "xmin": 2014.5,   "xmax": 2018.5,     "xlabel": "Year",    "preSel": "1", "skipTrue": True, "logy": True},
        "electron_pt":          {"accessor": "electron_pt",     "nbins": 20,    "xmin": 0.,     "xmax": 400.,     "xlabel": "Electron p_{T} [GeV]",    "preSel": "1", "skipTrue": True, "logy": True},
        "muon_pt":              {"accessor": "muon_pt",         "nbins": 20,    "xmin": 0.,     "xmax": 400.,     "xlabel": "Muon p_{T} [GeV]",    "preSel": "1", "skipTrue": True, "logy": True},
        "electron_dpt":         {"accessor": "electron_dpt",    "nbins": 20,    "xmin": -1,    "xmax": 10.,     "xlabel": "Electron (p_{T}^{track} - p_{T})/p_{T}",  "preSel": "1", "skipTrue": True, "logy": True},
        "electron_absd0":       {"accessor": "electron_absd0",  "nbins": 20,    "xmin": 0.,   "xmax": 300.,       "xlabel": "Electron d_{0} [mm]",     "preSel": "1", "skipTrue": True, "logy": True},
        "muon_absd0":           {"accessor": "muon_absd0",      "nbins": 20,    "xmin": 0.,   "xmax": 300.,       "xlabel": "Muon d_{0} [mm]",     "preSel": "1", "skipTrue": True, "logy": True},
        "electron_eta":         {"accessor": "electron_eta",    "nbins": 20,    "xmin": -3.,   "xmax": 3.,       "xlabel": "Electron #eta",            "preSel": "1", "skipTrue": True, "logy": True},
        "muon_eta":             {"accessor": "muon_eta",        "nbins": 20,    "xmin": -3.,   "xmax": 3.,       "xlabel": "Muon #eta",            "preSel": "1", "skipTrue": True, "logy": True},
        "electron_phi":         {"accessor": "electron_phi",    "nbins": 20,    "xmin": -3.2,   "xmax": 3.2,     "xlabel": "Electron #phi",    "preSel": "1", "skipTrue": True, "logy": True},
        "muon_phi":             {"accessor": "muon_phi",        "nbins": 20,    "xmin": -3.2,   "xmax": 3.2,     "xlabel": "Muon #phi",    "preSel": "1", "skipTrue": True, "logy": True},

        #"muon_nPres":           {"accessor": "muon_nPres",  "nbins":20, "xmin": 0., "xmax": 20., "xlabel": "Number of Precision Hits", "preSel": "1", "skipTrue": True, "logy": True},
        #"photon_pt":          {"accessor": "photon_pt",     "nbins": 40,    "xmin": 0.,     "xmax": 400.,     "xlabel": "Photon p_{T} [GeV]",    "preSel": "1", "skipTrue": True, "logy": False},
        #"electron_n":           {"accessor": "electron_n",      "nbins": 10,    "xmin": 0.,     "xmax": 10.,     "xlabel": "Number of Electrons",    "preSel": "1", "skipTrue": True, "logy": False},
        #"electron_z0":          {"accessor": "electron_z0",     "nbins": 40,    "xmin": -500,   "xmax": 500.,     "xlabel": "Electron z_{0} [mm]",    "preSel": "1", "skipTrue": True, "logy": False},
        #"electron_passOR":      {"accessor": "electron_passOR", "nbins": 3,    "xmin": 0.,     "xmax": 3.,     "xlabel": "Electron passes OR",    "preSel": "1", "skipTrue": True, "logy": True},
        #"electron_nSi":         {"accessor": "electron_nSi",    "nbins": 20,    "xmin": 0.,     "xmax": 20.,     "xlabel": "Number of Electron Silicon Hits",    "preSel": "1", "skipTrue": True, "logy": False},
        #"electron_ptcone20":    {"accessor": "electron_ptcone20", "nbins": 40,    "xmin": 0.,    "xmax": 10.,     "xlabel": "Electron ptcone20",    "preSel": "1", "skipTrue": True, "logy": True},
        #"electron_ptvarcone20": {"accessor": "electron_ptvarcone20", "nbins": 40,    "xmin": 0.,    "xmax": 10.,     "xlabel": "Electron ptvarcone20",    "preSel": "1", "skipTrue": True, "logy": True},
        #"electron_IDtrack_absd0":       {"accessor": "electron_IDtrack_absd0", "nbins": 50,    "xmin": 0.,   "xmax": 50.,       "xlabel": "Electron ID Track d_{0} [mm]",     "preSel": "idTrack_pt>65", "skipTrue": True, "logy": False},
        #"electron_IDtrack_pt":         {"accessor": "electron_IDtrack_pt", "nbins": 40,    "xmin": 0.,   "xmax": 400.,       "xlabel": "Electron ID Track p_{T} [GeV]",     "preSel": "idTrack_pt>65", "skipTrue": True, "logy": False},
        #"electron_IDtrack_isLRT":      {"accessor": "electron_IDtrack_isLRT", "nbins": 3,    "xmin": 0.,     "xmax": 3.,     "xlabel": "Electron ID Track is LRT",    "preSel": "1", "skipTrue": True, "logy": True},
        #"electron_IDtrack_chi2":          {"accessor": "electron_IDtrack_chi2",     "nbins": 40,    "xmin": 0.,     "xmax": 10.,     "xlabel": "Electron ID Track chi2",    "preSel": "1", "skipTrue": True, "logy": False},
        #"track_absd0":       {"accessor": "track_absd0", "nbins": 50,    "xmin": 0.,   "xmax": 50.,       "xlabel": "ID Track d_{0} [mm]",     "preSel": "idTrack_pt>65", "skipTrue": True, "logy": False},
        #"track_pt":         {"accessor": "track_pt", "nbins": 40,    "xmin": 0.,   "xmax": 400.,       "xlabel": "ID Track p_{T} [GeV]",     "preSel": "idTrack_pt>65", "skipTrue": True, "logy": False},
        #"track_chi2":          {"accessor": "track_chi2",     "nbins": 40,    "xmin": 0.,     "xmax": 10.,     "xlabel": "ID Track chi2",    "preSel": "1", "skipTrue": True, "logy": False},
        #"track_isLRT":      {"accessor": "track_isLRT", "nbins": 3,    "xmin": 0.,     "xmax": 3.,     "xlabel": "ID Track is LRT",    "preSel": "1", "skipTrue": True, "logy": True},
        }


# Loop over samples
samples = {}
for lt in lifetimes:
    samples[lt] = {}
    for m in masses:
        samples[lt][m] = {}
        for version in versions:
            samples[lt][m][version] = {"hists": {}, "cans": {}, "mods": {}}

            d = d_new
            if version=="old": d = d_old

            print "\nWorking with", version, "point lt =",lt,"m =",m
            #f_string = "%s/*%s*_%s*%s*mc16d*trees.root/*.root"%(d, f_search, m, lt)
            f_string = "%s"%(d)
            files = glob.glob(f_string)
            print "Found", len(files), "files."
            samples[lt][m][version]["tree"] = getTree(f_string)
            samples[lt][m][version]["irdf"] = ROOT.ROOT.RDataFrame(samples[lt][m][version]["tree"])

            #print "Events in ntuple", tree.GetEntries()

            # Get crossection
            #xs = getXS(files[0].split("/")[-2].split(".")[3])
            #n_events = getNEvents(f_string)
            #print "Cross section:", xs, "N events:", n_events
            #ev_weight = lumi*xs/n_events
            print "Found", samples[lt][m][version]["irdf"].Count().GetValue(), "events."

            # Do basic event skimming (only ee events)
            #n_lep = "int n_lep = 0; for (int i=0; i<truthLepton_pt.size(); i++) { if ((abs(truthLepton_pdgId[i])==11 || abs(truthLepton_pdgId[i])==13) &&  truthLepton_pt[i] > 65) n_lep++; } return n_lep;"
            #df_ne = samples[lt][m][version]["irdf"].Define("n_true_lep", n_lep)
            #df_ee = df_ne.Filter("n_true_lep > 1", "dilepton selection")

            # Do basic event skimming (only 1e events)
            #df_ll = samples[lt][m][version]["irdf"].Filter("(electron_n == 1 && muon_pt.size()==0 && electron_pt[0]>50 && abs(electron_d0[0])>2) || (electron_n == 0 && muon_pt.size()==1 && muon_pt[0]>50 && abs(muon_IDtrack_d0[0])>2 )", "1l selection")
            #df_sigll = df_ll.Define("sig_e", "electron_n == 1 && electron_dpt[0]>-0.5 && electron_nMissingLayers[0] < 2 && electron_chi2[0] < 2 && electron_pt[0]>65 && abs(electron_d0[0])>3") \
            #                .Define("sig_m", "muon_pt.size() == 1 && !muon_cosmicTag_MV[0] && muon_hasMStrack[0] && abs(muon_eta[0]) < 2.5 && muon_hasIDtrack[0] && abs(muon_IDtrack_z0[0]) < 500 && muon_hasCBtrack[0] && abs(muon_IDtrack_d0[0]) < 300 && muon_pt[0] > 65   && muon_MStrack_nPres[0] > 2.9 && muon_FCTightTTVA[0] && abs(muon_IDtrack_d0[0]) > 3 && muon_isNotCosmic[0] && muon_IDtrack_chi2[0]<2 && muon_IDtrack_nMissingLayers[0]<2 && muon_CBtrack_chi2[0]<3") \
            #                .Filter("sig_e || sig_m", "sig 1l selection")
            #df_sigll = df_ll

            tr = '''int tr = -1;
            if( electron_n_baseline > 0 && electron_pt[0] > 160 ){
                tr = 0;
            }else if( electron_n_baseline > 1 && electron_pt[1]>60 ){
                tr = 1;
            }else if( muon_n_basline > 0 && ( (muon_pt[0]>60 && abs(muon_eta[0])<1.07) || (muon_n_basline > 1 && muon_pt[1]>60 && abs(muon_eta[1])<1.07) ) ){
                tr = 2;
            }
            return tr;'''
            trpass = '''int trpass = -0;
            if( electron_n_baseline > 0 && electron_pt[0] > 160 ){
                if (pass_HLT_g140_loose) trpass = 1;
            }else if( electron_n_baseline > 1 && electron_pt[1]>60 ){
                if (pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH) trpass = 1;
            }else if( muon_n_basline > 0 && ( (muon_pt[0]>60 && abs(muon_eta[0])<1.07) || (muon_n_basline > 1 && muon_pt[1]>60 && abs(muon_eta[1])<1.07) ) ){
                if (pass_HLT_mu60_0eta105_msonly) trpass = 1;
            }
            return trpass;'''
            year = '''int year = 2015;
            if (runNumber >= 348885) year = 2018;
            else if (runNumber > 311481) year = 2017;
            else if (runNumber > 284484) year = 2016;
            return year;'''

            # FIXME df_1l = samples[lt][m][version]["irdf"].Define("tr", tr).Define("trpass", trpass).Filter("lepton_n_baseline == 1 && abs(lepton_d0[0])>3 && trpass", "1l selection")
            df_1l = samples[lt][m][version]["irdf"].Define("year", year).Define("tr", tr).Define("trpass", trpass).Filter("lepton_n_baseline == 2 && abs(lepton_d0[0])>3 && abs(lepton_d0[1])>3 && lepton_n_signal<2 && trpass", "1l selection")
            #df_1l_sig = df_1l.Filter("lepton_n_signal == 1", "1l signal selection")
            df_1l_sig = df_1l

            # Define some variables
            '''
            track_absd0 = ' ' 'auto v = idTrack_d0; v.clear();
            for (int i=0; i<idTrack_d0.size(); i++)
                if (idTrack_pt[i] > 50) v.push_back((double)abs(idTrack_d0[i]));
            return v;' ' '
            electron_IDtrack_absd0 = track_absd0.replace("idTrack_d0", "electron_IDtrack_d0")
            track_pt = track_absd0.replace("idTrack_d0", "idTrack_pt")
            track_chi2 = track_absd0.replace("idTrack_d0", "idTrack_chi2")
            track_isLRT = track_absd0.replace("idTrack_d0", "idTrack_isLRT")
            df_td0 = df_ee.Define("track_absd0", track_absd0).Define("electron_absd0", electron_absd0).Define("track_pt", track_pt).Define("track_chi2", track_chi2).Define("track_isLRT", track_isLRT).Define("electron_IDtrack_absd0", electron_IDtrack_absd0)
            '''
            electron_absd0 = '''auto v = electron_d0; v.clear();
            for (int i=0; i<electron_d0.size(); i++)
                v.push_back((double)abs(electron_d0[i]));
            return v;'''
            muon_absd0 = electron_absd0.replace("electron", "muon_IDtrack")
            df_d0 = df_1l_sig.Define("electron_absd0", electron_absd0).Define("muon_absd0", muon_absd0)

            # Plot
            samples[lt][m][version]["rdf"] = df_d0
            #df = df_td0

            print version
            for v in variables:
                print "Making plots for", v

                samples[lt][m][version]["hists"][v] = {}
                samples[lt][m][version]["mods"][v] = {}
                samples[lt][m][version]["cans"][v] = {}

                # Get distributions for all selections and make efficiencies
                samples[lt][m][version]["mods"][v]["reco"] = ROOT.RDF.TH1DModel("h_%s_%s"%(v,version), labels[version], variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
                samples[lt][m][version]["hists"][v]["reco"] = samples[lt][m][version]["rdf"].Histo1D(samples[lt][m][version]["mods"][v]["reco"], variables[v]["accessor"])

                if not variables[v]["skipTrue"]:

                    # Get true distributions
                    accessor = variables[v]["accessor"]
                    accessor = accessor.replace("electron", "truthLepton").replace("absd0", "d0")
                    samples[lt][m][version]["mods"][v]["true"] = ROOT.RDF.TH1DModel("h_true_%s_%s"%(v,version), labels[version], variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
                    samples[lt][m][version]["hists"][v]["true"]     = samples[lt][m][version]["rdf"].Histo1D(samples[lt][m][version]["mods"][v]["true"], accessor)

                    samples[lt][m][version]["hists"][v]["eff"] = samples[lt][m][version]["hists"][v]["reco"].Clone(samples[lt][m][version]["hists"][v]["reco"].GetName()+"_eff")
                    samples[lt][m][version]["hists"][v]["eff"].Divide(samples[lt][m][version]["hists"][v]["eff"], samples[lt][m][version]["hists"][v]["true"].Clone("h_true_%s_%s_%s"%(m,lt,v)), 1, 1,"B")


        # Draw plots
        for t in ["reco", "true", "eff"]:
            print "Plot type:", t
            for v in variables:

                if variables[v]["skipTrue"] and t in ["true", "eff"]: continue
                print v

                # Make simple ratio plots
                makeRatioPlot(samples[lt][m]["new"]["hists"][v][t].Clone(), samples[lt][m]["old"]["hists"][v][t].Clone(), xname=variables[v]["xlabel"], savename="plots/r_%s_%s.pdf"%(v, append), logy=variables[v]["logy"], rmin=0.8, rmax=1.2)
                continue
                '''

                can_name = "c_%s_%s_%s_%s"%(m, lt, v, t)
                samples[lt][m]["old"]["cans"][v][t] = ROOT.TCanvas(can_name, can_name)

                for i, version in enumerate(samples[lt][m]):
                    samples[lt][m][version]["hists"][v][t].SetLineColor(theme_colors2[i])
                    samples[lt][m][version]["hists"][v][t].SetMarkerColor(theme_colors2[i])
                    if i==0:
                        samples[lt][m][version]["hists"][v][t].SetMinimum(0)
                        if variables[v]["logy"]:
                            samples[lt][m][version]["hists"][v][t].SetMinimum(0.1)
                            samples[lt][m][version]["hists"][v][t].SetMaximum(1e10)
                        elif t=="eff":
                            samples[lt][m][version]["hists"][v][t].SetMaximum(1.5)
                        else:
                            samples[lt][m][version]["hists"][v][t].SetMaximum(samples[lt][m][version]["hists"][v][t].GetMaximum()*2)
                        samples[lt][m][version]["hists"][v][t].GetXaxis().SetTitle(variables[v]["xlabel"])
                        if t=="eff": samples[lt][m][version]["hists"][v][t].GetYaxis().SetTitle("Efficiency")
                        elif t=="reco": samples[lt][m][version]["hists"][v][t].GetYaxis().SetTitle("Reconstructed Electrons")
                        elif t=="true": samples[lt][m][version]["hists"][v][t].GetYaxis().SetTitle("True Electrons")
                        samples[lt][m][version]["hists"][v][t].Draw("ehist")# PLC PMC")
                    else:
                        samples[lt][m][version]["hists"][v][t].Draw("same ehist")# PLC PMC")

                # Set log Y
                if variables[v]["logy"]: samples[lt][m]["old"]["cans"][v][t].SetLogy()

                leg = samples[lt][m]["old"]["cans"][v][t].BuildLegend(.55,.75,.8,.90)
                leg.Draw()

                ROOT.ATLASLabel(0.2,0.85, "Internal")
                text = ROOT.TLatex()
                text.SetNDC()
                text.SetTextSize(0.04)
                #text.DrawLatex(0.2,0.78, "Simulation, 140 fb^{-1}")
                text.DrawLatex(0.2,0.78, "Data 2018")
                text.DrawLatex(0.2,0.73, "CR-1l")

                #raw_input("...")

                print "Saving as", can_name+append+".pdf"
                samples[lt][m]["old"]["cans"][v][t].SaveAs(can_name+append+".pdf")
                '''
            ## Cutflow for fun
            print labels["old"]
            report = samples[lt][m]["old"]["rdf"].Report()
            report.Print()
            print labels["new"]
            report = samples[lt][m]["new"]["rdf"].Report()
            report.Print()

