#!/usr/bin/env python
import glob
import csv
import ROOT
import os
from math import *
from array import array
execfile("../plot_helpers/basic_plotting.py")

#ROOT.gStyle.SetLegendBorderSize(0)
#ROOT.gROOT.SetBatch(0)
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.TH1.SetDefaultSumw2()

years = {
        2015: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        2016: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        2017: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root ",
        2018: "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root",
}

select_runs = True
selected_runs = [284427, 304178, 340368, 358031]

#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3.3_Nov2019_DVs/already_merged/"
file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/20-01-08-ReprocessingTest/"

triggers = "(pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH || pass_HLT_g140_loose || pass_HLT_mu60_0eta105_msonly)"
#triggers = "(pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH)"

regions = {
        "trigger": triggers,
        "2l":  "lepton_pt[0]>50 && lepton_pt[1]>50 && %s"%triggers,
        #"2l, d0 > 2mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2"%triggers,
        #"2l, d0, ee": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && electron_n==2"%triggers,
        #"2l, d0, mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && muon_n==2"%triggers,
        "1e": "lepton_n==1 && electron_n==1 && electron_pt[0]>50 && electron_dpt[0]>-0.5 && electron_chi2[0]>2 && abs(electron_d0)>2 && electron_nMissingLayers[0]<1.1",
        "1m": "lepton_n==1 && muon_n==1 && muon_CBtrack_chi2[0]<3 && muon_pt[0]>50 && abs(muon_IDtrack_d0[0])>2",
        "2e/g": "muon_n==0 && (electron_n + photon_n)==2 && %s"%triggers,
        "1e+g": "lepton_n==1 && electron_n==1 && electron_pt[0]>50 && electron_dpt[0]>-0.5 && electron_chi2[0]>2 && abs(electron_d0)>2 && electron_nMissingLayers[0]<1.1 && photon_n==1"
        }
ordered_regions = ["2l", "2l, d0, ee", "2l, d0, mm", "1e", "1m", "2e/g"] #, "2l+2j", "Low SR", "Med SR", "High SR"]
ordered_regions = ["2l", "1e", "1m", "2e/g", "1e+g"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

# Index the runs by bin number
if select_runs:
    runs = []
    run_candidates = glob.glob(file_loc+"/*.root")
    for r in run_candidates:
        for rn in selected_runs:
            if str(rn) in r: runs.append(r)
else:
    runs = glob.glob(file_loc+"/*.root")
run_bins = {}
i = 0
for run in sorted(runs):
    i+=1
    run_bins[run] = i

# Set up histograms
h_lumis = ROOT.TH1F("h_lumis", "h_lumis", len(run_bins), 0, len(run_bins)-1)
h_passed = {}
h_frac = {}
for reg in regions.keys():
    h_passed[reg] = ROOT.TH1F("h_passed_"+reg, "h_passed_"+reg, len(run_bins), 0, len(run_bins)-1)
    h_frac[reg] = ROOT.TH1F(reg, reg, len(run_bins), 0, len(run_bins)-1)

# Get data for each run
for run in runs:
    year = int("20" + run.split("data")[1].split("_")[0])
    print year, ":", run
    runid = int(run.split("13TeV.")[1].split(".")[0])

    # Read in run luminosities
    lumi_file = years[year]
    lumis = {}
    f = ROOT.TFile(lumi_file, "READ")
    h = f.Get("run%d_intlumi"%runid)
    lumi = h.GetMaximum()
    lumierr = h.GetBinError(h.FindBin(h.GetMaximum()))
    print lumi
    h_lumis.SetBinContent(run_bins[run], lumi)
    h_lumis.SetBinError(run_bins[run], lumierr)

    '''
    all_objs = f.GetListOfKeys()
    for obj in all_objs:
        if "_intlumi" in obj.GetName():
            runid = int(obj.GetName().split("_")[0].strip("run"))
            hist = f.Get(obj.GetName())
            lumi = hist.GetMaximum() # Gives lumi in microb-1
            lumis[runid] = lumi/1000000 # Gives lumi in pb-1
    runid = int(run.split("13TeV.")[1].split(".")[0])
    print lumis[runid]
    h_lumis.SetBinContent(run_bins[run], lumis[runid])
    h_lumis.SetBinError(run_bins[run], lumis[runid]*.1)
    '''

    # Fill passing histograms
    t = getTree(run+"/*.root")
    for reg in regions.keys():
        #print run
        #print runid
        h_temp = getHist(t, "h_temp_%s_%d"%(reg, runid), "runNumber", binstr = "1,%d,%d"%(runid,runid+1), sel_name=regions[reg])
        #print reg
        #c = ROOT.TCanvas("can", "can")
        #h_temp.Draw()
        #raw_input("...")
        n_passed = h_temp.GetBinContent(1)
        print n_passed
        h_passed[reg].SetBinContent(run_bins[run], n_passed)
        h_passed[reg].SetBinError(run_bins[run], h_temp.GetBinError(1))

        h_frac[reg].GetXaxis().SetBinLabel(run_bins[run], str(runid))
        h_passed[reg].GetXaxis().SetBinLabel(run_bins[run], str(runid))
        h_lumis.GetXaxis().SetBinLabel(run_bins[run], str(runid))

for reg in regions.keys():
    h_frac[reg].Divide(h_passed[reg], h_lumis)

# Draw all
hists = []
for reg in ordered_regions:
    hists += [h_frac[reg]]
for h in hists:
    h.GetXaxis().SetTitle("Run Number")
    h.GetYaxis().SetTitle("Events Passed/Luminosity")
h_lumis.GetXaxis().SetTitle("Run Number")
h_lumis.GetYaxis().SetTitle("Luminosity")

drawTwoPads(hists, h_lumis, savename="plots/runDependence_fewRuns.pdf", logTop=True, logBottom=False)
raw_input("...")


'''

# Make plot with all? fractions
c = ROOT.TCanvas("c", "c")
#c.SetLogy()
colors = [ROOT.kAzure+2, ROOT.kGreen+2, ROOT.kPink+4, ROOT.kOrange+10, ROOT.kOrange, ROOT.kSpring+7]
leg = ROOT.TLegend(.75, .65, .85, .85)

hists = []
for reg in regions.keys(): hists += [h_frac[reg]]
#scaleHists(hists)
ymax = 10*getMaximum(hists)
ymin = .001

h_frac["2l on Z"].Draw()

h_lumis.SetMaximum(100*h_lumis.GetMaximum())
h2max = h_lumis.GetMaximum()
scale = ymax/h2max
h_lumis.Scale(scale)

f3 = ROOT.TF1("f3","log10(x)",ymin/scale,h2max);
axis = ROOT.TGaxis(len(run_bins.keys())-1, ymin, len(run_bins.keys())-1, ymax, "f3",510,"G")
axis.SetTitle("Luminosity (pb-1)")
axis.SetTitleOffset(-1.2)
axis.SetTitleSize(.03)
axis.SetLabelSize(.03)
axis.Draw()

h_lumis.SetFillStyle(3003)
h_lumis.SetFillColor(ROOT.kBlue-9)
h_lumis.SetLineColor(ROOT.kBlue-9)
#h_lumis.Scale(1./h_lumis.GetMaximum())
h_lumis.Draw("histsame")

i=0
for reg in ["2l on Z", "2l+2j on Z", "VRT", "VRZ", "CRT", "SRZ"]:
    h_frac[reg].SetLineWidth(2)
    h_frac[reg].SetLineColor(colors[i])
    h_frac[reg].SetMaximum(ymax)
    h_frac[reg].SetMinimum(ymin)
    h_frac[reg].GetXaxis().SetTitle("Run Number")
    h_frac[reg].GetXaxis().SetTitleOffset(2.4)
    h_frac[reg].GetYaxis().SetTitle("Events Passed / Luminosity")
    #if i==0: h_frac[reg].Draw()
    #else: h_frac[reg].Draw("same")
    h_frac[reg].Draw("same")
    leg.AddEntry(h_frac[reg], reg, "l")
    i+=1

c.SetLogy()
leg.Draw()
c.Update()
raw_input("...")
c.SaveAs("plots/run_dependence.png")

# Make plot with all? fractions
c2 = ROOT.TCanvas("c2", "c2")
h_lumis.Draw()
raw_input("...")

c1 = ROOT.TCanvas("c1", "c1")
c1.SetLogy()
leg = ROOT.TLegend(.7, .7, .9, .9)

hists = []
for reg in regions.keys(): hists += [h_frac[reg]]
#scaleHists(hists)
ymax = 1000*getMaximum(hists)

i=0
for reg in regions.keys():
    h_passed[reg].SetLineWidth(2)
    h_passed[reg].SetLineColor(colors[i])
    h_passed[reg].SetMaximum(ymax)
    h_passed[reg].SetMinimum(.001)
    h_passed[reg].GetXaxis().SetTitle("Run Number")
    h_passed[reg].GetYaxis().SetTitle("Events Passed")
    if i==0: h_passed[reg].Draw()
    else: h_passed[reg].Draw("same")
    leg.AddEntry(h_passed[reg], reg, "l")
    i+=1

leg.Draw()
raw_input("...")
#c1.SaveAs("plots/run_dependence.png")

'''


