#!/usr/bin/env python
import glob
import csv
import ROOT
import os
import collections
from math import *
from array import array
execfile("../plot_helpers/basic_plotting.py")

#ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.TH1.SetDefaultSumw2()

variables = {
        "mu":       {"acc": "averageInteractionsPerCrossing", "label": "<#mu>", "nbins":500, "xmin":0, "xmax":80 },
        "lep0_pt":  {"acc": "lepton_pt[0]",     "label": "leading lepton p_{T} [GeV]",  "nbins":50, "xmin":0, "xmax":300 },
        "lep0_phi": {"acc": "lepton_phi[0]",    "label": "leading lepton #phi",         "nbins":50, "xmin":-3.14, "xmax":3.14 },
        "lep0_eta": {"acc": "lepton_eta[0]",    "label": "leading lepton #eta",         "nbins":50, "xmin":-2.5, "xmax":2.5 },
        "MET_phi":  {"acc": "MET_phi",          "label": "E_{T}^{miss} #phi",           "nbins":50, "xmin":-3.14, "xmax":3.14 },
        }
years = [2016] #, 2016, 2017, 2018]

file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data_wip/Data/"

triggers = "(pass_HLT_2g50_loose || pass_HLT_2g50_loose_L12EM20VH || pass_HLT_g140_loose || pass_HLT_mu60_0eta105_msonly)"
regions = {
        "trigger": triggers,
        "2l":  "lepton_pt[0]>50 && lepton_pt[1]>50 && %s"%triggers,
        #"2l, d0 > 2mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2"%triggers,
        #"2l, d0, ee": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && electron_n==2"%triggers,
        #"2l, d0, mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && muon_n==2"%triggers,
        "1 signal e": "lepton_n==1 && electron_n==1 && electron_pt[0]>50 && electron_dpt[0]>-0.5 && electron_chi2[0]<2 && abs(electron_d0)>2 && electron_nMissingLayers[0]<1.1",
        "1 signal m": "lepton_n==1 && muon_n==1 && muon_CBtrack_chi2[0]<3 && muon_pt[0]>50 && abs(muon_IDtrack_d0[0])>2",
        }
ordered_regions = ["2l", "2l, d0 > 2mm", "2l, d0, ee", "2l, d0, mm", "1e", "1m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]
ordered_regions = ["trigger", "2l", "1 signal e", "1 signal m"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

for year in years:

    # Fill passing histograms
    fname = "data%d*.root"%(year%2000)
    print fname
    t = getTree(file_loc+fname)

    # Fill yield as a function of mu
    for var in variables:
        h_var = collections.OrderedDict()
        for reg in ordered_regions:
            h_var[reg] = getHist(t, reg, variables[var]["acc"], binstr="%d,%d,%d"%(variables[var]["nbins"],variables[var]["xmin"],variables[var]["xmax"]), sel_name=regions[reg])

        # Draw all hists for var
        for h in ordered_regions:
            h_var[h].GetXaxis().SetTitle(variables[var]["label"])
            h_var[h].GetYaxis().SetTitle("Events Passed")

        plotHistograms(h_var, "plots/h_%s_dist_%d.pdf"%(var, year), xlabel=variables[var]["label"], ylabel="Events Passed", interactive=False, logy=True, atltext=["Internal", "%d Data"%year])
