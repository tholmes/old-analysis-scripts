#!/usr/bin/env python
import glob
import csv
import ROOT
import os
import collections
from math import *
from array import array
execfile("../plot_helpers/basic_plotting.py")

#ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetOptTitle(0)
ROOT.TH1.SetDefaultSumw2()

variables = {
        #"mu":       {"acc": "averageInteractionsPerCrossing", "label": "<#mu>", "nbins":500, "xmin":0, "xmax":80 },
        #"lep0_pt":  {"acc": "lepton_pt[0]",     "label": "leading lepton p_{T} [GeV]",  "nbins":50, "xmin":0, "xmax":300 },
        "lep0_phi": {"acc": "lepton_phi[0]",    "label": "leading lepton #phi",         "nbins":50, "xmin":-3.14, "xmax":3.14 },
        "lep0_eta": {"acc": "lepton_eta[0]",    "label": "leading lepton #eta",         "nbins":50, "xmin":-2.5, "xmax":2.5 },
        #"MET_phi":  {"acc": "MET_phi",          "label": "E_{T}^{miss} #phi",           "nbins":50, "xmin":-3.14, "xmax":3.14 },
        }
years = [""] #, 2016, 2017, 2018]

#file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/data/"
file_loc = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/data/merged/"

triggers = "triggerRegion_pass"
regions = {
        #"trigger": triggers,
        #"2l":  "lepton_pt[0]>50 && lepton_pt[1]>50 && %s"%triggers,
        #"2l, d0 > 2mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2"%triggers,
        #"2l, d0, ee": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && electron_n==2"%triggers,
        #"2l, d0, mm": "lepton_pt[0]>50 && lepton_pt[1]>50 && %s && abs(lepton_d0[0])>2 && abs(lepton_d0[1])>2 && muon_n==2"%triggers,
        "1signale": "lepton_n_baseline==1 && electron_n_signal==1",
        "1signalm": "lepton_n_baseline==1 && muon_n_signal==1 && abs(muon_t0avg[0])<30",
        "1baselinee": "lepton_n_baseline==1 && electron_n_baseline==1",
        "1baselinem": "lepton_n_baseline==1 && muon_n_basline==1",
        }
ordered_regions = ["1baselinee", "1baselinem"] #, "2l+2j", "Low SR", "Med SR", "High SR"]

for year in years:

    # Fill passing histograms
    fname = "data%s*.root"%(year)
    print fname
    t = getTree(file_loc+fname)

    for reg in ordered_regions:
        var1 = "lep0_eta"
        var2 = "lep0_phi"
        varstr = "%s:%s"%(variables[var2]["acc"], variables[var1]["acc"])
        binstr = "%f,%f,%f,%f,%f,%f"%(variables[var1]["nbins"],variables[var1]["xmin"],variables[var1]["xmax"],variables[var2]["nbins"],variables[var2]["xmin"],variables[var2]["xmax"])
        h = getHist(t, reg, varstr, binstr=binstr, sel_name=regions[reg])
        h.GetXaxis().SetTitle(variables[var1]["label"])
        h.GetYaxis().SetTitle(variables[var2]["label"])
        c = ROOT.TCanvas("c_%s"%reg, "c")
        h.Draw("colz")
        c.SaveAs("plots/h_%s_%s_%s.pdf"%(var1,var2,reg))
