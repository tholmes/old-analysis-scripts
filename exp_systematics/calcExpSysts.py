
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
ROOT.ROOT.EnableImplicitMT()
ROOT.gStyle.SetPalette(ROOT.kBird)
ROOT.gStyle.SetPaintTextFormat(".2f");

# Define grid
sigtypes = ["SlepSlep", "StauStau"]
lifetimes = ["0p01", "0p1", "1"]
masses = ["50", "100", "200", "300", "400", "500", "600", "700", "800"]
#lifetimes = ["0p01"]
#masses = ["100"]

append = "_v5.1"
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/ExpSysts/"
testfile = "user.tholmes.mc16_13TeV.399021.MGPy8EG_A14NNPDF23LO_StauStau_directLLP_500_0_0p01ns.DAOD_RPVLL_mc16a_expSystv2_trees.root/user.tholmes.399021.e6633_e5984_s3307_r11915_r11748.21736085._000001.trees.root"
sr = "triggerRegion_pass && lepton_n_signal>1 && lepton_isSignal[0] && lepton_isSignal[1] && deltaR>0.2 && muon_n_cosmic==0"

# Get list of variations
treename = "trees_SR_highd0_"
fname = os.path.join(d, testfile)
f = ROOT.TFile(fname, "READ")
keys = f.GetListOfKeys()
variations = []
for k in keys:
    if k.GetName().startswith(treename):
        variations.append(k.GetName())

# For each point, make a plot of relative change for each variation
hists = {}
for st in sigtypes:
    hists[st] = {}
    for v in variations:
        hists[st][v] = ROOT.TH2F("h_%s_%s"%(st,v), "h_%s_%s"%(st,v), 17, 0, 850, 3, -2., 1.)

for st in sigtypes:
    for lt in lifetimes:
        for m in masses:
            print "Working with", st, " mass", m, "and lifetime", lt

            for v in variations:
                if "MUON" not in v: continue
                print v
                t = getTree("%s/*%s*_%s_*_%sns*_trees.root/*.root"%(d,st,m,lt), v)
                if t==-1: break

                # Set up grid point
                mass = int(m)
                lifetime = 0
                if lt == "0p01": lifetime = -2
                elif lt == "0p1": lifetime = -1
                elif lt == "1": lifetime = 0
                ev_weight = 1

                # For debugging
                #if "EG" in v or v == treename:
                #    print v
                #    for event in t:
                #        if event.electron_n_baseline>0:
                #            print event.electron_pt[0]
                #            break

                h_pass = getHist(t, "h_ev_%s_%s_%s_%s_pass"%(st,m,lt,v), "lepton_isSignal[0]", sel_name="%f*mcEventWeight*(%s)"%(ev_weight,sr))
                hists[st][v].Fill(mass, lifetime, h_pass.Integral())
                print "Passing:", h_pass.Integral(), "N events:", h_pass.GetEntries()

for st in sigtypes:
    for v in variations:
        if v == treename: continue

        h = hists[st][v].Clone()
        h.Divide(hists[st][treename])

        h.GetXaxis().SetTitle("slepton mass [GeV]")
        h.GetYaxis().SetTitle("slepton lifetime [log10(ns)]")
        h.SetMinimum(0.8)
        h.SetMaximum(1.2)

        can = ROOT.TCanvas("can_%s_%s"%(st,v), "can_%s_%s"%(st,v))
        h.Draw("colz")
        h_temp = h.Clone()
        h_temp.SetMarkerSize(2)
        h_temp.SetMarkerColor(ROOT.kWhite)
        h_temp.Draw("text90 same")

        can.SaveAs("plots/h_%s_%s%s.pdf"%(st,v,append))


