
import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(0)
ROOT.ROOT.EnableImplicitMT()

# Define grid
#lifetimes = ["0p01", "0p1", "1"]
#masses = ["50", "100", "200", "300", "400", "500", "700"]
lifetimes = ["1"]
masses = ["300"]

# Hand-done cross-sections [pb], per flavor
lumi = 100000
cross_sections = {
        "50": 5.4825,
        "100": 0.3673,
        "200": 0.02996,
        "300": 0.00611,
        "400": 0.00181,
        "500": 0.000667,
        "600": 0.000275,
        "700": 0.000247/2,
        "800": 0.000112/2,
        }

# Set up hists for acceptance
# Plot: for the first IDtrack, look at difference from truth d0
# Plot: for the best fit (non-first) IDtrack, look at difference from truth d0
SRs = ["ee"]

hists = {}
for sr in SRs:
    hists[sr] = {}
    hists[sr]["first_Dd0"] =   ROOT.TH1F(sr+"_first_Dd0", sr+"_first_Dd0", 100, -100, 100)
    hists[sr]["best_Dd0"] =    ROOT.TH1F(sr+"_best_Dd0", sr+"_best_Dd0", 100, -100, 100)
    hists[sr]["best_d0"] =    ROOT.TH1F(sr+"_best_d0", sr+"_best_d0", 100, 0, 100)
    hists[sr]["true_d0"] =    ROOT.TH1F(sr+"_true_d0", sr+"_true_d0", 100, 0, 100)
    hists[sr]["first_d0"] =    ROOT.TH1F(sr+"_first_d0", sr+"_first_d0", 100, 0, 100)
    hists[sr]["first_Dd0_v_true_d0"] =    ROOT.TH2F(sr+"_first_Dd0_v_true_d0", sr+"_first_Dd0_v_true_d0", 100, 0, 100, 60,-2,10)
    hists[sr]["best_Dd0_v_true_d0"] =    ROOT.TH2F(sr+"_best_Dd0_v_true_d0", sr+"_best_Dd0_v_true_d0", 100, 0, 100, 60,-2,10)
    hists[sr]["first_d0_v_true_d0"] =    ROOT.TH2F(sr+"_first_d0_v_true_d0", sr+"_first_d0_v_true_d0", 100, 0, 100, 100,0,100)
    hists[sr]["best_d0_v_true_d0"] =    ROOT.TH2F(sr+"_best_d0_v_true_d0", sr+"_best_d0_v_true_d0", 100, 0, 100, 100,0,100)

# Loop over files and make plots
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/pre_IDtrk_fix"
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"

#append = "_largeR"
append = "_d0match"

for lt in lifetimes:
    for m in masses:

        print "Working with mass", m, "and lifetime", lt
        t = getTree("%s/*_%s_*_%sns*/*.root"%(d,m,lt))
        if t==-1: continue

        # Normalize
        n_events = t.GetEntries()
        ev_weight = lumi*2.*cross_sections[m]/n_events

        n_events=0
        for event in t:

            if n_events%5000==0: print "Processing event", n_events
            n_events += 1

            #if n_events>10: break

            n_electrons = len(event.electron_pt)
            if n_electrons==0: continue

            #print "\nEvent num:", n_events
            for i in range(n_electrons):

                # Find a truth match
                barcode = event.electron_truthMatchedBarcode[i]
                true_i = -1
                for j in range(len(event.truthLepton_barcode)):
                    if event.truthLepton_barcode[j] == barcode:
                        true_i = j
                        break
                if true_i == -1:
                    #print "Couldn't find truth match."
                    continue

                # Only look at large R electrons
                #R = (event.truthLepton_VtxX[true_i]*event.truthLepton_VtxX[true_i]+event.truthLepton_VtxY[true_i]*event.truthLepton_VtxY[true_i])**(1./2)
                #if R<150:continue



                #print "Found passing lepton, d0 =", event.truthLepton_d0[true_i]

                # Compare true d0 to different ID tracks
                true_d0 = event.truthLepton_d0[true_i]
                true_pt = event.truthLepton_pt[true_i]
                index_for_id_track = 0
                for index in xrange(i):
                    index_for_id_track += event.electron_nTracks[index]
                first_d0 = abs(event.electron_IDtrack_d0[index_for_id_track])

                if true_d0 == 0: continue

                best_d0_i = index_for_id_track
                best_pt_i = index_for_id_track
                for j in range(len(event.electron_IDtrack_d0)):
                    #print "j=", j, "d0=", event.electron_IDtrack_d0[j]
                    if abs(true_d0 - abs(event.electron_IDtrack_d0[j])) < abs(true_d0 - abs(event.electron_IDtrack_d0[best_d0_i])):
                        #print "\nj=", j
                        #print "diff here:", abs(true_d0 - event.electron_IDtrack_d0[j])
                        #print "old best :", abs(true_d0 - event.electron_IDtrack_d0[best_d0])
                        best_d0_i = j
                    if abs(true_pt - event.electron_IDtrack_pt[j]) < abs(true_pt - event.electron_IDtrack_pt[best_pt_i]): best_pt_i = j

                #print "True d0:", true_d0
                #print "First d0:", first_d0
                #print "Best d0:", event.electron_IDtrack_d0[best_d0_i], "(index = %d)"%best_d0_i

                #if best_d0_i != best_pt_i:
                #    print "Found different results for pt and d0. Skipping for now."
                #    continue
                best_d0 = abs(event.electron_IDtrack_d0[best_d0_i])
                #best_d0 = abs(event.electron_IDtrack_d0[best_pt_i])

                hists[sr]["true_d0"].Fill(true_d0)
                hists[sr]["best_d0"].Fill(best_d0)
                hists[sr]["first_d0"].Fill(first_d0)

                hists[sr]["first_Dd0"].Fill(abs(first_d0 - true_d0)/true_d0)
                hists[sr]["best_Dd0"].Fill(abs(best_d0 - true_d0)/true_d0)

                hists[sr]["first_Dd0_v_true_d0"].Fill(true_d0, abs(first_d0 - true_d0)/true_d0)
                hists[sr]["best_Dd0_v_true_d0"].Fill(true_d0, abs(best_d0 - true_d0)/true_d0)
                hists[sr]["first_d0_v_true_d0"].Fill(true_d0, first_d0)
                hists[sr]["best_d0_v_true_d0"].Fill(true_d0, best_d0)
'''
# Plot various d0s on top of one another
h = {   "true d0": hists[sr]["true_d0"],
        "best d0": hists[sr]["best_d0"],
        "first d0": hists[sr]["first_d0"],}
plotHistograms(h, "plots/idtrk_selection_d0.pdf", xlabel="d_0 [mm]", ylabel="", interactive=True, logy=False)

can1 = ROOT.TCanvas("can1", "can1")
hists[sr]["first_d0"].SetLineColor(ROOT.kRed)
hists[sr]["first_d0"].Draw()
hists[sr]["true_d0"].Draw("same")
hists[sr]["best_d0"].SetLineColor(ROOT.kCyan)
hists[sr]["best_d0"].Draw("same")
raw_input("...3")

can2 = ROOT.TCanvas("can2", "can2")
hists[sr]["first_Dd0_v_true_d0"].Draw("colz")
hists[sr]["first_Dd0_v_true_d0"].GetXaxis().SetTitle("true d_{0} [mm]")
hists[sr]["first_Dd0_v_true_d0"].GetYaxis().SetTitle("#Delta d_{0} / true d_{0} for first ID track")
raw_input("... first")
can2.SaveAs("plots/first_Dd0_v_true_d0.pdf")

can3 = ROOT.TCanvas("can3", "can3")
hists[sr]["best_Dd0_v_true_d0"].Draw("colz")
hists[sr]["best_Dd0_v_true_d0"].GetXaxis().SetTitle("true d_{0} [mm]")
hists[sr]["best_Dd0_v_true_d0"].GetYaxis().SetTitle("#Delta d_{0} / true d_{0} for best ID track")
raw_input("... best")
can3.SaveAs("plots/best_Dd0_v_true_d0.pdf")
'''

can4 = ROOT.TCanvas("can4", "can4")
hists[sr]["first_d0_v_true_d0"].Draw("colz")
hists[sr]["first_d0_v_true_d0"].GetXaxis().SetTitle("true d_{0} [mm]")
hists[sr]["first_d0_v_true_d0"].GetYaxis().SetTitle("first d_{0} [mm]")
#raw_input("... first")
can4.SaveAs("plots/first_d0_v_true_d0"+append+".pdf")

can5 = ROOT.TCanvas("can5", "can5")
hists[sr]["best_d0_v_true_d0"].Draw("colz")
hists[sr]["best_d0_v_true_d0"].GetXaxis().SetTitle("true d_{0} [mm]")
hists[sr]["best_d0_v_true_d0"].GetYaxis().SetTitle("best d_{0} [mm]")
#raw_input("... best")
can5.SaveAs("plots/best_d0_v_true_d0"+append+".pdf")

'''
for sr in SRs:

    print sr

    hists[sr]["pass"].GetXaxis().SetTitle("slepton mass [GeV]")
    hists[sr]["pass"].GetYaxis().SetTitle("slepton lifetime [log(ns)]")
    hists[sr]["pass"].SetMinimum(.001)
    hists[sr]["pass"].SetMaximum(10000)

    can = ROOT.TCanvas("can_pass_"+sr, "can_pass_"+sr)
    ROOT.gPad.SetLogz(1)
    hists[sr]["pass"].Draw("colz")
    h_temp = hists[sr]["pass"].Clone("text_"+sr)
    h_temp.SetMarkerSize(2)
    h_temp.SetMarkerColor(ROOT.kWhite)
    h_temp.Draw("text90 same")
    #raw_input("...")
    can.SaveAs("plots/"+sr+"_pass.pdf")
'''

