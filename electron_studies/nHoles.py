# Goal: Estimate the fake electron contribution to the SR

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gStyle.SetErrorX(0.45)
#ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
samples = {
        #"photon":   {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/042319/photon.root"},
        #"data":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/data/data15.root"},
        #"signal":     {"fname": "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal/399015.root"},
        "signal":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_children/data-trees/mc16_13TeV.399048.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_500_0_1ns.r101nochanges_1kevt.AOD.pool.root.root"},
        #"data":   {"fname": "/afs/cern.ch/user/t/tholmes/LLP/FactoryTools_v3/WorkArea/run/submit_dir_children_data/data-trees/DAOD_RPVLL.14400886._000210.pool.root.1.root"},
        }

variables = {
        "nMissingLayers": "event.electron_nMissingLayers[i]",
        "nHoles": "event.electron_nPixHoles[i] + event.electron_nSCTHoles[i]",
        "nPixHoles": "event.electron_nPixHoles[i]",
        "SCTCorr": "event.electron_nPixHoles[i] + max(0, 4-event.electron_nSCT[i])",
}

hists = {}
for v in variables:
    hists[v] = ROOT.TH2F("nMissingLayers_v_%s"%v, "nMissingLayers_v_%s"%v, 15, -0.5, 14.5, 15, -0.5, 14.5)


t = getTree(samples["signal"]["fname"])
#t = getTree(samples["data"]["fname"])
n_events=0
for event in t:

    if n_events%5000==0: print "Processing event", n_events
    n_events += 1

    for i in xrange(len(event.electron_pt)):
        for v in variables:
            var_val = eval(variables[v])
            #print variables[v]
            #print var_val
            hists[v].Fill(event.electron_nMissingLayers[i], var_val)

cans = {}
for v in variables:
    cans[v] = ROOT.TCanvas("c_%s"%v, "c")
    hists[v].GetXaxis().SetTitle("nMissingLayers")
    hists[v].GetYaxis().SetTitle(v)
    hists[v].Draw("colz")
    raw_input("...")

