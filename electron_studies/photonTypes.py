# Goal: Study whether or not using objects in the photon container is useful

import glob
import ROOT
import os

execfile("../plot_helpers/basic_plotting.py")
execfile("../plot_helpers/selections.py")
ROOT.gROOT.SetBatch(1)
#ROOT.ROOT.EnableImplicitMT()
#ROOT.gStyle.SetPalette(ROOT.kBird)

# Settings
lumi = 140000
#d = "/afs/cern.ch/work/t/tholmes/LLP/ntuples/staus"
#d = "/eos/user/l/lhoryn/highd0/stau/"
#d = "/eos/user/l/lhoryn/highd0/120318/"
d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/signal"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/photon.root"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/041119"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/082019_photon_studies"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v2_Aug2019/signal"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v3_October2019/signal"
#d = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/090219_fixed_photon_def"
#d = "/eos/user/l/lhoryn/electron_fix/"


# File type
f_search = "SlepSlep"
#f_search = "MRChanges*1kevt_rel93.ntup"
append = "_v5.1"
#append = "_updatedreco"
#append = "_slep_noamb"

# Define grid
#lifetimes = ["0p01ns", "0p1ns", "1ns"]
lifetimes = ["*ns"]
#masses = ["50", "100", "200", "300", "400", "500", "700"]
masses = ["100", "300", "500"]
#masses = ["500"]
#maximums = {"100":22, "300":7.5, "500":2.2}

# Channels
# 0 = ee, 1 = mm, 2 = em, 3 = me

variables = {
        "pt":  {"nbins": 25,    "xmin": 0.,     "xmax": 200.,     "xlabel": "True Electron p_{T} [GeV]"},
        "d0":  {"nbins": 25,    "xmin": 0.,   "xmax": 200.,       "xlabel": "True Electron d_{0} [mm]"},
        "R":   {"nbins": 60,    "xmin": 0.,   "xmax": 600.,       "xlabel": "True Electron decay R [mm]"},
        #"electron_dpt":  {"nbins": 25,    "xmin": -1.,   "xmax": 5., "xlabel": "(electron_trackpt - electron_pt)/electron_pt"},
        }


# Conversion types
types = {   "e": "Reconstructed Electron",
            "0": "Unconverted Photon",
            "1": "singleSi Photon",
            "2": "singleTRT Photon",
            "3": "doubleSi Photon",
            "4": "doubleTRT Photon",
            "5": "doubleSiTRT Photon"}

# Loop over samples
samples = {}
for lt in lifetimes:
    samples[lt] = {}
    for m in masses:
        samples[lt][m] = {"hists": {}, "effhists": {}, "cans": {}, "mods": {}}

        print "\nWorking with point lt =",lt,"m =",m
        f_string = "%s/*%s*_%s*_%s*trees.root/*.root"%(d, f_search, m, lt)
        #f_string = "%s/*%s*%s*%s*.root"%(d, m, lt, f_search)

        files = glob.glob(f_string)
        print "Found", len(files), "files."
        print files
        tree = getTree(f_string)
        dframe = ROOT.ROOT.RDataFrame(tree)

        # Get crossection
        #xs = getXS(files[0].split("/")[-2].split(".")[3])
        #n_events = getNEvents(f_string)
        #print "Cross section:", xs, "N events:", n_events
        #ev_weight = lumi*xs/n_events
        #print "Found", dframe.Count().GetValue(), "events."

        # Define an acceptance for true electrons
        true_e_sel = "abs(truthLepton_pdgId[i])==11 && truthLepton_pt[i] > 65 && abs(truthLepton_d0[i])>3"
        n_e = "int n_e = 0; for (int i=0; i<truthLepton_pt.size(); i++) { if (%s) n_e++; } return n_e;"%true_e_sel
        true_e_pt = '''auto v = truthLepton_pt; v.clear();
                        for (int i=0; i<truthLepton_pt.size(); i++)
                            if (%s) v.push_back((double)truthLepton_pt[i]);
                        return v;'''%true_e_sel
        true_e_d0 = true_e_pt.replace("(double)truthLepton_pt[i]", "(double)truthLepton_d0[i]")
        true_e_bar = true_e_pt.replace("(double)truthLepton_pt[i]", "(double)truthLepton_barcode[i]")
        true_e_R = '''auto v = truthLepton_pt; v.clear();
                        for (int i=0; i<truthLepton_pt.size(); i++)
                            if (%s) v.push_back((double)TMath::Sqrt(truthLepton_VtxX[i]*truthLepton_VtxX[i]+truthLepton_VtxY[i]*truthLepton_VtxY[i]));
                        return v;'''%true_e_sel
        df_tvars = dframe.Define("n_true_e", n_e).Define("true_pt", true_e_pt).Define("true_d0", true_e_d0).Define("true_R", true_e_R).Define("true_barcode", true_e_bar)
        df_e = df_tvars.Filter("n_true_e > 0", "e selection")

        '''
        n_e = "int n_e = 0; for (int i=0; i<truthLepton_pt.size(); i++) { if (abs(truthLepton_pdgId[i])==11 && truthLepton_pt[i] > 65 && abs(truthLepton_d0[i])>3) n_e++; } return n_e;"
        df_ne = dframe.Define("n_true_e", n_e)
        df_ee = df_ne.Filter("n_true_e > 0", "e selection")
        truth_R = ' ' 'auto v = truthLepton_pt; v.clear();
                for (int i=0; i<truthLepton_pt.size(); i++)
                    v.push_back((double)TMath::Sqrt(truthLepton_VtxX[i]*truthLepton_VtxX[i]+truthLepton_VtxY[i]*truthLepton_VtxY[i]));
                return v;' ' '
        df_trueR = df_ee.Define("truthLepton_R", truth_R)
        '''

        # Select all truth-matched electrons
        matched_e_pt = '''auto v = electron_pt; v.clear();
        for (int i=0; i<electron_pt.size(); i++)
            for (int j=0; j<true_pt.size(); j++)
                if ( true_barcode[j] == electron_truthMatchedBarcode[i] && electron_pt[i]>65)
                    v.push_back((double)true_pt[j]);
        return v;'''
        matched_e_d0 = matched_e_pt.replace("(double)true_pt", "(double)true_d0")
        matched_e_R  = matched_e_pt.replace("(double)true_pt", "(double)true_R")
        df_pt = df_e.Define("tm_electron_pt", matched_e_pt)
        df_d0 = df_pt.Define("tm_electron_d0", matched_e_d0)
        df_R = df_d0.Define("tm_electron_R", matched_e_R)

        # Select all truth-matched photons. To start, just use a similar pT (within 10%)
        matched_g_pt = '''auto v = photon_pt; v.clear();
        for (int i=0; i<photon_pt.size(); i++)
            for (int j=0; j<true_pt.size(); j++)
                if ( photon_pt[i]>65 && photon_truthMatchedBarcode[i]==-1 && abs(photon_pt[i] - true_pt[j])/true_pt[j] <.1 && photon_conversionType[i] == 0 && photon_passOR[i])
                    v.push_back((double)true_pt[j]);
        return v;'''
        matched_g_d0 = matched_g_pt.replace("(double)true_pt", "(double)true_d0")
        matched_g_R  = matched_g_pt.replace("(double)true_pt", "(double)true_R")
        matched_1_pt = matched_g_pt.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 1")
        matched_1_d0 = matched_g_d0.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 1")
        matched_1_R  =  matched_g_R.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 1")
        matched_2_pt = matched_g_pt.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 2")
        matched_2_d0 = matched_g_d0.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 2")
        matched_2_R  =  matched_g_R.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 2")
        matched_3_pt = matched_g_pt.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 3")
        matched_3_d0 = matched_g_d0.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 3")
        matched_3_R  =  matched_g_R.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 3")
        matched_4_pt = matched_g_pt.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 4")
        matched_4_d0 = matched_g_d0.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 4")
        matched_4_R  =  matched_g_R.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 4")
        matched_5_pt = matched_g_pt.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 5")
        matched_5_d0 = matched_g_d0.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 5")
        matched_5_R  =  matched_g_R.replace("photon_conversionType[i] == 0", "photon_conversionType[i] == 5")
        print matched_5_R
        df_g = df_R.Define("tm_photon_pt_g", matched_g_pt).Define("tm_photon_d0_g", matched_g_d0).Define("tm_photon_R_g", matched_g_R)
        df_1 = df_g.Define("tm_photon_pt_1", matched_1_pt).Define("tm_photon_d0_1", matched_1_d0).Define("tm_photon_R_1", matched_1_R)
        df_2 = df_1.Define("tm_photon_pt_2", matched_2_pt).Define("tm_photon_d0_2", matched_2_d0).Define("tm_photon_R_2", matched_2_R)
        df_3 = df_2.Define("tm_photon_pt_3", matched_3_pt).Define("tm_photon_d0_3", matched_3_d0).Define("tm_photon_R_3", matched_3_R)
        df_4 = df_3.Define("tm_photon_pt_4", matched_4_pt).Define("tm_photon_d0_4", matched_4_d0).Define("tm_photon_R_4", matched_4_R)
        df_5 = df_4.Define("tm_photon_pt_5", matched_5_pt).Define("tm_photon_d0_5", matched_5_d0).Define("tm_photon_R_5", matched_5_R)

        # Plot
        df = df_5

        for v in variables:

            samples[lt][m]["hists"][v] = {}
            samples[lt][m]["mods"][v] = {}
            samples[lt][m]["effhists"][v] = {}

            # Get true distributions
            samples[lt][m]["mods"][v]["true"] = ROOT.RDF.TH1DModel("h_true_%s"%v, "true", variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
            samples[lt][m]["hists"][v]["true"]     = df.Histo1D(samples[lt][m]["mods"][v]["true"], "true_%s"%v)

            # Get distributions for all selections and make efficiencies
            for t in types:
                samples[lt][m]["mods"][v][t] = ROOT.RDF.TH1DModel("h%s_%s"%(t,v), types[t], variables[v]["nbins"], variables[v]["xmin"], variables[v]["xmax"])
                if t == "e":
                    samples[lt][m]["hists"][v][t] = df.Histo1D(samples[lt][m]["mods"][v][t], "tm_electron_%s"%v)
                elif t == "0":
                    samples[lt][m]["hists"][v][t] = df.Histo1D(samples[lt][m]["mods"][v][t], "tm_photon_%s_g"%v)
                else:
                    samples[lt][m]["hists"][v][t] = df.Histo1D(samples[lt][m]["mods"][v][t], "tm_photon_%s_%s"%(v,t))
                    #samples[lt][m]["hists"][v][t] = df.Histo1D(samples[lt][m]["mods"][v][t], "tm_photon_%s"%v)

                #print "Plot of distribution", v, "for sample", t
                #samples[lt][m]["hists"][v][t].Draw()
                #raw_input("...")

                samples[lt][m]["effhists"][v][t] = samples[lt][m]["hists"][v][t].Clone(samples[lt][m]["hists"][v][t].GetName()+"_eff")
                samples[lt][m]["effhists"][v][t].Divide(samples[lt][m]["effhists"][v][t], samples[lt][m]["hists"][v]["true"].Clone("h_true_%s_%s_%s"%(m,lt,v)), 1, 1,"B")

                #print "Plot of efficiency", v, "for sample", t
                #samples[lt][m]["effhists"][v][t].Draw()
                #raw_input("...")

            # Draw plots
            can_name = "c_%s_%s_%s"%(m, lt, v)
            samples[lt][m]["cans"][v] = ROOT.TCanvas(can_name, can_name)

            for i, t in enumerate(samples[lt][m]["effhists"][v]):

                if i==0:
                    samples[lt][m]["effhists"][v][t].SetMinimum(0)
                    samples[lt][m]["effhists"][v][t].SetMaximum(1.2)
                    samples[lt][m]["effhists"][v][t].GetXaxis().SetTitle(variables[v]["xlabel"])
                    samples[lt][m]["effhists"][v][t].GetYaxis().SetTitle("Efficiency")
                    samples[lt][m]["effhists"][v][t].Draw("hist PLC PMC")
                else:
                    samples[lt][m]["effhists"][v][t].Draw("same hist PLC PMC")

            leg = samples[lt][m]["cans"][v].BuildLegend(.5,.65,.8,.90)
            leg.Draw()

            ROOT.ATLASLabel(0.2,0.85, "Internal")
            text = ROOT.TLatex()
            text.SetNDC()
            text.SetTextSize(0.04)
            text.DrawLatex(0.2,0.78, "Simulation, 140 fb^{-1}")

            #raw_input("...")

            samples[lt][m]["cans"][v].SaveAs(can_name+append+".pdf")

        ## Cutflow for fun
        #report = dframe.Report()
        #report.Print()

