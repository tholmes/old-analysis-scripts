import math

def getDPhi(phi1, phi2):

    dphi = abs(phi1-phi2)
    if dphi > math.pi:
        dphi = 2*math.pi - dphi
    return dphi

def getDR(phi1, phi2, eta1, eta2):

    dphi = getDPhi(phi1, phi2)
    deta = abs(eta1-eta2)
    return math.sqrt(dphi**2 + deta**2)

