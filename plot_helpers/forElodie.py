import ROOT

execfile("basic_plotting.py")
ROOT.gROOT.SetBatch(1)

c = ROOT.TCanvas()

ROOT.gStyle.SetPalette(ROOT.kBird)

h = ROOT.TH2F("h","h",20,0,100,70,1,8)

#just fil with whatever
for i in xrange(1,h.GetNbinsX()+1):
    for j in xrange(1,h.GetNbinsY()+1):
        h.SetBinContent(i,j, i*j)

h.Draw("colz")
c.SaveAs("test_before.pdf")
c.Clear()


## set bin labels on main hist
h.GetYaxis().SetTitle("lifetime ns")
h.GetYaxis().SetBinLabel(10, "0.001")
h.GetYaxis().SetBinLabel(20, "0.01")
h.GetYaxis().SetBinLabel(30, "0.01")
h.GetYaxis().SetBinLabel(40, "1")
h.GetYaxis().SetBinLabel(50, "10")
h.GetYaxis().SetBinLabel(60, "100")
h.GetYaxis().SetTickSize(0) # no ticks
h.Draw("colz")

## make a new axis to set the ticks (0 length in x, full anxis range in y)
labels = ROOT.TGaxis(c.GetUxmin(), c.GetUymin(), c.GetUxmin(), c.GetUymax(), 1, 8)
labels.SetLabelSize(0)
labels.SetNdivisions(8)
labels.Draw()

c.Modified()
c.Update()
c.SaveAs("test_after.pdf")
