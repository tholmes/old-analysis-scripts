regions = {
    "sr_ee" : "event.triggerRegion_pass and abs(event.deltaR) > 0.2 and event.electron_n_signal > 1",
    "sr_mm" : "event.triggerRegion_pass and abs(event.deltaR) > 0.2 and event.muon_n_signal > 1",
    "sr_em" : "event.triggerRegion_pass and abs(event.deltaR) > 0.2 and event.muon_n_signal > 0 and event.electron_n_signal > 0",

    "cr_b_m" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.lepton_n_baseline == 1 and event.muon_n_basline == 1",
    "cr_s_m" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.lepton_n_signal == 1 and event.muon_n_signal == 1 and abs(event.muon_t0avg[0]) < 30",
    "cr_b_e" : "event.pass_HLT_g140_loose == 1 and event.lepton_n_baseline == 1 and event.electron_n_baseline == 1",
    "cr_s_e" : "event.pass_HLT_g140_loose == 1 and event.lepton_n_signal == 1 and event.electron_n_signal == 1",

    "cr_ee_fake" : "event.channel == 0 and (event.electron_isSRcuts[0] == 1 and event.electron_isIsolated[0] == 1 and event.electron_isGoodQual[0] == 0) and (event.electron_isSRcuts[1] == 1 and event.electron_isIsolated[1] == 1 and event.electron_isGoodQual[1] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",
    "cr_em_fake" : "(event.channel == 2 or event.channel == 3)  and (event.electron_isSRcuts[0] == 1 and event.electron_isIsolated[0] == 1 and event.electron_isGoodQual[0] == 0) and (event.muon_isSRcuts[0] == 1 and event.muon_isIsolated[0] == 1 and event.muon_isGoodQual[0] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",
    "cr_hf_mm" : "event.channel == 1 and event.cosmicEvent == 0 and (event.muon_isIsolated[0] == 0 or event.muon_isIsolated[1] == 0) and abs(event.deltaR) > 0.2",
    "cr_mm_topbad" : "event.channel == 1 and event.triggerRegion_pass and ( (event.muon_phi[0] > 0 and event.muon_isSRcuts[0] == 1 and event.muon_isIsolated[0] == 1 and event.muon_isPassIDtrk[0] == 1 and event.muon_isGoodQual[0] == 0 and event.muon_phi[1] < 0 and event.muon_isSignal[1] == 1) or (event.muon_phi[1] > 0 and event.muon_isSRcuts[1] == 1 and event.muon_isPassIDtrk[1] == 1 and event.muon_isIsolated[1] == 1 and event.muon_isGoodQual[1] == 0 and event.muon_phi[0] < 0 and event.muon_isSignal[0] == 1)) and event.muon_n_cosmic == 0",

    "cr_mm_botbad" : "event.channel == 1 and event.triggerRegion_pass and ( (event.muon_phi[0] > 0 and event.muon_isSRcuts[0] == 1 and event.muon_isIsolated[0] == 1 and event.muon_isPassIDtrk[0] == 1 and event.muon_isNotCosmic[0] == 1 and event.muon_chi2l3[0] == 1 and event.muon_phi[1] < 0 and event.muon_isSRcuts[1] == 1 and event.muon_isGoodQual[1] == 0) or (event.muon_phi[1] > 0 and event.muon_isSRcuts[1] == 1 and event.muon_isPassIDtrk[1] == 1 and event.muon_isIsolated[1] == 1 and event.muon_isNotCosmic[1] == 1 and event.muon_chi2l3[0] == 1 and event.muon_phi[0] < 0 and event.muon_isSRcuts[0] == 1 and event.muon_isGoodQual[0] == 0 ))",

    "vr_M" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.electron_n_baseline == 0 and event.muon_n_cosmic > 0 ",
    "vr_M_narrow" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.electron_n_baseline == 0 and event.muon_n_cosmic_narrow > 0 ",

    "vr_ee_fake" : "event.channel == 0 and (event.electron_isSRcuts[0] == 1 and event.electron_isIsolated[0] == 1 and event.electron_dptgt0p5[0] == 0) and (event.electron_isSRcuts[1] == 1 and event.electron_isIsolated[1] == 1 and event.electron_dptgt0p5[1] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",
    "vr_ee_fake_hf" : "event.channel == 0 and (event.electron_isSRcuts[0] == 1 and event.electron_isSRcuts[1] == 1) and (event.electron_isIsolated[0] == 0 or event.electron_isIsolated[1] == 0) and (event.electron_isGoodQual[0] == 0 or event.electron_isGoodQual[1] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",
    "vr_em_fake" : "(event.channel == 2 or event.channel == 3) and (event.electron_isSRcuts[0] == 1 and event.electron_isIsolated[0] == 1 and event.electron_dptgt0p5[0] == 0) and (event.muon_isSRcuts[0] == 1 and event.muon_isIsolated[0] == 1 and event.muon_chi2l3[0] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",
    "vr_em_fake_hf" : "(event.channel == 2 or event.channel == 3) and (event.electron_isSRcuts[0] == 1 and event.muon_isSRcuts[0] == 1) and (event.electron_isIsolated[0] == 0 or event.muon_isIsolated[0] == 0) and (event.electron_isGoodQual[0] == 0 or event.muon_isGoodQual[0] == 0) and abs(event.deltaR) > 0.2 and event.cosmicEvent == 0",


    "vr_mM" : "event.channel == 1 and event.muon_n_cosmic == 1 and abs(event.deltaR) > 0.2",
    "vr_mM_narrow" : "event.channel == 1 and event.muon_n_cosmic_narrow == 1 and abs(event.deltaR) > 0.2",
    "vr_m_b_narrow" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.electron_n_baseline == 0 and event.muon_n_basline == 1 and event.muon_n_cosmic_narrow == 0",
    "vr_m_s_narrow" : "event.pass_HLT_mu60_0eta105_msonly == 1 and event.electron_n_baseline == 0 and event.muon_n_basline == 1 and event.muon_isSRcuts[0] == 1 and event.muon_isGoodQual[0] == 1 and event.muon_isIsolated[0] == 1 and event.muon_isPassIDtrk[0] == 1 and event.muon_n_cosmic_narrow == 0"

}
