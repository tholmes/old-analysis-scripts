regions = {
    "cr_b_m" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_baseline == 1 && muon_n_basline == 1",
    "cr_s_m" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) < 30",
    "cr_b_e" : "pass_HLT_g140_loose == 1 && lepton_n_baseline == 1 && electron_n_baseline == 1",
    "cr_s_e" : "pass_HLT_g140_loose == 1 && lepton_n_baseline == 1 && electron_n_signal == 1",
    
    "cr_s_m_tl5" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) < 5 && muon_t0avg[0] != 0",
    "cr_s_m_tl10" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) < 10 && muon_t0avg[0] != 0",
    "cr_s_m_tl20" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) < 20 && muon_t0avg[0] != 0",
    "cr_s_m_tg5" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) > 5 && fabs(muon_t0avg[0]) < 30 && muon_t0avg[0] != 0",
    "cr_s_m_tg10" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) > 10 && fabs(muon_t0avg[0]) < 30 && muon_t0avg[0] != 0",
    "cr_s_m_tg20" : "pass_HLT_mu60_0eta105_msonly == 1 && lepton_n_signal == 1 && muon_n_signal == 1 && fabs(muon_t0avg[0]) > 20 && fabs(muon_t0avg[0]) < 30 && muon_t0avg[0] != 0",
    
    
    "cr_ee_fake" : "channel == 0 && (electron_isSRcuts[0] == 1 && electron_isIsolated[0] == 1 && electron_isGoodQual[0] == 0) && (electron_isSRcuts[1] == 1 && electron_isIsolated[1] == 1 && electron_isGoodQual[1] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
    "cr_em_fake" : "(channel == 2 || channel == 3)  && (electron_isSRcuts[0] == 1 && electron_isIsolated[0] == 1 && electron_isGoodQual[0] == 0) && (muon_isSRcuts[0] == 1 && muon_isIsolated[0] == 1 && muon_isGoodQual[0] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
    "cr_hf_mm" : "channel == 1 && cosmicEvent == 0 && (muon_isIsolated[0] == 0 || muon_isIsolated[1] == 0) && fabs(deltaR) > 0.2",
    "cr_mm_topbad" : "channel == 1 && triggerRegion_pass && ( (muon_phi[0] > 0 && muon_isSRcuts[0] == 1 && muon_isIsolated[0] == 1 && muon_isPassIDtrk[0] == 1 && muon_isGoodQual[0] == 0 && muon_phi[1] < 0 && muon_isSignal[1] == 1) || (muon_phi[1] > 0 && muon_isSRcuts[1] == 1 && muon_isPassIDtrk[1] == 1 && muon_isIsolated[1] == 1 && muon_isGoodQual[1] == 0 && muon_phi[0] < 0 && muon_isSignal[0] == 1)) && muon_n_cosmic == 0",
    
    "vr_M" : "pass_HLT_mu60_0eta105_msonly == 1 && electron_n_baseline == 0 && muon_n_cosmic > 0 ",
    "vr_M_narrow" : "pass_HLT_mu60_0eta105_msonly == 1 && electron_n_baseline == 0 && muon_n_cosmic_narrow > 0 ",
   
    "vr_ee_fake" : "channel == 0 && (electron_isSRcuts[0] == 1 && electron_isIsolated[0] == 1 && electron_dptgt0p5[0] == 0) && (electron_isSRcuts[1] == 1 && electron_isIsolated[1] == 1 && electron_dptgt0p5[1] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
    "vr_ee_fake_hf" : "channel == 0 && (electron_isSRcuts[0] == 1 && electron_isSRcuts[1] == 1) && (electron_isIsolated[0] == 0 || electron_isIsolated[1] == 0) && (electron_isGoodQual[0] == 0 || electron_isGoodQual[1] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
    "vr_em_fake" : "(channel == 2 || channel == 3) && (electron_isSRcuts[0] == 1 && electron_isIsolated[0] == 1 && electron_dptgt0p5[0] == 0) && (muon_isSRcuts[0] == 1 && muon_isIsolated[0] == 1 && muon_chi2l3[0] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
    "vr_em_fake_hf" : "(channel == 2 || channel == 3) && (electron_isSRcuts[0] == 1 && muon_isSRcuts[0] == 1) && (electron_isIsolated[0] == 0 || muon_isIsolated[0] == 0) && (electron_isGoodQual[0] == 0 || muon_isGoodQual[0] == 0) && fabs(deltaR) > 0.2 && cosmicEvent == 0",
   
   
    "vr_mM" : "channel == 1 && muon_n_cosmic == 1 && fabs(deltaR) > 0.2",
    "vr_mM_narrow" : "channel == 1 && muon_n_cosmic_narrow == 1 && fabs(deltaR) > 0.2",
    "vr_m_narrow" : "pass_HLT_mu60_0eta105_msonly == 1 && electron_n_baseline == 0 && muon_n_basline == 1 && muon_n_cosmic_narrow == 0"
   
}
