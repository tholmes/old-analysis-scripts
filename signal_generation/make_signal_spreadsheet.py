# Script to generate the signal spreadsheet required for MC gen

lifetimes = ["0p001", "0p01", "0p1", "1"]
masses = [50, 100, 200, 300, 400, 500, 600, 700, 800]
xs = {      # in fb
        50: { "left": 4104.9,   "right": 1377.6 },
        100: { "left": 270.79,  "right": 96.51 },
        200: { "left": 21.81,   "right": 8.15 },
        300: { "left": 4.43,    "right": 1.68 },
        400: { "left": 1.31,    "right": 0.50 },
        500: { "left": 0.4786,  "right": 0.1886 },
        600: { "left": 0.1969,  "right": 0.0778 },
        700: { "left": 0.0882,  "right": 0.0352 },
        800: { "left": 0.0403,  "right": 0.0156 },
    }
flavor = ["StauStau", "SlepSlep"]

mc_campaign = "mc16a"
#mc_campaign = "mc16d"
#mc_campaign = "mc16e"
#mc_campaign = "mc16d/e"

class SignalPoint:
    def __init__ (self):
        self.Name = ""
        self.JO = ""
        self.ComEnergy = 0
        self.nEvGen = 0
        self.nFullSim = 0
        self.nFastSim = 0
        self.Priority = 0
        self.OutputFormats = ""
        self.CrossSection = 0
        self.EffectiveLumi = ""
        self.FilterEff = ""
        self.EvGenCPU = ""
        self.InputFiles = ""
        self.MCTag = ""
        self.Release = ""
        self.Comment = ""

    def __str__(self):

        self.Comment = "MinEvents = %d"%self.getMinEvents()
        self.EffectiveLumi = self.getEffectiveLumi()

        output_str = ""
        output_str += "%s\t"%self.Name
        output_str += "%s\t"%self.JO
        output_str += "%s\t"%self.ComEnergy
        output_str += "%s\t"%self.ignoreZero(self.nEvGen)
        output_str += "%s\t"%self.ignoreZero(self.nFullSim)
        output_str += "%s\t"%self.ignoreZero(self.nFastSim)
        output_str += "%s\t"%self.Priority
        output_str += "%s\t"%self.OutputFormats
        output_str += "%f\t"%self.CrossSection
        output_str += "%s\t"%self.EffectiveLumi
        output_str += "%s\t"%self.FilterEff
        output_str += "%s\t"%self.EvGenCPU
        output_str += "%s\t"%self.InputFiles
        output_str += "%s\t"%self.MCTag
        output_str += "%s\t"%self.Release
        output_str += "%s\t"%self.Comment

        return output_str

    def ignoreZero(self, number):
        if number == 0: return ""
        else: return number

    def getTotalEvents(self):
        return self.nEvGen + self.nFullSim + self.nFastSim

    def getMinEvents(self):
        return 10000
        #total_events = self.getTotalEvents()
        #if total_events%5000 == 0: return 5000
        #elif total_events%4000 == 0: return 4000
        #elif total_events%2000 == 0: return 2000
        #else:
        #    print "Error, could not set minEvents."
        #    return -1

    def getEffectiveLumi(self):
        total_events = self.getTotalEvents()

        # effective lumi [pb-1] = n events / cross section
        if self.CrossSection != 0:
            return 0.001*float(total_events)/self.CrossSection
        else:
            print "Couldn't get effective lumi; no cross section defined."
            return 0

dsid = 399000
for fl in flavor:
    for mass in masses:
        for lt in lifetimes:

            # Set up default values
            sig = SignalPoint()
            sig.Name = "MGPy8EG_A14NNPDF23LO_%s_directLLP_%d_0_%sns"%(fl, mass, lt)
            sig.JO = "MC15.%d.%s.py"%(dsid, sig.Name)
            sig.ComEnergy = 13000
            sig.nEvGen = 6000
            sig.nFullSim = 0
            sig.Priority = 2
            sig.CrossSection = .001*(xs[mass]["left"] + xs[mass]["right"])
            if fl=="SlepSlep": sig.CrossSection *= 2    # Double the cross section for e + m
            sig.MCTag = "MC15JobOptions-00-09-35"
            sig.Release = "MCProd-19.2.5.31 for evgen"

            # Set up exceptions
            if fl=="StauStau":
                if mass <= 200: sig.nEvGen = 50000
                if mass == 300:
                    if lt in ["0p01", "0p1"]: sig.nEvGen = 24000
                    else: sig.nEvGen = 12000
            else:
                if mass==300:
                    if lt != "1": sig.nEvGen = 12000
                elif mass<300:
                    if lt == "0p001": sig.nEvGen = 50000
                    elif lt in ["0p01", "1"]: sig.nEvGen = 12000

            # Events for full sim
            if mc_campaign != "mc16e":
                if lt != "0p001" and ((mass in [100, 300, 500]) or (mass==700 and fl=="SlepSlep")):
                    sig.nFullSim = sig.nEvGen

            # Values for campaigns
            if mc_campaign == "mc16a":
                if sig.nEvGen == 50000:
                    sig.nFullSim /= 5
                    sig.nEvGen /= 5
                else:
                    sig.nFullSim /= 3
                    sig.nEvGen /= 3
            elif mc_campaign == "mc16d/e":
                if sig.nEvGen == 50000:
                    sig.nFullSim = sig.nFullSim * 2/5
                    sig.nEvGen = sig.nEvGen * 4/5
                else:
                    sig.nEvGen = sig.nEvGen * 2/3
                    sig.nFullSim /= 3
            else:
                if sig.nEvGen == 50000:
                    sig.nFullSim = sig.nFullSim * 2/5
                    sig.nEvGen = sig.nEvGen * 2/5
                else:
                    sig.nEvGen /= 3
                    sig.nFullSim /= 3


            # Don't count the evgen that goes to sim
            sig.nEvGen = sig.nEvGen - sig.nFullSim

            # Double for sleptons (ee + mm)
            if fl == "SlepSlep":
                sig.nEvGen += sig.nEvGen
                sig.nFullSim += sig.nFullSim

            # High priority
            if mass==300 and lt in ["0p01", "0p1"]: sig.Priority = 1

            # Cut off past our reach
            if fl == "StauStau":
                if mass>=600:
                    if not (mass==600 and lt=="0p1"): continue
            else:
                if mass==800 and lt != "0p1": continue

            print sig
            dsid += 1


