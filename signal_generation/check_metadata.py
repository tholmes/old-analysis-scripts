#!/usr/bin/env python
"""
2019/11/07
Emma Kuwertz

Made with the StauStau and SlepSlep_directLLP samples in mind.

Reads in copy-pasted slepton cross-sections and uncertainties from the LHC xsec twiki (https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections13TeVslepslep#NLO_NLL_lL_lL) of the format:
MASS      XSEC     REL_UNC_DW REL_UNC_UP
150     0.08712      -1.7 %     1.7 %

and compares with the your-signal-out file produced following the instructions here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYSignalXsecMC16
Provides a screen dump of text of the format necessary to open a CENTRPAGE ticket and request a metadata update. 

See for example:
https://its.cern.ch/jira/browse/CENTRPAGE-194

"""
import os,ROOT
from ROOT import *
from math import *

# Create xsec dict from twiki table print out
xsec_file = open("LHC-xsecs.txt")
lhc_xsecs = dict()
for l in xsec_file.readlines():
    # fill dict indexed by sparticle mass
    lhc_xsecs[l.split()[0]] = [0.,0.,0.]
    # xsec
    lhc_xsecs[l.split()[0]][0]=float(l.split()[1])*0.001
    # unc down
    lhc_xsecs[l.split()[0]][1]=fabs(float(l.split()[2])/100.)
    # unc up
    lhc_xsecs[l.split()[0]][2]=float(l.split()[4])/100.


outline=""
meta = open("your-signal-out.txt")
for l in meta.readlines():
    if not "mc15" in l.split("_")[0]: 
        if "ldn/C" in l.split(":")[0]: outline+="ldn/C:dataset_number/I:subprocessID/I:crossSection/D:kFactor/D:genFiltEff/D:crossSectionTotalRelUncertUP/D:crossSectionTotalRelUncertDOWN/D:crossSectionRef/C\n"
        else: outline+=l
        continue
    sparticle = l.split("_")[3]
    mass = l.split("_")[5]
    xsec = float(l.split(";")[3])
    
    # check against LHC twiki
    if not "Slep" in sparticle:
        #if not xsec in lhc_xsecs[mass]: print "xsec differs for %s (%s GeV):"%(sparticle,mass),xsec," -- ", lhc_xsecs[mass][0]
        new_xsec = lhc_xsecs[mass][0]
    # for sleptons (selectron+smuon) need to multiply LHC twiki xsec by 2
    else: 
        #if xsec!=lhc_xsecs[mass][0]*2:  print "xsec differs for %s (%s GeV):"%(sparticle,mass),xsec," -- ", lhc_xsecs[mass][0]*2
        new_xsec = lhc_xsecs[mass][0]*2
    #outline+=l

    outline += l.split(";")[0]+";"+l.split(";")[1]+";"+l.split(";")[2]+";"+str(new_xsec)+";"+l.split(";")[4]+";"+l.split(";")[5]+";"+str(lhc_xsecs[mass][2])+";"+str(lhc_xsecs[mass][1])+";XsecSUSY\n"

print outline
