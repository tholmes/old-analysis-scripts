import pyAMI.client
import pyAMI.atlas.api as AtlasAPI

client = pyAMI.client.Client('atlas')

AtlasAPI.init()

datasets = [
        "mc16_13TeV.361044.Sherpa_CT10_SinglePhotonPt70_140_BFilter.simul.HITS.e3587_s3126",
        "mc16_13TeV.361047.Sherpa_CT10_SinglePhotonPt140_280_BFilter.simul.HITS.e3587_s3126",
        "mc16_13TeV.361050.Sherpa_CT10_SinglePhotonPt280_500_BFilter.simul.HITS.e3587_s3126",
        "mc16_13TeV.361053.Sherpa_CT10_SinglePhotonPt500_1000_BFilter.simul.HITS.e3587_s3126",
        "mc16_13TeV.361056.Sherpa_CT10_SinglePhotonPt1000_2000_BFilter.simul.HITS.e3587_s3126"]

for ds in datasets:
    print float(pyAMI.atlas.api.get_dataset_info(client, ds)[0]["crossSection"])*1000

