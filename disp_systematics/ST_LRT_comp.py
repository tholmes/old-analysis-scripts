import glob
import ROOT
import os


execfile("../../scripts/plot_helpers/basic_plotting.py")

ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)


color = ["#C33C54","#37718E"]

to_plot = {
    "d0":"d_{0}",
    "d0_zoom":"d_{0}",
    "pt":"p_{T}",
    "eta": "#eta",
    "phi": "#phi"
}
f_sig = ROOT.TFile("outputFiles/signal_muon_LRT_eff_firstpass.root")


c = ROOT.TCanvas()

for p in to_plot:
    c.Clear()
    g_st = f_sig.Get("ST_eff_%s"%p)
    g_lrt = f_sig.Get("LRT_eff_%s"%p)
    g_all = f_sig.Get("all_eff_%s"%p)
    g_qual = f_sig.Get("qual_eff_%s"%p)


    g_st.SetMarkerStyle(21)
    g_st.SetMarkerColor(ROOT.TColor.GetColor(color[0]))
    g_st.SetLineColor(ROOT.TColor.GetColor(color[0]))

    g_lrt.SetMarkerStyle(22)
    g_lrt.SetLineColor(ROOT.TColor.GetColor(color[1]))
    g_lrt.SetMarkerColor(ROOT.TColor.GetColor(color[1]))
    g_all.SetMarkerStyle(20)
    g_qual.SetMarkerStyle(24)
    

    leg = ROOT.TLegend(0.52,0.75,0.82,0.92)
    leg.AddEntry(g_all, "all", "p")
    leg.AddEntry(g_qual, "all, w/ quality cuts", "p")
    leg.AddEntry(g_st, "standard tracking", "p")
    leg.AddEntry(g_lrt, "large radius tracking", "p")
    
    g_all.Draw("pa")
    ROOT.gPad.Update()
    g_all.GetPaintedGraph().GetYaxis().SetRangeUser(0,1.4)
    g_all.SetTitle(";true muon %s; Efficiency to reconstruct track"% to_plot[p])
    
    
    g_qual.Draw("psame")
    g_st.Draw("psame")
    g_lrt.Draw("psame")
    leg.Draw("same") 
    
    ROOT.ATLASLabel(0.20,0.88, "Internal")
    text = ROOT.TLatex()
    text.SetNDC()
    text.DrawLatex(0.20,0.83, "Muons, p_{T} > 20 GeV" )
    
    ROOT.gPad.Update()
    c.Update()
    c.SaveAs("outputPlots/stlrt_compare_muon_%s.pdf"%p)
