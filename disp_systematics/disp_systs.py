## return % uncertainty due to displacement
def getDispSyst_el(id0):
    d0 = abs(id0)
    
    if d0 > 0 and d0 < 3: return 0.0 
    if d0 > 3 and d0 < 10: return 2.2786
    if d0 > 10 and d0 < 50: return 6.5601
    if d0 > 50 and d0 < 120: return 12.4947
    if d0 > 120 and d0 < 300: return 26.3930

    print "oh no!"
    return -1

## return % uncertainty due to displacement
def getDispSyst_mu(id0):
    d0 = abs(id0)
    
    if d0 > 0 and d0 < 3: return 0.0 
    if d0 > 3 and d0 < 10: return 1.648
    if d0 > 10 and d0 < 50: return 0.4494
    if d0 > 50 and d0 < 120: return 2.7337
    if d0 > 120 and d0 < 300: return 4.4044

    print "oh no!"
    return -1


