import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#append = "truthMatch"
append = "allmu_rp05_pt30_Rd0_allmass"


#are the truth d0 and R between the same two SI layers -- in cosmics, this should be true
def d0Rlayers(event, it):

    d0 = abs(event.truthLepton_d0[it])
    R = math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2)
    si_layers = [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554]
    
    for i in xrange(len(si_layers)-1):
        if d0 > si_layers[i] and d0 < si_layers[i+1]:
            if R > si_layers[i] and R < si_layers[i+1]:
                return True
            else:
                return False


names = {
    "muon_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "true #mu #eta"}, 
    "track_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "track #mu #eta"}, 
    
    "muon_phi": { "nb": 10, "low": -4, "high": 4, "label": "true #mu #phi"}, 
    "track_phi": { "nb": 10, "low": -4, "high": 4, "label": "true #mu #phi"}, 
    
    "muon_pt": { "bins": [0,50,150,350,1000], "label": "true #mu p_{t}"}, 
    "track_pt": { "bins": [0,50,150,350,1000], "label": "track #mu p_{t}"}, 
    
    "muon_d0": { "bins": [0,3,10,20,50,100,300], "label": "true #mu d_{0}"}, 
    "track_d0": { "bins":  [0,3,10,20,50,100,300],  "label": "track #mu d_{0}"}, 
    
    "muon_z0": { "nb": 10, "low": 0, "high": 500, "label": "true #mu z_{0}"}, 
    "track_z0": { "nb": 10, "low": 0, "high": 500, "label": "track #mu z_{0}"}, 

    "n_matched": { "nb": 10, "low": 0, "high": 10, "label": "N Matched tracks"},

}

names_2d = {
    "muon_pt_d0": { "bins_x": [0,50,150,350,1000], "label_x": "p_{T}", "bins_y": [0,3,10,20,50,100,300], "label_y": "d_{0}", "log":0},
    "track_pt_d0": { "bins_x": [0,50,150,350,1000], "label_x": "p_{T}", "bins_y": [0,3,10,20,50,100,300], "label_y": "d_{0}", "log":0},
    
    "muon_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "track_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
}

h = {}
h2 = {}
h = setHistos("", names)
h2 = setHistos2D("",names_2d)


basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_*_0_*.DAOD_RPVLL_mc16*_v4_Feb27_trees.root/*"
#basename = "/afs/cern.ch/user/e/eressegu/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v4_Feb27_trees.root/*"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

print t.GetEntries()
eventNum = 0
for event in t:
    
    if eventNum%10000==0: print "processing ", eventNum
    eventNum += 1

    #if eventNum > 200000: break
    #if len(event.muon_pt) != 1: continue
    
    try:
        event.idTrack_pt
    except:
        continue
    
       
    #if abs(event.muon_eta[0]) > 1 or abs(event.muon_phi[0]) > 2 or abs(event.muon_phi[0]) < 1 or event.muon_phi[0] > 0: continue 
    #print "passes fiducial selection"
    
    if not event.pass_HLT_mu60_0eta105_msonly: continue

    v_mu = ROOT.TVector3()
    im = -1
    barcode = -1
    for it in xrange(len(event.truthLepton_pt)):
        #if abs(event.truthLepton_pdgId[it]) == 13 and event.truthLepton_pt[it] > 50  and abs(event.truthLepton_eta[it]) < 1.05 and abs(event.truthLepton_parentPdgId[it]) > 100000 and abs(event.truthLepton_d0[it]) > 3 and event.truthLepton_phi[it] > 0: 
        if abs(event.truthLepton_pdgId[it]) == 13 and event.truthLepton_pt[it] > 65 and abs(event.truthLepton_eta[it]) < 1.05 and abs(event.truthLepton_d0[it]) > 3 and abs(event.truthLepton_parentPdgId[it]) > 100000 and d0Rlayers(event, it): 
        #if abs(event.truthLepton_pdgId[it]) == 13 and event.truthLepton_pt[it] > 50  and abs(event.truthLepton_eta[it]) < 1.05 and abs(event.truthLepton_parentPdgId[it]) > 100000 and abs(event.truthLepton_d0[it]) > 3: 
             
            h["muon_eta"].Fill(event.truthLepton_eta[it])
            h["muon_phi"].Fill(event.truthLepton_phi[it])
            h["muon_pt"].Fill(event.truthLepton_pt[it])
            h["muon_d0"].Fill(abs(event.truthLepton_d0[it]))
            h2["muon_pt_d0"].Fill(event.truthLepton_pt[it], abs(event.truthLepton_d0[it]))
            h2["muon_eta_d0"].Fill(event.truthLepton_eta[it], abs(event.truthLepton_d0[it]))
            #h["muon_z0"].Fill(abs(event.muon_IDtrack_z0[0])) # no truth z0

            v_mu.SetPtEtaPhi(event.truthLepton_pt[it], event.truthLepton_eta[it], event.truthLepton_phi[it])
            # for now, only do this for the first muon
            im = it
            barcode = event.truthLepton_barcode[it]
            
            nmatched = 0
            for idt in xrange(len(event.idTrack_pt)):    
                v_trk = ROOT.TVector3()
                v_trk.SetPtEtaPhi(event.idTrack_pt[idt], event.idTrack_eta[idt], event.idTrack_phi[idt])
                 
                #if abs( (event.idTrack_pt[idt] - v_mu.Pt())/v_mu.Pt()) < 0.50 and abs(v_trk.DeltaR(v_mu)) < 0.1:
                #if abs(v_trk.DeltaR(v_mu)) < 0.1:
                #if abs(v_trk.DeltaR(v_mu)) < 0.05 and event.idTrack_pt[idt] > 30 and (event.idTrack_NPix_Hits[idt] + event.idTrack_NSct_Hits[idt]) >= 9:
                if abs(v_trk.DeltaR(v_mu)) < 0.05 and event.idTrack_pt[idt] > 30:
                #if abs(v_trk.DeltaR(v_mu)) < 0.05 and event.idTrack_pt[idt] > 30:
                #if abs(v_trk.DeltaR(v_mu)) < 0.05 and event.idTrack_pt[idt] > 30 :
                #if abs(v_trk.DeltaR(v_mu)) < 0.05:
                #if event.idTrack_truthMatchedBarcode[idt] == barcode: 
                    
                    if nmatched == 0:                    
                        h["track_eta"].Fill(event.truthLepton_eta[im])
                        h["track_phi"].Fill(event.truthLepton_phi[im])
                        h["track_pt"].Fill(event.truthLepton_pt[im])
                        h["track_d0"].Fill(abs(event.truthLepton_d0[im]))
                        h2["track_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                        h2["track_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))
                    nmatched +=1

            h["n_matched"].Fill(nmatched)




print "making eff_eta"
h["eff_eta"] = ROOT.TEfficiency(h["track_eta"], h["muon_eta"])
print "making eff_phi"
h["eff_phi"] = ROOT.TEfficiency(h["track_phi"], h["muon_phi"])
print "making eff_pt"
h["eff_pt"] = ROOT.TEfficiency(h["track_pt"], h["muon_pt"])
print "making eff_d0"
h["eff_d0"] = ROOT.TEfficiency(h["track_d0"], h["muon_d0"])
print "making eff_z0"
h["eff_z0"] = ROOT.TEfficiency(h["track_z0"], h["muon_z0"])
print "making pt_d0"
h2["eff_pt_d0"] = h2["track_pt_d0"].Clone("eff_pt_d0")
h2["eff_pt_d0"].Divide(h2["muon_pt_d0"])
print "making eta_d0"
h2["eff_eta_d0"] = h2["track_eta_d0"].Clone("eff_eta_d0")
h2["eff_eta_d0"].Divide(h2["muon_eta_d0"])

outFile = ROOT.TFile("outputFiles/signal_LRT_eff_%s.root"%append, "RECREATE")

c = ROOT.TCanvas()

h["n_matched"].Draw()
c.SaveAs("outputPlots/signal_nmatched_%s.pdf"%append)

for histo in h:

    h[histo].Write()
    if "eff" in histo:
        h[histo].Draw()
        ROOT.gPad.Update() 
        graph = h[histo].GetPaintedGraph()
        graph.SetMinimum(0)
        graph.SetMaximum(1) 
        ROOT.gPad.Update()
        c.SaveAs("outputPlots/signal_%s_%s.pdf"%(histo, append))



ROOT.gStyle.SetPalette(ROOT.kBird)
for histo in h2:
    h2[histo].Write()
    if "eff" in histo:
        h2[histo].SetMaximum(1)
        h2[histo].SetMinimum(0)
        h2[histo].Draw("COLZ")
        
        tmp = h2[histo].Clone()
        tmp.SetMarkerColor(ROOT.kBlack) 
        tmp.Draw("text same")
        
        c.SaveAs("outputPlots/signal_"+histo+"_"+append+".pdf")
        

