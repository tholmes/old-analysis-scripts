import ROOT
import math
import glob
import os
import sys

execfile("../cosmics/cosmic_helpers.py")				       
execfile("../../scripts/plot_helpers/basic_plotting.py")
ROOT.SetAtlasStyle()
ROOT.gROOT.SetBatch(1)

#append = "truthMatch"
#append = "mu"
append = "mu_300"

if "mu" in append: pdg = 13
if "el" in append: pdg = 11

em = "el"
if "mu" in append: em ="mu"

names = {
    "true_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "true %s #eta"% em}, 
    "track_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "track %s #eta"% em}, 
    "baseline_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "baseline %s #eta"% em}, 
    "signal_eta": { "nb": 10, "low": -2.5, "high": 2.5, "label": "signal %s #eta"% em}, 
    
    "true_phi": { "nb": 10, "low": -3.14, "high": 3.14, "label": "true %s #phi"% em}, 
    "track_phi": { "nb": 10, "low": -3.14, "high": 3.14, "label": "true %s #phi"% em}, 
    "baseline_phi": { "nb": 10, "low": -3.14, "high": 3.14, "label": "baseline %s #phi"% em}, 
    "signal_phi": { "nb": 10, "low": -3.14, "high": 3.14, "label": "signal %s #phi"% em}, 
    
    "true_pt": { "nb": 10, "low": 0, "high": 1000, "label": "true %s p_{T}"% em}, 
    "track_pt": { "nb": 10, "low": 0, "high": 1000, "label": "track %s p_{T}"% em}, 
    "baseline_pt": { "nb": 10, "low": 0, "high": 1000, "label": "baseline %s p_{T}"% em}, 
    "signal_pt": { "nb": 10, "low": 0, "high": 1000, "label": "signal %s p_{T}"% em}, 
    
    #"true_d0": { "nb": 20, "low": 0, "high": 300, "label": "true %s d_{0}"% em}, 
    #"track_d0": { "nb": 20, "low": 0, "high": 300, "label": "track %s d_{0}"% em}, 
    #"baseline_d0": { "nb": 20, "low": 0, "high": 300, "label": "baseline %s d_{0}"% em}, 
    #"signal_d0": { "nb": 20, "low": 0, "high": 300, "label": "signal %s d_{0}"% em}, 
   
    "true_d0": { "bins": [0,3,10,20,50,100,300], "label": "true %s d_{0}"% em}, 
    "track_d0": { "bins": [0,3,10,20,50,100,300], "label": "true %s d_{0}"% em}, 
    "baseline_d0": { "bins": [0,3,10,20,50,100,300], "label": "true %s d_{0}"% em}, 
    "signal_d0": { "bins": [0,3,10,20,50,100,300], "label": "true %s d_{0}"% em}, 
    "signal_d0_ratio": { "bins": [0,3,10,20,50,100,300], "label": "true %s d_{0}"% em}, 
    
    "true_R": {"bins": [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554,700], "label": "true %s R"% em},  
    "track_R": {"bins": [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554,700], "label": "true %s R"% em},  
    "baseline_R": {"bins": [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554,700], "label": "true %s R"% em},  
    "signal_R": {"bins": [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554,700], "label": "true %s R"% em},  
    "signal_R_ratio": {"bins": [0,33.25, 50.5, 88.5, 122.5, 299, 371, 443, 514, 554,700], "label": "true %s R"% em},  
   
    
    #"true_R": { "nb": 20, "low": 0, "high": 700, "label": "true %s R"% em}, 
    #"track_R": { "nb": 20, "low": 0, "high": 700, "label": "track %s R"% em}, 
    #"baseline_R": { "nb": 20, "low": 0, "high": 700, "label": "baseline %s R"% em}, 
    #"signal_R": { "nb": 20, "low": 0, "high": 700, "label": "signal %s R"% em}, 
    
    "n_matched": { "nb": 10, "low": 0, "high": 10, "label": "N Matched tracks"},

}

names_2d = {
    "true_pt_d0": { "nb_x": 5, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "track_pt_d0": { "nb_x": 5, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "baseline_pt_d0": { "nb_x": 5, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "signal_pt_d0": { "nb_x": 5, "low_x": 0, "high_x": 1000, "label_x": "p_{T}", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    
    "true_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "track_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "baseline_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
    "signal_eta_d0": { "nb_x": 6, "low_x": -1.05, "high_x": 1.05, "label_x": "#eta", "nb_y": 5, "low_y": 0, "high_y": 300, "label_y": "d_{0}", "log":0},
}

h = {}
h2 = {}
h = setHistos("", names)
h2 = setHistos2D("",names_2d)

def getTrackIndex(pt):
    for ii in xrange(len(event.idTrack_pt)):
        if event.idTrack_pt[ii] == pt: return ii
    return -1





#basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v4_Feb27/signal/user.tholmes.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v4_Feb27_trees.root/*"
basename = "/eos/atlas/atlascerngroupdisk/phys-susy/Highd0Lep_ANA-SUSY-2018-14/v5.1_May14_NewReco/systs/user.lhoryn.mc16_13TeV.*SlepSlep*300_0_*.v5.1_forsystematics_Jul28_*_trees.root/*.root"

#basename = "/afs/cern.ch/user/e/eressegu/Highd0Lep_ANA-SUSY-2018-14/v5.2_June4_NewReco_withDVs/signal/user.eressegu.mc16_13TeV.*.MGPy8EG_A14NNPDF23LO_SlepSlep_directLLP_300_0_*.DAOD_RPVLL_mc16*_v5.2g_trees.root/*"

AODFiles = glob.glob(basename)
t = ROOT.TChain("trees_SR_highd0_")
for filename in AODFiles:
    t.Add(filename)

print t.GetEntries()
eventNum = 0
for event in t:
    
    if eventNum%10000==0: print "processing ", eventNum
    eventNum += 1

    #if eventNum > 10000: break
    #if len(event.muon_pt) != 1: continue
    
    if not (event.pass_HLT_mu60_0eta105_msonly or event.pass_HLT_2g50_loose or event.pass_HLT_g140_loose): continue
        
    try:
        event.idTrack_pt
    except:
        continue
    
    v_mu = ROOT.TVector3()
    im = -1
    barcode = -1
    for it in xrange(len(event.truthLepton_pt)):
        #if abs(event.truthLepton_pdgId[it]) == pdg and event.truthLepton_pt[it] > 65  and abs(event.truthLepton_eta[it]) < 2.5 and abs(event.truthLepton_parentPdgId[it]) > 100000 and abs(event.truthLepton_d0[it]) > 3: 
        if abs(event.truthLepton_pdgId[it]) == pdg and event.truthLepton_pt[it] > 65  and abs(event.truthLepton_eta[it]) < 2.5 and abs(event.truthLepton_parentPdgId[it]) > 100000 : 
            
            R = math.sqrt(event.truthLepton_VtxX[it]**2 + event.truthLepton_VtxY[it]**2)
            
             
            h["true_eta"].Fill(event.truthLepton_eta[it])
            h["true_phi"].Fill(event.truthLepton_phi[it])
            h["true_pt"].Fill(event.truthLepton_pt[it])
            h["true_d0"].Fill(abs(event.truthLepton_d0[it]))
            h["true_R"].Fill(R)
            h2["true_pt_d0"].Fill(event.truthLepton_pt[it], abs(event.truthLepton_d0[it]))
            h2["true_eta_d0"].Fill(event.truthLepton_eta[it], abs(event.truthLepton_d0[it]))

            im = it
            barcode = event.truthLepton_barcode[it]
            

            ## match tracks
            for idt in xrange(len(event.idTrack_pt)):    
                 
                if event.idTrack_truthMatchedBarcode[idt] == barcode and (event.idTrack_NPix_Holes[idt] + event.idTrack_NSct_Holes[idt]) < 2 and event.idTrack_chi2[idt] < 2 : 
                    
                    h["track_eta"].Fill(event.truthLepton_eta[im])
                    h["track_phi"].Fill(event.truthLepton_phi[im])
                    h["track_pt"].Fill(event.truthLepton_pt[im])
                    h["track_d0"].Fill(abs(event.truthLepton_d0[im]))
                    h["track_R"].Fill(R)
                    h2["track_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                    h2["track_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))


                    if pdg==11:
                        for ie in xrange(len(event.electron_pt)):
                            #if baseline_electron(event, ie) and event.electron_truthMatchedBarcode[ie] == barcode :
                            if event.electron_truthMatchedBarcode[ie] == barcode :
                                h["baseline_eta"].Fill(event.truthLepton_eta[im])
                                h["baseline_phi"].Fill(event.truthLepton_phi[im])
                                h["baseline_pt"].Fill(event.truthLepton_pt[im])
                                h["baseline_d0"].Fill(abs(event.truthLepton_d0[im]))
                                h["baseline_R"].Fill(R)
                                h2["baseline_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                                h2["baseline_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))
                            
                            #if signal_electron(event, ie) and event.electron_truthMatchedBarcode[ie] == barcode:
                            #if event.electron_isSignal[i] and event.electron_truthMatchedBarcode[ie] == barcode:
                            if event.electron_isGoodQual[ie] and event.electron_isIsolated[ie] and event.electron_isPassIDtrk[ie] and event.electron_pt[ie] > 65 and event.electron_truthMatchedBarcode[ie] == barcode:
                                h["signal_eta"].Fill(event.truthLepton_eta[im])
                                h["signal_phi"].Fill(event.truthLepton_phi[im])
                                h["signal_pt"].Fill(event.truthLepton_pt[im])
                                h["signal_d0"].Fill(abs(event.truthLepton_d0[im]))
                                h["signal_R"].Fill(R)
                                h2["signal_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                                h2["signal_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))
                    
                    if pdg==13:
                        for ie in xrange(len(event.muon_pt)):
                            #if baseline_muon(event, ie) and event.muon_truthMatchedBarcode[ie] == barcode:
                            if event.muon_truthMatchedBarcode[ie] == barcode:
                                h["baseline_eta"].Fill(event.truthLepton_eta[im])
                                h["baseline_phi"].Fill(event.truthLepton_phi[im])
                                h["baseline_pt"].Fill(event.truthLepton_pt[im])
                                h["baseline_d0"].Fill(abs(event.truthLepton_d0[im]))
                                h["baseline_R"].Fill(R)
                                h2["baseline_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                                h2["baseline_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))
                            
                            #if preselect_muon(event, ie, dod0=False) and event.muon_truthMatchedBarcode[ie] == barcode:
                            if event.muon_isGoodQual[ie] and event.muon_isIsolated[ie] and event.muon_isNotCosmic[ie] and event.muon_isPassIDtrk[ie] and event.muon_isPassTiming[ie] and event.muon_pt[ie] > 65 and event.muon_truthMatchedBarcode[ie] == barcode:
                                h["signal_eta"].Fill(event.truthLepton_eta[im])
                                h["signal_phi"].Fill(event.truthLepton_phi[im])
                                h["signal_pt"].Fill(event.truthLepton_pt[im])
                                h["signal_d0"].Fill(abs(event.truthLepton_d0[im]))
                                h["signal_R"].Fill(R)
                                h2["signal_pt_d0"].Fill(event.truthLepton_pt[im], abs(event.truthLepton_d0[im]))
                                h2["signal_eta_d0"].Fill(event.truthLepton_eta[im], abs(event.truthLepton_d0[im]))

                     


c = ROOT.TCanvas()
vars1d=["eta","phi","pt","d0","R"]

for v in vars1d:

    if v == "d0" : ROOT.gPad.SetLogx()
    else: ROOT.gPad.SetLogx(0)
    print "making eff_%s"%v

    ## Ratios wrt truth
    #h["track_%s"%v].GetYaxis().SetTitle("Efficiency to reconstruct w.r.t truth")
    h["track_eff_%s"%v] = ROOT.TEfficiency(h["track_%s"%v], h["true_%s"%v])
    h["track_eff_%s"%v].SetName("track_eff_%s"%v)
    h["baseline_eff_%s"%v] = ROOT.TEfficiency(h["baseline_%s"%v], h["true_%s"%v])
    h["baseline_eff_%s"%v].SetName("baseline_eff_%s"%v)
    h["signal_eff_%s"%v] = ROOT.TEfficiency(h["signal_%s"%v], h["true_%s"%v])
    h["signal_eff_%s"%v].SetName("signal_eff_%s"%v)
    
    h["track_eff_%s"%v].SetTitle(";true %s %s; Efficiency to reconstruct w.r.t truth"%(em,v))
    h["track_eff_%s"%v].Draw()
    ROOT.gPad.Update() 
    graph = h["track_eff_%s"%v].GetPaintedGraph()
    graph.SetMinimum(0)
    graph.SetMaximum(1.5) 
    ROOT.gPad.Update()

    h["baseline_eff_%s"%v].SetLineColor(more_colors[0])
    h["baseline_eff_%s"%v].SetMarkerColor(more_colors[0])
    h["signal_eff_%s"%v].SetLineColor(more_colors[1])
    h["signal_eff_%s"%v].SetMarkerColor(more_colors[1])
    
    h["baseline_eff_%s"%v].Draw("same")
    h["signal_eff_%s"%v].Draw("same")

    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    leg.AddEntry(h["track_eff_%s"%v], "track","p")
    leg.AddEntry(h["baseline_eff_%s"%v], "baseline","p")
    leg.AddEntry(h["signal_eff_%s"%v], "signal","p")
    
    leg.Draw("same")
    c.Modified()
    c.SaveAs("outputPlots/disp/signal_effcompare_%s_%s.pdf"%(v,append))
    c.Clear()


    ## Ratios wrt tracking
    h["track_heff_%s"%v] = getHistFromEff( h["track_eff_%s"%v]) 
    h["baseline_heff_%s"%v] = getHistFromEff( h["baseline_eff_%s"%v]) 
    h["signal_heff_%s"%v] = getHistFromEff( h["signal_eff_%s"%v]) 
    
    h["signal_track_eff_%s"%v] = ROOT.TEfficiency(h["signal_heff_%s"%v], h["track_heff_%s"%v])
    h["signal_track_eff_%s"%v].SetName("signal_track_eff_%s"%v)
    h["baseline_track_eff_%s"%v] = ROOT.TEfficiency(h["baseline_heff_%s"%v], h["track_heff_%s"%v])
    h["baseline_track_eff_%s"%v].SetName("baseline_track_eff_%s"%v)

    h["signal_track_eff_%s"%v].Draw()
    ROOT.gPad.Update() 
    graph = h["signal_track_eff_%s"%v].GetPaintedGraph()
    graph.SetTitle(";true %s %s; Efficiency to reconstruct w.r.t tracking efficiency"%(em,v))
    graph.SetMinimum(0)
    graph.SetMaximum(1.5) 
    ROOT.gPad.Update()

    h["baseline_track_eff_%s"%v].Draw("same")
    
    h["baseline_track_eff_%s"%v].SetLineColor(more_colors[0])
    h["baseline_track_eff_%s"%v].SetMarkerColor(more_colors[0])
    h["signal_track_eff_%s"%v].SetLineColor(more_colors[1])
    h["signal_track_eff_%s"%v].SetMarkerColor(more_colors[1])
 
    leg = ROOT.TLegend(0.60,0.75,0.80,0.92)
    leg.AddEntry(h["baseline_track_eff_%s"%v], "baseline","p")
    leg.AddEntry(h["signal_track_eff_%s"%v], "signal","p")
    leg.Draw("same")
    c.Update()
    c.Modified()
    c.SaveAs("outputPlots/disp/signal_effwrttrack_%s_%s.pdf"%(v,append))
    c.Clear()
    
    ROOT.gPad.SetLogx(0)
    ## Ratios wrt tracking
    if "R" in v or "d0" in v:
        for i in xrange(1,h["signal_%s_ratio"%v].GetNbinsX()+1):
            #divide bin by prompt bin
            val = 1.0 - h["signal_track_eff_%s"%v].GetEfficiency(i)/h["signal_track_eff_%s"%v].GetEfficiency(1)
            print "bin %i val %f low edge %f eff at bin %f eff at prompt %f"%(i,val, h["signal_%s_ratio"%v].GetBinLowEdge(i), h["signal_track_eff_%s"%v].GetEfficiency(i), h["signal_track_eff_%s"%v].GetEfficiency(1))
            h["signal_%s_ratio"%v].SetBinContent(i, val)
            #err = val * math.sqrt( (tmp.GetBinError(i)/tmp.GetBinContent(i))**2 + (tmp.GetBinError(1)/tmp.GetBinContent(1))**2)
            #h["signal_%s_ratio"%v].SetBinError(i, err)
                

        h["signal_%s_ratio"%v].SetMaximum(.3)
        h["signal_%s_ratio"%v].SetMinimum(-.3)
        h["signal_%s_ratio"%v].GetXaxis().SetRangeUser(0.00001,h["signal_%s_ratio"%v].GetBinLowEdge(h["signal_%s_ratio"%v].GetNbinsX()+1))
        
        #if "d0" in val: h["signal_%s_ratio"%v].GetXaxis().SetRangeUser(0,700)
        h["signal_%s_ratio"%v].GetYaxis().SetTitle("Systematic Uncertainty")
        h["signal_%s_ratio"%v].SetName("Systematic Uncertainty")
        h["signal_%s_ratio"%v].Draw("hist")
        #if "d0" in v: ROOT.gPad.SetLogx()
        c.Modified()
        c.Update()
        c.SaveAs("outputPlots/disp/signal_sf_%s_%s.pdf"%(v,append))


     
    #drawTwoPads([getHistFromEff(h["signal_track_eff_%s"%v])], h["signal_%s_ratio"%v], savename="outputPlots/disp/signal_sf_%s_%s.pdf"%(v,append))




print "making pt_d0"
h2["eff_pt_d0"] = h2["track_pt_d0"].Clone("eff_pt_d0")
h2["eff_pt_d0"].Divide(h2["true_pt_d0"])
print "making eta_d0"
h2["eff_eta_d0"] = h2["track_eta_d0"].Clone("eff_eta_d0")
h2["eff_eta_d0"].Divide(h2["true_eta_d0"])

outFile = ROOT.TFile("outputFiles/signal_disp_reco_eff_%s.root"%append, "RECREATE")

c.Clear()
for histo in h:
    h[histo].Write()



ROOT.gStyle.SetPalette(ROOT.kBird)
for histo in h2:
    h2[histo].Write()
        

